
jQuery('ul.menu ul').hide();
jQuery('ul.menu li').on('hover',function(){jQuery(this).find('ul').eq(0).slideToggle(200)})

function sticky_relocate() {
	
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
	
	if (window_top > div_top) {
		$('.fixing_head').addClass('stick');
	} else {
		$('.fixing_head').removeClass('stick');
	}
	
	if (window_top > div_top) {
		$('.container_wrap').addClass('stick');
	} else {
		$('.container_wrap').removeClass('stick');
	}
	
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});
