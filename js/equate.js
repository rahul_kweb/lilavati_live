"use strict";
/* Thanks to CSS Tricks for pointing out this bit of jQuery
http://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

var equalheight = function (container) {
var currentTallest = 0,
   currentRowStart = 0,
   rowDivs = [],
   $el,
   topPosition = 0,
   currentDiv;
 $(container).each(function() {
   $el = $(this);
   $($el).height('auto');
   topPosition = $el.position().top;

   if (currentRowStart != topPosition) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPosition;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
};


 //app implementation
function applyEquate() {    
  equalheight('.doc_div');
}

(function($, undefined) {
  var resizeTimer = null;
  $(window).resize(function() {
   clearTimeout(resizeTimer);
   resizeTimer = window.setTimeout(applyEquate, 300);
  });

  $(window).load(function() {
   applyEquate();
  });
})(jQuery);





//setInterval(function () {
//    $(window).ready(function () {
//        equalheight('.equate');
//        equalheight('.inner');
//    });
//}, 1);
//setInterval(function () {
//    $(window).load(function () {
//        equalheight('.equate');
//        equalheight('.inner');
//    });
//}, 1);
$(window).load(function () {
    equalheight('.equate');
    equalheight('.inner');
});
$(window).resize(function () {
    equalheight('.equate');
    equalheight('.inner');
});



$(document).ready(function () {
    $('.c_expand_hd , .tab_list li').click(function () {
        $('.inner').css("height", "auto");
        equalheight('.inner');
    });
});