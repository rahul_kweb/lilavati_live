﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="pain_management.aspx.cs" Inherits="pain_management" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Pain Management</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Pain_Management.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%-- <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litAnesthesiology" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Aim of our Pain management department is to improve quality of life for the patient who are suffering in pain for years together inspite of regular treatment/ medication/ surgery being done. It has two renowned Pain specialists of India namely Dr. D. K. Baheti and Dr. Jitendra Jain with support staff like Pain management nurse, Physiotherapist and Psychologist.</p>
                            <p>Our Pain management department provides round the clock services treating nearly <strong>40-60 OPD</strong> patients and <strong>10-20 IPD</strong> patients per month.</p>
                            <p>The commonly seen patients in Pain clinics at Lilavati are as below :</p>
                            <p>Causes of Common Chronic Pain</p>
                            <ul class="bullet">
                                <li>Chronic Back pain and Slip Disc</li>
                                <li>Neck Pain (Cervical Spondylosis)</li>
                                <li>Chronic shoulder, arm, Hip, Knee, Leg and Foot Pain</li>
                                <li>Cancer Pain </li>
                                <li>Reflex Sympathetic Dystrophy (New name CRPS, type 1 and 2, unexplained pain and swelling of hand or leg) </li>
                                <li>Nerve related pain, Neuropathic Pain, Neuralgias like Trigeminal, Neuropathies like Diabetes and Central Pain like Phantom pain (Pain after amputation, cutting the Limb) </li>
                                <li>Post Surgery/Trauma Persistent Pain (Any kind of Surgery or trauma, like spine or hernia or bypass or chest or any abdominal surgery) </li>
                                <li>Headache and Face pain.</li>
                                <li>Any other pain lasting for more than 3 months even after effective medical or surgical treatment</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Consultation and Counselling </li>
                                <li>Interventional Pain procedures like non-operative slip disc or Spondylosis nerve blocks etc. and plenty more listed below:
    <ul>
        <li>Radiofrequency nerve ablation (RF) for back pain, slip Di Spondylosis, nerve pain and cancer pain </li>
        <li>Local anaesthetic, Steroid, Non steroidal anti inflammatory drugs and other medication assisted various blocks for  back pain, slip di spondylosis, nerve pain and cancer pain </li>
        <li>Chemical Neurolysis for Cancer pain and Nerve pain </li>
        <li>Spinal cord stimulation for Post spine surgery pain, Nerve pain and any pain which not responding the treatment mentioned here</li>
        <li>Intrathecal pumps for Post spine surgery pain, Cancer pain, Nerve pain and any pain which not responding the treatment mentioned here </li>
        <li>Trigger point procedure for persistent Muscle pain or Nerve pain </li>
        <li>PCA* Patient controlled analgesia and continuous catheters for acute pain  for admitted Inpatients and also acute on chronic pain</li>
        <li>Complied Medications</li>
        <li>Combining various treatment like medications, physiotherapy and psychologist for the maximum benefit of the patient.</li>
    </ul>
                                </li>
                                <li>ADVANTAGES of Interventional pain procedures:
    <ul class="bullet">
        <li>More options and flexibility for controlling their pain quickly and immediately in most cases.</li>
        <li>Might need less or no medications for their pain symptom after pain procedure. </li>
        <li>Good options for the patients who are not surgical candidates according to surgeons/physicians or unfit for operative procedures and also an option for those who don’t want surgery like few of Slip Disc, Back pain or Spondylosis patients. </li>
        <li>All above mentioned procedures are day cases except selected few like Implantable Spinal Cord stimulator or implantable pump. </li>
        <li>Performed mostly under local anaesthesia, General anaesthesia is not required. </li>
        <li>Almost all of these procedures are safe. Complications are rare.</li>
        <li>Early post procedure recovery and quick rehabilitation is one of the most important aspect of these procedures.</li>
    </ul>
                                </li>
                            </ul>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <p>This department is fully equipped with all amenities that a world class Pain management department would need. A dedicated team of experienced doctors, dedicated consulting area, pain management nurse, physiotherapy and psychologist along with other referring departments like neurology, psychiatry, oncology, spine, pharmacy and dedicated day care centre for Interventional pain procedure with good fluoroscopy machine, radiofrequency generator and dedicated staff for maintaining it.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

