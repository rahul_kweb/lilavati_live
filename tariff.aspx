﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="tariff.aspx.cs" Inherits="tariff" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Tariff</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>
                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<ul class="bullet">
                    <li><a href="#" target="_blank">INTERVENTIONAL RADIOLOGY PROCEDURES</a></li>
                    <li><a href="docs/tariff/cag-v1.pdf" target="_blank">CAG 1</a></li>
                    <li><a href="docs/tariff/ptca-package-v1.pdf" target="_blank">(PTCA) PACKAGE</a></li>
                    <li><a href="docs/tariff/cabg-package-v1.pdf" target="_blank">(CABG) PACKAGE</a></li>
                    <li><a href="docs/tariff/Tariff_cardiac_cathlab1.pdf" target="_blank">TARIFF CARDIAC CATHLAB</a></li>
                    <li><a href="docs/tariff/Tariff_Surgeries2.pdf" target="_blank">TARIFF SURGERIES</a></li>
                    <li><a href="docs/tariff/Tariff_maternity3.pdf" target="_blank">TARIFF MATERNITY</a></li>
                    <li><a href="docs/tariff/Tariff_General_Services4.pdf" target="_blank">TARIFF GENERAL SERVICES</a></li>
                    <li><a href="docs/tariff/Tariff_Endoscopy5.pdf" target="_blank">TARIFF ENDOSCOPY</a></li>
                    <li><a href="docs/tariff/Tariff_Cataract_And_Opthalmology6.pdf" target="_blank">TARIFF CATARACT & OPTHALMOLOGY</a></li>
                </ul>

                <p><strong class="uppercase">Deposit amount for patients getting admitted in ICCU/ SICU/ ICU/ PICU/ NICU</strong></p>

                <table>
                    <thead>
                        <tr>
                            <td>Class of Patients</td>
                            <td>Deposit Amount ( In Rupees)</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Common</td>
                            <td>1,00,000</td>
                        </tr>
                        <tr>
                            <td>Economy</td>
                            <td>2,00,000</td>
                        </tr>
                        <tr>
                            <td>Twin Sharing</td>
                            <td>2,50,000</td>
                        </tr>
                        <tr>
                            <td>Special </td>
                            <td>3,50,000</td>
                        </tr>
                        <tr>
                            <td>Deluxe</td>
                            <td>3,50,000</td>
                        </tr>
                        <tr>
                            <td>Super Deluxe</td>
                            <td>4,00,000</td>
                        </tr>
                        <tr>
                            <td>Executive Suit</td>
                            <td>5,00,000</td>
                        </tr>
                    </tbody>
                </table>--%>



                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

