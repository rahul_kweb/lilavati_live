﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="visiting_hours.aspx.cs" Inherits="visiting_hours" %>

<%@ Register TagName="Visitors" TagPrefix="menu" Src="Control/visitors.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Visiting Hours</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg" />
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<table>
                    <thead>
                        <tr>
                            <td>Section</td>
                            <td>Monday to Saturday</td>
                            <td>Sunday</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ICU</td>
                            <td>5 PM to 7 PM</td>
                            <td>10 AM to 11 AM / 5 PM to 7 PM</td>
                        </tr>
                        <tr>
                            <td>Ward</td>
                            <td>4 PM to 7 PM</td>
                            <td>10 AM to 11 AM / 4 PM to 7 PM</td>
                        </tr>

                    </tbody>
                </table>

                <p><strong>Children below the age of 10 years are not allowed as visitors.</strong></p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Visitors ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

