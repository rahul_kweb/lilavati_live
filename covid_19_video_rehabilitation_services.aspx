﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="covid_19_video_rehabilitation_services.aspx.cs" Inherits="covid_19_video_rehabilitation_services" %>
<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">COVID – 19 Video Rehabilitation Services (Physiotherapy Department)</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>



                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                             <asp:Literal ID="ltrContent" runat="server"></asp:Literal>







<%--               
                <p>COVID-19 is an infectious viral disease which mainly attacks the lung and is transmitted through the droplets produced when an infected person coughs ,sneezes or exhales.</p>



                <p><strong class="green_text uppercase">Symptoms:</strong></p>
             <img src="images/Symptoms_of_Covid_19.jpg" />

                <p>Patients are classified into:Asymptomatic and Symptomatic</p>
                <p>Recovering from Covid-19 will be slow and the ability to return to everyday tasks will be gradual.</p>
                <p>Rehabilitation improves health outcomes of covid-19 patients and benefits through:-</p>
                <ul class="bullet">
                    <li>Optimizing health and functioning outcomes of elderly and patients with pre-existing health conditions who may be more vulnerable to the effects of severe illness.Rehabilitation can be beneficial for achieving and maintaining their previous level of independence.</li>
                    <li>Facilitating an early discharge.</li>
                    <li>Reducing the risk of hospital readmissions.</li>
                    <li>Improving overall psychological wellbeing.</li>
                    <li>Improving the quality of life.</li>
                </ul>

                <p>At LHRC physiotherapists have been at the forefront in providing the rehabilitation services to covid-19 affected patients since the beginning of the pandemic.</p>
                <p>We provide the rehabilitation services in all the 3 phases of recovery:-</p>
                <ul class="bullet">
                    <li>Acute-ICU-:-Chest physiotherapy, Postural drainage positioning, Breathing techniques, Bed mobility.</li>
                    <li>Subacute-Wards-:Breathing retraining, Coping techniques, Relaxation techniques, Graded mobilization.</li>
                    <li>Long term-Return to home:-
                        <ul>
                            <li>Managing breathlessness:-Breathing control, Positions to relieve breathlessness, Relaxation techniques, Coping techniques.</li>                  
                           
                            <li>Airway clearance techniques:-Postural drainage positions, Coughing and huffing techniques.<br />Breathing techniques to clear airways and use of breathing instruments.</li>
                         <li>Energy conservation techniques :-Positioning, Planning, Pacing, Prioritizing.</li>
                    <li>Functional training:-Managing activities of daily living, Goal oriented activities, Return to your hobbies/related activities</li>
                            <li>Aerobic training:-Monitored exercise regime will be  prescribed to improve cardio respiratory endurance/overall fitness.</li>
                            <li>Strength training:-Upper and lower body major muscle group strengthening exercises.</li>
                        </ul>
                    </li>                   
                </ul>
                <p>Timely reassessment and progression of exercises will be done by the cardio respiratory physiotherapists.</p>
                <p>Covid-19 affected patients may experience remnant symptoms post discharge as well.Rehabilitation with trained physical therapists will aid them to regain their functional independence.
So along with OPD services,we at LHRC are also available for VIDEO-BASED rehabilitation for all covid-19 affected patients.
</p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />


            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>




        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox-media')
                     .attr('rel', 'media-gallery')
                     .fancybox({
                         openEffect: 'none',
                         closeEffect: 'none',
                         prevEffect: 'none',
                         nextEffect: 'none',

                         arrows: false,
                         helpers: {
                             thumbs: { width: 75, height: 50 },
                             media: {},
                             buttons: {}
                         }
                     });

        });
    </script>
</asp:Content>

