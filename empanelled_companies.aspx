﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="empanelled_companies.aspx.cs" Inherits="empanelled_companies" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Empanelled Corporates</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>
                        <div class="tab_content">
                            <p>The biggest and the best Indian companies have for long trusted us for provision of hassle free healthcare for their employees. Empanelment with us ensures fast track admission and cashless hospitalization in case there is an emergency.</p>
                        </div>
                    </div>--%>

                    <div class="tabbertab">
                        <h2>List of Empanalled Corporates</h2>
                        <div class="tab_content">
                            <ul class="emp_clients fade_anim">
                                <asp:Repeater ID="rptListOFEmp" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <img src='<%#Eval("LOGO","uploads/Empanelled_Companies/{0}") %>'>
                                            <span><%#Eval("NAME") %></span> </li>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </ul>
                        </div>
                    </div>



                    <%--  <div class="tabbertab">
                        <h2>Corporates</h2>
                        <div class="tab_content">

                            <ul class="emp_clients fade_anim">
                                <li>
                                    <img src="images/empanelled_corporates/ADITYA-BIRLA-MANAGEMENT-CORPORATION.jpg">
                                    <span>Aditya Birla Management Corporation</span> </li>
                                <li>
                                    <img src="images/empanelled_corporates/AKER-POWERGAS-PRIVATE-LIMITED.jpg">
                                    <span>Aker Powergas Private Limited</span> </li>
                                <li>
                                    <img src="images/empanelled_corporates/AMERICAN-SCHOOL-OF-BOMBAY.jpg">
                                    <span>American School Of Bombay</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/ASSOCIATED-CEMENT-COMPANIES-LIMITED-(ACC).jpg">
                                    <span>Associated Cement Companies Limited (ACC)</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/BANK-OF-INDIA.jpg">
                                    <span>Bank Of India</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/BAYER-(INDIA)-LIMITED.jpg">
                                    <span>Bayer (India) Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/BG-EXPLORATION-PRODUCTION-INDIA-LTD.(BG).jpg" />                                    
                                    <span>Bg Exploration & Production India  Ltd.</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/BHARAT-PETROLEUM-CORPORATION-LIMITED.(BPCL).jpg">
                                    <span>Bharat Petroleum Corporation Limited.</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/BRITISH-DEPUTY-HIGH-COMMISSION.jpg">
                                    <span>British Deputy High Commission</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/CONSULATE-GENERAL-OF-UNITED-ARAB-EMIRATES-(MUMBAI).jpg">
                                    <span>Consulate General Of United Arab Emirates (Mumbai)</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/CONSULATE-GENERAL-OF-UNITED-STATES-OF-AMERICA.jpg">
                                    <span>Consulate General Of United States Of America</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/DOW-CHEMICALS-INTERNATIONAL-LTD.jpg">
                                    <span>Dow Chemicals International Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/ENGINEERING-PROJECTS-(INDIA)-LTD.jpg">
                                    <span>Engineering Projects (India) Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/ESSAR-OIL-LTD.jpg">
                                    <span>Essar Oil Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/GAIL(INDIA)-LTD.jpg">
                                    <span>Gail (India) Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/HINDUSTAN-PETROLEUM-CORPORATION-LIMITED.jpg">
                                    <span>Hindustan Petroleum Corporation Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/HINDUSTAN-UNILEVER-LTD.jpg">
                                    <span>Hindustan Unilever Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/ICICI-BANK-LTD.jpg">
                                    <span>ICICI Bank Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/INDIAN-OIL-CORPORATION.jpg">
                                    <span>Indian Oil Corporation</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/INDIAN-REGISTER-OF-SHIPPING.jpg">
                                    <span>Indian Register Of Shipping</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/INTERNATIONAL-SOS-SERVICES.jpg">
                                    <span>International SOS Services</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/ITC-GRAND-MARATHA-SHERATON-HOTEL-TOWERS.jpg" />                                    
                                    <span>ITC Grand Maratha Sheraton Hotel & Towers</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/KOTAK-GROUP-OF-COMPANIES.jpg">
                                    <span>Kotak Group Of Companies</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/MAHINDRA-MAHINDRA-LTD.jpg">
                                    <span>Mahindra & Mahindra Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/MANGLORE-REFINERY-PETROCHEMICAL-LTD-(MRPL).jpg">
                                    <span>Manglore Refinery & Petrochemical Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/MARICO-INDUSTRIES-LTD.jpg">
                                    <span>Marico Industries Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/MSTC-LIMITED.jpg">
                                    <span>MSTC Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/NATIONAL-BANK-FOR-AGRICULTURE-AND-RURAL-DEVELOPMENT(NABARD).jpg">
                                    <span>National Bank For Agriculture And Rural Development</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/NATIONAL-HYDROELECTRIC-POWER-CORPORATION-LTD.jpg">
                                    <span>National Hydroelectric Power Corporation Ltd</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/PALANPUR-SAMAJ-TRUST.jpg">
                                    <span>Palanpur Samaj Trust</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/PAWAN-HANS-HELICOPTERS-LIMITED.jpg">
                                    <span>Pawan Hans Helicopters Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/PEPSICO-INDIA-HOLDINGS-PVT-LTD.jpg">
                                    <span>Pepsico India Holdings Pvt Ltd.</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/POWER-FINANCE-CORPORATION-LIMITED.jpg">
                                    <span>Power Finance Corporation Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/PROCTER-GAMBLE-INDIA-LIMITED.jpg">
                                    <span>Procter & Gamble India Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/RESERVE-BANK-OF-INDIA.jpg">
                                    <span>Reserve Bank Of India</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/RURAL-ELECTRIFICATION-CORPORATION-LTD.jpg">
                                    <span>Rural Electrification Corporation Ltd.</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/SANOFI-SYNTHELABO-(INDIA)-LTD.jpg">
                                    <span>Sanofi Synthelabo (India) Ltd.</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/SIEMENS-LIMITED.jpg">
                                    <span>Siemens Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/TAJ-LANDS-END-LIMITED.jpg">
                                    <span>Taj Lands End Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/TATA-COMMUNICATIONS-LIMITED.jpg">
                                    <span>Tata Communication Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/TATA-MOTORS-LIMITED.jpg">
                                    <span>Tata Motors Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/TECNIMONT-ICB-PVT-LTD.jpg">
                                    <span>Tecnimont ICB Pvt Ltd.</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/THE-SHIPPING-CORPORATION-OF-INDIA-LIMITED.jpg">
                                    <span>The Shipping Corporation Of India Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/THE-STATE-TRADING-CORP-OF-INDIA-LIMITED.jpg">
                                    <span>The State Trading Corp Of India Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/UNION-BANK-OF-INDIA.jpg">
                                    <span>Union Bank Of India</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/UNITED-PHOSPHORUS-LIMITED(UPL).jpg">
                                    <span>United Phosphorus Limited</span> </li>
                                <li>
                                    <img src="images/empanelled_corporates/UNIT-TRUST-OF-INDIA.jpg">
                                    <span>Unit Trust Of India</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/VARUN-SHIPPING-COMPANY-LIMITED.jpg">
                                    <span>Varun Shipping Company Limited</span></li>
                                <li>
                                    <img src="images/empanelled_corporates/VOLTAS-LIMITED.jpg">
                                    <span>Voltas Limited</span></li>

                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Insurance</h2>
                        <div class="tab_content">

                            <ul class="emp_clients fade_anim">
                                <li>
                                    <img src="images/insurance_companies/APOLLO-MUNICH.jpg">
                                    <span>Apollo Munich</span> </li>
                                <li>
                                    <img src="images/insurance_companies/BAJAJ-ALLIANZ-GENERAL-INSURANCE-CO-LTD.jpg">
                                    <span>Bajaj Allianz General Insurance Co Ltd</span> </li>
                                <li>
                                    <img src="images/insurance_companies/FUTURE-GENERALI-INDIA-INSURANCE-COMPANY-LTD.jpg">
                                    <span>Future Generali India Insurance Company Ltd</span></li>
                                <li>
                                    <img src="images/insurance_companies/HDFC-ERGO.jpg">
                                    <span>HDFC Ergo</span></li>
                                <li>
                                    <img src="images/insurance_companies/ICICI-PRUDENTIAL-LIFE-INSURANCE-COMPANY-LTD.jpg">
                                    <span>ICICI Prudential Life Insurance Company Ltd</span></li>
                                <li>
                                    <img src="images/insurance_companies/ICICI-LOMBARD-GEN-INSURANCE-CO-LTD.jpg">
                                    <span>ICICI Lombard Gen Insurance Co Ltd.</span></li>
                                <li>
                                    <img src="images/insurance_companies/MAX-BUPA-HEALTH-INSURANCE-COMPANY-LIMITED.jpg">
                                    <span>Max Bupa Health Insurance Company Limited</span></li>
                                <li>
                                    <img src="images/insurance_companies/RELIANCE-GENERAL-INSURANCE-CO-LTD.jpg">
                                    <span>Reliance General Insurance Co Ltd.</span></li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>TPA</h2>
                        <div class="tab_content">

                            <ul class="emp_clients fade_anim">
                                <li>
                                    <img src="images/TPA/DEDICATED-HEALTHCARE-SERVICES-(INDIA)-PRIVATE-LIMITED.jpg">
                                    <span>Dedicated Healthcare Services (India) Private Limited</span> </li>
                                <li>
                                    <img src="images/TPA/MD-INDIA-HEALTHCARE-SERVICES.jpg">
                                    <span>MD INDIA HEALTHCARE SERVICES</span> </li>
                                <li>
                                    <img src="images/TPA/MEDI-ASSIST-INDIA-PRIVATE-LIMITED.jpg">
                                    <span>Medi Assist India Private Limited</span></li>
                                <li>
                                    <img src="images/TPA/PARAMOUNT-HEALTHCARE-SERVICES-(TPA)-PVT-LIMITED.jpg">
                                    <span>Paramount Healthcare Services (TPA) Pvt Limited)</span></li>
                                <li>
                                    <img src="images/TPA/RAKSHA-TPA-PVT-LTD.jpg">
                                    <span>Raksha TPA Pvt Ltd</span></li>
                                <li>
                                    <img src="images/TPA/UNITED-HEALTHCARE-(PAREKH)-INDIA-PRIVATE-LIMITED.jpg">
                                    <span>United Healthcare (Parekh) India Private Limited</span></li>
                                <li>
                                    <img src="images/TPA/VIPUL-MEDCORP-TPA-PVT-LTD.jpg">
                                    <span>Vipul Medcorp TPA Pvt Ltd</span></li>

                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

