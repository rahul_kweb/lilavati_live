﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="convenience_facilities.aspx.cs" Inherits="convenience_facilities" %>

<%@ Register TagName="Visitors" TagPrefix="menu" Src="Control/visitors.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Convenience & Facilities</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>
                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<p><strong class="green_text uppercase">Cafeteria</strong></p>
                <p>Our cafeteria is located on the 2<sup>nd</sup> Floor. The meals served here are delicious. It is a convenient option for all visitors to have refreshment within hospital premises at a nominal rate.</p>
                <p>Cafeteria Timing: 7.30 AM to 9.30 PM.</p>
                <p><strong class="green_text uppercase">Telephone</strong></p>
                <p>Available in the patient room</p>
                <ul class="bullet">
                    <li><strong>1st to 9th Floor :</strong> Dial 26758 or 26568 followed by bed no. e.g. bed no. 924; Dial 26758924 or 26568924</li>
                    <li><strong>10th and 11th Floor :</strong> Dial 2675 or 2656 followed by bed no. e.g. bed no. 1104; Dial 26751104 or 26561104)</li>
                </ul>
                <p><strong class="green_text uppercase">Bank</strong></p>
                <p>We have an extension counter of the Union Bank within our premises. This makes it more convenient for you to make financial transactions.</p>
                <p><strong class="green_text uppercase">ATM</strong></p>
                <p>For your convenience, we have an ATM located at the ground floor near Gate No.2.</p>
                <p><strong class="green_text uppercase">Dining Area</strong></p>
                <p>For visitors who carry home cooked food, we have a dining area besides the hospital’s pharmacy shop.</p>
                <p><strong class="green_text uppercase">Parking</strong></p>
                <p>A Pay &amp; Park facility run by MCGM is available close to the hospital.</p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Visitors ID="menu" runat="server" />
            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

