﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="AddSpeciality.aspx.cs" Inherits="Admin_AddSpeciality" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        ADD SPECIALITY</div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Speciality<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtspeciality" CssClass="textbox" runat="server" Width="373px" />
        <br />
        <label class="control-label">
            Group<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlGroup" runat="server" Width="373px" CssClass="dropdown">
            <asp:ListItem Value="0">---- Select Group ----</asp:ListItem>
            <asp:ListItem Value="1">Medical</asp:ListItem>
            <asp:ListItem Value="2">Surgical</asp:ListItem>
            <asp:ListItem Value="3">Diagnostics &amp; Clinics</asp:ListItem>
        </asp:DropDownList>
        <br />
        <label class="control-label">
            Details <span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtDetails" runat="server" />
        <br />
        <label class="control-label">
            Facilities <span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtFacilities" runat="server" />
        <br />
        <div style="display: none;">
            <label class="control-label">
                Services <span class="required">*</span> :
            </label>
            <CKEditor:CKEditorControl ID="txtservices" runat="server" />
            <br />
        </div>
        <br />
        <asp:Button ID="btnSave" Text="Add Speciality" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" Text="Cancel" CssClass="button" runat="server" 
            Visible="true" onclick="btncancel_Click" />
         <asp:Button ID="btnbacktolist" Text="Back to list" CssClass="button" 
            runat="server" Visible="false" onclick="btnbacktolist_Click" />
    </div>
</asp:Content>
