﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="FailedAppointment.aspx.cs" Inherits="Admin_Appointment" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .viewdetails {
            color: #48524A;
            text-decoration: none;
        }

            .viewdetails:hover {
                color: #04869A;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Failed Appointment
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="30" AllowPaging="true" OnRowDeleting="gdView_RowDeleting"
            OnPageIndexChanging="gdView_PageIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="AddressIndex" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:BoundField DataField="FaildDate" HeaderText="Faild Date" />
                <asp:BoundField DataField="Name" HeaderText="Patien Name" />
                <asp:BoundField DataField="DoctorName" HeaderText="Doctor Name" />
                <asp:BoundField DataField="SpecialityName" HeaderText="Speciality Name" />
                <asp:BoundField DataField="AvailableTime" HeaderText="Available Timing" />
                <asp:BoundField DataField="DateOfApp" HeaderText="App Date" />
                <asp:BoundField DataField="PreferredTime" HeaderText="Preferred Time" />
                <asp:BoundField DataField="FailedFrom" HeaderText="Failed From" />
                <asp:TemplateField HeaderText="Resent">
                    <ItemTemplate>
                        <asp:Button ID="btnclose" runat="server"
                            CommandName="Delete" Text="Resent" CssClass="button" OnClientClick="if (!confirm('Are you sure do you want to resent appointment?')) return false;" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
