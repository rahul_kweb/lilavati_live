﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_media_press_year : AdminPage
{
    MediaYear tblMediaYear = new MediaYear();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        tblMediaYear.YearName = txtYear.Text.Trim();
        cmsbal.SaveMediaYear(tblMediaYear);
        if (HttpContext.Current.Session["SaveYear"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowWarning("Year Already Exits.");
        }
    }

    public void UpdateRecord()
    {
        tblMediaYear.YearId = int.Parse(hdnId.Value.ToString());
        tblMediaYear.YearName = txtYear.Text.Trim();
        cmsbal.UpdateYear(tblMediaYear);

        if (HttpContext.Current.Session["UpdateYear"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record Update Successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Year Already Exits.");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetYear();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtYear.Text = string.Empty;
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblMediaYear.YearId = id;
        cmsbal.DeleteYear(tblMediaYear);

        if (HttpContext.Current.Session["DeleteYear"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblMediaYear.YearId = id;
        cmsbal.GetByYearID(tblMediaYear);
        hdnId.Value = Convert.ToInt32(tblMediaYear.YearId).ToString();
        txtYear.Text = tblMediaYear.YearName;
        btnSave.Text = "Update";
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtYear.Text.Trim() == "" && txtYear.Text.Equals(""))
        {
            IsOk = false;
            msg += "Year";
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details <br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtYear.Text.Trim() == "" && txtYear.Text.Equals(""))
        {
            IsOk = false;
            msg += "Year";
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details <br>" + msg);
        }
        return IsOk;
    }
}