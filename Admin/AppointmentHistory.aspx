﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="AppointmentHistory.aspx.cs" Inherits="Admin_AppointmentHistory" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <link href="../css/AppointmentDropdowncss.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <%--  <script src="../js/dropdowndashboard.min.js" type="text/javascript"></script>
    --%>
    <%--  <script src="../js/1.8.3_min.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(function () {
            $(".fromdate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: "dd-mm-yy"
            });
            $(".todate").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: "dd-mm-yy"
            });
            //          $("#txtfromdate").change(function () {
            //              var data = $("#txtfromdate").val();
            //              if (data.length != 0 || data != "") {

            //                  $("#Dob").removeClass("error");
            //                  $("#Dob").addClass("populated");
            //              }
            //              else {
            //                  $("#Dob").addClass("error");
            //              }


            //          });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Appointment
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 90%  !important;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <h1 style="margin-left: 39%; font-weight: bold; font-size: 17px;">
            Search Appointment History</h1>
        <hr />
        <asp:TextBox ID="txtfromdate" CssClass="textbox fromdate" runat="server" placeholder="Select From Date"
            autocomplete="off" />
        <asp:TextBox ID="txttodate" CssClass="textbox todate" runat="server" placeholder="Select To  Date"
            Style="margin-left: 146px; position: static;" autocomplete="off" />
        <asp:Button ID="Button1" runat="server" Text="Search" CssClass="button" Style="padding: 4px 14px !important;" OnClick="Button1_Click" />
        <br />
        <br />
        <asp:DropDownList ID="ddldoctorlist" runat="server" CssClass="dropdown js-example-basic-single" Width="262px"
            Font-Bold="true">
            <asp:ListItem Value="0">Select Doctor</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="Button3" runat="server" Text="Search" CssClass="button" Style="padding: 4px 14px !important;" OnClick="Button3_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="ddlSpecialty" runat="server" CssClass="dropdown js-example-basic-single"
            style="margin-left: 65px !important; position: static;" Font-Bold="true">
            <asp:ListItem Value="0">Select Specialty</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="Button2" runat="server" Text="Search" CssClass="button" Style="padding: 4px 14px !important;" OnClick="Button2_Click" />
        <br />
        <br />
        <asp:Button ID="btnexporttoexcel" runat="server" Text="Export to Excel" CssClass="button"
            Style="background: darkcyan; color: azure;" OnClick="btnExport_Click" />
        <asp:Button ID="Button4" runat="server" Text="Export to CSV" CssClass="button" Style="background: darkcyan;
            color: azure;" OnClick="btnExportCsv_Click" />
        <asp:Button ID="btnrefresh" runat="server" Text="Refresh" CssClass="button" Style="background: darkcyan;
            color: azure;" OnClick="btnrefresh_Click" />
    </div>
    <br />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="10" AllowPaging="true" OnRowDeleting="gdView_RowDeleting" OnPageIndexChanging="gdView_PageIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="App_Id_bint" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Doctor_Name_vcr" HeaderText="Doctor Name" />
                <asp:BoundField DataField="Speciality_vcr" HeaderText="Speciality" />
                <asp:BoundField DataField="App_Date" HeaderText="Appt Date" />
                <asp:BoundField DataField="App_Avai_Time" HeaderText="Available Timing" />
                <asp:BoundField DataField="App_Visit_Time" HeaderText="Appt Visit Time" />
                <asp:BoundField DataField="PATIENTNAME" HeaderText="Patient Name" />
                <asp:TemplateField HeaderText="Patient Email">
                    <ItemTemplate>
                        <a href='mailto:<%#Eval("Email") %>'>
                            <%#Eval("Email") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MOBILENO" HeaderText="Patient ContactNo." />
                <%--<asp:BoundField DataField="App_Status" HeaderText="App Staus"/>--%>
                <%--<asp:TemplateField HeaderText="App Staus">
                    <ItemTemplate>
                        <a href="#" style='<%#Eval("App_Status").ToString()=="Cancelled"?"color:#A25020": ""%>;
                            <%#Eval("App_Status").ToString()=="Closed"?"color:#F44336": ""%>; <%#Eval("App_Status").ToString()=="Confirmed"?"color:#1FBB25": ""%>;
                            text-decoration: none'>
                            <%#Eval("App_Status") %></a>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <%--<asp:TemplateField HeaderText="View Details">
                    <ItemTemplate>
                        <a href='AppointmentDetails.aspx?<%#Eval("App_Id_bint","AppId={0}") %>' class="viewdetails">
                            View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <%--<asp:TemplateField HeaderText="App Closed">
                    <ItemTemplate>
                        <asp:Button ID="btnclose" runat="server" Enabled='<%#Eval("App_Status").ToString()=="Cancelled"|| Eval("App_Status").ToString()=="Closed"?false:true %>'
                            CommandName="Delete" Style='<%#Eval("App_Status").ToString()=="Cancelled" || Eval("App_Status").ToString()=="Closed"?"color:#ccc": "" %>'
                            Text="Closed" CssClass="button" OnClientClick="if (!confirm('Are you sure do you want to closed appointment?')) return false;" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
        <br />
        <asp:GridView ID="Exportgrid" runat="server" AutoGenerateColumns="false" Visible="false">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="SRNO" HeaderText="Sr No." />
                <asp:BoundField DataField="Doctor_Name_vcr" HeaderText="Doctor Name" />
                <asp:BoundField DataField="Speciality_vcr" HeaderText="Speciality" />
                <asp:BoundField DataField="App_Date" HeaderText="Appt Date" />
                <asp:BoundField DataField="App_Avai_Time" HeaderText="Available Timing" />
                <asp:BoundField DataField="App_Visit_Time" HeaderText="Appt Visit Time" />
                <asp:BoundField DataField="PATIENTNAME" HeaderText="Patient Name" />
                <asp:BoundField DataField="Email" HeaderText="Patient Email" />
                <asp:BoundField DataField="MOBILENO" HeaderText="Patient ContactNo." />
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>

    <script src="../select2/select2.min.js"></script>
    <link href="../select2/select2.min.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {
            $(".js-example-basic-single").select2();
           
        });
    </script>
    <style>
        .select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    padding-top: 0px;
    vertical-align: middle;
    border: 1px solid #ccc;
    height:30px;
    /*width: 300px;
    margin-left: 65px;*/
}
        .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 30px;
    user-select: none;
    -webkit-user-select: none;
}
.select2-container .select2-selection--single .select2-selection__rendered {
    display: block;
    padding-left: 3px;
    padding-right: 20px;
    padding-top: 0px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 15px;
    position: absolute;
    top: 1px;
    right: 1px;
    width: 20px;
}
    </style>
</asp:Content>
