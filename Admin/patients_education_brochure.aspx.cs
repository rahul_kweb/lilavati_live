﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_packages_details : System.Web.UI.Page
{
    PatientsEducationBrochure tblPatientsEducationBrochure = new PatientsEducationBrochure();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();

    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualImage = "~/uploads/brochure/";
    string TabName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //string Id = Request.QueryString["Id"].ToString();
        if (!IsPostBack)
        {
            BindGrid();
            CompTabName();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            UpdateRecord();
        }
    }

    public void SaveRecord()
    {
        if (FileUploadBrochure.HasFile)
        {
            ext = System.IO.Path.GetExtension(FileUploadBrochure.FileName);
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only (.jpg , .jpeg , .png , .bmp , .gif) Image files.");
                return;
            }

            CompTabName();

            StringBuilder sb = new StringBuilder(TabName);
            string dsc = sb.Replace("&", "")
                           .Replace(" ", "").ToString();

            TabName = dsc;

            MainImage = utility.GetUniqueName(VirtualImage, TabName + "Img", ext, this, false);
            FileUploadBrochure.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblPatientsEducationBrochure.Brochure = MainImage + ext;
        }

        tblPatientsEducationBrochure.Pat_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.SavePatientsEducationBrochure(tblPatientsEducationBrochure);

        if (HttpContext.Current.Session["SavePatientsEducationBrochure"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not insert successfully");
        }
    }

    public void UpdateRecord()
    {
        if (FileUploadBrochure.HasFile)
        {
            ext = System.IO.Path.GetExtension(FileUploadBrochure.FileName);
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only (.jpg , .jpeg , .png , .bmp , .gif) Image files.");
                return;
            }

            CompTabName();
            StringBuilder sb = new StringBuilder(TabName);
            string dsc = sb.Replace("&", "")
                           .Replace(" ", "").ToString();

            TabName = dsc;

            MainImage = utility.GetUniqueName(VirtualImage, TabName + "Img", ext, this, false);
            FileUploadBrochure.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblPatientsEducationBrochure.Brochure = MainImage + ext;
        }

        tblPatientsEducationBrochure.Broc_Id = Convert.ToInt32(hdnId.Value);
        cmsbal.UpdatePatientsEducationBrochure(tblPatientsEducationBrochure);

        if (HttpContext.Current.Session["UpdatePatientsEducationBrochure"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        tblPatientsEducationBrochure.Pat_Id = int.Parse(Request.QueryString["Id"].ToString());
        dt = cmsbal.GetPatientsEducationBrochure(tblPatientsEducationBrochure);
               
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
    }

    public void Reset()
    {        
        imgLogo.Visible = false;  

        btnSave.Text = "Save";
    }
      
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblPatientsEducationBrochure.Broc_Id = id;
        cmsbal.DeletePatientsEducationBrochure(tblPatientsEducationBrochure);

        if (HttpContext.Current.Session["DeletePatientsEducationBrochure"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblPatientsEducationBrochure.Broc_Id = id;
        cmsbal.GetBYPatientsEducationBrochure(tblPatientsEducationBrochure);

        hdnId.Value = Convert.ToInt32(tblPatientsEducationBrochure.Broc_Id).ToString();      

        imgLogo.Visible = true;
        imgLogo.ImageUrl = string.Format("../uploads/brochure/{0}", tblPatientsEducationBrochure.Brochure).ToString(); 

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (!FileUploadBrochure.HasFile)
        {
            IsOk = false;
            msg += "Brochure ,";
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }    

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("patients_education.aspx");
    }

    public void CompTabName()
    {
        tblPatientsEducationBrochure.Pat_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.GetTittlePatientsEducationBrochure(tblPatientsEducationBrochure);

        TabName = tblPatientsEducationBrochure.Tittle;
        lblTittle.Text = tblPatientsEducationBrochure.Tittle;
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}