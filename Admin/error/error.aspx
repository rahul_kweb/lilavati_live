﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="admin_error_error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1 style="text-align: center;">Something went wrong !!!</h1>
        <h3 style="text-align: center;">Go to 
            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/admin/index.aspx" Text="home" runat="server" />
        </h3>
    </div>
    </form>
</body>
</html>
