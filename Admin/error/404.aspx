﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="admin_error_404" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1 style="text-align: center;">Page not found !!!</h1>
        <h3 style="text-align: center;">Go to 
            <asp:HyperLink NavigateUrl="~/admin/index.aspx" Text="home" runat="server" />
        </h3>
    </div>
    </form>
</body>
</html>
