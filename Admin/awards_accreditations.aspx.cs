﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_awards_accreditations : System.Web.UI.Page
{
    Utility utility = new Utility();
    Awards_Accreditations tblAwards = new Awards_Accreditations();
    CMSBAL cmsbal = new CMSBAL();
    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualPart = "~/uploads/awards/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                Save();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                Update();
            }
        }
    }

    public void Save()
    {
        if (fileUploadImg.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImg.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
                return; // STOP FURTHER PROCESSING
            }

            MainImage = utility.GetUniqueName(VirtualPart, "imgAwards", ext, this, false);
            fileUploadImg.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
        }

        tblAwards.AwardsImage = MainImage + ext;
        tblAwards.Tittle = txtTittle.Text.Trim();
        tblAwards.Description = txtDescription.Text.Trim();
        cmsbal.SaveAwardsAccreditations(tblAwards);

        if (HttpContext.Current.Session["AwardsSave"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully");
        }
    }

    public void Update()
    {
        if (fileUploadImg.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImg.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualPart, "imgAwards", ext, this, false);
            fileUploadImg.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
        }

        tblAwards.ID = int.Parse(hdnId.Value);
        tblAwards.AwardsImage = MainImage + ext;
        tblAwards.Tittle = txtTittle.Text.Trim();
        tblAwards.Description = txtDescription.Text.Trim();
        cmsbal.UpdateAwards(tblAwards);

        if (HttpContext.Current.Session["UpdateAwards"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetAwards();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else 
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtTittle.Text = string.Empty;
        txtDescription.Text = string.Empty;
        imgAwards.Visible = false;
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblAwards.ID = id;
        cmsbal.DeletedAwards(tblAwards);
        if (HttpContext.Current.Session["AwardsDelete"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete Successfully");
        }
        else 
        {
            MyMessageBox1.ShowSuccess("Record Could Not delete Successfully");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblAwards.ID=id;
        cmsbal.GetByIdAwards(tblAwards);
        hdnId.Value = Convert.ToInt32(tblAwards.ID).ToString();
        txtTittle.Text = tblAwards.Tittle;
        txtDescription.Text = tblAwards.Description;
        imgAwards.Visible = true;
        imgAwards.Src = string.Format("../uploads/awards/{0}", tblAwards.AwardsImage).ToString();
        btnSave.Text = "Update";
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (!fileUploadImg.HasFile)
        {
            msg = "Awards Image ,";
            IsOk = false;
        }
       
        if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        {
            msg += " Tittle ,";
            IsOk = false;
        }
        //if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        //{
        //    msg += " Description ";
        //    IsOk = false;
        //}
        if (msg != "")
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Please Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;              

        if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        {
            msg += " Tittle ,";
            IsOk = false;
        }
        //if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        //{
        //    msg += " Description ";
        //    IsOk = false;
        //}
        if (msg != "")
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Please Fill Following Details<br>" + msg);
        }
        return IsOk;
    }
}