﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Text;

public partial class Admin_AppointmentHistory : AdminPage
{
    RegistrationTable tbl = new RegistrationTable();
    AppointmentTable apptbl = new AppointmentTable();
    BAL bal = new BAL();
    AdminAppointmentDetails obj = new AdminAppointmentDetails();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            //BindAppointmentGrid();
            FillDropdown();
        }
    }

    public void BindAppointmentGrid()
    {
        gdView.Columns[0].Visible = true;
        gdView.DataSource = bal.AdminGetAppointment();
        gdView.DataBind();
        gdView.Columns[0].Visible = false;
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        apptbl.App_Id_bint = int.Parse(id.ToString());
        bal.ClosedAppointment(apptbl);

        if (HttpContext.Current.Session["ClosesMsg"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Appointment Closed Successfully");
            BindAppointmentGrid();
        }
        else
        {
            MyMessageBox1.ShowError("Appointment Could Not Closed Successfully");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        if (Session["button1_click1"] != null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] == null)
        {
            ButtonOne();
        }
        else if (Session["button1_click1"] == null && Session["button1_click2"] != null && Session["button2_click"] == null && Session["button3_click"] == null)
        {
            ButtonOne();
        }
        else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] != null && Session["button3_click"] == null)
        {
            ButtonTwo();
        }
        else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] != null)
        {
            ButtonThree();
        }
        else
        { }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ButtonOne();
    }
    public void ButtonOne()
    {
        try
        {
            DataTable dt = new DataTable();

            if (txtfromdate.Text != "" && txttodate.Text == "")
            {

                obj._fromdate = txtfromdate.Text.ToString();
                dt = bal.GetAppointmentHistoryByFromdate(obj);
                if (dt.Rows.Count > 0)
                {
                    gdView.Columns[0].Visible = true;
                    gdView.DataSource = dt;
                    gdView.DataBind();
                    gdView.Columns[0].Visible = false;
                }
                else
                {
                    // gdView.Columns[0].Visible = true;
                    gdView.DataSource = null;
                    gdView.DataBind();
                    // gdView.Columns[0].Visible = false;
                }
                Session["button1_click1"] = "fromdate";

                Session.Remove("button1_click2");
                Session.Remove("button2_click");
                Session.Remove("button3_click");

            }
            if (txtfromdate.Text != "" && txttodate.Text != "")
            {
                obj._fromdate = txtfromdate.Text.ToString();
                obj._todate = txttodate.Text.ToString();

                dt = bal.GetAppointmentHistoryByFromdate(obj);
                if (dt.Rows.Count > 0)
                {
                    gdView.Columns[0].Visible = true;
                    gdView.DataSource = dt;
                    gdView.DataBind();
                    gdView.Columns[0].Visible = false;
                }
                else
                {
                    // gdView.Columns[0].Visible = true;
                    gdView.DataSource = null;
                    gdView.DataBind();
                    // gdView.Columns[0].Visible = false;
                }
                Session["button1_click2"] = "todate";


                Session.Remove("button1_click1");
                Session.Remove("button2_click");
                Session.Remove("button3_click");
            }
            else
            { }
        }
        catch (Exception ex)
        {

            this.Title = ex.Message;
            //throw;
        }
    }
  
     protected void Button2_Click(object sender, EventArgs e)
    {
        ButtonTwo();
    }
    public void ButtonTwo()
    {
        try
        {
            DataTable dt = new DataTable();

            if (ddlSpecialty.SelectedValue != "0")
            {

                obj.Doctor_Id_bint = 0;
                obj.Speciality_Id_bint = int.Parse(ddlSpecialty.SelectedValue);

                dt = bal.GetAppointmentHistoryByDoctor(obj);
                if (dt.Rows.Count > 0)
                {
                    gdView.Columns[0].Visible = true;
                    gdView.DataSource = dt;
                    gdView.DataBind();
                    gdView.Columns[0].Visible = false;
                }
                else
                {
                    // gdView.Columns[0].Visible = true;
                    gdView.DataSource = null;
                    gdView.DataBind();
                    // gdView.Columns[0].Visible = false;
                }
                Session["button2_click"] = "specialty";

                Session.Remove("button1_click1");
                Session.Remove("button1_click2");
                Session.Remove("button3_click");

            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        ButtonThree(); 
    }
    public void ButtonThree()
    {
        try
        {
            DataTable dt = new DataTable();
            if (ddldoctorlist.SelectedValue != "0")
            {

                obj.Speciality_Id_bint = 0;
                obj.Doctor_Id_bint = int.Parse(ddldoctorlist.SelectedValue);

                dt = bal.GetAppointmentHistoryByDoctor(obj);
                if (dt.Rows.Count > 0)
                {
                    gdView.Columns[0].Visible = true;
                    gdView.DataSource = dt;
                    gdView.DataBind();
                    gdView.Columns[0].Visible = false;
                }
                else
                {
                    // gdView.Columns[0].Visible = true;
                    gdView.DataSource = null;
                    gdView.DataBind();
                    // gdView.Columns[0].Visible = false;
                }
                Session["button3_click"] = "doctor";

                Session.Remove("button1_click1");
                Session.Remove("button1_click2");
                Session.Remove("button2_click");
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
           // throw;
        }
    }

    public void FillDropdown()
    {
        bal.FillDoctorListOnPageLoad(ddldoctorlist, "Doctor_Id_bint", "Doctor_Name_vcr");

        bal.FillSpecialtyListOnPageLoad(ddlSpecialty, "Speciality_Id_bint", "Speciality_vcr");
    }

    protected void btnrefresh_Click(object sender, EventArgs e)
    {
        Server.Transfer("AppointmentHistory.aspx");
        //Server.Transfer("Change_Password.aspx");
    }
    
    //protected void ExportToExcel(object sender, EventArgs e)
    //{
    //    Response.Clear();
    //    Response.Buffer = true;
    //    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
    //    Response.Charset = "";
    //    Response.ContentType = "application/vnd.ms-excel";
    //    using (StringWriter sw = new StringWriter())
    //    {
    //        HtmlTextWriter hw = new HtmlTextWriter(sw);

    //        //To Export all pages
    //        gdView.AllowPaging = false;
           
    //        if (Session["button1_click1"] != null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] == null)
    //        {
    //            ButtonOne();
    //        }
    //        else if (Session["button1_click1"] == null && Session["button1_click2"] != null && Session["button2_click"] == null && Session["button3_click"] == null)
    //        {
    //            ButtonOne();
    //        }
    //        else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] != null && Session["button3_click"] == null)
    //        {
    //            ButtonTwo();
    //        }
    //        else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] != null)
    //        {
    //            ButtonThree();
    //        }
    //        else
    //        { }

    //        gdView.HeaderRow.BackColor = Color.White;
    //        foreach (TableCell cell in gdView.HeaderRow.Cells)
    //        {
    //            cell.BackColor = gdView.HeaderStyle.BackColor;
    //        }
    //        foreach (GridViewRow row in gdView.Rows)
    //        {
    //            row.BackColor = Color.White;
    //            foreach (TableCell cell in row.Cells)
    //            {
    //                if (row.RowIndex % 2 == 0)
    //                {
    //                    cell.BackColor = gdView.AlternatingRowStyle.BackColor;
    //                }
    //                else
    //                {
    //                    cell.BackColor = gdView.RowStyle.BackColor;
    //                }
    //                cell.CssClass = "textmode";
    //            }
    //        }

    //        gdView.RenderControl(hw);

    //        //style to format numbers to string
    //        string style = @"<style> .textmode { } </style>";
    //        Response.Write(style);
    //        Response.Output.Write(sw.ToString());
    //        Response.Flush();
    //        Response.End();
    //    }
    //}

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    private void ExportToExcel(string strFileName, DataGrid dg)
    {
        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        dg.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {

        try
        {
            DataTable dt = new DataTable();

            if (Session["button1_click1"] != null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] == null)
            {
                obj._fromdate = txtfromdate.Text.ToString();
                dt = bal.GetAppointmentHistoryByFromdate(obj);
            }
            else if (Session["button1_click1"] == null && Session["button1_click2"] != null && Session["button2_click"] == null && Session["button3_click"] == null)
            {
                obj._fromdate = txtfromdate.Text.ToString();
                obj._todate = txttodate.Text.ToString();
                dt = bal.GetAppointmentHistoryByFromdate(obj);
            }
            else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] != null && Session["button3_click"] == null)
            {
                obj.Doctor_Id_bint = 0;
                obj.Speciality_Id_bint = int.Parse(ddlSpecialty.SelectedValue);
                dt = bal.GetAppointmentHistoryByDoctor(obj);
            }
            else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] != null)
            {
                obj.Speciality_Id_bint = 0;
                obj.Doctor_Id_bint = int.Parse(ddldoctorlist.SelectedValue);
                dt = bal.GetAppointmentHistoryByDoctor(obj);
            }
            else
            { }
            if (dt.Rows.Count > 0)
            {


                DataTable dtOriginal = new DataTable();
                dtOriginal = dt; //Return Table consisting data

                //Create Tempory Table
                DataTable dtTemp = new DataTable();

                //Creating Header Row
                dtTemp.Columns.Add("<b>Sr.No</b>");
                dtTemp.Columns.Add("<b>Doctor Name</b>");
                dtTemp.Columns.Add("<b>Specialty</b>");
                dtTemp.Columns.Add("<b>Appt Date</b>");
                dtTemp.Columns.Add("<b>Dr. Available Timing</b>");
                dtTemp.Columns.Add("<b>Appt Visit Time</b>");
                dtTemp.Columns.Add("<b>Patient Name</b>");
                dtTemp.Columns.Add("<b>Patient Email</b>");
                dtTemp.Columns.Add("<b>Patient ContactNo.</b>");


                DataRow drAddItem;
                for (int i = 0; i < dtOriginal.Rows.Count; i++)
                {
                    drAddItem = dtTemp.NewRow();

                    drAddItem[0] = dtOriginal.Rows[i][0].ToString();//SrId
                    drAddItem[1] = dtOriginal.Rows[i][3].ToString();//Email
                    drAddItem[2] = dtOriginal.Rows[i][4].ToString();//Office
                    drAddItem[3] = dtOriginal.Rows[i][5].ToString();//Country
                    drAddItem[4] = dtOriginal.Rows[i][6].ToString();//Country
                    drAddItem[5] = dtOriginal.Rows[i][7].ToString();//Country
                    drAddItem[6] = dtOriginal.Rows[i][8].ToString();//Country
                    drAddItem[7] = dtOriginal.Rows[i][9].ToString();//Country
                    drAddItem[8] = dtOriginal.Rows[i][10].ToString();//Country


                    dtTemp.Rows.Add(drAddItem);

                }

                //Temp Grid
                DataGrid dg = new DataGrid();
                dg.DataSource = dtTemp;
                dg.DataBind();
                string Filename = "AppointmentDetails_on" + "_" + DateTime.Now.ToString("dd_MMM_yyyy") + ".xls";
                ExportToExcel(Filename, dg);
                dg = null;
                dg.Dispose();

            }
            else
            {

            }
        }
        catch (Exception error)
        {
            this.Title = error.Message;
        }
    }


   protected void btnExportCsv_Click(object sender, EventArgs e)
   {
       try
       {

           DataTable dt = new DataTable();

           if (Session["button1_click1"] != null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] == null)
           {
               obj._fromdate = txtfromdate.Text.ToString();
               dt = bal.ExportAppointmentHistoryByFromdate(obj);
           }
           else if (Session["button1_click1"] == null && Session["button1_click2"] != null && Session["button2_click"] == null && Session["button3_click"] == null)
           {
               obj._fromdate = txtfromdate.Text.ToString();
               obj._todate = txttodate.Text.ToString();
               dt = bal.ExportAppointmentHistoryByFromdate(obj);
           }
           else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] != null && Session["button3_click"] == null)
           {
               obj.Doctor_Id_bint = 0;
               obj.Speciality_Id_bint = int.Parse(ddlSpecialty.SelectedValue);
               dt = bal.ExportAppointmentHistoryByDoctor(obj);
           }
           else if (Session["button1_click1"] == null && Session["button1_click2"] == null && Session["button2_click"] == null && Session["button3_click"] != null)
           {
               obj.Speciality_Id_bint = 0;
               obj.Doctor_Id_bint = int.Parse(ddldoctorlist.SelectedValue);
               dt = bal.ExportAppointmentHistoryByDoctor(obj);
           }
           else
           { }

           Response.Clear();

           if (dt.Rows.Count > 0)
           {

               string Filename = "AppointmentDetails_on" + "_" + DateTime.Now.ToString("dd_MMM_yyyy") + ".csv";
               Response.Buffer = true;
               Response.AddHeader("content-disposition",
                "attachment;filename='" + Filename + "'");
               Response.Charset = "";
               Response.ContentType = "application/text";

               Exportgrid.AllowPaging = false;
               Exportgrid.DataSource = dt;
               Exportgrid.DataBind();

               StringBuilder sb = new StringBuilder();
               for (int k = 0; k < Exportgrid.Columns.Count; k++)
               {
                   //add separator
                   sb.Append(Exportgrid.Columns[k].HeaderText + ',');
               }
               //append new line
               sb.Append("\r\n");
               for (int i = 0; i < Exportgrid.Rows.Count; i++)
               {
                   for (int k = 0; k < Exportgrid.Columns.Count; k++)
                   {
                       //add separator
                       sb.Append(Exportgrid.Rows[i].Cells[k].Text + ',');
                   }
                   //append new line
                   sb.Append("\r\n");
               }
               Response.Output.Write(sb.ToString());
               Response.Flush();
               Response.End();
           }
           else
           {

           }
       }
       catch (Exception error)
       {
           this.Title = error.Message;
       }

   }


}