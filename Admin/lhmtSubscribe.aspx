﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="lhmtSubscribe.aspx.cs" Inherits="Admin_Registration" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Subscribe to LHMT
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <asp:Button ID="btnexporttoexcel" runat="server" Text="Export to Excel" CssClass="button"
        Style="background: darkcyan; color: azure;margin-left:1000px;" OnClick="btnexporttoexcel_Click"/>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="15" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Name" HeaderText="Name" />
               <%-- <asp:BoundField DataField="Email" HeaderText="Email" />--%>
                <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                        <a href="mailto:<%#Eval("Email") %>"><%#Eval("Email") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Contact" HeaderText="Contact No" />
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('Are you sure want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>

