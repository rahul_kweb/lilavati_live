﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="styles/reset.css" rel="stylesheet" type="text/css" />
    <link href="styles/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="scripts/jquery-1.8.3.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="border: solid 1px #1b1a1c; width: 400px; margin: 50px auto auto auto;
            background: #fff;">
            <img src="../images/logo.gif" />
            <%--<img src="../images/indigo_logo.jpg"  alt="Indigo"  style="padding-left: 10px;"/>--%>
        </div>
        <div style="border: solid 1px #1b1a1c; width: 400px; margin: 0px auto;">
            <table style="width: 100%; margin: 10px;">
                <tr>
                    <td>
                        <label class="control-label">
                            Username :
                        </label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAdminUserName" CssClass="textbox" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAdminUserName"
                            runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label">
                            Password :
                        </label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAdminPassword" CssClass="textbox" TextMode="Password" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtAdminPassword"
                            runat="server" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="control-label">
                            Login Type :
                        </label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddllogintype" runat="server" CssClass="dropdown" Width="263px">
                            <asp:ListItem Value="0">Select Login Type</asp:ListItem>
                            <asp:ListItem Value="1">Super Admin</asp:ListItem>
                            <asp:ListItem Value="2">Appointment Check</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="txtAdmintype" ControlToValidate="ddllogintype" runat="server"
                            InitialValue="0" ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnAdminLogin" Text="Login" CssClass="button" runat="server" OnClick="btnAdminLogin_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblStatus" Visible="false" ForeColor="Red" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
