﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="Registration.aspx.cs" Inherits="Admin_Registration" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Registered User
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="100" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="AddressIndex" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="LastName" HeaderText="Sure Name" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="MiddleName" HeaderText="Middle Name" />
                <asp:BoundField DataField="Email" HeaderText="Email ID" />
                <asp:BoundField DataField="MOBILENO" HeaderText="Mobile" />
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <p style="color: green; display: <%#Convert.ToInt16(Eval("USERACTIVE"))==1?"block":"none"%>;">Verified</p>
                        <p style="color: red;display: <%#Convert.ToInt16(Eval("USERACTIVE"))==1?"none":"block"%>">Not Verified</p>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View Detail">
                    <ItemTemplate>
                        <a href='RegistrationDetails.aspx?<%#Eval("ADDRESSINDEX","Id={0}") %>' style="text-decoration: none; color: black">View Detail</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Update LHNO">
                    <ItemTemplate>
                        <a href='RegistrationDetails.aspx?<%#Eval("ADDRESSINDEX","LHNO={0}") %>'>Update LHNO</a>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <%--<asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                            Height="41px" margin-top="11px" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('Are you sure want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
