﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string AppId = Request.QueryString["AppId"].ToString();
            AppViewDetails(AppId);
        }
    }

    public void AppViewDetails(string AppId)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute AddUpdateGetAppointmentMaster 'GET_BY_ID','" + AppId + "'");

        if (dt.Rows.Count > 0)
        {
            lblSpeciality.Text = dt.Rows[0]["Speciality_vcr"].ToString();
            lblDoctorName.Text = dt.Rows[0]["Doctor_Name_vcr"].ToString();
            lblAvailableTiming.Text = dt.Rows[0]["App_Time_vcr"].ToString();
            lblAppointmentDate.Text = dt.Rows[0]["App_date_dtm"].ToString();

            lblwelcome.Text = dt.Rows[0]["Patient_Name_vcr"].ToString();
            lblPatientName.Text = dt.Rows[0]["Patient_Name_vcr"].ToString();
            lblEmailID.Text = dt.Rows[0]["Email_Id_vcr"].ToString();
            lblMobileNo.Text = dt.Rows[0]["Mobile_No_vcr"].ToString();
            lblAddress.Text = dt.Rows[0]["Address_vcr"].ToString();
            lblCountry.Text = dt.Rows[0]["Country_vcr"].ToString();
            lblState.Text = dt.Rows[0]["State_vcr"].ToString();
            lblCity.Text = dt.Rows[0]["City_vcr"].ToString();
            lblPincode.Text = dt.Rows[0]["Postal_vcr"].ToString();
            lblVisit.Text = dt.Rows[0]["Visit_vcr"].ToString();
        }
    }


}