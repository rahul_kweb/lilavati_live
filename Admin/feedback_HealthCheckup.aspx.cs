﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Registration : AdminPage
{
    FeedBackHealthCheckUp tblFeedBackHealthCheckUp = new FeedBackHealthCheckUp();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetFeedBackHealthCheckUp();

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblFeedBackHealthCheckUp.ID = int.Parse(Id.ToString());
        cmsbal.DeleteFeedBackHealthCheckUp(tblFeedBackHealthCheckUp);

        if (HttpContext.Current.Session["DeleteFeedBackHealthCheckUp"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Record deleted Successfully.");
            BindGrid();
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not delete Successfully.");
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}