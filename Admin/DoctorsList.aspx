﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="DoctorsList.aspx.cs" Inherits="DoctorsList" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .viewdetails {
            color: #48524A;
            text-decoration: none;
        }
        .viewdetails:hover {
            color: #04869A;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        DOCTOR LIST
    </div>
     <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box">
        <asp:TextBox ID="txtsearch" CssClass="textbox" runat="server" Width="250px" Placeholder="Search.." />
        <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" CssClass="button" />
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            DataKeyNames="Doctor_Id_bint" OnRowDeleting="gdView_RowDeleting"
            OnPageIndexChanging="gdView_PageIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Doctor_Id_bint" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Image runat="server" Height="150px" Width="150px" ID="ImgDoc" ImageUrl='<%# string.Format("~/Admin/Doctors/{0}",Eval("Photo_vcr")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Doctor_Name_vcr" HeaderText="Doctor" />
                <asp:BoundField DataField="Qualification_vcr" HeaderText="Qualification" />
                <%--<asp:TemplateField HeaderText="Profile">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblProfile" Text='<%#Eval("ProfileCnt") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Area Of Interest">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbaoi" Text='<%#Eval("Areaofinterest_vcr") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Awards and Recognition">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lb" Text='<%#Eval("Achievements_vcr") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <%--<asp:TemplateField HeaderText="Research Publications">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblresearch" Text='<%#Eval("RESEARCHPUBLICATION") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:BoundField DataField="Specialities" HeaderText="Specialities" />

             <%--            <asp:TemplateField HeaderText="DirectAvailable">
                    <ItemTemplate>
                        <asp:Label runat="server"  Text='<%#Eval("DirectAvailable") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Contact No">
                    <ItemTemplate>
                        <asp:Label runat="server"  Text='<%#Eval("ContactNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Time From">
                    <ItemTemplate>
                        <asp:Label runat="server"  Text='<%#Eval("DirectTimeFrom") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                  <asp:TemplateField HeaderText="Time To">
                    <ItemTemplate>
                        <asp:Label runat="server"  Text='<%#Eval("DirectTimeTo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                    <asp:TemplateField HeaderText="Days From">
                    <ItemTemplate>
                        <asp:Label runat="server"  Text='<%#Eval("DirectDaysFrom") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField HeaderText="Days To">
                    <ItemTemplate>
                        <asp:Label runat="server"  Text='<%#Eval("DirectDaysTo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click" />
                        <asp:HiddenField ID="hdnfldid" runat="server" Value='<%#Eval("Doctor_Id_bint") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btndelete" runat="server" Text="Delete" CommandName="Delete" OnClientClick="javascript:return confirm('Are you sure want to delete this record ?');" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
