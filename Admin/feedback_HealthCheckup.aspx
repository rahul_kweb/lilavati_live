﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="feedback_HealthCheckup.aspx.cs" Inherits="Admin_Registration" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Feedback Health CheckUp
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="15" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="LHNO" HeaderText="LH No" />
                <asp:BoundField DataField="Date" HeaderText="Date" />
                <asp:BoundField DataField="ContactNo" HeaderText="Contact No" />
                <asp:TemplateField HeaderText="Email ID">
                    <ItemTemplate>
                        <a href='mailto:<%#Eval("EmailID") %>'><%#Eval("EmailID") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View Detail">
                    <ItemTemplate>
                        <a href='feedback_HealthCheckupDetail.aspx?<%#Eval("Id","Id={0}") %>' style="color:black;">View Detail</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('Are you sure want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>

