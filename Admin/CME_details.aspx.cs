﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CME_details : AdminPage
{
    CMSBAL cmdbal = new CMSBAL();
    CMEDetails tblCMEDetails = new CMEDetails();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Heading();
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveData();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateData();
            }
        }
    }

    public void SaveData()
    {
        tblCMEDetails.Cme_Id = int.Parse(Request.QueryString["Id"].ToString());       
        string date = DateTime.ParseExact(txtDateAndMonth.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        tblCMEDetails.DateAndMonth = date;
        tblCMEDetails.Topic = txtTopic.Text.Trim();
        tblCMEDetails.Department = txtDepartment.Text.Trim();

        cmdbal.SaveCMEDetails(tblCMEDetails);
        if (HttpContext.Current.Session["SaveCMEDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Nt insert successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        tblCMEDetails.Cme_Id = int.Parse(Request.QueryString["Id"].ToString());       
        dt = cmdbal.GetCMEDetails(tblCMEDetails);

        DataTable dt1 = new DataTable();
        tblCMEDetails.Cme_Id = int.Parse(Request.QueryString["Id"].ToString());
        dt1 = cmdbal.HeadingCMEDetails(tblCMEDetails);              
                
        if (dt.Rows.Count > 0)
        {
            string finaldate = string.Empty;
            foreach (DataRow row in dt.Rows)
            {
                row[1] = dt1.Rows[0]["EVENTNAME"].ToString();
                string date = row[4].ToString();
                string[] words = date.Split(' ');

                string dd = words[0].ToString() + "th";
                string mm = words[1].ToString() + ",";
                string yy = words[2].ToString();

                finaldate = dd + " " + mm + " " + yy;
                                
                row[4] = finaldate;

                gdView.Columns[0].Visible = true;
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
                
            }
            
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtDateAndMonth.Text = string.Empty;
        txtTopic.Text = string.Empty;
        txtDepartment.Text = string.Empty;
        btnSave.Text = "Save";
    }

    public void Heading()
    {
        DataTable dt = new DataTable();
        tblCMEDetails.Cme_Id = int.Parse(Request.QueryString["Id"].ToString());    
        dt = cmdbal.HeadingCMEDetails(tblCMEDetails);

        lblheading.Text = dt.Rows[0]["EVENTNAME"].ToString();
    }

    public void UpdateData()
    {
        tblCMEDetails.Id = int.Parse(hdnId.Value.ToString());
        string date = DateTime.ParseExact(txtDateAndMonth.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        tblCMEDetails.DateAndMonth = date;
        tblCMEDetails.Topic = txtTopic.Text.Trim();
        tblCMEDetails.Department = txtDepartment.Text.Trim();

        cmdbal.UpdateCMEDetails(tblCMEDetails);
        if (HttpContext.Current.Session["UpdateCMEDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Nt update successfully");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblCMEDetails.Id = id;
        cmdbal.GetBYIdCMEDetails(tblCMEDetails);

        hdnId.Value = Convert.ToInt32(tblCMEDetails.Id).ToString();
        txtDateAndMonth.Text = tblCMEDetails.DateAndMonth;        
        txtTopic.Text = tblCMEDetails.Topic;
        txtDepartment.Text = tblCMEDetails.Department;
        btnSave.Text = "Update";
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblCMEDetails.Id = id;
        cmdbal.DeleteCMEDetails(tblCMEDetails);

        if (HttpContext.Current.Session["DeleteCMEDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Nt delete successfully");
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    public bool CheckSave()
    {
        bool Isok = true;
        string msg = string.Empty;

        if (txtDateAndMonth.Text == "" && txtDateAndMonth.Text.Equals(""))
        {
            msg += "Date & Month ,";
            Isok = false;
        }
        if (txtTopic.Text == "" && txtTopic.Text.Equals(""))
        {
            msg += " Topic ,";
            Isok = false;
        }
        if (txtDepartment.Text == "" && txtDepartment.Text.Equals(""))
        {
            msg += " Department ,";
            Isok = false;
        }
        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }
        if (!Isok)
        {
            MyMessageBox1.ShowWarning("Fill Following Details<br>" + msg);
        }
        return Isok;
    }

    public bool CheckUpdate()
    {
        bool Isok = true;
        string msg = string.Empty;

        if (txtDateAndMonth.Text == "" && txtDateAndMonth.Text.Equals(""))
        {
            msg += "Date & Month ,";
            Isok = false;
        }
        if (txtTopic.Text == "" && txtTopic.Text.Equals(""))
        {
            msg += " Topic ,";
            Isok = false;
        }
        if (txtDepartment.Text == "" && txtDepartment.Text.Equals(""))
        {
            msg += " Department ,";
            Isok = false;
        }
        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }
        if (!Isok)
        {
            MyMessageBox1.ShowWarning("Fill Following Details<br>" + msg);
        }
        return Isok;
    }
}