﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="PushNotification.aspx.cs" Inherits="Admin_Appointment" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .viewdetails {
            color: #48524A;
            text-decoration: none;
        }

            .viewdetails:hover {
                color: #04869A;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Health Tips Notification
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="30" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="ADDRESSINDEX" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="checkAll" runat="server" Text="Select All" onclick="checkAll(this);" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" ToolTip='<%#Eval("ADDRESSINDEX") %>' CssClass="chkClass" runat="server" onclick="Check_Click(this)" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="FIRSTNAME" HeaderText="First Name" />
                <asp:BoundField DataField="LASTNAME" HeaderText="Last Name" />
                <asp:BoundField DataField="DEVICEID" HeaderText="Device ID" />
                <asp:BoundField DataField="DEVICETYPE" HeaderText="Device Type" />
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>

    <div class="form-box" style="width: 500px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Message :            
        </label>
        <asp:TextBox ID="txtMessage" runat="server" CssClass="textbox" Width="500" Height="100" TextMode="MultiLine"></asp:TextBox>        
        <br />
        <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="button" OnClick="btnSend_Click" OnClientClick="javacript:return CheckValidation();" />
    </div>
    <script type = "text/javascript">
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {            
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {   
                        inputList[i].checked = true;
                    }
                    else {         
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function CheckValidation() {
            var check = "";
            jQuery(".chkClass > input").each(function (index, el) {
                var chk = $(this).is(":checked");
                if (chk == true) {
                    check += "true";
                }
            });

            var blank = false;
            var message=$('#<%=txtMessage.ClientID%>').val();
            if (check == '') {
                alert('Please select at least one checkbox!');
                blank = true;
            } else {
                blank = false;
            }

            if (message == '') {
                $('#<%=txtMessage.ClientID%>').css('border-color', 'red');
                blank = true;
            } else {
                $('#<%=txtMessage.ClientID%>').css('border-color', '');
                blank = false;
            }

            if (blank) {
                return false;
            } else {                
                return true;
            }


            //console.log(check);
            //alert($(this).is(":checked"));        
        }
        
</script> 
</asp:Content>
