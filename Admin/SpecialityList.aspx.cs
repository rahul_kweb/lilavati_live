﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SpecialityList : AdminPage
{

    BAL bal = new BAL();
    Specialty obj = new Specialty();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSpecialityList();
        }
    }

    public void BindSpecialityList()
    {
        if (txtsearch.Text == "")
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = bal.GetSpecialityList();
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            string key = txtsearch.Text.ToString();
            SearchKey(key);
        }
    }
    //protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
    //    apptbl.App_Id_bint = int.Parse(id.ToString());
    //    bal.ClosedAppointment(apptbl);

    //    if (HttpContext.Current.Session["ClosesMsg"] == "Success")
    //    {
    //        MyMessageBox1.ShowSuccess("Appointment Closed Successfully");
    //        BindAppointmentGrid();
    //    }
    //    else
    //    {
    //        MyMessageBox1.ShowError("Appointment Could Not Closed Successfully");
    //    }
    //}
    protected void btnedit_Click(object sender, EventArgs e)
    {

        //Determine the RowIndex of the Row whose Button was clicked.
        int rowIndex = ((sender as Button).NamingContainer as GridViewRow).RowIndex;

        //Get the value of column from the DataKeys using the RowIndex.
        int id = Convert.ToInt32(gdView.DataKeys[rowIndex].Values[0]);

        Response.Redirect("AddSpeciality.aspx?SpecilityId=" + id);
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
            obj._buttontext = "Delete Speciality";
            obj._Id = id;
            int i = bal.PassSpeciality(obj);
            if (i == 1)
            {
                BindSpecialityList();
                MyMessageBox1.ShowSuccess("Speciality Delete SuccessFully");
            }
            else
            {
                MyMessageBox1.ShowError("Speciality could not Delete");
            }


        }
        catch (Exception ex)
        {

            this.Title = ex.Message;
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindSpecialityList();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        string key = txtsearch.Text.ToString();
        SearchKey(key);
    }
    public void SearchKey(string key)
    {
        obj._seachKey = key;
        try
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = bal.GetSpecialitySearchList(obj);
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        catch (Exception ex)
        {

            //throw;
            this.Title = ex.Message;
        }
    }
}