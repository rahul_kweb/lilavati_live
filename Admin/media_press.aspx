﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="media_press.aspx.cs" Inherits="Admin_media_press" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Media Press
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Year<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlYear" runat="server" CssClass="dropdown"></asp:DropDownList>
        <br />
        <label class="control-label">
            Month<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="dropdown"></asp:DropDownList>
        <br />
        <label class="control-label">
            Publication<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlPublication" runat="server" CssClass="dropdown">
            <asp:ListItem>-- Select --</asp:ListItem>
            <asp:ListItem>English</asp:ListItem>
            <asp:ListItem>Regional</asp:ListItem>
            <asp:ListItem>Gujarati</asp:ListItem>
            <asp:ListItem>Other Publication</asp:ListItem>
            <asp:ListItem>Video Gallery</asp:ListItem>
        </asp:DropDownList>
        <br />
        <label class="control-label">
            Tittle<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox" TextMode="MultiLine" Height="70"></asp:TextBox>
        <div id="divpdf">
            <br />
            <label class="control-label">
                Pdf File<span class="required">*</span> :
            </label>
            <asp:FileUpload ID="fileuploadPdfFile" runat="server" CssClass="file-upload"></asp:FileUpload>
            <asp:HyperLink ID="hyplnk" runat="server" Target="_blank" CssClass="control-label" Visible="false">View File</asp:HyperLink>
        </div>
        <div id="divVideo" style="display:none;">
            <br />
            <label class="control-label">
                Video Url<span class="required">*</span> :
            </label>
            <asp:TextBox ID="txtVideo" runat="server" CssClass="textbox"></asp:TextBox>
        </div>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="MEDIA_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="YEAR_NAME" HeaderText="Year" />
            <asp:BoundField DataField="YEAR_WISENAME" HeaderText="Month" />
            <asp:BoundField DataField="DESCRIPTION" HeaderText="Tittle" />
            <asp:BoundField DataField="Publication" HeaderText="Publication" />
            <asp:TemplateField HeaderText="Pdf File">
                <ItemTemplate>
                    <a href='<%#Eval("Publication").ToString()=="Video Gallery"?Eval("PDF_FILE"):Eval("PDF_FILE","../uploads/media_press/{0}")  %>' target="_blank">View File </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="false">
                <ItemTemplate>
                    <asp:RadioButton ID="rbdActive" runat="server" Text="Show" Checked='<%# Eval("STATUS").ToString().Equals("True") %>'
                        GroupName="radio" OnCheckedChanged="rbdActive_CheckedChanged" AutoPostBack="true" />
                    <asp:RadioButton ID="rbdInActive" runat="server" Text="Hide" Checked='<%# Eval("STATUS").ToString().Equals("False") %>'
                        GroupName="radio" OnCheckedChanged="rbdInActive_CheckedChanged" AutoPostBack="true" />
                    <asp:HiddenField ID="hdid" runat="server" Value='<%#Eval("MEDIA_ID") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>

    <script>
        $(document).ready(function () {
            $('#<%=ddlPublication.ClientID%>').change(function () {
                if ($(this).val() == "Video Gallery") {
                    $('#divVideo').prop('style', 'display:block');
                    $('#divpdf').prop('style', 'display:none');
                } else {
                    $('#divVideo').prop('style', 'display:none');
                    $('#divpdf').prop('style', 'display:block');
                }
                //alert($(this).val());
            })

            var val = $('#<%=ddlPublication.ClientID%>').val();
            if (val == "Video Gallery") {
                $('#divVideo').prop('style', 'display:block');
                $('#divpdf').prop('style', 'display:none');
            } else {
                $('#divVideo').prop('style', 'display:none');
                $('#divpdf').prop('style', 'display:block');
            }
        })
    </script>
</asp:Content>


