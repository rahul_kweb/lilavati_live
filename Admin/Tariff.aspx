﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="Tariff.aspx.cs" Inherits="Admin_Tariff" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Add Tariff</div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnId1" runat="server" />
        <label class="control-label">
            Heading<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtheadind" runat="server" CssClass="textbox" Enabled="false" Style="cursor: not-allowed;
            background-color: #eee;"></asp:TextBox>
        <br />
        <label class="control-label">
            Discription<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtdesc" runat="server" CssClass="textbox" ReadOnly="true" TextMode="MultiLine"
            Style="cursor: not-allowed; background-color: #eee;" Height="50px"></asp:TextBox>
        <br />
        <label class="control-label">
            Choose Type<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddtype" runat="server" CssClass="dropdown">
            <asp:ListItem Value="Select" Text="Select"></asp:ListItem>
            <asp:ListItem Value="Adult" Text="Adult"></asp:ListItem>
            <asp:ListItem Value="Child" Text="Child"></asp:ListItem>
        </asp:DropDownList>
        <br />
        <label class="control-label">
            Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtname" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Room Member<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtroommemb" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Room Tariff Rs. :
        </label>
        <asp:TextBox ID="txtroomtariff" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Meal Charges :
        </label>
        <asp:TextBox ID="txtmealcharges" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            All Taxes :
        </label>
        <asp:TextBox ID="txtalltaxes" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Total :
        </label>
        <asp:TextBox ID="txttotal" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="button" OnClick="btnsave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClick="btnback_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging"
        OnPageIndexChanging="gdView_PageIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="TARIFF_ID" HeaderText="ID" />
            <asp:BoundField DataField="Heading_Id" HeaderText="ID" />
            <asp:BoundField DataField="HEADING" HeaderText="Heading" />
            <%--<asp:BoundField DataField="DISCRIPTION" HeaderText="Discription" />--%>
            <asp:TemplateField HeaderText="Discription">
                <ItemTemplate>
                    <asp:Literal ID="content" runat="server" Text='<%#Eval("DISCRIPTION") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="NAME" HeaderText="Name" />
            <asp:BoundField DataField="TYPE" HeaderText="Type" />
            <asp:BoundField DataField="ROOM" HeaderText="Room Member" />
            <asp:BoundField DataField="ROOM_TARIFF" HeaderText="Room Tariff" />
            <asp:BoundField DataField="MEAL_CHARGES" HeaderText="Meal Charges" />
            <asp:BoundField DataField="ALL_TAXES" HeaderText="All Taxes" />
            <asp:BoundField DataField="TOTAL" HeaderText="Total" />
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.jpeg"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.jpeg"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
    <asp:HiddenField ID="hdnforedit" runat="server" />
</asp:Content>
