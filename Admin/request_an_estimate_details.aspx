﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="request_an_estimate_details.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Request An Estimate Details
    </div>
    <div class="form-box" style="width: 350px;">
        <table class="gridtable">
            <tr>
                <td>Patient Name</td>
                <td>
                    <asp:Label ID="lblPatientName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Speciality</td>
                <td>
                    <asp:Label ID="lblSpeciality" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Doctor Name
                </td>
                <td>
                    <asp:Label ID="lblDoctorName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Type Of Room
                </td>
                <td>
                    <asp:Label ID="lblTypeOfRoom" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Nationality
                </td>
                <td>
                    <asp:Label ID="lblNationality" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>Email ID</td>
                <td>
                    <asp:Label ID="lblEmailID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Mobile No</td>
                <td>
                    <asp:Label ID="lblMobileNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Age</td>
                <td>
                    <asp:Label ID="lblAge" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    <asp:Label ID="lblGender" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Description of Procedure</td>
                <td>
                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Past History (if any)</td>
                <td>
                    <asp:Label ID="lblPastHistory" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

