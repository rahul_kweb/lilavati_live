﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="patients_education.aspx.cs" Inherits="Admin_Job_Area" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Patients Education Brochure
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Tittle<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTittle" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Thumb Image<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="fileUploadThumbImage" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:Image ID="imgPreview" runat="server" Visible="false" Height="300" Width="200"/>
        <br />
        <label class="control-label">
            Brochure<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="fileUploadBrochure" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:TextBox ID="txtBrochurePreview" runat="server" CssClass="textbox" Enabled="false" Visible="false" BackColor="#cccCCC"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="10" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" OnPageIndexChanging="gdView_PageIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="PAT_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TITTLE" HeaderText="Tittle" />
             <asp:TemplateField HeaderText="Thumb Image">
                <ItemTemplate>
                    <img src='<%#Eval("ThumbImage","../uploads/brochure/{0}") %>' height="300" width="200" />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Brochure">
                <ItemTemplate>
                    <a href='<%#Eval("Brochure","../uploads/brochure/{0}") %>' target="_blank">View Brochure</a>
                </ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderText="Add Brochure">
                <ItemTemplate>
                    <a href='patients_education_brochure.aspx?Id=<%#Eval("PAT_ID") %>'>Add Brochure</a>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

