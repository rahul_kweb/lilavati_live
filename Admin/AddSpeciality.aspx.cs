﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Admin_AddSpeciality : AdminPage
{
    BAL bal = new BAL();
    Specialty obj = new Specialty();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SpecilityId"] != null)
            {
                string id=Request.QueryString["SpecilityId"].ToString();

                
               GetRecordEditTime(id);

                txtspeciality.Text = obj._Specialityname;
                txtDetails.Text = obj._Details;
                txtFacilities.Text = obj._Facilities;
                txtservices.Text = obj._Services;
                ddlGroup.SelectedItem.Text = obj._Group;
                btnSave.Text = "Update Speciality";
                hdnId.Value = id;
                btnbacktolist.Visible = true;
                btncancel.Visible = false;
                
            }
            else
            {

            }
        }
        
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (CheckFieldsSaveTime())
        {
            if (btnSave.Text == "Add Speciality")
            {
                obj._Specialityname = txtspeciality.Text.ToString();
                obj._Details = txtDetails.Text.ToString();
                obj._Facilities = txtFacilities.Text.ToString();
                obj._Services = txtservices.Text.ToString();
                obj._buttontext = "Add Speciality";
                if (ddlGroup.SelectedValue=="0")
                {
                    obj._Group = "No Group";
                }
                else
                {
                    obj._Group = ddlGroup.SelectedItem.Text.ToString();
                }
                //bal.PassSpeciality(obj);
                int i = bal.PassSpeciality(obj);
                if (i == 0 || i == 1)
                {
                    Reset();
                    MyMessageBox1.ShowSuccess("Speciality added SuccessFully");
                }
                else
                {
                    MyMessageBox1.ShowError("Speciality could not added");
                }
            }
            else if (btnSave.Text == "Update Speciality")
            {

                obj._Id = int.Parse(hdnId.Value);
                obj._Specialityname = txtspeciality.Text.ToString();
                obj._Details = txtDetails.Text.ToString();
                obj._Facilities = txtFacilities.Text.ToString();
                obj._Services = txtservices.Text.ToString();
                obj._buttontext = "Update Speciality";
                if (ddlGroup.SelectedValue.Equals(0))
                {
                    obj._Group = "No Group";
                }
                else
                {
                    obj._Group = ddlGroup.SelectedItem.Text.ToString();
                }
                //bal.PassSpeciality(obj);
                int i = bal.PassSpeciality(obj);
                if (i == 1)
                {
                    MyMessageBox1.ShowSuccess("Speciality Update SuccessFully");
                }
                else
                {
                    MyMessageBox1.ShowError("Speciality could not Update");
                }

            }
        }
    }

    public DataTable GetRecordEditTime(string id)
    {
       obj._Id=int.Parse(id);

     
       return bal.GetSpeciality(obj);

    }

    public bool CheckFieldsSaveTime()
    {
        bool IsoK = true;
        try
        {
          
            string msg = "";
            if (txtspeciality.Text == string.Empty)
            {
                msg += "Speciality Name,";
                IsoK=false;
            }
            if (txtDetails.Text == string.Empty)
            {
                msg += " Details,";
                IsoK=false;
            }
            //if (ddlGroup.SelectedValue.Equals(0))
            //{
            //    msg += "Select Group,";
            //    IsoK=false;
            //}
            if (txtFacilities.Text == string.Empty)
            {
                msg += " Facilities";
                IsoK=false;
            }
            if (!IsoK)
            {
                MyMessageBox1.ShowError("Please Fill the follwoing Details <br />" + msg);
            }
           
        }
        catch (Exception ex)
        {
            
           this.Title=ex.Message;
        }
        return IsoK;
    }

    public void Reset()
    {
        txtspeciality.Text = string.Empty;
        txtDetails.Text = string.Empty;
        txtFacilities.Text = string.Empty;
        ddlGroup.SelectedIndex = -1;
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void btnbacktolist_Click(object sender, EventArgs e)
    {
        Response.Redirect("SpecialityList.aspx");
    }
}