﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Inner_Page_Banner : AdminPage
{

    Utility utility = new Utility();
    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualImage = "~/uploads/home_banner/";
    InnerBanner innerbannerObj = new InnerBanner();
    BAL bal = new BAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //   Response.Write(GetUniqueImageName());
            BindGrid();

        }
    }
    public string GetUniqueImageName()
    {
        Random rnb = new Random();
        string ImageId = string.Format("{0}{1}{2}{3}-{4}{5}{6}{7}-{8}{9}{10}{11}", rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10), rnb.Next(0, 10));
        return ImageId;
    }
    public bool CheckExtention(string image)
    {

        //string isok = string.Empty;
        //if 
        //{
        //    isok = "true";
        //}
        //else
        //{
        //    isok = "false";
        //}
        return (image == ".JPG" || image == ".jpg" || image == ".png");
    }
    //public bool Validation
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validate())
            {

                if (btnSave.Text == "Save")
                {

                    innerbannerObj.pagename = txtpagename.Text.ToString();
                    if (fileUploadImage.HasFile)
                    {
                        string filename = string.Empty;
                        ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                        if (CheckExtention(ext) == true)
                        {
                            filename = fileUploadImage.FileName.Replace(fileUploadImage.FileName, GetUniqueImageName());
                            filename = "InnerBanner-" + filename + ext;
                            fileUploadImage.SaveAs(Server.MapPath(VirtualImage) + filename);
                            innerbannerObj.image = filename;
                        }
                        else
                        {
                            MyMessageBox1.ShowWarning("Only .jpg .png image can upload !");
                            return;

                        }
                    }
                    else
                    {
                        innerbannerObj.image = "DefaultInnerImage.jpg";
                    }
                    innerbannerObj.ButtonName = "Submit";


                    string Result = bal.InsertUpdateInnerBanner(innerbannerObj);
                    if (Result == "success")
                    {

                        MyMessageBox1.ShowSuccess("Banner Added SuccessFully!!");
                        Reset();
                        BindGrid();
                    }
                    else
                    {
                        MyMessageBox1.ShowError("Banner Could not Added SuccessFully!!");
                    }


                }
                else if (btnSave.Text == "Update")
                {
                    innerbannerObj.pagename = txtpagename.Text.ToString();
                    if (fileUploadImage.HasFile)
                    {
                        string filename = string.Empty;
                        ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                        if (CheckExtention(ext) == true)
                        {
                            filename = fileUploadImage.FileName.Replace(fileUploadImage.FileName, GetUniqueImageName());
                            filename = "InnerBanner-" + filename + ext;
                            fileUploadImage.SaveAs(Server.MapPath(VirtualImage) + filename);
                            innerbannerObj.image = filename;
                        }
                        else
                        {
                            MyMessageBox1.ShowWarning("Only .jpg .png image can upload !");
                            return;

                        }
                    }
                    else
                    {
                        innerbannerObj.image = "";
                    }
                    innerbannerObj.innerid = int.Parse(hdnId.Value);
                    innerbannerObj.ButtonName = "Update";

                    string UpdateResult = bal.InsertUpdateInnerBanner(innerbannerObj);
                    if (UpdateResult == "success")
                    {

                        MyMessageBox1.ShowSuccess("Banner Update SuccessFully!!");
                        Reset();
                        BindGrid();
                    }
                    else
                    {
                        MyMessageBox1.ShowError("Banner Could not Update SuccessFully!!");
                    }
                }
            }
        }
        catch (Exception error)
        {

            this.Title = error.Message;
            // throw;
        }
    }
    public bool Validate()
    {
        bool Isok = true;
        string msg = string.Empty;

        if (txtpagename.Text == "")
        {
            msg += "Page name, ";
            Isok = false;
        }
        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 2);
        }
        //if (!fileUploadImage.HasFile)
        //{
        //    msg += "Image, ";
        //}
        if (!Isok)
        {
            MyMessageBox1.ShowError("Please fill detail " + msg);
        }
        return Isok;
    }
    public DataTable BindGrid()
    {
        DataTable dt = new DataTable();
        dt = bal.GetGridRecord();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
        return dt;
    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        innerbannerObj.innerid = id;
        hdnId.Value = Convert.ToString(id);
        DataTable dt = new DataTable();
        dt = bal.GetRecordByEdittime(innerbannerObj);
        if (dt.Rows.Count > 0)
        {

            txtpagename.Text = innerbannerObj._pagename;
            imgpreview.ImageUrl = "/uploads/home_banner/" + innerbannerObj._image;
            imgpreview.Visible = true;
            btnSave.Text = "Update";
        }
        else
        {
        }


    }
    public void Reset()
    {
        txtpagename.Text = string.Empty;
        imgpreview.Visible = false;
        btnSave.Text = "Save";
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        innerbannerObj.innerid = id;
        string result = bal.DeleteRecordById(innerbannerObj);

        if (result == "success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Banner deleted SuccessFully!");
        }
        else
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowError("Banner could not deleted SuccessFully!");
        }


    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Reset();
    }
}