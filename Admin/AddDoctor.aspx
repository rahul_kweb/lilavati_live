﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="AddDoctor.aspx.cs" Inherits="AddDoctor" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .tbl {
            width: 100%;
            height: 314px;
        }

        .style2 {
        }

        .style3 {
        }

        .style4 {
        }

        .style5 {
            width: 191px;
        }

        .style6 {
            width: 111px;
        }

        .style7 {
            width: 116px;
        }

        .style8 {
            width: 155px;
        }
    </style>
    <%--Show Hide code of availability--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=rdbAvailMon.ClientID %> input').change(function () {
                //alert($(this).val());
                var Number = $(this).val();
                if (Number == 1) {
                    $("#<%=txtMonFrom.ClientID %>").show();
                    $("#<%=ddlMonFrom.ClientID %>").show();
                    $("#<%=txtMonTo.ClientID %>").show();
                    $("#<%=ddlMonTo.ClientID %>").show();
                    $("#<%=txtRoomMon.ClientID %>").show();

                    $("#<%=txtMonFrom1.ClientID %>").show();
                    $("#<%=txtMonTo1.ClientID %>").show();
                    $("#<%=txtRoomMon1.ClientID %>").show();

                }
                else {
                    $("#<%=txtMonFrom.ClientID %>").hide();
                    $("#<%=ddlMonFrom.ClientID %>").hide();
                    $("#<%=txtMonTo.ClientID %>").hide();
                    $("#<%=ddlMonTo.ClientID %>").hide();
                    $("#<%=txtRoomMon.ClientID %>").hide();

                    $("#<%=txtMonFrom1.ClientID %>").hide();
                    $("#<%=txtMonTo1.ClientID %>").hide();
                    $("#<%=txtRoomMon1.ClientID %>").hide();
                }

            });

            $('#<%=rdbAvailTue.ClientID %> input').change(function () {

                //alert($(this).val());
                var Number = $(this).val();
                if (Number == 1) {
                    $("#<%=txtTueFrom.ClientID %>").show();
                    $("#<%=ddlTueFrom.ClientID %>").show();
                    $("#<%=txtTueTo.ClientID %>").show();
                    $("#<%=ddlTueTo.ClientID %>").show();
                    $("#<%=txtRoomTue.ClientID %>").show();

                    $("#<%=txtTueFrom1.ClientID %>").show();
                    $("#<%=txtTueTo1.ClientID %>").show();
                    $("#<%=txtRoomTue1.ClientID %>").show();


                }
                else {
                    $("#<%=txtTueFrom.ClientID %>").hide();
                    $("#<%=ddlTueFrom.ClientID %>").hide();
                    $("#<%=txtTueTo.ClientID %>").hide();
                    $("#<%=ddlTueTo.ClientID %>").hide();
                    $("#<%=txtRoomTue.ClientID %>").hide();

                    $("#<%=txtTueFrom1.ClientID %>").hide();
                    $("#<%=txtTueTo1.ClientID %>").hide();
                    $("#<%=txtRoomTue1.ClientID %>").hide();
                }

            });

            $('#<%=rdbAvailWed.ClientID %> input').change(function () {

                //alert($(this).val());
                var Number = $(this).val();
                if (Number == 1) {
                    $("#<%=txtWedFrom.ClientID %>").show();
                    $("#<%=ddlWedFrom.ClientID %>").show();
                    $("#<%=txtWedTo.ClientID %>").show();
                    $("#<%=ddlWedTo.ClientID %>").show();
                    $("#<%=txtRoomWed.ClientID %>").show();


                    $("#<%=txtWedFrom1.ClientID %>").show();
                    $("#<%=txtWedTo1.ClientID %>").show();
                    $("#<%=txtRoomWed1.ClientID %>").show();

                }
                else {
                    $("#<%=txtWedFrom.ClientID %>").hide();
                    $("#<%=ddlWedFrom.ClientID %>").hide();
                    $("#<%=txtWedTo.ClientID %>").hide();
                    $("#<%=ddlWedTo.ClientID %>").hide();
                    $("#<%=txtRoomWed.ClientID %>").hide();

                    $("#<%=txtWedFrom1.ClientID %>").hide();
                    $("#<%=txtWedTo1.ClientID %>").hide();
                    $("#<%=txtRoomWed1.ClientID %>").hide();
                }

            });

            $('#<%=rdbAvailThu.ClientID %> input').change(function () {

                //alert($(this).val());
                var Number = $(this).val();
                if (Number == 1) {
                    $("#<%=txtThuFrom.ClientID %>").show();
                    $("#<%=ddlThuFrom.ClientID %>").show();
                    $("#<%=txtThuTo.ClientID %>").show();
                    $("#<%=ddlThuTo.ClientID %>").show();
                    $("#<%=txtRoomThu.ClientID %>").show();
                    $("#<%=txtThuFrom1.ClientID %>").show();
                    $("#<%=txtThuTo1.ClientID %>").show();
                    $("#<%=txtRoomThu1.ClientID %>").show();
                }
                else {
                    $("#<%=txtThuFrom.ClientID %>").hide();
                    $("#<%=ddlThuFrom.ClientID %>").hide();
                    $("#<%=txtThuTo.ClientID %>").hide();
                    $("#<%=ddlThuTo.ClientID %>").hide();
                    $("#<%=txtRoomThu.ClientID %>").hide();
                    $("#<%=txtThuFrom1.ClientID %>").hide();
                    $("#<%=txtThuTo1.ClientID %>").hide();
                    $("#<%=txtRoomThu1.ClientID %>").hide();
                }

            });

            $('#<%=rdbAvailFri.ClientID %> input').change(function () {

                //alert($(this).val());
                var Number = $(this).val();
                if (Number == 1) {
                    $("#<%=txtFriFrom.ClientID %>").show();
                    $("#<%=ddlFriFrom.ClientID %>").show();
                    $("#<%=txtFriTo.ClientID %>").show();
                    $("#<%=ddlFriTo.ClientID %>").show();
                    $("#<%=txtRoomFri.ClientID %>").show();


                    $("#<%=txtFriFrom1.ClientID %>").show();

                    $("#<%=txtFriTo1.ClientID %>").show();

                    $("#<%=txtRoomFri1.ClientID %>").show();
                }
                else {
                    $("#<%=txtFriFrom.ClientID %>").hide();
                    $("#<%=ddlFriFrom.ClientID %>").hide();
                    $("#<%=txtFriTo.ClientID %>").hide();
                    $("#<%=ddlFriTo.ClientID %>").hide();
                    $("#<%=txtRoomFri.ClientID %>").hide();

                    $("#<%=txtFriFrom1.ClientID %>").hide();

                    $("#<%=txtFriTo1.ClientID %>").hide();

                    $("#<%=txtRoomFri1.ClientID %>").hide();
                }

            });

            $('#<%=rdbAvailSat.ClientID %> input').change(function () {

                //alert($(this).val());
                var Number = $(this).val();
                if (Number == 1) {
                    $("#<%=txtSatFrom.ClientID %>").show();
                    $("#<%=ddlSatFrom.ClientID %>").show();
                    $("#<%=txtSatTo.ClientID %>").show();
                    $("#<%=ddlSatTo.ClientID %>").show();
                    $("#<%=txtRoomSat.ClientID %>").show();


                    $("#<%=txtSatFrom1.ClientID %>").show();
                    $("#<%=txtSatTo1.ClientID %>").show();
                    $("#<%=txtRoomSat1.ClientID %>").show();
                }
                else {
                    $("#<%=txtSatFrom.ClientID %>").hide();
                    $("#<%=ddlSatFrom.ClientID %>").hide();
                    $("#<%=txtSatTo.ClientID %>").hide();
                    $("#<%=ddlSatTo.ClientID %>").hide();
                    $("#<%=txtRoomSat.ClientID %>").hide();


                    $("#<%=txtSatFrom1.ClientID %>").hide();
                    $("#<%=txtSatTo1.ClientID %>").hide();
                    $("#<%=txtRoomSat1.ClientID %>").hide();
                }

            });
        });




    </script>
    <script type="text/jscript">
        $(document).ready(function () {


            $('#<%=ddlGender.ClientID %>').change(function () {
                var value = $('#<%=ddlGender.ClientID %>').val();


                if (value == "Male") {
                    $('#<%=ImgDoctor.ClientID %>').attr('src', '../images/doctors_img/doctor.jpg');

                    //                    console.log("male working" + c);
                }
                else if (value == "Female") {
                    //console.log("female working");
                    $('#<%=ImgDoctor.ClientID %>').attr('src', '../images/doctors_img/female-doctor.jpg');

                    //                    alert(d);
                    //                    console.log("female working" + d);
                }




                //                console.log(value);
            });


        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            /*xxxxxxxxxxxxxxxxxxxxxxxxMonday startxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            $("#btnaddmonday").click(function () {

                var mondayfrom = $('#<%=txtMonFrom.ClientID %>').val();
                mondayfrom = mondayfrom + "#";
                $('#<%=txtMonFrom.ClientID %>').val(mondayfrom)

                var mondayto = $('#<%=txtMonTo.ClientID %>').val();
                mondayto = mondayto + "#";
                $('#<%=txtMonTo.ClientID %>').val(mondayto)

                var mondaytoroom = $('#<%=txtRoomMon.ClientID %>').val();
                mondaytoroom = mondaytoroom + "#";
                $('#<%=txtRoomMon.ClientID %>').val(mondaytoroom)



                $("#btncancelmonday").css('display', 'block');
                $("#btnaddmonday").css('display', 'none');

                return false;
            });

            $("#btncancelmonday").click(function () {
                var mondayfrom = $('#<%=txtMonFrom.ClientID %>').val();
                mondayfrom = mondayfrom.substring(0, mondayfrom.length - 1);
                $('#<%=txtMonFrom.ClientID %>').val(mondayfrom)

                var mondayto = $('#<%=txtMonTo.ClientID %>').val();
                mondayto = mondayto.substring(0, mondayto.length - 1);
                $('#<%=txtMonTo.ClientID %>').val(mondayto)

                var mondaytoroom = $('#<%=txtRoomMon.ClientID %>').val();
                mondaytoroom = mondaytoroom.substring(0, mondaytoroom.length - 1);
                $('#<%=txtRoomMon.ClientID %>').val(mondaytoroom)

                $("#btncancelmonday").css('display', 'none');
                $("#btnaddmonday").css('display', 'block');

                return false;
            });




            /*xxxxxxxxxxxxxxxxxxxxxxxxTuesday startxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            $("#btnaddtue").click(function () {

                var tuedayfrom = $('#<%=txtTueFrom.ClientID %>').val();
                tuedayfrom = tuedayfrom + "#";
                $('#<%=txtTueFrom.ClientID %>').val(tuedayfrom)

                var tuedayto = $('#<%=txtTueTo.ClientID %>').val();
                tuedayto = tuedayto + "#";
                $('#<%=txtTueTo.ClientID %>').val(tuedayto)

                var tuedaytoroom = $('#<%=txtRoomTue.ClientID %>').val();
                tuedaytoroom = tuedaytoroom + "#";
                $('#<%=txtRoomTue.ClientID %>').val(tuedaytoroom)

                $("#btncanceltue").css('display', 'block');
                $("#btnaddtue").css('display', 'none');

                return false;
            });

            $("#btncanceltue").click(function () {
                var tuedayfrom = $('#<%=txtTueFrom.ClientID %>').val();
                tuedayfrom = tuedayfrom.substring(0, tuedayfrom.length - 1);
                $('#<%=txtTueFrom.ClientID %>').val(tuedayfrom)

                var tuedayto = $('#<%=txtTueTo.ClientID %>').val();
                tuedayto = tuedayto.substring(0, tuedayto.length - 1);
                $('#<%=txtTueTo.ClientID %>').val(tuedayto)

                var tuedaytoroom = $('#<%=txtRoomTue.ClientID %>').val();
                tuedaytoroom = tuedaytoroom.substring(0, tuedaytoroom.length - 1);
                $('#<%=txtRoomTue.ClientID %>').val(tuedaytoroom)

                $("#btncanceltue").css('display', 'none');
                $("#btnaddtue").css('display', 'block');

                return false;
            });





            /*xxxxxxxxxxxxxxxxxxxxxxxxWednesday startxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            $("#btnaddwed").click(function () {

                var weddayfrom = $('#<%=txtWedFrom.ClientID %>').val();
                weddayfrom = weddayfrom + "#";
                $('#<%=txtWedFrom.ClientID %>').val(weddayfrom)

                var wednesdayto = $('#<%=txtWedTo.ClientID %>').val();
                wednesdayto = wednesdayto + "#";
                $('#<%=txtWedTo.ClientID %>').val(wednesdayto)

                var wednesdaytoroom = $('#<%=txtRoomWed.ClientID %>').val();
                wednesdaytoroom = wednesdaytoroom + "#";
                $('#<%=txtRoomWed.ClientID %>').val(wednesdaytoroom)


                $("#btncancelwed").css('display', 'block');
                $("#btnaddwed").css('display', 'none');

                return false;
            });

            $("#btncancelwed").click(function () {
                var weddayfrom = $('#<%=txtWedFrom.ClientID %>').val();
                weddayfrom = weddayfrom.substring(0, weddayfrom.length - 1);
                $('#<%=txtWedFrom.ClientID %>').val(weddayfrom)

                var wednesdayto = $('#<%=txtWedTo.ClientID %>').val();
                wednesdayto = wednesdayto.substring(0, wednesdayto.length - 1);
                $('#<%=txtWedTo.ClientID %>').val(wednesdayto)

                var wednesdaytoroom = $('#<%=txtRoomWed.ClientID %>').val();
                wednesdaytoroom = wednesdaytoroom.substring(0, wednesdaytoroom.length - 1);
                $('#<%=txtRoomWed.ClientID %>').val(wednesdaytoroom)


                $("#btncancelwed").css('display', 'none');
                $("#btnaddwed").css('display', 'block');

                return false;
            });






            /*xxxxxxxxxxxxxxxxxxxxxxxxThursday startxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            $("#btnaddThu").click(function () {

                var thursdayfrom = $('#<%=txtThuFrom.ClientID %>').val();
                thursdayfrom = thursdayfrom + "#";
                $('#<%=txtThuFrom.ClientID %>').val(thursdayfrom)

                var thursdayto = $('#<%=txtThuTo.ClientID %>').val();
                thursdayto = thursdayto + "#";
                $('#<%=txtThuTo.ClientID %>').val(thursdayto)

                var thursdaytoroom = $('#<%=txtRoomThu.ClientID %>').val();
                thursdaytoroom = thursdaytoroom + "#";
                $('#<%=txtRoomThu.ClientID %>').val(thursdaytoroom)


                $("#btncancelthur").css('display', 'block');
                $("#btnaddThu").css('display', 'none');

                return false;
            });

            $("#btncancelthur").click(function () {
                var thursdayfrom = $('#<%=txtThuFrom.ClientID %>').val();
                thursdayfrom = thursdayfrom.substring(0, thursdayfrom.length - 1);
                $('#<%=txtThuFrom.ClientID %>').val(thursdayfrom)

                var thursdayto = $('#<%=txtThuTo.ClientID %>').val();
                thursdayto = thursdayto.substring(0, thursdayto.length - 1);
                $('#<%=txtThuTo.ClientID %>').val(thursdayto)


                var thursdaytoroom = $('#<%=txtRoomThu.ClientID %>').val();
                thursdaytoroom = thursdaytoroom.substring(0, thursdaytoroom.length - 1);
                $('#<%=txtRoomThu.ClientID %>').val(thursdaytoroom)

                $("#btncancelthur").css('display', 'none');
                $("#btnaddThu").css('display', 'block');

                return false;
            });




            /*xxxxxxxxxxxxxxxxxxxxxxxxFriday startxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            $("#btnaddFri").click(function () {

                var fridayfrom = $('#<%=txtFriFrom.ClientID %>').val();
                fridayfrom = fridayfrom + "#";
                $('#<%=txtFriFrom.ClientID %>').val(fridayfrom)

                var fridayto = $('#<%=txtFriTo.ClientID %>').val();
                fridayto = fridayto + "#";
                $('#<%=txtFriTo.ClientID %>').val(fridayto)

                var fridaytoroom = $('#<%=txtRoomFri.ClientID %>').val();
                fridaytoroom = fridaytoroom + "#";
                $('#<%=txtRoomFri.ClientID %>').val(fridaytoroom)


                $("#btncancelfri").css('display', 'block');
                $("#btnaddFri").css('display', 'none');

                return false;
            });

            $("#btncancelfri").click(function () {
                var fridayfrom = $('#<%=txtFriFrom.ClientID %>').val();
                fridayfrom = fridayfrom.substring(0, fridayfrom.length - 1);
                $('#<%=txtFriFrom.ClientID %>').val(fridayfrom)

                var fridayto = $('#<%=txtFriTo.ClientID %>').val();
                fridayto = fridayto.substring(0, fridayto.length - 1);
                $('#<%=txtFriTo.ClientID %>').val(fridayto)

                var fridaytoroom = $('#<%=txtRoomFri.ClientID %>').val();
                fridaytoroom = fridaytoroom.substring(0, fridaytoroom.length - 1);
                $('#<%=txtRoomFri.ClientID %>').val(fridaytoroom)


                $("#btncancelfri").css('display', 'none');
                $("#btnaddFri").css('display', 'block');

                return false;
            });



            /*xxxxxxxxxxxxxxxxxxxxxxxxSaturday startxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            $("#btnaddsat").click(function () {

                var satdayfrom = $('#<%=txtSatFrom.ClientID %>').val();
                satdayfrom = satdayfrom + "#";
                $('#<%=txtSatFrom.ClientID %>').val(satdayfrom)

                var satdayto = $('#<%=txtSatTo.ClientID %>').val();
                satdayto = satdayto + "#";
                $('#<%=txtSatTo.ClientID %>').val(satdayto)

                var satdaytoroom = $('#<%=txtRoomSat.ClientID %>').val();
                satdaytoroom = satdaytoroom + "#";
                $('#<%=txtRoomSat.ClientID %>').val(satdaytoroom)


                $("#btncancelsat").css('display', 'block');
                $("#btnaddsat").css('display', 'none');

                return false;
            });

            $("#btncancelsat").click(function () {
                var satdayfrom = $('#<%=txtSatFrom.ClientID %>').val();
                satdayfrom = satdayfrom.substring(0, satdayfrom.length - 1);
                $('#<%=txtSatFrom.ClientID %>').val(satdayfrom)

                var satdayto = $('#<%=txtSatTo.ClientID %>').val();
                satdayto = satdayto.substring(0, satdayto.length - 1);
                $('#<%=txtSatTo.ClientID %>').val(satdayto)

                var satdaytoroom = $('#<%=txtRoomSat.ClientID %>').val();
                satdaytoroom = satdaytoroom.substring(0, satdaytoroom.length - 1);
                $('#<%=txtRoomSat.ClientID %>').val(satdaytoroom)


                $("#btncancelsat").css('display', 'none');
                $("#btnaddsat").css('display', 'block');

                return false;
            });




        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        <%-- ADD DOCTOR--%>
        <%=heading %>
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <asp:Label ID="lbltest" runat="server"></asp:Label>
    <div class="form-box" style="width: 700px  !important;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Doctor Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDoctorName" CssClass="textbox" runat="server" />
        <br />
        <label class="control-label">
            Group<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlGender" runat="server" CssClass="dropdown">
            <asp:ListItem Text="Male" Value="Male" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
        </asp:DropDownList>
        <br />
        <label class="control-label">
            Doctor Photo <span class="required">*</span> :
             <br />
            (Recommended image size for Doctor Image - Width 200px X Height 200px) 
        </label>
        <asp:FileUpload ID="fileuploadimage" runat="server" CssClass="file-upload" />
        &nbsp;
        <asp:Image ID="ImgDoctor" Height="100px" Width="120px" runat="server" ImageUrl="~/images/doctors_img/doctor.jpg" />
        <br />
        <label class="control-label">
            Qualification <span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtQualification" CssClass="textbox1" runat="server" Width="373px" />
        <br />
        <label class="control-label">
            Profile <span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtProfile" runat="server" />
        <br />
        <label class="control-label">
            Area Of Interest <span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtAreaOfInterest" runat="server" />
        <br />
        <label class="control-label">
            Awards and Recognition <span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtAchievments" runat="server" />
        <br />
        <label class="control-label">
            Research Publication <span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtResearchPublication" runat="server" />
        <br />
        <label class="control-label">
            Slot Minuts <span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtslotminuts" CssClass="textbox1" runat="server" Width="373px" />
        <br />
        <label class="control-label">
            Cons Dr Id <span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtconsdocid" CssClass="textbox1" runat="server" Width="373px" />
        <br />
        <table class="tbl" style="border-top: 0px;">
            <tr style="display: block;">
                <td class="style3" colspan="2">
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Available Timings"></asp:Label>
                </td>
            </tr>
            <tr style="display: block;">
                <td class="style7"></td>

                <td class="style5">
                    <asp:Label ID="Label18" runat="server" Font-Bold="True" Text="Availability Status"></asp:Label>
                </td>
                <td class="style8">
                    <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="From"></asp:Label>
                </td>
                <td class="style8">
                    <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="To"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="Room"></asp:Label>
                </td>
                <%--<td class="style8">&nbsp;</td>--%>
            </tr>
            <tr style="display: block;">
                <td class="style7">
                    <asp:Label ID="Label5" runat="server" Text="Mon :-"></asp:Label>
                </td>
                <td class="style5">
                    <asp:RadioButtonList ID="rdbAvailMon" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="Available" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Not Available"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtMonFrom" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtMonFrom1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlMonFrom" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtMonTo" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtMonTo1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlMonTo" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRoomMon" runat="server" CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtRoomMon1" runat="server" CssClass="textbox1"></asp:TextBox>
                </td>
                <%--<td style="padding-left: 10px;" class="style8">
                  
                    <input type="image" src="Doctors/AddMoreTime.png" alt="Submit" id="btnaddmonday" />
                    <input type="image" src="Doctors/cancelMoreTime.png" alt="Submit" id="btncancelmonday"
                        style="display: none;" />
                   
                </td>--%>
            </tr>
            <tr style="display: block;">
                <td class="style7">
                    <asp:Label ID="Label6" runat="server" Text="Tue :-"></asp:Label>
                </td>
                <td class="style5">
                    <asp:RadioButtonList ID="rdbAvailTue" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="Available" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Not Available"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtTueFrom" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>

                    <br />
                    <asp:TextBox ID="txtTueFrom1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlTueFrom" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtTueTo" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtTueTo1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlTueTo" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRoomTue" runat="server" CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtRoomTue1" runat="server" CssClass="textbox1"></asp:TextBox>
                </td>
                <%--<td style="padding-left: 10px;">
                  
                    <input type="image" src="Doctors/AddMoreTime.png" alt="Submit" id="btnaddtue" />
                    <input type="image" src="Doctors/cancelMoreTime.png" alt="Submit" id="btncanceltue"
                        style="display: none;" />
                  
                </td>--%>
            </tr>
            <tr style="display: block;">
                <td class="style7">
                    <asp:Label ID="Label7" runat="server" Text="Wed :-"></asp:Label>
                </td>
                <td class="style5">
                    <asp:RadioButtonList ID="rdbAvailWed" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="Available" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Not Available"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtWedFrom" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtWedFrom1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlWedFrom" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtWedTo" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtWedTo1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlWedTo" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRoomWed" runat="server" CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtRoomWed1" runat="server" CssClass="textbox1"></asp:TextBox>
                </td>
                <%--<td style="padding-left: 10px;">
                   
                    <input type="image" src="Doctors/AddMoreTime.png" alt="Submit" id="btnaddwed" />
                    <input type="image" src="Doctors/cancelMoreTime.png" alt="Submit" id="btncancelwed"
                        style="display: none;" />
                </td>--%>
            </tr>
            <tr style="display: block;">
                <td class="style7">
                    <asp:Label ID="Label13" runat="server" Text="Thu :-"></asp:Label>
                </td>
                <td class="style5">
                    <asp:RadioButtonList ID="rdbAvailThu" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="Available" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Not Available"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtThuFrom" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtThuFrom1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlThuFrom" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtThuTo" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtThuTo1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlThuTo" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRoomThu" runat="server" CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtRoomThu1" runat="server" CssClass="textbox1"></asp:TextBox>
                </td>
                <%--<td style="padding-left: 10px;">
                   
                    <input type="image" src="Doctors/AddMoreTime.png" alt="Submit" id="btnaddThu" />
                    <input type="image" src="Doctors/cancelMoreTime.png" alt="Submit" id="btncancelthur"
                        style="display: none;" />
                </td>--%>
            </tr>
            <tr style="display: block;">
                <td class="style7">
                    <asp:Label ID="Label14" runat="server" Text="Fri :-"></asp:Label>
                </td>
                <td class="style5">
                    <asp:RadioButtonList ID="rdbAvailFri" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="Available" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Not Available"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtFriFrom" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtFriFrom1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlFriFrom" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtFriTo" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtFriTo1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlFriTo" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRoomFri" runat="server" CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtRoomFri1" runat="server" CssClass="textbox1"></asp:TextBox>
                </td>
                <%--<td style="padding-left: 10px;">
                  
                    <input type="image" src="Doctors/AddMoreTime.png" alt="Submit" id="btnaddFri" />
                    <input type="image" src="Doctors/cancelMoreTime.png" alt="Submit" id="btncancelfri"
                        style="display: none;" />
                </td>--%>
            </tr>
            <tr style="display: block;">
                <td class="style7">
                    <asp:Label ID="Label15" runat="server" Text="Sat :-"></asp:Label>
                </td>
                <td class="style5">
                    <asp:RadioButtonList ID="rdbAvailSat" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1" Text="Available" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0" Text="Not Available"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtSatFrom" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtSatFrom1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlSatFrom" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:TextBox ID="txtSatTo" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtSatTo1" runat="server" onKeyPress="return onlyNumbers();"
                        CssClass="textbox1"></asp:TextBox>
                    <asp:DropDownList ID="ddlSatTo" runat="server" CssClass="dropdown" Visible="false">
                        <asp:ListItem>am</asp:ListItem>
                        <asp:ListItem>pm</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRoomSat" runat="server" CssClass="textbox1"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="txtRoomSat1" runat="server" CssClass="textbox1"></asp:TextBox>
                </td>
                <%--<td style="padding-left: 10px;">
                   
                    <input type="image" src="Doctors/AddMoreTime.png" alt="Submit" id="btnaddsat" />
                    <input type="image" src="Doctors/cancelMoreTime.png" alt="Submit" id="btncancelsat"
                        style="display: none;" />
                </td>--%>
            </tr>
        </table>
        <table class="tbl" style="border-top: 0px;">

            <tr>
                <td class="style7"></td>
                <td class="style5"></td>
                <td class="style8"></td>
                <td></td>
            </tr>
            <tr>
                <td class="style7">&nbsp;
                </td>
                <td class="style2" colspan="3">&nbsp;
                </td>
            </tr>
            <tr>
                <td class="style3" colspan="4">
                    <asp:Label ID="Label17" runat="server" Font-Bold="True" Text="Specialities"></asp:Label>
                </td>
            </tr>
            <tr>

                <td class="style2" colspan="4">
                    <asp:CheckBoxList ID="chkSpecialities" runat="server" RepeatColumns="3" CssClass="multi-select-check-list">
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="style7">&nbsp;
                </td>
                <td class="style2" colspan="3">&nbsp;
                </td>
            </tr>
            <tr>
                <td class="style7">&nbsp;
                </td>
                <td class="style2" colspan="3"></td>
            </tr>
            <tr>
                <td class="style7">&nbsp;
                </td>
                <td class="style2" colspan="3">&nbsp;
                </td>
            </tr>
        </table>
             
        <label class="control-label">
            Video Consultation :
        </label>
        <asp:RadioButtonList ID="rdbTeleConsultation" runat="server" RepeatDirection="Horizontal">
            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
            <asp:ListItem Text="No" Value="0"></asp:ListItem>
        </asp:RadioButtonList>
        <br />

        <label class="control-label">
            Direct Appointment<span class="required">*</span> :
        </label>
        <asp:RadioButtonList ID="rdDirectAvailable" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rdDirectAvailable_SelectedIndexChanged">
            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <div id="DirectAvailable" runat="server" visible="false">
            <label class="control-label">
                Contact Number<span class="required">*</span> :
            </label>
            <asp:TextBox ID="txtDirectContact" TextMode="MultiLine" runat="server" CssClass="textbox1" Style="width: 373px; height: 70px"> </asp:TextBox>
            <br />
        <%--    <label class="control-label">
                Time :
            </label>--%>
            <div>
                <label class="control-label">
                    Time<span class="required">*</span> :
                </label>
        <CKEditor:CKEditorControl ID="txtDirectTimeFrom" runat="server" Styles="height:100px;width:100px" />
<%--                <asp:TextBox ID="txtDirectTimeFrom" runat="server" CssClass="textbox1"></asp:TextBox>--%>
            </div>
<%--            <div style="width: 150px; float: left">
                <label class="control-label">
                    To<span class="required">*</span> :
                </label>
                <asp:TextBox ID="txtDirectTimeTo" runat="server" CssClass="textbox1"></asp:TextBox>
            </div>--%>
            <div style="clear: both; width: 100%"></div>

       <%--     <label class="control-label">
                Days :
            </label>--%>
<%--            <div>
                <label class="control-label">
                    Days<span class="required">*</span> :
                </label>
                <asp:TextBox ID="txtDirectDaysFrom" runat="server" CssClass="textbox1" Style="width: 200px;"></asp:TextBox>
                <CKEditor:CKEditorControl ID="txtDirectDaysFrom" runat="server" Styles="height:100px;width:100px" />
            </div>--%>
<%--            <div style="width: 300px; float: left">
                <label class="control-label">
                    To<span class="required">*</span> :
                </label>
                <asp:TextBox ID="txtDirectDaysTo" runat="server" CssClass="textbox1" Style="width: 200px;"></asp:TextBox>
            </div>--%>
            <div style="clear: both; width: 100%"></div>

        </div>

        <br />
        <br />
        <br />
        <asp:Button ID="btnSave" Text="Add Doctor" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" Text="Cancel" CssClass="button" runat="server" Visible="true"
            OnClick="btncancel_Click" />
        <asp:Button ID="btnbacktolist" Text="Back to list" CssClass="button" runat="server"
            Visible="false" OnClick="btnbacktolist_Click" />
    </div>
</asp:Content>
