﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="feedback_OutPatientDetail.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Feedback Out Patient Details
    </div>
    <div class="form-box" style="width: 800px;">
        <table class="gridtable">
            <tr>
                <td>First Name</td>
                <td>
                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>LH No.</td>
                <td>
                    <asp:Label ID="lblLhno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Name of Doctor</td>
                <td>
                    <asp:Label ID="lblNameOfDoctor" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Contact No.</td>
                <td>
                    <asp:Label ID="lblContactNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email Id</td>
                <td>
                    <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Date</td>
                <td>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>HOW DID YOU COME TO KNOW ABOUT LILAVATI HOSPITAL?</td>
                <td>
                    <asp:Label ID="lblHowDidYouComeToKnow" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Time taken by appointment staff to give an appointment for Doctor/ service.</td>
                <td>
                    <asp:Label ID="lblTimeTakenByAppointment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Guidance and information provided by the staff.</td>
                <td>
                    <asp:Label ID="lblGuidanceAndInformation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy and friendliness of OPD staff.</td>
                <td>
                    <asp:Label ID="lblCourtesyAndFriendlinessOPD" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Time taken to complete the registration process.
                </td>
                <td>
                    <asp:Label ID="lblTimeTakenToCompleteRegistration" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Time taken to complete the billing process.
                </td>
                <td>
                    <asp:Label ID="lblTimeTakenToCompleteBilling" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy and information provided by OPD staff.
                </td>
                <td>
                    <asp:Label ID="lblCourtesyAndInformation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Waiting time to see the doctor after completing registration/ billing formalities.
                </td>
                <td>
                    <asp:Label ID="lblWaitingTimeToSeeDoctor" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Clarity & information shared by the doctor about your health & line of treatment.</td>
                <td>
                    <asp:Label ID="lblClarityAndInformation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Care & attention given by the doctor.</td>
                <td>
                    <asp:Label ID="lblCareAndAttention" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Behavior & attitude of nurse.</td>
                <td>
                    <asp:Label ID="lblBehaviorAndAttitude" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Respect for your privacy.</td>
                <td>
                    <asp:Label ID="lblRespectForYourPrivacy" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Care offered during procedure/ Investigation by nurse.</td>
                <td>
                    <asp:Label ID="lblCareOfferedDuringProc" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Waiting time for the test/ Investigation after completing registration & billing formalities.</td>
                <td>
                    <asp:Label ID="lblWaitingTimeForTheTest" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Experience with technical staff (Doctor, Technician) during test & Investigation (in terms of guidance, behavior & coutesy)</td>
                <td>
                    <asp:Label ID="lblExperienceWithTechnical" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy and friendliness of the staff.</td>
                <td>
                    <asp:Label ID="lblCourtesyAndFriendlinessStaff" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Comfort of waiting area.</td>
                <td>
                    <asp:Label ID="lblComfortOfWaitingArea" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Cleanliness in the OPD area.</td>
                <td>
                    <asp:Label ID="lblCleanlinessInOPD" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Cleanliness of toilets.</td>
                <td>
                    <asp:Label ID="lblCleanlinessOfToilets" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>How would you rate your overall experience at Lilavati Hospital.</td>
                <td>
                    <asp:Label ID="lblHowWouldYouRate" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>What did you like the most about Lilavati hospital? </td>
                <td>
                    <asp:Label ID="lblWhatDidYouLike" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>What did you like the least about Lilavati hospital? </td>
                <td>
                    <asp:Label ID="lblWhatDidYouLikeLeast" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Any suggestion you would like to put forward which will help us to serve you better</td>
                <td>
                    <asp:Label ID="lblAnysuggestionYouWouldLike" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

