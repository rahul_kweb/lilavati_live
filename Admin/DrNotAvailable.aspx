﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="DrNotAvailable.aspx.cs" Inherits="Admin_DrNotAvailable" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <link href="../css/AppointmentDropdowncss.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--  <script src="../js/dropdowndashboard.min.js" type="text/javascript"></script>
    --%>
    <%--  <script src="../js/1.8.3_min.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(function () {
            $(".selectdate").datepicker({
                changeMonth: true,
                changeYear: true,
                //yearRange: "c-100:c",
                yearRange: '1950:2020',
                dateFormat: "dd-mm-yy"
               // dateFormat: "yyyy-mm-dd"
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Dr Not Available 
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />


        <label class="control-label">
            Select Date<span class="required">*</span> :            
        </label>
        <asp:TextBox ID="txtdate" runat="server" CssClass="textbox selectdate"></asp:TextBox>
        <br />

        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btnClear" runat="server" Text="Cancel" CssClass="button" OnClick="btnClear_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="50" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" OnPageIndexChanging="gdView_PageIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="NOTAVAILABLEID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Dr Not Available on this Date's">
                <ItemTemplate>
                    <asp:Literal ID="ltrldate" runat="server" Text='<%#Eval("NOTAVAILBLEDATE") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

