﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    RegistrationTable tbl = new RegistrationTable();
    BAL bal = new BAL();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //ViewState["RefUrl"] = Request.UrlReferrer.ToString();

            if (Request.QueryString["LHNO"] != null)
            {
                string LHNO = Request.QueryString["LHNO"].ToString();
                UpdateLHNO(LHNO);

                pnlRegistrationDetails.Visible = false;
                pnlLHNOUpdate.Visible = true;
            }
            else if (Request.QueryString["Id"] != null)
            {
                string Id = Request.QueryString["Id"].ToString();
                ViewDetail(Id);

                pnlRegistrationDetails.Visible = true;
                pnlLHNOUpdate.Visible = false;
            }
        }
    }

    public void ViewDetail(string Id)
    {
        tbl.AddressIndex = int.Parse(Id.ToString());
        bal.ViewRegistrationDetail(tbl);

        lblwelcome.Text = tbl.FirstName + " " + tbl.SureName;
        lblSureName.Text = tbl.SureName;
        lblFirstName.Text = tbl.FirstName;
        lblLastName.Text = tbl.MiddleName;
        lblSex.Text = tbl.Sex;
        lblEmailID.Text = tbl.EmailID;
        lblLHNO.Text = tbl.LHNO;
        lblDOB.Text = tbl.DOB;
        lblMaritalStatus.Text = tbl.MaritalStatus;
        lblReligion.Text = tbl.Religion;
        lblOccupation.Text = tbl.Occupation;
        lblAddress.Text = tbl.Address;
        lblTelephoneResi.Text = tbl.TelephoneResidence;
        lblMobileNo.Text = tbl.MobileNo;
        lblPwd.Text = tbl.Password;
        lblCountry.Text = tbl.Country;
        lblCity.Text = tbl.City;
        lblState.Text = tbl.State;
        lblPincode.Text = tbl.Pincode;
        lblUserId.Text = tbl.UserName;
        lblTitle.Text = tbl.Title;
        lblBloodGroup.Text = tbl.BloodGroup;
        lblHosLHNO.Text = tbl.OriginalLHNO;
    }

    public void UpdateLHNO(string LHNO)
    {
        tbl.AddressIndex = int.Parse(LHNO.ToString());
        bal.ViewRegistrationDetail(tbl);
        lblUserName.Text = tbl.FirstName + " " + tbl.SureName;
        txtLHNO.Text = tbl.LHNO;
        Session["LHNO"] = int.Parse(LHNO.ToString());
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        tbl.AddressIndex = Convert.ToInt32(Session["LHNO"].ToString());
        tbl.LHNO = txtLHNO.Text.Trim();        
        bal.UpdateUserLHNO(tbl);

        if (HttpContext.Current.Session["msgLHNO"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Record Updated Successfully.");
            bal.SendLHNO(tbl);
        }
        else
        {
            MyMessageBox1.ShowWarning("LHNO Already Exits.");
        }
    }

    protected void btnBack1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Registration.aspx");
    }

    
}