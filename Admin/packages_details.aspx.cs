﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_packages_details : AdminPage
{
    HealthPackagesDetails tblHealthPackagesDetails = new HealthPackagesDetails();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        //string Id = Request.QueryString["Id"].ToString();
        if (!IsPostBack)
        {
            BindGrid();
            ddlType.Enabled = true;
            TittleName();
        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        string value = ddlType.SelectedValue;

        if (value == "Tab")
        {
            idDiv.Visible = true;
        }
        else if (value == "Without Tab")
        {
            idDiv.Visible = false;
        }
        else
        {
            idDiv.Visible = false;
        }
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if(CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        tblHealthPackagesDetails.Health_Id = int.Parse(Request.QueryString["Id"].ToString());
        tblHealthPackagesDetails.Pack_Price = txtPackagePrice.Text.Trim();
        tblHealthPackagesDetails.Tittle = txtTittle.Text.Trim();
        tblHealthPackagesDetails.Tab_Name = txtTab.Text.Trim();
        tblHealthPackagesDetails.Description = txtDescription.Text.Trim();
        tblHealthPackagesDetails.Note = txtNote.Text.Trim();
        cmsbal.SaveHealthPackageDetails(tblHealthPackagesDetails);

        if (HttpContext.Current.Session["SaveHealthPackagesDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not insert successfully");
        }
    }

    public void UpdateRecord()
    {
        tblHealthPackagesDetails.Pack_Id = Convert.ToInt32(hdnId.Value);
        tblHealthPackagesDetails.Health_Id = int.Parse(Request.QueryString["Id"].ToString());
        tblHealthPackagesDetails.Pack_Price = txtPackagePrice.Text.Trim();
        tblHealthPackagesDetails.Tittle = txtTittle.Text.Trim();
        tblHealthPackagesDetails.Tab_Name = txtTab.Text.Trim();
        tblHealthPackagesDetails.Description = txtDescription.Text.Trim();
        tblHealthPackagesDetails.Note = txtNote.Text.Trim();
        cmsbal.UpdateHealthPackageDetails(tblHealthPackagesDetails);

        if (HttpContext.Current.Session["UpdateHealthPackagesDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        tblHealthPackagesDetails.Health_Id = int.Parse(Request.QueryString["Id"].ToString());
        dt = cmsbal.GetHealthPackageDetails(tblHealthPackagesDetails);

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
    }

    public void Reset()
    {
        ddlType.SelectedIndex = -1;
        txtPackagePrice.Text = string.Empty;
        txtTab.Text = string.Empty;
        txtTittle.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtNote.Text = string.Empty;

        btnSave.Text = "Save";
        ddlType.Enabled = true;
        idDiv.Visible = false;
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
    
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblHealthPackagesDetails.Pack_Id = id;
        cmsbal.DeleteHealthPackageDetails(tblHealthPackagesDetails);

        if (HttpContext.Current.Session["DeleteHealthPackagesDetails"] == "Success")
        {
            BindGrid();           
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblHealthPackagesDetails.Pack_Id = id;
        cmsbal.GetByHealthPackagesDetails(tblHealthPackagesDetails);

        hdnId.Value = Convert.ToInt32(tblHealthPackagesDetails.Pack_Id).ToString();
        hdnId2.Value = Convert.ToInt32(tblHealthPackagesDetails.Health_Id).ToString();
        txtPackagePrice.Text = tblHealthPackagesDetails.Pack_Price;
        txtTittle.Text = tblHealthPackagesDetails.Tittle;

        if (tblHealthPackagesDetails.Tab_Name != "")
        {
            idDiv.Visible = true;
            txtTab.Text = tblHealthPackagesDetails.Tab_Name;
            ddlType.SelectedValue = "Tab";
            ddlType.Enabled = false;
        }
        else
        {
            idDiv.Visible = false;
            ddlType.SelectedValue = "Without Tab";
            ddlType.Enabled = false;
        }

        txtDescription.Text = tblHealthPackagesDetails.Description;
        txtNote.Text = tblHealthPackagesDetails.Note;

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddlType.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg = "Choose Type ,";
        }
        if (ddlType.SelectedValue == "Tab")
        {
            if (txtTab.Text == "" && txtTab.Text.Equals(""))
            {
                IsOk = false;
                msg += "Tab Name ,";
            }
        }
        //if (txtPackagePrice.Text == "" && txtPackagePrice.Text.Equals(""))
        //{
        //    IsOk = false;
        //    msg += "Package Price ,";
        //}
        //if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        //{
        //    IsOk = false;
        //    msg += "Tittle ,";
        //}
        if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        {
            IsOk = false;
            msg += "Description ,";
        }
        //if (txtNote.Text == "" && txtNote.Text.Equals(""))
        //{
        //    IsOk = false;
        //    msg += "Note ";
        //}
        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;
               
        //if (txtPackagePrice.Text == "" && txtPackagePrice.Text.Equals(""))
        //{
        //    IsOk = false;
        //    msg += "Package Price ,";
        //}
        //if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        //{
        //    IsOk = false;
        //    msg += "Tittle ,";
        //}
        if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        {
            IsOk = false;
            msg += "Description ,";
        }
        //if (txtNote.Text == "" && txtNote.Text.Equals(""))
        //{
        //    IsOk = false;
        //    msg += "Note ";
        //}
        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("health_checkup.aspx");
    }

    public void TittleName()
    {
        HealthCheckup tbl = new HealthCheckup();
        tbl.Health_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.GetBYHealthCheckup(tbl);

        lblTittle.Text = tbl.Package_Name;
    }
}