﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_media_press : AdminPage
{
    CMSBAL cmsbal = new CMSBAL();
    MediaPress tblMediaPress = new MediaPress();
    Utility utility = new Utility();

    string MainFile = string.Empty;
    string ext = string.Empty;
    string VirtualFile = "~/uploads/media_press/";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindYear();
            BindMonth();
            BindGrid();

            ddlYear.Items.Insert(0, "-- Select --");
            ddlMonth.Items.Insert(0, "-- Select --");
        }
    }

    public void BindYear()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.BindYear();

        if (dt.Rows.Count > 0)
        {
            ddlYear.DataSource = dt;
            ddlYear.DataValueField = "YEAR_ID";
            ddlYear.DataTextField = "YEAR_NAME";
            ddlYear.DataBind();
        }
    }

    public void BindMonth()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.BindMonth();
        if (dt.Rows.Count > 0)
        {
            ddlMonth.DataSource = dt;
            ddlMonth.DataValueField = "YEARNAME_ID";
            ddlMonth.DataTextField = "YEAR_WISENAME";
            ddlMonth.DataBind();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {

        if (fileuploadPdfFile.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileuploadPdfFile.FileName);
            if (!utility.IsValidPDFFileExtension(ext))
            {
                MyMessageBox1.ShowWarning("Allow only Pdf File.");
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "pdfMedia", ext, this, false);
            fileuploadPdfFile.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblMediaPress.PdfFile = MainFile + ext;
        }


        tblMediaPress.YEAR_ID = int.Parse(ddlYear.SelectedValue.ToString());
        tblMediaPress.YEARNAME_ID = int.Parse(ddlMonth.SelectedValue.ToString());
        tblMediaPress.DESCRIPTION = txtDescription.Text.Trim();
        tblMediaPress.Publication = ddlPublication.SelectedValue;
        tblMediaPress.Video = txtVideo.Text.Trim();

        cmsbal.SaveMediaPress(tblMediaPress);

        if (HttpContext.Current.Session["SavePressMedia"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully");
        }
    }

    public void UpdateRecord()
    {
        if (fileuploadPdfFile.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileuploadPdfFile.FileName);
            if (!utility.IsValidPDFFileExtension(ext))
            {
                MyMessageBox1.ShowWarning("Allow only Pdf File.");
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "pdfMedia", ext, this, false);
            fileuploadPdfFile.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblMediaPress.PdfFile = MainFile + ext;
        }


        tblMediaPress.MEDIA_ID = int.Parse(hdnId.Value.ToString());
        tblMediaPress.YEAR_ID = int.Parse(ddlYear.SelectedValue.ToString());       
        tblMediaPress.YEARNAME_ID = int.Parse(ddlMonth.SelectedValue.ToString());
        tblMediaPress.DESCRIPTION = txtDescription.Text.Trim();
        tblMediaPress.Publication = ddlPublication.SelectedValue;
        tblMediaPress.Video = txtVideo.Text.Trim();

        cmsbal.UpdateMediaPress(tblMediaPress);
        if (HttpContext.Current.Session["UpdateMediaPress"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetMediaPress();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
            GetDetails();
        }
    }

    public void Reset()
    {
        ddlYear.SelectedIndex = -1;
        ddlMonth.SelectedIndex = -1;
        txtDescription.Text = string.Empty;
        btnSave.Text = "Save";
        hyplnk.Visible = false;
        ddlPublication.SelectedIndex = -1;
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblMediaPress.MEDIA_ID = id;
        cmsbal.DeleteMediaPress(tblMediaPress);
        if (HttpContext.Current.Session["DeleteMediaPress"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else 
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblMediaPress.MEDIA_ID = id;
        cmsbal.GETByMediaPressID(tblMediaPress);

        hdnId.Value = Convert.ToInt32(tblMediaPress.MEDIA_ID).ToString();
        ddlYear.SelectedValue = Convert.ToInt32(tblMediaPress.YEAR_ID).ToString();
        ddlMonth.SelectedValue = Convert.ToInt32(tblMediaPress.YEARNAME_ID).ToString();

        ddlYear.SelectedItem.Text = tblMediaPress.Year;
        ddlMonth.SelectedItem.Text = tblMediaPress.Month;
        txtDescription.Text = tblMediaPress.DESCRIPTION;

        ddlPublication.SelectedValue = tblMediaPress.Publication;

        hyplnk.Visible = true;
        hyplnk.NavigateUrl = string.Format("../uploads/media_press/{0}", tblMediaPress.PdfFile).ToString();

        txtVideo.Text = tblMediaPress.Video;
                
        btnSave.Text = "Update";
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddlYear.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg = " Select Year ,";
        }

        if (ddlMonth.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg += " Select Month ,";
        }

        if (ddlPublication.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg += " Select Publication ,";
        }

        if (txtDescription.Text.Trim() == "" && txtDescription.Text.Equals(""))
        {
            IsOk = false;
            msg += " Tittle ,";
        }

        if (ddlPublication.SelectedValue == "Video Gallery")
        {
            if (txtVideo.Text.Trim() == "")
            {
                IsOk = false;
                msg += " Video Url ,";
            }
        }
        else
        {
            if (!fileuploadPdfFile.HasFile)
            {
                IsOk = false;
                msg += " Pdf File ";
            }
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }
        

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddlYear.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg = " Select Year ,";
        }

        if (ddlMonth.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg += " Select Month ,";
        }

        if (ddlPublication.SelectedValue == "-- Select --")
        {
            IsOk = false;
            msg += " Select Publication ,";
        }

        if (txtDescription.Text.Trim() == "" && txtDescription.Text.Equals(""))
        {
            IsOk = false;
            msg += " Tittle ";
        }
        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void rbdActive_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((RadioButton)sender).NamingContainer);
        HiddenField id = (HiddenField)row.Cells[0].FindControl("hdid");
        string value = id.Value;
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Press"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "SHOW_HEADING");
            cmd.Parameters.AddWithValue("@MEDIA_ID", value);
            if (utility.Execute(cmd))
            {
                BindGrid();
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "javascript:alert('Record Showing SuccessFully');", true);
            }
        }
    }

    protected void rbdInActive_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((RadioButton)sender).NamingContainer);
        HiddenField id = (HiddenField)row.Cells[0].FindControl("hdid");
        string value = id.Value;
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Press"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "HIDE_HEADING");
            cmd.Parameters.AddWithValue("@MEDIA_ID", value);
            if (utility.Execute(cmd))
            {
                BindGrid();
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "javascript:alert('Record Hide SuccessFully');", true);
            }
        }
    }

    public void GetDetails()
    {
        foreach (GridViewRow row in gdView.Rows)
        {
            //Find the Radio button control
            RadioButton rb = (RadioButton)row.FindControl("rbdActive");
            RadioButton rb1 = (RadioButton)row.FindControl("rbdInActive");
            string value = rb.Text;
            if (rb.Checked == true)
            {
                rb.ForeColor = System.Drawing.Color.Green;
            }
            if (rb1.Checked == true)
            {
                rb1.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

}