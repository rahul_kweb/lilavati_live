﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="Job-Applied.aspx.cs" Inherits="Job_Applied" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Job Applied
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging"
        OnRowDeleting="gdView_RowDeleting">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="JOBAPP_ID_BINT" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="POSTION_NAME_VCR" HeaderText="Position" />
            <asp:BoundField DataField="SPEC_NAME_VCR" HeaderText="Department" />
            <asp:BoundField DataField="FIRST_NAME_VCR" HeaderText="Name" />
            <%--  <asp:BoundField DataField="LAST_NAME_VCR" HeaderText="Surname" />--%>
            <%--  <asp:BoundField DataField="DOB" HeaderText="DOB" />--%>
            <%-- <asp:BoundField DataField="GENDER" HeaderText="Gender" />--%>
            <%--<asp:BoundField DataField="EMAILID" HeaderText="Email" />--%>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                    <a id="anchor" runat="server" href='<%#Eval("EMAILID","mailto:{0}") %>'>
                        <%#Eval("EMAILID") %></a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PHONE" HeaderText="Contact No." />
            <asp:BoundField DataField="RESIDENT_OF_VCR" HeaderText="Residance" />
            <asp:TemplateField HeaderText="Resume">
                <ItemTemplate>
                    <asp:HyperLink ID="hyprlinkresume" runat="server" Text="View Resume" NavigateUrl='<%#Eval("FILE_NAME_VCR","Resume/{0}") %>'
                        Target="_blank" Style="cursor: pointer; text-decoration: none;"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
           <asp:TemplateField>
                <ItemTemplate>
                    <a href='Job_Posting_Details.aspx?<%#Eval("JOBAPP_ID_BINT","Id={0}") %>'>View Details</a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>
