﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CME : AdminPage
{
    CMSBAL cmdbal = new CMSBAL();
    CME tblcme = new CME();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveData();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateData();
            }
        }
    }

    public void SaveData()
    {
        tblcme.EventName = txtEventName.Text.Trim();

        cmdbal.SaveCME(tblcme);

        if (HttpContext.Current.Session["SaveCME"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Nt insert successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmdbal.GetCME();

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }

    }

    public void Reset()
    {
        txtEventName.Text = string.Empty;
        btnSave.Text = "Save";

        btnSave.Visible = false;
        btncancel.Visible = false;
    }

    public void UpdateData()
    {       

        tblcme.Cme_Id = int.Parse(hdnId.Value);
        tblcme.EventName = txtEventName.Text.Trim();
        cmdbal.UpdateCME(tblcme);

        if (HttpContext.Current.Session["UpdateCME"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Nt update successfully");
        }
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblcme.Cme_Id = id;
        cmdbal.DeleteCME(tblcme);

        if (HttpContext.Current.Session["DeleteCME"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Nt delete successfully");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        btnSave.Visible = true;
        btncancel.Visible = true;

        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblcme.Cme_Id = id;

        cmdbal.GetByIdCME(tblcme);

        hdnId.Value = Convert.ToInt32(tblcme.Cme_Id).ToString();
        txtEventName.Text = tblcme.EventName;
        btnSave.Text = "Update";

    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtEventName.Text == "" && txtEventName.Text.Equals(""))
        {
            IsOk = false;
            msg = "Enter Event Name";
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowWarning(msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtEventName.Text == "" && txtEventName.Text.Equals(""))
        {
            IsOk = false;
            msg = "Enter Event Name";
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowWarning(msg);
        }
        return IsOk;
    }
       
}