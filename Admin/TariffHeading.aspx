﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="TariffHeading.aspx.cs" Inherits="Admin_TariffHeading" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Tariff Heading</div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <div id="divheading" runat="server" visible="false">
            <label class="control-label">
                Heading<span class="required">*</span> :
            </label>
            <asp:TextBox ID="txtheading" runat="server" CssClass="textbox" Enabled="false" Style="cursor: not-allowed;
                background-color: #eee;"></asp:TextBox>
            <br />
        </div>
        <label class="control-label">
            Discription<span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtdisc" runat="server" CssClass="textbox"></CKEditor:CKEditorControl>
        <br />
        <br />
        <asp:Button ID="btnsave" runat="server" Text="Update" CssClass="button" OnClick="btnsave_Click"
            Visible="false" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" 
            Visible="false" onclick="btncancel_Click" />
    </div>
    <br />
    <br />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="10" AllowPaging="true" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="HEADING_ID" HeaderText="ID" />
                <asp:BoundField DataField="HEADING" HeaderText="Heading" />
                <%--<asp:BoundField DataField="DISCRIPTION" HeaderText="Category" />--%>
                <asp:TemplateField HeaderText="Discription">
                    <ItemTemplate>
                        <asp:Literal ID="content" runat="server" Text='<%#Eval("DISCRIPTION") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <a href='Tariff.aspx?<%#Eval("Heading_Id","Id={0}") %>'>Add Tariff</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="false">
                    <ItemTemplate>
                        <asp:RadioButton ID="rbdActive" runat="server" Text="Show" Checked='<%# Eval("STATUS").ToString().Equals("True") %>'
                            GroupName="radio" OnCheckedChanged="rbdActive_CheckedChanged" AutoPostBack="true" />
                        <asp:RadioButton ID="rbdInActive" runat="server" Text="Hide" Checked='<%# Eval("STATUS").ToString().Equals("False") %>'
                            GroupName="radio" OnCheckedChanged="rbdInActive_CheckedChanged" AutoPostBack="true" />
                        <asp:HiddenField ID="hdid" runat="server" Value='<%#Eval("HEADING_ID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.jpeg"
                            Height="41px" margin-top="11px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.jpeg"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
