﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Admin_Login : System.Web.UI.Page
{
    Utility Utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private bool CheckPage()
    {
        if (txtAdminUserName.Text.Trim() == "")
        {
            return false;
        }

        if (txtAdminPassword.Text.Trim() == "")
        {
            return false;
        }

        return true;
    }
    protected void btnAdminLogin_Click(object sender, EventArgs e)
    {
        if (CheckPage())
        {
            using (SqlCommand cmd = new SqlCommand("Proc_ADMIN"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "GET_FOR_LOGIN");
                cmd.Parameters.AddWithValue("@USER_NAME", txtAdminUserName.Text);
                cmd.Parameters.AddWithValue("@PASSWORD", txtAdminPassword.Text);
                cmd.Parameters.AddWithValue("@ADMIN_ID", ddllogintype.SelectedValue);
                
                DataTable dTable = new DataTable();
                dTable = Utility.Display(cmd);
                if (dTable != null && dTable.Rows.Count > 0)
                {
                    lblStatus.Visible = false;
                    Session[AppKey.SESSION_ADMIN_USERID_KEY] = dTable.Rows[0]["Admin_Id"].ToString();
                    Session[AppKey.SESSION_ADMIN_USERNAME_KEY] = dTable.Rows[0]["USER_NAME"].ToString();
                    Session[AppKey.Admin_Login_Type] = dTable.Rows[0]["AdminType"].ToString();
                   // string check = Session[AppKey.Admin_Login_Type].ToString();

                    if (ddllogintype.SelectedValue == "1" && Session[AppKey.Admin_Login_Type].ToString() == "#Admin")
                    {
                        Response.Redirect("~/admin/index.aspx");
                    }

                    else if (ddllogintype.SelectedValue == "2" && Session[AppKey.Admin_Login_Type].ToString() == "#appointmentadmin")
                    {
                        Response.Redirect("~/admin/AppointmentHistory.aspx");
                    }
                    else
                    {
                        lblStatus.Text = "Invalid Admin";
                        lblStatus.Visible = true;
                    }
                }
                else
                {
                    lblStatus.Text = "Invalid username or password";
                    lblStatus.Visible = true;
                }
            }
        }
    }
}