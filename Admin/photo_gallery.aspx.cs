﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_photo_gallery : AdminPage
{
    PhotoGallery tblPhotoGallery = new PhotoGallery();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualImage = "~/uploads/gallery/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            UpdateRecord();
        }
    }

    public void SaveRecord()
    {
        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Image Files");
                return;
            }            

            MainImage = utility.GetUniqueName(VirtualImage, "imgGallery", ext, this, false);
            fileUploadImage.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblPhotoGallery.Images = MainImage + ext;                       
        }

        tblPhotoGallery.Tittle = txtTittle.Text.Trim();
        cmsbal.SavePhotoGallery(tblPhotoGallery);

        if (HttpContext.Current.Session["SavePhotoGallery"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully");
        }

    }

    public void UpdateRecord()
    {
        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Image Files");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgGallery", ext, this, false);
            fileUploadImage.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblPhotoGallery.Images = MainImage + ext;           
        }

        tblPhotoGallery.ID = int.Parse(hdnId.Value);
        tblPhotoGallery.Tittle = txtTittle.Text.Trim();

        cmsbal.UpdatePhotoGallery(tblPhotoGallery);

        if (HttpContext.Current.Session["UpdatePhotoGallery"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetPhotoGallery();

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblPhotoGallery.ID = id;

        cmsbal.DeletePhotoGallery(tblPhotoGallery);
        if (HttpContext.Current.Session["DeletePhotoGallery"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }

    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblPhotoGallery.ID = id;

        cmsbal.GetBYPhotoGallery(tblPhotoGallery);
        hdnId.Value = Convert.ToInt32(tblPhotoGallery.ID).ToString();
        imgpreview.Visible = true;
        imgpreview.ImageUrl = string.Format("../uploads/gallery/{0}", tblPhotoGallery.Images).ToString();

        txtTittle.Text = tblPhotoGallery.Tittle;

        btnSave.Text = "Update";

    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Reset();
    }

    public void Reset()
    {
        imgpreview.Visible = false;
        btnSave.Text = "Save";
        txtTittle.Text = string.Empty;
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (!fileUploadImage.HasFile)
        {
            msg = "Choose Image";
            IsOk = false;
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }
}