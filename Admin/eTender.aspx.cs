﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class eTender : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["Id"] != null)
            {
                BindData(Request.QueryString["Id"]);
            }
        }

    }

    public void BindData(string Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_eTender 'getbyid','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            hdnId.Value = dt.Rows[0]["eId"].ToString();
            txteTenderNo.Text = dt.Rows[0]["eTenderNo"].ToString();
            txtTenderName.Text = dt.Rows[0]["TenderName"].ToString();
            txtStartDateTime.Text = dt.Rows[0]["TenderStartDateTime"].ToString();
            txtEndDateTime.Text = dt.Rows[0]["TenderEndDateTime"].ToString();
            ddlTypeOfTender.SelectedValue = dt.Rows[0]["TypeOfTender"].ToString();
            txtLastTenderReceiveDateTime.Text = dt.Rows[0]["LastTenderReceiveDateTime"].ToString();
            txtTendeOpeningDateTime.Text = dt.Rows[0]["TenderOpeningDateTime"].ToString();
            ddlDepartment.SelectedValue = dt.Rows[0]["Department"].ToString();
            ddlCategory.SelectedValue = dt.Rows[0]["Category"].ToString();
            txtTenderDescription.Text = dt.Rows[0]["TenderDescription"].ToString();
            txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            chkIsActive.Checked = Boolean.Parse(dt.Rows[0]["Status"].ToString());

            btnSave.Text = "Update";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (CheckValidation())
        {
            if (btnSave.Text == "Submit")
            {
                using (SqlCommand cmd = new SqlCommand("Proc_eTender"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "Add");
                    cmd.Parameters.AddWithValue("@TenderStartDateTime", DateTime.ParseExact(txtStartDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@TenderEndDateTime", DateTime.ParseExact(txtEndDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@TenderName", txtTenderName.Text.Trim());
                    cmd.Parameters.AddWithValue("@TypeOfTender", ddlTypeOfTender.SelectedValue);
                    cmd.Parameters.AddWithValue("@LastTenderReceiveDateTime", DateTime.ParseExact(txtLastTenderReceiveDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@TenderOpeningDateTime", DateTime.ParseExact(txtTendeOpeningDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@Department", ddlDepartment.SelectedValue);
                    cmd.Parameters.AddWithValue("@Category", ddlCategory.SelectedValue);
                    cmd.Parameters.AddWithValue("@TenderDescription", txtTenderDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                    cmd.Parameters.AddWithValue("@Status", chkIsActive.Checked);
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        MyMessageBox1.ShowSuccess("Record inserted successfully.");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Record could not insert successfully.");

                    }
                }
            }
            else
            {
                using (SqlCommand cmd = new SqlCommand("Proc_eTender"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "Update");
                    cmd.Parameters.AddWithValue("@eId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@TenderStartDateTime", DateTime.ParseExact(txtStartDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@TenderEndDateTime", DateTime.ParseExact(txtEndDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@TenderName", txtTenderName.Text.Trim());
                    cmd.Parameters.AddWithValue("@TypeOfTender", ddlTypeOfTender.SelectedValue);
                    cmd.Parameters.AddWithValue("@LastTenderReceiveDateTime", DateTime.ParseExact(txtLastTenderReceiveDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@TenderOpeningDateTime", DateTime.ParseExact(txtTendeOpeningDateTime.Text, "dd-MM-yyyy hh:mm tt", null).ToString("yyyy-MM-dd hh:mm:ss tt"));
                    cmd.Parameters.AddWithValue("@Department", ddlDepartment.SelectedValue);
                    cmd.Parameters.AddWithValue("@Category", ddlCategory.SelectedValue);
                    cmd.Parameters.AddWithValue("@TenderDescription", txtTenderDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                    cmd.Parameters.AddWithValue("@Status", chkIsActive.Checked);
                    if (utility.Execute(cmd))
                    {
                        //Reset();
                        MyMessageBox1.ShowSuccess("Record updated successfully.");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Record could not updated successfully.");

                    }
                }
            }
        }
    }

    public bool CheckValidation()
    {
        bool IsoK = true;
        try
        {

            string msg = "";
            if (txtTenderName.Text == string.Empty)
            {
                msg += "Tender Name, ";
                IsoK = false;
            }
            if (txtStartDateTime.Text == string.Empty)
            {
                msg += " Start Date & Time, ";
                IsoK = false;
            }
            if (txtEndDateTime.Text == string.Empty)
            {
                msg += " End Date & Time, ";
                IsoK = false;
            }
            if (ddlTypeOfTender.SelectedValue.Equals("0"))
            {
                msg += " Type of Tender, ";
                IsoK = false;
            }
            if (txtLastTenderReceiveDateTime.Text == string.Empty)
            {
                msg += " Last Tender Receive Date & Time, ";
                IsoK = false;
            }
            if (txtTendeOpeningDateTime.Text == string.Empty)
            {
                msg += " Tender Opening Date & Time, ";
                IsoK = false;
            }
            if (ddlDepartment.SelectedValue.Equals("0"))
            {
                msg += " Select Department, ";
                IsoK = false;
            }
            if (ddlCategory.SelectedValue.Equals("0"))
            {
                msg += " Select Category ";
                IsoK = false;
            }
            //if (txtTenderDescription.Text == string.Empty)
            //{
            //    msg += " Tender Description, ";
            //    IsoK = false;
            //}
            //if (txtRemarks.Text == string.Empty)
            //{
            //    msg += " Remarks";
            //    IsoK = false;
            //}
            if (!IsoK)
            {
                MyMessageBox1.ShowError("Please Fill the follwoing Details <br />" + msg);
            }

        }
        catch (Exception ex)
        {

            this.Title = ex.Message;
        }
        return IsoK;
    }

    public void Reset()
    {
        txtTenderName.Text = string.Empty;
        txtStartDateTime.Text = string.Empty;
        txtEndDateTime.Text = string.Empty;
        ddlTypeOfTender.SelectedIndex = -1;
        txtLastTenderReceiveDateTime.Text = string.Empty;
        txtTendeOpeningDateTime.Text = string.Empty;
        ddlDepartment.SelectedIndex = -1;
        ddlCategory.SelectedIndex = -1;
        txtTenderDescription.Text = string.Empty;
        txtRemarks.Text = string.Empty;

        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void btnbacktolist_Click(object sender, EventArgs e)
    {
        Response.Redirect("eTender_list.aspx");
    }
}