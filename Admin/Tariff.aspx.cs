﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_Tariff : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnId.Value = Request.QueryString["Id"];
        if (!IsPostBack)
        {          
            BindGrid();
            Discription();
            BindHEandigandDesc();
        }
    }   

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            if (CheckSave())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Tariff"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "ADD");
                    cmd.Parameters.AddWithValue("@HEADING_ID", hdnId.Value);
                    cmd.Parameters.AddWithValue("@HEADING", txtheadind.Text.Trim());
                    cmd.Parameters.AddWithValue("@NAME", txtname.Text.Trim());
                    cmd.Parameters.AddWithValue("@TYPE", ddtype.SelectedValue);
                    cmd.Parameters.AddWithValue("@ROOM", txtroommemb.Text.Trim());
                    if (txtroomtariff.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@ROOM_TARIFF", txtroomtariff.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ROOM_TARIFF", "&nbsp;");
                    }
                    if (txtmealcharges.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@MEAL_CHARGES", txtmealcharges.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@MEAL_CHARGES", "&nbsp;");
                    }
                    if (txtalltaxes.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@ALL_TAXES", txtalltaxes.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ALL_TAXES", "&nbsp;");
                    }
                    if (txttotal.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@TOTAL", txttotal.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@TOTAL", "&nbsp;");
                    }
                    if (utility.Execute(cmd))
                    {
                        BindGrid();
                        Reset();
                        MyMessageBox1.ShowSuccess("Record insert successfully");
                    }
                    else
                    {
                        MyMessageBox1.ShowError("Record Could Not insert successfully");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Tariff"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                    cmd.Parameters.AddWithValue("@TARIFF_ID", hdnforedit.Value);
                    cmd.Parameters.AddWithValue("@HEADING_ID", hdnId.Value);
                    cmd.Parameters.AddWithValue("@TYPE", ddtype.SelectedValue);
                    cmd.Parameters.AddWithValue("@NAME", txtname.Text.Trim());
                    cmd.Parameters.AddWithValue("@ROOM", txtroommemb.Text.Trim());
                    if (txtroomtariff.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@ROOM_TARIFF", txtroomtariff.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ROOM_TARIFF", "&nbsp;");
                    }
                    if (txtmealcharges.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@MEAL_CHARGES", txtmealcharges.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@MEAL_CHARGES", "&nbsp;");
                    }
                    if (txtalltaxes.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@ALL_TAXES", txtalltaxes.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ALL_TAXES", "&nbsp;");
                    }
                    if (txttotal.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@TOTAL", txttotal.Text.Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@TOTAL", "&nbsp;");
                    }
                    if (utility.Execute(cmd))
                    {
                        BindGrid();
                        Reset();
                        MyMessageBox1.ShowSuccess("Record update successfully");
                    }
                    else
                    {
                        MyMessageBox1.ShowError("Record Could Not update successfully");
                    }
                }
            }
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Tariff 'GET'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.Columns[1].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
            gdView.Columns[1].Visible = false;
        }
    }
    public void Reset()
    {
        //txtheadind.Text = string.Empty;
        txtname.Text = string.Empty;
        txtroommemb.Text = string.Empty;
        txtroomtariff.Text = string.Empty;
        txtmealcharges.Text = string.Empty;
        txtalltaxes.Text = string.Empty;
        txttotal.Text = string.Empty;
        btnsave.Text = "Save";
        ddtype.SelectedIndex = -1;
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        using (SqlCommand cmd = new SqlCommand("Proc_Tariff"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@TARIFF_ID", id);
            if (utility.Execute(cmd))
            {
                BindGrid();
                Reset();
                MyMessageBox1.ShowSuccess("Record delete successfully");
            }
            else
            {
                MyMessageBox1.ShowError("Record Could Not delete successfully");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Tariff 'GET_BY_ID','" + id + "'");
        if (dt.Rows.Count > 0)
        {
            hdnforedit.Value = dt.Rows[0]["TARIFF_ID"].ToString();
            txtheadind.Text = dt.Rows[0]["HEADING"].ToString();
            txtdesc.Text = dt.Rows[0]["DISCRIPTION"].ToString();
            txtname.Text = dt.Rows[0]["NAME"].ToString();
            ddtype.SelectedValue = dt.Rows[0]["TYPE"].ToString();
            txtroommemb.Text = dt.Rows[0]["ROOM"].ToString();
            txtroomtariff.Text = dt.Rows[0]["ROOM_TARIFF"].ToString();
            txtmealcharges.Text = dt.Rows[0]["MEAL_CHARGES"].ToString();
            txtalltaxes.Text = dt.Rows[0]["ALL_TAXES"].ToString();
            txttotal.Text = dt.Rows[0]["TOTAL"].ToString();
            Discription();
            btnsave.Text = "Update";
        }
    }
    
    public void Discription()
    {
        string ddhead = string.Empty;
        ddhead = txtheadind.Text;
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Tariff_Heading 'SHOW_DISCRIPTION',0,'" + ddhead + "'");
        if (dt.Rows.Count > 0)
        {
            string storedescription = string.Empty;
            storedescription =dt.Rows[0]["DISCRIPTION"].ToString(); 
                
              

            //while (storedescription.Contains("<p>") && storedescription.Contains("</p>"))
            storedescription = storedescription.Replace("<p>", "");
            storedescription = storedescription.Replace("\r\n\t", "");
            storedescription = storedescription.Replace("</p>", "");
            storedescription = storedescription.Replace("<br />", "");

            txtdesc.Text = storedescription;
           
        }
        
    }

    public void BindHEandigandDesc()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Tariff_Heading 'GET_BY_ID','" + hdnId.Value + "'");
        if (dt.Rows.Count > 0)
        {
            txtheadind.Text = dt.Rows[0]["HEADING"].ToString();
            txtdesc.Text = dt.Rows[0]["DISCRIPTION"].ToString();
            Discription();
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("TariffHeading.aspx");
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddtype.SelectedValue == "Select")
        {
            msg += "Select Type ,";
            IsOk = false;
        }
        if (txtname.Text == "" && txtname.Text.Equals(""))
        {
            msg += " Name ,";
            IsOk = false;
        }
        if (txtroommemb.Text == "" && txtroommemb.Text.Equals(""))
        {
            msg += " Room Member ";
            IsOk = false;
        }
        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Detail<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddtype.SelectedValue == "Select")
        {
            msg += "Select Type ,";
            IsOk = false;
        }
        if (txtname.Text == "" && txtname.Text.Equals(""))
        {
            msg += " Name ,";
            IsOk = false;
        }
        if (txtroommemb.Text == "" && txtroommemb.Text.Equals(""))
        {
            msg += " Room Member ";
            IsOk = false;
        }
        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Detail<br>" + msg);
        }
        return IsOk;
    }
}