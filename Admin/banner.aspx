﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="banner.aspx.cs" Inherits="Admin_photo_gallery" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>

    <script>

        $(function () {
            $('.minus').click(function () {
                var Order = parseInt($('#<%=txtOrder.ClientID%>').val());
                Order = (Order - 1);
                if (!isNaN(Order) && Order > 0) {
                    $('#<%=txtOrder.ClientID%>').val(Order);
                } else {
                    $('#<%=txtOrder.ClientID%>').val(0);
                }
            });

            $('.plus').click(function () {
                var Order = parseInt($('#<%=txtOrder.ClientID%>').val());
                Order = (Order + 1);

                $('#<%=txtOrder.ClientID%>').val(Order);
            })
        })

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Home Banner
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Image<span class="required">*</span> :    
             <br />
            (Recommended image size for Home Image - Width 1020px X Height 600px)         
        </label>
        <asp:FileUpload ID="fileUploadImage" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:Image ID="imgpreview" runat="server" Width="300" Height="200" Visible="false" />
        <br />
        <label class="control-label">
            Order No.<span class="required">*</span> :
        </label>
        <input type="button" class="minus" value="-" />
        <asp:TextBox ID="txtOrder" runat="server" CssClass="textbox1" Text="0" onKeyPress='javascript:return onlyNumbers();' MaxLength="2" Style="width: 20px;"></asp:TextBox>
        <input type="button" class="plus" value="+" />
        <br />
        <label class="control-label">
            Page Url :
        </label>
        <asp:TextBox ID="txtPageUrl" runat="server" CssClass="textbox"></asp:TextBox>
        <label class="control-label">
            Show/Hide :
        </label>
        <asp:CheckBox ID="chkPublish" runat="server" CssClass="multi-select-check-list" Checked="true"></asp:CheckBox>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btnClear" runat="server" Text="Cancel" CssClass="button" OnClick="btnClear_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="10" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Home Banner">
                <ItemTemplate>
                    <img src='<%#Eval("IMAGE","../uploads/home_banner/{0}") %>' width="300" height="200"></img>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ORDER_NO" HeaderText="Order No" />
            <asp:BoundField DataField="Page_Url" HeaderText="Page Url" />
            <asp:TemplateField HeaderText="Show/Hide">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSHowHide" runat="server" ToolTip='<%#Eval("ID") %>' Checked='<%#Eval("PUBLISH").ToString()=="True"?true:false %>' OnCheckedChanged="chkSHowHide_CheckedChanged" AutoPostBack="true" style="text-align:center;" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

