﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="CME_details.aspx.cs" Inherits="Admin_CME_details" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
    <script type="text/javascript" src="../js/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".doa").datepicker({
                changeMonth: true,
                changeYear: false,
                dateFormat: 'dd-mm-yy'
            });
        });

        function RedirFun() {
            window.location.assign("CME.aspx");
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        <asp:Label ID="lblheading" runat="server"></asp:Label>
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 400px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Date & Month<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDateAndMonth" runat="server" CssClass="textbox doa" onkeyPress="javascript:return(false)"></asp:TextBox>
        <br />
        <label class="control-label">
            Topic<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTopic" runat="server" CssClass="textbox" TextMode="MultiLine" Height="50"></asp:TextBox>
        <br />
        <label class="control-label">
            Department<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDepartment" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click"/>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" OnClientClick="javascript:return RedirFun();" />

    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EVENTNAME" HeaderText="Event Name" />
            <asp:BoundField DataField="DATEANDMONTH" HeaderText="Date & Month" />
            <asp:BoundField DataField="TOPIC" HeaderText="Topic" />
            <asp:BoundField DataField="DEPARTMENT" HeaderText="Department" />
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

