﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="AppointmentDetails.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">

    <div class="page-header">
        Appointment Details
    </div>
    <div class="form-box" style="width: 500px;">
        <table class="gridtable">
            <tr>
                <td colspan="2">
                    <h1>Welcome
                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                    </h1>
                </td>
            </tr>
            <tr>
                <td>Speciality</td>
                <td>
                    <asp:Label ID="lblSpeciality" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Doctor Name
                </td>
                <td>
                    <asp:Label ID="lblDoctorName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Available Timing
                </td>
                <td>
                    <asp:Label ID="lblAvailableTiming" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Appointment Date
                </td>
                <td>
                    <asp:Label ID="lblAppointmentDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Visit Time</td>
                <td>
                    <asp:Label ID="lblVisitTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Title</td>
                <td>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Sure Name</td>
                <td>
                    <asp:Label ID="lblSureName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>First Name</td>
                <td>
                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email ID</td>
                <td>
                    <asp:Label ID="lblEmailID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Hospital LHNO</td>
                <td>
                    <asp:Label ID="lblHospitalLHNO" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>LHNO</td>
                <td>
                    <asp:Label ID="lblLHNO" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Mobile No</td>
                <td>
                    <asp:Label ID="lblMobileNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Telephone Residence</td>
                <td>
                    <asp:Label ID="lblTelephoneResi" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Sex</td>
                <td>
                    <asp:Label ID="lblSex" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Date Of Birth</td>
                <td>
                    <asp:Label ID="lblDOB" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td>Blood Group</td>
                <td>
                    <asp:Label ID="lblBloodGroup" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Marital Satus</td>
                <td>
                    <asp:Label ID="lblMaritalStatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Religion</td>
                <td>
                    <asp:Label ID="lblReligion" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Occupation</td>
                <td>
                    <asp:Label ID="lblOccupation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Address</td>
                <td>
                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>




</asp:Content>

