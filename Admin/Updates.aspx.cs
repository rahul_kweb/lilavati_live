﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Updates : System.Web.UI.Page
{
    Updates tblUpdates = new Updates();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string ext = string.Empty;
    string MainFile = string.Empty;
    string VirtualFile = "~/uploads/updates/";   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {

        tblUpdates.Tittle = txtTittle.Text.Trim();
        tblUpdates.Content = txtContent.Text.Trim();
               
        ext = System.IO.Path.GetExtension(fileUploadImages.FileName);
        if (ext != "")
        {
            if (!utility.IsValidImageFileExtension(ext))
            {
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "Img", ext, this, false);
            fileUploadImages.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblUpdates.Images = MainFile + ext;
        }

        ext = Path.GetExtension(fileUploadPdfFile.FileName);
        if (ext != "")
        {
            if (!utility.IsValidPDFFileExtension(ext))
            {
                return;
            }
            MainFile = utility.GetUniqueName(VirtualFile, "Pdf", ext, this, false);
            fileUploadPdfFile.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblUpdates.Pdf = MainFile + ext;
        }


        ext = Path.GetExtension(fileUploadDocFile.FileName);
        if (ext != "")
        {
            if (!utility.IsValidResumeFileExtension(ext))
            {
                return;
            }
            MainFile = utility.GetUniqueName(VirtualFile, "Resume", ext, this, false);
            fileUploadDocFile.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblUpdates.Doc = MainFile + ext;
        }
      
        cmsbal.SaveUpdatesPage(tblUpdates);

        if (HttpContext.Current.Session["SaveUpdatesPage"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully");
        }

    }

    public void UpdateRecord()
    {

        tblUpdates.Tittle = txtTittle.Text.Trim();
        tblUpdates.Content = txtContent.Text.Trim();

        ext = System.IO.Path.GetExtension(fileUploadImages.FileName);
        if (ext != "")
        {
            if (!utility.IsValidImageFileExtension(ext))
            {
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "Img", ext, this, false);
            fileUploadImages.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblUpdates.Images = MainFile + ext;
        }

        ext = Path.GetExtension(fileUploadPdfFile.FileName);
        if (ext != "")
        {
            if (!utility.IsValidPDFFileExtension(ext))
            {
                return;
            }
            MainFile = utility.GetUniqueName(VirtualFile, "Pdf", ext, this, false);
            fileUploadPdfFile.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblUpdates.Pdf = MainFile + ext;
        }


        ext = Path.GetExtension(fileUploadDocFile.FileName);
        if (ext != "")
        {
            if (!utility.IsValidResumeFileExtension(ext))
            {
                return;
            }
            MainFile = utility.GetUniqueName(VirtualFile, "Resume", ext, this, false);
            fileUploadDocFile.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblUpdates.Doc = MainFile + ext;
        }

        tblUpdates.Id = Convert.ToInt32(hdnId.Value);
        cmsbal.UpdateUpdatesPage(tblUpdates);

        if (HttpContext.Current.Session["UpdateUpdatesPage"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record Update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not Update successfully");
        }

    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetUpdatesPage();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
    }

    public void Reset()
    {
        txtTittle.Text = string.Empty;
        txtContent.Text = string.Empty;
        imgpreview.Visible = false;
        hyplnkPdf.Visible = false;
        hyplnkDoc.Visible = false;
        btnSave.Text = "Save";
    }

    //HttpFileCollection hfc = Request.Files;
    //  string val = hfc.AllKeys[0];

    //  foreach (var FileName in hfc.AllKeys)
    //  {

    //      for (int i = 0; i < hfc.Count; i++)
    //      {
    //          HttpPostedFile hpf = hfc[i];

    //          if (FileName == "ctl00$mainContentPlaceHolder$fileUploadImages" && strImagesName == "")
    //          {
    //              if (hpf.ContentLength > 0)
    //              {

    //                  hpf.SaveAs(Server.MapPath("~/uploads/updates/" + hpf.FileName));                     

    //                  strImages += hpf.FileName + ",";
    //              }

    //          }

    //          if (FileName == "ctl00$mainContentPlaceHolder$fileUploadPdfFile" && strPdfName == "")
    //          {
    //              if (hpf.ContentLength > 0)
    //              {

    //                  hpf.SaveAs(Server.MapPath("~/uploads/updates/" + hpf.FileName));

    //                  strPdf += hpf.FileName + ",";
    //              }
    //          }

    //          if (FileName == "ctl00$mainContentPlaceHolder$fileUploadDocFile")
    //          {

    //          }
    //      }
    //      strImagesName = "first";
    //      strPdfName = "Second";
    //  }

    //  strImages = strImages.Substring(0, strImages.Length - 1);
    //  tblUpdates.Images = strImages;

    //  strPdf = strPdf.Substring(0, strPdf.Length - 1);
    //  tblUpdates.Pdf = strPdf;

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblUpdates.Id = id;

        cmsbal.DeleteUpdatesPage(tblUpdates);
        if (HttpContext.Current.Session["DeleteUpdatesPage"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }

    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int ID = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblUpdates.Id = ID;

        cmsbal.GetByUpdatesPage(tblUpdates);
        hdnId.Value = Convert.ToInt32(tblUpdates.Id).ToString();
        txtTittle.Text = tblUpdates.Tittle;
        txtContent.Text = tblUpdates.Content;

        if (tblUpdates.Images != "")
        {
            imgpreview.Visible = true;
            imgpreview.ImageUrl = string.Format("../uploads/updates/{0}", tblUpdates.Images);
        }

        if (tblUpdates.Pdf != "")
        {
            hyplnkPdf.Visible = true;
            hyplnkPdf.NavigateUrl = string.Format("../uploads/updates/{0}", tblUpdates.Pdf);
        }

        if (tblUpdates.Doc != "")
        {
            hyplnkDoc.Visible = true;
            hyplnkDoc.NavigateUrl = string.Format("../uploads/updates/{0}", tblUpdates.Doc);
        }

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtTittle.Text.Trim() == "" && txtTittle.Text.Equals(""))
        {
            msg = "Tittle ";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            MyMessageBox1.ShowError("Fill Details <br>" + msg);
        }

        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtTittle.Text.Trim() == "" && txtTittle.Text.Equals(""))
        {
            msg = "Tittle ";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            MyMessageBox1.ShowError("Fill Details <br>" + msg);
        }

        return IsOk;
    }

}