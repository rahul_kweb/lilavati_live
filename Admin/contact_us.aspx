﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="Admin_contact_us" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Contact - US
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            How To Reach US <span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtAddress" runat="server" CssClass="textbox" TextMode="MultiLine" Height="80px"></asp:TextBox>
        <br />
        <label class="control-label">
            Emergency/ Casualty<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtEmergencyCasulty" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Admission Department<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtAdmissionDepartment" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Ambulance<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtAmbulance" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Appointment - OPD<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtAppointmentOPD" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Billing - Inpatient<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtBillingInpatient" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Billing - OPD<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtBillingOPD" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Blood Bank<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtBloodBank" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Cardiology<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtCardiology" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            CT Scan Department<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtCTScanDepartment" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Dental<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDental" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Dermatology<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDermatology" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            EMG/ EEG<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtEMG_EEG" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Endoscopy<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtEndoscopy" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            ENT/ Audiometry<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtENT_Audiometry" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Health Check-up Department<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtHealthCheckupDepartment" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Hospital Board Line<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtHospitalBoardLine" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Hospital Fax<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtHospitalFax" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            IVF<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtIVF" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            MRI Department<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtMRIDepartment" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Nuclear Medicine<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtNuclear_Medicine" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Ophthalmology<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtOphthalmology" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Physiotherapy<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtPhysiotherapy" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Report Dispatch Counter<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtReportDispatchCounter" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Sample Collection Room<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtSample_collection_room" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            TPA Cell<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTPACell" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            X-Ray, Sonography Department<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtXRaySonographyDepartment" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Visa Section<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtVisa_Section" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Email Address<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="textbox"></asp:TextBox>
        <label class="control-label">
            Toll Free <span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTollFree" runat="server" CssClass="textbox"></asp:TextBox>

        <br />
        <br />
        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="button" OnClick="btnUpdate_Click" />
    </div>
</asp:Content>

