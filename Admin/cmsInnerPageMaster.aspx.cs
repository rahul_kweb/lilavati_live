﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Job_Area : AdminPage
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        string Id = Request.QueryString["Id"].ToString();
        if (!IsPostBack)
        {
            BindPageName();
            BindGrid(Id);

            ddlPageName.Items.Insert(0, "-- Select Page --");
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void BindPageName()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetPageNAmeCMSInnerPageMaster();

        if (dt.Rows.Count > 0)
        {
            ddlPageName.DataSource = dt;
            ddlPageName.DataTextField = "PAGE_NAME";
            ddlPageName.DataValueField = "PAGE_ID";
            ddlPageName.DataBind();
        }
    }

    public void SaveRecord()
    {
        //tblcmsInnerPageMaster.PageId = int.Parse(ddlPageName.SelectedValue.ToString());
        tblcmsInnerPageMaster.PageId = int.Parse(Request.QueryString["Id"].ToString());
        tblcmsInnerPageMaster.Tab = txtTab.Text.Trim();
        tblcmsInnerPageMaster.Description = txtDescription.Text.Trim();

        cmsbal.SaveCMSInnerPageMaster(tblcmsInnerPageMaster);

        if (HttpContext.Current.Session["SaveCMSInnerPageMaster"] == "Success")
        {
            string Id = Request.QueryString["Id"].ToString();
            BindGrid(Id);
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        tblcmsInnerPageMaster.InnerId = Convert.ToInt32(hdnId.Value);
        //tblcmsInnerPageMaster.PageId = Convert.ToInt32(ddlPageName.SelectedValue);
        tblcmsInnerPageMaster.PageId = int.Parse(Request.QueryString["Id"].ToString());
        tblcmsInnerPageMaster.Tab = txtTab.Text.Trim();
        tblcmsInnerPageMaster.Description = txtDescription.Text.Trim();

        cmsbal.UpdateCMSInnerPageMaster(tblcmsInnerPageMaster);

        if (HttpContext.Current.Session["UpdateCmsInnerPageMAster"] == "Success")
        {
            string Id = Request.QueryString["Id"].ToString();
            BindGrid(Id);
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully.");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully.");
        }
    }

    public void BindGrid(string Id)
    {
        DataTable dt = new DataTable();
        tblcmsInnerPageMaster.PageId = int.Parse(Id.ToString());
        dt = cmsbal.GetCMSInnerPageMaster(tblcmsInnerPageMaster);
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;

            lblTittle.Text = dt.Rows[0]["PAGE_NAME"].ToString();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        ddlPageName.SelectedIndex = -1;
        txtTab.Text = string.Empty;
        txtDescription.Text = string.Empty;
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblcmsInnerPageMaster.InnerId = id;
        cmsbal.DeleteCMSInnerPageMaster(tblcmsInnerPageMaster);

        if (HttpContext.Current.Session["DeleteCmsInnerPageMAster"] == "Success")
        {
            string Id = Request.QueryString["Id"].ToString();
            BindGrid(Id);
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblcmsInnerPageMaster.InnerId = id;
        cmsbal.GetBYCmsInnerPageMaster(tblcmsInnerPageMaster);

        hdnId.Value = Convert.ToInt32(tblcmsInnerPageMaster.InnerId).ToString();
      //ddlPageName.SelectedValue = Convert.ToInt32(tblcmsInnerPageMaster.PageId).ToString();
      //ddlPageName.SelectedItem.Text = tblcmsInnerPageMaster.PageName.ToString();
        txtTab.Text = tblcmsInnerPageMaster.Tab;
        txtDescription.Text = tblcmsInnerPageMaster.Description;

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        //if (ddlPageName.SelectedValue == "-- Select Page --")
        //{
        //    msg = "Select Page Name ,";
        //    IsOk = false;
        //}

        if (txtTab.Text == "" && txtTab.Text.Equals(""))
        {
            msg += "Tab ,";
            IsOk = false;
        }

        if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        {
            msg += "Description ,";
            IsOk = false;
        }


        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        //if (ddlPageName.SelectedValue == "-- Select Page --")
        //{
        //    msg = "Select Page Name ,";
        //    IsOk = false;
        //}

        if (txtTab.Text == "" && txtTab.Text.Equals(""))
        {
            msg += "Tab ,";
            IsOk = false;
        }

        if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        {
            msg += "Description ,";
            IsOk = false;
        }


        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        string Id = Request.QueryString["Id"].ToString();
        BindGrid(Id);
    }
}