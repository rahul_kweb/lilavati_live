﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


partial class Admin_Appointment : AdminPage
{
    RegistrationTable tbl = new RegistrationTable();
    AppointmentTable apptbl = new AppointmentTable();
    BAL bal = new BAL();
    Utility utility = new Utility();
    string attachmentname = string.Empty;
    Dictionary<string, string> sendata = new Dictionary<string, string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAppointmentGrid();
        }
    }

    public void BindAppointmentGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_PCipopRegistrationMaster 'PUSH_NOTIFICATION_GET'");
        if (dt.Rows.Count > 0)
        {

            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {

        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindAppointmentGrid();
    }


    protected void btnSend_Click(object sender, EventArgs e)
    {
        string CheckedVal = string.Empty;
        sendata.Add("notitype", "1");
        foreach (GridViewRow row in gdView.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox c = (CheckBox)row.FindControl("CheckBox1");
                bool cbValue = c.Checked;
                if (cbValue == true)
                {
                    //CheckedVal += c.ToolTip + ",";

                    DataTable dt = new DataTable();
                    dt = utility.Display("select * from GAadAddressMaster where AddressIndex = '" + c.ToolTip + "'");
                    if (dt.Rows.Count > 0)
                    {
                        string deviceId = dt.Rows[0]["DeviceId"].ToString();
                        string deviceType = dt.Rows[0]["DeviceType"].ToString();
                        string message = txtMessage.Text;
                        string url = string.Empty;

                        if (deviceType == "A")
                        {

                            using (SqlCommand cmd = new SqlCommand("Proc_HealthTips"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@PARA", "ADD");
                                cmd.Parameters.AddWithValue("@AddressIndex", c.ToolTip);
                                cmd.Parameters.AddWithValue("@Message", message);
                                utility.Execute(cmd);

                            }

                            url = "http://kwebmakerdigitalagency.com/Lilavati/androidnotification.php";
                            //url = "https://9c728cec.ngrok.io/Lilavati_Hospital_live/notification/androidnotification.php";
                            //url = "http://www.mflcard.in/android/NewPHP/mfl_android_notification.php";
                            //url = "http://kwebmakerdigitalagency.com/arvind/notification.php";
                        }
                        else
                        {

                            using (SqlCommand cmd = new SqlCommand("Proc_HealthTips"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@PARA", "ADD");
                                cmd.Parameters.AddWithValue("@AddressIndex", c.ToolTip);
                                cmd.Parameters.AddWithValue("@Message", message);
                                utility.Execute(cmd);

                            }

                            url = "http://kwebmakerdigitalagency.com/Lilavati/iosnotification.php";
                            //url = "http://kwebmakerdigitalagency.com/arvind/notification.php";
                            //url = "http://www.mflcard.in/android/NewPHP/mfl_android_notification.php";
                        }

                        senddata(url, deviceId, message);
                    }
                    else
                    { 
                        
                    }
                }
            }
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(),
        "alert",
        "alert('Health Tips sent sucessfully.');window.location ='PushNotification.aspx';",
        true);

        //if (CheckedVal != "")
        //{
        //    CheckedVal = CheckedVal.Substring(0, CheckedVal.Length - 1);
        //}
    }

    private void senddata(string url, string deviceId, string message)
    {
        try
        {
            using (var client = new WebClient())
            {
                var postData = new System.Collections.Specialized.NameValueCollection();
                postData.Add("message", message);
                postData.Add("DeviceId", deviceId);
                string data = JsonConvert.SerializeObject(sendata);
                //data = data.Replace(@"\", string.Empty);
                //postData.Add("sendata", data.Trim());
                byte[] response = client.UploadValues(url, "POST", postData);
                var responsebody = Encoding.UTF8.GetString(response);
            }

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;

        }


    }

    public string HtmlResult { get; set; }
}