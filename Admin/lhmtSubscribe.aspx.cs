﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Registration : AdminPage
{
    LHMTSubscribe tblLHMTSubscribe = new LHMTSubscribe();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetLHMTSubscribe();

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblLHMTSubscribe.Id = int.Parse(Id.ToString());
        cmsbal.DeleteLHMTSubscribe(tblLHMTSubscribe);

        if (HttpContext.Current.Session["DeleteLHMTSubscribe"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Record deleted Successfully.");
            BindGrid();
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not delete Successfully.");
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void btnexporttoexcel_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = cmsbal.GetLHMTSubscribe();

            if (dt.Rows.Count > 0)
            {

                DataTable dtOriginal = new DataTable();
                dtOriginal = dt; //Return Table consisting data

                //Create Tempory Table
                DataTable dtTemp = new DataTable();

                //Creating Header Row
                dtTemp.Columns.Add("<b>Sr.No</b>");
                dtTemp.Columns.Add("<b>Name</b>");
                dtTemp.Columns.Add("<b>Email Id</b>");
                dtTemp.Columns.Add("<b>Contact No</b>");               


                DataRow drAddItem;
                for (int i = 0; i < dtOriginal.Rows.Count; i++)
                {
                    drAddItem = dtTemp.NewRow();

                    //drAddItem[0] = dtOriginal.Rows[i][0].ToString();//SrId
                    drAddItem[0] = i + 1;
                    drAddItem[1] = dtOriginal.Rows[i][1].ToString();//Name
                    drAddItem[2] = dtOriginal.Rows[i][2].ToString();//Email
                    drAddItem[3] = dtOriginal.Rows[i][3].ToString();//Contact No                   

                    dtTemp.Rows.Add(drAddItem);

                }

                //Temp Grid
                DataGrid dg = new DataGrid();
                dg.DataSource = dtTemp;
                dg.DataBind();
                string Filename = "LHMTSubscribe" + "_" + DateTime.Now.ToString("dd_MMM_yyyy") + ".xls";
                ExportToExcel(Filename, dg);
                dg = null;
                dg.Dispose();

            }
            else
            {

            }
        }
        catch (Exception)
        {
            
            throw;
        }
       
    }

    private void ExportToExcel(string strFileName, DataGrid dg)
    {
        Response.ClearContent();
        Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        dg.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

}