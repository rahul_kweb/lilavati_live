﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Job_Applied : AdminPage
{
    Career crobj = new Career();
    BAL bal = new BAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            BindGrid();

            
        }
    }

    public void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = bal.BindGridViewRecord();

            if (dt.Rows.Count > 0)
            {
                gdView.DataSource = dt;
                gdView.Columns[0].Visible = true;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
           // throw;
        }
       
    }

    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    if (btnSave.Text == "Save")
    //    {
    //        if (CheckSave())
    //        {
    //            SaveRecord();
    //        }
    //    }
    //    else
    //    {
    //        if (CheckUpdate())
    //        {
    //            UpdateRecord();
    //        }
    //    }
    //}

    //public void SaveRecord()
    //{
    //    tblJobPosting.Job_Id = int.Parse(ddlJobArea.SelectedValue.ToString());
    //    tblJobPosting.Position = txtPosition.Text.Trim();
    //    tblJobPosting.Specification = txtSpecification.Text.Trim();        
    //    tblJobPosting.Duration = DateTime.ParseExact(txtDuration.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
    //    tblJobPosting.Vacancy = txtVacancy.Text.Trim();
    //    cmsbal.SaveJobPosting(tblJobPosting);

    //    if (HttpContext.Current.Session["SaveJobPosting"] == "Success")
    //    {
    //        BindGrid();
    //        Reset();
    //        MyMessageBox1.ShowSuccess("Record insert successfully");
    //    }
    //    else
    //    {
    //        MyMessageBox1.ShowError("Record Could Not insert successfully");
    //    }
    //}

    //public void UpdateRecord()
    //{
    //    tblJobPosting.Post_Id = int.Parse(hdnId.Value.ToString());
    //    tblJobPosting.Job_Id = int.Parse(ddlJobArea.SelectedValue.ToString());
    //    tblJobPosting.Position = txtPosition.Text.Trim();
    //    tblJobPosting.Specification = txtSpecification.Text.Trim();
    //    //tblJobPosting.Duration = txtDuration.Text.Trim();
    //    tblJobPosting.Duration = DateTime.ParseExact(txtDuration.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
    //    tblJobPosting.Vacancy = txtVacancy.Text.Trim();
    //    cmsbal.UpdateJobPosting(tblJobPosting);

    //    if (HttpContext.Current.Session["UpdateJobPosting"] == "Success")
    //    {
    //        BindGrid();
    //        Reset();
    //        MyMessageBox1.ShowSuccess("Record update successfully");
    //    }
    //    else
    //    {
    //        MyMessageBox1.ShowError("Record Could Not update successfully");
    //    }

    //}

    //public void BindGrid()
    //{
    //    DataTable dt = new DataTable();
    //    dt = cmsbal.GetJobPosting();
    //    if (dt.Rows.Count > 0)
    //    {
    //        gdView.Columns[0].Visible = true;
    //        gdView.DataSource = dt;
    //        gdView.DataBind();
    //        gdView.Columns[0].Visible = false;
    //    }
    //}

    //public void Reset()
    //{
    //    ddlJobArea.SelectedIndex = -1;
    //    txtPosition.Text = string.Empty;
    //    txtSpecification.Text = string.Empty;
    //    txtDuration.Text = string.Empty;
    //    txtVacancy.Text = string.Empty;

    //    btnSave.Text = "Save";
    //}

    //protected void btncancel_Click(object sender, EventArgs e)
    //{
    //    Reset();
    //}

    //protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
    //    tblJobPosting.Post_Id = id;
    //    cmsbal.DeleteJobPosting(tblJobPosting);

    //    if (HttpContext.Current.Session["DeleteJobPosting"] == "Success")
    //    {
    //        BindGrid();
    //        MyMessageBox1.ShowSuccess("Record delete successfully.");
    //    }
    //    else
    //    {
    //        MyMessageBox1.ShowError("Record Could Not delete successfully.");
    //    }

    //}

    //protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
    //    tblJobPosting.Post_Id = id;
    //    cmsbal.GetByJobPostingId(tblJobPosting);

    //    hdnId.Value = Convert.ToInt32(tblJobPosting.Post_Id).ToString();
    //    ddlJobArea.SelectedValue = Convert.ToInt32(tblJobPosting.Job_Id).ToString();
    //    txtPosition.Text = tblJobPosting.Position;
    //    txtSpecification.Text = tblJobPosting.Specification;
    //    txtDuration.Text = tblJobPosting.Duration;
    //    txtVacancy.Text = tblJobPosting.Vacancy;

    //    btnSave.Text = "Update";

    //}

    //protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    gdView.PageIndex = e.NewPageIndex;
    //    BindGrid();
    //}

    //public bool CheckSave()
    //{
    //    bool IsOk = true;
    //    string msg = string.Empty;

    //    if (ddlJobArea.SelectedValue == "-- Select --")
    //    {
    //        msg = "Select Job Area ,";
    //        IsOk = false;
    //    }

    //    if (txtPosition.Text == "" && txtPosition.Text.Equals(""))
    //    {
    //        msg += "Position ,";
    //        IsOk = false;
    //    }

    //    if (txtSpecification.Text == "" && txtSpecification.Text.Equals(""))
    //    {
    //        msg += "Specification ,";
    //        IsOk = false;
    //    }

    //    if (txtVacancy.Text == "" && txtVacancy.Text.Equals(""))
    //    {
    //        msg += "Vacancy ,";
    //        IsOk = false;
    //    }

    //    if (txtDuration.Text == "" && txtDuration.Text.Equals(""))
    //    {
    //        msg += "Duration ,";
    //        IsOk = false;
    //    }

    //    if (msg.Length > 0)
    //    {
    //        msg = msg.Substring(0, msg.Length - 1);
    //    }

    //    if (!IsOk)
    //    {
    //        MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
    //    }
    //    return IsOk;
    //}

    //public bool CheckUpdate()
    //{
    //    bool IsOk = true;
    //    string msg = string.Empty;

    //    if (ddlJobArea.SelectedValue == "-- Select --")
    //    {
    //        msg = "Select Job Area ,";
    //        IsOk = false;
    //    }

    //    if (txtPosition.Text == "" && txtPosition.Text.Equals(""))
    //    {
    //        msg += "Position ,";
    //        IsOk = false;
    //    }

    //    if (txtSpecification.Text == "" && txtSpecification.Text.Equals(""))
    //    {
    //        msg += "Specification ,";
    //        IsOk = false;
    //    }

    //    if (txtVacancy.Text == "" && txtVacancy.Text.Equals(""))
    //    {
    //        msg += "Vacancy ,";
    //        IsOk = false;
    //    }


    //    if (txtDuration.Text == "" && txtDuration.Text.Equals(""))
    //    {
    //        msg += "Duration ,";
    //        IsOk = false;
    //    }

    //    if (msg.Length > 0)
    //    {
    //        msg = msg.Substring(0, msg.Length - 1);
    //    }

    //    if (!IsOk)
    //    {
    //        MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
    //    }
    //    return IsOk;
    //}

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        crobj._JobId = id;
        string result = string.Empty;
        result = bal.DeleteGridViewRecord(crobj);

        if (result == "1")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record Delete Successfully!!!");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could not Delete Successfully!!!");
        }

    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}