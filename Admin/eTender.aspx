﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="eTender.aspx.cs" Inherits="eTender" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        E - Tender
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            E Tender No. :
        </label>
        <asp:TextBox ID="txteTenderNo" CssClass="textbox" runat="server" Width="373px" autocomplete="off" ReadOnly="true" BackColor="#ece5e5" />
        <br />

        <label class="control-label">
            Tender Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTenderName" CssClass="textbox" runat="server" Width="373px" autocomplete="off" />
        <br />

        <label class="control-label">
            Tender Start Date & Time<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtStartDateTime" CssClass="textbox" runat="server" Width="373px" onKeyPress='javascript:return onlyNumbers();' autocomplete="off" />
        <br />

        <label class="control-label">
            Tender End Date & Time<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtEndDateTime" CssClass="textbox" runat="server" Width="373px" onKeyPress='javascript:return onlyNumbers();' autocomplete="off" />
        <br />

        <label class="control-label">
            Type of Tender<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlTypeOfTender" runat="server" Width="373px" CssClass="dropdown">
            <asp:ListItem Value="0">---- Type of Tender ----</asp:ListItem>
            <asp:ListItem Value="One Time">One Time</asp:ListItem>
            <asp:ListItem Value="Rate Contract">Rate Contract</asp:ListItem>
            <asp:ListItem Value="AMC/CMC/Hire">AMC/CMC/Hire</asp:ListItem>
            <asp:ListItem Value="Manpower">Manpower</asp:ListItem>
            <asp:ListItem Value="Others">Others</asp:ListItem>
        </asp:DropDownList>
        <br />

        <label class="control-label">
            Last Tender Receive Date & Time<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtLastTenderReceiveDateTime" CssClass="textbox" runat="server" Width="373px" onKeyPress='javascript:return onlyNumbers();' autocomplete="off" />
        <br />

        <label class="control-label">
            Tender Opening Date & Time<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTendeOpeningDateTime" CssClass="textbox" runat="server" Width="373px" onKeyPress='javascript:return onlyNumbers();' autocomplete="off" />
        <br />

        <label class="control-label">
            Department<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlDepartment" runat="server" Width="373px" CssClass="dropdown">
            <asp:ListItem Value="0">---- Select Department ----</asp:ListItem>
            <asp:ListItem Value="Material (Non-Surgical) Department">Material (Non-Surgical) Department</asp:ListItem>
        </asp:DropDownList>
        <br />

        <label class="control-label">
            Category<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlCategory" runat="server" Width="373px" CssClass="dropdown">
            <asp:ListItem Value="0">---- Select Category ----</asp:ListItem>
            <asp:ListItem Value="Bio Medical Equipment">Bio Medical Equipment</asp:ListItem>
            <asp:ListItem Value="Bio Medical Spare parts">Bio Medical Spare parts</asp:ListItem>
            <asp:ListItem Value="Bio Medical Consumable">Bio Medical Consumable</asp:ListItem>
            <asp:ListItem Value="Bio Medical AMC/CMC">Bio Medical AMC/CMC</asp:ListItem>
            <asp:ListItem Value="Engineering Equipment">Engineering Equipment</asp:ListItem>
            <asp:ListItem Value="Engineering Spare parts">Engineering Spare parts</asp:ListItem>
            <asp:ListItem Value="Engineering Consumable">Engineering Consumable</asp:ListItem>
            <asp:ListItem Value="Engineering AMC/CMC">Engineering AMC/CMC</asp:ListItem>
            <asp:ListItem Value="Engineering Services">Engineering Services</asp:ListItem>
            <asp:ListItem Value="IT Equipment">IT Equipment</asp:ListItem>
            <asp:ListItem Value="IT Software">IT Software</asp:ListItem>
            <asp:ListItem Value="IT Consumable">IT Consumable</asp:ListItem>
            <asp:ListItem Value="IT AMC/CMC">IT AMC/CMC</asp:ListItem>
            <asp:ListItem Value="IT Services">IT Services</asp:ListItem>
            <asp:ListItem Value="Other Equipment">Other Equipment</asp:ListItem>
            <asp:ListItem Value="Printing and stationery">Printing and stationery</asp:ListItem>
            <asp:ListItem Value="Food Raw material">Food Raw material</asp:ListItem>
            <asp:ListItem Value="Soft Drink & Beverages">Soft Drink & Beverages</asp:ListItem>
        </asp:DropDownList>
        <br />

        <label class="control-label">
            Tender Description :
        </label>
        <asp:TextBox ID="txtTenderDescription" CssClass="textbox" runat="server" TextMode="MultiLine" Width="373px" autocomplete="off" Height="80px" />
        <br />

        <label class="control-label">
            Remarks :
        </label>
        <asp:TextBox ID="txtRemarks" CssClass="textbox" runat="server" TextMode="MultiLine" Width="373px" autocomplete="off" Height="80px" />
        <br />

        <label class="control-label">
            IsActive :
        </label>
        <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" autocomplete="off" style="margin:-15px 0px 0px 70px" />
        <br />
        <br />
        <asp:Button ID="btnSave" Text="Submit" CssClass="button" runat="server" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" Text="Clear" CssClass="button" runat="server" Visible="true" OnClick="btncancel_Click" />
        <asp:Button ID="btnbacktolist" Text="Back to list" CssClass="button" runat="server" OnClick="btnbacktolist_Click" />
    </div>

    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js"></script>
    <link rel="stylesheet" href="https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css" />

    <style>
        input[type="checkbox" i]{
            margin:-15px 0px 0px 0px !important;
        }
    </style>
    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function onlyAmount(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                return false;
            }
            else {
                return true;
            }
        }

        $(function () {

            var startDateTextBox = $("#<%=txtStartDateTime.ClientID%>");
            var endDateTextBox = $("#<%=txtEndDateTime.ClientID%>");

            startDateTextBox.datetimepicker({
                changeMonth: true,
                changeYear: true,
                //yearRange: "c-100:c",
                yearRange: "c-100:c+1",
                dateFormat: 'dd-mm-yy',
                timeFormat: 'hh:mm tt',                
                onClose: function (dateText, inst) {
                    $("#EndDateTime").addClass("populated");
                    if (endDateTextBox.val() != '') {
                        var testStartDate = startDateTextBox.datetimepicker('getDate');
                        var testEndDate = endDateTextBox.datetimepicker('getDate');
                        if (testStartDate > testEndDate)
                            endDateTextBox.datetimepicker('setDate', testStartDate);
                    }
                    else {
                        endDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime) {
                    endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate'));
                }
            });
            endDateTextBox.datetimepicker({
                changeMonth: true,
                changeYear: true,
                //yearRange: "c-100:c",
                yearRange: "c-100:c+1",
                dateFormat: 'dd-mm-yy',
                timeFormat: 'hh:mm tt',                
                onClose: function (dateText, inst) {
                    if (startDateTextBox.val() != '') {
                        var testStartDate = startDateTextBox.datetimepicker('getDate');
                        var testEndDate = endDateTextBox.datetimepicker('getDate');
                        if (testStartDate > testEndDate)
                            startDateTextBox.datetimepicker('setDate', testEndDate);
                    }
                    else {
                        startDateTextBox.val(dateText);
                    }
                },
                onSelect: function (selectedDateTime) {
                    startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate'));
                }
            });

            $("#<%=txtLastTenderReceiveDateTime.ClientID%>").datetimepicker({
                changeMonth: true,
                changeYear: true,
                //yearRange: "c-100:c",
                yearRange: "c-100:c+1",
                dateFormat: 'dd-mm-yy',
                timeFormat: 'hh:mm tt'
            });

            $("#<%=txtTendeOpeningDateTime.ClientID%>").datetimepicker({
                changeMonth: true,
                changeYear: true,
                //yearRange: "c-100:c",
                yearRange: "c-100:c+1",
                dateFormat: 'dd-mm-yy',
                timeFormat: 'hh:mm tt'
            });         

        });
    </script>
</asp:Content>

