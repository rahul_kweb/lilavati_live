﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class cms : AdminPage
{
    CmsMaster cmstbl = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnid.Value=Request.QueryString["Id"].ToString();
        if (!IsPostBack)
        {
            BindRecord(hdnid.Value);
        }
    }

    public void BindRecord(string ID)
    {
        cmstbl.ID = int.Parse(ID.ToString());
        cmsbal.GETBYIDContent(cmstbl);
        lblheading.Text = cmstbl.Heading;
        txtdesc.Text = cmstbl.Description;
        lblTittle.Text = cmstbl.Heading;
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        cmstbl.ID = int.Parse(hdnid.Value);
        cmstbl.Description = txtdesc.Text.Trim();
        cmsbal.UpdateContent(cmstbl);

        if (HttpContext.Current.Session["UpdateCms"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Record Update Successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not Update Successfully");
        }
    }
}