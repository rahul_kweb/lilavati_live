﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_contact_us : AdminPage
{
    CMSBAL cmsbal = new CMSBAL();
    ContactUs tblContactUs = new ContactUs();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRecord();
        }
    }

    public void BindRecord()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetContactUS(tblContactUs);

        txtAddress.Text = tblContactUs.Address;
        txtEmergencyCasulty.Text = tblContactUs.Emergency_Casualty;
        txtAmbulance.Text = tblContactUs.Ambulance;
        txtHospitalBoardLine.Text = tblContactUs.Hospital_Board_Line;
        txtHospitalFax.Text = tblContactUs.Hospital_Fax;
        txtAdmissionDepartment.Text = tblContactUs.Admission_Department;
        txtTPACell.Text = tblContactUs.TPA_Cell;
        txtBillingInpatient.Text = tblContactUs.Billing_Inpatient;
        txtBillingOPD.Text = tblContactUs.Billing_OPD;
        txtAppointmentOPD.Text = tblContactUs.Appointment_OPD;
        txtBloodBank.Text = tblContactUs.Blood_Bank;
        txtHealthCheckupDepartment.Text = tblContactUs.Health_Check_up_Department;
        txtReportDispatchCounter.Text = tblContactUs.Report_Dispatch_Counter;
        txtMRIDepartment.Text = tblContactUs.MRI_Department;
        txtXRaySonographyDepartment.Text = tblContactUs.X_Ray_Sonography_Department;
        txtCTScanDepartment.Text = tblContactUs.CT_Scan_Department;
        txtEmailAddress.Text = tblContactUs.Email_Address;

        txtNuclear_Medicine.Text = tblContactUs.Nuclear_Medicine;
        txtPhysiotherapy.Text = tblContactUs.Physiotherapy;
        txtDental.Text = tblContactUs.Dental;
        txtDermatology.Text = tblContactUs.Dermatology;
        txtOphthalmology.Text = tblContactUs.Ophthalmology;
        txtENT_Audiometry.Text = tblContactUs.ENT_Audiometry;
        txtIVF.Text = tblContactUs.IVF;
        txtEMG_EEG.Text = tblContactUs.EMG_EEG;
        txtVisa_Section.Text = tblContactUs.Visa_Section;
        txtCardiology.Text = tblContactUs.Cardiology;
        txtSample_collection_room.Text = tblContactUs.Sample_collection_room;
        txtEndoscopy.Text = tblContactUs.Endoscopy;
        txtTollFree.Text = tblContactUs.TollFree;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (CheckSave())
        {
            UpdateRecord();
            BindRecord();
        }
    }

    public void UpdateRecord()
    {
        tblContactUs.Cnt_Id = 1;
        tblContactUs.Address = txtAddress.Text.Trim();
        tblContactUs.Emergency_Casualty = txtEmergencyCasulty.Text.Trim();
        tblContactUs.Ambulance = txtAmbulance.Text.Trim();
        tblContactUs.Hospital_Board_Line = txtHospitalBoardLine.Text.Trim();
        tblContactUs.Hospital_Fax = txtHospitalFax.Text.Trim();
        tblContactUs.Admission_Department = txtAdmissionDepartment.Text.Trim();
        tblContactUs.TPA_Cell = txtTPACell.Text.Trim();
        tblContactUs.Billing_Inpatient = txtBillingInpatient.Text.Trim();
        tblContactUs.Billing_OPD = txtBillingOPD.Text.Trim();
        tblContactUs.Appointment_OPD = txtAppointmentOPD.Text.Trim();
        tblContactUs.Blood_Bank = txtBloodBank.Text.Trim();
        tblContactUs.Health_Check_up_Department = txtHealthCheckupDepartment.Text.Trim();
        tblContactUs.Report_Dispatch_Counter = txtReportDispatchCounter.Text.Trim();
        tblContactUs.MRI_Department = txtMRIDepartment.Text.Trim();
        tblContactUs.X_Ray_Sonography_Department = txtXRaySonographyDepartment.Text.Trim();
        tblContactUs.CT_Scan_Department = txtCTScanDepartment.Text.Trim();
        tblContactUs.Email_Address = txtEmailAddress.Text.Trim();

        tblContactUs.Nuclear_Medicine = txtNuclear_Medicine.Text.Trim();
        tblContactUs.Physiotherapy = txtPhysiotherapy.Text.Trim();
        tblContactUs.Dental = txtDental.Text.Trim();
        tblContactUs.Dermatology = txtDermatology.Text.Trim();
        tblContactUs.Ophthalmology = txtOphthalmology.Text.Trim();
        tblContactUs.ENT_Audiometry = txtENT_Audiometry.Text.Trim();
        tblContactUs.IVF = txtIVF.Text.Trim();
        tblContactUs.EMG_EEG = txtEMG_EEG.Text.Trim();
        tblContactUs.Visa_Section = txtVisa_Section.Text.Trim();
        tblContactUs.Cardiology = txtCardiology.Text.Trim();
        tblContactUs.Sample_collection_room = txtSample_collection_room.Text.Trim();
        tblContactUs.Endoscopy = txtEndoscopy.Text.Trim();
        tblContactUs.TollFree = txtTollFree.Text.Trim();


        cmsbal.UpdateContactUs(tblContactUs);

        if (HttpContext.Current.Session["UpdateContactUs"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not update successfully");
        }
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtAddress.Text == "" && txtAddress.Text.Equals(""))
        {
            msg = "How To Reach Us ,";
            IsOk = false;
        }
        if (txtEmergencyCasulty.Text == "" && txtEmergencyCasulty.Text.Equals(""))
        {
            msg += "Emergency/ Casualty ,";
            IsOk = false;
        }
        if (txtAmbulance.Text == "" && txtAmbulance.Text.Equals(""))
        {
            msg += "Ambulance ,";
            IsOk = false;
        }
        if (txtHospitalBoardLine.Text == "" && txtHospitalBoardLine.Text.Equals(""))
        {
            msg += "Hospital Board Line ,";
            IsOk = false;
        }
        if (txtHospitalFax.Text == "" && txtHospitalFax.Text.Equals(""))
        {
            msg += "Hospital Fax ,";
            IsOk = false;
        }
        if (txtAdmissionDepartment.Text == "" && txtAdmissionDepartment.Text.Equals(""))
        {
            msg += "Admission Department ,";
            IsOk = false;
        }
        if (txtTPACell.Text == "" && txtTPACell.Text.Equals(""))
        {
            msg += "TPA Cell ,";
            IsOk = false;
        }
        if (txtBillingInpatient.Text == "" && txtBillingInpatient.Text.Equals(""))
        {
            msg += "Billing - Inpatient ,";
            IsOk = false;
        }
        if (txtBillingOPD.Text == "" && txtBillingOPD.Text.Equals(""))
        {
            msg += "Billing - OPD ,";
            IsOk = false;
        }
        if (txtAppointmentOPD.Text == "" && txtAppointmentOPD.Text.Equals(""))
        {
            msg += "Appointment - OPD ,";
            IsOk = false;
        }
        if (txtBloodBank.Text == "" && txtBloodBank.Text.Equals(""))
        {
            msg += "Blood Bank ,";
            IsOk = false;
        }
        if (txtHealthCheckupDepartment.Text == "" && txtHealthCheckupDepartment.Text.Equals(""))
        {
            msg += "Health Check-up Department ,";
            IsOk = false;
        }
        if (txtReportDispatchCounter.Text == "" && txtReportDispatchCounter.Text.Equals(""))
        {
            msg += "Report Dispatch Counter ,";
            IsOk = false;
        }
        if (txtMRIDepartment.Text == "" && txtMRIDepartment.Text.Equals(""))
        {
            msg += "MRI Department ,";
            IsOk = false;
        }
        if (txtXRaySonographyDepartment.Text == "" && txtXRaySonographyDepartment.Text.Equals(""))
        {
            msg += "X-Ray, Sonography Department ,";
            IsOk = false;
        }
        if (txtCTScanDepartment.Text == "" && txtCTScanDepartment.Text.Equals(""))
        {
            msg += "CT Scan Department ,";
            IsOk = false;
        }
        if (txtEmailAddress.Text == "" && txtEmailAddress.Text.Equals(""))
        {
            msg += "Email Address ,";
            IsOk = false;
        }

        if (txtTollFree.Text == "" && txtTollFree.Text.Equals(""))
        {
            msg += "Toll Free ,";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }


        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }
}