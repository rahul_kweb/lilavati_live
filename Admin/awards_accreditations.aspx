﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="awards_accreditations.aspx.cs" Inherits="Admin_awards_accreditations" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Awards
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Awards Image<span class="required">*</span> :
        </label>
        <asp:FileUpload ID="fileUploadImg" runat="server" CssClass="file-upload" />
        <img id="imgAwards" runat="server" style="width: 110px; height: 110px;" visible="false" />
        <br />
        <label class="control-label">
            Tittle<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTittle" runat="server" CssClass="textbox" TextMode="MultiLine" style="height:70px;"></asp:TextBox>
        <br />
        <label class="control-label">
            Description<span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtDescription" runat="server"></CKEditor:CKEditorControl>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="AWARDS_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Awards Images">
                <ItemTemplate>
                    <img id="idimg" runat="server" src='<%#Eval("AWARDS_IMAGE","../uploads/awards/{0}") %>' alt="" style="height: 110px; width: 110px;" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AWARDS_TITTLE" HeaderText="Tittle" />
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <asp:Literal ID="litDesc" runat="server" Text='<%#Eval("AWARDS_DESCRIPTION") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

