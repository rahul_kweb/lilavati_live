﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddCms : System.Web.UI.Page
{
    CmsMaster cmstbl = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.BindGrid();

        if (dt.Rows.Count > 0)
        {
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (CheckSave())
        {
            cmstbl.Heading = txtHeading.Text.Trim();
            cmstbl.PageName = txtPageName.Text.Trim();
            cmsbal.SaveCMS(cmstbl);
            if (HttpContext.Current.Session["SaveCms"] == "Success")
            {
                MyMessageBox1.ShowSuccess("Record insert Successfully.");
                BindGrid();
                Reset();
            }
            else
            {
                MyMessageBox1.ShowError("Record Could Not insert Successfully.");
            }
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtHeading.Text == "" && txtHeading.Text.Equals(""))
        {
            msg = "Heading ,";
            IsOk = false;
        }
        if (txtPageName.Text == "" && txtPageName.Text.Equals(""))
        {           
            msg += " Page Name ,";
            IsOk = false;
        }
        if (msg != "")
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowWarning("Please Fill Following Details.<br>" + msg);
        }
        return IsOk;
    }

    public void Reset()
    {
        txtHeading.Text = string.Empty;
        txtPageName.Text = string.Empty;
    }
}