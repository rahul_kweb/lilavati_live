﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Job_Area : System.Web.UI.Page
{
    PatientsEducation tblPatientsEducation = new PatientsEducation();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string ext = string.Empty;
    string MainFile = string.Empty;
    string VirtualFile = "~/uploads/brochure/";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        if (fileUploadThumbImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadThumbImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Image.");
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "thumbImg", ext, this, false);
            fileUploadThumbImage.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblPatientsEducation.ThumbImage = MainFile + ext;
        }

        if (fileUploadBrochure.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadBrochure.FileName).ToLower();
            if (!utility.IsValidPDFFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Pdf Files.");
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "pdf", ext, this, false);
            fileUploadBrochure.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblPatientsEducation.Brochure = MainFile + ext;
        }

        tblPatientsEducation.Tittle = txtTittle.Text.Trim();
        cmsbal.SavePatientsEducation(tblPatientsEducation);

        if (HttpContext.Current.Session["SavePatientsEducation"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        if (fileUploadThumbImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadThumbImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Image.");
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "thumbImg", ext, this, false);
            fileUploadThumbImage.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblPatientsEducation.ThumbImage = MainFile + ext;
        }

        if (fileUploadBrochure.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadBrochure.FileName).ToLower();
            if (!utility.IsValidPDFFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Pdf Files.");
                return;
            }

            MainFile = utility.GetUniqueName(VirtualFile, "pdf", ext, this, false);
            fileUploadBrochure.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
            tblPatientsEducation.Brochure = MainFile + ext;
        }

        tblPatientsEducation.Pat_Id = Convert.ToInt32(hdnId.Value);
        tblPatientsEducation.Tittle = txtTittle.Text.Trim();
        cmsbal.UpdatePatientsEducation(tblPatientsEducation);

        if (HttpContext.Current.Session["UpdatePatientsEducation"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully.");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully.");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetPatientsEducation();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtTittle.Text = string.Empty;
        imgPreview.Visible = false;
        txtBrochurePreview.Visible = false;
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblPatientsEducation.Pat_Id = id;
        cmsbal.DeletePatientsEducation(tblPatientsEducation);

        if (HttpContext.Current.Session["DeletePatientsEducation"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblPatientsEducation.Pat_Id = id;
        cmsbal.GetBYPatientsEducation(tblPatientsEducation);

        hdnId.Value = Convert.ToInt32(tblPatientsEducation.Pat_Id).ToString();
        txtTittle.Text = tblPatientsEducation.Tittle;
        imgPreview.Visible = true;
        imgPreview.ImageUrl = string.Format("../uploads/brochure/{0}",tblPatientsEducation.ThumbImage).ToString();
        txtBrochurePreview.Visible = true;
        txtBrochurePreview.Text = tblPatientsEducation.Brochure;

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        {
            msg = "Tittle ,";
            IsOk = false;
        }

        if (!fileUploadThumbImage.HasFile)
        {
            msg += " Thumb Image ,";
            IsOk = false;
        }
        if (!fileUploadBrochure.HasFile)
        {
            msg += " Brochure ,";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        {
            msg = "Tittle ,";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}