﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Appointment_Page.aspx.cs" Inherits="Admin_Appointment" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .viewdetails {
            color: #48524A;
        }

            .viewdetails:hover {
                color: #04869A;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
       Doctor Appointments
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="20" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="App_Id_bint" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Speciality_vcr" HeaderText="Speciality" />
                <asp:BoundField DataField="Doctor_Name_vcr" HeaderText="Doctor Name" />
                <asp:BoundField DataField="App_Time_vcr" HeaderText="App Day & Timming" />
                <asp:BoundField DataField="App_date_dtm" HeaderText="App Date" />
                <asp:BoundField DataField="Patient_Name_vcr" HeaderText="Patient Name" />
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <a href='mailto:<%#Eval("Email_Id_vcr") %>'><%#Eval("Email_Id_vcr") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Mobile_No_vcr" HeaderText="Mobile" />
                <asp:TemplateField HeaderText="View Details">
                    <ItemTemplate>
                        <a href='Appointment_Page_Detail.aspx?<%#Eval("App_Id_bint","AppId={0}") %>' class="viewdetails">View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>


</asp:Content>

