﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Job_Area : System.Web.UI.Page
{
    LHMT tblLHMT = new LHMT();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();

    string ext = string.Empty;
    string MainFile = string.Empty;
    string VirtualFile = "~/uploads/lhmt/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        if (fileUploadBrouchre.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadBrouchre.FileName);
            if (!utility.IsValidPDFFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Pdf File.");
                return;
            }
            else
            {
                MainFile = utility.GetUniqueName(VirtualFile, "lhmt", ext, this, false);
                fileUploadBrouchre.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
                tblLHMT.Brouchre = MainFile + ext;
            }

        }


        tblLHMT.Tittle = txtTittle.Text.Trim();
        cmsbal.SaveLHMT(tblLHMT);

        if (HttpContext.Current.Session["SaveLHMT"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        if (fileUploadBrouchre.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadBrouchre.FileName);
            if (!utility.IsValidPDFFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Pdf File.");
                return;
            }
            else
            {
                MainFile = utility.GetUniqueName(VirtualFile, "lhmt", ext, this, false);
                fileUploadBrouchre.SaveAs(Server.MapPath(VirtualFile + MainFile + ext));
                tblLHMT.Brouchre = MainFile + ext;
            }
        }

        tblLHMT.Id = Convert.ToInt32(hdnId.Value);
        tblLHMT.Tittle = txtTittle.Text.Trim();
        cmsbal.UpdateLHMT(tblLHMT);

        if (HttpContext.Current.Session["UpdateLHMT"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully.");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully.");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetLHMT();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtTittle.Text = string.Empty;
        hyplink.Visible = false;
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblLHMT.Id = id;
        cmsbal.DeleteLHMT(tblLHMT);

        if (HttpContext.Current.Session["DeleteLHMT"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblLHMT.Id = id;
        cmsbal.GetByLHMT(tblLHMT);

        hdnId.Value = Convert.ToInt32(tblLHMT.Id).ToString();
        txtTittle.Text = tblLHMT.Tittle;

        if (tblLHMT.Brouchre != "")
        {
            hyplink.Visible = true;
            hyplink.NavigateUrl = string.Format("../uploads/lhmt/{0}", tblLHMT.Brouchre).ToString();
        }

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        {
            msg = "Tittle ,";
            IsOk = false;
        }

        if (!fileUploadBrouchre.HasFile)
        {
            msg += "Brouchre ,";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtTittle.Text == "" && txtTittle.Text.Equals(""))
        {
            msg = "Tittle ,";
            IsOk = false;
        }


        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}