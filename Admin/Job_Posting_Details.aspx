﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Job_Posting_Details.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<style type="text/css">
        .auto-style1 {
            width: 50%;
            /*border: 1px solid;*/
            margin-left: 275px;
            border: 1px solid black;
            border-collapse: collapse;
        }

      
    </style>--%>

    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">

    <div class="page-header">
        Job Applied Details
    </div>
    <div class="form-box" style="width: 500px;">
        <table class="gridtable">
            <tr>
                <td>Position</td>
                <td>
                    <asp:Label ID="lblPosition" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Department</td>
                <td>
                    <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>First Name</td>
                <td>
                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Date Of Birth</td>
                <td>
                    <asp:Label ID="lblDob" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                    <asp:Label ID="lblGender" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Contact No</td>
                <td>
                    <asp:Label ID="lblContactNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email Id</td>
                <td>
                    <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Resident of</td>
                <td>
                    <asp:Label ID="lblResidentof" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>Presently working location</td>
                <td>
                    <asp:Label ID="lblPresentlyworkinglocation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Relevant Experience in years</td>
                <td>
                    <asp:Label ID="lblRelevantExperience" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Total Experience</td>
                <td>
                    <asp:Label ID="lblTotalExperience" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Monthly expected Salary</td>
                <td>
                    <asp:Label ID="lblMonthlyexpectedSalary" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Qualification
                </td>
                <td>
                    <asp:Label ID="lblQualification" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Hostel Accommodation required
                </td>
                <td>
                    <asp:Label ID="lblHostelAccommodationrequired" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Registration with MMC/ MNC
                </td>
                <td>
                    <asp:Label ID="lblRegistrationwithMMCMNC" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

