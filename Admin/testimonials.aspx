﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="testimonials.aspx.cs" Inherits="Admin_testimonials" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script type="text/javascript">

        $(document).ready(function () {

            $('#<%=txtSearch.ClientID%>').on('keypress', function () {

                var txt = $('#<%=txtSearch.ClientID%>').val();

                $.ajax({
                    type: "POST",
                    url: "testimonials.aspx/SearchTestimonial12",
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    data: '{txt:"' + txt + '"}',
                    success: OnSuccess,
                    error: function (a, b, c) {
                    },
                });
            });



            function OnSuccess(response) {
                //// debugger;
                //var xmlDoc = $.parseXML(response.d);
                //var xml = $(xmlDoc);
                //var customers = xml.find("Table1");
                //var row = $("[id*=gdView] tr:last-child").clone(true);
                //$("[id*=gdView] tr").not($("[id*=gdView] tr:first-child")).remove();

                //if (customers.length > 0) {
                //    $.each(customers, function () {
                //        //  debugger;
                //        var customer = $(this);
                //        $("td", row).eq(0).html($(this).find("Test_Id").text());
                //        $("td", row).eq(1).html($(this).find("CELEBRITY_IMAGE").text());
                //        $("td", row).eq(2).html($(this).find("Name").text());
                //        $("[id*=gdView]").append(row);
                //        row = $("[id*=gdView] tr:last-child").clone(true);
                //    });
                //}
                //else {
                //    var empty_row = row.clone(true);
                //    $("[id*=gdView]").append(empty_row);
                //    console.log("else");
                //}
            }


            //var row;
            //function OnSuccess(response) {

            //    console.log(response.d);

            //    var xmlDoc = $.parseXML(response.d);
            //    var xml = $(xmlDoc);
            //    var customers = xml.find("Table1");
            //    if (row == null) {
            //        row = $("[id*=gdView] tr:last-child").clone(true);
            //    }
            //    $("[id*=gdView] tr").not($("[id*=gdView] tr:first-child")).remove();


            //    if (customers.length > 0) {                                      

            //        $.each(customers, function () {
            //            var customer = $(this);
            //            $("td", row).eq(0).html($(this).find("Test_Id").text());
            //            $("td", row).eq(1).html($(this).find("CELEBRITY_IMAGE").text());
            //            $("td", row).eq(2).html($(this).find("Name").text());
            //            $("td", row).eq(3).html($(this).find("DESCRIPTION").text());
            //            $("td", row).eq(4).html($(this).find("TYPE").text());
            //            $("[id*=gdView]").append(row);
            //            row = $("[id*=gdView] tr:last-child").clone(true);
            //        });
            //    }
            //    else {
            //        var empty_row = row.clone(true);
            //        $("td:first-child", empty_row).attr("colspan", $("td", row).length);
            //        $("td:first-child", empty_row).attr("align", "center");
            //        $("td:first-child", empty_row).html("No records found for the search criteria.");
            //        $("td", empty_row).not($("td:first-child", empty_row)).remove();
            //        $("[id*=gdView]").append(empty_row);                

            //    }
            //}            


        });

    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Testimonial
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Choose Type<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlType" runat="server" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" AutoPostBack="true" CssClass="dropdown">
            <asp:ListItem>-- Select --</asp:ListItem>
            <asp:ListItem>Common People</asp:ListItem>
            <asp:ListItem>Celebrity</asp:ListItem>
        </asp:DropDownList>
        <br />
        <div id="divImage" runat="server" visible="false">
            <label class="control-label">
                Celebrity Image <span class="required">*</span> :
                 <br />
                (Recommended image size for Celebrity Image - Width 85px X Height 85px)
            </label>
            <asp:FileUpload ID="fileUploadImage" runat="server" CssClass="file-upload" />
            <img id="img" runat="server" visible="false" style="width: 85px; height: 85px;" />
            <br />
        </div>
        <label class="control-label">
            Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Testimonial :
        </label>
        <CKEditor:CKEditorControl ID="txtDescription" runat="server"></CKEditor:CKEditorControl>
        <br />
        <label class="control-label">
            Testimonial Image :
        </label>
        <asp:FileUpload ID="fileuploadTestiImage" runat="server" CssClass="file-upload" />
        <img id="imgTest" runat="server" visible="false" style="width: 85px; height: 85px;" />
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
    </div>
    <br />
    <br />
    <div style="margin-left: 350px;">
        <label>
            Search : 
        </label>
        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
    </div>
    <br />
    <br />
    <div style="overflow: auto; height: 500px; width: 100%;">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="TEST_ID" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr. No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Images">
                    <ItemTemplate>
                        <img src='<%#Eval("CELEBRITY_IMAGE","../uploads/testimonial/{0}") %>' alt="Not Found" style="width: 85px; height: 85px;" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="NAME" HeaderText="Name" />
                <asp:TemplateField HeaderText="Testimonial">
                    <ItemTemplate>
                        <asp:Literal ID="litdesc" runat="server" Text='<%#Eval("DESCRIPTION") %>' Visible='<%#Eval("DESCRIPTION").ToString()==""?false:true %>'></asp:Literal>
                        <img src='<%#Eval("TESTI_IMAGE","../uploads/testimonial/{0}") %>' style='display: <%#Eval("TESTI_IMAGE").ToString()==""?"none":"block" %>; width: 85px; height: 85px;' alt="Not Found" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TYPE" HeaderText="Type" />
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                            Height="41px" margin-top="11px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
        <asp:GridView ID="dumyfrid" runat="server"></asp:GridView>
    </div>
</asp:Content>

