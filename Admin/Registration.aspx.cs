﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Registration : AdminPage
{
    RegistrationTable tbl = new RegistrationTable();
    BAL bal = new BAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    public void BindGrid()
    {
        gdView.Columns[0].Visible = true;
        gdView.DataSource = bal.BindGrid(tbl);
        gdView.DataBind();
        gdView.Columns[0].Visible = false;
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tbl.AddressIndex = int.Parse(Id.ToString());
        bal.DeleteRegisteredUser(tbl);

        if (HttpContext.Current.Session["msgdelete"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Record deleted Successfully.");
            BindGrid();
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not delete Successfully.");
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}