﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{

    AdminAppointmentDetails apptbl = new AdminAppointmentDetails();
    BAL bal = new BAL();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string AppId = Request.QueryString["AppId"].ToString();
            AppViewDetails(AppId);
        }
    }

    public void AppViewDetails(string AppId)
    {
        apptbl.App_Id_bint = int.Parse(AppId.ToString());
        bal.AdminGetAppointmentDetails(apptbl);

        lblSpeciality.Text = apptbl.SpecialityName;
        lblDoctorName.Text = apptbl.DoctorName;
        lblAvailableTiming.Text = apptbl.App_Avai_Time;
        lblAppointmentDate.Text = apptbl.App_Date;
        lblVisitTime.Text = apptbl.App_Visit_Time;

        lblwelcome.Text = apptbl.FirstName + " " + apptbl.SureName;
        lblSureName.Text = apptbl.SureName;
        lblFirstName.Text = apptbl.FirstName;
        lblLastName.Text = apptbl.MiddleName;
        lblSex.Text = apptbl.Sex;
        lblEmailID.Text = apptbl.EmailID;
        lblLHNO.Text = apptbl.LHNO;
        lblDOB.Text = apptbl.DOB;
        lblMaritalStatus.Text = apptbl.MaritalStatus;
        lblReligion.Text = apptbl.Religion;
        lblOccupation.Text = apptbl.Occupation;
        lblAddress.Text = apptbl.Address;
        lblTelephoneResi.Text = apptbl.TelephoneResidence;
        lblMobileNo.Text = apptbl.MobileNo;

        lblTitle.Text = apptbl.Title;
        lblBloodGroup.Text = apptbl.BloodGroup;
        lblHospitalLHNO.Text = apptbl.OriginalLHNO;
    }


}