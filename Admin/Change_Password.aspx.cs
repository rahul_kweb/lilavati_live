﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class admin_Change_Password : AdminPage
{
    Utility Utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (CheckSave())
        {
            using (SqlCommand cmd = new SqlCommand("Proc_ADMIN"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "CHANGE_PASSWORD");
                cmd.Parameters.AddWithValue("@User_name", AdminName);
                cmd.Parameters.AddWithValue("@Password", txtOldPwd.Text.Trim());
                cmd.Parameters.AddWithValue("@NewPassword", txtNewPwd.Text.Trim());
                cmd.Parameters.AddWithValue("@ADMINTYPE", Session[AppKey.Admin_Login_Type].ToString());

                DataTable dTable = new DataTable();
                dTable = Utility.Display(cmd);
                if (dTable != null && dTable.Rows.Count > 0)
                {
                    string STATUS = dTable.Rows[0][0].ToString();
                    if (STATUS == "1")
                    {
                        MyMessageBox1.ShowSuccess("Password changed successfully");
                    }
                    else if (STATUS == "0")
                    {
                        MyMessageBox1.ShowWarning("Old password did not match.");
                    }
                }
            }
        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtOldPwd.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Old Password, ";
        }
        if (txtNewPwd.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "New Password, ";
        }     

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

}