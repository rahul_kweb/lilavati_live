﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    VisaInvitation tblVisaInvitation = new VisaInvitation();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewDetail();
        }
    }

    public void ViewDetail()
    {

        tblVisaInvitation.Visa_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.GetByIdVisaInvitation(tblVisaInvitation);

        lblPatientName.Text = tblVisaInvitation.PatientName;
        lblEmailID.Text = tblVisaInvitation.EmailID;
        lblContactNo.Text = tblVisaInvitation.ContactNo;
        lblNationality.Text = tblVisaInvitation.Nationality;
        lblPassportNo.Text = tblVisaInvitation.PassportNo;        
        hyplinkPatientPasswordCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.PatientPasswordCopy;
        lblHospitalName.Text = tblVisaInvitation.HospitalName;
        lblTreatmentDocName.Text = tblVisaInvitation.TreatmentDocName;
        hyplinkTreatment.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.Treatment;
        lblProvisionalDiagnosis.Text = tblVisaInvitation.ProvisionalDiagnosis;
        lblPurposeOfVisit.Text = tblVisaInvitation.PurposeOfVisit;
        lblLilavatiDoctorName.Text = tblVisaInvitation.LilavatiDoctorName;
        lblPreferredDate.Text = tblVisaInvitation.PreferredDate;
        lblDurationOfTreatment.Text = tblVisaInvitation.DurationOfTreatment;
        lblComingForFollowup.Text = tblVisaInvitation.ComingForFollowup;

        if (tblVisaInvitation.DateOfLastVisit != "")
        {
            lblDateOfLastVisit.Text = tblVisaInvitation.DateOfLastVisit;
        }
        else
        {
            trDOV.Visible = false;
        }

        lblNoOfAttendant.Text = tblVisaInvitation.NoOfAttendant;
        lblPleaseSpecifyTheReason.Text = tblVisaInvitation.PleaseSpecifyTheReason;

        if (tblVisaInvitation.NoOfAttendant == "1")
        {
            lblFirstAttName.Text = tblVisaInvitation.FirstAttName;
            lblFirstRelationship.Text = tblVisaInvitation.FirstRelationship;
            lblFirstPassportNo.Text = tblVisaInvitation.FirstPassportNo;
            hyplinkFirstFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.FirstFileUploadCopy;
        }
        else
        {
            divFirst.Visible = false;
        }

        if (tblVisaInvitation.NoOfAttendant == "2")
        {
            divFirst.Visible = true;

            lblFirstAttName.Text = tblVisaInvitation.FirstAttName;
            lblFirstRelationship.Text = tblVisaInvitation.FirstRelationship;
            lblFirstPassportNo.Text = tblVisaInvitation.FirstPassportNo;
            hyplinkFirstFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.FirstFileUploadCopy;

            lblSecondAttName.Text = tblVisaInvitation.SecondAttName;
            lblSecondRelationship.Text = tblVisaInvitation.SecondRelationship;
            lblSecondPassportNo.Text = tblVisaInvitation.SecondPassportNo;
            hyplinkSecondFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.SecondFileUploadCopy;
        }
        else
        {
            divSecond.Visible = false;
        }

        if (tblVisaInvitation.NoOfAttendant == "3")
        {
            divFirst.Visible = true;
            divSecond.Visible = true;

            lblFirstAttName.Text = tblVisaInvitation.FirstAttName;
            lblFirstRelationship.Text = tblVisaInvitation.FirstRelationship;
            lblFirstPassportNo.Text = tblVisaInvitation.FirstPassportNo;
            hyplinkFirstFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.FirstFileUploadCopy;

            lblSecondAttName.Text = tblVisaInvitation.SecondAttName;
            lblSecondRelationship.Text = tblVisaInvitation.SecondRelationship;
            lblSecondPassportNo.Text = tblVisaInvitation.SecondPassportNo;
            hyplinkSecondFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.SecondFileUploadCopy;

            lblThirdAttName.Text = tblVisaInvitation.ThirdAttName;
            lblThirdRelationship.Text = tblVisaInvitation.ThirdRelationship;
            lblThirdPassportNo.Text = tblVisaInvitation.ThirdPassportNo;
            hyplinkThirdFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.ThirdFileUploadCopy;
        }
        else
        {
            divThird.Visible = false;
        }

        if (tblVisaInvitation.NoOfAttendant == "4")
        {
            divFirst.Visible = true;
            divSecond.Visible = true;
            divThird.Visible = true;

            lblFirstAttName.Text = tblVisaInvitation.FirstAttName;
            lblFirstRelationship.Text = tblVisaInvitation.FirstRelationship;
            lblFirstPassportNo.Text = tblVisaInvitation.FirstPassportNo;
            hyplinkFirstFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.FirstFileUploadCopy;

            lblSecondAttName.Text = tblVisaInvitation.SecondAttName;
            lblSecondRelationship.Text = tblVisaInvitation.SecondRelationship;
            lblSecondPassportNo.Text = tblVisaInvitation.SecondPassportNo;
            hyplinkSecondFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.SecondFileUploadCopy;

            lblThirdAttName.Text = tblVisaInvitation.ThirdAttName;
            lblThirdRelationship.Text = tblVisaInvitation.ThirdRelationship;
            lblThirdPassportNo.Text = tblVisaInvitation.ThirdPassportNo;
            hyplinkThirdFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.ThirdFileUploadCopy;

            lblFourthAttName.Text = tblVisaInvitation.FourthAttName;
            lblFourthRelationship.Text = tblVisaInvitation.FourthRelationship;
            lblFourthPassportNo.Text = tblVisaInvitation.FourthPassportNo;
            hyplinkFourthFileUploadCopy.NavigateUrl = "../uploads/visa_invitation/" + tblVisaInvitation.FourthFileUploadCopy;
        }
        else
        {
            divFourth.Visible = false;
        }
            
    }   
           
}