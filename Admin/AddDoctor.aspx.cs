﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AddDoctor : AdminPage
{
    BAL bal = new BAL();
    Specialty obj = new Specialty();
    Doctor obj1 = new Doctor();
    Utility utility = new Utility();
    public string heading;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSpecialityCheckbox();


            if (Request.QueryString["DrId"] != null)
            {
                string id = Request.QueryString["DrId"].ToString();
                obj1._Buttontext = "Update Doctor";
                btnSave.Text = "Update Doctor";
                btnbacktolist.Visible = true;
                btncancel.Visible = false;
                GetRecordEditTime(id);
                txtDoctorName.Text = obj1._Doctorname;
                //txtDesignation.Text = dt.Rows[0]["Designation_vcr"].ToString();
                txtQualification.Text = obj1._Qualification;
                txtAreaOfInterest.Text = obj1._Areaofinterest;
                txtAchievments.Text = obj1._Achievements;

                txtslotminuts.Text = obj1._SlotMinuts;
                txtconsdocid.Text = obj1._ConsDrId;
                txtResearchPublication.Text = obj1.ResearchPub;
                txtProfile.Text = obj1.ProfileCnt;


                // Direct Appointment

                if (obj1._DirectAvailable == true)
                {
                    rdDirectAvailable.SelectedValue = "1";
                    this.DirectAvailable.Visible = true;

                }
                else
                {
                    rdDirectAvailable.SelectedValue = "0";
                    this.DirectAvailable.Visible = false;
                }

                txtDirectContact.Text = obj1._ContactNo;

                txtDirectTimeFrom.Text = obj1._DirectTimeFrom;
                //txtDirectTimeTo.Text = obj1.DirectTimeTo;

                ////txtDirectDaysFrom.Text = obj1._DirectDaysFrom;
                //txtDirectDaysTo.Text = obj1._DirectDaysTo;





                ShowHideOneditTime();
                SplitTime(obj1._AvailTimeMon, obj1._AvailTimeMon1, obj1._AvailTimeTue, obj1._AvailTimeTue1, obj1._AvailTimeWed, obj1._AvailTimeWed1, obj1._AvailTimeThurs, obj1._AvailTimeThurs1, obj1._AvailTimeFri, obj1._AvailTimeFri1, obj1._AvailTimeSat, obj1._AvailTimeSat1, obj1._Room_vcr_Mon, obj1._Room_vcr_Mon1, obj1._Room_vcr_Tue, obj1._Room_vcr_Tue1, obj1._Room_vcr_Wed, obj1._Room_vcr_Wed1, obj1._Room_vcr_Thurs, obj1._Room_vcr_Thurs1, obj1._Room_vcr_Fri, obj1._Room_vcr_Fri1, obj1._Room_vcr_Sat, obj1._Room_vcr_Sat1);

                //txtRoomMon.Text = obj1._Room_vcr_Mon;
                //txtRoomTue.Text = obj1._Room_vcr_Tue;
                //txtRoomWed.Text = obj1._Room_vcr_Wed;
                //txtRoomThu.Text = obj1._Room_vcr_Thurs;
                //txtRoomFri.Text = obj1._Room_vcr_Fri;
                //txtRoomSat.Text = obj1._Room_vcr_Sat;

                ImgDoctor.ImageUrl = obj1._Photo;

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("EXEC  AddUpdateGetDoctorSpecialityMaster 'Get_By_Doc_Id',0," + id);
                if (dt1.Rows.Count > 0)
                {
                    SetCheckBox(dt1);
                }

                //Tele consultation
                if (obj1.TeleConsultation)
                {
                    rdbTeleConsultation.SelectedValue = "1";
                }
                else
                {
                    rdbTeleConsultation.SelectedValue = "0";
                }
            }
            else
            {
                heading = "ADD DOCTOR";
                rdbTeleConsultation.SelectedValue = "0";
            }
        }
    }

    public DataTable GetRecordEditTime(string id)
    {
        obj1._DoctorId = int.Parse(id);


        return bal.GetDoctorlity(obj1);

    }

    public void BindSpecialityCheckbox()
    {
        obj._Specialityname = "Speciality_vcr";
        obj._CheckBoxId = "Speciality_Id_bint";
        string query = "EXEC AddUpdateGetSpecialtyMaster 'Get'";
        bal.GetCheckListOfSpeciality(obj, chkSpecialities, query);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (Validation())
        {
            if (Request.QueryString["DrId"] != null)
            {
                obj1._Buttontext = "Update Doctor";
                obj1._DoctorId = int.Parse(Request.QueryString["DrId"].ToString());
                setdeletevalueofcheckbox(Request.QueryString["DrId"].ToString());
            }
            else
            {
                obj1._Buttontext = "Add Doctor";
            }


            obj1._Doctorname = txtDoctorName.Text.ToString();
            obj1._Achievements = txtAchievments.Text.ToString();

            obj1._Photo = SaveFile();

            obj1._Qualification = txtQualification.Text.ToString();
            obj1._Areaofinterest = txtAreaOfInterest.Text.ToString();
            obj1._Gender = ddlGender.SelectedValue;


            ///Direct Appointment
            if (rdDirectAvailable.SelectedItem.Value == "1")
            {
                obj1._DirectAvailable = true;

            }
            else
            {
                obj1._DirectAvailable = false;

            }

            obj1._ContactNo = txtDirectContact.Text.ToString();
            obj1._DirectTimeFrom = txtDirectTimeFrom.Text.ToString();
            /*  obj1.DirectTimeTo = txtDirectTimeTo.Text.ToString()*/
            ;
            //obj1._DirectDaysFrom = txtDirectDaysFrom.Text.ToString();
            //obj1._DirectDaysTo = txtDirectDaysTo.Text.ToString();



            /*Monday*/
            #region Monday

            if (rdbAvailMon.SelectedValue == "1")
            {
                #region commented code
                //    ////obj1._AvailTimeMon = txtMonFrom.Text.Trim() + "-" + ddlMonFrom.Text + "-" + txtMonTo.Text.Trim() + "-" + ddlMonTo.Text;
                //    string Mondayfrom1 = txtMonFrom.Text.Trim();
                //    string MondaytimeInsert = string.Empty;

                //    if (Mondayfrom1.Contains("#") == true)
                //    {
                //        char[] Mondayfrom = { '#' };

                //        string[] mondayfrom2 = Mondayfrom1.Split(Mondayfrom);
                //        string mondayfrom3 = string.Empty;
                //        string mondayfrom4 = string.Empty;
                //        string mondayfrom5 = string.Empty;
                //        foreach (string monfrom in mondayfrom2)
                //        {
                //            if (mondayfrom3 == "")
                //            {
                //                mondayfrom3 = monfrom.ToString();
                //            }
                //            else if (mondayfrom4 == "")
                //            {
                //                mondayfrom4 = monfrom.ToString();
                //            }
                //            else if (mondayfrom5 == "")
                //            {
                //                mondayfrom5 = monfrom.ToString();
                //            }
                //            else
                //            {

                //            }
                //        }
                //        char[] Mondayto = { '#' };
                //        string Mondayto1 = txtMonTo.Text.Trim();
                //        string[] mondayto2 = Mondayto1.Split(Mondayto);
                //        string mondayto3 = string.Empty;
                //        string mondayto4 = string.Empty;
                //        string mondayto5 = string.Empty;
                //        foreach (string monto in mondayto2)
                //        {
                //            if (mondayto3 == "")
                //            {
                //                mondayto3 = monto.ToString();
                //            }
                //            else if (mondayto4 == "")
                //            {
                //                mondayto4 = monto.ToString();
                //            }
                //            else if (mondayto5 == "")
                //            {
                //                mondayto5 = monto.ToString();
                //            }
                //            else
                //            {

                //            }
                //        }

                #region Room code Comment
                //char[] MondayRoom = { '#' };
                //string MondayRoom1 = txtMonTo.Text.Trim();
                //string[] mondayRoom2 = MondayRoom1.Split(MondayRoom);
                //string mondayRoom3 = string.Empty;
                //string mondayRoom4 = string.Empty;
                //string mondayRoom5 = string.Empty;
                //foreach (string monroom in mondayRoom2)
                //{
                //    if (mondayRoom3 == "")
                //    {
                //        mondayRoom3 = monroom.ToString();
                //    }
                //    else if (mondayRoom4 == "")
                //    {
                //        mondayRoom4 = monroom.ToString();
                //    }
                //    else if (mondayRoom5 == "")
                //    {
                //        mondayRoom5 = monroom.ToString();
                //    }
                //    else
                //    {

                //    }
                //}
                #endregion


                //if (mondayfrom3 != "" && mondayto3 != "")
                //{
                //    MondaytimeInsert += mondayfrom3.ToString() + "-" + mondayto3.ToString();
                //}
                //if (mondayfrom4 != "" && mondayto4 != "")
                //{
                //    MondaytimeInsert += "#" + mondayfrom4.ToString() + "-" + mondayto4.ToString();
                //}
                //if (mondayfrom5 != "" && mondayto5 != "")
                //{
                //    MondaytimeInsert += "#" + mondayfrom5.ToString() + "-" + mondayto5.ToString();
                //}
                #endregion

                obj1._AvailTimeMon = txtMonFrom.Text + " - " + txtMonTo.Text;
                obj1._Room_vcr_Mon = txtRoomMon.Text.ToString();
                obj1._NotAvailMonbit = false;
                if (txtMonFrom1.Text != "" && txtMonTo1.Text != "" && txtRoomMon1.Text != "")
                {
                    obj1._AvailTimeMon1 = txtMonFrom1.Text + " - " + txtMonTo1.Text;
                    obj1._Room_vcr_Mon1 = txtRoomMon1.Text.ToString();
                }
                else
                {
                    obj1._AvailTimeMon1 = "N.A";
                    obj1._Room_vcr_Mon1 = "N.R";

                }
            }
            else
            {
                obj1._AvailTimeMon = "N.A";
                obj1._Room_vcr_Mon = "N.R";
                obj1._AvailTimeMon1 = "N.A";
                obj1._Room_vcr_Mon1 = "N.R";
                obj1._NotAvailMonbit = true;
            }

            #endregion


            /*Tuesday*/
            #region Tuesday
            if (rdbAvailTue.SelectedValue == "1")
            {

                obj1._AvailTimeTue = txtTueFrom.Text + " - " + txtTueTo.Text;
                obj1._Room_vcr_Tue = txtRoomTue.Text.ToString();
                obj1._NotAvailTuebit = false;
                if (txtTueFrom1.Text != "" && txtTueTo1.Text != "" && txtRoomTue1.Text != "")
                {
                    obj1._AvailTimeTue1 = txtTueFrom1.Text + " - " + txtTueTo1.Text;
                    obj1._Room_vcr_Tue1 = txtRoomTue1.Text.ToString();
                }
                else
                {
                    obj1._AvailTimeTue1 = "N.A";
                    obj1._Room_vcr_Tue1 = "N.R";

                }
            }
            else
            {
                obj1._AvailTimeTue = "N.A";
                obj1._Room_vcr_Tue = "N.R";
                obj1._AvailTimeTue1 = "N.A";
                obj1._Room_vcr_Tue1 = "N.R";
                obj1._NotAvailTuebit = true;
            }

            #endregion


            /*Wednesday*/
            #region Wednesday
            if (rdbAvailWed.SelectedValue == "1")
            {

                obj1._AvailTimeWed = txtWedFrom.Text + " - " + txtWedTo.Text;
                obj1._Room_vcr_Wed = txtRoomWed.Text.ToString();
                obj1._NotAvailWedbit = false;
                if (txtWedFrom1.Text != "" && txtWedTo1.Text != "" && txtRoomWed1.Text != "")
                {
                    obj1._AvailTimeWed1 = txtWedFrom1.Text + " - " + txtWedTo1.Text;
                    obj1._Room_vcr_Wed1 = txtRoomWed1.Text.ToString();
                }
                else
                {
                    obj1._AvailTimeWed1 = "N.A";
                    obj1._Room_vcr_Wed1 = "N.R";

                }
            }
            else
            {
                obj1._AvailTimeWed = "N.A";
                obj1._Room_vcr_Wed = "N.R";
                obj1._AvailTimeWed1 = "N.A";
                obj1._Room_vcr_Wed1 = "N.R";
                obj1._NotAvailWedbit = true;
            }

            #endregion

            /*Thursday*/
            #region Thursday
            if (rdbAvailThu.SelectedValue == "1")
            {

                obj1._AvailTimeThurs = txtThuFrom.Text + " - " + txtThuTo.Text;
                obj1._Room_vcr_Thurs = txtRoomThu.Text.ToString();
                obj1._NotAvailThursbit = false;
                if (txtThuFrom1.Text != "" && txtThuTo1.Text != "" && txtRoomThu1.Text != "")
                {
                    obj1._AvailTimeThurs1 = txtThuFrom1.Text + " - " + txtThuTo1.Text;
                    obj1._Room_vcr_Thurs1 = txtRoomThu1.Text.ToString();
                }
                else
                {
                    obj1._AvailTimeThurs1 = "N.A";
                    obj1._Room_vcr_Thurs1 = "N.R";

                }
            }
            else
            {
                obj1._AvailTimeThurs = "N.A";
                obj1._Room_vcr_Thurs = "N.R";
                obj1._AvailTimeThurs1 = "N.A";
                obj1._Room_vcr_Thurs1 = "N.R";
                obj1._NotAvailThursbit = true;
            }

            #endregion

            /*Friday*/
            #region Friday
            if (rdbAvailFri.SelectedValue == "1")
            {

                obj1._AvailTimeFri = txtFriFrom.Text + " - " + txtFriTo.Text;
                obj1._Room_vcr_Fri = txtRoomFri.Text.ToString();
                obj1._NotAvailFribit = false;
                if (txtFriFrom1.Text != "" && txtFriTo1.Text != "" && txtRoomFri1.Text != "")
                {
                    obj1._AvailTimeFri1 = txtFriFrom1.Text + " - " + txtFriTo1.Text;
                    obj1._Room_vcr_Fri1 = txtRoomFri1.Text.ToString();
                }
                else
                {
                    obj1._AvailTimeFri1 = "N.A";
                    obj1._Room_vcr_Fri1 = "N.R";

                }
            }
            else
            {
                obj1._AvailTimeFri = "N.A";
                obj1._Room_vcr_Fri = "N.R";
                obj1._AvailTimeFri1 = "N.A";
                obj1._Room_vcr_Fri1 = "N.R";
                obj1._NotAvailFribit = true;
            }

            #endregion

            /*Saturday*/
            #region Saturday
            if (rdbAvailSat.SelectedValue == "1")
            {

                obj1._AvailTimeSat = txtSatFrom.Text + " - " + txtSatTo.Text;
                obj1._Room_vcr_Sat = txtRoomSat.Text.ToString();
                obj1._NotAvailSatbit = false;
                if (txtSatFrom1.Text != "" && txtSatTo1.Text != "" && txtRoomSat1.Text != "")
                {
                    obj1._AvailTimeSat1 = txtSatFrom1.Text + " - " + txtSatTo1.Text;
                    obj1._Room_vcr_Sat1 = txtRoomSat1.Text.ToString();
                }
                else
                {
                    obj1._AvailTimeSat1 = "N.A";
                    obj1._Room_vcr_Sat1 = "N.R";

                }
            }
            else
            {
                obj1._AvailTimeSat = "N.A";
                obj1._Room_vcr_Sat = "N.R";
                obj1._AvailTimeSat1 = "N.A";
                obj1._Room_vcr_Sat1 = "N.R";
                obj1._NotAvailSatbit = true;
            }

            #endregion

            obj1._SlotMinuts = txtslotminuts.Text.Trim();
            obj1._ConsDrId = txtconsdocid.Text.Trim();
            obj1.ResearchPub = txtResearchPublication.Text.Trim();
            obj1.ProfileCnt = txtProfile.Text.Trim();


            foreach (ListItem li in chkSpecialities.Items)
            {
                if (li.Selected == true)
                {

                    obj1.MultipleSpeciality.Add(li.Value);
                }
                else
                {

                }

            }

            //tele consultation
            var tele = rdbTeleConsultation.SelectedValue;
            if (tele == "1")
            {
                obj1.TeleConsultation = true;
            }
            else
            {
                obj1.TeleConsultation = false;
            }

            string result = bal.PassDrRecord(obj1);

            if (result == "success")
            {
                Reset();
                MyMessageBox1.ShowSuccess("Dr Added SuccessFully");
            }
            else if (result == "Dr Update")
            {

                MyMessageBox1.ShowSuccess("Dr Update SuccessFully");
            }
            else
            {
                MyMessageBox1.ShowError("Error occurred");
            }
        }
    }
    private string SaveFile()
    {
        string FilePath = "";
        string extensionI1 = System.IO.Path.GetExtension(fileuploadimage.FileName);

        if (extensionI1 != "")
        {
            string fName = "Doc_" + txtDoctorName.Text.Trim() + "_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_');


            fileuploadimage.SaveAs(Server.MapPath("Doctors/" + fName.Replace(' ', '_') + extensionI1 + ""));
            FilePath = fName.Replace(' ', '_') + extensionI1;
        }
        return FilePath;

    }

    private void SetCheckBox(DataTable dt1)
    {
        foreach (DataRow dr in dt1.Rows)
        {
            string txt = dr["Speciality_vcr"].ToString();
            string id = dr["Speciality_Id_bint"].ToString();
            chkSpecialities.Items.FindByText(txt).Selected = true;
            obj1.MultipleSpecialitydelete.Add(id);


        }

    }

    private void setdeletevalueofcheckbox(string id)
    {


        DataTable dt1 = new DataTable();
        dt1 = utility.Display("EXEC  AddUpdateGetDoctorSpecialityMaster 'Get_By_Doc_Id',0," + id);
        if (dt1.Rows.Count > 0)
        {

            foreach (DataRow dr in dt1.Rows)
            {

                string id1 = dr["Speciality_Id_bint"].ToString();
                obj1.MultipleSpecialitydelete.Add(id1);


            }
        }
    }

    public void SplitTime(string mon, string mon1, string tue, string tue1, string wed, string wed1, string thurs, string thurs1, string frid, string frid1, string sat, string sat1, string mondroom, string mondroom1, string tuedroom, string tuedroom1, string wedroom, string wedroom1, string thurroom, string thurroom1, string friroom, string friroom1, string satroom, string satroom1)
    {
        char[] monday = { '-' };
        char[] tuesday = { '-' };
        char[] wednesday = { '-' };
        char[] thursday = { '-' };
        char[] friday = { '-' };
        char[] saturday = { '-' };

        /*Monday array code*/
        #region Monday
        if (mon != "N.A" && mon != "N.A." && mon != "----" && mon != "")
        {
            string[] monday1 = mon.Split(monday);
            bool monfrom = false;
            //  bool monam = false;
            bool monto = false;
            // bool monpm = false;
            foreach (string monlist in monday1)
            {
                if (monfrom != true)
                {
                    txtMonFrom.Text = monlist;
                    monfrom = true;
                }

                else if (monto != true)
                {
                    txtMonTo.Text = monlist;
                    monto = true;
                }

            }
            txtRoomMon.Text = mondroom;
        }
        else
        {
            txtMonFrom.Text = string.Empty;
            txtMonTo.Text = string.Empty;
            txtRoomMon.Text = string.Empty;
        }
        if (mon1 != "N.A" && mon1 != "N.A." && mon1 != "----" && mon1 != "")
        {
            string[] monday1 = mon1.Split(monday);
            bool monfrom1 = false;
            //  bool monam = false;
            bool monto1 = false;
            // bool monpm = false;
            foreach (string monlist1 in monday1)
            {
                if (monfrom1 != true)
                {
                    txtMonFrom1.Text = monlist1;
                    monfrom1 = true;
                }

                else if (monto1 != true)
                {
                    txtMonTo1.Text = monlist1;
                    monto1 = true;
                }

            }
            txtRoomMon1.Text = mondroom1;
        }
        else
        {
            txtMonFrom1.Text = string.Empty;
            txtMonTo1.Text = string.Empty;
            txtRoomMon1.Text = string.Empty;
        }


        #endregion


        /*Tuesday array code*/

        #region Tuesday
        if (tue != "N.A" && tue != "N.A." && tue != "----" && tue != "")
        {
            string[] tuesday1 = tue.Split(tuesday);
            bool tuesfrom = false;
            // bool tuesam = false;
            bool tuesto = false;
            //  bool tuespm = false;
            foreach (string tueslist in tuesday1)
            {
                if (tuesfrom != true)
                {
                    txtTueFrom.Text = tueslist;
                    tuesfrom = true;
                }

                else if (tuesto != true)
                {
                    txtTueTo.Text = tueslist;
                    tuesto = true;
                }

            }
            txtRoomTue.Text = tuedroom;
        }
        else
        {
            txtTueFrom.Text = string.Empty;
            txtTueTo.Text = string.Empty;
            txtRoomTue.Text = string.Empty;
        }

        if (tue1 != "N.A" && tue1 != "N.A." && tue1 != "----" && tue1 != "")
        {
            string[] tuesday1 = tue1.Split(tuesday);
            bool tuesfrom1 = false;
            // bool tuesam = false;
            bool tuesto1 = false;
            //  bool tuespm = false;
            foreach (string tueslist1 in tuesday1)
            {
                if (tuesfrom1 != true)
                {
                    txtTueFrom1.Text = tueslist1;
                    tuesfrom1 = true;
                }

                else if (tuesto1 != true)
                {
                    txtTueTo1.Text = tueslist1;
                    tuesto1 = true;
                }

            }
            txtRoomTue1.Text = tuedroom1;
        }
        else
        {
            txtTueFrom1.Text = string.Empty;
            txtTueTo1.Text = string.Empty;
            txtRoomTue1.Text = string.Empty;
        }

        #endregion

        /*wednesday array code*/
        #region wednesday
        if (wed != "N.A" && wed != "N.A." && wed != "----")
        {
            string[] wednedday1 = wed.Split(wednesday);
            bool wednedfrom = false;
            // bool wednedam = false;
            bool wednedto = false;
            // bool wednedpm = false;
            foreach (string wednesdaylist in wednedday1)
            {
                if (wednedfrom != true)
                {
                    txtWedFrom.Text = wednesdaylist;
                    wednedfrom = true;
                }

                else if (wednedto != true)
                {
                    txtWedTo.Text = wednesdaylist;
                    wednedto = true;
                }

            }
            txtRoomWed.Text = wedroom;
        }
        else
        {
            txtWedFrom.Text = string.Empty;
            txtWedTo.Text = string.Empty;
            txtRoomWed.Text = string.Empty;
        }


        if (wed1 != "N.A" && wed1 != "N.A." && wed1 != "----" && wed1 != "")
        {
            string[] wednedday1 = wed1.Split(wednesday);
            bool wednedfrom1 = false;
            // bool wednedam = false;
            bool wednedto1 = false;
            // bool wednedpm = false;
            foreach (string wednesdaylist1 in wednedday1)
            {
                if (wednedfrom1 != true)
                {
                    txtWedFrom1.Text = wednesdaylist1;
                    wednedfrom1 = true;
                }

                else if (wednedto1 != true)
                {
                    txtWedTo1.Text = wednesdaylist1;
                    wednedto1 = true;
                }

            }
            txtRoomWed1.Text = wedroom1;
        }
        else
        {
            txtWedFrom1.Text = string.Empty;
            txtWedTo1.Text = string.Empty;
            txtRoomWed1.Text = string.Empty;
        }

        #endregion

        /*Thursday array code*/
        #region Thurday
        if (thurs != "N.A" && thurs != "N.A." && thurs != "----")
        {
            string[] Thursday1 = thurs.Split(thursday);
            bool Thursfrom = false;
            // bool Thursam = false;
            bool Thursto = false;
            //bool Thurspm = false;
            foreach (string thursdaylist in Thursday1)
            {
                if (Thursfrom != true)
                {
                    txtThuFrom.Text = thursdaylist;
                    Thursfrom = true;
                }

                else if (Thursto != true)
                {
                    txtThuTo.Text = thursdaylist;
                    Thursto = true;
                }

            }
            txtRoomThu.Text = thurroom;
        }
        else
        {
            txtThuFrom.Text = string.Empty;
            txtThuTo.Text = string.Empty;
            txtRoomThu.Text = string.Empty;
        }

        if (thurs1 != "N.A" && thurs1 != "N.A." && thurs1 != "----" && thurs1 != "")
        {
            string[] Thursday1 = thurs1.Split(thursday);
            bool Thursfrom1 = false;
            // bool Thursam = false;
            bool Thursto1 = false;
            //bool Thurspm = false;
            foreach (string thursdaylist1 in Thursday1)
            {
                if (Thursfrom1 != true)
                {
                    txtThuFrom1.Text = thursdaylist1;
                    Thursfrom1 = true;
                }

                else if (Thursto1 != true)
                {
                    txtThuTo1.Text = thursdaylist1;
                    Thursto1 = true;
                }

            }
            txtRoomThu1.Text = thurroom1;
        }
        else
        {
            txtThuFrom1.Text = string.Empty;
            txtThuTo1.Text = string.Empty;
            txtRoomThu1.Text = string.Empty;
        }
        #endregion

        /*Friday array code*/
        #region Friday
        if (frid != "N.A" && frid != "N.A." && frid != "----" && frid != "")
        {
            string[] Friday1 = frid.Split(friday);
            bool fridfrom = false;
            // bool fridam = false;
            bool fridto = false;
            // bool fridpm = false;
            foreach (string fridaylist in Friday1)
            {
                if (fridfrom != true)
                {
                    txtFriFrom.Text = fridaylist;
                    fridfrom = true;
                }

                else if (fridto != true)
                {
                    txtFriTo.Text = fridaylist;
                    fridto = true;
                }

            }
            txtRoomFri.Text = friroom;
        }
        else
        {
            txtFriFrom.Text = string.Empty;
            txtFriTo.Text = string.Empty;
            txtRoomFri.Text = string.Empty;
        }
        if (frid1 != "N.A" && frid1 != "N.A." && frid1 != "----" && frid1 != "")
        {
            string[] Friday1 = frid1.Split(friday);
            bool fridfrom1 = false;
            // bool fridam = false;
            bool fridto1 = false;
            // bool fridpm = false;
            foreach (string fridaylist1 in Friday1)
            {
                if (fridfrom1 != true)
                {
                    txtFriFrom1.Text = fridaylist1;
                    fridfrom1 = true;
                }

                else if (fridto1 != true)
                {
                    txtFriTo1.Text = fridaylist1;
                    fridto1 = true;
                }

            }
            txtRoomFri1.Text = friroom1;
        }
        else
        {
            txtFriFrom1.Text = string.Empty;
            txtFriTo1.Text = string.Empty;
            txtRoomFri1.Text = string.Empty;
        }

        #endregion

        /*Saturday array code*/
        #region saturday
        if (sat != "N.A" && sat != "N.A." && sat != "----" && sat != "")
        {
            string[] satday1 = sat.Split(saturday);
            bool satfrom = false;
            //bool satam = false;
            bool satto = false;
            //bool satpm = false;
            foreach (string saturdaylist in satday1)
            {
                if (satfrom != true)
                {
                    txtSatFrom.Text = saturdaylist;
                    satfrom = true;
                }

                else if (satto != true)
                {
                    txtSatTo.Text = saturdaylist;
                    satto = true;
                }

            }
            txtRoomSat.Text = satroom;
        }
        else
        {
            txtSatFrom.Text = string.Empty;
            txtSatTo.Text = string.Empty;
            txtRoomSat.Text = string.Empty;
        }
        if (sat1 != "N.A" && sat1 != "N.A." && sat1 != "----" && sat1 != "")
        {
            string[] satday1 = sat1.Split(saturday);
            bool satfrom1 = false;
            //bool satam = false;
            bool satto1 = false;
            //bool satpm = false;
            foreach (string saturdaylist1 in satday1)
            {
                if (satfrom1 != true)
                {
                    txtSatFrom1.Text = saturdaylist1;
                    satfrom1 = true;
                }

                else if (satto1 != true)
                {
                    txtSatTo1.Text = saturdaylist1;
                    satto1 = true;
                }

            }
            txtRoomSat1.Text = satroom1;
        }
        else
        {
            txtSatFrom1.Text = string.Empty;
            txtSatTo1.Text = string.Empty;
            txtRoomSat1.Text = string.Empty;
        }

        #endregion
    }

    public void ShowHideOneditTime()
    {
        bool isok = true;

        #region Monday
        if (isok == obj1._NotAvailMonbit)
        {
            rdbAvailMon.SelectedValue = "0";
            txtMonFrom.Attributes.Add("style", "display:none;");
            ddlMonFrom.Attributes.Add("style", "display:none;");
            txtMonTo.Attributes.Add("style", "display:none;");
            ddlMonTo.Attributes.Add("style", "display:none;");
            txtRoomMon.Attributes.Add("style", "display:none;");

            txtMonFrom1.Attributes.Add("style", "display:none;");

            txtMonTo1.Attributes.Add("style", "display:none;");

            txtRoomMon1.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtMonFrom.Attributes.Add("style", "Display:block;");
        //    ddlMonFrom.Attributes.Add("style", "Display:block;");
        //    txtMonTo.Attributes.Add("style", "Display:block;");
        //    ddlMonTo.Attributes.Add("style", "Display:block;");
        //    txtRoomMon.Attributes.Add("style", "Display:block;");
        //}
        #endregion

        #region Tuesday
        if (isok == obj1._NotAvailTuebit)
        {
            rdbAvailTue.SelectedValue = "0";
            txtTueFrom.Attributes.Add("style", "display:none;");
            ddlTueFrom.Attributes.Add("style", "display:none;");
            txtTueTo.Attributes.Add("style", "display:none;");
            ddlTueTo.Attributes.Add("style", "display:none;");
            txtRoomTue.Attributes.Add("style", "display:none;");

            txtTueFrom1.Attributes.Add("style", "display:none;");

            txtTueTo1.Attributes.Add("style", "display:none;");

            txtRoomTue1.Attributes.Add("style", "display:none;");
        }
        //if (isok == obj1._NotAvailThursbit)
        //{
        //    txtTueFrom.Attributes.Add("style", "Display:block;");
        //    ddlTueFrom.Attributes.Add("style", "Display:block;");
        //    txtTueTo.Attributes.Add("style", "Display:block;");
        //    ddlTueTo.Attributes.Add("style", "Display:block;");
        //    txtRoomTue.Attributes.Add("style", "Display:block;");
        //}
        #endregion


        #region Wednesday
        if (isok == obj1._NotAvailWedbit)
        {
            rdbAvailWed.SelectedValue = "0";
            txtWedFrom.Attributes.Add("style", "display:none;");
            ddlWedFrom.Attributes.Add("style", "display:none;");
            txtWedTo.Attributes.Add("style", "display:none;");
            ddlWedTo.Attributes.Add("style", "display:none;");
            txtRoomWed.Attributes.Add("style", "display:none;");

            txtWedFrom1.Attributes.Add("style", "display:none;");
            txtWedTo1.Attributes.Add("style", "display:none;");
            txtRoomWed1.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtWedFrom.Attributes.Add("style", "Display:block;");
        //    ddlWedFrom.Attributes.Add("style", "Display:block;");
        //    txtWedTo.Attributes.Add("style", "Display:block;");
        //    ddlWedTo.Attributes.Add("style", "Display:block;");
        //    txtRoomWed.Attributes.Add("style", "Display:block;");
        //}
        #endregion


        #region Thursday
        if (isok == obj1._NotAvailThursbit)
        {
            rdbAvailThu.SelectedValue = "0";
            txtThuFrom.Attributes.Add("style", "display:none;");
            ddlThuFrom.Attributes.Add("style", "display:none;");
            txtThuTo.Attributes.Add("style", "display:none;");
            ddlThuTo.Attributes.Add("style", "display:none;");
            txtRoomThu.Attributes.Add("style", "display:none;");


            txtThuFrom1.Attributes.Add("style", "display:none;");

            txtThuTo1.Attributes.Add("style", "display:none;");

            txtRoomThu1.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtThuFrom.Attributes.Add("style", "Display:block;");
        //    ddlThuFrom.Attributes.Add("style", "Display:block;");
        //    txtThuTo.Attributes.Add("style", "Display:block;");
        //    ddlThuTo.Attributes.Add("style", "Display:block;");
        //    txtRoomThu.Attributes.Add("style", "Display:block;");
        //}
        #endregion


        #region Friday
        if (isok == obj1._NotAvailFribit)
        {
            rdbAvailFri.SelectedValue = "0";
            txtFriFrom.Attributes.Add("style", "display:none;");
            ddlFriFrom.Attributes.Add("style", "display:none;");
            txtFriTo.Attributes.Add("style", "display:none;");
            ddlFriTo.Attributes.Add("style", "display:none;");
            txtRoomFri.Attributes.Add("style", "display:none;");

            txtFriFrom1.Attributes.Add("style", "display:none;");

            txtFriTo1.Attributes.Add("style", "display:none;");

            txtRoomFri1.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtFriFrom.Attributes.Add("style", "Display:block;");
        //    ddlFriFrom.Attributes.Add("style", "Display:block;");
        //    txtFriTo.Attributes.Add("style", "Display:block;");
        //    ddlFriTo.Attributes.Add("style", "Display:block;");
        //    txtRoomFri.Attributes.Add("style", "Display:block;");
        //}
        #endregion

        #region Saturday
        if (isok == obj1._NotAvailSatbit)
        {
            rdbAvailSat.SelectedValue = "0";
            txtSatFrom.Attributes.Add("style", "display:none;");
            ddlSatFrom.Attributes.Add("style", "display:none;");
            txtSatTo.Attributes.Add("style", "display:none;");
            ddlSatTo.Attributes.Add("style", "display:none;");
            txtRoomSat.Attributes.Add("style", "display:none;");



            txtSatFrom1.Attributes.Add("style", "display:none;");

            txtSatTo1.Attributes.Add("style", "display:none;");

            txtRoomSat1.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtSatFrom.Attributes.Add("style", "Display:block;");
        //    ddlSatFrom.Attributes.Add("style", "Display:block;");
        //    txtSatTo.Attributes.Add("style", "Display:block;");
        //    ddlSatTo.Attributes.Add("style", "Display:block;");
        //    txtRoomSat.Attributes.Add("style", "Display:block;");
        //}
        #endregion

    }

    public bool Validation()
    {
        bool isok = true;
        string msg = string.Empty;

        if (txtDoctorName.Text == "")
        {
            msg += "Doctor Name, ";
            isok = false;
        }
        if (txtQualification.Text == "")
        {
            msg += "Qualification, ";
            isok = false;
        }
        if (txtconsdocid.Text == "")
        {
            msg += "Dr ConsId, ";
            isok = false;
        }
        if (txtslotminuts.Text == "")
        {
            msg += "Slots Minuts, ";
            isok = false;
        }
        //if (txtAreaOfInterest.Text == "")
        //{
        //    msg += "Area Of Interest, ";
        //    isok = false;
        //}
        //if (txtAchievments.Text == "")
        //{
        //    msg += "Achievments, ";
        //    isok = false;
        //}

        if (chkSpecialities.SelectedValue == "")
        {
            msg += "Select Specialities";
            isok = false;
        }
        if (!isok)
        {
            MyMessageBox1.ShowError("Please Fill Following Details <br />" + msg);
        }
        return isok;
    }

    protected void btnbacktolist_Click(object sender, EventArgs e)
    {
        Response.Redirect("DoctorsList.aspx");
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    public void Reset()
    {
        ClearTextBoxes(Page);
        chkSpecialities.SelectedIndex = -1;
        rdbAvailMon.SelectedValue = "1";
        rdbAvailTue.SelectedValue = "1";
        rdbAvailWed.SelectedValue = "1";
        rdbAvailThu.SelectedValue = "1";
        rdbAvailFri.SelectedValue = "1";
        rdbAvailSat.SelectedValue = "1";
        //txtAchievments.Text = string.Empty;
        //txtAreaOfInterest.Text = string.Empty;
        //txtDoctorName.Text = string.Empty;
        //txtQualification.Text = string.Empty;
        //txt
    }

    protected void ClearTextBoxes(Control p1)
    {
        foreach (Control ctrl in p1.Controls)
        {
            if (ctrl is TextBox)
            {
                TextBox t = ctrl as TextBox;

                if (t != null)
                {
                    t.Text = String.Empty;
                }
            }
            else
            {
                if (ctrl.Controls.Count > 0)
                {
                    ClearTextBoxes(ctrl);
                }
            }
        }
    }

    protected void rdDirectAvailable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.rdDirectAvailable.SelectedValue == "1")
            this.DirectAvailable.Visible = true;
        else
            this.DirectAvailable.Visible = false;
    }
}