﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Job_Area : AdminPage
{
    HealthCheckup tblHealthCheckup = new HealthCheckup();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        tblHealthCheckup.Package_Name = txtPackageName.Text.Trim();
        cmsbal.SaveHealthCheckup(tblHealthCheckup);

        if (HttpContext.Current.Session["SaveHealthCheckup"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        tblHealthCheckup.Health_Id = Convert.ToInt32(hdnId.Value);
        tblHealthCheckup.Package_Name = txtPackageName.Text.Trim();
        cmsbal.UpdateHealthCheckup(tblHealthCheckup);

        if (HttpContext.Current.Session["UpdateHealthCheckup"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully.");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully.");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        //dt = cmsbal.GetHealthCheckup();
        dt = cmsbal.AdminGetHealthCheckup();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtPackageName.Text = string.Empty;        
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblHealthCheckup.Health_Id = id;
        cmsbal.DeleteHealthCheckup(tblHealthCheckup);

        if (HttpContext.Current.Session["DeleteHealthCheckup"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblHealthCheckup.Health_Id = id;
        cmsbal.GetBYHealthCheckup(tblHealthCheckup);

        hdnId.Value = Convert.ToInt32(tblHealthCheckup.Health_Id).ToString();
        txtPackageName.Text = tblHealthCheckup.Package_Name;

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtPackageName.Text == "" && txtPackageName.Text.Equals(""))
        {
            msg = "Package Name ,";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtPackageName.Text == "" && txtPackageName.Text.Equals(""))
        {
            msg = "Package Name ,";
            IsOk = false;
        }


        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}