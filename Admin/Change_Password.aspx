﻿<%@ Page Title="CHANGE PASSWORD" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="Change_Password.aspx.cs" Inherits="admin_Change_Password" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" Runat="Server">
    <div class="page-header">CHANGE PASSWORD</div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Old Password<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtOldPwd" TextMode="Password" CssClass="textbox" runat="server" 
            Width="373px" />
        <br />
        <label class="control-label">
           New Password<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtNewPwd" TextMode="Password"  CssClass="textbox" runat="server" 
            Width="373px" />
        <br />        
        <asp:Button ID="btnSave" Text="Change Password" CssClass="button" runat="server" OnClick="btnSave_Click" />
       
    </div>
</asp:Content>

