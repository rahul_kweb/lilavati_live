﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="request_an_estimate.aspx.cs" Inherits="Admin_Appointment" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .viewdetails {
            color: #48524A;
        }

            .viewdetails:hover {
                color: #04869A;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Request An Estimate
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="20" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PATIENTNAME" HeaderText="Patient Name" />
                <asp:BoundField DataField="SPECIALITY" HeaderText="Speciality" />
                <asp:BoundField DataField="DOCTOR" HeaderText="Doctor" />            
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <a href='mailto:<%#Eval("EMAILID") %>'><%#Eval("EMAILID") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MOBILENO" HeaderText="Mobile" />
                <asp:TemplateField HeaderText="View Details">
                    <ItemTemplate>
                        <a href='request_an_estimate_details.aspx?<%#Eval("ID","AppId={0}") %>' class="viewdetails">View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>


</asp:Content>

