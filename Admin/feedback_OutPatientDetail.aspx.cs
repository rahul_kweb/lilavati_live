﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    FeedbackOutPatient tblFeedbackOutPatient = new FeedbackOutPatient();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewDetail();
        }
    }

    public void ViewDetail()
    {
        tblFeedbackOutPatient.Feed_Opd_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.DetailsFeedBackOutPatient(tblFeedbackOutPatient);

        lblFirstName.Text = tblFeedbackOutPatient.FirstName;
        lblLastName.Text = tblFeedbackOutPatient.LastName;
        lblLhno.Text = tblFeedbackOutPatient.Lh_No;
        lblNameOfDoctor.Text = tblFeedbackOutPatient.NameOfDoctor;
        lblContactNo.Text = tblFeedbackOutPatient.PhoneNo;
        lblEmailId.Text = tblFeedbackOutPatient.EmailId;
        lblHowDidYouComeToKnow.Text = tblFeedbackOutPatient.HowDidYouComeToKnow;
        lblTimeTakenByAppointment.Text = tblFeedbackOutPatient.TimeTakenByAppointment;
        lblGuidanceAndInformation.Text = tblFeedbackOutPatient.GuidanceAndInformation;
        lblCourtesyAndFriendlinessOPD.Text = tblFeedbackOutPatient.CourtesyAndFriendlinessOPD;
        lblTimeTakenToCompleteRegistration.Text = tblFeedbackOutPatient.TimeTakenToCompleteRegistration;
        lblTimeTakenToCompleteBilling.Text = tblFeedbackOutPatient.TimeTakenToCompleteBilling;
        lblCourtesyAndInformation.Text = tblFeedbackOutPatient.CourtesyAndInformation;
        lblWaitingTimeToSeeDoctor.Text = tblFeedbackOutPatient.WaitingTimeToSeeDoctor;
        lblClarityAndInformation.Text = tblFeedbackOutPatient.ClarityAndInformation;
        lblCareAndAttention.Text = tblFeedbackOutPatient.CareAndAttention;
        lblBehaviorAndAttitude.Text = tblFeedbackOutPatient.BehaviorAndAttitude;
        lblRespectForYourPrivacy.Text = tblFeedbackOutPatient.RespectForYourPrivacy;
        lblCareOfferedDuringProc.Text = tblFeedbackOutPatient.CareOfferedDuringProc;
        lblWaitingTimeForTheTest.Text = tblFeedbackOutPatient.WaitingTimeForTheTest;
        lblExperienceWithTechnical.Text = tblFeedbackOutPatient.ExperienceWithTechnical;
        lblCourtesyAndFriendlinessStaff.Text = tblFeedbackOutPatient.CourtesyAndFriendlinessStaff;
        lblComfortOfWaitingArea.Text = tblFeedbackOutPatient.ComfortOfWaitingArea;
        lblCleanlinessInOPD.Text = tblFeedbackOutPatient.CleanlinessInOPD;
        lblCleanlinessOfToilets.Text = tblFeedbackOutPatient.CleanlinessOfToilets;
        lblHowWouldYouRate.Text = tblFeedbackOutPatient.HowWouldYouRate;
        lblWhatDidYouLike.Text = tblFeedbackOutPatient.WhatDidYouLIke;
        lblWhatDidYouLikeLeast.Text = tblFeedbackOutPatient.WhatDidYouLikeLeast;
        lblAnysuggestionYouWouldLike.Text = tblFeedbackOutPatient.AnysuggestionYouWouldLike;
        lblDate.Text = tblFeedbackOutPatient.Date;
    }

}