﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_admin : System.Web.UI.MasterPage
{
    public string Currentpage { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

        Currentpage = HttpContext.Current.Request.Url.AbsolutePath;

       // Response.Write(Currentpage + "sunil");

        if (Currentpage == "/admin/index.aspx")
        {

            divsuperadmin.Attributes.Add("style", "display:block");
            divappointmentadmin.Attributes.Add("style", "display: none");
        }
        else if (Currentpage == "/admin/AppointmentHistory.aspx")
        {
            divsuperadmin.Attributes.Add("style", "display:none");
            divappointmentadmin.Attributes.Add("style", "display:block");
        }
        else
        {
            divsuperadmin.Attributes.Add("style", "display:block");
            divappointmentadmin.Attributes.Add("style", "display:none");
        }


    }
}
