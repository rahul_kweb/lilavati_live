﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="feedback_HealthCheckupDetail.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Feedback Health Check-Up Details
    </div>
    <div class="form-box" style="width: 800px;">
        <table class="gridtable">
            <tr>
                <td>First Name</td>
                <td>
                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>LH No.</td>
                <td>
                    <asp:Label ID="lblLhno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Date</td>
                <td>
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Contact No.</td>
                <td>
                    <asp:Label ID="lblContactNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email Id</td>
                <td>
                    <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Health Checkup scheme availed</td>
                <td>
                    <asp:Label ID="lblHealthCheckup" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>HOW DID YOU HEAR ABOUT LILAVATI HOSPITAL HEALTH CHECK UP?</td>
                <td>
                    <asp:Label ID="lblHowDidYouCome" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>How would you rate the way your appointment call was handled?</td>
                <td>
                    <asp:Label ID="lblAppointmentHowwouldyourate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>How was your document formalities and queries handled during Registration and payment?</td>
                <td>
                    <asp:Label ID="lblRegistrationHowwasyourdocument" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Pathology (Blood Collection)</td>
                <td>
                    <asp:Label ID="lblTestsPathologyBlood" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)	
                </td>
                <td>
                    <asp:Label ID="lblTestsRadiology_Xray" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Cardiology (ECG, Stress test, 2D Echo, etc)
                </td>
                <td>
                    <asp:Label ID="lblTestsCardiologyECG" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Spirometry
                </td>
                <td>
                    <asp:Label ID="lblTestsSpirometry" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>General Physician
                </td>
                <td>
                    <asp:Label ID="lblConsultationGeneralPhysician" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Gynaec/ Surgeon</td>
                <td>
                    <asp:Label ID="lblConsultationGynaec" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Ophthalmology</td>
                <td>
                    <asp:Label ID="lblConsultationOphthalmology" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>ENT</td>
                <td>
                    <asp:Label ID="lblConsultationENT" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Dental</td>
                <td>
                    <asp:Label ID="lblConsultationDental" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Skin</td>
                <td>
                    <asp:Label ID="lblConsultationSkin" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Dietition</td>
                <td>
                    <asp:Label ID="lblConsultationDietition" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Overall hygiene and cleanliness of the health check up department.</td>
                <td>
                    <asp:Label ID="lblHousekeepingOverallhygiene" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>STAFF</td>
                <td>
                    <asp:Label ID="lblStaff" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>FOOD & BEVERAGES</td>
                <td>
                    <asp:Label ID="lblFoodBeverages" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>RESPECT FOR YOUR PRIVACY.</td>
                <td>
                    <asp:Label ID="lblRespectforyour" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>TIME TAKEN FOR HEALTH CHECK UP AS AGAINST YOUR EXPECTATION.</td>
                <td>
                    <asp:Label ID="lblTimetakenforhealthcheck" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>HOW WOULD YOU RATE YOUR OVERALL EXPERIENCE AT LILAVATI HOSPITAL DURING HEALTH CHECK UP.</td>
                <td>
                    <asp:Label ID="lblHowwouldyourate" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>What did you like the most about Lilavati hospital? </td>
                <td>
                    <asp:Label ID="lblWhatdidYouLike" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>What did you like the least about Lilavati hospital? </td>
                <td>
                    <asp:Label ID="lblWhatDidYouLikeLeast" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Any suggestion you would like to put forward which will help us to serve you better</td>
                <td>
                    <asp:Label ID="lblAnySuggestionYou" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Would you like to mention of our services for it’s outstanding quality and/ recommend any staff for their outstanding Performance? </td>
                <td>
                    <asp:Label ID="lblWouldYouLikeTOMention" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

