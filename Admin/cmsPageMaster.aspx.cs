﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Job_Area : System.Web.UI.Page
{
    CMSPageMaster tblcmsPageMaster = new CMSPageMaster();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        tblcmsPageMaster.PageName = txtPageName.Text.Trim();
        cmsbal.SaveCMSPageMaster(tblcmsPageMaster);

        if (HttpContext.Current.Session["SaveCmsPageMAster"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        tblcmsPageMaster.PageId = Convert.ToInt32(hdnId.Value);
        tblcmsPageMaster.PageName = txtPageName.Text.Trim();
        cmsbal.UpdateCMSPageMaster(tblcmsPageMaster);

        if (HttpContext.Current.Session["UpdateCmsPageMAster"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully.");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully.");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetCmsPageMaster();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtPageName.Text = string.Empty;        
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblcmsPageMaster.PageId = id;
        cmsbal.DeleteCMSPageMaster(tblcmsPageMaster);

        if (HttpContext.Current.Session["DeleteCmsPageMAster"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblcmsPageMaster.PageId = id;
        cmsbal.GetBYCmsPageMaster(tblcmsPageMaster);

        hdnId.Value = Convert.ToInt32(tblcmsPageMaster.PageId).ToString();
        txtPageName.Text = tblcmsPageMaster.PageName;

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtPageName.Text == "" && txtPageName.Text.Equals(""))
        {
            msg = "Job Area ,";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtPageName.Text == "" && txtPageName.Text.Equals(""))
        {
            msg = "Job Area ,";
            IsOk = false;
        }


        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}