﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Appointment : AdminPage
{
    RegistrationTable tbl = new RegistrationTable();
    AppointmentTable apptbl = new AppointmentTable();
    BAL bal = new BAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAppointmentGrid();
        }
    }

    public void BindAppointmentGrid()
    {
        gdView.Columns[0].Visible = true;
        gdView.DataSource = bal.AdminGetAppointment();
        gdView.DataBind();
        gdView.Columns[0].Visible = false;
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        apptbl.App_Id_bint = int.Parse(id.ToString());
        bal.ClosedAppointment(apptbl);

        if (HttpContext.Current.Session["ClosesMsg"] == "Success")
        {
            MyMessageBox1.ShowSuccess("Appointment Closed Successfully");
            BindAppointmentGrid();
        }
        else
        {
            MyMessageBox1.ShowError("Appointment Could Not Closed Successfully");
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindAppointmentGrid();
    }
}