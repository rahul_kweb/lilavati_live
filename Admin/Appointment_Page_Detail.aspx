﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Appointment_Page_Detail.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Appointment Details
    </div>
    <div class="form-box" style="width: 350px;">
        <table class="gridtable">
            <tr>
                <td colspan="2" align="center">
                    <h1>Welcome
                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                    </h1>
                </td>
            </tr>
            <tr>
                <td>Speciality</td>
                <td>
                    <asp:Label ID="lblSpeciality" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Doctor Name
                </td>
                <td>
                    <asp:Label ID="lblDoctorName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>App Day & Timming
                </td>
                <td>
                    <asp:Label ID="lblAvailableTiming" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Appointment Date
                </td>
                <td>
                    <asp:Label ID="lblAppointmentDate" runat="server"></asp:Label>
                </td>
            </tr>         
            <tr>
                <td>Patient Name</td>
                <td>
                    <asp:Label ID="lblPatientName" runat="server"></asp:Label>
                </td>
            </tr>          
            <tr>
                <td>Email</td>
                <td>
                    <asp:Label ID="lblEmailID" runat="server"></asp:Label>
                </td>
            </tr>           
            <tr>
                <td>Mobile No</td>
                <td>
                    <asp:Label ID="lblMobileNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Address</td>
                <td>
                    <asp:Label ID="lblAddress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Country</td>
                <td>
                    <asp:Label ID="lblCountry" runat="server"></asp:Label>
                </td>
            </tr>            
            <tr>
                <td>State</td>
                <td>
                    <asp:Label ID="lblState" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>City</td>
                <td>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Pincode</td>
                <td>
                    <asp:Label ID="lblPincode" runat="server"></asp:Label>
                </td>
            </tr>           
             <tr>
                <td>Visit</td>
                <td>
                    <asp:Label ID="lblVisit" runat="server"></asp:Label>
                </td>
            </tr>           
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

