﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Admin_Registration : AdminPage
{
    LHMTSubscribe tblLHMTSubscribe = new LHMTSubscribe();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_WriteToUs 'GET'");

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);

        using (SqlCommand cmd = new SqlCommand("Proc_WriteToUs"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", Id);
            if (utility.Execute(cmd))
            {
                BindGrid();
                MyMessageBox1.ShowSuccess("Record deleted Successfully.");
            }
            else
            {
                MyMessageBox1.ShowSuccess("Record Could Not delete Successfully.");
            }
        }
       
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
   

}