﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="photo_gallery.aspx.cs" Inherits="Admin_photo_gallery" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.tooltip.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".gridImages").tooltip({
                track: true,
                delay: 0,
                showURL: false,
                fade: 100,
                bodyHandler: function () {
                    return $($(this).next().html());
                },
                showURL: false
            });
        })

    </script>

    <style type="text/css">
        #tooltip {
            position: absolute;
            z-index: 3000;
            border: 1px solid #111;
            background-color: #FEFFFF;
            padding: 5px;
            opacity: 1.55;
        }

            #tooltip h3, #tooltip div {
                margin: 0;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Photo Gallary
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Tittle :            
        </label>
        <asp:TextBox ID="txtTittle" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Image<span class="required">*</span> :            
        </label>
        <asp:FileUpload ID="fileUploadImage" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:Image ID="imgpreview" runat="server" Width="300" Height="200" Visible="false" />
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btnClear" runat="server" Text="Cancel" CssClass="button" OnClick="btnClear_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="10" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TITTLE" HeaderText="Tittle" />
            <asp:TemplateField HeaderText="Images">
                <ItemTemplate>
                    <img src='<%#Eval("IMAGE","../uploads/gallery/{0}") %>' width="300" height="200" class="gridImages"></img>
                    <div id="tooltip" style="display: none;">
                        <table>
                            <tr>
                                <td>
                                    <asp:Image ID="imgview" ImageUrl='<%#Eval("IMAGE","../uploads/gallery/{0}") %>' Width="500" Height="500" runat="server" /></td>
                            </tr>
                        </table>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

