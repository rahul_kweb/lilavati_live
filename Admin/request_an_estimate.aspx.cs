﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Appointment : AdminPage
{
    DataTable dt = new DataTable();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAppointmentGrid();
        }
    }

    public void BindAppointmentGrid()
    {
        dt = utility.Display("EXEC Proc_Request_Estimate 'GET'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        using (SqlCommand cmd = new SqlCommand("Proc_Request_Estimate"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", id);
            if (utility.Execute(cmd))
            {
                BindAppointmentGrid();
                MyMessageBox1.ShowSuccess("Request Delete Successfully.");
            }
            else
            {
                MyMessageBox1.ShowError("Request Could Not Delete Successfully.");
            }
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindAppointmentGrid();
    }
}