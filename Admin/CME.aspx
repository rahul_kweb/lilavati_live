﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="CME.aspx.cs" Inherits="Admin_CME" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        CME
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 400px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Event Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtEventName" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" Visible="false" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" Visible="false" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="CME_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EVENTNAME" HeaderText="Event Name" />
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <a href='CME_details.aspx?Id=<%#Eval("CME_ID") %>' class="button-grid" style="width: 110px">AddUpdateCME</a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete" Visible="false">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

