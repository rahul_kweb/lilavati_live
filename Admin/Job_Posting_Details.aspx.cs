﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewDetail();
        }
    }

    public void ViewDetail()
    {       

        DataTable dt = new DataTable();
        dt = utility.Display("Execute PROC_JOBAPPLIED 'GET_BY_ID','" + Request.QueryString["Id"] + "'");

        lblPosition.Text = dt.Rows[0]["Postion_Name_vcr"].ToString();
        lblDepartment.Text = dt.Rows[0]["Spec_Name_vcr"].ToString();
        lblFirstName.Text = dt.Rows[0]["First_Name_vcr"].ToString();
        lblLastName.Text = dt.Rows[0]["Last_Name_vcr"].ToString();
        lblDob.Text = dt.Rows[0]["DOB"].ToString();
        lblGender.Text = dt.Rows[0]["Gender"].ToString();
        lblContactNo.Text = dt.Rows[0]["Phone"].ToString();
        lblEmailId.Text = dt.Rows[0]["EmailId"].ToString();
        lblResidentof.Text = dt.Rows[0]["Resident_of_vcr"].ToString();
        lblPresentlyworkinglocation.Text = dt.Rows[0]["Presently_working_location"].ToString();
        lblRelevantExperience.Text = dt.Rows[0]["Relevant_Exp"].ToString();
        lblTotalExperience.Text = dt.Rows[0]["Total_Exp"].ToString();
        lblMonthlyexpectedSalary.Text = dt.Rows[0]["Monthly_expected_Salary"].ToString();
        lblQualification.Text = dt.Rows[0]["Qualification"].ToString();
        lblHostelAccommodationrequired.Text = dt.Rows[0]["Hostel_Accommodation_required"].ToString();
        lblRegistrationwithMMCMNC.Text = dt.Rows[0]["Registration_with_MMC"].ToString();     
    } 
    
}