﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Admin_TariffHeading : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Save")
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Tariff_Heading"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "ADD");
                cmd.Parameters.AddWithValue("@HEADING", txtheading.Text.Trim());
                cmd.Parameters.AddWithValue("@DISCRIPTION", txtdisc.Text.Trim());
                if (utility.Execute(cmd))
                {
                    BindGrid();
                    Reset();
                    MyMessageBox1.ShowSuccess("Record insert successfully");
                }
                else
                {
                    MyMessageBox1.ShowSuccess("Record Could Not insert successfully");
                }
            }
        }
        else
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Tariff_Heading"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                cmd.Parameters.AddWithValue("@HEADING_ID", hdnId.Value);
                cmd.Parameters.AddWithValue("@HEADING", txtheading.Text.Trim());
                cmd.Parameters.AddWithValue("@DISCRIPTION", txtdisc.Text.Trim());
                if (utility.Execute(cmd))
                {
                    BindGrid();
                    Reset();
                    MyMessageBox1.ShowSuccess("Record update successfully");
                }
                else
                {
                    MyMessageBox1.ShowSuccess("Record Could Not update successfully");
                }
            }
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Tariff_Heading 'GET'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
            GetDetails();  
        }
    }

    public void Reset()
    {
        txtheading.Text = string.Empty;
        txtdisc.Text = string.Empty;
        //btnsave.Text = "Save";
        btnsave.Visible = false;
        divheading.Visible = false;
        btncancel.Visible = false;
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        using (SqlCommand cmd = new SqlCommand("Proc_Tariff_Heading"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@HEADING_ID", id);
            if (utility.Execute(cmd))
            {
                BindGrid();
                Reset();
                MyMessageBox1.ShowSuccess("Record delete successfully");
            }
            else
            {
                MyMessageBox1.ShowSuccess("Record Could Not delete successfully");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Tariff_Heading 'GET_BY_ID','" + id + "'");
        if (dt.Rows.Count > 0)
        {
            hdnId.Value = dt.Rows[0]["HEADING_ID"].ToString();
            txtheading.Text = dt.Rows[0]["HEADING"].ToString();
            txtdisc.Text = dt.Rows[0]["DISCRIPTION"].ToString();
            btnsave.Visible = true;
            divheading.Visible = true;
            btncancel.Visible = true;
            btnsave.Text = "Update";
        }
    }

    protected void rbdActive_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((RadioButton)sender).NamingContainer);
        HiddenField id = (HiddenField)row.Cells[0].FindControl("hdid");
        string value = id.Value;
        using (SqlCommand cmd = new SqlCommand("Proc_Tariff_Heading"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "SHOW_HEADING");
            cmd.Parameters.AddWithValue("@HEADING_ID", value);
            if (utility.Execute(cmd))
            {
                BindGrid();
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "javascript:alert('Record Showing SuccessFully');", true);
            }
        }
    }
    protected void rbdInActive_CheckedChanged(object sender, EventArgs e)
    {
        GridViewRow row = (GridViewRow)(((RadioButton)sender).NamingContainer);
        HiddenField id = (HiddenField)row.Cells[0].FindControl("hdid");
        string value = id.Value;
        using (SqlCommand cmd = new SqlCommand("Proc_Tariff_Heading"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "HIDE_HEADING");
            cmd.Parameters.AddWithValue("@HEADING_ID", value);
            if (utility.Execute(cmd))
            {
                BindGrid();
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "javascript:alert('Record Hide SuccessFully');", true);
            }
        }
    }

    public void GetDetails()
    {
        foreach (GridViewRow row in gdView.Rows)
        {
            //Find the Radio button control
            RadioButton rb = (RadioButton)row.FindControl("rbdActive");
            RadioButton rb1 = (RadioButton)row.FindControl("rbdInActive");
            string value = rb.Text;
            if (rb.Checked == true)
            {
                rb.ForeColor = System.Drawing.Color.Green;
            }
            if (rb1.Checked == true)
            {
                rb1.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
}