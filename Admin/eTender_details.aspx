﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="eTender_details.aspx.cs" Inherits="eTender_details" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<style type="text/css">
        .auto-style1 {
            width: 50%;
            /*border: 1px solid;*/
            margin-left: 275px;
            border: 1px solid black;
            border-collapse: collapse;
        }

      
    </style>--%>

    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }

        thead tr th:first-child,
        tbody tr td:first-child {
            width: 8em;
            min-width: 18em;
            max-width: 18em;
            word-break: break-all;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">

    <div class="page-header">
        eTender Details
    </div>
    <div class="form-box" style="width: 550px;">
        <table class="gridtable">
            <tr>
                <td>E Tender No.</td>
                <td>
                    <asp:Label ID="lblETenderNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Tender Name</td>
                <td>
                    <asp:Label ID="lblTenderName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Tender Start Date & Time</td>
                <td>
                    <asp:Label ID="lblTenderStartDateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Tender End Date & Time</td>
                <td>
                    <asp:Label ID="lblTenderEndDateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Type of Tender</td>
                <td>
                    <asp:Label ID="lblTypeofTender" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Last Tender Receive Date & Time</td>
                <td>
                    <asp:Label ID="lblLastTenderReceiveDateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Tender Opening Date & Time</td>
                <td>
                    <asp:Label ID="lblTenderOpeningDateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Department</td>
                <td>
                    <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Category</td>
                <td>
                    <asp:Label ID="lblCategory" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>Tender Description</td>
                <td>
                    <asp:Label ID="lblTenderDescription" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Remarks</td>
                <td>
                    <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>IsActive</td>
                <td>
                    <asp:Label ID="lblActive" runat="server"></asp:Label>
                </td>
            </tr>


            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClick="btnBack1_Click" />
                </td>
            </tr>
        </table>
    </div>



</asp:Content>

