﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    FeedBackHealthCheckUp tblFeedBackHealthCheckUp = new FeedBackHealthCheckUp();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewDetail();
        }
    }

    public void ViewDetail()
    {

        tblFeedBackHealthCheckUp.ID = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.DetailsFeedBackHealthCheckUp(tblFeedBackHealthCheckUp);

        lblFirstName.Text = tblFeedBackHealthCheckUp.FirstName;
        lblLastName.Text = tblFeedBackHealthCheckUp.LastName;
        lblLhno.Text = tblFeedBackHealthCheckUp.Lh_No;
        lblDate.Text = tblFeedBackHealthCheckUp.Date;
        lblContactNo.Text = tblFeedBackHealthCheckUp.Contact;
        lblEmailId.Text = tblFeedBackHealthCheckUp.EmailId;
        lblHealthCheckup.Text = tblFeedBackHealthCheckUp.HealthCheckup;

        lblHowDidYouCome.Text = tblFeedBackHealthCheckUp.HowDidYouComeToKnow;
        lblAppointmentHowwouldyourate.Text = tblFeedBackHealthCheckUp.AppointmentHowwouldyourate;
        lblRegistrationHowwasyourdocument.Text = tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument;

        lblTestsPathologyBlood.Text = tblFeedBackHealthCheckUp.TestsPathologyBlood;
        lblTestsRadiology_Xray.Text = tblFeedBackHealthCheckUp.TestsRadiology_Xray;
        lblTestsCardiologyECG.Text = tblFeedBackHealthCheckUp.TestsCardiologyECG;
        lblTestsSpirometry.Text = tblFeedBackHealthCheckUp.TestsSpirometry;

        lblConsultationGeneralPhysician.Text = tblFeedBackHealthCheckUp.ConsultationGeneralPhysician;
        lblConsultationGynaec.Text = tblFeedBackHealthCheckUp.ConsultationGynaec;
        lblConsultationOphthalmology.Text = tblFeedBackHealthCheckUp.ConsultationOphthalmology;
        lblConsultationENT.Text = tblFeedBackHealthCheckUp.ConsultationENT;
        lblConsultationDental.Text = tblFeedBackHealthCheckUp.ConsultationDental;
        lblConsultationSkin.Text = tblFeedBackHealthCheckUp.ConsultationSkin;
        lblConsultationDietition.Text = tblFeedBackHealthCheckUp.ConsultationDietition;

        lblHousekeepingOverallhygiene.Text = tblFeedBackHealthCheckUp.HousekeepingOverallhygiene;
        lblStaff.Text = tblFeedBackHealthCheckUp.Staff;
        lblFoodBeverages.Text = tblFeedBackHealthCheckUp.FoodBeverages;
        lblRespectforyour.Text = tblFeedBackHealthCheckUp.Respectforyour;
        lblTimetakenforhealthcheck.Text = tblFeedBackHealthCheckUp.Timetakenforhealthcheck;
        lblHowwouldyourate.Text = tblFeedBackHealthCheckUp.Howwouldyourate;

        lblWhatdidYouLike.Text = tblFeedBackHealthCheckUp.WhatdidYouLike;
        lblWhatDidYouLikeLeast.Text = tblFeedBackHealthCheckUp.WhatDidYouLikeLeast;
        lblAnySuggestionYou.Text = tblFeedBackHealthCheckUp.AnySuggestionYou;
        lblWouldYouLikeTOMention.Text = tblFeedBackHealthCheckUp.WouldYouLikeTOMention;      
            
    }   
           
}