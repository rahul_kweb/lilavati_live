﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string AppId = Request.QueryString["AppId"].ToString();
            AppViewDetails(AppId);
        }
    }

    public void AppViewDetails(string AppId)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Request_Estimate 'GET_BY_ID','" + AppId + "'");

        if (dt.Rows.Count > 0)
        {
            lblSpeciality.Text = dt.Rows[0]["SPECIALITY"].ToString();
            lblDoctorName.Text = dt.Rows[0]["DOCTOR"].ToString();
            lblPatientName.Text = dt.Rows[0]["PATIENTNAME"].ToString();
            lblEmailID.Text = dt.Rows[0]["EMAILID"].ToString();
            lblMobileNo.Text = dt.Rows[0]["MOBILENO"].ToString();
            lblTypeOfRoom.Text = dt.Rows[0]["TYPEOFROOM"].ToString();
            lblNationality.Text = dt.Rows[0]["NATIONALITY"].ToString();
            lblAge.Text = dt.Rows[0]["AGE"].ToString();
            lblGender.Text = dt.Rows[0]["GENDER"].ToString();
            lblDescription.Text = dt.Rows[0]["DESCRIPTION"].ToString();
            lblPastHistory.Text = dt.Rows[0]["PASTHISTORY"].ToString();
        }
    }


}