﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Job_Posting.aspx.cs" Inherits="Admin_Job_Posting" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Job Posting
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Job Area<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlJobArea" runat="server" CssClass="dropdown"></asp:DropDownList>
        <br />
        <label class="control-label">
            Position<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtPosition" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Specification<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtSpecification" runat="server" CssClass="textbox" TextMode="MultiLine" Height="80"></asp:TextBox>
        <label class="control-label">
            Vacancy<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtVacancy" runat="server" CssClass="textbox"></asp:TextBox>
        <br />      
        <label class="control-label">
            Duration<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtDuration" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:CalendarExtender runat="server" ID="CalenderExtender1" TargetControlID="txtDuration" Format="dd-MM-yyyy"></asp:CalendarExtender>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="POST_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="JOB_AREA" HeaderText="Job Area" />
            <asp:BoundField DataField="POSITION" HeaderText="Position" />
            <asp:BoundField DataField="SPECIFICATION" HeaderText="Specification" />
            <asp:BoundField DataField="DURATION" HeaderText="Duration" />
            <asp:BoundField DataField="VACANCY" HeaderText="Vacancy" />
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

