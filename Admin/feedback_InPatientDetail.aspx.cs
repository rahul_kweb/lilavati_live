﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegistrationDetails : AdminPage
{
    FeedBackInPatient tblFeedBackInPatient = new FeedBackInPatient();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewDetail();
        }
    }

    public void ViewDetail()
    {
        tblFeedBackInPatient.ID = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.DetailsFeedBackInPatient(tblFeedBackInPatient);

        lblFirstName.Text = tblFeedBackInPatient.FirstName;
        lblLastName.Text = tblFeedBackInPatient.LastName;
        lblLhno.Text = tblFeedBackInPatient.Lh_No;
        lblDateOfAdmission.Text = tblFeedBackInPatient.DateOfAdmission;
        lblContactNo.Text = tblFeedBackInPatient.PhoneNo;
        lblBedNo.Text = tblFeedBackInPatient.BedNo;
        lblDateOfDischarge.Text = tblFeedBackInPatient.DateOfDischarge;
        lblEmailId.Text = tblFeedBackInPatient.EmailId;
        lblHowDidYouComeToKnow.Text = tblFeedBackInPatient.HowDidYouComeToKnow;
        lblGuidanceSupport.Text = tblFeedBackInPatient.GuidanceSupport;
        lblCourtesyAndFriendliness.Text = tblFeedBackInPatient.CourtesyAndFriendliness;
        lblTimeTakenToComplete.Text = tblFeedBackInPatient.TimeTakenToComplete;
        lblClarityProvided.Text = tblFeedBackInPatient.ClarityProvided;
        lblCareAttention.Text = tblFeedBackInPatient.CareAttention;
        lblDoctorsEfforts.Text = tblFeedBackInPatient.DoctorsEfforts;
        lblCourtesyFriendnessNursing.Text = tblFeedBackInPatient.CourtesyFriendnessNursing;
        lblCareAndAttentionMedication.Text = tblFeedBackInPatient.CareAndAttentionMedication;
        lblResponseToYou.Text = tblFeedBackInPatient.ResponseToYou;
        lblRespectForPrivacy.Text = tblFeedBackInPatient.RespectForPrivacy;
        //lblCountesyOfStaff.Text = tblFeedBackInPatient.CountesyOfStaff;
        //lblClarityOfInstructions.Text = tblFeedBackInPatient.ClarityOfInstructions;
        lblQualityOfFood.Text = tblFeedBackInPatient.QualityOfFood;
        lblVarietyOfFood.Text = tblFeedBackInPatient.VarietyOfFood;
        lblPromptnessOfService.Text = tblFeedBackInPatient.PromptnessOfService;
        lblInformationGuidance.Text = tblFeedBackInPatient.InformationGuidance;
        lblCleanlinessOfRoom.Text = tblFeedBackInPatient.CleanlinessOfRoom;
        lblResponseToYouNeed.Text = tblFeedBackInPatient.ResponseToYouNeed;
        lblCourtesyAndHelpfulness.Text = tblFeedBackInPatient.CourtesyAndHelpfulness;
        lblCourtesyOfSecurity.Text = tblFeedBackInPatient.CourtesyOfSecurity;
        lblProfessionalism.Text = tblFeedBackInPatient.Professionalism;
        lblBillingCounseling.Text = tblFeedBackInPatient.BillingCounseling;
        lblPromptnessOfBilling.Text = tblFeedBackInPatient.PromptnessOfBilling;
        lblCountesyHelpfulness.Text = tblFeedBackInPatient.CountesyHelpfulness;
        lblPromptnessOfDischarge.Text = tblFeedBackInPatient.PromptnessOfDischarge;
        lblDischargeCounseling.Text = tblFeedBackInPatient.DischargeCounseling;
        lblConvenienceOfDischarge.Text = tblFeedBackInPatient.ConvenienceOfDischarge;
        lblHowWouldYouRate.Text = tblFeedBackInPatient.HowWouldYouRate;
        lblWhatDidYouLike.Text = tblFeedBackInPatient.WhatDidYouLike;
        lblWhatDidYouLikeLeast.Text = tblFeedBackInPatient.WhatDidYouLikeLeast;
        lblAnysuggestionYouWouldLike.Text = tblFeedBackInPatient.AnysuggestionYouWouldLike;
        lblWouldYouLikeToRecommend.Text = tblFeedBackInPatient.WouldYouLikeToRecommend;

        lblPathology_BloodCollection.Text = tblFeedBackInPatient.Pathology_BloodCollection;
        lblRadiology_X_ray.Text = tblFeedBackInPatient.Radiology_X_ray;
        lblCardiology_ECG_Stress_test.Text = tblFeedBackInPatient.Cardiology_ECG_Stress_test;
        lblNuclear_Dept_BoneScanLiverScan.Text = tblFeedBackInPatient.Nuclear_Dept_BoneScanLiverScan;
        lblTimetakenforTestRadiology_X_ray.Text = tblFeedBackInPatient.TimetakenforTestRadiology_X_ray;
        lblTimetakenforTestCardiology_ECG_Stress_test.Text = tblFeedBackInPatient.TimetakenforTestCardiology_ECG_Stress_test;
        lblTimetakenforTestNuclear_Dept_BoneScanLiverScan.Text = tblFeedBackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan;        
    }   

    protected void btnBack1_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Registration.aspx");
    }

    
}