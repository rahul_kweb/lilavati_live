﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Job_Area : System.Web.UI.Page
{
    Job_Area tblJobArea = new Job_Area();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        tblJobArea.Job_Areas = txtJobArea.Text.Trim();
        tblJobArea.Job_Position = txtPosition.Text.Trim();
        cmsbal.SaveJobArea(tblJobArea);

        if (HttpContext.Current.Session["SaveJobArea"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        tblJobArea.Job_Id = int.Parse(hdnId.Value.ToString());
        tblJobArea.Job_Areas = txtJobArea.Text.Trim();
        tblJobArea.Job_Position = txtPosition.Text.Trim();
        cmsbal.UpdateJobArea(tblJobArea);

        if (HttpContext.Current.Session["UpdateJobArea"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully.");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully.");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetJobArea();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void Reset()
    {
        txtJobArea.Text = string.Empty;
        txtPosition.Text = string.Empty;
        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblJobArea.Job_Id = id;
        cmsbal.DeleteJobArea(tblJobArea);

        if (HttpContext.Current.Session["DeleteJobArea"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblJobArea.Job_Id = id;
        cmsbal.GEtByJobArea(tblJobArea);

        hdnId.Value = Convert.ToInt32(tblJobArea.Job_Id).ToString();
        txtJobArea.Text = tblJobArea.Job_Areas;
        txtPosition.Text = tblJobArea.Job_Position;

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtJobArea.Text == "" && txtJobArea.Text.Equals(""))
        {
            msg = "Job Area ,";
            IsOk = false;
        }

        if (txtPosition.Text == "" && txtPosition.Text.Equals(""))
        {
            msg += "Position ";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtJobArea.Text == "" && txtJobArea.Text.Equals(""))
        {
            msg = "Job Area ,";
            IsOk = false;
        }

        if (txtPosition.Text == "" && txtPosition.Text.Equals(""))
        {
            msg += "Position ";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

}