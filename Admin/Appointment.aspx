﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="Appointment.aspx.cs" Inherits="Admin_Appointment" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .viewdetails {
            color: #48524A;
            text-decoration: none;
        }
        .viewdetails:hover {
            color: #04869A;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Appointment
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="30" AllowPaging="true" OnRowDeleting="gdView_RowDeleting" 
            onpageindexchanging="gdView_PageIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="App_Id_bint" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Speciality_vcr" HeaderText="Speciality" />
                <asp:BoundField DataField="Doctor_Name_vcr" HeaderText="Doctor Name" />
                <asp:BoundField DataField="App_Avai_Time" HeaderText="Available Timing" />
                <asp:BoundField DataField="App_Date" HeaderText="App Date" />
                <asp:BoundField DataField="App_Visit_Time" HeaderText="App Visit Time" />
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <%--<asp:BoundField DataField="Email" HeaderText="Email" />--%>
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <a href='mailto:<%#Eval("Email") %>'>
                            <%#Eval("Email") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="App_Status" HeaderText="App Staus"/>--%>
                <asp:TemplateField HeaderText="App Staus">
                    <ItemTemplate>
                        <a href="#" style='<%#Eval("App_Status").ToString()=="Cancelled"?"color:#A25020": ""%>;
                            <%#Eval("App_Status").ToString()=="Closed"?"color:#F44336": ""%>; <%#Eval("App_Status").ToString()=="Confirmed"?"color:#1FBB25": ""%>;
                            text-decoration: none'>
                            <%#Eval("App_Status") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="View Details">
                    <ItemTemplate>
                        <a href='AppointmentDetails.aspx?<%#Eval("App_Id_bint","AppId={0}") %>' class="viewdetails">
                            View Details</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="App Closed">
                    <ItemTemplate>
                        <asp:Button ID="btnclose" runat="server" Enabled='<%#Eval("App_Status").ToString()=="Cancelled"|| Eval("App_Status").ToString()=="Closed"?false:true %>'
                            CommandName="Delete" Style='<%#Eval("App_Status").ToString()=="Cancelled" || Eval("App_Status").ToString()=="Closed"?"color:#ccc": "" %>'
                            Text="Closed" CssClass="button" OnClientClick="if (!confirm('Are you sure do you want to closed appointment?')) return false;" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
