﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_DrNotAvailable :AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid(this.GetRecordForGrid());
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (validation())
            {



               

                if (btnSave.Text == "Save")
                {
                    string date = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
                    using (SqlCommand cmd = new SqlCommand("Proc_DrNotAvailabeTable"))
                    {

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PARA", "ADD");

                        cmd.Parameters.AddWithValue("@NOTAVAILBLEDATE", date);
                        if (utility.Execute(cmd))
                        {
                            Refresh();
                            BindGrid(this.GetRecordForGrid());
                            MyMessageBox1.ShowSuccess("Date Added SuccessFully!!");
                        }
                        else
                        {
                            MyMessageBox1.ShowError("Date  Could not added!!");
                        }
                    }
                }
                else
                {
                    string date1 = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
                    using (SqlCommand cmd = new SqlCommand("Proc_DrNotAvailabeTable"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                        cmd.Parameters.AddWithValue("@NOTAVAILABLEID", hdnId.Value);
                        cmd.Parameters.AddWithValue("@NOTAVAILBLEDATE", date1);
                        if (utility.Execute(cmd))
                        {
                            Refresh();
                            BindGrid(this.GetRecordForGrid());
                            MyMessageBox1.ShowSuccess("Date Update SuccessFully!!");
                        }
                        else
                        {
                            MyMessageBox1.ShowError("Date  Could not Update!!");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            // throw;
        }
    }

    public class NotAvailable
    {
        //public string _dates;
        //public int _id;

        public string _NOTAVAILBLEDATE;
        public int _NOTAVAILABLEID;

        public string NOTAVAILBLEDATE
        {
            get { return _NOTAVAILBLEDATE; }
            set { _NOTAVAILBLEDATE = value; }
        }
        public int NOTAVAILABLEID
        {
            get { return _NOTAVAILABLEID; }
            set { _NOTAVAILABLEID = value; }
        }
    }

    public List<NotAvailable> GetRecordForGrid()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("Execute Proc_DrNotAvailabeTable 'GET'", con);
        SqlDataReader dr = cmd.ExecuteReader();
        List<NotAvailable> nalist = new List<NotAvailable>();
        while (dr.Read())
        {
            NotAvailable na = new NotAvailable();

            na._NOTAVAILABLEID = int.Parse(dr["NOTAVAILABLEID"].ToString());
            na._NOTAVAILBLEDATE = dr["NOTAVAILBLEDATE"].ToString();
            nalist.Add(na);
        }
        con.Close();
        return nalist;
    }

    public void BindGrid(List<NotAvailable> list)
    {
        // foreach(string 
        gdView.Columns[0].Visible = true;
        gdView.DataSource = list;
        gdView.DataBind();
        gdView.Columns[0].Visible = false;
    }

    public bool validation()
    {
        bool IsOk = true;
        string msg = string.Empty;
        if (txtdate.Text == "")
        {
            msg = "Select Date";
            IsOk = false;
        }
        if (!IsOk)
        {
            MyMessageBox1.ShowError(msg);
        }
        return IsOk;
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        SqlCommand cmd = new SqlCommand("Execute Proc_DrNotAvailabeTable 'GET_BY_ID','" + id + "'");
        DataTable dt = new DataTable();
        dt = utility.Display(cmd);
        if (dt.Rows.Count > 0)
        {

            string date = dt.Rows[0]["NOTAVAILBLEDATE"].ToString();
            string date1 = DateTime.ParseExact(date, "yyyy-MM-dd", null).ToString("dd-MM-yyyy");
            txtdate.Text = date1;

            hdnId.Value = dt.Rows[0]["NOTAVAILABLEID"].ToString();
            btnSave.Text = "Update";
        }

    }

    public void Refresh()
    {
        txtdate.Text = string.Empty;
        btnSave.Text = "Save";
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Refresh();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);

        using (SqlCommand cmd = new SqlCommand("Proc_DrNotAvailabeTable"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@NOTAVAILABLEID", id);
            if (utility.Execute(cmd))
            {
                Refresh();
                BindGrid(this.GetRecordForGrid());
                MyMessageBox1.ShowSuccess("Date Delete SuccessFully!!");
            }
            else
            {
                MyMessageBox1.ShowError("Date  Could not Delete!!");
            }
        }
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid(this.GetRecordForGrid());
    }
}