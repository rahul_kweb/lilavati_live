﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_packages_details : System.Web.UI.Page
{
    EmpanelledDetails tblEmpanelledDetails = new EmpanelledDetails();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();

    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualImage = "~/uploads/Empanelled_Companies/";
    string TabName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //string Id = Request.QueryString["Id"].ToString();
        if (!IsPostBack)
        {
            BindGrid();
            CompTabName();
        }
    }   

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if(CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        if (fileUploadLogo.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadLogo.FileName);
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only (.jpg , .jpeg , .png , .bmp , .gif) Image files.");
                return;
            }

            CompTabName();
            //TabName = TabName.Replace(" ", "");

            StringBuilder strbul = new StringBuilder(TabName);
            strbul.Replace(" ", "");
            strbul.Replace("'", "");

            MainImage = utility.GetUniqueName(VirtualImage, strbul + "Img", ext, this, false);
            fileUploadLogo.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblEmpanelledDetails.Logo = MainImage + ext;
        }

        tblEmpanelledDetails.Tab_Id = int.Parse(Request.QueryString["Id"].ToString());
        tblEmpanelledDetails.Name = txtCompanyName.Text.Trim();
        cmsbal.SaveEmpanelledDetails(tblEmpanelledDetails);

        if (HttpContext.Current.Session["SaveEmpanelledDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not insert successfully");
        }
    }

    public void UpdateRecord()
    {
        if (fileUploadLogo.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadLogo.FileName);
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only (.jpg , .jpeg , .png , .bmp , .gif) Image files.");
                return;
            }

            CompTabName();
            //TabName = TabName.Replace(" ", "");

            StringBuilder strbul = new StringBuilder(TabName);
            strbul.Replace(" ", "");
            strbul.Replace("'", "");

            MainImage = utility.GetUniqueName(VirtualImage, strbul + "Img", ext, this, false);
            fileUploadLogo.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblEmpanelledDetails.Logo = MainImage + ext;
        }

        tblEmpanelledDetails.Emp_Id = Convert.ToInt32(hdnId.Value);
        tblEmpanelledDetails.Name = txtCompanyName.Text.Trim();
        cmsbal.UpdateEmpanelledDetails(tblEmpanelledDetails);

        if (HttpContext.Current.Session["UpdateEmpanelledDetails"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowSuccess("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        tblEmpanelledDetails.Tab_Id = int.Parse(Request.QueryString["Id"].ToString());
        dt = cmsbal.GetEmpanelledDetails(tblEmpanelledDetails);
               
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
    }

    public void Reset()
    {
        txtCompanyName.Text = string.Empty;
        imgLogo.Visible = false;  

        btnSave.Text = "Save";
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblEmpanelledDetails.Emp_Id = id;
        cmsbal.DeleteEmpanelledDetails(tblEmpanelledDetails);

        if (HttpContext.Current.Session["DeleteEmpanelledDetails"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblEmpanelledDetails.Emp_Id = id;
        cmsbal.GetBYEmpanelledDetails(tblEmpanelledDetails);

        hdnId.Value = Convert.ToInt32(tblEmpanelledDetails.Emp_Id).ToString();
        txtCompanyName.Text = tblEmpanelledDetails.Name;

        imgLogo.Visible = true;
        imgLogo.ImageUrl = string.Format("../uploads/Empanelled_Companies/{0}", tblEmpanelledDetails.Logo).ToString(); 

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;


        if (txtCompanyName.Text == "" && txtCompanyName.Text.Equals(""))
        {
            IsOk = false;
            msg += "Tab Name ,";
        }

        if (!fileUploadLogo.HasFile)
        {
            IsOk = false;
            msg += "Company Logo ,";
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (txtCompanyName.Text == "" && txtCompanyName.Text.Equals(""))
        {
            IsOk = false;
            msg += "Tab Name ,";
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("empanelled_companies.aspx");
    }

    public void CompTabName()
    {
        tblEmpanelledDetails.Tab_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.GetTabNameEmpanelledDetails(tblEmpanelledDetails);

        TabName = tblEmpanelledDetails.Tab_Name;
        lblTittle.Text = tblEmpanelledDetails.Tab_Name;
    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
}