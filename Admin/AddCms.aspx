﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="AddCms.aspx.cs" Inherits="Admin_AddCms" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
       ADD CMS
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Heading<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtHeading" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Page Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtPageName" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btnsave" runat="server" Text="Save" CssClass="button" OnClick="btnsave_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="20" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" />
            <asp:BoundField DataField="HEADING" HeaderText="Heading" />
            <asp:BoundField DataField="PAGE_NAME" HeaderText="Page Name" />
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
    <asp:HiddenField ID="hdnforedit" runat="server" />
</asp:Content>

