﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Visa_Invitation_Details.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Visa Invitation Details
    </div>
    <div class="form-box" style="width: 800px;">
        <table class="gridtable">
            <tr>
                <td>Patient's Name</td>
                <td>
                    <asp:Label ID="lblPatientName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email Id</td>
                <td>
                    <asp:Label ID="lblEmailID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Contact No.</td>
                <td>
                    <asp:Label ID="lblContactNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Nationality</td>
                <td>
                    <asp:Label ID="lblNationality" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Passport No.</td>
                <td>
                    <asp:Label ID="lblPassportNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Passport Copy</td>
                <td>
                    <asp:HyperLink ID="hyplinkPatientPasswordCopy" runat="server" Target="_blank" Text="View Passport Copy"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>Your country Hospital Name</td>
                <td>
                    <asp:Label ID="lblHospitalName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Your country Doctor Name</td>
                <td>
                    <asp:Label ID="lblTreatmentDocName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Reports Copy</td>
                <td>
                    <asp:HyperLink ID="hyplinkTreatment" runat="server" Target="_blank" Text="View Reports Copy"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>Your country Provisional Diagnosis & Name of Ailment</td>
                <td>
                    <asp:Label ID="lblProvisionalDiagnosis" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Lilavati Hospital Purpose of Visit</td>
                <td>
                    <asp:Label ID="lblPurposeOfVisit" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Lilavati Hospital Doctor's Name	
                </td>
                <td>
                    <asp:Label ID="lblLilavatiDoctorName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Lilavati Hospital Preferred Date of Visit
                </td>
                <td>
                    <asp:Label ID="lblPreferredDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Lilavati Hospital Duration of Treatment (approx.)
                </td>
                <td>
                    <asp:Label ID="lblDurationOfTreatment" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Lilavati Hospital Coming for Followup
                </td>
                <td>
                    <asp:Label ID="lblComingForFollowup" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="trDOV" runat="server">
                <td>Lilavati Hospital Date of Last Visit</td>
                <td>
                    <asp:Label ID="lblDateOfLastVisit" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>No. of Attendant</td>
                <td>
                    <asp:Label ID="lblNoOfAttendant" runat="server"></asp:Label>
                </td>
            </tr>
            <div id="divFirst" runat="server">
                <tr>
                    <td>1st Attendant Name</td>
                    <td>
                        <asp:Label ID="lblFirstAttName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>1st Relationship with Patient</td>
                    <td>
                        <asp:Label ID="lblFirstRelationship" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>1st Passport No.</td>
                    <td>
                        <asp:Label ID="lblFirstPassportNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>1st Passport Copy</td>
                    <td>
                        <asp:HyperLink ID="hyplinkFirstFileUploadCopy" runat="server" Target="_blank" Text="View 1st Passport Copy"></asp:HyperLink>
                    </td>
                </tr>
            </div>
            <div id="divSecond" runat="server">
                <tr>
                    <td>2nd Attendant Name</td>
                    <td>
                        <asp:Label ID="lblSecondAttName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>2nd Relationship with Patient</td>
                    <td>
                        <asp:Label ID="lblSecondRelationship" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>2nd Passport No.</td>
                    <td>
                        <asp:Label ID="lblSecondPassportNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>2nd Passport Copy</td>
                    <td>
                        <asp:HyperLink ID="hyplinkSecondFileUploadCopy" runat="server" Target="_blank" Text="View 2nd Passport Copy"></asp:HyperLink>
                    </td>
                </tr>
            </div>
            <div id="divThird" runat="server">
                <tr>
                    <td>Please specify the reason for more than 2 attendant</td>
                    <td>
                        <asp:Label ID="lblPleaseSpecifyTheReason" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>3rd Attendant Name</td>
                    <td>
                        <asp:Label ID="lblThirdAttName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>3rd Relationship with Patient</td>
                    <td>
                        <asp:Label ID="lblThirdRelationship" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>3rd Passport No. </td>
                    <td>
                        <asp:Label ID="lblThirdPassportNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>3rd Passport Copy </td>
                    <td>
                        <asp:HyperLink ID="hyplinkThirdFileUploadCopy" runat="server" Target="_blank" Text="View 3rd Passport Copy"></asp:HyperLink>
                    </td>
                </tr>
            </div>
            <div id="divFourth" runat="server">
                <tr>
                    <td>4th Attendant Name</td>
                    <td>
                        <asp:Label ID="lblFourthAttName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>4th Relationship with Patient </td>
                    <td>
                        <asp:Label ID="lblFourthRelationship" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>4th Passport No. </td>
                    <td>
                        <asp:Label ID="lblFourthPassportNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>4th Passport Copy </td>
                    <td>
                        <asp:HyperLink ID="hyplinkFourthFileUploadCopy" runat="server" Target="_blank" Text="View 4th Passport Copy"></asp:HyperLink>
                    </td>
                </tr>
            </div>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

