﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class eTender_details : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {            
            if (Request.QueryString["Id"] != null)
            {
                string Id = Request.QueryString["Id"].ToString();
                ViewDetail(Id);
            }
        }
    }

    public void ViewDetail(string Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_eTender 'getbyid','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            lblETenderNo.Text = dt.Rows[0]["eTenderNo"].ToString();
            lblTenderName.Text = dt.Rows[0]["TenderName"].ToString();
            lblTenderStartDateTime.Text = dt.Rows[0]["TenderStartDateTime"].ToString();
            lblTenderEndDateTime.Text = dt.Rows[0]["TenderEndDateTime"].ToString();
            lblTypeofTender.Text = dt.Rows[0]["TypeOfTender"].ToString();
            lblLastTenderReceiveDateTime.Text = dt.Rows[0]["LastTenderReceiveDateTime"].ToString();
            lblTenderOpeningDateTime.Text = dt.Rows[0]["TenderOpeningDateTime"].ToString();
            lblDepartment.Text = dt.Rows[0]["Department"].ToString();
            lblCategory.Text = dt.Rows[0]["Category"].ToString();
            lblTenderDescription.Text = dt.Rows[0]["TenderDescription"].ToString();
            lblRemarks.Text = dt.Rows[0]["Remarks"].ToString();
            lblActive.Text = dt.Rows[0]["Status"].ToString();
            
        }
    }
       
    protected void btnBack1_Click(object sender, EventArgs e)
    {
        Response.Redirect("eTender_list.aspx");
    }

    
}