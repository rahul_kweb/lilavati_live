﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="SpecialityList.aspx.cs" Inherits="SpecialityList" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .viewdetails {
            color: #48524A;
            text-decoration: none;
        }
        .viewdetails:hover {
            color: #04869A;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        SPECIALITY LIST
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box">
        <asp:TextBox ID="txtsearch" CssClass="textbox" runat="server" Width="250px" Placeholder="Search.." />
        <asp:Button ID="btnsearch" runat="server" Text="Search" 
            onclick="btnsearch_Click" />
    </div>
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="10" AllowPaging="true" DataKeyNames="Speciality_Id_bint" OnRowDeleting="gdView_RowDeleting"
            OnPageIndexChanging="gdView_PageIndexChanging">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Speciality_Id_bint" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Speciality_vcr" HeaderText="Speciality" />
                <asp:TemplateField HeaderText="Details">
                    <ItemTemplate>
                        <asp:Literal ID="txtdetails" runat="server" Text='<%#Eval("Details_vcr") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Facilities">
                    <ItemTemplate>
                        <asp:Literal ID="txtfaciliaty" runat="server" Text='<%#Eval("Facilities_vcr") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Group">
                    <ItemTemplate>
                        <asp:Literal ID="txtgroup" runat="server" Text='<%#Eval("Group_vcr") %>'></asp:Literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>
                        <%--<a href='AddSpeciality.aspx?<%#Eval("Speciality_Id_bint","SpecilityId={0}") %>' class="viewdetails">
                            Edit</a>--%>
                        <asp:Button ID="btnedit" runat="server" Text="Edit" OnClick="btnedit_Click" />
                        <asp:HiddenField ID="hdnfldid" runat="server" Value='<%#Eval("Speciality_Id_bint") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <%--<a href='AddSpeciality.aspx?<%#Eval("Speciality_Id_bint","SpecilityId={0}") %>' class="viewdetails">
                            Edit</a>--%>
                        <asp:Button ID="btndelete" runat="server" Text="Delete" CommandName="Delete" OnClientClick="javascript:return confirm('Are you sure want to delete this record ?');" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
