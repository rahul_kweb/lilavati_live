﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="patients_education_brochure.aspx.cs" Inherits="Admin_packages_details" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        <asp:Label ID="lblTittle" runat="server"></asp:Label>
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Brochure<span class="required">*</span> :
            <br />
            (Recommend Brouchure Size (300*700))
        </label>
        <asp:FileUpload ID="FileUploadBrochure" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:Image ID="imgLogo" runat="server" Height="500" Width="200" Visible="false" />
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />        
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="10" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" OnPageIndexChanging="gdView_PageIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="BROC_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TITTLE" HeaderText="Tittle" />            
            <asp:TemplateField HeaderText="Brochures">
                <ItemTemplate>
                    <img src='<%#Eval("BROCHURE","../uploads/brochure/{0}") %>' width="200" height="500"></img>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

