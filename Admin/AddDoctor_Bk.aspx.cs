﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class AddDoctor : AdminPage
{
    BAL bal = new BAL();
    Specialty obj = new Specialty();
    Doctor obj1 = new Doctor();
    Utility utility = new Utility();
    public string heading;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSpecialityCheckbox();


            if (Request.QueryString["DrId"] != null)
            {
                string id = Request.QueryString["DrId"].ToString();
                obj1._Buttontext = "Update Doctor";
                btnSave.Text = "Update Doctor";
                btnbacktolist.Visible = true;
                btncancel.Visible = false;
                GetRecordEditTime(id);
                txtDoctorName.Text = obj1._Doctorname;
                //txtDesignation.Text = dt.Rows[0]["Designation_vcr"].ToString();
                txtQualification.Text = obj1._Qualification;
                txtAreaOfInterest.Text = obj1._Areaofinterest;
                txtAchievments.Text = obj1._Achievements;

                ShowHideOneditTime();
                SplitTime(obj1._AvailTimeMon, obj1._AvailTimeTue, obj1._AvailTimeWed, obj1._AvailTimeThurs, obj1._AvailTimeFri, obj1._AvailTimeSat);

                txtRoomMon.Text = obj1._Room_vcr_Mon;
                txtRoomTue.Text = obj1._Room_vcr_Tue;
                txtRoomWed.Text = obj1._Room_vcr_Wed;
                txtRoomThu.Text = obj1._Room_vcr_Thurs;
                txtRoomFri.Text = obj1._Room_vcr_Fri;
                txtRoomSat.Text = obj1._Room_vcr_Sat;

                ImgDoctor.ImageUrl = obj1._Photo;

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("EXEC  AddUpdateGetDoctorSpecialityMaster 'Get_By_Doc_Id',0," + id);
                if (dt1.Rows.Count > 0)
                {
                    SetCheckBox(dt1);
                }
            }
            else
            {
                heading = "ADD DOCTOR";
            }
        }
    }
   
    public DataTable GetRecordEditTime(string id)
    {
        obj1._DoctorId = int.Parse(id);


        return bal.GetDoctorlity(obj1);

    }
   
    public void BindSpecialityCheckbox()
    {
        obj._Specialityname = "Speciality_vcr";
        obj._CheckBoxId = "Speciality_Id_bint";
        string query = "EXEC AddUpdateGetSpecialtyMaster 'Get'";
        bal.GetCheckListOfSpeciality(obj, chkSpecialities, query);
    }
    
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (Validation())
        {
            if (Request.QueryString["DrId"] != null)
            {
                obj1._Buttontext = "Update Doctor";
                obj1._DoctorId = int.Parse(Request.QueryString["DrId"].ToString());
                setdeletevalueofcheckbox(Request.QueryString["DrId"].ToString());
            }
            else
            {
                obj1._Buttontext = "Add Doctor";
            }


            obj1._Doctorname = txtDoctorName.Text.ToString();
            obj1._Achievements = txtAchievments.Text.ToString();

            obj1._Photo = SaveFile();

            obj1._Qualification = txtQualification.Text.ToString();
            obj1._Areaofinterest = txtAreaOfInterest.Text.ToString();
            obj1._Gender = ddlGender.SelectedValue;
            /*Monday*/
            if (rdbAvailMon.SelectedValue == "1")
            {
                ////obj1._AvailTimeMon = txtMonFrom.Text.Trim() + "-" + ddlMonFrom.Text + "-" + txtMonTo.Text.Trim() + "-" + ddlMonTo.Text;
                string Mondayfrom1 = txtMonFrom.Text.Trim();
                string MondaytimeInsert = string.Empty;

                if (Mondayfrom1.Contains("#") == true)
                {
                    char[] Mondayfrom = { '#' };

                    string[] mondayfrom2 = Mondayfrom1.Split(Mondayfrom);
                    string mondayfrom3 = string.Empty;
                    string mondayfrom4 = string.Empty;
                    string mondayfrom5 = string.Empty;
                    foreach (string monfrom in mondayfrom2)
                    {
                        if (mondayfrom3 == "")
                        {
                            mondayfrom3 = monfrom.ToString();
                        }
                        else if (mondayfrom4 == "")
                        {
                            mondayfrom4 = monfrom.ToString();
                        }
                        else if (mondayfrom5 == "")
                        {
                            mondayfrom5 = monfrom.ToString();
                        }
                        else
                        {

                        }
                    }
                    char[] Mondayto = { '#' };
                    string Mondayto1 = txtMonTo.Text.Trim();
                    string[] mondayto2 = Mondayto1.Split(Mondayto);
                    string mondayto3 = string.Empty;
                    string mondayto4 = string.Empty;
                    string mondayto5 = string.Empty;
                    foreach (string monto in mondayto2)
                    {
                        if (mondayto3 == "")
                        {
                            mondayto3 = monto.ToString();
                        }
                        else if (mondayto4 == "")
                        {
                            mondayto4 = monto.ToString();
                        }
                        else if (mondayto5 == "")
                        {
                            mondayto5 = monto.ToString();
                        }
                        else
                        {

                        }
                    }

                    #region Room code Comment
                    //char[] MondayRoom = { '#' };
                    //string MondayRoom1 = txtMonTo.Text.Trim();
                    //string[] mondayRoom2 = MondayRoom1.Split(MondayRoom);
                    //string mondayRoom3 = string.Empty;
                    //string mondayRoom4 = string.Empty;
                    //string mondayRoom5 = string.Empty;
                    //foreach (string monroom in mondayRoom2)
                    //{
                    //    if (mondayRoom3 == "")
                    //    {
                    //        mondayRoom3 = monroom.ToString();
                    //    }
                    //    else if (mondayRoom4 == "")
                    //    {
                    //        mondayRoom4 = monroom.ToString();
                    //    }
                    //    else if (mondayRoom5 == "")
                    //    {
                    //        mondayRoom5 = monroom.ToString();
                    //    }
                    //    else
                    //    {

                    //    }
                    //}
                    #endregion


                    if (mondayfrom3 != "" && mondayto3 != "")
                    {
                        MondaytimeInsert += mondayfrom3.ToString() + "-" + mondayto3.ToString();
                    }
                    if (mondayfrom4 != "" && mondayto4 != "")
                    {
                        MondaytimeInsert += "#" + mondayfrom4.ToString() + "-" + mondayto4.ToString();
                    }
                    if (mondayfrom5 != "" && mondayto5 != "")
                    {
                        MondaytimeInsert += "#" + mondayfrom5.ToString() + "-" + mondayto5.ToString();
                    }
                    obj1._AvailTimeMon = MondaytimeInsert.ToString();
                }
                else
                {
                    obj1._AvailTimeMon = txtMonFrom.Text.Trim() + "-" + txtMonTo.Text.Trim();
                }


                obj1._Room_vcr_Mon = txtRoomMon.Text.ToString();
                obj1._NotAvailMonbit = false;

            }
            else
            {
                obj1._AvailTimeMon = "N.A";
                obj1._Room_vcr_Mon = "N.R";
                obj1._NotAvailMonbit = true;


            }
            /*Tuesday*/
            if (rdbAvailTue.SelectedValue == "1")
            {
                //obj1._AvailTimeTue = txtTueFrom.Text.Trim() + "-" + ddlTueFrom.Text + "-" + txtTueTo.Text.Trim() + "-" + ddlTueTo.Text;

                string Tuesdayfrom1 = txtTueFrom.Text.Trim();
                string TuesdaytimeInsert = string.Empty;

                if (Tuesdayfrom1.Contains("#") == true)
                {
                    char[] Tuesdayfrom = { '#' };

                    string[] Tuesdayfrom2 = Tuesdayfrom1.Split(Tuesdayfrom);
                    string Tuesdayfrom3 = string.Empty;
                    string Tuesdayfrom4 = string.Empty;
                    string Tuesdayfrom5 = string.Empty;
                    foreach (string Tuefrom in Tuesdayfrom2)
                    {
                        if (Tuesdayfrom3 == "")
                        {
                            Tuesdayfrom3 = Tuefrom.ToString();
                        }
                        else if (Tuesdayfrom4 == "")
                        {
                            Tuesdayfrom4 = Tuefrom.ToString();
                        }
                        else if (Tuesdayfrom5 == "")
                        {
                            Tuesdayfrom5 = Tuefrom.ToString();
                        }
                        else
                        {

                        }
                    }
                    char[] Tuesdayto = { '#' };
                    string Tuesdayto1 = txtTueTo.Text.Trim();
                    string[] Tuesdayto2 = Tuesdayto1.Split(Tuesdayto);
                    string Tuesdayto3 = string.Empty;
                    string Tuesdayto4 = string.Empty;
                    string Tuesdayto5 = string.Empty;
                    foreach (string tueto in Tuesdayto2)
                    {
                        if (Tuesdayto3 == "")
                        {
                            Tuesdayto3 = tueto.ToString();
                        }
                        else if (Tuesdayto4 == "")
                        {
                            Tuesdayto4 = tueto.ToString();
                        }
                        else if (Tuesdayto5 == "")
                        {
                            Tuesdayto5 = tueto.ToString();
                        }
                        else
                        {

                        }
                    }


                    if (Tuesdayfrom3 != "" && Tuesdayto3 != "")
                    {
                        TuesdaytimeInsert += Tuesdayfrom3.ToString() + "-" + Tuesdayto3.ToString();
                    }
                    if (Tuesdayfrom4 != "" && Tuesdayto4 != "")
                    {
                        TuesdaytimeInsert += "#" + Tuesdayfrom4.ToString() + "-" + Tuesdayto4.ToString();
                    }
                    if (Tuesdayfrom5 != "" && Tuesdayto5 != "")
                    {
                        TuesdaytimeInsert += "#" + Tuesdayfrom5.ToString() + "-" + Tuesdayto5.ToString();
                    }
                    obj1._AvailTimeTue = TuesdaytimeInsert.ToString();
                }
                else
                {
                    obj1._AvailTimeTue = txtTueFrom.Text.Trim() + "-" + txtTueTo.Text.Trim();
                }

                obj1._Room_vcr_Tue = txtRoomTue.Text.ToString();
                obj1._NotAvailTuebit = false;

            }
            else
            {
                obj1._AvailTimeTue = "N.A";
                obj1._Room_vcr_Tue = "N.R";
                obj1._NotAvailTuebit = true;


            }
            /*Wednesday*/
            if (rdbAvailWed.SelectedValue == "1")
            {
                //obj1._AvailTimeWed = txtWedFrom.Text.Trim() + "-" + ddlWedFrom.Text + "-" + txtWedTo.Text.Trim() + "-" + ddlWedTo.Text;


                string Wednesdayfrom1 = txtWedFrom.Text.Trim();
                string WednesdaytimeInsert = string.Empty;

                if (Wednesdayfrom1.Contains("#") == true)
                {
                    char[] Wednesdayfrom = { '#' };

                    string[] Wednesdayfrom2 = Wednesdayfrom1.Split(Wednesdayfrom);
                    string Wednesdayfrom3 = string.Empty;
                    string Wednesdayfrom4 = string.Empty;
                    string Wednesdayfrom5 = string.Empty;
                    foreach (string Wedfrom in Wednesdayfrom2)
                    {
                        if (Wednesdayfrom3 == "")
                        {
                            Wednesdayfrom3 = Wedfrom.ToString();
                        }
                        else if (Wednesdayfrom4 == "")
                        {
                            Wednesdayfrom4 = Wedfrom.ToString();
                        }
                        else if (Wednesdayfrom5 == "")
                        {
                            Wednesdayfrom5 = Wedfrom.ToString();
                        }
                        else
                        {

                        }
                    }
                    char[] Wednesdayto = { '#' };
                    string Wednesdayto1 = txtWedTo.Text.Trim();
                    string[] Wednesdayto2 = Wednesdayto1.Split(Wednesdayto);
                    string Wednesdayto3 = string.Empty;
                    string Wednesdayto4 = string.Empty;
                    string Wednesdayto5 = string.Empty;
                    foreach (string Wedto in Wednesdayto2)
                    {
                        if (Wednesdayto3 == "")
                        {
                            Wednesdayto3 = Wedto.ToString();
                        }
                        else if (Wednesdayto4 == "")
                        {
                            Wednesdayto4 = Wedto.ToString();
                        }
                        else if (Wednesdayto5 == "")
                        {
                            Wednesdayto5 = Wedto.ToString();
                        }
                        else
                        {

                        }
                    }


                    if (Wednesdayfrom3 != "" && Wednesdayto3 != "")
                    {
                        WednesdaytimeInsert += Wednesdayfrom3.ToString() + "-" + Wednesdayto3.ToString();
                    }
                    if (Wednesdayfrom4 != "" && Wednesdayto4 != "")
                    {
                        WednesdaytimeInsert += "#" + Wednesdayfrom4.ToString() + "-" + Wednesdayto4.ToString();
                    }
                    if (Wednesdayfrom5 != "" && Wednesdayto5 != "")
                    {
                        WednesdaytimeInsert += "#" + Wednesdayfrom5.ToString() + "-" + Wednesdayto5.ToString();
                    }
                    obj1._AvailTimeWed = WednesdaytimeInsert.ToString();
                }
                else
                {
                    obj1._AvailTimeWed = txtWedFrom.Text.Trim() + "-" + txtWedTo.Text.Trim();
                }


                obj1._Room_vcr_Wed = txtRoomWed.Text.ToString();
                obj1._NotAvailWedbit = false;

            }
            else
            {
                obj1._AvailTimeWed = "N.A";
                obj1._Room_vcr_Wed = "N.R";
                obj1._NotAvailWedbit = true;


            }
            /*Thursday*/
            if (rdbAvailThu.SelectedValue == "1")
            {
                //obj1._AvailTimeThurs = txtThuFrom.Text.Trim() + "-" + ddlThuFrom.Text + "-" + txtThuTo.Text.Trim() + "-" + ddlThuTo.Text;

                string Thursdayfrom1 = txtThuFrom.Text.Trim();
                string ThursdaytimeInsert = string.Empty;

                if (Thursdayfrom1.Contains("#") == true)
                {
                    char[] Thursdayfrom = { '#' };

                    string[] Thursdayfrom2 = Thursdayfrom1.Split(Thursdayfrom);
                    string Thursdayfrom3 = string.Empty;
                    string Thursdayfrom4 = string.Empty;
                    string Thursdayfrom5 = string.Empty;
                    foreach (string Thufrom in Thursdayfrom2)
                    {
                        if (Thursdayfrom3 == "")
                        {
                            Thursdayfrom3 = Thufrom.ToString();
                        }
                        else if (Thursdayfrom4 == "")
                        {
                            Thursdayfrom4 = Thufrom.ToString();
                        }
                        else if (Thursdayfrom5 == "")
                        {
                            Thursdayfrom5 = Thufrom.ToString();
                        }
                        else
                        {

                        }
                    }
                    char[] Thursdayto = { '#' };
                    string Thursdayto1 = txtThuTo.Text.Trim();
                    string[] Thursdayto2 = Thursdayto1.Split(Thursdayto);
                    string Thursdayto3 = string.Empty;
                    string Thursdayto4 = string.Empty;
                    string Thursdayto5 = string.Empty;
                    foreach (string Thursto in Thursdayto2)
                    {
                        if (Thursdayto3 == "")
                        {
                            Thursdayto3 = Thursto.ToString();
                        }
                        else if (Thursdayto4 == "")
                        {
                            Thursdayto4 = Thursto.ToString();
                        }
                        else if (Thursdayto5 == "")
                        {
                            Thursdayto5 = Thursto.ToString();
                        }
                        else
                        {

                        }
                    }


                    if (Thursdayfrom3 != "" && Thursdayto3 != "")
                    {
                        ThursdaytimeInsert += Thursdayfrom3.ToString() + "-" + Thursdayto3.ToString();
                    }
                    if (Thursdayfrom4 != "" && Thursdayto4 != "")
                    {
                        ThursdaytimeInsert += "#" + Thursdayfrom4.ToString() + "-" + Thursdayto4.ToString();
                    }
                    if (Thursdayfrom5 != "" && Thursdayto5 != "")
                    {
                        ThursdaytimeInsert += "#" + Thursdayfrom5.ToString() + "-" + Thursdayto5.ToString();
                    }
                    obj1._AvailTimeThurs = ThursdaytimeInsert.ToString();
                }
                else
                {
                    obj1._AvailTimeThurs = txtThuFrom.Text.Trim() + "-" + txtThuTo.Text.Trim();
                }
                obj1._Room_vcr_Thurs = txtRoomThu.Text.ToString();
                obj1._NotAvailThursbit = false;

            }
            else
            {
                obj1._AvailTimeThurs = "N.A";
                obj1._Room_vcr_Thurs = "N.R";
                obj1._NotAvailThursbit = true;


            }
            /*Friday*/
            if (rdbAvailFri.SelectedValue == "1")
            {
                //obj1._AvailTimeFri = txtFriFrom.Text.Trim() + "-" + ddlFriFrom.Text + "-" + txtFriTo.Text.Trim() + "-" + ddlFriTo.Text;

                string Fridayfrom1 = txtFriFrom.Text.Trim();
                string FridaytimeInsert = string.Empty;

                if (Fridayfrom1.Contains("#") == true)
                {
                    char[] Fridayfrom = { '#' };

                    string[] Fridayfrom2 = Fridayfrom1.Split(Fridayfrom);
                    string Fridayfrom3 = string.Empty;
                    string Fridayfrom4 = string.Empty;
                    string Fridayfrom5 = string.Empty;
                    foreach (string Frifrom in Fridayfrom2)
                    {
                        if (Fridayfrom3 == "")
                        {
                            Fridayfrom3 = Frifrom.ToString();
                        }
                        else if (Fridayfrom4 == "")
                        {
                            Fridayfrom4 = Frifrom.ToString();
                        }
                        else if (Fridayfrom5 == "")
                        {
                            Fridayfrom5 = Frifrom.ToString();
                        }
                        else
                        {

                        }
                    }
                    char[] Fridayto = { '#' };
                    string Fridayto1 = txtFriTo.Text.Trim();
                    string[] Fridayto2 = Fridayto1.Split(Fridayto);
                    string Fridayto3 = string.Empty;
                    string Fridayto4 = string.Empty;
                    string Fridayto5 = string.Empty;
                    foreach (string Frito in Fridayto2)
                    {
                        if (Fridayto3 == "")
                        {
                            Fridayto3 = Frito.ToString();
                        }
                        else if (Fridayto4 == "")
                        {
                            Fridayto4 = Frito.ToString();
                        }
                        else if (Fridayto5 == "")
                        {
                            Fridayto5 = Frito.ToString();
                        }
                        else
                        {

                        }
                    }


                    if (Fridayfrom3 != "" && Fridayto3 != "")
                    {
                        FridaytimeInsert += Fridayfrom3.ToString() + "-" + Fridayto3.ToString();
                    }
                    if (Fridayfrom4 != "" && Fridayto4 != "")
                    {
                        FridaytimeInsert += "#" + Fridayfrom4.ToString() + "-" + Fridayto4.ToString();
                    }
                    if (Fridayfrom5 != "" && Fridayto5 != "")
                    {
                        FridaytimeInsert += "#" + Fridayfrom5.ToString() + "-" + Fridayto5.ToString();
                    }
                    obj1._AvailTimeFri = FridaytimeInsert.ToString();
                }
                else
                {
                    obj1._AvailTimeFri = txtFriFrom.Text.Trim() + "-" + txtFriTo.Text.Trim();
                }
                obj1._Room_vcr_Fri = txtRoomFri.Text.ToString();
                obj1._NotAvailFribit = false;

            }
            else
            {
                obj1._AvailTimeFri = "N.A";
                obj1._Room_vcr_Fri = "N.R";
                obj1._NotAvailFribit = true;
            }
            /*Saturday*/
            if (rdbAvailSat.SelectedValue == "1")
            {
                //obj1._AvailTimeSat = txtSatFrom.Text.Trim() + "-" + ddlSatFrom.Text + "-" + txtSatTo.Text.Trim() + "-" + ddlSatTo.Text;

                string Saturdayfrom1 = txtSatFrom.Text.Trim();
                string SaturdaytimeInsert = string.Empty;

                if (Saturdayfrom1.Contains("#") == true)
                {
                    char[] Saturdayfrom = { '#' };

                    string[] Saturdayfrom2 = Saturdayfrom1.Split(Saturdayfrom);
                    string Saturdayfrom3 = string.Empty;
                    string Saturdayfrom4 = string.Empty;
                    string Saturdayfrom5 = string.Empty;
                    foreach (string Frifrom in Saturdayfrom2)
                    {
                        if (Saturdayfrom3 == "")
                        {
                            Saturdayfrom3 = Frifrom.ToString();
                        }
                        else if (Saturdayfrom4 == "")
                        {
                            Saturdayfrom4 = Frifrom.ToString();
                        }
                        else if (Saturdayfrom5 == "")
                        {
                            Saturdayfrom5 = Frifrom.ToString();
                        }
                        else
                        {

                        }
                    }
                    char[] Saturdayto = { '#' };
                    string Saturdayto1 = txtSatTo.Text.Trim();
                    string[] Saturdayto2 = Saturdayto1.Split(Saturdayto);
                    string Saturdayto3 = string.Empty;
                    string Saturdayto4 = string.Empty;
                    string Saturdayto5 = string.Empty;
                    foreach (string Frito in Saturdayto2)
                    {
                        if (Saturdayto3 == "")
                        {
                            Saturdayto3 = Frito.ToString();
                        }
                        else if (Saturdayto4 == "")
                        {
                            Saturdayto4 = Frito.ToString();
                        }
                        else if (Saturdayto5 == "")
                        {
                            Saturdayto5 = Frito.ToString();
                        }
                        else
                        {

                        }
                    }


                    if (Saturdayfrom3 != "" && Saturdayto3 != "")
                    {
                        SaturdaytimeInsert += Saturdayfrom3.ToString() + "-" + Saturdayto3.ToString();
                    }
                    if (Saturdayfrom4 != "" && Saturdayto4 != "")
                    {
                        SaturdaytimeInsert += "#" + Saturdayfrom4.ToString() + "-" + Saturdayto4.ToString();
                    }
                    if (Saturdayfrom5 != "" && Saturdayto5 != "")
                    {
                        SaturdaytimeInsert += "#" + Saturdayfrom5.ToString() + "-" + Saturdayto5.ToString();
                    }
                    obj1._AvailTimeSat = SaturdaytimeInsert.ToString();
                }
                else
                {
                    obj1._AvailTimeSat = txtSatFrom.Text.Trim() + "-" + txtSatTo.Text.Trim();
                }
                
                obj1._Room_vcr_Sat = txtRoomSat.Text.ToString();
                obj1._NotAvailSatbit = false;

            }
            else
            {
                obj1._AvailTimeSat = "N.A";
                obj1._Room_vcr_Sat = "N.R";
                obj1._NotAvailSatbit = true;
            }



            foreach (ListItem li in chkSpecialities.Items)
            {
                if (li.Selected == true)
                {

                    obj1.MultipleSpeciality.Add(li.Value);
                }
                else
                {

                }

            }
            string result = bal.PassDrRecord(obj1);

            if (result == "success")
            {
                 Reset();
                MyMessageBox1.ShowSuccess("Dr Added SuccessFylly");
            }
            else if (result == "Dr Update")
            {
               
                MyMessageBox1.ShowSuccess("Dr Update SuccessFylly");
            }
            else
            {
                MyMessageBox1.ShowError("Error occurred");
            }
        }
    }
    
    private string SaveFile()
    {
        string FilePath = "";
        string extensionI1 = System.IO.Path.GetExtension(fileuploadimage.FileName);

        if (extensionI1 != "")
        {
            string fName = "Doc_" + txtDoctorName.Text.Trim() + "_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_');


            fileuploadimage.SaveAs(Server.MapPath("Doctors/" + fName.Replace(' ', '_') + extensionI1 + ""));
            FilePath = fName.Replace(' ', '_') + extensionI1;
        }
        return FilePath;

    }
   
    private void SetCheckBox(DataTable dt1)
    {
        foreach (DataRow dr in dt1.Rows)
        {
            string txt = dr["Speciality_vcr"].ToString();
            string id = dr["Speciality_Id_bint"].ToString();
            chkSpecialities.Items.FindByText(txt).Selected = true;
            obj1.MultipleSpecialitydelete.Add(id);


        }

    }

    private void setdeletevalueofcheckbox(string id)
    {


        DataTable dt1 = new DataTable();
        dt1 = utility.Display("EXEC  AddUpdateGetDoctorSpecialityMaster 'Get_By_Doc_Id',0," + id);
        if (dt1.Rows.Count > 0)
        {

            foreach (DataRow dr in dt1.Rows)
            {

                string id1 = dr["Speciality_Id_bint"].ToString();
                obj1.MultipleSpecialitydelete.Add(id1);


            }
        }
    }

    public void SplitTime(string mon, string tue, string wed, string thurs, string frid, string sat)
    {
        char[] monday = { '-' };
        char[] tuesday = { '-' };
        char[] wednesday = { '-' };
        char[] thursday = { '-' };
        char[] friday = { '-' };
        char[] saturday = { '-' };

        /*Monday array code*/
        #region Monday
        if (mon != "N.A" && mon != "N.A." && mon != "----")
        {
            string[] monday1 = mon.Split(monday);
            bool monfrom = false;
            bool monam = false;
            bool monto = false;
            bool monpm = false;
            foreach (string monlist in monday1)
            {
                if (monfrom != true)
                {
                    txtMonFrom.Text = monlist;
                    monfrom = true;
                }
                else if (monam != true)
                {
                    ddlMonFrom.Text = monlist;
                    monam = true;
                }
                else if (monto != true)
                {
                    txtMonTo.Text = monlist;
                    monto = true;
                }
                else
                {
                    ddlMonTo.Text = monlist;
                    monto = true;
                }
            }
        }
        #endregion


        /*Tuesday array code*/

        #region Tuesday
        if (tue != "N.A" && tue != "N.A." && tue != "----")
        {
            string[] tuesday1 = tue.Split(tuesday);
            bool tuesfrom = false;
            bool tuesam = false;
            bool tuesto = false;
            bool tuespm = false;
            foreach (string tueslist in tuesday1)
            {
                if (tuesfrom != true)
                {
                    txtTueFrom.Text = tueslist;
                    tuesfrom = true;
                }
                else if (tuesam != true)
                {
                    ddlTueFrom.Text = tueslist;
                    tuesam = true;
                }
                else if (tuesto != true)
                {
                    txtTueTo.Text = tueslist;
                    tuesto = true;
                }
                else
                {
                    ddlTueTo.Text = tueslist;
                    tuespm = true;
                }
            }
        }
        #endregion

        /*wednesday array code*/
        #region wednesday
        if (wed != "N.A" && wed != "N.A." && wed != "----")
        {
            string[] wednedday1 = wed.Split(wednesday);
            bool wednedfrom = false;
            bool wednedam = false;
            bool wednedto = false;
            bool wednedpm = false;
            foreach (string wednesdaylist in wednedday1)
            {
                if (wednedfrom != true)
                {
                    txtWedFrom.Text = wednesdaylist;
                    wednedfrom = true;
                }
                else if (wednedam != true)
                {
                    ddlWedFrom.Text = wednesdaylist;
                    wednedam = true;
                }
                else if (wednedto != true)
                {
                    txtWedTo.Text = wednesdaylist;
                    wednedto = true;
                }
                else
                {
                    ddlWedTo.Text = wednesdaylist;
                    wednedpm = true;
                }
            }
        }
        #endregion

        /*Thursday array code*/
        #region Thurday
        if (thurs != "N.A" && thurs != "N.A." && thurs != "----")
        {
            string[] Thursday1 = thurs.Split(thursday);
            bool Thursfrom = false;
            bool Thursam = false;
            bool Thursto = false;
            bool Thurspm = false;
            foreach (string thursdaylist in Thursday1)
            {
                if (Thursfrom != true)
                {
                    txtThuFrom.Text = thursdaylist;
                    Thursfrom = true;
                }
                else if (Thursam != true)
                {
                    ddlThuFrom.Text = thursdaylist;
                    Thursam = true;
                }
                else if (Thursto != true)
                {
                    txtThuTo.Text = thursdaylist;
                    Thursto = true;
                }
                else
                {
                    ddlThuTo.Text = thursdaylist;
                    Thurspm = true;
                }
            }
        }
        #endregion

        /*Friday array code*/
        #region Friday
        if (frid != "N.A" && frid != "N.A." && frid != "----")
        {
            string[] Friday1 = frid.Split(friday);
            bool fridfrom = false;
            bool fridam = false;
            bool fridto = false;
            bool fridpm = false;
            foreach (string fridaylist in Friday1)
            {
                if (fridfrom != true)
                {
                    txtFriFrom.Text = fridaylist;
                    fridfrom = true;
                }
                else if (fridam != true)
                {
                    ddlFriFrom.Text = fridaylist;
                    fridam = true;
                }
                else if (fridto != true)
                {
                    txtFriTo.Text = fridaylist;
                    fridto = true;
                }
                else
                {
                    ddlFriTo.Text = fridaylist;
                    fridpm = true;
                }
            }
        }
        #endregion

        /*Saturday array code*/
        #region saturday
        if (sat != "N.A" && sat != "N.A." && sat != "----")
        {
            string[] satday1 = sat.Split(saturday);
            bool satfrom = false;
            bool satam = false;
            bool satto = false;
            bool satpm = false;
            foreach (string saturdaylist in satday1)
            {
                if (satfrom != true)
                {
                    txtSatFrom.Text = saturdaylist;
                    satfrom = true;
                }
                else if (satam != true)
                {
                    ddlSatFrom.Text = saturdaylist;
                    satam = true;
                }
                else if (satto != true)
                {
                    txtSatTo.Text = saturdaylist;
                    satto = true;
                }
                else
                {
                    ddlSatTo.Text = saturdaylist;
                    satpm = true;
                }
            }
        }
        #endregion
    }

    public void ShowHideOneditTime()
    {
        bool isok = true;

        #region Monday
        if (isok == obj1._NotAvailMonbit)
        {
            rdbAvailMon.SelectedValue = "0";
            txtMonFrom.Attributes.Add("style", "display:none;");
            ddlMonFrom.Attributes.Add("style", "display:none;");
            txtMonTo.Attributes.Add("style", "display:none;");
            ddlMonTo.Attributes.Add("style", "display:none;");
            txtRoomMon.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtMonFrom.Attributes.Add("style", "Display:block;");
        //    ddlMonFrom.Attributes.Add("style", "Display:block;");
        //    txtMonTo.Attributes.Add("style", "Display:block;");
        //    ddlMonTo.Attributes.Add("style", "Display:block;");
        //    txtRoomMon.Attributes.Add("style", "Display:block;");
        //}
        #endregion

        #region Tuesday
        if (isok == obj1._NotAvailTuebit)
        {
            rdbAvailTue.SelectedValue = "0";
            txtTueFrom.Attributes.Add("style", "display:none;");
            ddlTueFrom.Attributes.Add("style", "display:none;");
            txtTueTo.Attributes.Add("style", "display:none;");
            ddlTueTo.Attributes.Add("style", "display:none;");
            txtRoomTue.Attributes.Add("style", "display:none;");
        }
        //if (isok == obj1._NotAvailThursbit)
        //{
        //    txtTueFrom.Attributes.Add("style", "Display:block;");
        //    ddlTueFrom.Attributes.Add("style", "Display:block;");
        //    txtTueTo.Attributes.Add("style", "Display:block;");
        //    ddlTueTo.Attributes.Add("style", "Display:block;");
        //    txtRoomTue.Attributes.Add("style", "Display:block;");
        //}
        #endregion


        #region Wednesday
        if (isok == obj1._NotAvailWedbit)
        {
            rdbAvailWed.SelectedValue = "0";
            txtWedFrom.Attributes.Add("style", "display:none;");
            ddlWedFrom.Attributes.Add("style", "display:none;");
            txtWedTo.Attributes.Add("style", "display:none;");
            ddlWedTo.Attributes.Add("style", "display:none;");
            txtRoomWed.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtWedFrom.Attributes.Add("style", "Display:block;");
        //    ddlWedFrom.Attributes.Add("style", "Display:block;");
        //    txtWedTo.Attributes.Add("style", "Display:block;");
        //    ddlWedTo.Attributes.Add("style", "Display:block;");
        //    txtRoomWed.Attributes.Add("style", "Display:block;");
        //}
        #endregion


        #region Thursday
        if (isok == obj1._NotAvailThursbit)
        {
            rdbAvailThu.SelectedValue = "0";
            txtThuFrom.Attributes.Add("style", "display:none;");
            ddlThuFrom.Attributes.Add("style", "display:none;");
            txtThuTo.Attributes.Add("style", "display:none;");
            ddlThuTo.Attributes.Add("style", "display:none;");
            txtRoomThu.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtThuFrom.Attributes.Add("style", "Display:block;");
        //    ddlThuFrom.Attributes.Add("style", "Display:block;");
        //    txtThuTo.Attributes.Add("style", "Display:block;");
        //    ddlThuTo.Attributes.Add("style", "Display:block;");
        //    txtRoomThu.Attributes.Add("style", "Display:block;");
        //}
        #endregion


        #region Friday
        if (isok == obj1._NotAvailFribit)
        {
            rdbAvailFri.SelectedValue = "0";
            txtFriFrom.Attributes.Add("style", "display:none;");
            ddlFriFrom.Attributes.Add("style", "display:none;");
            txtFriTo.Attributes.Add("style", "display:none;");
            ddlFriTo.Attributes.Add("style", "display:none;");
            txtRoomFri.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtFriFrom.Attributes.Add("style", "Display:block;");
        //    ddlFriFrom.Attributes.Add("style", "Display:block;");
        //    txtFriTo.Attributes.Add("style", "Display:block;");
        //    ddlFriTo.Attributes.Add("style", "Display:block;");
        //    txtRoomFri.Attributes.Add("style", "Display:block;");
        //}
        #endregion

        #region Saturday
        if (isok == obj1._NotAvailSatbit)
        {
            rdbAvailSat.SelectedValue = "0";
            txtSatFrom.Attributes.Add("style", "display:none;");
            ddlSatFrom.Attributes.Add("style", "display:none;");
            txtSatTo.Attributes.Add("style", "display:none;");
            ddlSatTo.Attributes.Add("style", "display:none;");
            txtRoomSat.Attributes.Add("style", "display:none;");
        }
        //else
        //{
        //    txtSatFrom.Attributes.Add("style", "Display:block;");
        //    ddlSatFrom.Attributes.Add("style", "Display:block;");
        //    txtSatTo.Attributes.Add("style", "Display:block;");
        //    ddlSatTo.Attributes.Add("style", "Display:block;");
        //    txtRoomSat.Attributes.Add("style", "Display:block;");
        //}
        #endregion

    }

    public bool Validation()
    {
        bool isok = true;
        string msg = string.Empty;

        if (txtDoctorName.Text == "")
        {
            msg += "Doctor Name, ";
            isok = false;
        }
        if (txtQualification.Text == "")
        {
            msg += "Qualification, ";
            isok = false;
        }
        if (txtAreaOfInterest.Text == "")
        {
            msg += "Area Of Interest, ";
            isok = false;
        }
        if (txtAchievments.Text == "")
        {
            msg += "Achievments, ";
            isok = false;
        }

        if (chkSpecialities.SelectedValue == "")
        {
            msg += "Select Specialities";
            isok = false;
        }
        if (!isok)
        {
            MyMessageBox1.ShowError("Please Fill Following Details <br />" + msg);
        }
        return isok;
    }
   
    protected void btnbacktolist_Click(object sender, EventArgs e)
    {
        Response.Redirect("DoctorsList.aspx");
    }
    
    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }
   
    public void Reset()
    {
        ClearTextBoxes(Page);
        //txtAchievments.Text = string.Empty;
        //txtAreaOfInterest.Text = string.Empty;
        //txtDoctorName.Text = string.Empty;
        //txtQualification.Text = string.Empty;
        //txt
    }
  
    protected void ClearTextBoxes(Control p1)
    {
        foreach (Control ctrl in p1.Controls)
        {
            if (ctrl is TextBox)
            {
                TextBox t = ctrl as TextBox;

                if (t != null)
                {
                    t.Text = String.Empty;
                }
            }
            else
            {
                if (ctrl.Controls.Count > 0)
                {
                    ClearTextBoxes(ctrl);
                }
            }
        }
    }
}