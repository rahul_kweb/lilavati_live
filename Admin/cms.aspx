﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true"
    CodeFile="cms.aspx.cs" Inherits="cms" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        <asp:Label ID="lblTittle" runat="server"></asp:Label>
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnid" runat="server" />
        <label class="control-label">
            <asp:Label ID="lblheading" runat="server"></asp:Label>
            
        </label>
        <div class="controls" style="margin-left: 0px;">
            <CKEditor:CKEditorControl ID="txtdesc" runat="server"></CKEditor:CKEditorControl>
        </div>
        <br />
        <br />
        <asp:Button ID="btnupdate" runat="server" CssClass="button" Text="Update" OnClick="btnupdate_Click" />
    </div>
</asp:Content>
