﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="RegistrationDetails.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<style type="text/css">
        .auto-style1 {
            width: 50%;
            /*border: 1px solid;*/
            margin-left: 275px;
            border: 1px solid black;
            border-collapse: collapse;
        }

      
    </style>--%>

    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <asp:Panel ID="pnlRegistrationDetails" runat="server" Visible="false">
        <div class="page-header">
            Registration Detail
        </div>
        <div class="form-box" style="width: 500px;">
            <table class="gridtable">
                <tr>
                    <td colspan="2">
                        <h1>Welcome
                    <asp:Label ID="lblwelcome" runat="server"></asp:Label>
                        </h1>
                    </td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td>
                        <asp:Label ID="lblUserId" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Sure Name</td>
                    <td>
                        <asp:Label ID="lblSureName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td>
                        <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td>
                        <asp:Label ID="lblLastName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Sex</td>
                    <td>
                        <asp:Label ID="lblSex" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Email ID</td>
                    <td>
                        <asp:Label ID="lblEmailID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Hospital LHNO</td>
                    <td>
                        <asp:Label ID="lblHosLHNO" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>TLHNO</td>
                    <td>
                        <asp:Label ID="lblLHNO" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none;">
                    <td>Password</td>
                    <td>
                        <asp:Label ID="lblPwd" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Mobile No</td>
                    <td>
                        <asp:Label ID="lblMobileNo" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Telephone Residence</td>
                    <td>
                        <asp:Label ID="lblTelephoneResi" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td>Date Of Birth</td>
                    <td>
                        <asp:Label ID="lblDOB" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Blood Group</td>
                    <td>
                        <asp:Label ID="lblBloodGroup" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Marital Satus</td>
                    <td>
                        <asp:Label ID="lblMaritalStatus" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Religion</td>
                    <td>
                        <asp:Label ID="lblReligion" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Occupation</td>
                    <td>
                        <asp:Label ID="lblOccupation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Country
                    </td>
                    <td>
                        <asp:Label ID="lblCountry" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>State
                    </td>
                    <td>
                        <asp:Label ID="lblState" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>City
                    </td>
                    <td>
                        <asp:Label ID="lblCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Pincode
                    </td>
                    <td>
                        <asp:Label ID="lblPincode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" style="text-align: center;">
                        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlLHNOUpdate" runat="server" Visible="true">
        <div class="page-header">
            Update LHNO
        </div>
        <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
        <div class="form-box" style="width: 500px;">
            <asp:HiddenField ID="hdnId" runat="server" />
            <h1>
                <asp:Label ID="lblUserName" runat="server"></asp:Label>
            </h1>
            <br />
            <label class="control-label">
                LHNO<span class="required">*</span> :
            </label>
            <asp:TextBox ID="txtLHNO" runat="server" CssClass="textbox"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnUpdate" runat="server" Text="Update LHNO" CssClass="button" OnClick="btnUpdate_Click" />
            <asp:Button ID="btnBack1" runat="server" Text="Back" CssClass="button" OnClick="btnBack1_Click" />
        </div>
    </asp:Panel>

</asp:Content>

