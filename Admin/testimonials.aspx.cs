﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_testimonials : System.Web.UI.Page
{
    DataTable dtMain = new DataTable();
    Testimonial tblTestimonial = new Testimonial();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualImage = "~/uploads/testimonial/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Gridview"] == "")
            {
                BindGrid12();
            }
            else
            {
                BindGrid();
            }
        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        string value = ddlType.SelectedValue;

        if (value == "-- Select --")
        {
            divImage.Visible = false;
        }
        else if (value == "Common People")
        {
            divImage.Visible = false;
        }
        else if (value == "Celebrity")
        {
            divImage.Visible = true;
        }
        else
        {
            divImage.Visible = false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgCelebrity", ext, this, false);
            fileUploadImage.SaveAs(MapPath(VirtualImage + MainImage + ext));
            tblTestimonial.CelebrityImage = MainImage + ext;
        }
        else
        {
            tblTestimonial.CelebrityImage = "blank.png";
        }

        tblTestimonial.Type = ddlType.SelectedValue;       
        tblTestimonial.Name = txtName.Text.Trim();
        tblTestimonial.Description = txtDescription.Text.Trim();

        if (fileuploadTestiImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileuploadTestiImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgTestimonial", ext, this, false);
            fileuploadTestiImage.SaveAs(MapPath(VirtualImage + MainImage + ext));
            tblTestimonial.TestImage = MainImage + ext;
        }
        
        cmsbal.SaveTestimonial(tblTestimonial);

        if (HttpContext.Current.Session["SaveTestimonial"] == "Success")
        {
            Reset();
            BindGrid();
            MyMessageBox1.ShowSuccess("Record insert successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully.");
        }
    }

    public void UpdateRecord()
    {
        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName);
            if (!utility.IsValidImageFileExtension(ext))
            {
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgCelebrity", ext, this, false);
            fileUploadImage.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblTestimonial.CelebrityImage = MainImage + ext;
        }

        if (ddlType.SelectedValue == "Common People")
        {
            tblTestimonial.CelebrityImage = "blank.png";
        }

        tblTestimonial.Test_Id = int.Parse(hdnId.Value.ToString());
        tblTestimonial.Type = ddlType.SelectedValue;
        tblTestimonial.Name = txtName.Text.Trim();
        tblTestimonial.Description = txtDescription.Text.Trim();

        if (fileuploadTestiImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileuploadTestiImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgTestimonial", ext, this, false);
            fileuploadTestiImage.SaveAs(MapPath(VirtualImage + MainImage + ext));
            tblTestimonial.TestImage = MainImage + ext;
        }

        //if (fileuploadTestiImage.HasFile)
        //{
        //    ext = System.IO.Path.GetExtension(fileuploadTestiImage.FileName).ToLower();
        //    if (!utility.IsValidImageFileExtension(ext))
        //    {
        //        MyMessageBox1.ShowError("Only Image (jpg, jpeg, png, bmp) file allowed.");
        //        return;
        //    }

        //    fileuploadTestiImage.SaveAs(Server.MapPath(VirtualImage + fileuploadTestiImage.FileName));
        //    tblTestimonial.TestImage = fileuploadTestiImage.FileName;
        //}

        cmsbal.UpdateTestimonial(tblTestimonial);
        if (HttpContext.Current.Session["UpdateTest"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not update successfully");
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetTestimonial();
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public void BindGrid12()
    {
        try
        {
            DataTable dt = new DataTable();
            //dt = (DataTable)HttpContext.Current.Session["Gridview"];
            dt = dtMain;

            if (dt.Rows.Count > 0)
            {                
                //gdView.Columns[0].Visible = true;
                dumyfrid.DataSource = dt;
                dumyfrid.DataBind();
                //gdView.Columns[0].Visible = false;
            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

       
    }

    public void Reset()
    {
        ddlType.SelectedIndex = -1;
        divImage.Visible = false;
        txtName.Text = string.Empty;
        txtDescription.Text = string.Empty;
        btnSave.Text = "Save";
        imgTest.Visible = false;
        txtSearch.Text = string.Empty;
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblTestimonial.Test_Id = id;
        cmsbal.DeleteTestimonial(tblTestimonial);
        if (HttpContext.Current.Session["DeleteTest"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully.");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblTestimonial.Test_Id = id;
        cmsbal.GetByTestimonial(tblTestimonial);

        hdnId.Value = Convert.ToInt32(tblTestimonial.Test_Id).ToString();
        ddlType.SelectedValue = tblTestimonial.Type;        
        txtName.Text = tblTestimonial.Name;
        txtDescription.Text = tblTestimonial.Description;

        if (tblTestimonial.Type == "Celebrity")
        {
            divImage.Visible = true;
            img.Visible = true;
            img.Src = string.Format("../uploads/testimonial/{0}", tblTestimonial.CelebrityImage).ToString();
        }

        if (tblTestimonial.TestImage != "")
        {
            imgTest.Visible = true;
            imgTest.Src = string.Format("../uploads/testimonial/{0}", tblTestimonial.TestImage).ToString();
        }

        btnSave.Text = "Update";
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddlType.SelectedValue == "-- Select --")
        {
            msg = "Select Type ,";
            IsOk = false;
        }

        if (ddlType.SelectedValue == "Celebrity")
        {
            if (!fileUploadImage.HasFile)
            {
                IsOk = false;
                msg += "Celebrity Image ,";
            }
        }

        if (txtName.Text == "" && txtName.Text.Equals(""))
        {
            msg += "Name ,";
            IsOk = false;
        }

        //if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        //{
        //    msg += "Description ,";
        //    IsOk = false;
        //}

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (ddlType.SelectedValue == "-- Select --")
        {
            msg = "Select Type ,";
            IsOk = false;
        }
        
        if (txtName.Text == "" && txtName.Text.Equals(""))
        {
            msg += "Name ,";
            IsOk = false;
        }

        //if (txtDescription.Text == "" && txtDescription.Text.Equals(""))
        //{
        //    msg += "Description ,";
        //    IsOk = false;
        //}

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }
     
    public void SearchTestimonial()
    {
        DataTable dt = new DataTable();

        tblTestimonial.Search = txtSearch.Text.Trim();
        dt = cmsbal.SearchTestimonial(tblTestimonial);

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void txtSearch_TextChanged(object sender, EventArgs e)
    {
        SearchTestimonial();
    }

    [WebMethod]
    public static DataTable SearchTestimonial12(string txt)
    {        
        Testimonial tblTestimonial = new Testimonial();
        CMSBAL cmsbal = new CMSBAL();

        DataTable dt = new DataTable();

        tblTestimonial.Search = txt;
        dt = cmsbal.SearchTestimonial(tblTestimonial);

       // DataSet ds = new DataSet();       
       // ds.Tables.Add(dt);

        HttpContext.Current.Session["Gridview"] = dt;
        

        Admin_testimonials test = new Admin_testimonials();
        test.dtMain = dt;
        test.BindGrid12();

        return dt;                       
    }
    
}