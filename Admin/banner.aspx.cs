﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_photo_gallery : AdminPage
{
    HomeBanner tblHomeBanner = new HomeBanner();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string ext = string.Empty;
    string MainImage = string.Empty;
    string VirtualImage = "~/uploads/home_banner/";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (btnSave.Text == "Save")
        {
            if (CheckSave())
            {
                SaveRecord();
            }
        }
        else
        {
            if (CheckUpdate())
            {
                UpdateRecord();
            }
        }
    }

    public void SaveRecord()
    {
        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Images File.");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgBanner", ext, this, false);
            fileUploadImage.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblHomeBanner.Images = MainImage + ext;
        }
        tblHomeBanner.Order = txtOrder.Text.Trim();
        tblHomeBanner.PageUrl = txtPageUrl.Text.Trim();
        if (chkPublish.Checked)
        {
            tblHomeBanner.Publish = "1";
        }
        else
        {
            tblHomeBanner.Publish = "0";
        }

        cmsbal.SaveHomeBanner(tblHomeBanner);

        if (HttpContext.Current.Session["SaveHomeBanner"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record insert successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not insert successfully");
        }

    }

    public void UpdateRecord()
    {
        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Allow Only Images File.");
                return;
            }

            MainImage = utility.GetUniqueName(VirtualImage, "imgBanner", ext, this, false);
            fileUploadImage.SaveAs(Server.MapPath(VirtualImage + MainImage + ext));
            tblHomeBanner.Images = MainImage + ext;
        }
        tblHomeBanner.Order = txtOrder.Text.Trim();
        tblHomeBanner.ID = int.Parse(hdnId.Value);
        tblHomeBanner.PageUrl = txtPageUrl.Text.Trim();
        if (chkPublish.Checked)
        {
            tblHomeBanner.Publish = "1";
        }
        else
        {
            tblHomeBanner.Publish = "0";
        }

        cmsbal.UpdateHomeBanner(tblHomeBanner);

        if (HttpContext.Current.Session["UpdateHomeBanner"] == "Success")
        {
            BindGrid();
            Reset();
            MyMessageBox1.ShowSuccess("Record update successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not update successfully");
        }

    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.AdminGetHomeBanner();

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);
        tblHomeBanner.ID = id;

        cmsbal.DeleteHomeBanner(tblHomeBanner);
        if (HttpContext.Current.Session["DeleteHomeBanner"] == "Success")
        {
            BindGrid();
            MyMessageBox1.ShowSuccess("Record delete successfully");
        }
        else
        {
            MyMessageBox1.ShowError("Record Could Not delete successfully");
        }

    }
    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
        tblHomeBanner.ID = id;

        cmsbal.GetBYHomeBanner(tblHomeBanner);
        hdnId.Value = Convert.ToInt32(tblHomeBanner.ID).ToString();
        txtOrder.Text = tblHomeBanner.Order;
        imgpreview.Visible = true;
        imgpreview.ImageUrl = string.Format("../uploads/home_banner/{0}", tblHomeBanner.Images).ToString();

        txtPageUrl.Text = tblHomeBanner.PageUrl;
        if (tblHomeBanner.Publish == "True")
        {
            chkPublish.Checked = true;
        }
        else
        {
            chkPublish.Checked = false;
        }
        btnSave.Text = "Update";

    }
    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Reset();
    }

    public void Reset()
    {
        txtOrder.Text = "0";
        imgpreview.Visible = false;
        btnSave.Text = "Save";
        txtPageUrl.Text = string.Empty;
    }

    public bool CheckSave()
    {
        bool IsOk = true;
        string msg = string.Empty;

        if (!fileUploadImage.HasFile)
        {
            msg = "Choose Image ,";
            IsOk = false;
        }

        if (txtOrder.Text == "" && txtOrder.Text.Equals(""))
        {
            msg += " Image Order ";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    public bool CheckUpdate()
    {
        bool IsOk = true;
        string msg = string.Empty;        

        if (txtOrder.Text == "" && txtOrder.Text.Equals(""))
        {
            msg += " Image Order ";
            IsOk = false;
        }

        if (msg.Length > 0)
        {
            msg = msg.Substring(0, msg.Length - 1);
        }

        if (!IsOk)
        {
            MyMessageBox1.ShowError("Fill Following Details<br>" + msg);
        }
        return IsOk;
    }

    protected void chkSHowHide_CheckedChanged(object sender, EventArgs e)
    {
        //string Id = string.Empty;
        int Id = 0;
        CheckBox chk = (CheckBox)sender;
        Id = Convert.ToInt32(chk.ToolTip);
        bool status = chk.Checked;

        if (status == true)
        {
            tblHomeBanner.ID = Id;
            tblHomeBanner.Publish = "1";
            cmsbal.ShowHomeBanner(tblHomeBanner);
            if (HttpContext.Current.Session["ShowHomeBanner"] == "Success")
            {
                BindGrid();
                MyMessageBox1.ShowSuccess("Banner show successfully");
            }
            else
            {
                MyMessageBox1.ShowError("Banner Could Not show successfully");
            }
        }
        else
        {
            tblHomeBanner.ID = Id;
            tblHomeBanner.Publish = "0";
            cmsbal.HideHomeBanner(tblHomeBanner);
            if (HttpContext.Current.Session["HideHomeBanner"] == "Success")
            {
                BindGrid();
                MyMessageBox1.ShowSuccess("Banner hide successfully");
            }
            else
            {
                MyMessageBox1.ShowError("Banner Could Not hide successfully");
            }
        }

    }
}