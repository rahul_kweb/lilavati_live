﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="empanelled_details.aspx.cs" Inherits="Admin_packages_details" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        <asp:Label ID="lblTittle" runat="server"></asp:Label>
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnId2" runat="server" />
        <label class="control-label">
            Company Name<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Company Logo<span class="required">*</span> :
            <br />
            (Recommend Company Logo Size (200*100))
        </label>
        <asp:FileUpload ID="fileUploadLogo" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:Image ID="imgLogo" runat="server" Height="100" Width="200" Visible="false" />
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" /> 
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" OnClick="btnBack_Click" Visible="false" />          
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="10" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" OnPageIndexChanging="gdView_PageIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="EMP_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TAB_NAME" HeaderText="Tab Name" />
            <asp:BoundField DataField="NAME" HeaderText="Company Name" />
            <asp:TemplateField HeaderText="Company Logo">
                <ItemTemplate>
                    <img src='<%#Eval("LOGO","../uploads/Empanelled_Companies/{0}") %>' width="200" height="100" ></img>
                </ItemTemplate>
            </asp:TemplateField>                   
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

