﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="feedback_InPatientDetail.aspx.cs" Inherits="Admin_RegistrationDetails" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-collapse: collapse;
        }

            table.gridtable th {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #dedede;
            }

            table.gridtable td {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #666666;
                background-color: #ffffff;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Feedback InPatient Details
    </div>
    <div class="form-box" style="width: 800px;">
        <table class="gridtable">
            <tr>
                <td>First Name</td>
                <td>
                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>
                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>LH No.</td>
                <td>
                    <asp:Label ID="lblLhno" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Bed No.</td>
                <td>
                    <asp:Label ID="lblBedNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Date Of Admission</td>
                <td>
                    <asp:Label ID="lblDateOfAdmission" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Date Of Discharge</td>
                <td>
                    <asp:Label ID="lblDateOfDischarge" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Contact No.</td>
                <td>
                    <asp:Label ID="lblContactNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Email Id</td>
                <td>
                    <asp:Label ID="lblEmailId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>HOW DID YOU COME TO KNOW ABOUT LILAVATI HOSPITAL?</td>
                <td>
                    <asp:Label ID="lblHowDidYouComeToKnow" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Guidance & support provided in completing paper work & formalities for admission.</td>
                <td>
                    <asp:Label ID="lblGuidanceSupport" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy and friendliness of admission staff.</td>
                <td>
                    <asp:Label ID="lblCourtesyAndFriendliness" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Time taken to complete admission process.</td>
                <td>
                    <asp:Label ID="lblTimeTakenToComplete" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Clarity provided about your condition and line of treatment.
                </td>
                <td>
                    <asp:Label ID="lblClarityProvided" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Care & attention (in terms of regular visit & time spent)
                </td>
                <td>
                    <asp:Label ID="lblCareAttention" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Doctor's efforts to include you in the decision about your treatment.
                </td>
                <td>
                    <asp:Label ID="lblDoctorsEfforts" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy & friendliness of Nursing Staff.
                </td>
                <td>
                    <asp:Label ID="lblCourtesyFriendnessNursing" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Care & attention (in terms of Medication).</td>
                <td>
                    <asp:Label ID="lblCareAndAttentionMedication" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Response to your needs.</td>
                <td>
                    <asp:Label ID="lblResponseToYou" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Respect for privacy and dignity.</td>
                <td>
                    <asp:Label ID="lblRespectForPrivacy" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>DIAGNOSTICS - Pathology (Blood Collection)</td>
                <td>
                    <asp:Label ID="lblPathology_BloodCollection" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>DIAGNOSTICS - Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)</td>
                <td>
                    <asp:Label ID="lblRadiology_X_ray" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>DIAGNOSTICS - Cardiology (ECG, Stress test, 2D Echo, etc)</td>
                <td>
                    <asp:Label ID="lblCardiology_ECG_Stress_test" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>DIAGNOSTICS - Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc)</td>
                <td>
                    <asp:Label ID="lblNuclear_Dept_BoneScanLiverScan" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>TIME TAKEN FOR TEST - Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)</td>
                <td>
                    <asp:Label ID="lblTimetakenforTestRadiology_X_ray" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>TIME TAKEN FOR TEST - Cardiology (ECG, Stress test, 2D Echo, etc)</td>
                <td>
                    <asp:Label ID="lblTimetakenforTestCardiology_ECG_Stress_test" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>TIME TAKEN FOR TEST - Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc)</td>
                <td>
                    <asp:Label ID="lblTimetakenforTestNuclear_Dept_BoneScanLiverScan" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Quality of food & Beverage served.</td>
                <td>
                    <asp:Label ID="lblQualityOfFood" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Variety of food served.</td>
                <td>
                    <asp:Label ID="lblVarietyOfFood" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Promptness of service.</td>
                <td>
                    <asp:Label ID="lblPromptnessOfService" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Information & guidance provided by dietitian regarding your diet.</td>
                <td>
                    <asp:Label ID="lblInformationGuidance" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Cleanliness of the room, toilet & Linen.</td>
                <td>
                    <asp:Label ID="lblCleanlinessOfRoom" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Response to your needs.</td>
                <td>
                    <asp:Label ID="lblResponseToYouNeed" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy & helpfulness of the Housekeeping staff.</td>
                <td>
                    <asp:Label ID="lblCourtesyAndHelpfulness" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy of the security guards.</td>
                <td>
                    <asp:Label ID="lblCourtesyOfSecurity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Professionalism of security guards.</td>
                <td>
                    <asp:Label ID="lblProfessionalism" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Billing counseling (in terms of explanation of charges/ bills)	
                </td>
                <td>
                    <asp:Label ID="lblBillingCounseling" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Promptness of billing process.</td>
                <td>
                    <asp:Label ID="lblPromptnessOfBilling" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Courtesy & helpfulness of the billing staff.</td>
                <td>
                    <asp:Label ID="lblCountesyHelpfulness" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Promptness of discharge process.</td>
                <td>
                    <asp:Label ID="lblPromptnessOfDischarge" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Discharge counseling (In terms of Post discharge care & follow up)</td>
                <td>
                    <asp:Label ID="lblDischargeCounseling" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>Convenience of discharge process.</td>
                <td>
                    <asp:Label ID="lblConvenienceOfDischarge" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td>How would you rate your overall experience at Lilavati Hospital.</td>
                <td>
                    <asp:Label ID="lblHowWouldYouRate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>What did you like the most about Lilavati hospital? </td>
                <td>
                    <asp:Label ID="lblWhatDidYouLike" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>What did you like the least about Lilavati hospital? </td>
                <td>
                    <asp:Label ID="lblWhatDidYouLikeLeast" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Any suggestion you would like to put forward which will help us to serve you better</td>
                <td>
                    <asp:Label ID="lblAnysuggestionYouWouldLike" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Would you like to recommend any staff for their outstanding Performance? Please specify name & reason. </td>
                <td>
                    <asp:Label ID="lblWouldYouLikeToRecommend" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClientClick="JavaScript:window.history.back(1); return false;" />
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

