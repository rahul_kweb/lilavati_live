﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="packages_details.aspx.cs" Inherits="Admin_packages_details" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        <asp:Label ID="lblTittle" runat="server"></asp:Label>
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <asp:HiddenField ID="hdnId2" runat="server" />
        <label class="control-label">
            Choose Type<span class="required">*</span> :
        </label>
        <asp:DropDownList ID="ddlType" runat="server" CssClass="dropdown" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem>-- Select --</asp:ListItem>
            <asp:ListItem>Tab</asp:ListItem>
            <asp:ListItem>Without Tab</asp:ListItem>
        </asp:DropDownList>
        <br />
        <label class="control-label">
            Package Price
        </label>
        <asp:TextBox ID="txtPackagePrice" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <div id="idDiv" runat="server" visible="false">
            <label class="control-label">
                Tab Name<span class="required">*</span> :
            </label>
            <asp:TextBox ID="txtTab" runat="server" CssClass="textbox"></asp:TextBox>
            <br />
        </div>
        <label class="control-label">
            Tittle
        </label>
        <CKEditor:CKEditorControl ID="txtTittle" runat="server" CssClass="textbox"></CKEditor:CKEditorControl>
        <br />
        <label class="control-label">
            Description<span class="required">*</span> :
        </label>
        <CKEditor:CKEditorControl ID="txtDescription" runat="server" CssClass="textbox"></CKEditor:CKEditorControl>
        <br />
        <label class="control-label">
            Note
        </label>
        <CKEditor:CKEditorControl ID="txtNote" runat="server" CssClass="textbox"></CKEditor:CKEditorControl>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" OnClick="btncancel_Click" />
        <asp:Button ID="btnback" runat="server" Text="Back" CssClass="button" OnClick="btnback_Click" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid" AllowPaging="true"
        PageSize="10" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="PACK_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="PACKAGE_NAME" HeaderText="Packages Name" />
            <asp:BoundField DataField="PACK_PRICE" HeaderText="Packages Price" />
            <asp:TemplateField HeaderText="Tittle">
                <ItemTemplate>
                    <asp:Literal ID="litTittle" runat="server" Text='<%#Eval("TITTLE") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TAB_NAME" HeaderText="Packages Name" />
            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <asp:Literal ID="litDes" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Note">
                <ItemTemplate>
                    <asp:Literal ID="litNote" runat="server" Text='<%#Eval("NOTE") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>
</asp:Content>

