﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

partial class Admin_Appointment : AdminPage
{
    RegistrationTable tbl = new RegistrationTable();
    AppointmentTable apptbl = new AppointmentTable();
    BAL bal = new BAL();
    Utility utility = new Utility();
    string attachmentname = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAppointmentGrid();
        }
    }

    public void BindAppointmentGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_FailedAppointment 'GET'");
        if (dt.Rows.Count > 0)
        {

            gdView.Columns[0].Visible = true;
            gdView.DataSource = dt;
            gdView.DataBind();
            gdView.Columns[0].Visible = false;
        }
        else
        {

        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gdView.Rows[e.RowIndex].Cells[0].Text);

        DataTable dtff = new DataTable();
        dtff = utility.Display("Exec Proc_FailedAppointment 'GET_BY_ID','" + id + "'");

        if (dtff.Rows.Count > 0)
        {
            DataTable getpatientdetail = new DataTable();
            getpatientdetail = utility.Display("Proc_PCipopRegistrationMaster 'GET_BY_ID','" + id + "'");
            if (getpatientdetail.Rows.Count > 0)
            {
                #region Call Web services and update LHNO into database

                ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");
                DataSet ds = new DataSet();
                string appointmentdate = dtff.Rows[0]["DateOfApp"].ToString();
                if (appointmentdate != "")
                {
                    appointmentdate = DateTime.ParseExact(appointmentdate, "dd/MM/yyyy", null).ToString("yyyy/MM/dd");
                }
                //ds = obj.webappointmentprocess("LH00057131", "Mr", "Sunil", "Dinesh", "Tiwari", "Male", "1994/05/04", "Single", "B+", "Hindu", "Indian", "India", "Software Developer","TLHNO6603", "Mahalaxmi", "Mumbai", "Maharashtra", "India", "400011", "9594907366", "sunil.t@kwebmaker.com", "9594907366", 0,"1210318", "2017/05/04", "106203791411", "");
                ds = obj.webappointmentprocess(getpatientdetail.Rows[0]["LH_NO"].ToString(), getpatientdetail.Rows[0]["TITLE"].ToString(), getpatientdetail.Rows[0]["FIRSTNAME"].ToString(), getpatientdetail.Rows[0]["MIDDLENAME"].ToString(), getpatientdetail.Rows[0]["LASTNAME"].ToString(), getpatientdetail.Rows[0]["SEX"].ToString(), getpatientdetail.Rows[0]["DATEOFBIRTHNEW"].ToString(), getpatientdetail.Rows[0]["MARITALSTATUS"].ToString(), getpatientdetail.Rows[0]["BLOODGROUP"].ToString(), getpatientdetail.Rows[0]["RELIGION"].ToString(), getpatientdetail.Rows[0]["NATIONALITY"].ToString(), getpatientdetail.Rows[0]["RESIDENCE"].ToString(), getpatientdetail.Rows[0]["OCCUPATION"].ToString(), getpatientdetail.Rows[0]["LHNO"].ToString(), getpatientdetail.Rows[0]["ADDRESS"].ToString(), getpatientdetail.Rows[0]["CITY"].ToString(), getpatientdetail.Rows[0]["STATE"].ToString(), getpatientdetail.Rows[0]["COUNTRY"].ToString(), getpatientdetail.Rows[0]["PINCODE"].ToString(), getpatientdetail.Rows[0]["TELNORESIDENCE"].ToString(), getpatientdetail.Rows[0]["EMAIL"].ToString(), getpatientdetail.Rows[0]["MOBILENO"].ToString(), float.Parse(dtff.Rows[0]["Amount"].ToString()), dtff.Rows[0]["ConsDocId"].ToString(), appointmentdate, dtff.Rows[0]["OrderId"].ToString(), "");
                DataTable dt123 = ds.Tables[0];
                if (dt123.Columns.Contains("output"))
                {
                    MyMessageBox1.ShowError("Failed please try again..!");

                }
                else
                {
                    using (SqlCommand cmdnew = new SqlCommand("Proc_PCipopRegistrationMaster"))
                    {
                        cmdnew.CommandType = CommandType.StoredProcedure;
                        cmdnew.Parameters.AddWithValue("@PARA", "UPDATE_OG_LHNO_AFTER_WEBSERVICES_RESP");
                        cmdnew.Parameters.AddWithValue("@ADDRESSINDEX", id);
                        if (dt123.Rows[0]["RegistrationNumber"] != "")
                        {
                            cmdnew.Parameters.AddWithValue("@LH_NO", dt123.Rows[0]["RegistrationNumber"].ToString());
                        }
                        else
                        {
                            cmdnew.Parameters.AddWithValue("@LH_NO", "-");
                        }
                        utility.Execute(cmdnew);

                        CreateInvoiceForAppointment(dt123.Rows[0]["Reg NO"].ToString(), dt123.Rows[0]["Op No"].ToString(), dt123.Rows[0]["Doc No"].ToString(), dt123.Rows[0]["RegistrationNumber"].ToString(), dt123.Rows[0]["Firstname"].ToString(), dt123.Rows[0]["Middlename"].ToString(), dt123.Rows[0]["Lastname"].ToString(), dtff.Rows[0]["DoctorName"].ToString(), dtff.Rows[0]["DateOfApp"].ToString(), dtff.Rows[0]["PreferredTime"].ToString(), dtff.Rows[0]["Amount"].ToString());

                        #region send mail with attachment to patient
                        DataTable dt = new DataTable();
                        dt = utility.Display("SELECT * FROM GAADADDRESSMASTER WHERE ADDRESSINDEX = '" + id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            string EMailTo = dt.Rows[0]["Email"].ToString();
                            BookAnAppointment appointment = new BookAnAppointment();
                            appointment.EmailAppointment(dtff.Rows[0]["SpecialityName"].ToString(), dtff.Rows[0]["DoctorName"].ToString(), dtff.Rows[0]["AvailableTime"].ToString(), dtff.Rows[0]["DateOfApp"].ToString(), dtff.Rows[0]["PreferredTime"].ToString(), EMailTo, attachmentname);
                        }

                        #endregion

                        using (SqlCommand cmdfailed = new SqlCommand("Proc_FailedAppointment"))
                        {
                            cmdfailed.CommandType = CommandType.StoredProcedure;
                            cmdfailed.Parameters.AddWithValue("@PARA", "DELETE");
                            cmdfailed.Parameters.AddWithValue("@ID", id);
                            if (utility.Execute(cmdfailed))
                            {
                                MyMessageBox1.ShowSuccess("Appointment Fixed Successfully.");
                                BindAppointmentGrid();
                            }
                        }                                                           
                      
                    }
                }
                #endregion
            }   
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindAppointmentGrid();
    }

    #region Send mail and attachment

    public void CreateInvoiceForAppointment(string regno, string opno, string docno, string lhno, string fname, string middlename, string lastname, string docname, string AppDate, string PreferredTime, string Amount)
    {
        #region Create invoice for PDF by Arvind
        StringBuilder ltrlpatientinvoice = new StringBuilder();
        #region get data into litrel
        string regno1 = regno; string opno1 = opno; string docno1 = docno; string lhno1 = lhno; string fname1 = fname; string middlename1 = middlename; string lastname1 = lastname; string docname1 = docname;
        ltrlpatientinvoice.Append(AppointmentInvoiceStructure(regno1, opno1, docno1, lhno1, fname1, middlename1, lastname1, docname1, AppDate, PreferredTime, Amount));
        #endregion

        string strFileName = fname + "_" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + ".pdf";

        //HttpContext.Current.Session["attachmentname"] = strFileName;
        attachmentname = strFileName;

        //string fileName = "pdfDocument" + DateTime.Now.Ticks + ".pdf";
        //string strPath = Request.PhysicalApplicationPath + "\\PDF\\" + fileName;

        StringReader sr = new StringReader(ltrlpatientinvoice.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

        var output = new FileStream(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads\\visa_invitation\\", strFileName), FileMode.Create);

        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, output);
        pdfDoc.Open();
        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
        pdfDoc.Close();

        #endregion
    }

    public string AppointmentInvoiceStructure(string regno, string opno, string docno, string lhno, string fname, string middlename, string lastname, string docname, string AppDate, string PreferredTime, string Amount)
    {
        if (regno == "") { regno = "-"; } if (opno == "") { opno = "-"; } if (docno == "") { docno = "-"; } if (lhno == "") { lhno = "-"; }

        fname = fname + " " + middlename + " " + lastname;


        /// DateTime dateofappointment = Convert.ToDateTime(Request.Cookies["App_date_dtm"].Value);
        string dateofappointment = AppDate + " " + PreferredTime;

        StringBuilder dbinvoice = new StringBuilder();


        dbinvoice.Append("<table id='tblMain' runat='server' width='100%' cellpadding='0' cellspacing='0' style='border:1px solid #ccc; border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; color:#333;font-size:5px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='10'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td><img src='http://lilavatihospital.com/images/invoice_logo.png' title='Lilavati Hospital & Research Center' width='300px' /></td>");
        dbinvoice.Append("<td width='50%' align='center' style='font-size:10px;' valign='middle'><strong>OPD Invoice</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center' height='25'><hr width='96%' color='#04869a' /></td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' cellpadding='5' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td width='39.33%'>Reg. No.: <strong>" + regno + "</strong></td>");
        dbinvoice.Append("<td width='30.33%'>OP No.: <strong>" + opno + "</strong></td>");
        dbinvoice.Append("<td width='30.33%'>Document No.: <strong>" + docno + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td colspan='2'>Patient Name: <strong>" + fname + "</strong></td>");
        dbinvoice.Append("<td>Date: <strong>" + dateofappointment + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td colspan='2'>Referring Dr: <strong>No Reference</strong></td>");
        dbinvoice.Append("<td>New LHNO: <strong>" + lhno + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='30'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='100%' cellpadding='7' style='font-size:10px;'>");
        dbinvoice.Append("<tr bgcolor='#04869a' style='background-color:#04869a;color:#fff;'>");
        dbinvoice.Append("<td><strong>Department</strong></td>");
        dbinvoice.Append("<td><strong>Charge Description</strong></td>");
        dbinvoice.Append("<td width='100'><strong>Qty.</strong></td>");
        dbinvoice.Append("<td><strong>Rs.</strong></td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td>OPD Services</td>");
        dbinvoice.Append("<td>Consultation - Hospital OPD<br />" + docname + "</td>");
        dbinvoice.Append("<td>1</td>");
        dbinvoice.Append("<td>" + Amount + "</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr bgcolor='#eeeeee' style='background-color:#eeeeee;color:#000;'>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td><strong>TOTAL (Rs.)</strong></td>");
        dbinvoice.Append("<td><strong>" + Amount + "</strong></td>");
        dbinvoice.Append("</tr>");


        //dbinvoice.Append("<tr>");
        //dbinvoice.Append("<td colspan='4'>Rupees one thousand five hundred only received in cash</td>");
        //dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("<hr color='#04869a' width='96%' />");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='15'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' cellpadding='5' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td width='69.66%'>Bill Prepared by: <strong>Web Appointment</strong></td>");
        dbinvoice.Append("<td width='30.33%'>Conf By: <strong>Web Appointment</strong></td>");
        dbinvoice.Append("</tr>");
        //dbinvoice.Append("<tr>");
        //dbinvoice.Append("<td>Pan No. <strong>AAATL1398Q</strong></td>");
        //dbinvoice.Append("<td></td>");
        //dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='50'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td valign='top' width='45'><strong>NOTE:</strong> </td>");
        dbinvoice.Append("<td>Please preserve this original invoice carefully and produce it while collecting your investgation reports.<br /> Enclosed Requisition Slip(s) should be handed over to the hospital departments where service is availed.</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td>This is an electronic generated invoice.</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='15'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");

        return dbinvoice.ToString();
    }

    #endregion
}