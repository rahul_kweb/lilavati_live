﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="Updates.aspx.cs" Inherits="Admin_Updates" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        Updates
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="form-box" style="width: 800px;">
        <asp:HiddenField ID="hdnId" runat="server" />
        <label class="control-label">
            Tittle<span class="required">*</span> :
        </label>
        <asp:TextBox ID="txtTittle" runat="server" CssClass="textbox"></asp:TextBox>
        <br />
        <label class="control-label">
            Images :
        </label>
        <asp:FileUpload ID="fileUploadImages" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:Image ID="imgpreview" runat="server" Width="200" Visible="false" />
        <br />
        <label class="control-label">
            Content :
        </label>
        <CKEditor:CKEditorControl ID="txtContent" runat="server"></CKEditor:CKEditorControl>
        <br />
        <label class="control-label">
            Pdf File : 
        </label>
        <asp:FileUpload ID="fileUploadPdfFile" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:HyperLink ID="hyplnkPdf" runat="server" Target="_blank" CssClass="control-label" Style="text-decoration: underline;" Visible="false">View Pdf</asp:HyperLink>
        <br />
        <label class="control-label">
            Doc File :
        </label>
        <asp:FileUpload ID="fileUploadDocFile" runat="server" CssClass="file-upload"></asp:FileUpload>
        <asp:HyperLink ID="hyplnkDoc" runat="server" Target="_blank" CssClass="control-label" Style="text-decoration: underline;" Visible="false">View Doc</asp:HyperLink>
        <br />
        <br />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
        <asp:Button ID="btncancel" runat="server" Text="Cancel" CssClass="button" />
    </div>
    <br />
    <br />
    <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
        PageSize="10" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging" OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging">
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="UPD_ID" HeaderText="ID" />
            <asp:TemplateField HeaderText="Sr. No.">
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UPD_TITTLE" HeaderText="Tittle" />
            <asp:TemplateField HeaderText="Content">
                <ItemTemplate>
                    <asp:Literal ID="litContent" runat="server" Text='<%#Eval("UPD_CONTENT") %>'></asp:Literal>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Images">
                <ItemTemplate>
                    <img src='<%#Eval("UPD_IMAGES","../uploads/updates/{0}") %>' alt="not found" style="width: 300px; display: <%#Eval("UPD_IMAGES").ToString()==""?"none":"block"%>" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pdf File">
                <ItemTemplate>
                    <a href='<%#Eval("UPD_PDF","../uploads/updates/{0}") %>' target="_blank" style="display: <%#Eval("UPD_PDF").ToString()==""?"none":"block"%>">View Pdf </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Doc File">
                <ItemTemplate>
                    <a href='<%#Eval("UPD_DOC","../uploads/updates/{0}") %>' target="_blank" style="display: <%#Eval("UPD_DOC").ToString()==""?"none":"block"%>">View Doc </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
                <ItemTemplate>
                    <asp:ImageButton ID="btnedit" runat="server" CommandName="Select" ImageUrl="images/Editbutton.png"
                        Height="41px" margin-top="11px" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
                <ItemTemplate>
                    <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                        Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('sure do you want to delete this recored?');"
                        CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pagination"></PagerStyle>
    </asp:GridView>


</asp:Content>

