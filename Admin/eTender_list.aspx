﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/admin.master" AutoEventWireup="true" CodeFile="eTender_list.aspx.cs" Inherits="eTender_list" %>

<%@ Register Src="~/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentPlaceHolder" runat="Server">
    <div class="page-header">
        eTender list
    </div>
    <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
    <div class="row-fluid">
        <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" CssClass="mGrid"
            PageSize="20" AllowPaging="true" OnPageIndexChanging="gdView_PageIndexChanging"
            OnRowDeleting="gdView_RowDeleting">
            <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="eId" HeaderText="ID" />
                <asp:TemplateField HeaderText="Sr No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="eTenderNo" HeaderText="Tender No." />
                <asp:BoundField DataField="TenderName" HeaderText="Tender Name" />
                <asp:BoundField DataField="TenderStartDateTime" HeaderText="Start Date & Time" />
                <asp:BoundField DataField="TenderEndDateTime" HeaderText="End Date & Time" />
                <asp:BoundField DataField="LastTenderReceiveDateTime" HeaderText="Last Tender Receive Date & Time" />
                <asp:BoundField DataField="TenderOpeningDateTime" HeaderText="Tender Opening Date & Time" />
               <%-- <asp:BoundField DataField="Department" HeaderText="Department" />
                <asp:BoundField DataField="Category" HeaderText="Category" />
                <asp:BoundField DataField="TenderDescription" HeaderText="Tender Description" />
                <asp:BoundField DataField="Remarks" HeaderText="Remarks" />--%>
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:TemplateField HeaderText="View Detail">
                    <ItemTemplate>
                        <a href='eTender_details.aspx?<%#Eval("eId","Id={0}") %>' style="text-decoration: none; color: black">View Detail</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Edit">
                    <ItemTemplate>

                        <a href='eTender.aspx?<%#Eval("eId","Id={0}") %>' style="text-decoration: none; color: black;">
                            <img src="images/Editbutton.png" style="height: 35px; margin-top: 5px;" /></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="btndelete" runat="server" ImageUrl="images/deletebutton.png"
                            Height="41px" margin-top="11px" OnClientClick="javascript:return confirm('Are you sure want to delete this recored?');"
                            CommandName="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle CssClass="pagination"></PagerStyle>
        </asp:GridView>
    </div>
</asp:Content>
