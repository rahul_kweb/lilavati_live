﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="lactation.aspx.cs" Inherits="chest_medicine" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Lactation</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Chest_medicine.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>


                    
            <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>




                    <div class="clear"></div>
                </div>


                <%--  <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Department of Pulmonary diseases caters to patients with symptoms related to lungs, breathing, allergies and sleep disorders. There is a well- equipped setup with a dedicated sleep laboratory suite, endoscopy laboratory and pulmonary function laboratory. In a month, we treat more than 1200 patients on outpatient basis and more than 300 patients on inpatient basis. The usual diagnostic modalities in the department are pulmonary function test, skin allergy test and bronchoscopy. Under the guidance of our senior consultatns, a strong DNB program has been flourishing wherein approximately 10 efficient DNB students at a time are trained in our super speciality, who all form an important link for continued medical care. Thereby, regular clinico-radiological and clinico-pathological conferences are being conducted to enhance the clinical approach and also to the research work.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <p>In an out-patient department we offer a detailed comprehensive patient assessment by our panel of experts followed by essential diagnostic tests and appropriate treatment. For our in-patients, a multimodality support system aids to the overall patient improvement. Pulmonary function test not only help by diagnosing different respiratory diseases but also assess the functional status of patient. We also have a fully functioning skin allergy testing facility which not only supports advancements in dermatology but also throws light upon allergic pulmonary diseases. Asset of advanced surgical support in the form of video - assisted thoracoscopic lung biopsy or open lung biopsy, in our setup helps in diagnosing difficult cases which stay undiagnosed despite thorough non invasive evaluation.  Our diagnostic work up is further complimented by ancillary facilities of Pulmonary Rehabilitation program in the hospital premises which provides even better tender care to patients with chronic lung diseases by improving their quality of life.</p>

                            <div class="clear"></div>
                        </div>
                    </div>


                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <p>We run an advanced pulmonary function laboratory where around 200 patients in a month are being evaluated with spirometry, diffusion study, 6 minute walk test and allergy testing successfully as per the indication. We also have a dedicated endoscopy suite where flexible fibreoptic  Bronchoscopy is done. We have been giving excellent results with bronchoalveolar lavage, trans-bronchial needle aspiration, trans -bronchial lung biopsy, cryoprobe application etc to facilitate accurate diagnosis and eventually standard treatment as per international norms. We have excellent results of bronchoscopy guided foreign body removal. The department has latest technology equipments supported with a dedicated skilled staff to perform Complete overnight polysomnography study with CPAP titration in our sleep laboratory for patients with suspected sleep related ailments.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>



            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

