﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true"
    CodeFile="doctor_profile.aspx.cs" Inherits="doctor_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" />
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox();

        });
    </script>
    <style type="text/css">
        .select2-container--default .select2-selection--single {
            background-color: #fff; /* border: 1px solid #aaa; */
            border-radius: 4px;
            border-bottom: 0px solid #ddd;
        }

        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('/images/loader.gif') 50% 50% no-repeat rgb(249,249,249);
        }
    </style>
    <script type="text/javascript">
        $(window).load(function () {
            $(".loader").fadeOut("slow");
            $(".loader").fadeOut(1000);

        });

        //success msg

        function ShowPopUp() {
            $(function () {
                $("#showmsg").fancybox().trigger('click');
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container gray_box fade_anim" style="position: relative">
        <div class="loader">
        </div>
        <asp:Literal ID="ltrldrdetail" runat="server"></asp:Literal>
        <%--    <div class="profile_green_banner">
            <div class="doc_name_box">
                <div class="doc_img">
                    <img src="images/doctor_pic_2.jpg">
                </div>
                <div class="doc_prof">
                    <div class="doc_name uppercase">
                        Dr. Heena S. Garude</div>
                    <div>
                        Head of Department of Physiotherapy</div>
                    <div>
                        MSc. PT, Cert. MDT, C.M.P.</div>
                    <div>
                        physiotherapy@lilavatihospital.com</div>
                </div>
            </div>
        </div>--%>
        <%-- <div class="clear">
        </div>--%>
        <%--<div class="prof_main container text_format">
            <div class="side_box">
                <div class="schedule_tl green_text uppercase">
                    OPD Schedule
                </div>
                <table class="schedule">
                    <tbody>
                        <tr>
                            <td>
                                Mon
                            </td>
                            <td>
                                7am - 11am
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tue
                            </td>
                            <td>
                                7am - 11am
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Wed
                            </td>
                            <td>
                                7am - 11am
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Thu
                            </td>
                            <td>
                                7am - 11am
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fri
                            </td>
                            <td>
                                7am - 11am
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Sat
                            </td>
                            <td>
                                7am - 11am
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="clear">
                </div>

            <div class="appt_contact">
    	<div class="tl">For appointment contact</div>
    	<div class="details">
        <strong class="nos">9820100947/ 9987110947</strong>
        between 9 AM to 5 PM<br> 
        from MONDAY to FRIDAY</div>
    </div>

                <a href="#" class="bk_app_btn">Book an Appointment</a>
                <div class="clear">
                </div>
                <div class="ad_box">
                    <img src="images/ad_img.jpg">
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="prof_content">
                <div class="tabber">
                    <div class="tabbertab">
                        <h2>
                            Profile</h2>
                        <div class="tab_content">
                            <p class="hd green_text uppercase">
                                <strong>Area Of Interest</strong></p>
                            <ul class="bullet">
                                <li>Musculoskeletal conditions</li>
                                <li>Mechanical diagnosis – spine & extremities</li>
                                <li>Comprehensive shoulder care</li>
                                <li>Hand rehab including complex reconstruction & re-implantation</li>
                            </ul>
                            <p class="hd green_text uppercase">
                                <strong>Credentials</strong></p>
                            <ul class="bullet">
                                <li>Chief Physiotherapist in Lilavati Hospital for more than 15 years.Expanded the department
                                    from a strength of 2 physiotherapists to 24 physiotherapists of varying specialities
                                    namely musculoskeletal, neurosciences-adult and paediatric, cardiac & pulmonary.</li>
                                <li>25 years of work experience in Physiotherapy.</li>
                                <li>Ranked first in M.Sc. PT, University of Bombay.</li>
                                <li>Worked as a lecturer in KEM Hospital.</li>
                                <li>Worked for 3 years in NHS Hospitals, UK.</li>
                                <li>Director with McKenzie Institute India.</li>
                                <li>Instrumental in starting free interactive education sessions for Cardiac & Pulmonary
                                    Rehabilitation patients.</li>
                            </ul>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <!-- xxxxxxxxxxxxxxxxxx -->
                    <div class="tabbertab">
                        <h2>
                            Awards and Recognition</h2>
                        <div class="tab_content">
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <!-- xxxxxxxxxxxxxxxxxx -->
                    <div class="tabbertab">
                        <h2>
                            Research Publications</h2>
                        <div class="tab_content">
                            <p>
                                Presented paper on Patient Empowerment along with a case study at Mckenzie Symposium
                                in November 2015.</p>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                    <!-- xxxxxxxxxxxxxxxxxx -->
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
        </div>--%>
        <div id="apply_form1" style="display: none;">
            <div class="apply_form_tl uppercase green_text">
                Apologize for an appointment
            </div>
            <div class="clear">
            </div>
            <div class="form_apply_main">
                <div class="form_centering_div" style="font-size: 12px; line-height: 18px; color: #000; border-top: 1px solid #ccc; padding: 10px 15px 15px; text-align: center; background: #eee;">
                    The Doctor is not holding any OPD at LHRC,
                    <br />
                    And appointment for same cannot be taken.
                </div>
            </div>
        </div>

        <div id="showmsg" class="hc_form_div" style="display: none;">

            <div class="apply_form_tl uppercase green_text">Video Consultation</div>
            <div class="clear"></div>

            <div class="hc_thanks text_format">
                <p><strong class="green_text">Video consultation not available.</strong></p>
            </div>

            <div class="clear"></div>
        </div>


        <div class="clear">
        </div>
    </div>
</asp:Content>
