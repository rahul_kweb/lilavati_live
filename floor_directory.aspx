﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="floor_directory.aspx.cs" Inherits="floor_directory" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Floor Directory</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg" />
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<table class="floor_directory">
                    <thead>
                        <tr>
                            <td>Floor</td>
                            <td>"A" Wing</td>
                            <td>Lobby</td>
                            <td>"B" Wing</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Floor">Basement</td>
                            <td data-title="'A' Wing">- </td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">Physiotherapy Department, Free OPD </td>
                        </tr>
                        <tr>
                            <td data-title="Floor">Ground Floor</td>
                            <td data-title="'A' Wing">OPD, Dental Clinic, Dermo Cosmetology Clinic, X Ray, Sonography, CT, Nuclear Medicine, PFT, Sample Collection Room, Chemist Shop </td>
                            <td data-title="Lobby">Admission, Reception, Central Dispatch, TPA, Help Desk</td>
                            <td data-title="'B' Wing">Casualty, MRI, Day Care, Operation Theater Suite(7 & 8), Perioperative Care, Endoscopy & Billing, IP Billing & Discharge, Bank </td>
                        </tr>
                        <tr>
                            <td data-title="Floor">1st Floor</td>
                            <td data-title="'A' Wing">Operation Theaters (1,2,3,4,5,6,9,11), Histopathology </td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">ICCU, SICU, Cath Lab (100-132) </td>
                        </tr>
                        <tr>
                            <td data-title="Floor">2nd Floor</td>
                            <td data-title="'A' Wing">Pathology, Blood Bank </td>
                            <td data-title="Lobby">Cafeteria </td>
                            <td data-title="'B' Wing">Health Check up, IVF, Cardiology, ENT, Opthalmology, EMG, EEG </td>
                        </tr>
                        <tr>
                            <td data-title="Floor">3rd Floor</td>
                            <td data-title="'A' Wing">Executive Offices</td>
                            <td data-title="Lobby">-  </td>
                            <td data-title="'B' Wing">Administrative Offices </td>
                        </tr>
                        <tr>
                            <td data-title="Floor">4th Floor</td>
                            <td data-title="'A' Wing">-</td>
                            <td data-title="Lobby">Health Checkup Dining</td>
                            <td data-title="'B' Wing">-</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">5th Floor</td>
                            <td data-title="'A' Wing">-</td>
                            <td data-title="Lobby">Refugee Floor</td>
                            <td data-title="'B' Wing">-</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">6th Floor</td>
                            <td data-title="'A' Wing">Artificial Kidney Dialysis(AKD) </td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">ICU (601 -635)</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">7th Floor</td>
                            <td data-title="'A' Wing">Bed: 701 — 733</td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">Bed: 734 - 776</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">8th Floor</td>
                            <td data-title="'A' Wing">Bed: 801 — 825, Stroke Unit</td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">Bed: 830 - 856</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">9th Floor</td>
                            <td data-title="'A' Wing">Bed: 901 - 925</td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">Bed: 926 - 946</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">10th Floor</td>
                            <td data-title="'A' Wing">Bed: 1001 - 1017, Gynaec OT</td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">Bed: 1021 - 1051, PICU & NICU</td>
                        </tr>
                        <tr>
                            <td data-title="Floor">11th Floor</td>
                            <td data-title="'A' Wing">Bed: 11O1- 1112</td>
                            <td data-title="Lobby">- </td>
                            <td data-title="'B' Wing">Bed: 1113 - 1123</td>
                        </tr>
                    </tbody>
                </table>--%>



                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

