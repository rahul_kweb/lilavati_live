﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="ambulance.aspx.cs" Inherits="ambulance" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Ambulance</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%-- <div class="inner_banner">
                    <img src="images/inner_banner/Ambulance.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>State-of-the-art Ambulances at our hospital are equipped to handle most complex emergencies and are available to our patients 24 X 7. </li>
                                <li>These ambulances carry standard equipment’s like ventilator, Defibrillator, Oxygen, suction, scoop stretcher, spine board, head immobilizer, spencer trolley stretcher and lifesaving drugs.</li>
                                <li>These ambulances are manned by ALS / BLS trained Doctors, Nurses along with trained drivers and attendants, all are well trained in managing emergencies in their respective fields.</li>
                            </ul>

                            <p><strong class="green_text uppercase">Conditions</strong></p>
                            <ul class="bullet">
                                <li>Technical problem/failure of equipment/ breakdown of ambulance.</li>
                                <li>Any traffic jam, wrong address given & deadline to reach the destination.</li>
                                <li>This is not a free service and hence the patient will have to borne the Ambulance charges.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Ambulance Types</h2>

                        <p>To ensure that we meet different needs ranging from fracture to cardiac emergencies, we have following ambulance services:</p>
                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Cardiac Ambulance </li>
                                <li>Trauma Ambulance</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Coverage Boundary</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Western side: upto Virar</li>
                                <li>Harbour side: upto Panvel</li>
                                <li>Central side: upto Kalyan</li>

                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Reach us</h2>

                        <div class="tab_content">

                            <p><strong>Contact Us</strong></p>

                            <ul class="bullet">
                                <li>09769250010</li>
                                <li>07506358779</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->



                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

