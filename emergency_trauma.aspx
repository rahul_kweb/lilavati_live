﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="emergency_trauma.aspx.cs" Inherits="emergency_trauma" %>

<%@ Register TagName="Service" TagPrefix="menu" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Emergency/ Trauma</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Emergency_Trauma.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">

                            <p>We have an exclusive centre for emergency response to patients requiring urgent & immediate treatment. Lilavati Hospital & Research Centre’s Trauma/Emergency Care is world class with 24x7 availability, access to modern operation theatre’s, 24 hours pharmacy, imaging and diagnostic centres and round the clock superspeciality doctors on call.</p>
                            <p>The doctors and support staff here are well trained and equipped to handle any emergency. This goes a long way in ensuring that the patient is given prompt and accurate treatment.</p>

                            <p class="uppercase"><strong class="green_text">Facilities</strong></p>
                            <ul class="bullet">
                                <li>Tertiary Trauma centre with a world class trauma team which consists of specialists in the area of neurosurgery, orthopaedics, general surgery & anaesthesiology.</li>
                                <li>Emergency operation theatre, triage room, trauma bay, transit ward, code system with CPR team and a shock room for cardiac arrest patients to administer CPR (Cardiopulmonary Resuscitation).</li>
                                <li>Equipments include ventilators, cardiac monitors, biphasic defibrillator and other necessary emergency equipments including the drugs.</li>
                                <li>Mobile X-ray unit, an Ultrasonography, CT & MRI facility.</li>
                                <li>24x7 ambulance service with a control room.</li>
                                <li>24 hours pharmacy</li>
                                <li>24 hours superspeciality doctors on call.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <p><strong>The department is equipped to handle all emergencies ranging from minor to major medical and accident emergencies, including below:</strong></p>
                            <ul class="bullet">
                                <li>Surgical</li>
                                <li>Medical</li>
                                <li>Emergency care for elderly / handicapped, terminally ill or patients with multi-system failures</li>
                                <li>Treatment of mass causalities and disaster victims</li>
                                <li>Simple procedures like wound suturing, incision and drainage, dressing, application of plaster of Paris casts</li>
                                <li>Cardiopulmonary resuscitation, endotracheal intubation, securing central venous access, facility for treating poly -trauma and severe head injuries.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Reach us</h2>

                        <div class="tab_content">

                            <ul class="bullet">
                                <li>Contact: 91-22-26568063</li>
                                <li>Ambulance: 09769250010 / 07506358779</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">
                <menu:Service ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

