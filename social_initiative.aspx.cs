﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class social_initiative : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();

            BindBanner();
        }
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Social Initiative";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        string seva = dt.Rows[1]["DESCRIPTION"].ToString();
        seva = seva.Replace("../", "");
        dt.Rows[1]["DESCRIPTION"] = seva;

        string dsc = dt.Rows[0]["Description"].ToString();

        dsc = dsc.Replace("&nbsp;", "");
        string val = dsc;

        dt.Rows[0]["Description"] = val;

        string dsc1 = dt.Rows[2]["Description"].ToString();

        dsc1 = dsc1.Replace("&nbsp;", "");
        string val1 = dsc1;

        dt.Rows[2]["Description"] = val1;

        string dsc2 = dt.Rows[3]["Description"].ToString();

        dsc2 = dsc2.Replace("&nbsp;", "");
        string val2 = dsc2;

        dt.Rows[3]["Description"] = val2;


        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();

            rptContent1.DataSource = dt;
            rptContent1.DataBind();
        }
    }


    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(13);
    }

}