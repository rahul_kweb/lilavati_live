﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class doctor_profile : System.Web.UI.Page
{
    Utility utility = new Utility();
    string areaofintrested = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //string id = Request.QueryString["Id"].ToString();



        if (Page.RouteData.Values["Id"] != null)
        {
            Session["PreviousPage"] = "PreviousPage";

            string id = Page.RouteData.Values["Id"].ToString();
            int value;
            //Try converting the value to integer
            bool isValueNumeric = int.TryParse(Page.RouteData.Values["Id"].ToString(), out value);
            if (isValueNumeric)
            {
                BindDoctor(id);
            }
            else
            {

            }


        }
        else
        {
            Response.Redirect("/Home");
        }


    }
    public void BindDoctor(string id)
    {
        try
        {
            StringBuilder drlist = new StringBuilder();
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
            string Specility = string.Empty;

            if (dt.Rows.Count > 0)
            {
                drlist.Append("<div class='profile_green_banner'>");
                drlist.Append("<div class='doc_name_box'>");
                drlist.Append("<div class='doc_img'>");
                drlist.Append("<div class='docImgHolder'>");
                drlist.Append("<img src='/Admin/Doctors/" + dt.Rows[0]["PHOTO_VCR"].ToString() + "'>");
                drlist.Append("</div>");
                drlist.Append("</div>");
                drlist.Append("<div class='doc_prof'>");
                drlist.Append("<div class='doc_name uppercase'>");
                drlist.Append("Dr. " + dt.Rows[0]["DOCTOR_NAME_VCR"].ToString() + "");
                drlist.Append("</div>");
                drlist.Append("<div>");
                foreach (DataRow speciailtyList in dt.Rows)
                {
                    Specility += speciailtyList["SPECIALITY_VCR"].ToString() + ", ";

                }
                Specility = Specility.Substring(0, Specility.Length - 2);
                drlist.Append("Department of " + Specility);
                drlist.Append("</div>");
                drlist.Append("<div>");
                drlist.Append("" + dt.Rows[0]["QUALIFICATION_VCR"].ToString() + "");
                drlist.Append("</div>");
                //drlist.Append("<div>");
                //drlist.Append("<a href='mailto:physiotherapy@lilavatihospital.com'>physiotherapy@lilavatihospital.com</a>");
                //drlist.Append("</div>");
                drlist.Append("</div>");
                drlist.Append("</div>");
                drlist.Append("</div>");
                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");

                drlist.Append("<div class='prof_main container text_format'>");
                drlist.Append("<div class='side_box'>");
                drlist.Append("<div class='schedule_tl green_text uppercase'>");
                drlist.Append("OPD Schedule");
                drlist.Append("</div>");
                drlist.Append("<table class='schedule'>");
                drlist.Append("<tbody>");
                drlist.Append("<tr>");
                drlist.Append("<td>");

                drlist.Append("Mon");
                drlist.Append("</td>");
                drlist.Append("<td>");
                drlist.Append(BindMondayTime(id));
                //drlist.Append("7am - 11am");

                drlist.Append("</td>");
                drlist.Append("</tr>");
                drlist.Append("<tr>");
                drlist.Append("<td>");
                drlist.Append("Tue");
                drlist.Append("</td>");
                drlist.Append("<td>");
                drlist.Append(BindTuesdayTime(id));
                //drlist.Append("7am - 11am");

                drlist.Append("</td>");
                drlist.Append("</tr>");
                drlist.Append("<tr>");
                drlist.Append("<td>");
                drlist.Append("Wed");


                drlist.Append("</td>");
                drlist.Append("<td>");
                drlist.Append(BindWednesdayTime(id));

                //drlist.Append("7am - 11am");
                drlist.Append("</td>");
                drlist.Append("</tr>");
                drlist.Append("<tr>");
                drlist.Append("<td>");
                drlist.Append("Thu");
                drlist.Append("</td>");
                drlist.Append("<td>");
                drlist.Append(BindThursdayTime(id));

                //drlist.Append("7am - 11am");
                drlist.Append("</td>");

                drlist.Append("</tr>");
                drlist.Append("<tr>");
                drlist.Append("<td>");

                drlist.Append("Fri");
                drlist.Append("</td>");
                drlist.Append("<td>");
                drlist.Append(BindFridayTime(id));

                //drlist.Append("7am - 11am");
                drlist.Append("</td>");
                drlist.Append("</tr>");
                drlist.Append("<tr>");
                drlist.Append("<td>");
                drlist.Append("Sat");

                drlist.Append("</td>");
                drlist.Append("<td>");
                drlist.Append(BindSaturdayTime(id));


                //drlist.Append("7am - 11am");
                drlist.Append("</td>");
                drlist.Append("</tr>");
                drlist.Append("</tbody>");
                drlist.Append("</table>");
                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");

                if (bool.Parse(dt.Rows[0]["DirectAvailable"].ToString()) == true)
                {
                    if (CheckDays() == 1)
                    {
                        if (dt.Rows[0]["ContactNo"].ToString() == "" && dt.Rows[0]["DirectTimeFrom"].ToString() == "")
                        {

                        }
                        else
                        {
                            drlist.Append("<div class='appt_contact'>");
                            drlist.Append("<div class='tl'>For appointment contact</div>");
                            drlist.Append("<div class='details'>");
                            drlist.Append("<strong class='nos'>" + dt.Rows[0]["ContactNo"].ToString() + "</strong>");
                            if (dt.Rows[0]["DirectTimeFrom"].ToString() != "" || dt.Rows[0]["DirectTimeFrom"].ToString() != string.Empty)
                            {
                                drlist.Append(dt.Rows[0]["DirectTimeFrom"].ToString());
                            }
                            //if (dt.Rows[0]["DirectDaysFrom"].ToString() != "" || dt.Rows[0]["DirectDaysFrom"].ToString() != string.Empty && dt.Rows[0]["DirectDaysTo"].ToString() != "" || dt.Rows[0]["DirectDaysTo"].ToString() != string.Empty)
                            //{ 
                            //    drlist.Append("from " + dt.Rows[0]["DirectDaysFrom"].ToString() + " to " + dt.Rows[0]["DirectDaysTo"].ToString() + "");
                            //}
                            drlist.Append("</div>");
                            drlist.Append("</div>");
                        }
                      

                    }
                    else
                    {
                        if ((dt.Rows[0]["ContactNo"].ToString() == "" || dt.Rows[0]["ContactNo"].ToString() != string.Empty) && (dt.Rows[0]["DirectTimeFrom"].ToString() != "" || dt.Rows[0]["DirectTimeFrom"].ToString() != string.Empty))
                        {

                        }
                        else
                        {
                            drlist.Append("<div class='appt_contact'>");
                            drlist.Append("<div class='tl'>For appointment contact</div>");
                            drlist.Append("<div class='details'>");
                            drlist.Append("<strong class='nos'>" + dt.Rows[0]["ContactNo"].ToString() + "</strong>");
                            if (dt.Rows[0]["DirectTimeFrom"].ToString() != "" || dt.Rows[0]["DirectTimeFrom"].ToString() != string.Empty)
                            {
                                drlist.Append(dt.Rows[0]["DirectTimeFrom"].ToString());
                            }
                            //if (dt.Rows[0]["DirectDaysFrom"].ToString() != "" || dt.Rows[0]["DirectDaysFrom"].ToString() != string.Empty && dt.Rows[0]["DirectDaysTo"].ToString() != "" || dt.Rows[0]["DirectDaysTo"].ToString() != string.Empty)
                            //{
                            //    drlist.Append("from " + dt.Rows[0]["DirectDaysFrom"].ToString() + " to " + dt.Rows[0]["DirectDaysTo"].ToString() + "");
                            //}
                            drlist.Append("</div>");
                            drlist.Append("</div>");
                        }
                     

                    }
                }
                else
                {
                    if (CheckDays() == 1)
                    {
                        drlist.Append("<a href=" + RedirectAfterClickOnAppointment() + " class='bk_app_btn'>");
                        drlist.Append("Book an Appointment");
                        drlist.Append("</a>");
                    }
                    else
                    {
                        drlist.Append("<a href='#apply_form1' class='fancybox bk_app_btn'>");
                        drlist.Append("Book an Appointment");
                        drlist.Append("</a>");
                    }
                }

                //Tele consultation
                if (bool.Parse(dt.Rows[0]["TeleConsultation"].ToString()))
                {
                    drlist.Append("<a href='https://teleconsulting.lilavatihospital.com:8013/' target='_blank' class='bk_app_btn'>");
                    drlist.Append("Video Consultation");
                    drlist.Append("</a>");
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "show", "ShowPopUp();", true);
                }



                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");
                drlist.Append("<div class='ad_box'>");
                drlist.Append("<a href='/Health-Checkup?tab_list=0'>");
                drlist.Append("<img src='/images/ad_img.jpg'>");
                drlist.Append("</a>");
                drlist.Append("</div>");
                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");
                drlist.Append("</div>");


                drlist.Append("<div class='prof_content'>");
                drlist.Append("<div class='tabber'>");


                drlist.Append("<div class='tabbertab'>");
                drlist.Append("<h2>");
                drlist.Append("Profile");
                drlist.Append("</h2>");
                drlist.Append(dt.Rows[0]["PROFILECNT"].ToString());

                if (BindAreaofIntrested(id) != "")
                {
                    drlist.Append("<div class='tab_content'>");
                    drlist.Append("<p class='hd green_text uppercase'>");
                    drlist.Append("<strong>Area Of Interest</strong>");
                    drlist.Append("</p>");
                    drlist.Append("<ul class='bullet'>");
                    drlist.Append(BindAreaofIntrested(id));
                    drlist.Append("</ul>");
                    drlist.Append("<div class='clear'>");
                    drlist.Append("</div>");
                    drlist.Append("</div>");
                }

                drlist.Append("</div>");


                //<!-- xxxxxxxxTab codexxxxxxxxxx -->

                if (BindAwardsRecoginition(id) != "")
                {
                    drlist.Append("<div class='tabbertab'>");
                    drlist.Append("<h2>Awards and Recognition</h2>");
                    drlist.Append("<div class='tab_content'>");
                    drlist.Append("<ul class='bullet'>");
                    drlist.Append(BindAwardsRecoginition(id));
                    drlist.Append("</ul>");
                    drlist.Append("<div class='clear'>");
                    drlist.Append("</div>");
                    drlist.Append("</div>");
                    drlist.Append("</div>");
                }

                if (BindResearchPublication(id) != "")
                {
                    drlist.Append("<div class='tabbertab'>");
                    drlist.Append("<h2>Research Publications</h2>");
                    drlist.Append("<div class='tab_content'>");
                    drlist.Append("<ul class='bullet'>");
                    drlist.Append(BindResearchPublication(id));
                    drlist.Append("</ul>");
                    drlist.Append("<div class='clear'>");
                    drlist.Append("</div>");
                    drlist.Append("</div>");
                    drlist.Append("</div>");
                }


                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");
                drlist.Append("</div>");

                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");
                drlist.Append("</div>");
                drlist.Append("<div class='clear'>");
                drlist.Append("</div>");
                drlist.Append("</div>");
            }
            ltrldrdetail.Text = drlist.ToString();
        }
        catch (Exception Ex)
        {
            this.Title = Ex.Message;
        }
    }
    public string BindAreaofIntrested(string id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");

        areaofintrested = dt.Rows[0]["AREAOFINTEREST_VCR"].ToString();

        areaofintrested = areaofintrested.Replace("<ul>", "").Replace("</ul>", "");
        return areaofintrested;


    }
    public string BindAwardsRecoginition(string id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");

        string achievement = dt.Rows[0]["ACHIEVEMENTS_VCR"].ToString();

        achievement = achievement.Replace("<ul>", "").Replace("</ul>", "");
        return achievement;
    }
    public string BindResearchPublication(string id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");

        string ResearchPub = dt.Rows[0]["RESEARCHPUBLICATION"].ToString();

        ResearchPub = ResearchPub.Replace("<ul>", "").Replace("</ul>", "");
        return ResearchPub;
    }
    public string BindMondayTime(string id)
    {
        DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string mondaytime = dt.Rows[0]["AVAIL_TIME_MON_VCR"].ToString();
        char[] monday = new char[] { '#' };
        string[] monday1;
        string monday2 = string.Empty;
        if (mondaytime.Contains("#") == true)
        {
            monday1 = mondaytime.Split(monday);
            foreach (string time in monday1)
            {
                monday2 += time.ToString() + "<br>";
            }
            monday2 = monday2.Substring(0, monday2.Length - 4);
        }
        else
        {
            monday2 = dt.Rows[0]["AVAIL_TIME_MON_VCR"].ToString();
            string checkvalue = dt.Rows[0]["Avail_Time_Mon_vcr1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                monday2 += "<br />" + dt.Rows[0]["Avail_Time_Mon_vcr1"].ToString();
            }
            else
            {

            }
        }
        return monday2.ToUpper();
    }
    public string BindTuesdayTime(string id)
    {
        DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string tuesdaytime = dt.Rows[0]["AVAIL_TIME_TUE_VCR"].ToString();
        char[] tuesday = new char[] { '#' };
        string[] tuesday1;
        string tuesday2 = string.Empty;
        if (tuesdaytime.Contains("#") == true)
        {
            tuesday1 = tuesdaytime.Split(tuesday);
            foreach (string time in tuesday1)
            {
                tuesday2 += time.ToString() + "<br>";
            }
            tuesday2 = tuesday2.Substring(0, tuesday2.Length - 4);
        }
        else
        {
            tuesday2 = dt.Rows[0]["AVAIL_TIME_TUE_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_TUE_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                tuesday2 += "<br />" + dt.Rows[0]["AVAIL_TIME_TUE_VCR1"].ToString();
            }
            else
            {

            }


        }
        return tuesday2.ToUpper();
    }
    public string BindWednesdayTime(string id)
    {
        DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");


        string wednesdaytime = dt.Rows[0]["AVAIL_TIME_WED_VCR"].ToString();
        char[] wednesday = new char[] { '#' };
        string[] wednesday1;
        string wednesday2 = string.Empty;
        if (wednesdaytime.Contains("#") == true)
        {
            wednesday1 = wednesdaytime.Split(wednesday);
            foreach (string time in wednesday1)
            {
                wednesday2 += time.ToString() + "<br>";
            }
            wednesday2 = wednesday2.Substring(0, wednesday2.Length - 4);
        }
        else
        {
            wednesday2 = dt.Rows[0]["AVAIL_TIME_WED_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_WED_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                wednesday2 += "<br />" + dt.Rows[0]["AVAIL_TIME_WED_VCR1"].ToString();
            }
            else
            {

            }

        }
        return wednesday2.ToUpper();
    }
    public string BindThursdayTime(string id)
    {
        DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string thursdaytime = dt.Rows[0]["AVAIL_TIME_THU_VCR"].ToString();
        char[] thursday = new char[] { '#' };
        string[] thursday1;
        string thursday2 = string.Empty;
        if (thursdaytime.Contains("#") == true)
        {
            thursday1 = thursdaytime.Split(thursday);
            foreach (string time in thursday1)
            {
                thursday2 += time.ToString() + "<br>";
            }
            thursday2 = thursday2.Substring(0, thursday2.Length - 4);
        }
        else
        {
            thursday2 = dt.Rows[0]["AVAIL_TIME_THU_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_THU_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                thursday2 += "<br />" + dt.Rows[0]["AVAIL_TIME_THU_VCR1"].ToString();
            }
            else
            {

            }
        }
        return thursday2.ToUpper();
    }
    public string BindFridayTime(string id)
    {
        DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string fridaytime = dt.Rows[0]["AVAIL_TIME_FRI_VCR"].ToString();
        char[] friday = new char[] { '#' };
        string[] friday1;
        string friday2 = string.Empty;
        if (fridaytime.Contains("#") == true)
        {
            friday1 = fridaytime.Split(friday);
            foreach (string time in friday1)
            {
                friday2 += time.ToString() + "<br>";
            }
            friday2 = friday2.Substring(0, friday2.Length - 4);
        }
        else
        {
            friday2 = dt.Rows[0]["AVAIL_TIME_FRI_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_FRI_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                friday2 += "<br />" + dt.Rows[0]["AVAIL_TIME_FRI_VCR1"].ToString();
            }
            else
            {

            }
        }
        return friday2.ToUpper();
    }
    public string BindSaturdayTime(string id)
    {
        DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string saturdaytime = dt.Rows[0]["AVAIL_TIME_SAT_VCR"].ToString();
        char[] saturday = new char[] { '#' };
        string[] saturday1;
        string saturday2 = string.Empty;
        if (saturdaytime.Contains("#") == true)
        {
            saturday1 = saturdaytime.Split(saturday);
            foreach (string time in saturday1)
            {
                saturday2 += time.ToString() + "<br>";
            }
            saturday2 = saturday2.Substring(0, saturday2.Length - 4);
        }
        else
        {
            saturday2 = dt.Rows[0]["AVAIL_TIME_SAT_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_SAT_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                saturday2 += "<br />" + dt.Rows[0]["AVAIL_TIME_SAT_VCR1"].ToString();
            }
            else
            {

            }
        }
        return saturday2.ToUpper();
    }
    public string RedirectAfterClickOnAppointment()
    {
        string id = Page.RouteData.Values["Id"].ToString();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");


        string url = string.Empty;
        if (dt.Rows.Count > 0)
        {
            Session["appointmentstart"] = "process";
            Session["specialtyid"] = dt.Rows[0]["SPECIALITY_ID_BINT"].ToString();
            Session["doctorid"] = dt.Rows[0]["DOCTOR_ID_BINT"].ToString();
            Session["doctorname"] = dt.Rows[0]["DOCTOR_NAME_VCR"].ToString();
            Session["specialtyname"] = dt.Rows[0]["SPECIALITY_VCR"].ToString();
            Session["doctorconsdocid"] = dt.Rows[0]["CONSDOCID"].ToString();

            url = "/Book-Appointment/" + dt.Rows[0]["DOCTOR_ID_BINT"].ToString();

            //if (Session[AppKey.EmailID] != null && Session[AppKey.Password] != null)
            //{
            //    url = "/Dashboard/Book-An-Appointment";


            //}
            //else
            //{
            //     url = "/login";
            //}
        }
        else
        {
            url = "#";
        }
        return url;
    }
    public int CheckDays()
    {
        string id = Page.RouteData.Values["Id"].ToString();
        int result;
        //if ((BindMondayTime(id) == "N.A" || BindMondayTime(id) == "N.A." || BindMondayTime(id) == "----" || BindMondayTime(id) == "-") && (BindTuesdayTime(id) == "N.A" || BindTuesdayTime(id) == "N.A." || BindTuesdayTime(id) == "----" || BindTuesdayTime(id) == "-") && (BindWednesdayTime(id) != "N.A" || BindWednesdayTime(id) != "N.A." || BindWednesdayTime(id) == "----" || BindWednesdayTime(id) == "-") && (BindThursdayTime(id) != "N.A" || BindThursdayTime(id) != "N.A." || BindThursdayTime(id) == "----" || BindThursdayTime(id) == "-") && (BindFridayTime(id) != "N.A" || BindFridayTime(id) != "N.A." || BindFridayTime(id) == "----" || BindFridayTime(id) == "-") && (BindSaturdayTime(id) != "N.A" || BindSaturdayTime(id) != "N.A." || BindSaturdayTime(id) == "----" || BindSaturdayTime(id) == "-"))
        if ((BindMondayTime(id) == "N.A" || BindMondayTime(id) == "N.A." || BindMondayTime(id) == "----" || BindMondayTime(id) == "-") && (BindTuesdayTime(id) == "N.A" || BindTuesdayTime(id) == "N.A." || BindTuesdayTime(id) == "----" || BindTuesdayTime(id) == "-") && (BindWednesdayTime(id) == "N.A" || BindWednesdayTime(id) == "N.A." || BindWednesdayTime(id) == "----" || BindWednesdayTime(id) == "-") && (BindThursdayTime(id) == "N.A" || BindThursdayTime(id) == "N.A." || BindThursdayTime(id) == "----" || BindThursdayTime(id) == "-") && (BindFridayTime(id) == "N.A" || BindFridayTime(id) == "N.A." || BindFridayTime(id) == "----" || BindFridayTime(id) == "-") && (BindSaturdayTime(id) == "N.A" || BindSaturdayTime(id) == "N.A." || BindSaturdayTime(id) == "----" || BindSaturdayTime(id) == "-"))
        {
            result = 0;
        }
        else
        {
            result = 1;
        }
        return result;
    }

}