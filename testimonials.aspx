﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="testimonials.aspx.cs" Inherits="testimonials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" src="js/jquery.marquee.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#marquee-vertical').marquee({ direction: 'vertical', delay: 0, timing: 40 });
        });
    </script>
      
    <script type="text/javascript" src="js/packery.pkgd.min.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.grid').packery({
                itemSelector: '.grid-item'
            });
            var $grid = $('.grid').imagesLoaded(function () {
                $grid.packery({});
            });
        });
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Testimonials</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="grid celebrity_testimonials">

                <asp:Repeater ID="rptCelebrity" runat="server">
                    <ItemTemplate>
                        <div class="grid-item testimonial_box">
                            <div class="img_box">
                                <img src='<%#Eval("Celebrity_Image","uploads/testimonial/{0}") %>'>
                            </div>
                            <div class="testim_name green_text"><%#Eval("Name") %> </div>
                            <div class="testim_content">
                                <%#Eval("Description") %>
                                <a style='<%#Eval("Testi_Image").ToString()==""?"display:none": "display:inline-block"%>' href='<%#Eval("Testi_Image","uploads/testimonial/{0}") %>' target="_blank">
                                    <img src='<%#Eval("Testi_Image","uploads/testimonial/{0}") %>'></a>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <%-- <div class="grid-item testimonial_box">
                    <div class="img_box">
                        <img src="images/testimonial/dalai.png">
                    </div>
                    <div class="testim_name green_text">His Holiness Dalai Lama</div>
                    <div class="testim_content">
                        <p>I would like to thank the Lilavati Hospital for the excellent service provided during the medical check up I have had. I want to thank all concerned. I pray that this Hospital will continue to provide medical benefit to the poor.</p>
                    </div>
                </div>

                <div class="grid-item testimonial_box">
                    <div class="img_box">
                        <img src="images/testimonial/AB.png">
                    </div>
                    <div class="testim_name green_text">Shri. Amitabh Bachchan</div>
                    <div class="testim_content">
                        <p>My dear Shri Narendra Trivediji, This is to put on record my sincere appreciation for the very efficient care rendered to me by your staff and doctors during my recent illness at the Lilavati Hospital.</p>
                        <p>You have a very dedicated and qualified team of nurses and doctors working in great unison. This has enabled exceptional care for patients and given them a huge level of comfort.</p>
                        <p>I would like to compliment the management and the organization at Lilavati for creating a wonderful atmosphere of medical proficiency and hope that you will continue to do the good work that you have set yourselves for the betterment of humanity.</p>
                        <p>Please convey my very personal thanks to the concerned people, specially in the operation theatre, the ICU and the staff managing the floor I was on. I would also like you to convey my sincere regards to the security that must have gone through a trying time during this period.</p>
                        <p>Warm regards once again and thank you for all the help and care.</p>

                    </div>
                </div>

                <div class="grid-item testimonial_box">
                    <div class="img_box">
                        <img src="images/testimonial/sachin.png">
                    </div>
                    <div class="testim_name green_text">Shri. Sachin Tendulkar</div>
                    <div class="testim_content">
                        <a href="images/testimonial/t_sachin.jpg" target="_blank">
                            <img src="images/testimonial/t_sachin.jpg"></a>
                    </div>
                </div>

                <div class="grid-item testimonial_box">
                    <div class="img_box">
                        <img src="images/testimonial/thakre.png">
                    </div>
                    <div class="testim_name green_text">Late Shri Balasaheb Thackeray</div>
                    <div class="testim_content">
                        <a href="images/testimonial/t_balasaheb.jpg" target="_blank">
                            <img src="images/testimonial/t_balasaheb.jpg"></a>
                    </div>
                </div>

                <div class="grid-item testimonial_box">
                    <div class="img_box">
                        <img src="images/testimonial/dypatil.png">
                    </div>
                    <div class="testim_name green_text">Padmashree Dr. D. Y. Patil</div>
                    <div class="testim_content">
                        <p>
                            Clean, pleasant atmosphere, patient friendly staff
 I celebrated Diwali Festival here. I feel like home.
                        </p>
                        <p>The best hospital in Mumbai !!!</p>
                    </div>
                </div>

                <div class="grid-item testimonial_box">
                    <div class="img_box">
                        <img src="images/testimonial/birla.png">
                    </div>
                    <div class="testim_name green_text">Shri. Kumar Mangalam Birla</div>
                    <div class="testim_content">
                        <p>It was a pleasure and an educative experience for us to visit the Lilavati Hospital. I am sure, Lilavati Hospital can benchmark internationally with the best Hospitals in the world, which is something that as an Indian, one feels proud of.</p>
                        <p>The enthusiasm and dedication of the doctors and staff is palpable – that, I believe, will really make the difference. Congratulations to Vijaybhai for a commendable achievement.</p>
                    </div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="public_testimonials no_anim">

                <div id="marquee-vertical">

                    <asp:Repeater ID="rptCommonPeople" runat="server">
                        <ItemTemplate>
                            <div class="testim">
                                <%#Eval("DESCRIPTION") %>
                                <p><strong class="green_text"><%#Eval("NAME") %> </strong></p>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="testim">
                        <p>The doctors are excellent & the nurses are going beyond the call of the duty to care for the patient!</p>
                        <p><strong class="green_text">Norman Alva - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Well trained Nursing staff,  Quick emergency response from your &quot;Code Blue Team, Ability of handling any kind of serious cases by your doctors, especially in pedeatric ward- 10 B!'</p>
                        <p><strong class="green_text">Rudhrakshi Siddhaye - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Very very clean hospital! All front desk staff are very excellent !!!</p>
                        <p><strong class="green_text">Rati Limbuwalla - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Lilavati hospital is one of the most caretaking hospitals in Mumbai. Atmostphere is very care taking.</p>
                        <p><strong class="green_text">Laxman Rawat - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>We are happy to be served by you….we offer our gratitude and blessings in all huminity and wish everyone godspeed… Thank you!</p>
                        <p><strong class="green_text">Jatin Mehta - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent treatment &amp; service provided by the doctors &amp; technical staff, prompness in addressing the issues and concern of patients.</p>
                        <p><strong class="green_text">Tulasidas Edelli - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Doctors were very approachable and very caring. The staff was very good. The patient was comfortable with the good care by the nursing staff.</p>
                        <p><strong class="green_text">Ruksana Shaikh - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>It is very good hospital in terms of doctors treatment, the nurses are excellent and make efforts to make you feel better! Keep doing what you are doing !!</p>
                        <p><strong class="green_text">Anupa Janbandhu - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>The Nursing staff is truly exceptional and Kudos to your team for hiring such professionals who go beyond the call of duty!</p>
                        <p><strong class="green_text">Purnima Sharma - March 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Hospital is located in very convient place, easily approchabale. Doctors and staff quantity is very good. Overall very good experince!!!</p>
                        <p><strong class="green_text">Mr Basant Kumar Kullu - Feb 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Everything! The prompness of the staff is all department ws especially refreshing, Doctors were really nice.</p>
                        <p><strong class="green_text">Mr Shivam Saxena - Feb 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>We like the most about Lilavati Hospital is positive vibrations and peaceful surrounding which helps to improve health!!! Your management is very good. Keep it up!!!</p>
                        <p><strong class="green_text">Mr Dhiren Shah - Feb 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent staff &amp; technicians for delicately handling bed ridden patients!!!</p>
                        <p><strong class="green_text">Ms Sneha Shriyan - Feb 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>When you are admitted in Lilavati Hospital, you feel gurantee that you will get well soon. Everyone attends the patient dedicately, systamatically &amp; precisely. Perfect management skill!!!!</p>
                        <p><strong class="green_text">Sameer Satpalkar - Feb 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent medical and praramedical facilities &amp; staff. Nursing staff is very component. It was good to responce to the person advised diet need of patinet by dietitian…..</p>
                        <p><strong class="green_text">Hario Tiwari - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>All the sisters and staff &quot; All the best&quot;, God bless all of u, The Great sewa, Dr Hitesh Mehta, Dr Ajit Menon &amp; Team. Thank u for your kindness, unconditional love….</p>
                        <p><strong class="green_text">Bhavesh Gandhi - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Human care in addition to Health care of patient, clean enviornment and ambience, excellent sisters, Nurses staff, excellent doctors, good diagnostics.</p>
                        <p><strong class="green_text">Jayalaxmi Shankaran - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Polite and humble way of speaking of Securities, lift men, Nursing staff and other supportive staffs!!!</p>
                        <p><strong class="green_text">Rajesh Shelke - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Your slogan itself explains everything, 'More than Health care, Human care' it sums up everything!</p>
                        <p><strong class="green_text">Harishbhai Kopodi - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>1st time experienced PRO system and O liked it the way they took personal attention to see if the tests were sone &amp; the status of the test, Keep up the the good work and &quot;hats off' to your staff be it from small to big all are with smiles…</p>
                        <p><strong class="green_text">Rajaram Naik - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Cordial hospitality, great staff everywhere! Continue with the great work, all the best!</p>
                        <p><strong class="green_text">Aneeta Walawalkar - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Staff is havin human touch. Each & every staff is very cooperative, suportive. They have gone beyond call on the duty</p>
                        <p><strong class="green_text">Smita Gumaste - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Staff are knowledgable, courteous and helpful. The hospital was quite clean, and food was good. Keep up the Good work!</p>
                        <p><strong class="green_text">Sheila D char - Jan 2016</strong></p>
                    </div>

                    <div class="testim">
                        <p>Doctors are qualified &amp; focused, Nurses are well informed &amp; dedicated to their work, positive environment prevails everywhere here.</p>
                        <p><strong class="green_text">Perwaiz Waris - November 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The Hospital really &amp; trully lives up to its motto of Complete Human care &amp; not just Health Care. The extra ordinary attentiveness by the Sr Surgeon, Consultants, Doctors, &amp; Nursing staff was exceptionally good</p>
                        <p><strong class="green_text">Jayaraman Vishwanathan - November 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>As  you all say that a your hospital you all strive to provide not just healthcare but also &quot;Human Care&quot; . Request you to continue this ' Good work'</p>
                        <p><strong class="green_text">Shivani Agrawal - November 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Such a long stay but your absolutely brilliant Nurses and attendants of the 11th floor A wing, feel like a family now. Thank you ssso much my ANGELs`!!</p>
                        <p><strong class="green_text">Aarathi Chaudhari - November 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The well qualified well experienced doctors being down to earth and the cheerful service by Nursing staff</p>
                        <p><strong class="green_text">Srinivasan  Santhanagopalan - November 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Prompt service and the confidence of doctors In treating the patient in such a critical situation when other doctor denied to do so. Frankly no cause what we have got from this hospital I cant put it on words and I feel I am at a receiving end. Hats off to the entire team, Lilavati!!</p>
                        <p><strong class="green_text">Aadi Mehta - November 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>ICU is excellent service, knowledge of Nurses are above expectations. In ICU even doctors make you feel at home &amp; it is truly Human care</p>
                        <p><strong class="green_text">Radhika Kapadia - October 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Drs &amp; sisters are really excellent, as a patient I would not like to visit Lilavati bt as good friend, surely come again to meet all well wishers. I pray to GOD for their excellent life.</p>
                        <p><strong class="green_text">Samruddhi Lunwal - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The treatment is awesome. The staff in Lilavati hospital handle the patient with care &amp; gives respect. Due to this reason I like this hospital.</p>
                        <p><strong class="green_text">Glen Dabre - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>LH has clean &amp; well maintained rooms, excellent nursing staff, great housekeeping &amp; security, good canteen &amp; experienced doctors &amp; dietitian, Food quality &amp; variety is great.</p>
                        <p><strong class="green_text">Gaurav Pasricha - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Organized way of procedure, Clean &amp; positive ambience, correct guidance &amp; medication by the doctor, personal attention for telephonic reference to other doctor.</p>
                        <p><strong class="green_text">Priyanka Krishnan - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The very very excellent doctors, care &amp; attention (in terms of regular visit &amp; time spent) staff are good, nurse services are best &amp; most full it is guaranteed Hospital.</p>
                        <p><strong class="green_text">Kondibhau Dongre - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Discipline in restricting visits, &nbsp;calm &amp; quietness in Hospital, &nbsp;proper cleanliness. Caring for patient and efforts to maintain Hospital' Good image.</p>
                        <p><strong class="green_text">Devendra Vyas - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Reputed when talked about in friends &amp; relatives. Doctors are professional and upto to d mark. Following staff, nurses know there jobs &amp; are diligence.</p>
                        <p><strong class="green_text">Jagmohan Datta - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The Nurses are really the best. They are the true gems of the hospital, always smiling &amp; ready to help instantly.</p>
                        <p><strong class="green_text">Suresh Gundaria - May 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The care taken by the doctors &amp; the staff is excellent. They really treat patient with love &amp; affection. You are at your best!</p>
                        <p><strong class="green_text">Tejinder Singh Mankoo - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Its cleanliness, highly experience &amp; professional doctors, nurses &amp; staff. Amazing Hospital!!</p>
                        <p><strong class="green_text">Natasha Carvello - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>You are doing a great work truly justifies your tagline that you do not only provide healthcare, it is human care at its best! Everyone from all resident doctors to all nurses and attendants on duty on 10th floor for taking extraordinary care &amp; support throughout my stay for me n my baby.</p>
                        <p><strong class="green_text">Arpita Jana - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Good doctors, technicians, 2nd floor reception staff very considerate &amp; helpful. Staff are well behaved.</p>
                        <p><strong class="green_text">Vijaya Nair - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The staff! Lovely caring nurses, nursing aides, excellent cleaning staff, the room space, food services!</p>
                        <p><strong class="green_text">Bina Bajaj - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>It is a temple, not a Hospital. It is nice in all facility &amp; positive vibrations!</p>
                        <p><strong class="green_text">Jagdip Shah - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>You all are good, my best wishes to all of them! Just keep it making better &amp; better because &quot; Perfection is unachievable&quot;</p>
                        <p><strong class="green_text">Bharat Bhushan Kumar - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Doctors are excellent and friendliness. All staff are excellent &amp; friendly, Very Good experience.</p>
                        <p><strong class="green_text">Aarti Vaidya - April 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>The courteousness of the staff, an excellent facilitator for ONGC patient !!!</p>
                        <p><strong class="green_text">Sengupta - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>I love the professionalism and the good doctors. Keep the passion &amp; the good work !!!</p>
                        <p><strong class="green_text">Simrita Dhillon - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Respect &amp; responsibility toward patient. Doctors are very hardworking.!!!</p>
                        <p><strong class="green_text">Shrishti Jaiswal - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>God bless the good souls around here!!! For returning the smile on our faces.</p>
                        <p><strong class="green_text">Monali Ghag - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Doctors, nurses &amp; other staff are very friendly and approachable, disciplined in every single way. You people are serving in the best way, thank to all of you for my speedy recovery!</p>
                        <p><strong class="green_text">Ritu Banthena - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent medical expertise, excellent nursing services, superb dedication of all the doctors and staff., Keep it up!</p>
                        <p><strong class="green_text">Ganpath Subramaniam - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Lilavati Hospital is very good and clean, staff is well trained and knowledgeable, appreciate your help!, every dept is doing good job, keep it up!!! Thank u.</p>
                        <p><strong class="green_text">Rozy Margaret - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent doctors and staff service was very good . I made the whole experience as pleasant as possible. Nursing staff is very good!</p>
                        <p><strong class="green_text">Rakhee Sharma - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>After admission, promptness of doctors to know the problem of patient for providing the best treatment. Also the friendliness and supporting behavior of nurses which boost the confidence of the patient. Keep it up!</p>
                        <p><strong class="green_text">Manju Rai - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Courtesy, friendliness of staff &amp; professionalism of the doctors who attended me. Good experience, Keep it up!</p>
                        <p><strong class="green_text">Capt Sanjay Jhangiani - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>I would like to express gratitude towards the staff in Casualty, Mr Viraj for his patience &amp; promptness &amp; support.</p>
                        <p><strong class="green_text">Kishan Gopal Agrawal - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Quality of doctors, nurses,&amp; staff excellent courteous, polite &amp; very professional- Everything is excellent.</p>
                        <p><strong class="green_text">Kavita Joshi - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Great team of Doctors attended to me during my delivery with atmost care &amp; promptness. Very good service by all the staff, nurses &amp; housekeeping. Very friendly &amp; courteous.&nbsp;</p>
                        <p><strong class="green_text">Diya Jadhav - February 2015</strong></p>
                    </div>

                    <div class="testim">
                        <p>Keep serving us better and better every time we come to you, with new technologies and better treatment equipment!</p>
                        <p><strong class="green_text">Ms Shrawani Tawde -  November 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>It is one of the best Hospital, I have visited in my life. You can compare with the Hospital abroad. My complement to the management and excellent staff!</p>
                        <p><strong class="green_text">Ms Kusum Cardoza -  November 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Everything was excellent. It begins with your slogan "More than health care, Human care". This slogan sums up everything. I am unable to find any flows, it is so perfect and organized!</p>
                        <p><strong class="green_text">Mr Harishbhai Kapadi -  November 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Professionalism of the staff, The cleanliness of the hospital and your service made to feel like a home guest rather than just a patient</p>
                        <p><strong class="green_text">Mr Geoffery Menezes -  November 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Great experience ! Clean, pleasant atmosphere, patient friendly staff,  The Best hospital in Mumbai. I celebrate Diwali festival here. I feel like home here!</p>
                        <p><strong class="green_text">Dr D Y Patil -  November 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>It was wonderful &amp; homely feeling provided by all.Thanks!</p>
                        <p><strong class="green_text">Alok Vijay Manakar- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            The overall experience at the hospital was satisfactory. Dr Ajit Menon and Dr Vora have been great and gave us all the confidence we needed to go ahead with the surgery.  All services provided were to our entire satisfaction and we appreciate the care & attention given.
                        </p>
                        <p><strong class="green_text">Kamla Virwani- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>warmness in which the doctor and staff take care of patients Plz keep up the good work.</p>
                        <p><strong class="green_text">Ashok Patki- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Very professional at the same time, very courteous personnel. Excellent ambience, assurance and best medical treatment.</p>
                        <p><strong class="green_text">P K Balasubramanian- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent, helpful, courteous, knowledgeable staff &amp; doctors. &quot;Congrats&quot; keep up the good work &amp; friendly &amp; helpful approach. All worked great as 'TEAM LILAVATI' not as an Individual.</p>
                        <p><strong class="green_text">Benno Dsouza- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>The entire team of Dr Redkar & the Anesthetists , The caring and comforting sisters of 10th B wing.. I felt like it was my own home and the sisters were my own sisters. Such sisters will help to improve the reputation of Lilavati hospital and make it best!</p>
                        <p><strong class="green_text">Omkar Ravindran- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Courtesy and service of all hospital personnel at regular intervals. Everything about my stay was Fantabulous. Thing are just right as they are. Keep it up !!!</p>
                        <p><strong class="green_text">Lisa Gomes- Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>I love the fact that they are very attentive and react quick, if my mother is OK today, its because of you guys.. Thanks a Ton , you guys are doing a great job. Keep it up!</p>
                        <p><strong class="green_text">Prashant Dhondi - Oct 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>The friendly nature of everyone at the hospital actually make the patient recover faster. The food is really fresh and tasty.</p>
                        <p><strong class="green_text">Anil Palekar– September 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Well planned and organised care. Prompt explanation given by the doctors regarding the care helps to reduce the anxiety in the patient and relatives. Has a very good system of  administration without causing any inconvenience  to the patient. A special mention to the professionalism and dedicated care of Dr Ram Chaddha and his team.</p>
                        <p><strong class="green_text">Mary Ipe – August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>The love and care of the nursing staff is par excellence. Always ready to help and extremely professional. A variety of facilities are provided to the patient making them extremely comfortable. Keep up the good work of serving your patient. It is a very noble job. It is truly more than Health Care, Human care.</p>
                        <p><strong class="green_text">Nishika Choudhary – August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Consultants are very good, nursing staff are simply great and no where else have I experienced such good nursing care.</p>
                        <p><strong class="green_text">Saama Pandit – August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent System, Infrastructure, Security, Doctors &amp; Nursing staff. All nurses attending the patient were excellent and has performed their duties genuinely, professionally and sweetly. They are the backbone of this hospital.</p>
                        <p><strong class="green_text">Suhas Joshi – September 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>The doctors are very helpful and supportive to the patients, hospital is very nice &amp; each and every thing is excellent.</p>
                        <p><strong class="green_text">Samina Shaikh – September 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Amazing coordination &amp; great team work ofvarious department; skillfull staff. Keep up the wonderful work so that thehospital grows from Strength to strength</p>
                        <p><strong class="green_text">Asha Pandit– August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>Doctors are very cordial, they explained everything in comprehensive way in layman's language. Nursing staff, housekeeping and Room service staff are well behaved prompt and caring . Keep it up!</p>
                        </p>
                        <p><strong class="green_text">Rajashree Karkare- August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>My sister is mentally handicapped, but the way EEG dept. and Radiology dept. handled her for the test was appreciable.</p>
                        </p>
                        <p><strong class="green_text">Smruti Zaveri- August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>I liked the medical service & care of the patient. Patient's all queries has been sorted out and also took care of the diet of the pt.</p>
                        </p>
                        <p><strong class="green_text">Sushma Nagargoje- August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>Lilavati Hospital is the one of the best world class Hospital approved by NABH. I liked the Doctor and Nursing staff &amp; all other Junior Staff who are well experienced, self-dedicated and carried out my Kidney transplant surgery successfully and made me fit &amp; fine in a short tenure of my stay.</p>
                        </p>
                        <p><strong class="green_text">Baby Thomas- August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>We had a very good experience, &nbsp;keep the good work. Hope we dont need to go to hospital again, but if so then I would like to come again here !</p>
                        </p>
                        <p><strong class="green_text">Sandra Irani- August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>My patient was critical, but when we admitted him in the Lilavati hospital, he got relief and got cured in very few days, Thanks</p>
                        </p>
                        <p><strong class="green_text">Jagdish Bhopi- August 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>I would like to place on record my appreciation and profound thanks to Dr.Samuel Mathew,Dr.Ajit Menon and Dr.Priyank Mody for the painless Angioplasty they performed on me on 9.7.14. They were extremely skillful in their field. I was very nervous when I was taken to the Cath Lab but the whole team was very encouraging and made me feel very comfortable .Midway through the procedure I remember asking Dr.Mathew how long it would take and he smilingly informed me that it was already done. Thank you Dr.Mathew,Dr.Menon Dr.Mody and all the staff for the special care given.My prayers will always be with you.</p>
                        </p>
                        <p><strong class="green_text">LORNA ALVA - July 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>The most about Lilavati Hospital which I like is the stuff who constantly look and take care of patient.</p>
                        </p>
                        <p><strong class="green_text">Srita Shirurkar - July 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>
                            <p>All Medical facilities are availaible under one roof, treatment by doctors are also very satisfactory along with patietns attendant facility given is very good.</p>
                        </p>
                        <p><strong class="green_text">Atul Ajit Kumar Shah  - July 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Staff & doctors are good in nature. They provide good treatment and behave like a family member.</p>
                        <p><strong class="green_text">Diya Sachin Khanvilkar - May 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Expert Doctors, Cleanliness & Right Suggestions</p>
                        <p><strong class="green_text">Sanjay Umbarkar - May 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Best doctors and approach towards the patient. Care & loving nature towards the patient and understanding the patients kin.</p>
                        <p><strong class="green_text">Namita Rozario - May 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Excellent treatment with lots of patience and care</p>
                        <p><strong class="green_text">Prachi Chandra - May 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>Hospitality, Friendliness & attitude towards patient needs by all the staff (Doctors, Nurses & other support staff)</p>
                        <p><strong class="green_text">Donald Pradhan - May 2014</strong></p>
                    </div>

                    <div class="testim">
                        <p>All process of Lilavati hospital are good. Timely decisions and processes help us to save our patient who was in critical condition.</p>
                        <p><strong class="green_text">Vinayak gopal Ayare – April 2013</strong></p>
                    </div>

                    <div class="testim">
                        <p>The atmosphere of Lilavati hospital itself makes the patient comfortable; the serenity of the surrounding is outstanding.</p>
                        <p><strong class="green_text">Alice Mathew – April 2013</strong></p>
                    </div>

                    <div class="testim">
                        <p>The treatment I received from the hospital was excellent. No discrimination is observed between a rich and poor. I thank Lilavati Trustees, doctors and staff of the hospital.</p>
                        <p><strong class="green_text">Vasant Mahadu Kirpan – April 2013</strong></p>
                    </div>

                    <div class="testim">
                        <p>The entire support staff as well as the medical staff is extremely supportive, courteous and clarifies details. The nurses went out of the way to attend in the middle of the night.</p>
                        <p><strong class="green_text">Rashmi Pinto – May 2013</strong></p>
                    </div>--%>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>


        <div class="clear"></div>
    </div>

</asp:Content>

