﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="mission_motto.aspx.cs" Inherits="mission_motto" %>

<%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Mission & Motto</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>
                <%--<h1 class="hd green_text uppercase">Our Mission</h1>

                <p class="uppercase" style="border-left: 2px solid #ccc; margin-left: 30px; padding-left: 20px;">“To provide affordable healthcare of International Standard with human care.”</p>

                <h1 class="hd green_text uppercase">Our Motto</h1>

                <p class="uppercase" style="border-left: 2px solid #ccc; margin-left: 30px; padding-left: 20px;">“More than healthcare, human care”</p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

