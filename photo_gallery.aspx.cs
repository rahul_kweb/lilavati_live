﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class photo_gallery : System.Web.UI.Page
{
    PhotoGallery tblPhotoGallery = new PhotoGallery();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetPhotoGallery();

        if (dt.Rows.Count > 0)
        {            
            rptGallery.DataSource = dt;
            rptGallery.DataBind();            
        }
        else
        {
            rptGallery.DataSource = null;
            rptGallery.DataBind();
        }
    }
}