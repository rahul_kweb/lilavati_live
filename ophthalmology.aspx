﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="ophthalmology.aspx.cs" Inherits="ophthalmology" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Opthalmology</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Opthalmology.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%--<div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>The department of Ophthalmology is dedicated to provide high quality and cost-efficient eye care with a comprehensive range of examination, treatment and surgical options in all aspects of Adult and Pediatric ophthalmology. We have a team of highly experienced surgeons which are supported by most advanced medical equipment and techniques.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Cataract</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Phacoemulsification & foldable lens implantation surgery with no stiches.</li>
                                        <li>Refractive cataract surgery</li>
                                        <li>Small incision cataract surgery</li>
                                        <li>Pediatric cataract surgery</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Squint</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Occuloplasty</li>
                                        <li>Complete evaluation of eye deformities</li>
                                        <li>Ptosis Surgery</li>
                                        <li>Occuloplasty corrective surgery</li>
                                        <li>Reconstructive orbital surgery</li>
                                        <li>Orbitotomy for orbital tumor</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Glaucoma</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Glaucoma surgery</li>
                                        <li>Pediatric Glaucoma surgery</li>
                                        <li>Laser Glaucoma treatment (trabeculoplastyiridotomy & cyclophotocoagulation)</li>
                                        <li>Retina And Vitreous</li>
                                        <li>Specialty diagnostics and tests (retina imaging)</li>
                                        <li>Diabetic retinopathy management</li>
                                        <li>Age related macular degeneration management.</li>
                                        <li>Retinal detachment surgery</li>
                                        <li>Retinal laser photocoagulation</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Cornea</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Complete corneal diagnostics</li>
                                        <li>Corneal ulcer treatment</li>
                                        <li>Dry eye management</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Neurophthalmology</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Evaluation of neurological disorders of eye.</li>
                                        <li>Electrophysiology</li>
                                        <li>Optic nerve disease evaluation and management</li>
                                        <li>Ocular motility disorders & nerve paralysis</li>
                                        <li>Diplopia management</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Paediatric Ophthalmology</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Retinopathy of prematurity screening & treatment.</li>
                                        <li>Developmental cataract</li>
                                        <li>Developmental glaucoma</li>
                                        <li>Childhood blindness</li>
                                        <li>Congenital and hereditary disorders</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Other Services offered</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Comprehensive Eye Check up</li>
                                        <li>Cataract surgery</li>
                                        <li>Glaucoma management</li>
                                        <li>Retina Services which includes:
            <ul>
                <li>Vitreoretinal Surgeries (Buckle and Complex Vitrectomy)</li>
                <li>Diabetic Retinopathy Laser and Surgery</li>
                <li>Age Related Macular Degeneration (Lucentis/Avastin Inj and PDT)</li>
                <li>Medical Retina Lasers</li>
                <li>Retinopathy of Prematurity Management</li>
                <li>Contact Lens Clinic</li>
                <li>Low Vision Aids</li>
                <li>Eye care for children</li>
            </ul>
                                        </li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">

                            <ul class="bullet">
                                <li>Varient Digital Visual Charts</li>
                                <li>Autorefractometer</li>
                                <li>AutoLensometer</li>
                                <li>Humphrey's Visual Field Perimeter</li>
                                <li>Zeiss Anterior Segment Digital Imaging System Pictures of instrument</li>
                                <li>Zeiss Digital Fundus Camera</li>
                                <li>Zeiss Retinal Green laser</li>
                                <li>Zeiss Yag Laser</li>
                                <li>Zeiss Optical Coherence Tomography (Cirrus)</li>
                                <li>A - B Hi - Scan</li>
                                <li>Corneal Pachymetry</li>
                                <li>Zeiss Operating Microscope</li>
                                <li>High Speed Vitrectomy and Cold Phacoemulsification System</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                     <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

