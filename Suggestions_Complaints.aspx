﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="Suggestions_Complaints.aspx.cs" Inherits="Suggestions_Complaints" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Suggestions / Complaints</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>
                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

               
                    <p>For suggestions write to us on <a href="mailto:suggestions@lilavatihospital.com"> suggestions@lilavatihospital.com</a></p>
                <p>For complaints write to us on <a href="mailto:complaints@lilavatihospital.com"> complaints@lilavatihospital.com</a></p>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

