﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class search_results : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        SearchElement();
    }

    public void SearchElement()
    {
        string value = Page.RouteData.Values["key"].ToString();

        lblTittle.Text = value;

        value = value.Replace("Dr.", "").Trim();
       

        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_CmsMaster 'GET_SEARCH_CMS',0,'','','','" + value + "'");

        if (dt.Rows.Count > 0)
        {
            rptSearch.DataSource = dt;
            rptSearch.DataBind();

            foreach (RepeaterItem dl in rptSearch.Items)
            {
                string Desc = utility.ExtractHtmlInnerText(((Literal)dl.FindControl("litDesc")).Text);
                ((Literal)dl.FindControl("litDesc")).Text = "<p>" + Desc + "</p>";                              
            }
        }
        else
        {
            pError.Visible = true;
            rptSearch.DataSource = null;
            rptSearch.DataBind();
        }
    }
}