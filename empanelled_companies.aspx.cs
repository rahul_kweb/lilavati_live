﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class empanelled_companies : System.Web.UI.Page
{
    EmpanelledCompanies tblEmpanelledCompanies = new EmpanelledCompanies();
    EmpanelledDetails tblEmpanelledDetails = new EmpanelledDetails();
    CMSBAL cmsbal = new CMSBAL();
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindListOfEmpCorp();
            BindRepeater();
            BindBanner();
        }
    }

    public void BindListOfEmpCorp()
    {
        tblEmpanelledDetails.Tab_Id = 1;
        DataTable dt = new DataTable();
        dt = cmsbal.EmpanelledDetailsTabNameWiseCompany(tblEmpanelledDetails);

        if (dt.Rows.Count > 0)
        {
            rptListOFEmp.DataSource = dt;
            rptListOFEmp.DataBind();
        }
    }

    public void BindRepeater()
    {
        try
        {
            tblcmsInnerPageMaster.PageName = "Empanelled Corporates";
            DataTable dt = new DataTable();
            dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

            if (dt.Rows.Count > 0)
            {
                rptContent.DataSource = dt;
                rptContent.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(48);
    }
}