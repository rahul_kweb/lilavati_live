﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="pharmacy.aspx.cs" Inherits="pharmacy" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Pharmacy & Chemist</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Chemist.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>Chemist</h2>

                        <div class="tab_content">
                            <p>We have a chemist located at the ground floor which is open round the clock on all days and is available to outpatients. Medicines are dispensed only on doctor’s prescription except for the over the counter medicines. Our quality control process ensures that there is no product in stock post its expiry period. We ensure that our chemist is staffed only with registered pharmacists.</p>

                            <p>Contact: 91-22-2675 1578 / 1579</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Pharmacy</h2>

                        <div class="tab_content">
                            <p>Medicines and surgical items are made directly available through the our Pharmacy to the admitted patients. It is conveniently located in the basement.</p>

                            <p>The pharmacy is well stacked with major and rare drugs as well as surgical materials. Our quality control process ensures that there is no product in stock post expiry period. The stocking of drugs is managed efficiently to ensure there is no shortage or unavailability of any drug anytime. Since we house most of the major drugs necessary in the medical treatment, the patients and their relatives have easy accessibility to the required drugs any time.</p>

                            <p>We ensure that the pharmacy is staffed with only registered pharmacists.</p>

                            <p>Contact: 91-22-26751571</p>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->


                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

