﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_feedback_OutPatient : System.Web.UI.UserControl
{
    FeedbackOutPatient tblFeedbackOutPatient = new FeedbackOutPatient();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmitOut_Click(object sender, EventArgs e)
    {
        tblFeedbackOutPatient.FirstName = txtFirstName.Text.Trim();
        tblFeedbackOutPatient.LastName = txtLastName.Text.Trim();
        tblFeedbackOutPatient.Lh_No = txtLHNo.Text.Trim();
        tblFeedbackOutPatient.NameOfDoctor = txtNameOfDoctor.Text.Trim();
        tblFeedbackOutPatient.PhoneNo = txtContactNo.Text.Trim();
        tblFeedbackOutPatient.EmailId = txtEmailID.Text.Trim();
        tblFeedbackOutPatient.Date = txtDate11.Text.Trim();

        tblFeedbackOutPatient.Service = txtService.Text.Trim();
        tblFeedbackOutPatient.ServiceReason = txtServiceReason.Text.Trim();
        tblFeedbackOutPatient.Staff = txtStaff.Text.Trim();
        tblFeedbackOutPatient.AreaofWork = txtAreaofWork.Text.Trim();
        tblFeedbackOutPatient.AreaReason = txtAreaReason.Text.Trim();

        string chk = string.Empty;
        for (int i = 0; i < chkHowDidYouComeToKnow.Items.Count; i++)
        {
            if (chkHowDidYouComeToKnow.Items[i].Selected == true)
            {
                chk += chkHowDidYouComeToKnow.Items[i].Value + ",";
            }
        }

        if (chk != "")
        {
            chk = chk.Substring(0, chk.Length - 1);
        }
        tblFeedbackOutPatient.HowDidYouComeToKnow = chk;

        /** Time taken by appointment staff to give an appointment for Doctor/ service.  **/

        if (rdbTimeTakenByAppointmentExcellent.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenByAppointment = "Excellent";
        }
        if (rdbTimeTakenByAppointmentGood.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenByAppointment = "Good";
        }
        if (rdbTimeTakenByAppointmentFair.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenByAppointment = "Fair";
        }
        if (rdbTimeTakenByAppointmentAverage.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenByAppointment = "Average";
        }
        if (rdbTimeTakenByAppointmentPoor.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenByAppointment = "Poor";
        }

        /** Guidance and information provided by the staff.  **/

        if (rdbGuidanceAndInformationExcellent.Checked == true)
        {
            tblFeedbackOutPatient.GuidanceAndInformation = "Excellent";
        }
        if (rdbGuidanceAndInformationGood.Checked == true)
        {
            tblFeedbackOutPatient.GuidanceAndInformation = "Good";
        }
        if (rdbGuidanceAndInformationFair.Checked == true)
        {
            tblFeedbackOutPatient.GuidanceAndInformation = "Fair";
        }
        if (rdbGuidanceAndInformationAverage.Checked == true)
        {
            tblFeedbackOutPatient.GuidanceAndInformation = "Average";
        }
        if (rdbGuidanceAndInformationPoor.Checked == true)
        {
            tblFeedbackOutPatient.GuidanceAndInformation = "Poor";
        }

        /** Courtesy and friendliness of OPD staff.  **/

        if (rdbCourtesyAndFriendlinessOPDExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessOPD = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessOPDGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessOPD = "Good";
        }
        if (rdbCourtesyAndFriendlinessOPDFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessOPD = "Fair";
        }
        if (rdbCourtesyAndFriendlinessOPDAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessOPD = "Average";
        }
        if (rdbCourtesyAndFriendlinessOPDPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessOPD = "Poor";
        }


        /** Time taken to complete the registration process.  **/

        if (rdbTimeTakenToCompleteRegistrationExcellent.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteRegistration = "Excellent";
        }
        if (rdbTimeTakenToCompleteRegistrationGood.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteRegistration = "Good";
        }
        if (rdbTimeTakenToCompleteRegistrationFair.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteRegistration = "Fair";
        }
        if (rdbTimeTakenToCompleteRegistrationAverage.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteRegistration = "Average";
        }
        if (rdbTimeTakenToCompleteRegistrationPoor.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteRegistration = "Poor";
        }

        /** Time taken to complete the billing process.  **/

        if (rdbTimeTakenToCompleteBillingExcellent.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteBilling = "Excellent";
        }
        if (rdbTimeTakenToCompleteBillingGood.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteBilling = "Good";
        }
        if (rdbTimeTakenToCompleteBillingFair.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteBilling = "Fair";
        }
        if (rdbTimeTakenToCompleteBillingAverage.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteBilling = "Average";
        }
        if (rdbTimeTakenToCompleteBillingPoor.Checked == true)
        {
            tblFeedbackOutPatient.TimeTakenToCompleteBilling = "Poor";
        }

        /** Courtesy and information provided by OPD staff.  **/

        if (rdbCourtesyAndInformationExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndInformation = "Excellent";
        }
        if (rdbCourtesyAndInformationGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndInformation = "Good";
        }
        if (rdbCourtesyAndInformationFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndInformation = "Fair";
        }
        if (rdbCourtesyAndInformationAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndInformation = "Average";
        }
        if (rdbCourtesyAndInformationPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndInformation = "Poor";
        }

        /** Waiting time to see the doctor after completing registration/ billing formalities.  **/

        if (rdbWaitingTimeToSeeDoctorExcellent.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeToSeeDoctor = "Excellent";
        }
        if (rdbWaitingTimeToSeeDoctorGood.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeToSeeDoctor = "Good";
        }
        if (rdbWaitingTimeToSeeDoctorFair.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeToSeeDoctor = "Fair";
        }
        if (rdbWaitingTimeToSeeDoctorAverage.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeToSeeDoctor = "Average";
        }
        if (rdbWaitingTimeToSeeDoctorPoor.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeToSeeDoctor = "Poor";
        }

        /** Clarity & information shared by the doctor about your health & line of treatment.  **/

        if (rdbClarityAndInformationExcellent.Checked == true)
        {
            tblFeedbackOutPatient.ClarityAndInformation = "Excellent";
        }
        if (rdbClarityAndInformationGood.Checked == true)
        {
            tblFeedbackOutPatient.ClarityAndInformation = "Good";
        }
        if (rdbClarityAndInformationFair.Checked == true)
        {
            tblFeedbackOutPatient.ClarityAndInformation = "Fair";
        }
        if (rdbClarityAndInformationAverage.Checked == true)
        {
            tblFeedbackOutPatient.ClarityAndInformation = "Average";
        }
        if (rdbClarityAndInformationPoor.Checked == true)
        {
            tblFeedbackOutPatient.ClarityAndInformation = "Poor";
        }

        /** Care & attention given by the doctor.  **/

        if (rdbCareAndAttentionExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CareAndAttention = "Excellent";
        }
        if (rdbCareAndAttentionGood.Checked == true)
        {
            tblFeedbackOutPatient.CareAndAttention = "Good";
        }
        if (rdbCareAndAttentionFair.Checked == true)
        {
            tblFeedbackOutPatient.CareAndAttention = "Fair";
        }
        if (rdbCareAndAttentionAverage.Checked == true)
        {
            tblFeedbackOutPatient.CareAndAttention = "Average";
        }
        if (rdbCareAndAttentionPoor.Checked == true)
        {
            tblFeedbackOutPatient.CareAndAttention = "Poor";
        }

        /** Behavior & attitude of nurse.  **/

        if (rdbBehaviorAndAttitudeExcellent.Checked == true)
        {
            tblFeedbackOutPatient.BehaviorAndAttitude = "Excellent";
        }
        if (rdbBehaviorAndAttitudeGood.Checked == true)
        {
            tblFeedbackOutPatient.BehaviorAndAttitude = "Good";
        }
        if (rdbBehaviorAndAttitudeFair.Checked == true)
        {
            tblFeedbackOutPatient.BehaviorAndAttitude = "Fair";
        }
        if (rdbBehaviorAndAttitudeAverage.Checked == true)
        {
            tblFeedbackOutPatient.BehaviorAndAttitude = "Average";
        }
        if (rdbBehaviorAndAttitudePoor.Checked == true)
        {
            tblFeedbackOutPatient.BehaviorAndAttitude = "Poor";
        }

        /** Respect for your privacy.  **/

        if (rdbRespectForYourPrivacyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.RespectForYourPrivacy = "Excellent";
        }
        if (rdbRespectForYourPrivacyGood.Checked == true)
        {
            tblFeedbackOutPatient.RespectForYourPrivacy = "Good";
        }
        if (rdbRespectForYourPrivacyFair.Checked == true)
        {
            tblFeedbackOutPatient.RespectForYourPrivacy = "Fair";
        }
        if (rdbRespectForYourPrivacyAverage.Checked == true)
        {
            tblFeedbackOutPatient.RespectForYourPrivacy = "Average";
        }
        if (rdbRespectForYourPrivacyPoor.Checked == true)
        {
            tblFeedbackOutPatient.RespectForYourPrivacy = "Poor";
        }

        /** Care offered during procedure/ Investigation by nurse.  **/

        if (rdbCareOfferedDuringProcExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CareOfferedDuringProc = "Excellent";
        }
        if (rdbCareOfferedDuringProcGood.Checked == true)
        {
            tblFeedbackOutPatient.CareOfferedDuringProc = "Good";
        }
        if (rdbCareOfferedDuringProcFair.Checked == true)
        {
            tblFeedbackOutPatient.CareOfferedDuringProc = "Fair";
        }
        if (rdbCareOfferedDuringProcAverage.Checked == true)
        {
            tblFeedbackOutPatient.CareOfferedDuringProc = "Average";
        }
        if (rdbCareOfferedDuringProcPoor.Checked == true)
        {
            tblFeedbackOutPatient.CareOfferedDuringProc = "Poor";
        }

        /** Waiting time for the test/ Investigation after completing registration & billing formalities.  **/

        //if (rdbWaitingTimeForTheTestExcellent.Checked == true)
        //{
        //    tblFeedbackOutPatient.WaitingTimeForTheTest = "Excellent";
        //}
        //if (rdbWaitingTimeForTheTestGood.Checked == true)
        //{
        //    tblFeedbackOutPatient.WaitingTimeForTheTest = "Good";
        //}
        //if (rdbWaitingTimeForTheTestFair.Checked == true)
        //{
        //    tblFeedbackOutPatient.WaitingTimeForTheTest = "Fair";
        //}
        //if (rdbWaitingTimeForTheTestAverage.Checked == true)
        //{
        //    tblFeedbackOutPatient.WaitingTimeForTheTest = "Average";
        //}
        //if (rdbWaitingTimeForTheTestPoor.Checked == true)
        //{
        //    tblFeedbackOutPatient.WaitingTimeForTheTest = "Poor";
        //}

        /** Experience with technical staff (Doctor, Technician) during test & Investigation (in terms of guidance, behavior & coutesy)  **/

        //if (rdbExperienceWithTechnicalExcellent.Checked == true)
        //{
        //    tblFeedbackOutPatient.ExperienceWithTechnical = "Excellent";
        //}
        //if (rdbExperienceWithTechnicalGood.Checked == true)
        //{
        //    tblFeedbackOutPatient.ExperienceWithTechnical = "Good";
        //}
        //if (rdbExperienceWithTechnicalFair.Checked == true)
        //{
        //    tblFeedbackOutPatient.ExperienceWithTechnical = "Fair";
        //}
        //if (rdbExperienceWithTechnicalAverage.Checked == true)
        //{
        //    tblFeedbackOutPatient.ExperienceWithTechnical = "Average";
        //}
        //if (rdbExperienceWithTechnicalPoor.Checked == true)
        //{
        //    tblFeedbackOutPatient.ExperienceWithTechnical = "Poor";
        //}

        /** Courtesy and friendliness of the staff  **/

        //if (rdbCourtesyAndFriendlinessStaffExcellent.Checked == true)
        //{
        //    tblFeedbackOutPatient.CourtesyAndFriendlinessStaff = "Excellent";
        //}
        //if (rdbCourtesyAndFriendlinessStaffGood.Checked == true)
        //{
        //    tblFeedbackOutPatient.CourtesyAndFriendlinessStaff = "Good";
        //}
        //if (rdbCourtesyAndFriendlinessStaffFair.Checked == true)
        //{
        //    tblFeedbackOutPatient.CourtesyAndFriendlinessStaff = "Fair";
        //}
        //if (rdbCourtesyAndFriendlinessStaffAverage.Checked == true)
        //{
        //    tblFeedbackOutPatient.CourtesyAndFriendlinessStaff = "Average";
        //}
        //if (rdbCourtesyAndFriendlinessStaffPoor.Checked == true)
        //{
        //    tblFeedbackOutPatient.CourtesyAndFriendlinessStaff = "Poor";
        //}

        /** Courtesy and friendliness of the staff Pathology **/

        if (rdbCourtesyAndFriendlinessStaffPathologyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessStaffPathologyGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology = "Good";
        }
        if (rdbCourtesyAndFriendlinessStaffPathologyFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology = "Fair";
        }
        if (rdbCourtesyAndFriendlinessStaffPathologyAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology = "Average";
        }
        if (rdbCourtesyAndFriendlinessStaffPathologyPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology = "Poor";
        }

        /** Courtesy and friendliness of the staff Radiology **/

        if (rdbCourtesyAndFriendlinessStaffRadiologyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessStaffRadiologyGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology = "Good";
        }
        if (rdbCourtesyAndFriendlinessStaffRadiologyFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology = "Fair";
        }
        if (rdbCourtesyAndFriendlinessStaffRadiologyAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology = "Average";
        }
        if (rdbCourtesyAndFriendlinessStaffRadiologyPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology = "Poor";
        }


        /** Courtesy and friendliness of the staff Cardiology **/

        if (rdbCourtesyAndFriendlinessStaffCardiologyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessStaffCardiologyGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology = "Good";
        }
        if (rdbCourtesyAndFriendlinessStaffCardiologyFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology = "Fair";
        }
        if (rdbCourtesyAndFriendlinessStaffCardiologyAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology = "Average";
        }
        if (rdbCourtesyAndFriendlinessStaffCardiologyPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology = "Poor";
        }

        /** Courtesy and friendliness of the staff Nuclear Dept. **/

        if (rdbCourtesyAndFriendlinessStaffNuclearExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessStaffNuclearGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear = "Good";
        }
        if (rdbCourtesyAndFriendlinessStaffNuclearFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear = "Fair";
        }
        if (rdbCourtesyAndFriendlinessStaffNuclearAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear = "Average";
        }
        if (rdbCourtesyAndFriendlinessStaffNuclearPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear = "Poor";
        }


        /** Courtesy and friendliness of the staff Other **/

        if (rdbCourtesyAndFriendlinessStaffOtherExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessStaffOtherGood.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther = "Good";
        }
        if (rdbCourtesyAndFriendlinessStaffOtherFair.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther = "Fair";
        }
        if (rdbCourtesyAndFriendlinessStaffOtherAverage.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther = "Average";
        }
        if (rdbCourtesyAndFriendlinessStaffOtherPoor.Checked == true)
        {
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther = "Poor";
        }

        tblFeedbackOutPatient.Courtesyother = txtCourtesyAndFriendlinessStafothers.Text.Trim();

        /** Waiting time for the test/ Investigation after completing registration & billing formalities Pathology.  **/

        if (rdbWaitingTimeForTheTestPathologyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestPathology = "Excellent";
        }
        if (rdbWaitingTimeForTheTestPathologyGood.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestPathology = "Good";
        }
        if (rdbWaitingTimeForTheTestPathologyFair.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestPathology = "Fair";
        }
        if (rdbWaitingTimeForTheTestPathologyAverage.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestPathology = "Average";
        }
        if (rdbWaitingTimeForTheTestPathologyPoor.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestPathology = "Poor";
        }

        /** Waiting time for the test/ Investigation after completing registration & billing formalities Radiology.  **/

        if (rdbWaitingTimeForTheTestRadiologyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestRadiology = "Excellent";
        }
        if (rdbWaitingTimeForTheTestRadiologyGood.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestRadiology = "Good";
        }
        if (rdbWaitingTimeForTheTestRadiologyFair.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestRadiology = "Fair";
        }
        if (rdbWaitingTimeForTheTestRadiologyAverage.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestRadiology = "Average";
        }
        if (rdbWaitingTimeForTheTestRadiologyPoor.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestRadiology = "Poor";
        }


        /** Waiting time for the test/ Investigation after completing registration & billing formalities Radiology.Cardiology  **/

        if (rdbWaitingTimeForTheTestCardiologyExcellent.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestCardiology = "Excellent";
        }
        if (rdbWaitingTimeForTheTestCardiologyGood.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestCardiology = "Good";
        }
        if (rdbWaitingTimeForTheTestCardiologyFair.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestCardiology = "Fair";
        }
        if (rdbWaitingTimeForTheTestCardiologyAverage.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestCardiology = "Average";
        }
        if (rdbWaitingTimeForTheTestCardiologyPoor.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestCardiology = "Poor";
        }

        /** Waiting time for the test/ Investigation after completing registration & billing formalities Radiology. Nuclear **/

        if (rdbWaitingTimeForTheTestNuclearExcellent.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestNuclear = "Excellent";
        }
        if (rdbWaitingTimeForTheTestNuclearGood.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestNuclear = "Good";
        }
        if (rdbWaitingTimeForTheTestNuclearFair.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestNuclear = "Fair";
        }
        if (rdbWaitingTimeForTheTestNuclearAverage.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestNuclear = "Average";
        }
        if (rdbWaitingTimeForTheTestNuclearPoor.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestNuclear = "Poor";
        }

        /** Waiting time for the test/ Investigation after completing registration & billing formalities Radiology. other **/

        if (rdbWaitingTimeForTheTestOtherExcellent.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestOther = "Excellent";
        }
        if (rdbWaitingTimeForTheTestOtherGood.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestOther = "Good";
        }
        if (rdbWaitingTimeForTheTestOtherFair.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestOther = "Fair";
        }
        if (rdbWaitingTimeForTheTestOtherAverage.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestOther = "Average";
        }
        if (rdbWaitingTimeForTheTestOtherPoor.Checked == true)
        {
            tblFeedbackOutPatient.WaitingTimeForTheTestOther = "Poor";
        }

        tblFeedbackOutPatient.WaitingTimeother = txtrdbWaitingTimeForTheTestOthers.Text.Trim();


        /** Comfort of waiting area.  **/

        if (rdbComfortOfWaitingAreaExcellent.Checked == true)
        {
            tblFeedbackOutPatient.ComfortOfWaitingArea = "Excellent";
        }
        if (rdbComfortOfWaitingAreaGood.Checked == true)
        {
            tblFeedbackOutPatient.ComfortOfWaitingArea = "Good";
        }
        if (rdbComfortOfWaitingAreaFair.Checked == true)
        {
            tblFeedbackOutPatient.ComfortOfWaitingArea = "Fair";
        }
        if (rdbComfortOfWaitingAreaAverage.Checked == true)
        {
            tblFeedbackOutPatient.ComfortOfWaitingArea = "Average";
        }
        if (rdbComfortOfWaitingAreaPoor.Checked == true)
        {
            tblFeedbackOutPatient.ComfortOfWaitingArea = "Poor";
        }

        /** Cleanliness in the OPD area.  **/

        if (rdbCleanlinessInOPDExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessInOPD = "Excellent";
        }
        if (rdbCleanlinessInOPDGood.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessInOPD = "Good";
        }
        if (rdbCleanlinessInOPDFair.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessInOPD = "Fair";
        }
        if (rdbCleanlinessInOPDAverage.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessInOPD = "Average";
        }
        if (rdbCleanlinessInOPDPoor.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessInOPD = "Poor";
        }

        /** Cleanliness of toilets.  **/

        if (rdbCleanlinessOfToiletsExcellent.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessOfToilets = "Excellent";
        }
        if (rdbCleanlinessOfToiletsGood.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessOfToilets = "Good";
        }
        if (rdbCleanlinessOfToiletsFair.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessOfToilets = "Fair";
        }
        if (rdbCleanlinessOfToiletsAverage.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessOfToilets = "Average";
        }
        if (rdbCleanlinessOfToiletsPoor.Checked == true)
        {
            tblFeedbackOutPatient.CleanlinessOfToilets = "Poor";
        }


        /** How would you rate your overall experience at Lilavati Hospital.  **/

        if (rdbHowWouldYouRateExcellent.Checked == true)
        {
            tblFeedbackOutPatient.HowWouldYouRate = "Excellent";
        }
        if (rdbHowWouldYouRateGood.Checked == true)
        {
            tblFeedbackOutPatient.HowWouldYouRate = "Good";
        }
        if (rdbHowWouldYouRateFair.Checked == true)
        {
            tblFeedbackOutPatient.HowWouldYouRate = "Fair";
        }
        if (rdbHowWouldYouRateAverage.Checked == true)
        {
            tblFeedbackOutPatient.HowWouldYouRate = "Average";
        }
        if (rdbHowWouldYouRatePoor.Checked == true)
        {
            tblFeedbackOutPatient.HowWouldYouRate = "Poor";
        }


        tblFeedbackOutPatient.WhatDidYouLIke = txtWhatDidYouLIke.Text.Trim();
        tblFeedbackOutPatient.WhatDidYouLikeLeast = txtWhatDidYouLikeLeast.Text.Trim();
        tblFeedbackOutPatient.AnysuggestionYouWouldLike = txtAnysuggestionYouWouldLike.Text.Trim();

        cmsbal.SaveFeedbackOutPatient(tblFeedbackOutPatient);
        if (HttpContext.Current.Session["SaveFeedbackOutPatient"] == "Success")
        {
            Reset();
            //Response.Write("<script>alert('Thanks for your Valuable Feedback.') </script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopupOut();", true);
        }
        else
        {
            Response.Write("<script>alert('Failed') </script>");
        }

    }


    public void Reset()
    {
        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtLHNo.Text = string.Empty;
        txtNameOfDoctor.Text = string.Empty;
        txtContactNo.Text = string.Empty;
        txtEmailID.Text = string.Empty;
        txtDate11.Text = string.Empty;

        txtService.Text = string.Empty;
        txtServiceReason.Text = string.Empty;
        txtStaff.Text = string.Empty;
        txtAreaofWork.Text = string.Empty;
        txtAreaReason.Text = string.Empty;

        chkHowDidYouComeToKnow.SelectedIndex = -1;

        rdbTimeTakenByAppointmentExcellent.Checked = false;
        rdbTimeTakenByAppointmentGood.Checked = false;
        rdbTimeTakenByAppointmentFair.Checked = false;
        rdbTimeTakenByAppointmentAverage.Checked = false;
        rdbTimeTakenByAppointmentPoor.Checked = false;

        rdbGuidanceAndInformationExcellent.Checked = false;
        rdbGuidanceAndInformationGood.Checked = false;
        rdbGuidanceAndInformationFair.Checked = false;
        rdbGuidanceAndInformationAverage.Checked = false;
        rdbGuidanceAndInformationPoor.Checked = false;

        rdbCourtesyAndFriendlinessOPDExcellent.Checked = false;
        rdbCourtesyAndFriendlinessOPDGood.Checked = false;
        rdbCourtesyAndFriendlinessOPDFair.Checked = false;
        rdbCourtesyAndFriendlinessOPDAverage.Checked = false;
        rdbCourtesyAndFriendlinessOPDPoor.Checked = false;

        rdbTimeTakenToCompleteRegistrationExcellent.Checked = false;
        rdbTimeTakenToCompleteRegistrationGood.Checked = false;
        rdbTimeTakenToCompleteRegistrationFair.Checked = false;
        rdbTimeTakenToCompleteRegistrationAverage.Checked = false;
        rdbTimeTakenToCompleteRegistrationPoor.Checked = false;

        rdbTimeTakenToCompleteBillingExcellent.Checked = false;
        rdbTimeTakenToCompleteBillingGood.Checked = false;
        rdbTimeTakenToCompleteBillingFair.Checked = false;
        rdbTimeTakenToCompleteBillingAverage.Checked = false;
        rdbTimeTakenToCompleteBillingPoor.Checked = false;

        rdbCourtesyAndInformationExcellent.Checked = false;
        rdbCourtesyAndInformationGood.Checked = false;
        rdbCourtesyAndInformationFair.Checked = false;
        rdbCourtesyAndInformationAverage.Checked = false;
        rdbCourtesyAndInformationPoor.Checked = false;

        rdbWaitingTimeToSeeDoctorExcellent.Checked = false;
        rdbWaitingTimeToSeeDoctorGood.Checked = false;
        rdbWaitingTimeToSeeDoctorFair.Checked = false;
        rdbWaitingTimeToSeeDoctorAverage.Checked = false;
        rdbWaitingTimeToSeeDoctorPoor.Checked = false;

        rdbClarityAndInformationExcellent.Checked = false;
        rdbClarityAndInformationGood.Checked = false;
        rdbClarityAndInformationFair.Checked = false;
        rdbClarityAndInformationAverage.Checked = false;
        rdbClarityAndInformationPoor.Checked = false;

        rdbCareAndAttentionExcellent.Checked = false;
        rdbCareAndAttentionGood.Checked = false;
        rdbCareAndAttentionFair.Checked = false;
        rdbCareAndAttentionAverage.Checked = false;
        rdbCareAndAttentionPoor.Checked = false;

        rdbBehaviorAndAttitudeExcellent.Checked = false;
        rdbBehaviorAndAttitudeGood.Checked = false;
        rdbBehaviorAndAttitudeFair.Checked = false;
        rdbBehaviorAndAttitudeAverage.Checked = false;
        rdbBehaviorAndAttitudePoor.Checked = false;

        rdbRespectForYourPrivacyExcellent.Checked = false;
        rdbRespectForYourPrivacyGood.Checked = false;
        rdbRespectForYourPrivacyFair.Checked = false;
        rdbRespectForYourPrivacyAverage.Checked = false;
        rdbRespectForYourPrivacyPoor.Checked = false;

        rdbCareOfferedDuringProcExcellent.Checked = false;
        rdbCareOfferedDuringProcGood.Checked = false;
        rdbCareOfferedDuringProcFair.Checked = false;
        rdbCareOfferedDuringProcAverage.Checked = false;
        rdbCareOfferedDuringProcPoor.Checked = false;


        //rdbWaitingTimeForTheTestExcellent.Checked = false;
        //rdbWaitingTimeForTheTestGood.Checked = false;
        //rdbWaitingTimeForTheTestFair.Checked = false;
        //rdbWaitingTimeForTheTestAverage.Checked = false;
        //rdbWaitingTimeForTheTestPoor.Checked = false;

        //rdbExperienceWithTechnicalExcellent.Checked = false;
        //rdbExperienceWithTechnicalGood.Checked = false;
        //rdbExperienceWithTechnicalFair.Checked = false;
        //rdbExperienceWithTechnicalAverage.Checked = false;
        //rdbExperienceWithTechnicalPoor.Checked = false;

        //rdbCourtesyAndFriendlinessStaffExcellent.Checked = false;
        //rdbCourtesyAndFriendlinessStaffGood.Checked = false;
        //rdbCourtesyAndFriendlinessStaffFair.Checked = false;
        //rdbCourtesyAndFriendlinessStaffAverage.Checked = false;
        //rdbCourtesyAndFriendlinessStaffPoor.Checked = false;

        rdbCourtesyAndFriendlinessStaffPathologyExcellent.Checked = false;
        rdbCourtesyAndFriendlinessStaffPathologyGood.Checked = false;
        rdbCourtesyAndFriendlinessStaffPathologyFair.Checked = false;
        rdbCourtesyAndFriendlinessStaffPathologyAverage.Checked = false;
        rdbCourtesyAndFriendlinessStaffPathologyPoor.Checked = false;

        rdbCourtesyAndFriendlinessStaffRadiologyExcellent.Checked = false;
        rdbCourtesyAndFriendlinessStaffRadiologyGood.Checked = false;
        rdbCourtesyAndFriendlinessStaffRadiologyFair.Checked = false;
        rdbCourtesyAndFriendlinessStaffRadiologyAverage.Checked = false;
        rdbCourtesyAndFriendlinessStaffRadiologyPoor.Checked = false;

        rdbCourtesyAndFriendlinessStaffCardiologyExcellent.Checked = false;
        rdbCourtesyAndFriendlinessStaffCardiologyGood.Checked = false;
        rdbCourtesyAndFriendlinessStaffCardiologyFair.Checked = false;
        rdbCourtesyAndFriendlinessStaffCardiologyAverage.Checked = false;
        rdbCourtesyAndFriendlinessStaffCardiologyPoor.Checked = false;


        rdbCourtesyAndFriendlinessStaffNuclearExcellent.Checked = false;
        rdbCourtesyAndFriendlinessStaffNuclearGood.Checked = false;
        rdbCourtesyAndFriendlinessStaffNuclearFair.Checked = false;
        rdbCourtesyAndFriendlinessStaffNuclearAverage.Checked = false;
        rdbCourtesyAndFriendlinessStaffNuclearPoor.Checked = false;

        rdbCourtesyAndFriendlinessStaffOtherExcellent.Checked = false;
        rdbCourtesyAndFriendlinessStaffOtherGood.Checked = false;
        rdbCourtesyAndFriendlinessStaffOtherFair.Checked = false;
        rdbCourtesyAndFriendlinessStaffOtherAverage.Checked = false;
        rdbCourtesyAndFriendlinessStaffOtherPoor.Checked = false;

        txtCourtesyAndFriendlinessStafothers.Text = string.Empty;

        rdbWaitingTimeForTheTestPathologyExcellent.Checked = false;
        rdbWaitingTimeForTheTestPathologyGood.Checked = false;
        rdbWaitingTimeForTheTestPathologyFair.Checked = false;
        rdbWaitingTimeForTheTestPathologyAverage.Checked = false;
        rdbWaitingTimeForTheTestPathologyPoor.Checked = false;

        rdbWaitingTimeForTheTestRadiologyExcellent.Checked = false;
        rdbWaitingTimeForTheTestRadiologyGood.Checked = false;
        rdbWaitingTimeForTheTestRadiologyFair.Checked = false;
        rdbWaitingTimeForTheTestRadiologyAverage.Checked = false;
        rdbWaitingTimeForTheTestRadiologyPoor.Checked = false;

        rdbWaitingTimeForTheTestCardiologyExcellent.Checked = false;
        rdbWaitingTimeForTheTestCardiologyGood.Checked = false;
        rdbWaitingTimeForTheTestCardiologyFair.Checked = false;
        rdbWaitingTimeForTheTestCardiologyAverage.Checked = false;
        rdbWaitingTimeForTheTestCardiologyPoor.Checked = false;

        rdbWaitingTimeForTheTestNuclearExcellent.Checked = false;
        rdbWaitingTimeForTheTestNuclearGood.Checked = false;
        rdbWaitingTimeForTheTestNuclearFair.Checked = false;
        rdbWaitingTimeForTheTestNuclearAverage.Checked = false;
        rdbWaitingTimeForTheTestNuclearPoor.Checked = false;

        rdbWaitingTimeForTheTestOtherExcellent.Checked = false;
        rdbWaitingTimeForTheTestOtherGood.Checked = false;
        rdbWaitingTimeForTheTestOtherFair.Checked = false;
        rdbWaitingTimeForTheTestOtherAverage.Checked = false;
        rdbWaitingTimeForTheTestOtherPoor.Checked = false;

        txtrdbWaitingTimeForTheTestOthers.Text = string.Empty;

        rdbCleanlinessInOPDExcellent.Checked = false;
        rdbCleanlinessInOPDGood.Checked = false;
        rdbCleanlinessInOPDFair.Checked = false;
        rdbCleanlinessInOPDAverage.Checked = false;
        rdbCleanlinessInOPDPoor.Checked = false;

        rdbCleanlinessOfToiletsExcellent.Checked = false;
        rdbCleanlinessOfToiletsGood.Checked = false;
        rdbCleanlinessOfToiletsFair.Checked = false;
        rdbCleanlinessOfToiletsAverage.Checked = false;
        rdbCleanlinessOfToiletsPoor.Checked = false;

        rdbHowWouldYouRateExcellent.Checked = false;
        rdbHowWouldYouRateGood.Checked = false;
        rdbHowWouldYouRateFair.Checked = false;
        rdbHowWouldYouRateAverage.Checked = false;
        rdbHowWouldYouRatePoor.Checked = false;

        txtWhatDidYouLIke.Text = string.Empty;
        txtWhatDidYouLikeLeast.Text = string.Empty;
        txtAnysuggestionYouWouldLike.Text = string.Empty;

    }

}