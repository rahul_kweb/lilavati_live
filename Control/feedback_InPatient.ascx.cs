﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_feedback_InPatient : System.Web.UI.UserControl
{
    FeedBackInPatient tblFeedbackInPatient = new FeedBackInPatient();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        tblFeedbackInPatient.FirstName = txtFirstName.Text.Trim();
        tblFeedbackInPatient.LastName = txtLastName.Text.Trim();
        tblFeedbackInPatient.Lh_No = txtLHNo.Text.Trim();
        tblFeedbackInPatient.BedNo = txtBedNo.Text.Trim();
        tblFeedbackInPatient.DateOfAdmission = txtDateOfAdmission.Text.Trim();
        tblFeedbackInPatient.DateOfDischarge = txtDateOfDischarge.Text.Trim();
        tblFeedbackInPatient.PhoneNo = txtContactNo.Text.Trim();
        tblFeedbackInPatient.EmailId = txtEmailId.Text.Trim();

        string chk = string.Empty;
        for (int i = 0; chkHowDidYouCome.Items.Count > i; i++)
        {
            if (chkHowDidYouCome.Items[i].Selected == true)
            {
                chk += chkHowDidYouCome.Items[i].Value + ",";
            }
        }
        if (chk != "")
        {
            chk = chk.Substring(0, chk.Length - 1);
        }
        tblFeedbackInPatient.HowDidYouComeToKnow = chk;


        /** Guidance & support provided in completing paper work **/

        if (rdbGuidanceExcellent.Checked == true)
        {
            tblFeedbackInPatient.GuidanceSupport = "Excellent";
        }
        if (rdbGuidanceGood.Checked == true)
        {
            tblFeedbackInPatient.GuidanceSupport = "Good";
        }
        if (rdbGuidanceFair.Checked == true)
        {
            tblFeedbackInPatient.GuidanceSupport = "Fair";
        }
        if (rdbGuidanceAverage.Checked == true)
        {
            tblFeedbackInPatient.GuidanceSupport = "Average";
        }
        if (rdbGuidancePoor.Checked == true)
        {
            tblFeedbackInPatient.GuidanceSupport = "Poor";
        }

        /** Courtesy and friendliness of admission staff **/

        if (rdbCourtesyExcellent.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndFriendliness = "Excellent";
        }
        if (rdbCourtesyGood.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndFriendliness = "Good";
        }
        if (rdbCourtesyFair.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndFriendliness = "Fair";
        }
        if (rdbCourtesyAverage.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndFriendliness = "Average";
        }
        if (rdbCourtesyPoor.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndFriendliness = "Poor";
        }

        /** Time taken to complete admission process **/

        if (rdbTimeTakenToCompExcellent.Checked == true)
        {
            tblFeedbackInPatient.TimeTakenToComplete = "Excellent";
        }
        if (rdbTimeTakenToCompGood.Checked == true)
        {
            tblFeedbackInPatient.TimeTakenToComplete = "Good";
        }
        if (rdbTimeTakenToCompFair.Checked == true)
        {
            tblFeedbackInPatient.TimeTakenToComplete = "Fair";
        }
        if (rdbTimeTakenToCompAverage.Checked == true)
        {
            tblFeedbackInPatient.TimeTakenToComplete = "Average";
        }
        if (rdbTimeTakenToCompPoor.Checked == true)
        {
            tblFeedbackInPatient.TimeTakenToComplete = "Poor";
        }

        /** Clarity provided about your condition and line of treatment **/

        if (rdbClarityProvidedExcellent.Checked == true)
        {
            tblFeedbackInPatient.ClarityProvided = "Excellent";
        }
        if (rdbClarityProvidedGood.Checked == true)
        {
            tblFeedbackInPatient.ClarityProvided = "Good";
        }
        if (rdbClarityProvidedFair.Checked == true)
        {
            tblFeedbackInPatient.ClarityProvided = "Fair";
        }
        if (rdbClarityProvidedAverage.Checked == true)
        {
            tblFeedbackInPatient.ClarityProvided = "Average";
        }
        if (rdbClarityProvidedPoor.Checked == true)
        {
            tblFeedbackInPatient.ClarityProvided = "Poor";
        }

        /** Care & attention (in terms of regular visit & time spent) **/

        if (rdbCareAndAttentionDocExcellent.Checked == true)
        {
            tblFeedbackInPatient.CareAttention = "Excellent";
        }
        if (rdbCareAndAttentionDocGood.Checked == true)
        {
            tblFeedbackInPatient.CareAttention = "Good";
        }
        if (rdbCareAndAttentionDocFair.Checked == true)
        {
            tblFeedbackInPatient.CareAttention = "Fair";
        }
        if (rdbCareAndAttentionDocAverage.Checked == true)
        {
            tblFeedbackInPatient.CareAttention = "Average";
        }
        if (rdbCareAndAttentionDocPoor.Checked == true)
        {
            tblFeedbackInPatient.CareAttention = "Poor";
        }

        /** Doctor's efforts to include you in the decision about your treatment. **/

        if (rdbDoctorsEffortExcellent.Checked == true)
        {
            tblFeedbackInPatient.DoctorsEfforts = "Excellent";
        }
        if (rdbDoctorsEffortGood.Checked == true)
        {
            tblFeedbackInPatient.DoctorsEfforts = "Good";
        }
        if (rdbDoctorsEffortFair.Checked == true)
        {
            tblFeedbackInPatient.DoctorsEfforts = "Fair";
        }
        if (rdbDoctorsEffortAverage.Checked == true)
        {
            tblFeedbackInPatient.DoctorsEfforts = "Average";
        }
        if (rdbDoctorsEffortPoor.Checked == true)
        {
            tblFeedbackInPatient.DoctorsEfforts = "Poor";
        }

        /** Courtesy & friendliness of Nursing Staff. **/

        if (rdbCourtesyAndFriendlinessExcellent.Checked == true)
        {
            tblFeedbackInPatient.CourtesyFriendnessNursing = "Excellent";
        }
        if (rdbCourtesyAndFriendlinessGood.Checked == true)
        {
            tblFeedbackInPatient.CourtesyFriendnessNursing = "Good";
        }
        if (rdbCourtesyAndFriendlinessFair.Checked == true)
        {
            tblFeedbackInPatient.CourtesyFriendnessNursing = "Fair";
        }
        if (rdbCourtesyAndFriendlinessAverage.Checked == true)
        {
            tblFeedbackInPatient.CourtesyFriendnessNursing = "Average";
        }
        if (rdbCourtesyAndFriendlinessPoor.Checked == true)
        {
            tblFeedbackInPatient.CourtesyFriendnessNursing = "Poor";
        }

        /** Care & attention (in terms of Medication). **/

        if (rdbCareAndAttentionNursingExcellent.Checked == true)
        {
            tblFeedbackInPatient.CareAndAttentionMedication = "Excellent";
        }
        if (rdbCareAndAttentionNursingGood.Checked == true)
        {
            tblFeedbackInPatient.CareAndAttentionMedication = "Good";
        }
        if (rdbCareAndAttentionNursingFair.Checked == true)
        {
            tblFeedbackInPatient.CareAndAttentionMedication = "Fair";
        }
        if (rdbCareAndAttentionNursingAverage.Checked == true)
        {
            tblFeedbackInPatient.CareAndAttentionMedication = "Average";
        }
        if (rdbCareAndAttentionNursingPoor.Checked == true)
        {
            tblFeedbackInPatient.CareAndAttentionMedication = "Poor";
        }

        /** Response to your needs. **/

        if (rdbResponseToNeedExcellent.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYou = "Excellent";
        }
        if (rdbResponseToNeedGood.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYou = "Good";
        }
        if (rdbResponseToNeedFair.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYou = "Fair";
        }
        if (rdbResponseToNeedAverage.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYou = "Average";
        }
        if (rdbResponseToNeedPoor.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYou = "Poor";
        }

        /** Respect for privacy and dignity. **/

        if (rdbRespectForPrivacyExcellent.Checked == true)
        {
            tblFeedbackInPatient.RespectForPrivacy = "Excellent";
        }
        if (rdbRespectForPrivacyGood.Checked == true)
        {
            tblFeedbackInPatient.RespectForPrivacy = "Good";
        }
        if (rdbRespectForPrivacyFair.Checked == true)
        {
            tblFeedbackInPatient.RespectForPrivacy = "Fair";
        }
        if (rdbRespectForPrivacyAverage.Checked == true)
        {
            tblFeedbackInPatient.RespectForPrivacy = "Average";
        }
        if (rdbRespectForPrivacyPoor.Checked == true)
        {
            tblFeedbackInPatient.RespectForPrivacy = "Poor";
        }

        /** Courtesy & friendliness of Staff. **/

        //if (rdbCourtesyFriendlinessStaffExcellent.Checked == true)
        //{
        //    tblFeedbackInPatient.CountesyOfStaff = "Excellent";
        //}
        //if (rdbCourtesyFriendlinessStaffGood.Checked == true)
        //{
        //    tblFeedbackInPatient.CountesyOfStaff = "Good";
        //}
        //if (rdbCourtesyFriendlinessStaffFair.Checked == true)
        //{
        //    tblFeedbackInPatient.CountesyOfStaff = "Fair";
        //}
        //if (rdbCourtesyFriendlinessStaffAverage.Checked == true)
        //{
        //    tblFeedbackInPatient.CountesyOfStaff = "Average";
        //}
        //if (rdbCourtesyFriendlinessStaffPoor.Checked == true)
        //{
        //    tblFeedbackInPatient.CountesyOfStaff = "Poor";
        //}

        /** Clarity of Instructions. **/

        //if (rdbClarityInstructionExcellent.Checked == true)
        //{
        //    tblFeedbackInPatient.ClarityOfInstructions = "Excellent";
        //}
        //if (rdbClarityInstructionGood.Checked == true)
        //{
        //    tblFeedbackInPatient.ClarityOfInstructions = "Good";
        //}
        //if (rdbClarityInstructionFair.Checked == true)
        //{
        //    tblFeedbackInPatient.ClarityOfInstructions = "Fair";
        //}
        //if (rdbClarityInstructionAverage.Checked == true)
        //{
        //    tblFeedbackInPatient.ClarityOfInstructions = "Average";
        //}
        //if (rdbClarityInstructionPoor.Checked == true)
        //{
        //    tblFeedbackInPatient.ClarityOfInstructions = "Poor";
        //}

        /** Quality of food & Beverage served. **/

        if (rdbQualityOfFoodExcellent.Checked == true)
        {
            tblFeedbackInPatient.QualityOfFood = "Excellent";
        }
        if (rdbQualityOfFoodGood.Checked == true)
        {
            tblFeedbackInPatient.QualityOfFood = "Good";
        }
        if (rdbQualityOfFoodFair.Checked == true)
        {
            tblFeedbackInPatient.QualityOfFood = "Fair";
        }
        if (rdbQualityOfFoodAverage.Checked == true)
        {
            tblFeedbackInPatient.QualityOfFood = "Average";
        }
        if (rdbQualityOfFoodPoor.Checked == true)
        {
            tblFeedbackInPatient.QualityOfFood = "Poor";
        }

        /** Variety of food served. **/

        //if (rdbVarityOfFoodExcellent.Checked == true)
        //{
        //    tblFeedbackInPatient.VarietyOfFood = "Excellent";
        //}
        //if (rdbVarityOfFoodGood.Checked == true)
        //{
        //    tblFeedbackInPatient.VarietyOfFood = "Good";
        //}
        //if (rdbVarityOfFoodFair.Checked == true)
        //{
        //    tblFeedbackInPatient.VarietyOfFood = "Fair";
        //}
        //if (rdbVarityOfFoodAverage.Checked == true)
        //{
        //    tblFeedbackInPatient.VarietyOfFood = "Average";
        //}
        //if (rdbVarityOfFoodPoor.Checked == true)
        //{
        //    tblFeedbackInPatient.VarietyOfFood = "Poor";
        //}

        /** Promptness of service. **/

        if (rdbPromptnessOfServiceExcellent.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfService = "Excellent";
        }
        if (rdbPromptnessOfServiceGood.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfService = "Good";
        }
        if (rdbPromptnessOfServiceFair.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfService = "Fair";
        }
        if (rdbPromptnessOfServiceAverage.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfService = "Average";
        }
        if (rdbPromptnessOfServicePoor.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfService = "Poor";
        }

        /** Information & guidance provided by dietitian regarding your diet. **/

        if (rdbInformationGuidanceExcellent.Checked == true)
        {
            tblFeedbackInPatient.InformationGuidance = "Excellent";
        }
        if (rdbInformationGuidanceGood.Checked == true)
        {
            tblFeedbackInPatient.InformationGuidance = "Good";
        }
        if (rdbInformationGuidanceFair.Checked == true)
        {
            tblFeedbackInPatient.InformationGuidance = "Fair";
        }
        if (rdbInformationGuidanceAverage.Checked == true)
        {
            tblFeedbackInPatient.InformationGuidance = "Average";
        }
        if (rdbInformationGuidancePoor.Checked == true)
        {
            tblFeedbackInPatient.InformationGuidance = "Poor";
        }

        /** Cleanliness of the room, toilet & Linen. **/

        if (rdbCleanlinessOfTheRoomExcellent.Checked == true)
        {
            tblFeedbackInPatient.CleanlinessOfRoom = "Excellent";
        }
        if (rdbCleanlinessOfTheRoomGood.Checked == true)
        {
            tblFeedbackInPatient.CleanlinessOfRoom = "Good";
        }
        if (rdbCleanlinessOfTheRoomFair.Checked == true)
        {
            tblFeedbackInPatient.CleanlinessOfRoom = "Fair";
        }
        if (rdbCleanlinessOfTheRoomAverage.Checked == true)
        {
            tblFeedbackInPatient.CleanlinessOfRoom = "Average";
        }
        if (rdbCleanlinessOfTheRoomPoor.Checked == true)
        {
            tblFeedbackInPatient.CleanlinessOfRoom = "Poor";
        }

        /** Response to your needs. **/

        if (rdbResponseToNeedHouseExcellent.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYouNeed = "Excellent";
        }
        if (rdbResponseToNeedHouseGood.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYouNeed = "Good";
        }
        if (rdbResponseToNeedHouseFair.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYouNeed = "Fair";
        }
        if (rdbResponseToNeedHouseAverage.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYouNeed = "Average";
        }
        if (rdbResponseToNeedHousePoor.Checked == true)
        {
            tblFeedbackInPatient.ResponseToYouNeed = "Poor";
        }

        /** Courtesy & helpfulness of the Housekeeping staff. **/

        if (rdbCourtesyHelpfulnessExcellent.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndHelpfulness = "Excellent";
        }
        if (rdbCourtesyHelpfulnessGood.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndHelpfulness = "Good";
        }
        if (rdbCourtesyHelpfulnessFair.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndHelpfulness = "Fair";
        }
        if (rdbCourtesyHelpfulnessAverage.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndHelpfulness = "Average";
        }
        if (rdbCourtesyHelpfulnessPoor.Checked == true)
        {
            tblFeedbackInPatient.CourtesyAndHelpfulness = "Poor";
        }


        /** Courtesy of the security guards. **/

        if (rdbCourtesySecurityExcellent.Checked == true)
        {
            tblFeedbackInPatient.CourtesyOfSecurity = "Excellent";
        }
        if (rdbCourtesySecurityGood.Checked == true)
        {
            tblFeedbackInPatient.CourtesyOfSecurity = "Good";
        }
        if (rdbCourtesySecurityFair.Checked == true)
        {
            tblFeedbackInPatient.CourtesyOfSecurity = "Fair";
        }
        if (rdbCourtesySecurityAverage.Checked == true)
        {
            tblFeedbackInPatient.CourtesyOfSecurity = "Average";
        }
        if (rdbCourtesySecurityPoor.Checked == true)
        {
            tblFeedbackInPatient.CourtesyOfSecurity = "Poor";
        }

        /** Professionalism of security guards. **/

        if (rdbProfessionalismOfSecurityExcellent.Checked == true)
        {
            tblFeedbackInPatient.Professionalism = "Excellent";
        }
        if (rdbProfessionalismOfSecurityGood.Checked == true)
        {
            tblFeedbackInPatient.Professionalism = "Good";
        }
        if (rdbProfessionalismOfSecurityFair.Checked == true)
        {
            tblFeedbackInPatient.Professionalism = "Fair";
        }
        if (rdbProfessionalismOfSecurityAverage.Checked == true)
        {
            tblFeedbackInPatient.Professionalism = "Average";
        }
        if (rdbProfessionalismOfSecurityPoor.Checked == true)
        {
            tblFeedbackInPatient.Professionalism = "Poor";
        }

        /** Billing counseling (in terms of explanation of charges/ bills) **/

        if (rdbBillingCounselingExcellent.Checked == true)
        {
            tblFeedbackInPatient.BillingCounseling = "Excellent";
        }
        if (rdbBillingCounselingGood.Checked == true)
        {
            tblFeedbackInPatient.BillingCounseling = "Good";
        }
        if (rdbBillingCounselingFair.Checked == true)
        {
            tblFeedbackInPatient.BillingCounseling = "Fair";
        }
        if (rdbBillingCounselingAverage.Checked == true)
        {
            tblFeedbackInPatient.BillingCounseling = "Average";
        }
        if (rdbBillingCounselingPoor.Checked == true)
        {
            tblFeedbackInPatient.BillingCounseling = "Poor";
        }

        /** Promptness of billing process. **/

        if (rdbPromptnessOfBillingExcellent.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfBilling = "Excellent";
        }
        if (rdbPromptnessOfBillingGood.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfBilling = "Good";
        }
        if (rdbPromptnessOfBillingFair.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfBilling = "Fair";
        }
        if (rdbPromptnessOfBillingAverage.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfBilling = "Average";
        }
        if (rdbPromptnessOfBillingPoor.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfBilling = "Poor";
        }

        /** Courtesy & helpfulness of the billing staff. **/

        if (rdbCourtesyHelpfulnessBillingExcellent.Checked == true)
        {
            tblFeedbackInPatient.CountesyHelpfulness = "Excellent";
        }
        if (rdbCourtesyHelpfulnessBillingGood.Checked == true)
        {
            tblFeedbackInPatient.CountesyHelpfulness = "Good";
        }
        if (rdbCourtesyHelpfulnessBillingFair.Checked == true)
        {
            tblFeedbackInPatient.CountesyHelpfulness = "Fair";
        }
        if (rdbCourtesyHelpfulnessBillingAverage.Checked == true)
        {
            tblFeedbackInPatient.CountesyHelpfulness = "Average";
        }
        if (rdbCourtesyHelpfulnessBillingPoor.Checked == true)
        {
            tblFeedbackInPatient.CountesyHelpfulness = "Poor";
        }

        /** Promptness of discharge process. **/

        if (rdbPromptnessOFDischageExcellent.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfDischarge = "Excellent";
        }
        if (rdbPromptnessOFDischageGood.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfDischarge = "Good";
        }
        if (rdbPromptnessOFDischageFair.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfDischarge = "Fair";
        }
        if (rdbPromptnessOFDischageAverage.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfDischarge = "Average";
        }
        if (rdbPromptnessOFDischagePoor.Checked == true)
        {
            tblFeedbackInPatient.PromptnessOfDischarge = "Poor";
        }

        /** Discharge counseling (In terms of Post discharge care & follow up) **/

        if (rdbDischargeCounselingExcellent.Checked == true)
        {
            tblFeedbackInPatient.DischargeCounseling = "Excellent";
        }
        if (rdbDischargeCounselingGood.Checked == true)
        {
            tblFeedbackInPatient.DischargeCounseling = "Good";
        }
        if (rdbDischargeCounselingFair.Checked == true)
        {
            tblFeedbackInPatient.DischargeCounseling = "Fair";
        }
        if (rdbDischargeCounselingAverage.Checked == true)
        {
            tblFeedbackInPatient.DischargeCounseling = "Average";
        }
        if (rdbDischargeCounselingPoor.Checked == true)
        {
            tblFeedbackInPatient.DischargeCounseling = "Poor";
        }

        /** Convenience of discharge process. **/

        if (rdbConvenienceOfExcellent.Checked == true)
        {
            tblFeedbackInPatient.ConvenienceOfDischarge = "Excellent";
        }
        if (rdbConvenienceOfGood.Checked == true)
        {
            tblFeedbackInPatient.ConvenienceOfDischarge = "Good";
        }
        if (rdbConvenienceOfFair.Checked == true)
        {
            tblFeedbackInPatient.ConvenienceOfDischarge = "Fair";
        }
        if (rdbConvenienceOfAverage.Checked == true)
        {
            tblFeedbackInPatient.ConvenienceOfDischarge = "Average";
        }
        if (rdbConvenienceOfPoor.Checked == true)
        {
            tblFeedbackInPatient.ConvenienceOfDischarge = "Poor";
        }

        /** How would you rate your overall experience at Lilavati Hospital. **/

        if (rdbHowWouldYouRateExcellent.Checked == true)
        {
            tblFeedbackInPatient.HowWouldYouRate = "Excellent";
        }
        if (rdbHowWouldYouRateGood.Checked == true)
        {
            tblFeedbackInPatient.HowWouldYouRate = "Good";
        }
        if (rdbHowWouldYouRateFair.Checked == true)
        {
            tblFeedbackInPatient.HowWouldYouRate = "Fair";
        }
        if (rdbHowWouldYouRateAverage.Checked == true)
        {
            tblFeedbackInPatient.HowWouldYouRate = "Average";
        }
        if (rdbHowWouldYouRatePoor.Checked == true)
        {
            tblFeedbackInPatient.HowWouldYouRate = "Poor";
        }


        /** New  Added start **/

        /** DIAGNOSTICS - Pathology (Blood Collection) **/

        if (rdbPathology_BloodCollectionExcellent.Checked == true)
        {
            tblFeedbackInPatient.Pathology_BloodCollection = "Excellent";
        }
        if (rdbPathology_BloodCollectionGood.Checked == true)
        {
            tblFeedbackInPatient.Pathology_BloodCollection = "Good";
        }
        if (rdbPathology_BloodCollectionFair.Checked == true)
        {
            tblFeedbackInPatient.Pathology_BloodCollection = "Fair";
        }
        if (rdbPathology_BloodCollectionAverage.Checked == true)
        {
            tblFeedbackInPatient.Pathology_BloodCollection = "Average";
        }
        if (rdbPathology_BloodCollectionPoor.Checked == true)
        {
            tblFeedbackInPatient.Pathology_BloodCollection = "Poor";
        }

        /** DIAGNOSTICS - Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.) **/

        if (rdbRadiology_X_rayExcellent.Checked == true)
        {
            tblFeedbackInPatient.Radiology_X_ray = "Excellent";
        }
        if (rdbRadiology_X_rayGood.Checked == true)
        {
            tblFeedbackInPatient.Radiology_X_ray = "Good";
        }
        if (rdbRadiology_X_rayFair.Checked == true)
        {
            tblFeedbackInPatient.Radiology_X_ray = "Fair";
        }
        if (rdbRadiology_X_rayAverage.Checked == true)
        {
            tblFeedbackInPatient.Radiology_X_ray = "Average";
        }
        if (rdbRadiology_X_rayPoor.Checked == true)
        {
            tblFeedbackInPatient.Radiology_X_ray = "Poor";
        }

        /** DIAGNOSTICS - Cardiology (ECG, Stress test, 2D Echo, etc) **/

        if (rdbCardiology_ECG_Stress_testExcellent.Checked == true)
        {
            tblFeedbackInPatient.Cardiology_ECG_Stress_test = "Excellent";
        }
        if (rdbCardiology_ECG_Stress_testGood.Checked == true)
        {
            tblFeedbackInPatient.Cardiology_ECG_Stress_test = "Good";
        }
        if (rdbCardiology_ECG_Stress_testFair.Checked == true)
        {
            tblFeedbackInPatient.Cardiology_ECG_Stress_test = "Fair";
        }
        if (rdbCardiology_ECG_Stress_testAverage.Checked == true)
        {
            tblFeedbackInPatient.Cardiology_ECG_Stress_test = "Average";
        }
        if (rdbCardiology_ECG_Stress_testPoor.Checked == true)
        {
            tblFeedbackInPatient.Cardiology_ECG_Stress_test = "Poor";
        }

        /** DIAGNOSTICS - Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc) **/

        if (rdbNuclear_Dept_BoneScanLiverScanExcellent.Checked == true)
        {
            tblFeedbackInPatient.Nuclear_Dept_BoneScanLiverScan = "Excellent";
        }
        if (rdbNuclear_Dept_BoneScanLiverScanGood.Checked == true)
        {
            tblFeedbackInPatient.Nuclear_Dept_BoneScanLiverScan = "Good";
        }
        if (rdbNuclear_Dept_BoneScanLiverScanFair.Checked == true)
        {
            tblFeedbackInPatient.Nuclear_Dept_BoneScanLiverScan = "Fair";
        }
        if (rdbNuclear_Dept_BoneScanLiverScanAverage.Checked == true)
        {
            tblFeedbackInPatient.Nuclear_Dept_BoneScanLiverScan = "Average";
        }
        if (rdbNuclear_Dept_BoneScanLiverScanPoor.Checked == true)
        {
            tblFeedbackInPatient.Nuclear_Dept_BoneScanLiverScan = "Poor";
        }


        /** TIME TAKEN FOR TEST & INVESTIGATION - Pathology (Blood Collection) **/

        if (rdbTimetakenforTestPathologyExcellent.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestPathology = "Excellent";
        }
        if (rdbTimetakenforTestPathologyGood.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestPathology = "Good";
        }
        if (rdbTimetakenforTestPathologyFair.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestPathology = "Fair";
        }
        if (rdbTimetakenforTestPathologyAverage.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestPathology = "Average";
        }
        if (rdbTimetakenforTestPathologyPoor.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestPathology = "Poor";
        }


        /** TIME TAKEN FOR TEST & INVESTIGATION - Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.) **/

        if (rdbTimetakenforTestRadiology_X_rayExcellent.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestRadiology_X_ray = "Excellent";
        }
        if (rdbTimetakenforTestRadiology_X_rayGood.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestRadiology_X_ray = "Good";
        }
        if (rdbTimetakenforTestRadiology_X_rayFair.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestRadiology_X_ray = "Fair";
        }
        if (rdbTimetakenforTestRadiology_X_rayAverage.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestRadiology_X_ray = "Average";
        }
        if (rdbTimetakenforTestRadiology_X_rayPoor.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestRadiology_X_ray = "Poor";
        }

        /** TIME TAKEN FOR TEST & INVESTIGATION - Cardiology (ECG, Stress test, 2D Echo, etc) **/

        if (rdbTimetakenforTestCardiology_ECG_Stress_testExcellent.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestCardiology_ECG_Stress_test = "Excellent";
        }
        if (rdbTimetakenforTestCardiology_ECG_Stress_testGood.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestCardiology_ECG_Stress_test = "Good";
        }
        if (rdbTimetakenforTestCardiology_ECG_Stress_testFair.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestCardiology_ECG_Stress_test = "Fair";
        }
        if (rdbTimetakenforTestCardiology_ECG_Stress_testAverage.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestCardiology_ECG_Stress_test = "Average";
        }
        if (rdbTimetakenforTestCardiology_ECG_Stress_testPoor.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestCardiology_ECG_Stress_test = "Poor";
        }

        /** TIME TAKEN FOR TEST & INVESTIGATION - Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc) **/

        if (rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanExcellent.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan = "Excellent";
        }
        if (rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanGood.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan = "Good";
        }
        if (rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanFair.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan = "Fair";
        }
        if (rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanAverage.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan = "Average";
        }
        if (rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanPoor.Checked == true)
        {
            tblFeedbackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan = "Poor";
        }

        tblFeedbackInPatient.WhatDidYouLike = txtWhatDidYouLikeTheMost.Text.Trim();
        tblFeedbackInPatient.WhatDidYouLikeLeast = txtWhatDidYouLikeTheLeast.Text.Trim();
        tblFeedbackInPatient.AnysuggestionYouWouldLike = txtAnySuggestion.Text.Trim();
        tblFeedbackInPatient.WouldYouLikeToRecommend = txtWouldYouLikeToRecommend.Text.Trim();

        cmsbal.SaveFeedbackInPatient(tblFeedbackInPatient);
        if (HttpContext.Current.Session["SaveFeedbackInPatient"] == "Success")
        {
            //Response.Write("<script>alert('Thanks for your Valuable Feedback.')</script>");
            Reset();

            Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup();", true);
        }
        else
        {
            Response.Write("<script>alert('Failed')</script>");
        }
       

    }


    public void Reset()
    {
        txtFirstName.Text = string.Empty;
        txtLastName.Text = string.Empty;
        txtLHNo.Text = string.Empty;
        txtBedNo.Text = string.Empty;
        txtDateOfAdmission.Text = string.Empty;
        txtDateOfDischarge.Text = string.Empty;
        txtContactNo.Text = string.Empty;
        txtEmailId.Text = string.Empty;

        chkHowDidYouCome.SelectedIndex = -1;

        /** Guidance & support provided in completing paper work **/

        rdbGuidanceExcellent.Checked = false;
        rdbGuidanceGood.Checked = false;
        rdbGuidanceFair.Checked = false;
        rdbGuidanceAverage.Checked = false;
        rdbGuidancePoor.Checked = false;

        rdbCourtesyExcellent.Checked = false;
        rdbCourtesyGood.Checked = false;
        rdbCourtesyFair.Checked = false;
        rdbCourtesyAverage.Checked = false;
        rdbCourtesyPoor.Checked = false;

        rdbTimeTakenToCompExcellent.Checked = false;
        rdbTimeTakenToCompGood.Checked = false;
        rdbTimeTakenToCompFair.Checked = false;
        rdbTimeTakenToCompAverage.Checked = false;
        rdbTimeTakenToCompPoor.Checked = false;

        rdbClarityProvidedExcellent.Checked = false;
        rdbClarityProvidedGood.Checked = false;
        rdbClarityProvidedFair.Checked = false;
        rdbClarityProvidedAverage.Checked = false;
        rdbClarityProvidedPoor.Checked = false;

        rdbCareAndAttentionDocExcellent.Checked = false;
        rdbCareAndAttentionDocGood.Checked = false;
        rdbCareAndAttentionDocFair.Checked = false;
        rdbCareAndAttentionDocAverage.Checked = false;
        rdbCareAndAttentionDocPoor.Checked = false;

        rdbDoctorsEffortExcellent.Checked = false;
        rdbDoctorsEffortGood.Checked = false;
        rdbDoctorsEffortFair.Checked = false;
        rdbDoctorsEffortAverage.Checked = false;
        rdbDoctorsEffortPoor.Checked = false;

        rdbCourtesyAndFriendlinessExcellent.Checked = false;
        rdbCourtesyAndFriendlinessGood.Checked = false;
        rdbCourtesyAndFriendlinessFair.Checked = false;
        rdbCourtesyAndFriendlinessAverage.Checked = false;
        rdbCourtesyAndFriendlinessPoor.Checked = false;

        rdbCareAndAttentionNursingExcellent.Checked = false;
        rdbCareAndAttentionNursingGood.Checked = false;
        rdbCareAndAttentionNursingFair.Checked = false;
        rdbCareAndAttentionNursingAverage.Checked = false;
        rdbCareAndAttentionNursingPoor.Checked = false;

        rdbResponseToNeedExcellent.Checked = false;
        rdbResponseToNeedGood.Checked = false;
        rdbResponseToNeedFair.Checked = false;
        rdbResponseToNeedAverage.Checked = false;
        rdbResponseToNeedPoor.Checked = false;

        rdbRespectForPrivacyExcellent.Checked = false;
        rdbRespectForPrivacyGood.Checked = false;
        rdbRespectForPrivacyFair.Checked = false;
        rdbRespectForPrivacyAverage.Checked = false;
        rdbRespectForPrivacyPoor.Checked = false;

        //rdbCourtesyFriendlinessStaffExcellent.Checked = false;
        //rdbCourtesyFriendlinessStaffGood.Checked = false;
        //rdbCourtesyFriendlinessStaffFair.Checked = false;
        //rdbCourtesyFriendlinessStaffAverage.Checked = false;
        //rdbCourtesyFriendlinessStaffPoor.Checked = false;

        //rdbClarityInstructionExcellent.Checked = false;
        //rdbClarityInstructionGood.Checked = false;
        //rdbClarityInstructionFair.Checked = false;
        //rdbClarityInstructionAverage.Checked = false;
        //rdbClarityInstructionPoor.Checked = false;

        rdbQualityOfFoodExcellent.Checked = false;
        rdbQualityOfFoodGood.Checked = false;
        rdbQualityOfFoodFair.Checked = false;
        rdbQualityOfFoodAverage.Checked = false;
        rdbQualityOfFoodPoor.Checked = false;

        //rdbVarityOfFoodExcellent.Checked = false;
        //rdbVarityOfFoodGood.Checked = false;
        //rdbVarityOfFoodFair.Checked = false;
        //rdbVarityOfFoodAverage.Checked = false;
        //rdbVarityOfFoodPoor.Checked = false;

        rdbPromptnessOfServiceExcellent.Checked = false;
        rdbPromptnessOfServiceGood.Checked = false;
        rdbPromptnessOfServiceFair.Checked = false;
        rdbPromptnessOfServiceAverage.Checked = false;
        rdbPromptnessOfServicePoor.Checked = false;

        rdbInformationGuidanceExcellent.Checked = false;
        rdbInformationGuidanceGood.Checked = false;
        rdbInformationGuidanceFair.Checked = false;
        rdbInformationGuidanceAverage.Checked = false;
        rdbInformationGuidancePoor.Checked = false;

        rdbCleanlinessOfTheRoomExcellent.Checked = false;
        rdbCleanlinessOfTheRoomGood.Checked = false;
        rdbCleanlinessOfTheRoomFair.Checked = false;
        rdbCleanlinessOfTheRoomAverage.Checked = false;
        rdbCleanlinessOfTheRoomPoor.Checked = false;

        rdbResponseToNeedHouseExcellent.Checked = false;
        rdbResponseToNeedHouseGood.Checked = false;
        rdbResponseToNeedHouseFair.Checked = false;
        rdbResponseToNeedHouseAverage.Checked = false;
        rdbResponseToNeedHousePoor.Checked = false;

        rdbCourtesyHelpfulnessExcellent.Checked = false;
        rdbCourtesyHelpfulnessGood.Checked = false;
        rdbCourtesyHelpfulnessFair.Checked = false;
        rdbCourtesyHelpfulnessAverage.Checked = false;
        rdbCourtesyHelpfulnessPoor.Checked = false;

        rdbCourtesySecurityExcellent.Checked = false;
        rdbCourtesySecurityGood.Checked = false;
        rdbCourtesySecurityFair.Checked = false;
        rdbCourtesySecurityAverage.Checked = false;
        rdbCourtesySecurityPoor.Checked = false;

        rdbProfessionalismOfSecurityExcellent.Checked = false;
        rdbProfessionalismOfSecurityGood.Checked = false;
        rdbProfessionalismOfSecurityFair.Checked = false;
        rdbProfessionalismOfSecurityAverage.Checked = false;
        rdbProfessionalismOfSecurityPoor.Checked = false;

        rdbBillingCounselingExcellent.Checked = false;
        rdbBillingCounselingGood.Checked = false;
        rdbBillingCounselingFair.Checked = false;
        rdbBillingCounselingAverage.Checked = false;
        rdbBillingCounselingPoor.Checked = false;

        rdbPromptnessOfBillingExcellent.Checked = false;
        rdbPromptnessOfBillingGood.Checked = false;
        rdbPromptnessOfBillingFair.Checked = false;
        rdbPromptnessOfBillingAverage.Checked = false;
        rdbPromptnessOfBillingPoor.Checked = false;

        rdbCourtesyHelpfulnessBillingExcellent.Checked = false;
        rdbCourtesyHelpfulnessBillingGood.Checked = false;
        rdbCourtesyHelpfulnessBillingFair.Checked = false;
        rdbCourtesyHelpfulnessBillingAverage.Checked = false;
        rdbCourtesyHelpfulnessBillingPoor.Checked = false;

        rdbPromptnessOFDischageExcellent.Checked = false;
        rdbPromptnessOFDischageGood.Checked = false;
        rdbPromptnessOFDischageFair.Checked = false;
        rdbPromptnessOFDischageAverage.Checked = false;
        rdbPromptnessOFDischagePoor.Checked = false;

        rdbDischargeCounselingExcellent.Checked = false;
        rdbDischargeCounselingGood.Checked = false;
        rdbDischargeCounselingFair.Checked = false;
        rdbDischargeCounselingAverage.Checked = false;
        rdbDischargeCounselingPoor.Checked = false;

        rdbConvenienceOfExcellent.Checked = false;
        rdbConvenienceOfGood.Checked = false;
        rdbConvenienceOfFair.Checked = false;
        rdbConvenienceOfAverage.Checked = false;
        rdbConvenienceOfPoor.Checked = false;

        rdbHowWouldYouRateExcellent.Checked = false;
        rdbHowWouldYouRateGood.Checked = false;
        rdbHowWouldYouRateFair.Checked = false;
        rdbHowWouldYouRateAverage.Checked = false;
        rdbHowWouldYouRatePoor.Checked = false;

        txtWhatDidYouLikeTheMost.Text = string.Empty;
        txtWhatDidYouLikeTheLeast.Text = string.Empty;
        txtAnySuggestion.Text = string.Empty;
        txtWouldYouLikeToRecommend.Text = string.Empty;

        /*** New Added ***/

        rdbPathology_BloodCollectionExcellent.Checked = false;
        rdbPathology_BloodCollectionGood.Checked = false;
        rdbPathology_BloodCollectionFair.Checked = false;
        rdbPathology_BloodCollectionAverage.Checked = false;
        rdbPathology_BloodCollectionPoor.Checked = false;

        rdbRadiology_X_rayExcellent.Checked = false;
        rdbRadiology_X_rayGood.Checked = false;
        rdbRadiology_X_rayFair.Checked = false;
        rdbRadiology_X_rayAverage.Checked = false;
        rdbRadiology_X_rayPoor.Checked = false;

        rdbCardiology_ECG_Stress_testExcellent.Checked = false;
        rdbCardiology_ECG_Stress_testGood.Checked = false;
        rdbCardiology_ECG_Stress_testFair.Checked = false;
        rdbCardiology_ECG_Stress_testAverage.Checked = false;
        rdbCardiology_ECG_Stress_testPoor.Checked = false;

        rdbNuclear_Dept_BoneScanLiverScanExcellent.Checked = false;
        rdbNuclear_Dept_BoneScanLiverScanGood.Checked = false;
        rdbNuclear_Dept_BoneScanLiverScanFair.Checked = false;
        rdbNuclear_Dept_BoneScanLiverScanAverage.Checked = false;
        rdbNuclear_Dept_BoneScanLiverScanPoor.Checked = false;

        rdbTimetakenforTestRadiology_X_rayExcellent.Checked = false;
        rdbTimetakenforTestRadiology_X_rayGood.Checked = false;
        rdbTimetakenforTestRadiology_X_rayFair.Checked = false;
        rdbTimetakenforTestRadiology_X_rayAverage.Checked = false;
        rdbTimetakenforTestRadiology_X_rayPoor.Checked = false;

        rdbTimetakenforTestCardiology_ECG_Stress_testExcellent.Checked = false;
        rdbTimetakenforTestCardiology_ECG_Stress_testGood.Checked = false;
        rdbTimetakenforTestCardiology_ECG_Stress_testFair.Checked = false;
        rdbTimetakenforTestCardiology_ECG_Stress_testAverage.Checked = false;
        rdbTimetakenforTestCardiology_ECG_Stress_testPoor.Checked = false;


        rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanExcellent.Checked = false;
        rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanGood.Checked = false;
        rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanFair.Checked = false;
        rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanAverage.Checked = false;
        rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanPoor.Checked = false;

    }

}