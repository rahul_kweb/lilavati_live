﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="webviewservices.ascx.cs" Inherits="Control_webviewservices" %>
<div class="rel_menu fade_anim">
    <ul>
        <li id="idambulance" runat="server"><a href="Web-Ambulance">Ambulance</a></li>
        <li id="idblood_bank" runat="server"><a href="web-Blood-Bank">Blood Bank</a></li>
        <li id="idcentres_of_excellence" runat="server"><a href="web-Centres-of-Excellence">Centres
            of Excellence</a></li>
        <li id="idcritical_care" runat="server"><a href="web-Critical-Care">Critical Care</a></li>
        <li id="idday_care" runat="server"><a href="web-Day-Care">Day Care</a></li>
        <li id="iddiagnostics" runat="server"><a href="web-Diagnostics">Diagnostics</a></li>
        <li id="iddialysis_centre" runat="server"><a href="web-Dialysis-Centre">Dialysis Centre</a></li>
        <li id="idemergency_trauma" runat="server"><a href="web-Emergency-Trauma">Emergency/ Trauma</a></li>
        <li id="idhealth_checkup" runat="server"><a href="web-Health-Checkup">Health Check Up</a></li>
        <li id="idinterventional_neuroradiology" runat="server"><a href="web-Interventional-Neuroradiology">Interventional Neuroradiology</a></li>
        <li id="idinterventional_radiology" runat="server"><a href="web-Interventional_Radiology">Interventional Radiology</a></li>
        <li id="idout_patient" runat="server"><a href="web-Out-Patient">Out Patient</a></li>
        <li id="idpharmacy" runat="server"><a href="web-Pharmacy">Pharmacy/ Chemist</a></li>
        <li id="idphysiotherapy" runat="server"><a href="web-Physiotherapy">Physiotherapy</a></li>
        <%--<li id="idnuclear_medicine" runat="server"><a href="Nuclear_Medicine">Nuclear Medicine</a></li>--%>
        <li id="idsocial_initiative" runat="server"><a href="web-Social-Initiative?tab_list=0">Social Initiatives</a></li>
        <li id="idvisa_investigation" runat="server"><a href="web-Visa-Investigation">Visa Investigation</a></li>

    </ul>
</div>
<%--<div class="ad_box">
    <a href="Health-Checkup?tab_list=0">
        <img src="images/ad_img.jpg">
    </a>
</div>--%>
