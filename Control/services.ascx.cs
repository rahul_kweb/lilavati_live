﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_services : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

        if (PageName == "Centres-of-Excellence")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Out-Patient")
        {
            idout_patient.Attributes.Add("class", "active");
        }
        else if (PageName == "Critical-Care")
        {
            idcritical_care.Attributes.Add("class", "active");
        }
        else if (PageName == "Emergency-Trauma")
        {
            idemergency_trauma.Attributes.Add("class", "active");
        }
        else if (PageName == "Day-Care")
        {
            idday_care.Attributes.Add("class", "active");
        }
        else if (PageName == "Dialysis-Centre")
        {
            iddialysis_centre.Attributes.Add("class", "active");
        }
        else if (PageName == "Physiotherapy")
        {
            idphysiotherapy.Attributes.Add("class", "active");
        }
        else if (PageName == "Diagnostics")
        {
            iddiagnostics.Attributes.Add("class", "active");
        }
        else if (PageName == "Blood-Bank")
        {
            idblood_bank.Attributes.Add("class", "active");
        }
        else if (PageName == "Pharmacy")
        {
            idpharmacy.Attributes.Add("class", "active");
        }
        else if (PageName == "Ambulance")
        {
            idambulance.Attributes.Add("class", "active");
        }
        else if (PageName == "Health-Checkup")
        {
            idhealth_checkup.Attributes.Add("class", "active");
        }
        else if (PageName == "Visa-Investigation")
        {
            idvisa_investigation.Attributes.Add("class", "active");
        }
        else if (PageName == "Social-Initiative")
        {
            idsocial_initiative.Attributes.Add("class", "active");
        }
        else if (PageName == "Interventional_Radiology")
        {
            idinterventional_radiology.Attributes.Add("class", "active");
        }
        else if (PageName == "Nuclear_Medicine")
        {
            //idnuclear_medicine.Attributes.Add("class", "active");
        }
        else if (PageName == "Interventional-Neuroradiology")
        {
            idinterventional_neuroradiology.Attributes.Add("class", "active");
        }

        /*** COE Page ***/

        else if (PageName == "Anesthesiology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Audiology-Speech-Therapy")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Bariatric-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Cardiology-CVTS")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Chest-Medicine")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Colorectal-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Dental")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Dermo-Cosmetology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Dermo-Cosmetology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Ent")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Gastroenterology-Gastrointestinal-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "General-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Gynaecology-Obstetrics")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Haematology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Headache-Migraine-Clinic")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Hip-Replacement")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Infectious-Diseases")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Internal-Medicine")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Minimally-Invasive-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Nephrology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Neurology-Neuro-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Oncology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Ophthalmology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Orthopaedics")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Pediatrics-And-Paediatric-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Plastic-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Psychiatry-Psychology-Neuropsychology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Rheumatology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Sleep-Medicine")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Urology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Vascular-Endovascular-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Pain-Management")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Diabetology-Endocrinology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Hair-Transplant")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Corneal-Transplant")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "lactation")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "liver-transplant")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "Hydrotherapy-Centre")
        {
            idhydrotherapy.Attributes.Add("class", "active");
        }

        /*** End COE ***/




    }
}