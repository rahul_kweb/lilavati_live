﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="feedback_InPatient.ascx.cs" Inherits="Control_feedback_InPatient" %>

<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.fancybox').fancybox();

    });
</script>

<script type="text/javascript">
    function ShowPopup() {
        $(function () {
            //$("#apply_form1").html(message);
            //$("#apply_form1").dialog({
            //    title: "Feedback In-Patient",
            //    buttons: {
            //        Close: function () {
            //            $(this).dialog('close');
            //        }
            //    },
            //    modal: true
            //});                       

            $("#apply_form1").fancybox().trigger('click');

        });
    };

    //$(document).ready(function () {
    //    $("#apply_form1").fancybox().trigger('click');
    //});

</script>

<script>
    $(function () {
        $(".pickdate").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "c-100:c",
            dateFormat: "dd-mm-yy"
        });


        $('#<%=txtDateOfAdmission.ClientID%>').change(function () {

            var data = $('#<%=txtDateOfAdmission.ClientID%>').val();
            if (data.length != 0 || data != "") {

                $("#DoAdm").removeClass("error");
                $("#DoAdm").addClass("populated");
            }
            else {
                $("#DoAdm").addClass("error");
            }
        });

        $('#<%=txtDateOfDischarge.ClientID%>').change(function () {

            var data = $('#<%=txtDateOfDischarge.ClientID%>').val();
            if (data.length != 0 || data != "") {

                $("#DoDis").removeClass("error");
                $("#DoDis").addClass("populated");
            }
            else {
                $("#DoDis").addClass("error");
            }
        });

    });
</script>

<script>
    function CheckSave() {
        var FirstName = $('#<%=txtFirstName.ClientID%>').val();
        var LastName = $('#<%=txtLastName.ClientID%>').val();
        var LHNo = $('#<%=txtLHNo.ClientID%>').val();
        var BedNo = $('#<%=txtBedNo.ClientID%>').val();
        var DateOfAdmission = $('#<%=txtDateOfAdmission.ClientID%>').val();
        var DateOfDischarge = $('#<%=txtDateOfDischarge.ClientID%>').val();
        var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
        var EmailId = $('#<%=txtEmailId.ClientID%>').val();
        var blank = false;

        if (FirstName == '') {
            $("#First").addClass("error");
            blank = true;
        }
        else {
            $("#First").removeClass("error");
        }

        if (LastName == '') {
            $("#Last").addClass("error");
            blank = true;
        }
        else {
            $("#Last").removeClass("error");
        }

        if (LHNo == '') {
            $("#LHNo").addClass("error");
            blank = true;
        }
        else {
            $("#LHNo").removeClass("error");
        }

        if (BedNo == '') {
            $("#BedNo").addClass("error");
            blank = true;
        }
        else {
            $("#BedNo").removeClass("error");
        }

        if (DateOfAdmission == '') {
            $("#DoAdm").addClass("error");
            blank = true;
        }
        else {
            $("#DoAdm").removeClass("error");
        }

        if (DateOfDischarge == '') {
            $("#DoDis").addClass("error");
            blank = true;
        }
        else {
            $("#DoDis").removeClass("error");
        }

        if (ContactNo == '') {
            $("#ContactNo").addClass("error");
            blank = true;
        }
        else {
            if (ContactNo.length < 10) {
                $("#ContactNo1").addClass("error");
                blank = true;
            }
            else {
                $("#ContactNo1").removeClass("error");
            }
        }

        if (EmailId == '') {
            $("#EmailId").addClass("error");
            blank = true;
        }
        else {
            var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText.test(EmailId)) {
                $("#EmailId").addClass("error");
                alert("Valid Email ID");
                blank = true;
            }
            else {
                $("#EmailId").removeClass("error");
            }
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }

    }
</script>

<script>

    function FirstNameFun() {
        var FirstName = $('#<%=txtFirstName.ClientID%>').val();
        var blank = false;

        if (FirstName == '') {
            $("#First").addClass("error");
            blank = true;
        }
        else {
            $("#First").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function LastNAmeFun() {
        var LastName = $('#<%=txtLastName.ClientID%>').val();
        var blank = false;

        if (LastName == '') {
            $("#Last").addClass("error");
            blank = true;
        }
        else {
            $("#Last").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }

    }

    function LhnoFun() {
        var LHNo = $('#<%=txtLHNo.ClientID%>').val();
        var blank = false;

        if (LHNo == '') {
            $("#LHNo").addClass("error");
            blank = true;
        }
        else {
            $("#LHNo").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function BedNofun() {
        var BedNo = $('#<%=txtBedNo.ClientID%>').val();
        var blank = false;

        if (BedNo == '') {
            $("#BedNo").addClass("error");
            blank = true;
        }
        else {
            $("#BedNo").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function DateofAdmissionFun() {
        var DateOfAdmission = $('#<%=txtDateOfAdmission.ClientID%>').val();
        var blank = false;

        if (DateOfAdmission == '') {
            $("#DoAdm").addClass("error");
            blank = true;
        }
        else {
            $("#DoAdm").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function DateOfDischageFun() {
        var DateOfDischarge = $('#<%=txtDateOfDischarge.ClientID%>').val();
        var blank = false;

        if (DateOfDischarge == '') {
            $("#DoDis").addClass("error");
            blank = true;
        }
        else {
            $("#DoDis").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function ContactFun() {
        var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
        var blank = false;

        if (ContactNo == '') {
            $("#ContactNo").addClass("error");
            blank = true;
        }
        else {
            if (ContactNo.length < 10) {
                $("#ContactNo").addClass("error");
                blank = true;
            }
            else {
                $("#ContactNo").removeClass("error");
            }
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function EmailFun() {
        var EmailId = $('#<%=txtEmailId.ClientID%>').val();
        var blank = false;

        if (EmailId == '') {
            $("#EmailId").addClass("error");
            blank = true;
        }
        else {
            var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText.test(EmailId)) {
                $("#EmailId").addClass("error");
                alert("Valid Email ID");
                blank = true;
            }
            else {
                $("#EmailId").removeClass("error");
            }
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }


    $(document).ready(function () {

        /** First Name **/
        $(".Fn").click(function () {
            FirstNameFun();
        });
        $(".Fn").keyup(function () {
            FirstNameFun();
        });

        /** LAst Name **/
        $(".Ln").click(function () {
            LastNAmeFun();
        });
        $(".Ln").keyup(function () {
            LastNAmeFun();
        });

        /** LHNO **/
        $(".Lh").click(function () {
            LhnoFun();
        });
        $(".Lh").keyup(function () {
            LhnoFun();
        });

        /** Bed No **/
        $(".Bd").click(function () {
            BedNofun();
        });
        $(".Bd").keyup(function () {
            BedNofun();
        });

        /** Date Of Admission **/
        $(".Da").click(function () {
            DateofAdmissionFun();
        });
        $(".Da").keyup(function () {
            DateofAdmissionFun();
        });

        /** Date Of Discharge **/
        $(".Dd").click(function () {
            DateOfDischageFun();
        });
        $(".Dd").keyup(function () {
            DateOfDischageFun();
        });

        /** Contact No **/
        $(".Cn").click(function () {
            ContactFun();
        });
        $(".Cn").keyup(function () {
            ContactFun();
        });

        /** Email ID **/
        $(".EI").click(function () {
            EmailFun();
        });

        $(".EI").change(function () {
            EmailFun();
        });

    });

    $(document).ready(function () {

        $('#ctl00_ContentPlaceHolder1_InPatient_btnSubmit').click(function (event) {
            var FirstName = $('#<%=txtFirstName.ClientID%>').val();
            var LastName = $('#<%=txtLastName.ClientID%>').val();
            var LHNo = $('#<%=txtLHNo.ClientID%>').val();
            var BedNo = $('#<%=txtBedNo.ClientID%>').val();
            var DateOfAdmission = $('#<%=txtDateOfAdmission.ClientID%>').val();
            var DateOfDischarge = $('#<%=txtDateOfDischarge.ClientID%>').val();
            var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
            var EmailId = $('#<%=txtEmailId.ClientID%>').val();

            if (FirstName != '' && LastName != '' && LHNo != '' && BedNo != '' && DateOfAdmission != '' && DateOfDischarge != '' && ContactNo.length == '10' && EmailId != '') {
                $('#in_validate').hide();

                var EmailText11 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText11.test(EmailId)) {
                    var target = $($(this).attr('title'));
                    if (target.length) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top = '120'
                        }, 1000);
                    }
                }
                else {
                    $('#in_validate').hide();
                }

            }
            else {

                $('#in_validate').show();

                var target = $($(this).attr('title'));
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top = '120'
                    }, 1000);
                }
            }

        });


        //  $('.commonclass').keypress(function () {
        //      var FirstName = $('#<%=txtFirstName.ClientID%>').val();
        //      var LastName = $('#<%=txtLastName.ClientID%>').val();
        //     var LHNo = $('#<%=txtLHNo.ClientID%>').val();
        //     var BedNo = $('#<%=txtBedNo.ClientID%>').val();
        //     var DateOfAdmission = $('#<%=txtDateOfAdmission.ClientID%>').val();
        //    var DateOfDischarge = $('#<%=txtDateOfDischarge.ClientID%>').val();
        //     var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
        //     var EmailId = $('#<%=txtEmailId.ClientID%>').val();

        //     if (FirstName != '' && LastName != '' && LHNo != '' && BedNo != '' && DateOfAdmission != '' && DateOfDischarge != '' && ContactNo.length == '10' && EmailId != '') {
        //         $('#in_validate').hide();
        // 
        //        var EmailText11 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        // 
        //         if (!EmailText11.test(EmailId)) {
        //            $('#in_validate').show();
        //         }
        //         else {                    
        //             $('#in_validate').hide();
        //         }
        //     }
        //    else {
        //         $('#in_validate').show();
        //    }
        //  });


        $('.Da').keypress(function (event) {
            event.preventDefault();
        });

        $('.Dd').keypress(function (event) {
            event.preventDefault();
        });

    });


</script>

<script type="text/javascript">
    function onlyNumbers(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

<div class="tab_content">

    <div class="feedback_form FlowupLabels">

        <div class="validate_msg" id="in_validate" style="display: none;">Fields marked in red are complusory.</div>

        <div class="form_field fl_wrap" id="First">
            <label class="fl_label">First Name</label>
            <asp:TextBox ID="txtFirstName" runat="server" class="fl_input Fn commonclass"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="Last">
            <label class="fl_label">Last Name</label>
            <asp:TextBox ID="txtLastName" runat="server" class="fl_input Ln commonclass"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="LHNo">
            <label class="fl_label">LH No.</label>
            <asp:TextBox ID="txtLHNo" runat="server" class="fl_input Lh commonclass"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="BedNo">
            <label class="fl_label">Bed No.</label>
            <asp:TextBox ID="txtBedNo" runat="server" class="fl_input Bd commonclass"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="DoAdm">
            <label class="fl_label" for="dob">Date of Admission</label>
            <asp:TextBox ID="txtDateOfAdmission" runat="server" class="fl_input pickdate Da commonclass"></asp:TextBox>
            <%--<input class="fl_input pickdate" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="DoDis">
            <label class="fl_label" for="dob">Date of Discharge</label>
            <asp:TextBox ID="txtDateOfDischarge" runat="server" class="fl_input pickdate Dd commonclass"></asp:TextBox>
            <%--<input class="fl_input pickdate" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="ContactNo">
            <label class="fl_label">Contact No.</label>
            <asp:TextBox ID="txtContactNo" runat="server" class="fl_input Cn commonclass" MaxLength="10" onKeyPress='javascript:return onlyNumbers();'></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="EmailId">
            <label class="fl_label">Email ID</label>
            <asp:TextBox ID="txtEmailId" runat="server" class="fl_input EI commonclass"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear" style="height: 20px;"></div>

        <p><strong class="green_text uppercase">Guidelines </strong></p>
        <ul class="bullet">
            <li>To rate the following services that you received during your visit kindly tick in the box that represents your opinion. In case you did not receive any particular service; skip to the next question.</li>
            <li>For rating Fair and below; we request you to give your suggestion for improvement.</li>
        </ul>

        <p><strong class="green_text uppercase">How did you come to know about Lilavati Hospital? </strong></p>


        <asp:CheckBoxList ID="chkHowDidYouCome" runat="server" class="ez_checkbox fdbk_know">
            <asp:ListItem>Lilavati Hospital Doctor</asp:ListItem>
            <asp:ListItem>Reference of family doctor</asp:ListItem>
            <asp:ListItem>Corporate Tie Up </asp:ListItem>
            <asp:ListItem>Website</asp:ListItem>
            <asp:ListItem>Hospital employee</asp:ListItem>
            <asp:ListItem>Relative/ Friend</asp:ListItem>
            <asp:ListItem>Any Other</asp:ListItem>
        </asp:CheckBoxList>

        <p class="ez_checkbox fdbk_know">
            <%--<input type="checkbox" id="in_know1">
                                    <label for="in_know1">Lilavati Hospital Doctor </label>
                                    <br>
                                    <input type="checkbox" id="in_know2">
                                    <label for="in_know2">Reference of family doctor </label>
                                    <br>
                                    <input type="checkbox" id="in_know3">
                                    <label for="in_know3">Corporate Tie Up </label>
                                    <br>
                                    <input type="checkbox" id="in_know4">
                                    <label for="in_know4">Website </label>
                                    <br>
                                    <input type="checkbox" id="in_know5">
                                    <label for="in_know5">Hospital employee </label>
                                    <br>
                                    <input type="checkbox" id="in_know6">
                                    <label for="in_know6">Relative/ Friend </label>--%>
        </p>

        <div class="clear"></div>

        <div class="fdbk_box">
            <div class="fdbk_left"></div>
            <div class="fdbk_right green_text uppercase">
                <ul>
                    <li>Excellent</li>
                    <li>Good</li>
                    <li>Fair</li>
                    <li>Average</li>
                    <li>Poor</li>
                </ul>
            </div>
        </div>

                <div class="fdbk_subtl green_text uppercase"><strong><span>A.</span> Overall Experience</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">How would you rate your overall experience at Lilavati Hospital.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateExcellent" runat="server" GroupName="HowWouldYouRate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateGood" runat="server" GroupName="HowWouldYouRate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateFair" runat="server" GroupName="HowWouldYouRate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateAverage" runat="server" GroupName="HowWouldYouRate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRatePoor" runat="server" GroupName="HowWouldYouRate" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_j_1"></li>
                                            <li>
                                                <input type="radio" name="in_j_1"></li>
                                            <li>
                                                <input type="radio" name="in_j_1"></li>
                                            <li>
                                                <input type="radio" name="in_j_1"></li>
                                            <li>
                                                <input type="radio" name="in_j_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>B.</span> Admission</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Guidance & support provided in completing paper work & formalities for admission.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <%--<input type="radio" name="in_a_1">--%>
                        <asp:RadioButton ID="rdbGuidanceExcellent" runat="server" GroupName="Guidance" />
                    </li>

                    <li>
                        <%--<input type="radio" name="in_a_1">--%>
                        <asp:RadioButton ID="rdbGuidanceGood" runat="server" GroupName="Guidance" />
                    </li>
                    <li>
                        <%--<input type="radio" name="in_a_1">--%>
                        <asp:RadioButton ID="rdbGuidanceFair" runat="server" GroupName="Guidance" />
                    </li>
                    <li>
                        <%--<input type="radio" name="in_a_1">--%>
                        <asp:RadioButton ID="rdbGuidanceAverage" runat="server" GroupName="Guidance" />
                    </li>
                    <li>
                        <%--<input type="radio" name="in_a_1">--%>
                        <asp:RadioButton ID="rdbGuidancePoor" runat="server" GroupName="Guidance" />
                    </li>

                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Courtesy and friendliness of admission staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyExcellent" runat="server" GroupName="Courtesy" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCourtesyGood" runat="server" GroupName="Courtesy" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyFair" runat="server" GroupName="Courtesy" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAverage" runat="server" GroupName="Courtesy" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyPoor" runat="server" GroupName="Courtesy" />
                    </li>
                </ul>

                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_a_2"></li>
                                            <li>
                                                <input type="radio" name="in_a_2"></li>
                                            <li>
                                                <input type="radio" name="in_a_2"></li>
                                            <li>
                                                <input type="radio" name="in_a_2"></li>
                                            <li>
                                                <input type="radio" name="in_a_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Time taken to complete admission process.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompExcellent" runat="server" GroupName="TimeTakenToComp" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompGood" runat="server" GroupName="TimeTakenToComp" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompFair" runat="server" GroupName="TimeTakenToComp" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompAverage" runat="server" GroupName="TimeTakenToComp" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompPoor" runat="server" GroupName="TimeTakenToComp" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_a_3"></li>
                                            <li>
                                                <input type="radio" name="in_a_3"></li>
                                            <li>
                                                <input type="radio" name="in_a_3"></li>
                                            <li>
                                                <input type="radio" name="in_a_3"></li>
                                            <li>
                                                <input type="radio" name="in_a_3"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>C.</span> Doctor</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Clarity provided about your condition and line of treatment.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbClarityProvidedExcellent" runat="server" GroupName="ClarityProvided" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbClarityProvidedGood" runat="server" GroupName="ClarityProvided" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbClarityProvidedFair" runat="server" GroupName="ClarityProvided" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbClarityProvidedAverage" runat="server" GroupName="ClarityProvided" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbClarityProvidedPoor" runat="server" GroupName="ClarityProvided" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_b_1"></li>
                                            <li>
                                                <input type="radio" name="in_b_1"></li>
                                            <li>
                                                <input type="radio" name="in_b_1"></li>
                                            <li>
                                                <input type="radio" name="in_b_1"></li>
                                            <li>
                                                <input type="radio" name="in_b_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Care & attention (in terms of regular visit & time spent)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionDocExcellent" runat="server" GroupName="CareAndAttention" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionDocGood" runat="server" GroupName="CareAndAttention" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionDocFair" runat="server" GroupName="CareAndAttention" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionDocAverage" runat="server" GroupName="CareAndAttention" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionDocPoor" runat="server" GroupName="CareAndAttention" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_b_2"></li>
                                            <li>
                                                <input type="radio" name="in_b_2"></li>
                                            <li>
                                                <input type="radio" name="in_b_2"></li>
                                            <li>
                                                <input type="radio" name="in_b_2"></li>
                                            <li>
                                                <input type="radio" name="in_b_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Doctor's efforts to include you in the decision about your treatment.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbDoctorsEffortExcellent" runat="server" GroupName="DoctorsEffort" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbDoctorsEffortGood" runat="server" GroupName="DoctorsEffort" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDoctorsEffortFair" runat="server" GroupName="DoctorsEffort" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDoctorsEffortAverage" runat="server" GroupName="DoctorsEffort" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDoctorsEffortPoor" runat="server" GroupName="DoctorsEffort" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_b_3"></li>
                                            <li>
                                                <input type="radio" name="in_b_3"></li>
                                            <li>
                                                <input type="radio" name="in_b_3"></li>
                                            <li>
                                                <input type="radio" name="in_b_3"></li>
                                            <li>
                                                <input type="radio" name="in_b_3"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>D.</span> Nursing care</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Courtesy & friendliness of Nursing Staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessExcellent" runat="server" GroupName="CourtesyAndFriendliness" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessGood" runat="server" GroupName="CourtesyAndFriendliness" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessFair" runat="server" GroupName="CourtesyAndFriendliness" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessAverage" runat="server" GroupName="CourtesyAndFriendliness" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessPoor" runat="server" GroupName="CourtesyAndFriendliness" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_c_1"></li>
                                            <li>
                                                <input type="radio" name="in_c_1"></li>
                                            <li>
                                                <input type="radio" name="in_c_1"></li>
                                            <li>
                                                <input type="radio" name="in_c_1"></li>
                                            <li>
                                                <input type="radio" name="in_c_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Care & attention (in terms of Medication).</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionNursingExcellent" runat="server" GroupName="CareAndAttentionNursing" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionNursingGood" runat="server" GroupName="CareAndAttentionNursing" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionNursingFair" runat="server" GroupName="CareAndAttentionNursing" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionNursingAverage" runat="server" GroupName="CareAndAttentionNursing" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionNursingPoor" runat="server" GroupName="CareAndAttentionNursing" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_c_2"></li>
                                            <li>
                                                <input type="radio" name="in_c_2"></li>
                                            <li>
                                                <input type="radio" name="in_c_2"></li>
                                            <li>
                                                <input type="radio" name="in_c_2"></li>
                                            <li>
                                                <input type="radio" name="in_c_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Response to your needs.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedExcellent" runat="server" GroupName="ResponseToNeeds" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedGood" runat="server" GroupName="ResponseToNeeds" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedFair" runat="server" GroupName="ResponseToNeeds" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedAverage" runat="server" GroupName="ResponseToNeeds" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedPoor" runat="server" GroupName="ResponseToNeeds" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_c_3"></li>
                                            <li>
                                                <input type="radio" name="in_c_3"></li>
                                            <li>
                                                <input type="radio" name="in_c_3"></li>
                                            <li>
                                                <input type="radio" name="in_c_3"></li>
                                            <li>
                                                <input type="radio" name="in_c_3"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="4.">Respect for privacy and dignity.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbRespectForPrivacyExcellent" runat="server" GroupName="RepectForPrivacy" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbRespectForPrivacyGood" runat="server" GroupName="RepectForPrivacy" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectForPrivacyFair" runat="server" GroupName="RepectForPrivacy" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectForPrivacyAverage" runat="server" GroupName="RepectForPrivacy" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectForPrivacyPoor" runat="server" GroupName="RepectForPrivacy" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_c_4"></li>
                                            <li>
                                                <input type="radio" name="in_c_4"></li>
                                            <li>
                                                <input type="radio" name="in_c_4"></li>
                                            <li>
                                                <input type="radio" name="in_c_4"></li>
                                            <li>
                                                <input type="radio" name="in_c_4"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>E.</span> Diagnostics </strong></div>

        <%--<div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Courtesy & friendliness of Staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyFriendlinessStaffExcellent" runat="server" GroupName="CourtesyFriendlinessStaff" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCourtesyFriendlinessStaffGood" runat="server" GroupName="CourtesyFriendlinessStaff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyFriendlinessStaffFair" runat="server" GroupName="CourtesyFriendlinessStaff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyFriendlinessStaffAverage" runat="server" GroupName="CourtesyFriendlinessStaff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyFriendlinessStaffPoor" runat="server" GroupName="CourtesyFriendlinessStaff" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_d_1"></li>
                                            <li>
                                                <input type="radio" name="in_d_1"></li>
                                            <li>
                                                <input type="radio" name="in_d_1"></li>
                                            <li>
                                                <input type="radio" name="in_d_1"></li>
                                            <li>
                                                <input type="radio" name="in_d_1"></li>
                                        </ul>->
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Clarity of Instructions.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbClarityInstructionExcellent" runat="server" GroupName="ClarityOfInstruction" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbClarityInstructionGood" runat="server" GroupName="ClarityOfInstruction" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbClarityInstructionFair" runat="server" GroupName="ClarityOfInstruction" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbClarityInstructionAverage" runat="server" GroupName="ClarityOfInstruction" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbClarityInstructionPoor" runat="server" GroupName="ClarityOfInstruction" />
                    </li>

                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_d_2"></li>
                                            <li>
                                                <input type="radio" name="in_d_2"></li>
                                            <li>
                                                <input type="radio" name="in_d_2"></li>
                                            <li>
                                                <input type="radio" name="in_d_2"></li>
                                            <li>
                                                <input type="radio" name="in_d_2"></li>
                                        </ul>-%>
            </div>
        </div>--%>

        <p>Courtesy & friendliness of Staff. Clarity of Instructions.</p>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Pathology (Blood Collection)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbPathology_BloodCollectionExcellent" runat="server" GroupName="Pathology_BloodCollection" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbPathology_BloodCollectionGood" runat="server" GroupName="Pathology_BloodCollection" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPathology_BloodCollectionFair" runat="server" GroupName="Pathology_BloodCollection" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPathology_BloodCollectionAverage" runat="server" GroupName="Pathology_BloodCollection" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPathology_BloodCollectionPoor" runat="server" GroupName="Pathology_BloodCollection" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbRadiology_X_rayExcellent" runat="server" GroupName="Radiology_X_ray" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbRadiology_X_rayGood" runat="server" GroupName="Radiology_X_ray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRadiology_X_rayFair" runat="server" GroupName="Radiology_X_ray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRadiology_X_rayAverage" runat="server" GroupName="Radiology_X_ray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRadiology_X_rayPoor" runat="server" GroupName="Radiology_X_ray" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Cardiology (ECG, Stress test, 2D Echo, etc)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCardiology_ECG_Stress_testExcellent" runat="server" GroupName="Cardiology_ECG_Stress_test" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCardiology_ECG_Stress_testGood" runat="server" GroupName="Cardiology_ECG_Stress_test" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCardiology_ECG_Stress_testFair" runat="server" GroupName="Cardiology_ECG_Stress_test" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCardiology_ECG_Stress_testAverage" runat="server" GroupName="Cardiology_ECG_Stress_test" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCardiology_ECG_Stress_testPoor" runat="server" GroupName="Cardiology_ECG_Stress_test" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="4.">Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbNuclear_Dept_BoneScanLiverScanExcellent" runat="server" GroupName="Nuclear_Dept_BoneScanLiverScan" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbNuclear_Dept_BoneScanLiverScanGood" runat="server" GroupName="Nuclear_Dept_BoneScanLiverScan" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbNuclear_Dept_BoneScanLiverScanFair" runat="server" GroupName="Nuclear_Dept_BoneScanLiverScan" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbNuclear_Dept_BoneScanLiverScanAverage" runat="server" GroupName="Nuclear_Dept_BoneScanLiverScan" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbNuclear_Dept_BoneScanLiverScanPoor" runat="server" GroupName="Nuclear_Dept_BoneScanLiverScan" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>F.</span>  Time taken for Test & Investigation </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Pathology (Blood Collection)</div>
            <div class="fdbk_right">
                <ul>
                     <li>
                        <asp:RadioButton ID="rdbTimetakenforTestPathologyExcellent" runat="server" GroupName="TimetakenforTestPathology_BloodCollection" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestPathologyGood" runat="server" GroupName="TimetakenforTestPathology_BloodCollection" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestPathologyFair" runat="server" GroupName="TimetakenforTestPathology_BloodCollection" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestPathologyAverage" runat="server" GroupName="TimetakenforTestPathology_BloodCollection" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestPathologyPoor" runat="server" GroupName="TimetakenforTestPathology_BloodCollection" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestRadiology_X_rayExcellent" runat="server" GroupName="TimetakenforTestRadiology_X_ray" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestRadiology_X_rayGood" runat="server" GroupName="TimetakenforTestRadiology_X_ray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestRadiology_X_rayFair" runat="server" GroupName="TimetakenforTestRadiology_X_ray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestRadiology_X_rayAverage" runat="server" GroupName="TimetakenforTestRadiology_X_ray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestRadiology_X_rayPoor" runat="server" GroupName="TimetakenforTestRadiology_X_ray" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Cardiology (ECG, Stress test, 2D Echo, etc)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestCardiology_ECG_Stress_testExcellent" runat="server" GroupName="TimetakenforTestCardiology_ECG_Stress_test" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestCardiology_ECG_Stress_testGood" runat="server" GroupName="TimetakenforTestCardiology_ECG_Stress_test" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestCardiology_ECG_Stress_testFair" runat="server" GroupName="TimetakenforTestCardiology_ECG_Stress_test" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestCardiology_ECG_Stress_testAverage" runat="server" GroupName="TimetakenforTestCardiology_ECG_Stress_test" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestCardiology_ECG_Stress_testPoor" runat="server" GroupName="TimetakenforTestCardiology_ECG_Stress_test" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="4.">Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanExcellent" runat="server" GroupName="TimetakenforTestNuclear_Dept_BoneScanLiverScan" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanGood" runat="server" GroupName="TimetakenforTestNuclear_Dept_BoneScanLiverScan" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanFair" runat="server" GroupName="TimetakenforTestNuclear_Dept_BoneScanLiverScan" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanAverage" runat="server" GroupName="TimetakenforTestNuclear_Dept_BoneScanLiverScan" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforTestNuclear_Dept_BoneScanLiverScanPoor" runat="server" GroupName="TimetakenforTestNuclear_Dept_BoneScanLiverScan" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>G.</span> Food Services</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Quality of food & Beverage served.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbQualityOfFoodExcellent" runat="server" GroupName="QualityOfFood" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbQualityOfFoodGood" runat="server" GroupName="QualityOfFood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbQualityOfFoodFair" runat="server" GroupName="QualityOfFood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbQualityOfFoodAverage" runat="server" GroupName="QualityOfFood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbQualityOfFoodPoor" runat="server" GroupName="QualityOfFood" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_e_1"></li>
                                            <li>
                                                <input type="radio" name="in_e_1"></li>
                                            <li>
                                                <input type="radio" name="in_e_1"></li>
                                            <li>
                                                <input type="radio" name="in_e_1"></li>
                                            <li>
                                                <input type="radio" name="in_e_1"></li>
                                        </ul>--%>
            </div>
        </div>

  <%--      <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Variety of food served.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbVarityOfFoodExcellent" runat="server" GroupName="VarityOfFood" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbVarityOfFoodGood" runat="server" GroupName="VarityOfFood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbVarityOfFoodFair" runat="server" GroupName="VarityOfFood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbVarityOfFoodAverage" runat="server" GroupName="VarityOfFood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbVarityOfFoodPoor" runat="server" GroupName="VarityOfFood" />
                    </li>
                </ul>
            </div>
        </div>--%>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Promptness of service.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfServiceExcellent" runat="server" GroupName="PromptnesServices" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfServiceGood" runat="server" GroupName="PromptnesServices" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfServiceFair" runat="server" GroupName="PromptnesServices" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfServiceAverage" runat="server" GroupName="PromptnesServices" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfServicePoor" runat="server" GroupName="PromptnesServices" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_e_3"></li>
                                            <li>
                                                <input type="radio" name="in_e_3"></li>
                                            <li>
                                                <input type="radio" name="in_e_3"></li>
                                            <li>
                                                <input type="radio" name="in_e_3"></li>
                                            <li>
                                                <input type="radio" name="in_e_3"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="4.">Information & guidance provided by dietitian regarding your diet.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbInformationGuidanceExcellent" runat="server" GroupName="InformationGuidance" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbInformationGuidanceGood" runat="server" GroupName="InformationGuidance" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbInformationGuidanceFair" runat="server" GroupName="InformationGuidance" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbInformationGuidanceAverage" runat="server" GroupName="InformationGuidance" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbInformationGuidancePoor" runat="server" GroupName="InformationGuidance" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_e_4"></li>
                                            <li>
                                                <input type="radio" name="in_e_4"></li>
                                            <li>
                                                <input type="radio" name="in_e_4"></li>
                                            <li>
                                                <input type="radio" name="in_e_4"></li>
                                            <li>
                                                <input type="radio" name="in_e_4"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>H.</span> Housekeeping</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Cleanliness of the room, toilet & Linen.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfTheRoomExcellent" runat="server" GroupName="CleanlinessOfTheRoom" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfTheRoomGood" runat="server" GroupName="CleanlinessOfTheRoom" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfTheRoomFair" runat="server" GroupName="CleanlinessOfTheRoom" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfTheRoomAverage" runat="server" GroupName="CleanlinessOfTheRoom" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfTheRoomPoor" runat="server" GroupName="CleanlinessOfTheRoom" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_f_1"></li>
                                            <li>
                                                <input type="radio" name="in_f_1"></li>
                                            <li>
                                                <input type="radio" name="in_f_1"></li>
                                            <li>
                                                <input type="radio" name="in_f_1"></li>
                                            <li>
                                                <input type="radio" name="in_f_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Response to your needs.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedHouseExcellent" runat="server" GroupName="ResponseToNeedHouse" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedHouseGood" runat="server" GroupName="ResponseToNeedHouse" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedHouseFair" runat="server" GroupName="ResponseToNeedHouse" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedHouseAverage" runat="server" GroupName="ResponseToNeedHouse" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbResponseToNeedHousePoor" runat="server" GroupName="ResponseToNeedHouse" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_f_2"></li>
                                            <li>
                                                <input type="radio" name="in_f_2"></li>
                                            <li>
                                                <input type="radio" name="in_f_2"></li>
                                            <li>
                                                <input type="radio" name="in_f_2"></li>
                                            <li>
                                                <input type="radio" name="in_f_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Courtesy & helpfulness of the Housekeeping staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessExcellent" runat="server" GroupName="CourtesyHelpfulness" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessGood" runat="server" GroupName="CourtesyHelpfulness" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessFair" runat="server" GroupName="CourtesyHelpfulness" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessAverage" runat="server" GroupName="CourtesyHelpfulness" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessPoor" runat="server" GroupName="CourtesyHelpfulness" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_f_3"></li>
                                            <li>
                                                <input type="radio" name="in_f_3"></li>
                                            <li>
                                                <input type="radio" name="in_f_3"></li>
                                            <li>
                                                <input type="radio" name="in_f_3"></li>
                                            <li>
                                                <input type="radio" name="in_f_3"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>I.</span> Security</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Courtesy of the security guards.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesySecurityExcellent" runat="server" GroupName="CourtesySecurity" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbCourtesySecurityGood" runat="server" GroupName="CourtesySecurity" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesySecurityFair" runat="server" GroupName="CourtesySecurity" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesySecurityAverage" runat="server" GroupName="CourtesySecurity" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesySecurityPoor" runat="server" GroupName="CourtesySecurity" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_g_1"></li>
                                            <li>
                                                <input type="radio" name="in_g_1"></li>
                                            <li>
                                                <input type="radio" name="in_g_1"></li>
                                            <li>
                                                <input type="radio" name="in_g_1"></li>
                                            <li>
                                                <input type="radio" name="in_g_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Professionalism of security guards.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbProfessionalismOfSecurityExcellent" runat="server" GroupName="Professionalism" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbProfessionalismOfSecurityGood" runat="server" GroupName="Professionalism" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbProfessionalismOfSecurityFair" runat="server" GroupName="Professionalism" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbProfessionalismOfSecurityAverage" runat="server" GroupName="Professionalism" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbProfessionalismOfSecurityPoor" runat="server" GroupName="Professionalism" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_g_2"></li>
                                            <li>
                                                <input type="radio" name="in_g_2"></li>
                                            <li>
                                                <input type="radio" name="in_g_2"></li>
                                            <li>
                                                <input type="radio" name="in_g_2"></li>
                                            <li>
                                                <input type="radio" name="in_g_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>J.</span> Billing</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Billing counseling (in terms of explanation of charges/ bills)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbBillingCounselingExcellent" runat="server" GroupName="BillingCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbBillingCounselingGood" runat="server" GroupName="BillingCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbBillingCounselingFair" runat="server" GroupName="BillingCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbBillingCounselingAverage" runat="server" GroupName="BillingCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbBillingCounselingPoor" runat="server" GroupName="BillingCounseling" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_h_1"></li>
                                            <li>
                                                <input type="radio" name="in_h_1"></li>
                                            <li>
                                                <input type="radio" name="in_h_1"></li>
                                            <li>
                                                <input type="radio" name="in_h_1"></li>
                                            <li>
                                                <input type="radio" name="in_h_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Promptness of billing process.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfBillingExcellent" runat="server" GroupName="PromptnessOfBillingP" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfBillingGood" runat="server" GroupName="PromptnessOfBillingP" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfBillingFair" runat="server" GroupName="PromptnessOfBillingP" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfBillingAverage" runat="server" GroupName="PromptnessOfBillingP" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOfBillingPoor" runat="server" GroupName="PromptnessOfBillingP" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_h_2"></li>
                                            <li>
                                                <input type="radio" name="in_h_2"></li>
                                            <li>
                                                <input type="radio" name="in_h_2"></li>
                                            <li>
                                                <input type="radio" name="in_h_2"></li>
                                            <li>
                                                <input type="radio" name="in_h_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Courtesy & helpfulness of the billing staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessBillingExcellent" runat="server" GroupName="CourtesyHelpfulnessBilling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessBillingGood" runat="server" GroupName="CourtesyHelpfulnessBilling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessBillingFair" runat="server" GroupName="CourtesyHelpfulnessBilling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessBillingAverage" runat="server" GroupName="CourtesyHelpfulnessBilling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyHelpfulnessBillingPoor" runat="server" GroupName="CourtesyHelpfulnessBilling" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_h_3"></li>
                                            <li>
                                                <input type="radio" name="in_h_3"></li>
                                            <li>
                                                <input type="radio" name="in_h_3"></li>
                                            <li>
                                                <input type="radio" name="in_h_3"></li>
                                            <li>
                                                <input type="radio" name="in_h_3"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>K.</span> Discharge</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Promptness of discharge process.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOFDischageExcellent" runat="server" GroupName="PromptnessOFDischage" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOFDischageGood" runat="server" GroupName="PromptnessOFDischage" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOFDischageFair" runat="server" GroupName="PromptnessOFDischage" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOFDischageAverage" runat="server" GroupName="PromptnessOFDischage" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbPromptnessOFDischagePoor" runat="server" GroupName="PromptnessOFDischage" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_i_1"></li>
                                            <li>
                                                <input type="radio" name="in_i_1"></li>
                                            <li>
                                                <input type="radio" name="in_i_1"></li>
                                            <li>
                                                <input type="radio" name="in_i_1"></li>
                                            <li>
                                                <input type="radio" name="in_i_1"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Discharge counseling (In terms of Post discharge care & follow up)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbDischargeCounselingExcellent" runat="server" GroupName="DischargeCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDischargeCounselingGood" runat="server" GroupName="DischargeCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDischargeCounselingFair" runat="server" GroupName="DischargeCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDischargeCounselingAverage" runat="server" GroupName="DischargeCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbDischargeCounselingPoor" runat="server" GroupName="DischargeCounseling" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_i_2"></li>
                                            <li>
                                                <input type="radio" name="in_i_2"></li>
                                            <li>
                                                <input type="radio" name="in_i_2"></li>
                                            <li>
                                                <input type="radio" name="in_i_2"></li>
                                            <li>
                                                <input type="radio" name="in_i_2"></li>
                                        </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Convenience of discharge process.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConvenienceOfExcellent" runat="server" GroupName="ConvenienceCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConvenienceOfGood" runat="server" GroupName="ConvenienceCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConvenienceOfFair" runat="server" GroupName="ConvenienceCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConvenienceOfAverage" runat="server" GroupName="ConvenienceCounseling" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConvenienceOfPoor" runat="server" GroupName="ConvenienceCounseling" />
                    </li>
                </ul>
                <%--<ul>
                                            <li>
                                                <input type="radio" name="in_i_3"></li>
                                            <li>
                                                <input type="radio" name="in_i_3"></li>
                                            <li>
                                                <input type="radio" name="in_i_3"></li>
                                            <li>
                                                <input type="radio" name="in_i_3"></li>
                                            <li>
                                                <input type="radio" name="in_i_3"></li>
                                        </ul>--%>
            </div>
        </div>


        <div class="clear" style="height: 20px;"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">What did you like the most about Lilavati hospital?</label>
            <asp:TextBox ID="txtWhatDidYouLikeTheMost" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">What did you like the least about Lilavati hospital?</label>
            <asp:TextBox ID="txtWhatDidYouLikeTheLeast" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">Any suggestion you would like to put forward which will help us to serve you better</label>
            <asp:TextBox ID="txtAnySuggestion" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">Would you like to recommend any staff for their outstanding Performance? Please specify name & reason.</label>
            <asp:TextBox ID="txtWouldYouLikeToRecommend" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>

        <p><strong>I hereby give my consent to the hospital for publishing/ producing/ utilizing my feedback for service improvement/ promotion/ advertisement purpose.</strong></p>


        <div class="clear"></div>

        <div class="form_centering_div">
            <p align="center"><strong class="green_text">It was a pleasure caring for you. Get well soon.</strong></p>
            <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" OnClick="btnSubmit_Click" OnClientClick='javascript:return CheckSave();' class="submit_btn fade_anim uppercase" title="#in_validate" />
            <%--<input class="submit_btn fade_anim uppercase" type="submit" value="SUBMIT" />--%>
        </div>
        <hr style="margin-top:30px" />
        <p>For grievances write to us on <a href="mailto:complaints@lilavatihospital.com">complaints@lilavatihospital.com</a></p>

        <div id="apply_form1" style="display: none;" class="fancybox">
            <div class="form_apply_main">
                <div class="apply_form_tl uppercase green_text">
                    Thank You!!
                </div>
                <div class="clear">
                </div>
                <div class="form_centering_div" style="font-size: 12px; line-height: 18px; color: #000; border-top: 1px solid #ccc; padding: 10px 15px 15px; text-align: center; background: #eee;">
                    Your Information Has Been Submitted.                
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        //$('.submit_btn').on('click', function (event) {
        //    var target = $($(this).attr('title'));
        //    if (target.length) {
        //        event.preventDefault();
        //        $('html, body').animate({
        //            scrollTop: target.offset().top = '120'
        //        }, 1000);
        //    }
        //});
    </script>

</div>
