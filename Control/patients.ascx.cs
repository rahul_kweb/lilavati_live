﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_patients : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

        if (PageName == "Doctor")
        {
            idfind_a_doctor.Attributes.Add("class", "active");
        }
        else if (PageName == "Inpatients")
        {
            idinpatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Empanelled-Corporates")
        {
            idempanelled_companies.Attributes.Add("class", "active");
        }
        else if (PageName == "Tariff")
        {
            idtariff.Attributes.Add("class", "active");
        }
        else if (PageName == "International-Patients")
        {
            idinternational_patients.Attributes.Add("class", "active");
        }
        else if (PageName == "Patients-Education-Brochure")
        {
            idpatients_education_brochure.Attributes.Add("class", "active");
        }
        else if (PageName == "Feedback")
        {
            idfeedback.Attributes.Add("class", "active");
        }
        else if (PageName == "Empanelled-Insurance-Co-TPAs")
        {
            idEmpanelled_Insurance_Co_TPAs.Attributes.Add("class", "active");
        }
        else if (PageName == "Floor-Directory")
        {
            idFloor_Directory.Attributes.Add("class", "active");
        }
        else if (PageName == "Accommodation")
        {
            idaccommodation.Attributes.Add("class", "active");
        }
        else if (PageName == "Patients-Rights-Responsibilities")
        {
            idPatients_Rights_Responsibilities.Attributes.Add("class", "active");
        }
    }
}