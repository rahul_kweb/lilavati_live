﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="feedback_OutPatient.ascx.cs" Inherits="Control_feedback_OutPatient" %>

<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('.fancybox').fancybox();

    });
</script>

<script type="text/javascript">
    function ShowPopupOut() {
        $(function () {
            //$("#apply_form1").html(message);
            //$("#apply_form1").dialog({
            //    title: "Feedback In-Patient",
            //    buttons: {
            //        Close: function () {
            //            $(this).dialog('close');
            //        }
            //    },
            //    modal: true
            //});                       

            $("#apply_form2").fancybox().trigger('click');

        });
    };

    //$(document).ready(function () {
    //    $("#apply_form2").fancybox().trigger('click');       
    //});

</script>

<script>
    $(function () {
        $(".date11").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "c-100:c",
            dateFormat: "dd-mm-yy"
        });


        $('#<%=txtDate11.ClientID%>').change(function () {
            console.log("datattatt");
            var data = $('#<%=txtDate11.ClientID%>').val();
            if (data.length != 0 || data != "") {

                $("#date11").removeClass("error");
                $("#date11").addClass("populated");
            }
            else {
                $("#date11").addClass("error");
            }
        });

        $('.date11').keypress(function (event) {
            event.preventDefault();
        });

    });
</script>

<script type="text/javascript">
    function CheckSaveOut() {
        var FirstName = $('#<%=txtFirstName.ClientID%>').val();
        var LastName = $('#<%=txtLastName.ClientID%>').val();
        var LHNo = $('#<%=txtLHNo.ClientID%>').val();
        var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
        var EmailID = $('#<%=txtEmailID.ClientID%>').val();
        var NameOfDoctor = $('#<%=txtNameOfDoctor.ClientID%>').val();
        var date11 = $('#<%=txtDate11.ClientID%>').val();
        var blank = false;

        if (FirstName == '') {
            $("#FirstName1").addClass("error");
            blank = true;
        }
        else {
            $("#FirstName1").removeClass("error");
        }

        if (LastName == '') {
            $("#LastName1").addClass("error");
            blank = true;
        }
        else {
            $("#LastName1").removeClass("error");
        }
        if (LHNo == '') {
            $("#LHNo1").addClass("error");
            blank = true;
        }
        else {
            $("#LHNo1").removeClass("error");
        }
        if (ContactNo == '') {
            $("#ContactNo1").addClass("error");
            blank = true;
        }
        else {
            if (ContactNo.length < 10) {
                $("#ContactNo1").addClass("error");
                blank = true;
            }
            else {
                $("#ContactNo1").removeClass("error");
            }
        }
        if (EmailID == '') {
            $("#EmailID1").addClass("error");
            blank = true;
        }
        else {
            var EmailText1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText1.test(EmailID)) {
                $("#EmailID1").addClass("error");
                alert("Valid Email ID");
                blank = true;
            }
            else {
                $("#EmailID1").removeClass("error");
            }
        }
        if (NameOfDoctor == '') {
            $("#NameOfDoctor1").addClass("error");
            blank = true;
        }
        else {
            $("#NameOfDoctor1").removeClass("error");
        }

        if (date11 == '') {
            $("#date11").addClass("error");
            blank = true;
        }
        else {
            $("#date11").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }

    }
</script>

<script>

    function FirstNameFun1() {
        var FirstName = $('#<%=txtFirstName.ClientID%>').val();
        var blank = false;

        if (FirstName == '') {
            $("#FirstName1").addClass("error");
            blank = true;
        }
        else {
            $("#FirstName1").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function LastNAmeFun1() {
        var LastName = $('#<%=txtLastName.ClientID%>').val();
        var blank = false;

        if (LastName == '') {
            $("#LastName1").addClass("error");
            blank = true;
        }
        else {
            $("#LastName1").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }

    }

    function LhnoFun1() {
        var LHNo = $('#<%=txtLHNo.ClientID%>').val();
        var blank = false;

        if (LHNo == '') {
            $("#LHNo1").addClass("error");
            blank = true;
        }
        else {
            $("#LHNo1").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function ContactFun1() {
        var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
        var blank = false;

        if (ContactNo == '') {
            $("#ContactNo1").addClass("error");
            blank = true;
        }
        else {
            if (ContactNo.length < 10) {
                $("#ContactNo1").addClass("error");
                blank = true;
            }
            else {
                $("#ContactNo1").removeClass("error");
            }
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }

    function EmailFun1() {
        var EmailId = $('#<%=txtEmailID.ClientID%>').val();
        var blank = false;

        if (EmailId == '') {
            $("#EmailID1").addClass("error");
            blank = true;
        }
        else {
            var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText.test(EmailId)) {
                $("#EmailID1").addClass("error");
                alert("Valid Email ID");
                blank = true;
            }
            else {
                $("#EmailID1").removeClass("error");
            }
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }


    function NameOfDoc1() {
        var NameOfD = $('#<%=txtNameOfDoctor.ClientID%>').val();
        var blank = false;

        if (NameOfD == '') {
            $("#NameOfDoctor1").addClass("error");
            blank = true;
        }
        else {
            $("#NameOfDoctor1").removeClass("error");
        }

        if (blank) {
            return false;
        }
        else {
            return true;
        }
    }


    $(document).ready(function () {

        /** First Name **/
        $(".Fn1").click(function () {
            FirstNameFun1();
        });
        $(".Fn1").keyup(function () {
            FirstNameFun1();
        });

        /** LAst Name **/
        $(".Ln1").click(function () {
            LastNAmeFun1();
        });
        $(".Ln1").keyup(function () {
            LastNAmeFun1();
        });

        /** LHNO **/
        $(".Lh1").click(function () {
            LhnoFun1();
        });
        $(".Lh1").keyup(function () {
            LhnoFun1();
        });

        /** Contact No **/
        $(".Cn1").click(function () {
            ContactFun1();
        });
        $(".Cn1").keyup(function () {
            ContactFun1();
        });

        /** Email ID **/
        $(".EI1").click(function () {
            EmailFun1();
        });

        $(".EI1").change(function () {
            EmailFun1();
        });

        /** Name Of Doc **/
        $(".ND").click(function () {
            NameOfDoc1();
        });
        $(".ND").keyup(function () {
            NameOfDoc1();
        });

    });


</script>

<script type="text/javascript">
    function onlyNumbers(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>

<script>
    $(document).ready(function () {

        $('#ctl00_ContentPlaceHolder1_OutPatinet_btnSubmitOut').click(function (event) {
            var FirstName = $('#<%=txtFirstName.ClientID%>').val();
            var LastName = $('#<%=txtLastName.ClientID%>').val();
            var LHNo = $('#<%=txtLHNo.ClientID%>').val();
            var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
            var EmailID = $('#<%=txtEmailID.ClientID%>').val();
            var NameOfDoctor = $('#<%=txtNameOfDoctor.ClientID%>').val();
            var date11 = $('#<%=txtDate11.ClientID%>').val();

            if (FirstName != '' && LastName != '' && LHNo != '' && ContactNo.length == '10' && EmailID != '' && NameOfDoctor != '' && date11 != '') {

                $('#out_validate').hide();
                var EmailText1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText1.test(EmailID)) {
                    console.log("email if");
                    var target = $($(this).attr('title'));
                    if (target.length) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top = '120'
                        }, 1000);
                    }
                }
                else {
                    console.log("email if else");
                    $('#out_validate').hide();
                }

                //$('#out_validate').hide();
                //console.log(ContactNo.length);
            }
            else {
                //console.log(ContactNo.length);
                $('#out_validate').show();

                var target = $($(this).attr('title'));
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top = '120'
                    }, 1000);
                }
            }

        });


        //  $('.commonclass1').keypress(function () {
        //     var FirstName = $('#<%=txtFirstName.ClientID%>').val();
        //        var LastName = $('#<%=txtLastName.ClientID%>').val();
        //       var LHNo = $('#<%=txtLHNo.ClientID%>').val();
        //       var ContactNo = $('#<%=txtContactNo.ClientID%>').val();
        //     var EmailID = $('#<%=txtEmailID.ClientID%>').val();
        //     var NameOfDoctor = $('#<%=txtNameOfDoctor.ClientID%>').val();
        //
        //     if (FirstName != '' && LastName != '' && LHNo != '' && ContactNo.length == '10' && NameOfDoctor != '') {
        //         $('#out_validate').hide();
        //         var EmailText1 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        //
        //          if (!EmailText1.test(EmailID)) {
        //              console.log("email if2");
        //              $('#out_validate').show();
        //          }
        //          else {
        //              console.log("email if else2");
        //              $('#out_validate').hide();
        //          }
        //      }
        //      else {
        //
        //          $('#out_validate').show();
        //      }
        //  });

    });
</script>



<div class="tab_content">

    <div class="feedback_form FlowupLabels">

        <div class="validate_msg" id="out_validate" style="display: none;">Fields marked in red are complusory.</div>

        <div class="form_field fl_wrap" id="FirstName1">
            <label class="fl_label">First Name</label>
            <asp:TextBox ID="txtFirstName" runat="server" class="fl_input Fn1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="LastName1">
            <label class="fl_label">Last Name</label>
            <asp:TextBox ID="txtLastName" runat="server" class="fl_input Ln1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="LHNo1">
            <label class="fl_label">LH No.</label>
            <asp:TextBox ID="txtLHNo" runat="server" class="fl_input Lh1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="NameOfDoctor1">
            <label class="fl_label">Name of Doctor</label>
            <asp:TextBox ID="txtNameOfDoctor" runat="server" class="fl_input ND commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="ContactNo1">
            <label class="fl_label">Contact No.</label>
            <asp:TextBox ID="txtContactNo" runat="server" class="fl_input Cn1 commonclass1" MaxLength="10" onKeyPress='javascript:return onlyNumbers();'></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="EmailID1">
            <label class="fl_label">Email ID</label>
            <asp:TextBox ID="txtEmailID" runat="server" class="fl_input EI1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap" id="date11">
            <label class="fl_label">Date</label>
            <asp:TextBox ID="txtDate11" runat="server" class="fl_input date11 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear" style="height: 20px;"></div>

        <p><strong class="green_text uppercase">Guidelines </strong></p>
        <ul class="bullet">
            <li>To rate the following services that you received during your visit kindly tick in the box that represents your opinion. In case you did not receive any particular service; skip to the next question.</li>
            <li>For rating Fair and below; we request you to give your suggestion for improvement.</li>
        </ul>

        <p><strong class="green_text uppercase">How did you come to know about Lilavati Hospital? </strong></p>

        <asp:CheckBoxList ID="chkHowDidYouComeToKnow" runat="server" class="ez_checkbox fdbk_know">
            <asp:ListItem>Lilavati Hospital Doctor</asp:ListItem>
            <asp:ListItem>Reference of family doctor</asp:ListItem>
            <asp:ListItem>Corporate Tie Up</asp:ListItem>
            <asp:ListItem>Website</asp:ListItem>
            <asp:ListItem>Hospital employee</asp:ListItem>
            <asp:ListItem>Relative/ Friend</asp:ListItem>
            <asp:ListItem>Any Other</asp:ListItem>
        </asp:CheckBoxList>

        <%--<p class="ez_checkbox fdbk_know">
            <input type="checkbox" id="out_know1">
            <label for="out_know1">Lilavati Hospital Doctor </label>
            <br>
            <input type="checkbox" id="out_know2">
            <label for="out_know2">Reference of family doctor </label>
            <br>
            <input type="checkbox" id="out_know3">
            <label for="out_know3">Corporate Tie Up </label>
            <br>
            <input type="checkbox" id="out_know4">
            <label for="out_know4">Website </label>
            <br>
            <input type="checkbox" id="out_know5">
            <label for="out_know5">Hospital employee </label>
            <br>
            <input type="checkbox" id="out_know6">
            <label for="out_know6">Relative/ Friend </label>
            <br>
            <input type="checkbox" id="out_know7">
            <label for="out_know7">Any Other </label>
        </p>--%>

        <div class="clear"></div>

        <div class="fdbk_box">
            <div class="fdbk_left"></div>
            <div class="fdbk_right green_text uppercase">
                <ul>
                    <li>Excellent</li>
                    <li>Good</li>
                    <li>Fair</li>
                    <li>Average</li>
                    <li>Poor</li>
                </ul>
            </div>
        </div>

                <div class="fdbk_subtl green_text uppercase"><strong><span>A.</span> Overall Experience</strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">How would you rate your overall experience at Lilavati Hospital.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateExcellent" runat="server" GroupName="HowWouldYouRate" />
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateGood" runat="server" GroupName="HowWouldYouRate" />
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateFair" runat="server" GroupName="HowWouldYouRate" />
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRateAverage" runat="server" GroupName="HowWouldYouRate" />
                    <li>
                        <asp:RadioButton ID="rdbHowWouldYouRatePoor" runat="server" GroupName="HowWouldYouRate" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_j_1"></li>
                    <li>
                        <input type="radio" name="out_j_1"></li>
                    <li>
                        <input type="radio" name="out_j_1"></li>
                    <li>
                        <input type="radio" name="out_j_1"></li>
                    <li>
                        <input type="radio" name="out_j_1"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>B.</span> Appointment </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Time taken by appointment staff to give an appointment for Doctor/ service.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenByAppointmentExcellent" runat="server" GroupName="TimeTakenByAppointment" />
                        <%--<input type="radio" name="out_a_1">--%></li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenByAppointmentGood" runat="server" GroupName="TimeTakenByAppointment" />
                        <%--<input type="radio" name="out_a_1">--%></li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenByAppointmentFair" runat="server" GroupName="TimeTakenByAppointment" />
                        <%--<input type="radio" name="out_a_1">--%></li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenByAppointmentAverage" runat="server" GroupName="TimeTakenByAppointment" />
                        <%--<input type="radio" name="out_a_1">--%></li>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenByAppointmentPoor" runat="server" GroupName="TimeTakenByAppointment" />
                        <%--<input type="radio" name="out_a_1">--%></li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Guidance and information provided by the staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbGuidanceAndInformationExcellent" runat="server" GroupName="GuidanceAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbGuidanceAndInformationGood" runat="server" GroupName="GuidanceAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbGuidanceAndInformationFair" runat="server" GroupName="GuidanceAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbGuidanceAndInformationAverage" runat="server" GroupName="GuidanceAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbGuidanceAndInformationPoor" runat="server" GroupName="GuidanceAndInformation" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_a_2"></li>
                    <li>
                        <input type="radio" name="out_a_2"></li>
                    <li>
                        <input type="radio" name="out_a_2"></li>
                    <li>
                        <input type="radio" name="out_a_2"></li>
                    <li>
                        <input type="radio" name="out_a_2"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Courtesy and friendliness of OPD staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessOPDExcellent" runat="server" GroupName="CourtesyAndFriendlinessOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessOPDGood" runat="server" GroupName="CourtesyAndFriendlinessOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessOPDFair" runat="server" GroupName="CourtesyAndFriendlinessOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessOPDAverage" runat="server" GroupName="CourtesyAndFriendlinessOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessOPDPoor" runat="server" GroupName="CourtesyAndFriendlinessOPD" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_a_3"></li>
                    <li>
                        <input type="radio" name="out_a_3"></li>
                    <li>
                        <input type="radio" name="out_a_3"></li>
                    <li>
                        <input type="radio" name="out_a_3"></li>
                    <li>
                        <input type="radio" name="out_a_3"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>C.</span> Registration & Billing </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Time taken to complete the registration process.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteRegistrationExcellent" runat="server" GroupName="TimeTakenToCompleteRegistration" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteRegistrationGood" runat="server" GroupName="TimeTakenToCompleteRegistration" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteRegistrationFair" runat="server" GroupName="TimeTakenToCompleteRegistration" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteRegistrationAverage" runat="server" GroupName="TimeTakenToCompleteRegistration" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteRegistrationPoor" runat="server" GroupName="TimeTakenToCompleteRegistration" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_b_1"></li>
                    <li>
                        <input type="radio" name="out_b_1"></li>
                    <li>
                        <input type="radio" name="out_b_1"></li>
                    <li>
                        <input type="radio" name="out_b_1"></li>
                    <li>
                        <input type="radio" name="out_b_1"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Time taken to complete the billing process.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteBillingExcellent" runat="server" GroupName="TimeTakenToCompleteBilling" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteBillingGood" runat="server" GroupName="TimeTakenToCompleteBilling" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteBillingFair" runat="server" GroupName="TimeTakenToCompleteBilling" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteBillingAverage" runat="server" GroupName="TimeTakenToCompleteBilling" />
                    <li>
                        <asp:RadioButton ID="rdbTimeTakenToCompleteBillingPoor" runat="server" GroupName="TimeTakenToCompleteBilling" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_b_2"></li>
                    <li>
                        <input type="radio" name="out_b_2"></li>
                    <li>
                        <input type="radio" name="out_b_2"></li>
                    <li>
                        <input type="radio" name="out_b_2"></li>
                    <li>
                        <input type="radio" name="out_b_2"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Courtesy and information provided by OPD staff.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndInformationExcellent" runat="server" GroupName="CourtesyAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndInformationGood" runat="server" GroupName="CourtesyAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndInformationFair" runat="server" GroupName="CourtesyAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndInformationAverage" runat="server" GroupName="CourtesyAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndInformationPoor" runat="server" GroupName="CourtesyAndInformation" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_b_3"></li>
                    <li>
                        <input type="radio" name="out_b_3"></li>
                    <li>
                        <input type="radio" name="out_b_3"></li>
                    <li>
                        <input type="radio" name="out_b_3"></li>
                    <li>
                        <input type="radio" name="out_b_3"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>D.</span> Doctor </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Waiting time to see the doctor after completing registration/ billing formalities.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeToSeeDoctorExcellent" runat="server" GroupName="WaitingTimeToSeeDoctor" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeToSeeDoctorGood" runat="server" GroupName="WaitingTimeToSeeDoctor" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeToSeeDoctorFair" runat="server" GroupName="WaitingTimeToSeeDoctor" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeToSeeDoctorAverage" runat="server" GroupName="WaitingTimeToSeeDoctor" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeToSeeDoctorPoor" runat="server" GroupName="WaitingTimeToSeeDoctor" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_c_1"></li>
                    <li>
                        <input type="radio" name="out_c_1"></li>
                    <li>
                        <input type="radio" name="out_c_1"></li>
                    <li>
                        <input type="radio" name="out_c_1"></li>
                    <li>
                        <input type="radio" name="out_c_1"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Clarity & information shared by the doctor about your health & line of treatment.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbClarityAndInformationExcellent" runat="server" GroupName="ClarityAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbClarityAndInformationGood" runat="server" GroupName="ClarityAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbClarityAndInformationFair" runat="server" GroupName="ClarityAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbClarityAndInformationAverage" runat="server" GroupName="ClarityAndInformation" />
                    <li>
                        <asp:RadioButton ID="rdbClarityAndInformationPoor" runat="server" GroupName="ClarityAndInformation" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_c_2"></li>
                    <li>
                        <input type="radio" name="out_c_2"></li>
                    <li>
                        <input type="radio" name="out_c_2"></li>
                    <li>
                        <input type="radio" name="out_c_2"></li>
                    <li>
                        <input type="radio" name="out_c_2"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Care & attention given by the doctor.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionExcellent" runat="server" GroupName="CareAndAttention" />
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionGood" runat="server" GroupName="CareAndAttention" />
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionFair" runat="server" GroupName="CareAndAttention" />
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionAverage" runat="server" GroupName="CareAndAttention" />
                    <li>
                        <asp:RadioButton ID="rdbCareAndAttentionPoor" runat="server" GroupName="CareAndAttention" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_c_3"></li>
                    <li>
                        <input type="radio" name="out_c_3"></li>
                    <li>
                        <input type="radio" name="out_c_3"></li>
                    <li>
                        <input type="radio" name="out_c_3"></li>
                    <li>
                        <input type="radio" name="out_c_3"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>E.</span> Nursing Care </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Behavior & attitude of nurse.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbBehaviorAndAttitudeExcellent" runat="server" GroupName="BehaviorAndAttitude" />
                    <li>
                        <asp:RadioButton ID="rdbBehaviorAndAttitudeGood" runat="server" GroupName="BehaviorAndAttitude" />
                    <li>
                        <asp:RadioButton ID="rdbBehaviorAndAttitudeFair" runat="server" GroupName="BehaviorAndAttitude" />
                    <li>
                        <asp:RadioButton ID="rdbBehaviorAndAttitudeAverage" runat="server" GroupName="BehaviorAndAttitude" />
                    <li>
                        <asp:RadioButton ID="rdbBehaviorAndAttitudePoor" runat="server" GroupName="BehaviorAndAttitude" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_d_1"></li>
                    <li>
                        <input type="radio" name="out_d_1"></li>
                    <li>
                        <input type="radio" name="out_d_1"></li>
                    <li>
                        <input type="radio" name="out_d_1"></li>
                    <li>
                        <input type="radio" name="out_d_1"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Respect for your privacy.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbRespectForYourPrivacyExcellent" runat="server" GroupName="RespectForYourPrivacy" />
                    <li>
                        <asp:RadioButton ID="rdbRespectForYourPrivacyGood" runat="server" GroupName="RespectForYourPrivacy" />
                    <li>
                        <asp:RadioButton ID="rdbRespectForYourPrivacyFair" runat="server" GroupName="RespectForYourPrivacy" />
                    <li>
                        <asp:RadioButton ID="rdbRespectForYourPrivacyAverage" runat="server" GroupName="RespectForYourPrivacy" />
                    <li>
                        <asp:RadioButton ID="rdbRespectForYourPrivacyPoor" runat="server" GroupName="RespectForYourPrivacy" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_d_2"></li>
                    <li>
                        <input type="radio" name="out_d_2"></li>
                    <li>
                        <input type="radio" name="out_d_2"></li>
                    <li>
                        <input type="radio" name="out_d_2"></li>
                    <li>
                        <input type="radio" name="out_d_2"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Care offered during procedure/ Investigation by nurse.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCareOfferedDuringProcExcellent" runat="server" GroupName="CareOfferedDuringProc" />
                    <li>
                        <asp:RadioButton ID="rdbCareOfferedDuringProcGood" runat="server" GroupName="CareOfferedDuringProc" />
                    <li>
                        <asp:RadioButton ID="rdbCareOfferedDuringProcFair" runat="server" GroupName="CareOfferedDuringProc" />
                    <li>
                        <asp:RadioButton ID="rdbCareOfferedDuringProcAverage" runat="server" GroupName="CareOfferedDuringProc" />
                    <li>
                        <asp:RadioButton ID="rdbCareOfferedDuringProcPoor" runat="server" GroupName="CareOfferedDuringProc" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_d_3"></li>
                    <li>
                        <input type="radio" name="out_d_3"></li>
                    <li>
                        <input type="radio" name="out_d_3"></li>
                    <li>
                        <input type="radio" name="out_d_3"></li>
                    <li>
                        <input type="radio" name="out_d_3"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>F.</span> Test & Investigation </strong></div>

            <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Courtesy and friendliness of technical staff, Clarity of instruction</div>
            <div class="fdbk_right">
             <%--   <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffExcellent" runat="server" GroupName="CourtesyAndFriendlinessStaff" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffGood" runat="server" GroupName="CourtesyAndFriendlinessStaff" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffFair" runat="server" GroupName="CourtesyAndFriendlinessStaff" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffAverage" runat="server" GroupName="CourtesyAndFriendlinessStaff" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffPoor" runat="server" GroupName="CourtesyAndFriendlinessStaff" />
                </ul>--%>
            </div>
        </div>

              <div class="fdbk_box">
            <div class="fdbk_left" data-no="a."> Pathology (Blood/Sample Collection)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffPathologyExcellent" runat="server" GroupName="CourtesyAndFriendlinessStaffPathology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffPathologyGood" runat="server" GroupName="CourtesyAndFriendlinessStaffPathology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffPathologyFair" runat="server" GroupName="CourtesyAndFriendlinessStaffPathology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffPathologyAverage" runat="server" GroupName="CourtesyAndFriendlinessStaffPathology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffPathologyPoor" runat="server" GroupName="CourtesyAndFriendlinessStaffPathology" />
                </ul>
            </div>
        </div>
           <div class="fdbk_box">
            <div class="fdbk_left" data-no="b."> Radiology (X-ray, Sonography/Mammo, CT Scan, MRI etc.)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffRadiologyExcellent" runat="server" GroupName="CourtesyAndFriendlinessStaffRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffRadiologyGood" runat="server" GroupName="CourtesyAndFriendlinessStaffRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffRadiologyFair" runat="server" GroupName="CourtesyAndFriendlinessStaffRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffRadiologyAverage" runat="server" GroupName="CourtesyAndFriendlinessStaffRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffRadiologyPoor" runat="server" GroupName="CourtesyAndFriendlinessStaffRadiology" />
                </ul>
            </div>
        </div>
            <div class="fdbk_box">
            <div class="fdbk_left" data-no="c."> Cardiology (ECG, Stress Test 2D Echo etc.)</div>
            <div class="fdbk_right">
                <ul>
                <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffCardiologyExcellent" runat="server" GroupName="CourtesyAndFriendlinessStaffCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffCardiologyGood" runat="server" GroupName="CourtesyAndFriendlinessStaffCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffCardiologyFair" runat="server" GroupName="CourtesyAndFriendlinessStaffCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffCardiologyAverage" runat="server" GroupName="CourtesyAndFriendlinessStaffCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffCardiologyPoor" runat="server" GroupName="CourtesyAndFriendlinessStaffCardiology" />
                </ul>
            </div>
        </div>
           <div class="fdbk_box">
            <div class="fdbk_left" data-no="d."> Nuclear Dept. (PET CT, SPECT CT etc.)</div>
            <div class="fdbk_right">
                <ul>
                <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffNuclearExcellent" runat="server" GroupName="CourtesyAndFriendlinessStaffNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffNuclearGood" runat="server" GroupName="CourtesyAndFriendlinessStaffNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffNuclearFair" runat="server" GroupName="CourtesyAndFriendlinessStaffNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffNuclearAverage" runat="server" GroupName="CourtesyAndFriendlinessStaffNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffNuclearPoor" runat="server" GroupName="CourtesyAndFriendlinessStaffNuclear" />
                </ul>
            </div>
        </div>

         <div class="fdbk_box">
            <div class="fdbk_left" data-no="e. "> Other: <asp:TextBox ID="txtCourtesyAndFriendlinessStafothers" runat="server" Style="width:200px;border:0;border-bottom:1px solid #ccc"></asp:TextBox></div>
            <div class="fdbk_right">
                <ul>
                <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffOtherExcellent" runat="server" GroupName="CourtesyAndFriendlinessStaffOther" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffOtherGood" runat="server" GroupName="CourtesyAndFriendlinessStaffOther" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffOtherFair" runat="server" GroupName="CourtesyAndFriendlinessStaffOther" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffOtherAverage" runat="server" GroupName="CourtesyAndFriendlinessStaffOther" />
                    <li>
                        <asp:RadioButton ID="rdbCourtesyAndFriendlinessStaffOtherPoor" runat="server" GroupName="CourtesyAndFriendlinessStaffOther" />
                </ul>
            </div>
        </div>


        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Waiting time for the test after completing registration / Billing formalities.</div>
            <div class="fdbk_right">
                <%--<ul>
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestExcellent" runat="server" GroupName="WaitingTimeForTheTest" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestGood" runat="server" GroupName="WaitingTimeForTheTest" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestFair" runat="server" GroupName="WaitingTimeForTheTest" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestAverage" runat="server" GroupName="WaitingTimeForTheTest" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestPoor" runat="server" GroupName="WaitingTimeForTheTest" />
                </ul>--%>
            </div>
        </div>

         <div class="fdbk_box">
            <div class="fdbk_left" data-no="a."> Pathology (Blood/Sample Collection)</div>
            <div class="fdbk_right">
                <ul>
                     <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestPathologyExcellent" runat="server" GroupName="WaitingTimeForTheTestPathology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestPathologyGood" runat="server" GroupName="WaitingTimeForTheTestPathology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestPathologyFair" runat="server" GroupName="WaitingTimeForTheTestPathology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestPathologyAverage" runat="server" GroupName="WaitingTimeForTheTestPathology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestPathologyPoor" runat="server" GroupName="WaitingTimeForTheTestPathology" />
                </ul>
            </div>
        </div>
           <div class="fdbk_box">
            <div class="fdbk_left" data-no="b."> Radiology (X-ray, Sonography/Mammo, CT Scan, MRI etc.)</div>
            <div class="fdbk_right">
                <ul>
                <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestRadiologyExcellent" runat="server" GroupName="WaitingTimeForTheTestRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestRadiologyGood" runat="server" GroupName="WaitingTimeForTheTestRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestRadiologyFair" runat="server" GroupName="WaitingTimeForTheTestRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestRadiologyAverage" runat="server" GroupName="WaitingTimeForTheTestRadiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestRadiologyPoor" runat="server" GroupName="WaitingTimeForTheTestRadiology" />
                </ul>
            </div>
        </div>
            <div class="fdbk_box">
            <div class="fdbk_left" data-no="c."> Cardiology (ECG, Stress Test 2D Echo etc.)</div>
            <div class="fdbk_right">
                <ul>
                 <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestCardiologyExcellent" runat="server" GroupName="WaitingTimeForTheTestCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestCardiologyGood" runat="server" GroupName="WaitingTimeForTheTestCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestCardiologyFair" runat="server" GroupName="WaitingTimeForTheTestCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestCardiologyAverage" runat="server" GroupName="WaitingTimeForTheTestCardiology" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestCardiologyPoor" runat="server" GroupName="WaitingTimeForTheTestCardiology" />
                </ul>
            </div>
        </div>
           <div class="fdbk_box">
            <div class="fdbk_left" data-no="d."> Nuclear Dept. (PET CT, SPECT CT etc.)</div>
            <div class="fdbk_right">
                <ul>
                  <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestNuclearExcellent" runat="server" GroupName="WaitingTimeForTheTestNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestNuclearGood" runat="server" GroupName="WaitingTimeForTheTestNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestNuclearFair" runat="server" GroupName="WaitingTimeForTheTestNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestNuclearAverage" runat="server" GroupName="WaitingTimeForTheTestNuclear" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestNuclearPoor" runat="server" GroupName="WaitingTimeForTheTestNuclear" />
                </ul>
            </div>
        </div>

         <div class="fdbk_box">
            <div class="fdbk_left" data-no="e. "> Other: <asp:TextBox ID="txtrdbWaitingTimeForTheTestOthers" runat="server" Style="width:200px;border:0;border-bottom:1px solid #ccc"></asp:TextBox> </div>          
            <div class="fdbk_right">
                <ul>
               <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestOtherExcellent" runat="server" GroupName="WaitingTimeForTheTestOther" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestOtherGood" runat="server" GroupName="WaitingTimeForTheTestOther" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestOtherFair" runat="server" GroupName="WaitingTimeForTheTestOther" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestOtherAverage" runat="server" GroupName="WaitingTimeForTheTestOther" />
                    <li>
                        <asp:RadioButton ID="rdbWaitingTimeForTheTestOtherPoor" runat="server" GroupName="WaitingTimeForTheTestOther" />
                </ul>
            </div>
        </div>

   <%--     <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Experience with technical staff (Doctor, Technician) during test & Investigation (in terms of guidance, behavior & coutesy)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbExperienceWithTechnicalExcellent" runat="server" GroupName="ExperienceWithTechnical" />
                    <li>
                        <asp:RadioButton ID="rdbExperienceWithTechnicalGood" runat="server" GroupName="ExperienceWithTechnical" />
                    <li>
                        <asp:RadioButton ID="rdbExperienceWithTechnicalFair" runat="server" GroupName="ExperienceWithTechnical" />
                    <li>
                        <asp:RadioButton ID="rdbExperienceWithTechnicalAverage" runat="server" GroupName="ExperienceWithTechnical" />
                    <li>
                        <asp:RadioButton ID="rdbExperienceWithTechnicalPoor" runat="server" GroupName="ExperienceWithTechnical" />
                </ul>
          
            </div>
        </div>--%>

    

        <div class="fdbk_subtl green_text uppercase"><strong><span>G.</span> Comfort of OPD premises </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Comfort of waiting area.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbComfortOfWaitingAreaExcellent" runat="server" GroupName="ComfortOfWaitingArea" />
                    <li>
                        <asp:RadioButton ID="rdbComfortOfWaitingAreaGood" runat="server" GroupName="ComfortOfWaitingArea" />
                    <li>
                        <asp:RadioButton ID="rdbComfortOfWaitingAreaFair" runat="server" GroupName="ComfortOfWaitingArea" />
                    <li>
                        <asp:RadioButton ID="rdbComfortOfWaitingAreaAverage" runat="server" GroupName="ComfortOfWaitingArea" />
                    <li>
                        <asp:RadioButton ID="rdbComfortOfWaitingAreaPoor" runat="server" GroupName="ComfortOfWaitingArea" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_f_1"></li>
                    <li>
                        <input type="radio" name="out_f_1"></li>
                    <li>
                        <input type="radio" name="out_f_1"></li>
                    <li>
                        <input type="radio" name="out_f_1"></li>
                    <li>
                        <input type="radio" name="out_f_1"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Cleanliness in the OPD area.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessInOPDExcellent" runat="server" GroupName="CleanlinessInOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessInOPDGood" runat="server" GroupName="CleanlinessInOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessInOPDFair" runat="server" GroupName="CleanlinessInOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessInOPDAverage" runat="server" GroupName="CleanlinessInOPD" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessInOPDPoor" runat="server" GroupName="CleanlinessInOPD" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_f_2"></li>
                    <li>
                        <input type="radio" name="out_f_2"></li>
                    <li>
                        <input type="radio" name="out_f_2"></li>
                    <li>
                        <input type="radio" name="out_f_2"></li>
                    <li>
                        <input type="radio" name="out_f_2"></li>
                </ul>--%>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Cleanliness of toilets.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfToiletsExcellent" runat="server" GroupName="CleanlinessOfToilets" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfToiletsGood" runat="server" GroupName="CleanlinessOfToilets" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfToiletsFair" runat="server" GroupName="CleanlinessOfToilets" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfToiletsAverage" runat="server" GroupName="CleanlinessOfToilets" />
                    <li>
                        <asp:RadioButton ID="rdbCleanlinessOfToiletsPoor" runat="server" GroupName="CleanlinessOfToilets" />
                </ul>
                <%--<ul>
                    <li>
                        <input type="radio" name="out_f_3"></li>
                    <li>
                        <input type="radio" name="out_f_3"></li>
                    <li>
                        <input type="radio" name="out_f_3"></li>
                    <li>
                        <input type="radio" name="out_f_3"></li>
                    <li>
                        <input type="radio" name="out_f_3"></li>
                </ul>--%>
            </div>
        </div>


        <div class="clear" style="height: 20px;"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">What did you like the most about Lilavati hospital?</label>
            <asp:TextBox ID="txtWhatDidYouLIke" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">What did you like the least about Lilavati hospital?</label>
            <asp:TextBox ID="txtWhatDidYouLikeLeast" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">Any suggestion you would like to put forward which will help us to serve you better</label>
            <asp:TextBox ID="txtAnysuggestionYouWouldLike" runat="server" class="fl_input" TextMode="MultiLine"></asp:TextBox>
            <%--<textarea class="fl_input"></textarea>--%>
        </div>
        <div class="clear"></div>
         <div class="fdbk_box">
            <div class="" data-no="">Would you like to mention of our services for it's outstanding quality and / recommend any staff for their outstading Performance?</div>
        </div>

        <div class="clear"></div>
        
        <div class="form_field fl_wrap">
            <label class="fl_label">Service</label>
            <asp:TextBox ID="txtService" runat="server" class="fl_input Fn1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap">
            <label class="fl_label">Reason</label>
            <asp:TextBox ID="txtServiceReason" runat="server" class="fl_input Ln1 commonclass1" TextMode="MultiLine"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap">
            <label class="fl_label">Staff</label>
            <asp:TextBox ID="txtStaff" runat="server" class="fl_input Lh1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap">
            <label class="fl_label">Area of Work</label>
            <asp:TextBox ID="txtAreaofWork" runat="server" class="fl_input ND commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

          <div class="clear"></div>
         <div class="form_field fl_wrap">
            <label class="fl_label">Reason</label>
            <asp:TextBox ID="txtAreaReason" runat="server" class="fl_input Ln1 commonclass1" TextMode="MultiLine"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>
        <div class="clear" style="height: 20px;"></div>

                        <p><strong>I hereby give my consent to the hospital for publishing/ producing/ utilizing my feedback for service improvement/ promotion/ advertisement purpose.</strong></p>

              <div class="clear"></div>

        <div class="form_centering_div">
            <p align="center"><strong class="green_text">Thank you for your co-operation.</strong></p>
            <asp:Button ID="btnSubmitOut" runat="server" Text="SUBMIT" OnClick="btnSubmitOut_Click" OnClientClick='javascript:return CheckSaveOut();' class="submit_btn fade_anim uppercase" title="#out_validate" />
            <%--<input class="submit_btn fade_anim uppercase" type="submit" value="SUBMIT" />--%>

                 <hr style="margin-top:30px" />
        <p>For grievances write to us on <a href="mailto:complaints@lilavatihospital.com">complaints@lilavatihospital.com</a></p>
        </div>


        <div id="apply_form2" style="display: none;" class="fancybox">
            <div class="form_apply_main">
                <div class="apply_form_tl uppercase green_text">
                    Thank You!!
                </div>
                <div class="clear">
                </div>
                <div class="form_centering_div" style="font-size: 12px; line-height: 18px; color: #000; border-top: 1px solid #ccc; padding: 10px 15px 15px; text-align: center; background: #eee;">
                    Your Information Has Been Submitted. 
                </div>
            </div>
        </div>


    </div>

</div>
