﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_webviewheader : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

        string PageName2 = System.IO.Path.GetDirectoryName(Request.Path);

        /*** About US ***/

        if (PageName == "Introduction")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Founder")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Board-of-Trustees")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Management")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Mission-Motto")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Awards-Accreditations")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Media-Press-Releases")
        {
            idAboutUs.Attributes.Add("class", "active");
        }
        else if (PageName == "Statutory-Compilance")
        {
            idAboutUs.Attributes.Add("class", "active");
        }

         /*** Services ***/

        else if (PageName == "Centres-of-Excellence")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Out-Patient")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Critical-Care")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Emergency-Trauma")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Day-Care")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Dialysis-Centre")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Physiotherapy")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Diagnostics")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Blood-Bank")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Pharmacy")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Ambulance")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Health-Checkup")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Visa-Investigation")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Social-Initiative")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "packages_details")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Interventional_Radiology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Nuclear_Medicine")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "lactation")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Interventional-Neuroradiology")
        {
            idSevices.Attributes.Add("class", "active");
        }  

        /*** Visitors ***/

        else if (PageName == "Visiting-Hours")
        {
            idVisitors.Attributes.Add("class", "active");
        }
        else if (PageName == "Convenience-Facilities")
        {
            idVisitors.Attributes.Add("class", "active");
        }
        else if (PageName == "Dos-Donts")
        {
            idVisitors.Attributes.Add("class", "active");
        }
        else if (PageName == "Visitors-Policy")
        {
            idVisitors.Attributes.Add("class", "active");
        }

        /*** Contact US ***/

        else if (PageName == "Contact-Us")
        {
            idContactUs.Attributes.Add("class", "active");
        }

        /*** Professionals ***/

        else if (PageName == "DNB")
        {
            idProfessionals.Attributes.Add("class", "active");
        }
        else if (PageName == "Research")
        {
            idProfessionals.Attributes.Add("class", "active");
        }
        else if (PageName == "LHMT")
        {
            idProfessionals.Attributes.Add("class", "active");
        }
        else if (PageName == "CME")
        {
            idProfessionals.Attributes.Add("class", "active");
        }
        else if (PageName == "Careers")
        {
            idProfessionals.Attributes.Add("class", "active");
        }
        else if (PageName == "Nurses")
        {
            idProfessionals.Attributes.Add("class", "active");
        }
        else if (PageName == "Doctors")
        {
            idProfessionals.Attributes.Add("class", "active");
        }

        /*** COE Page ***/

        else if (PageName == "Anesthesiology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Audiology-Speech-Therapy")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Bariatric-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Cardiology-CVTS")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Chest-Medicine")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Colorectal-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Dental")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Dermo-Cosmetology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Dermo-Cosmetology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Ent")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Gastroenterology-Gastrointestinal-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "General-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Gynaecology-Obstetrics")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Haematology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Headache-Migraine-Clinic")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Hip-Replacement")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Infectious-Diseases")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Internal-Medicine")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Minimally-Invasive-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Nephrology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Neurology-Neuro-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Oncology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Ophthalmology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Orthopaedics")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Pediatrics-And-Paediatric-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Plastic-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Psychiatry-Psychology-Neuropsychology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Rheumatology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Sleep-Medicine")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Urology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Vascular-Endovascular-Surgery")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Pain-Management")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Hair-Transplant")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Diabetology-Endocrinology")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "Corneal-Transplant")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName == "liver-transplant")
        {
            idSevices.Attributes.Add("class", "active");
        } 
        

        /*** End COE ***/

        /*** Patients  ***/
        else if (PageName == "Doctor")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "doctor_profile")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Inpatients")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Empanelled-Corporates")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Tariff")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "International-Patients")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Patients-Education-Brochure")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Feedback")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Empanelled-Insurance-Co-TPAs")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Floor-Directory")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Accommodation")
        {
            idPatients.Attributes.Add("class", "active");
        }
        else if (PageName == "Patients-Rights-Responsibilities")
        {
            idPatients.Attributes.Add("class", "active");
        }
       

        if (PageName2 == "\\Packages-Details")
        {
            idSevices.Attributes.Add("class", "active");
        }
        else if (PageName2 == "\\Doctorprofile")
        {
            idPatients.Attributes.Add("class", "active");
        }


        else if (PageName == "Home")
        {
            idhome.Attributes.Add("class", "active");
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string key = txtSearch.Text.Trim();
        key = key.Replace("Dr.", "").Trim();
        if (key != "")
        {
            Response.Redirect("~/Search_Results/" + key);
        }
        else
        {
        }


       
    }
}