﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_visitors : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

        if (PageName == "Visiting-Hours")
        {
            idvisiting_hours.Attributes.Add("class", "active");
        }
        else if (PageName == "Convenience-Facilities")
        {
            idconvenience_facilities.Attributes.Add("class", "active");
        }
        else if (PageName == "Dos-Donts")
        {
            iddos_donts.Attributes.Add("class", "active");
        }
        else if (PageName == "Visitors-Policy")
        {
            idvisitors_policy.Attributes.Add("class", "active");
        }
    }
}