﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="webviewfooter.ascx.cs" Inherits="Control_webviewfooter" %>
<div class="foot_col" style="display:none;">
    <ul class="foot_links">
        <li><a href="/Ambulance">Ambulance</a></li>
        <li><a href="/Convenience-Facilities">Convenience & Facilities</a></li>
        <li><a href="/Donate-Blood">Donate Blood</a></li>
    </ul>
</div>
<div class="foot_col" style="display:none;">
    <ul class="foot_links">
        <li><a href="/Floor-Directory">Floor Directory</a></li>
        <li><a href="/Media-Press-Releases">Media & Press Releases</a></li>
        <li><a href="/Photo-Gallery">Photo Gallery</a></li>
    </ul>
</div>
<div class="foot_col" style="display:none;">
    <ul class="foot_links">
        <li><a href="/Pledge-Your-Eyes">Pledge Your Eyes</a></li>
        <li><a href="/Social-Initiative">Social Initiatives</a></li>
        <li><a href="/Statutory-Compilance">Statutory Compliance</a></li>
    </ul>
</div>
<%--<div class="foot_col social_icons">
    <ul class="foot_links no_anim">
        <li><a href="https://www.facebook.com/lilavatihospital1978/" target="_blank" title="Facebook" class="fb"><i class="fa fa-facebook"></i></a></li>
        <li><a href="https://twitter.com/LilavatiHRC" title="Twitter" target="_blank" class="tw"><i class="fa fa-twitter"></i></a></li>       
        <li><a href="https://www.instagram.com/lilavati.hospital/" target="_blank" title="Instagram" class="in"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://www.linkedin.com/in/lilavati-hospital-035540125?trk=hp-identity-name" target="_blank" title="LinkedIn" class="ln"><i class="fa fa-linkedin"></i></a></li>
    </ul>
</div>--%>
<div class="foot_col app_box" style="display:none;">
    Download Our Mobile App
    <div class="app_btns">
        <a href="#" class="app_links"><i class="fa fa-android"></i>Get it on Google Play
        </a><a href="#" class="app_links"><i class="fa fa-apple"></i>Get it on App Store
        </a>
    </div>
</div>
<div class="foot_col copy">
    &copy;
    <%=DateTime.Now.Year.ToString() %>. All rights reserved.<br>
    <a href="http://www.kwebmaker.com" target="_blank">Kwebmaker&trade;</a>
</div>
<div class="clear">
</div>
