﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_about_us_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);
        //Response.Write(PageName);

        if (PageName == "Introduction")
        {
            idintroduction.Attributes.Add("class", "active");
        }
        else if (PageName == "Founder")
        {
            idour_founders.Attributes.Add("class", "active");
        }
        else if (PageName == "Board-of-Trustees")
        {
            idboard_of_trustees.Attributes.Add("class", "active");
        }
        else if (PageName == "Management")
        {
            idmanagement.Attributes.Add("class", "active");
        }
        else if (PageName == "Mission-Motto")
        {
            idmission_motto.Attributes.Add("class", "active");
        }
        else if (PageName == "Awards-Accreditations")
        {
            idawards_accreditations.Attributes.Add("class", "active");
        }
        else if (PageName == "Media-Press-Releases")
        {
            idmedia_press_releases.Attributes.Add("class", "active");
        }
        //else if (PageName == "awards_1")
        //{
        //    idawards_accreditations.Attributes.Add("class", "active");
        //}
        else if (PageName == "Statutory-Compilance")
        {
            idstatutory_compilance.Attributes.Add("class", "active");
        }

    }
}