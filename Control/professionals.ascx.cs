﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_professionals : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

        if (PageName == "DNB")
        {
            iddnb.Attributes.Add("class", "active");
        }
        else if (PageName == "Research")
        {
            idresearch.Attributes.Add("class", "active");
        }
        else if (PageName == "LHMT")
        {
            idlhmt.Attributes.Add("class", "active");
        }
        else if (PageName == "CME")
        {
            idcme.Attributes.Add("class", "active");
        }
        else if (PageName == "Careers")
        {
            idcareers.Attributes.Add("class", "active");
        }
        else if (PageName == "Nurses")
        {
            idnureses.Attributes.Add("class", "active");
        }
        else if (PageName == "Doctors")
        {
            //iddoctors.Attributes.Add("class", "active");
        }
        else if (PageName == "muhs_Fellowship")
        {
            idMUHS.Attributes.Add("class", "active");
        }
    }
}