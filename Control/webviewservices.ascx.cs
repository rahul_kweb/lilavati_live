﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_webviewservices : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string PageName = System.IO.Path.GetFileNameWithoutExtension(Request.Path);

        if (PageName == "web-Centres-of-Excellence")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Out-Patient")
        {
            idout_patient.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Critical-Care")
        {
            idcritical_care.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Emergency-Trauma")
        {
            idemergency_trauma.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Day-Care")
        {
            idday_care.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Dialysis-Centre")
        {
            iddialysis_centre.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Physiotherapy")
        {
            idphysiotherapy.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Diagnostics")
        {
            iddiagnostics.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Blood-Bank")
        {
            idblood_bank.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Pharmacy")
        {
            idpharmacy.Attributes.Add("class", "active");
        }
        else if (PageName == "Web-Ambulance")
        {
            idambulance.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Health-Checkup")
        {
            idhealth_checkup.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Visa-Investigation")
        {
            idvisa_investigation.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Social-Initiative")
        {
            idsocial_initiative.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Interventional_Radiology")
        {
            idinterventional_radiology.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Nuclear_Medicine")
        {
            //idnuclear_medicine.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Interventional-Neuroradiology")
        {
            idinterventional_neuroradiology.Attributes.Add("class", "active");
        }


        /*** COE Page ***/

        else if (PageName == "web-Anesthesiology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Audiology-Speech-Therapy")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Bariatric-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Cardiology-CVTS")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Chest-Medicine")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Colorectal-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Dental")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Dermo-Cosmetology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Dermo-Cosmetology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Ent")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Gastroenterology-Gastrointestinal-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-General-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Gynaecology-Obstetrics")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Haematology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Headache-Migraine-Clinic")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Hip-Replacement")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Infectious-Diseases")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Internal-Medicine")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Minimally-Invasive-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Nephrology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Neurology-Neuro-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Oncology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Ophthalmology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Orthopaedics")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Pediatrics-And-Paediatric-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Plastic-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Psychiatry-Psychology-Neuropsychology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Rheumatology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Sleep-Medicine")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Urology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Vascular-Endovascular-Surgery")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Pain-Management")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Diabetology-Endocrinology")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Hair-Transplant")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-Corneal-Transplant")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-lactation")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        }
        else if (PageName == "web-liver-transplant")
        {
            idcentres_of_excellence.Attributes.Add("class", "active");
        } 

        /*** End COE ***/




    }
}