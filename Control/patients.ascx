﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="patients.ascx.cs" Inherits="Control_patients" %>
<div class="rel_menu fade_anim">
    <ul>
        <li id="idaccommodation" runat="server"><a href="Accommodation">Accommodation </a></li>
        <li id="idempanelled_companies" runat="server"><a href="Empanelled-Corporates">Empanelled Corporates </a></li>
        <li id="idEmpanelled_Insurance_Co_TPAs" runat="server"><a href="Empanelled-Insurance-Co-TPAs">Empanelled Insurance Co. & TPA's</a></li>
        <li id="idfeedback" runat="server"><a href="Feedback">Feedback</a></li>
        <li id="idSuggestions_Complaints" runat="server"><a href="Suggestions-Complaints">Suggestions/Complaints</a></li>
        <li id="idfind_a_doctor" runat="server"><a href="Doctor">Find a Doctor</a></li>
        <li id="idFloor_Directory" runat="server"><a href="Floor-Directory">Floor Directory</a></li>
        <li id="idinpatients" runat="server"><a href="Inpatients">Inpatients</a></li>
        <li id="idinternational_patients" runat="server"><a href="International-Patients">International
            Patients</a></li>
        <li id="idpatients_education_brochure" runat="server"><a href="Patients-Education-Brochure">Patients Education Brochure</a></li>
        <li id="idPatients_Rights_Responsibilities" runat="server"><a href="Patients-Rights-Responsibilities">Patients Rights & Responsibilities</a></li>
        <li id="idtariff" runat="server"><a href="Tariff">Tariff</a></li>
    </ul>
</div>
<div class="ad_box">
    <a href="Health-Checkup?tab_list=0">
<%--        <img src="images/ad_img.jpg">--%>
        <%--<a href="/sample-collection-faq"><img src="images/ad_img_new.jpg" style="width:100%"></a>--%>
        <a href="/sample-collection-faq"><img src="images/adv_service.jpg" style="width:100%"></a>
    </a>
</div>
