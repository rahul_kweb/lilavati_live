﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="webviewheader.ascx.cs" Inherits="Control_webviewheader" %>
<div class="header_top">
    <div class="nabh">
        <img src="/images/nabh_logo_sm.png">
    </div>
    <div class="emergency_nos">
        <div class="el">
            <strong>Emergency</strong>
            <br />
            +91 (22) 2656 8063 / 64
        </div>
        <div class="bl">
            <strong>Boardline</strong>
            <br />
            +91 (22) 2675 1000 /<br class="br_adjust" />
            +91 (22) 2656 8000
        </div>
    </div>
    <div class="clear" id="sticky-anchor">
    </div>
</div>
<div class="fixing_head">
    <div class="navigation">
        <div class="menu-btn menu_open" style="display:none;">
            <img src="/images/menu.png" alt="" title="Menu" style="display: block;" />
        </div>
        <div class="nav pushy pushy-right">
            <div class="menu-btn menu_close">
                X
            </div>
            <div class="language" id="google_translate_element">
            </div>
            <div class="search_box" style="display:none">
                <%-- <input type="text" class="search_type" placeholder="Search"><input type="button"
                    class="search_btn">--%>
                <asp:TextBox ID="txtSearch" runat="server" placeholder="Search" class="search_type" autocomplete="off"></asp:TextBox>
                <asp:Button ID="btnSearch" runat="server" class="search_btn" OnClick="btnSearch_Click" />
            </div>
            <ul class="menu" style="display:none;">
                <li id="idhome" runat="server"><a href="/Home"> <i class="fa fa-home" style="font-size:16px;"></i><span>Home</span></a></li>
                <li id="idAboutUs" runat="server"><a href="#">About Us</a>
                    <ul>                       
                        <li><a href="/Introduction">Introduction</a></li>
                        <li><a href="/Founder">The Founder</a></li>
                        <li><a href="/Board-of-Trustees">Board of Trustees</a></li>
                        <li><a href="/Management">Management</a></li>
                        <li><a href="/Mission-Motto">Mission & Motto</a></li>
                        <li><a href="/Awards-Accreditations">Awards & Accreditations</a></li>
                        <li><a href="/Media-Press-Releases">Media/ Press Release</a></li>
                        <li><a href="/Statutory-Compilance">Statutory Compliance</a></li>
                    </ul>
                </li>
                <li id="idSevices" runat="server"><a href="#">Services</a>
                    <ul class="col_2">
                        <li><a href="/Ambulance">Ambulance</a></li>
                        <li><a href="/Blood-Bank">Blood Bank</a></li>
                        <li><a href="/Centres-of-Excellence">Centres of Excellence</a></li>
                        <li><a href="/Critical-Care">Critical Care</a></li>
                        <li><a href="/Day-Care">Day Care</a></li>
                        <li><a href="/Diagnostics">Diagnostics</a></li>
                        <li><a href="/Dialysis-Centre">Dialysis Centre</a></li>
                        <li><a href="/Emergency-Trauma">Emergency/ Trauma</a></li>
                        <li><a href="/Health-Checkup">Health Check Up</a></li>
                        <li><a href="/Interventional-Neuroradiology">Interventional Neuroradiology</a></li>
                        <li><a href="/Interventional_Radiology">Interventional Radiology</a></li>
                        <li><a href="/Out-Patient">Out Patient</a></li>
                        <li><a href="/Pharmacy">Pharmacy/ Chemist</a></li>
                        <li><a href="/Physiotherapy">Physiotherapy</a></li>
                        <%--<li><a href="/Nuclear_Medicine">Nuclear Medicine</a></li>--%>
                        <li><a href="/Social-Initiative?tab_list=0">Social Initiatives</a></li>
                        <li><a href="/Visa-Investigation">Visa Investigation</a></li>

                    </ul>
                </li>
                <li id="idPatients" runat="server"><a href="#">Patients</a>
                    <ul class="col_2">
                        <li><a href="/Accommodation">Accommodation</a></li>
                        <li><a href="/Empanelled-Corporates">Empanelled Corporates</a></li>
                        <li><a href="/Empanelled-Insurance-Co-TPAs">Empanelled Insurance Co. & TPA's</a></li>
                        <li><a href="/Feedback">Feedback</a></li>
                        <li><a href="/Doctor">Find a Doctor</a></li>
                        <li><a href="/Floor-Directory">Floor Directory</a></li>
                        <li><a href="/Inpatients">Inpatients</a></li>
                        <li><a href="/International-Patients">International Patients</a></li>
                        <li><a href="/Patients-Education-Brochure">Patients Education Brochure</a></li>
                        <li><a href="/Patients-Rights-Responsibilities">Patients Rights & Responsibilities</a></li>
                        <li><a href="/Tariff">Tariff</a></li>
                    </ul>
                </li>
                <li id="idVisitors" runat="server"><a href="#">Visitors</a>
                    <ul>
                        <li><a href="/Visiting-Hours">Visiting Hours</a></li>
                        <li><a href="/Convenience-Facilities">Convenience & Facilities</a></li>
                        <li><a href="/Dos-Donts">Do's & Don'ts</a></li>
                        <li><a href="/Visitors-Policy">Visitors Policy</a></li>
                    </ul>
                </li>
                <li id="idProfessionals" runat="server"><a href="#">Professionals</a>
                    <ul>
                        <%--<li><a href="/Doctors">Doctors</a></li>--%>
                        <li><a href="/DNB">DNB</a></li>
                        <li><a href="/CME">CME</a></li>
                        <li><a href="/Nurses">Nurses</a></li>
                        <li><a href="/Research">Research</a></li>
                        <li><a href="/LHMT">LHMT</a></li>

                        <li><a href="/Careers">Careers</a></li>
                    </ul>
                </li>
                <li id="idContactUs" runat="server"><a href="/Contact-Us">Contact Us</a></li>
            </ul>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="logo_box">
        <a href="/Home">
            <img src="/images/logo.gif" class="logo">
        </a>
        <div class="clear">
        </div>
    </div>
</div>
<div class="clear">
</div>
