﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="about_us_menu.ascx.cs"
    Inherits="Control_about_us_menu" %>
<div class="rel_menu fade_anim">
    <ul>
        <li id="idintroduction" runat="server"><a href="Introduction">Introduction</a></li>
        <li id="idour_founders" runat="server"><a href="Founder">The Founder</a></li>
        <li id="idboard_of_trustees" runat="server"><a href="Board-of-Trustees">Board of Trustees</a></li>
        <li id="idmanagement" runat="server"><a href="Management">Management</a></li>
        <li id="idmission_motto" runat="server"><a href="Mission-Motto">Mission & Motto</a></li>
        <li id="idawards_accreditations" runat="server"><a href="Awards-Accreditations">Awards
            & Accreditations</a></li>
        <li id="idmedia_press_releases" runat="server"><a href="Media-Press-Releases">Media/
            Press Release</a></li>
        <li id="idstatutory_compilance" runat="server"><a href="Statutory-Compilance">Statutory
            Compliance</a></li>
    </ul>
</div>
<div class="ad_box">
    <img src="images/cthc.jpg">
</div>
