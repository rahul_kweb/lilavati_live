﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="visitors.ascx.cs" Inherits="Control_visitors" %>
<div class="rel_menu fade_anim">
    <ul>
        <li id="idvisiting_hours" runat="server"><a href="Visiting-Hours">Visiting Hours</a></li>
        <li id="idconvenience_facilities" runat="server"><a href="Convenience-Facilities">Convenience
            & Facilities</a></li>
        <li id="iddos_donts" runat="server"><a href="Dos-Donts">Do's & Don'ts</a></li>
        <%-- <li id="idvisitors_policy" runat="server"><a href="Visitors-Policy">Visitors Policy</a></li> --%>
		<li id="idvisitors_policy" runat="server"><a href="javascript:void(0)">Visitors Policy</a></li>
    </ul>
</div>
<div class="ad_box">
    <a href="Health-Checkup?tab_list=0">
        <img src="images/ad_img.jpg">
    </a>
</div>
