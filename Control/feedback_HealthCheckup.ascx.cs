﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Control_feedback_HealthCheckup : System.Web.UI.UserControl
{
    FeedBackHealthCheckUp tblFeedBackHealthCheckUp = new FeedBackHealthCheckUp();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void btnSubmit2_Click(object sender, EventArgs e)
    {
        try
        {
            tblFeedBackHealthCheckUp.FirstName = txtFirstName2.Text.Trim();
            tblFeedBackHealthCheckUp.LastName = txtLastName2.Text.Trim();
            tblFeedBackHealthCheckUp.Lh_No = txtLHNO2.Text.Trim();           
            tblFeedBackHealthCheckUp.Date = txtDate2.Text.Trim();
            tblFeedBackHealthCheckUp.Contact = txtContactNo2.Text.Trim();
            tblFeedBackHealthCheckUp.EmailId = txtEmailID2.Text.Trim();
            //tblFeedBackHealthCheckUp.HealthCheckup = txtHealthCheckup2.Text.Trim();

            tblFeedBackHealthCheckUp.Service = txtService.Text.Trim();
            tblFeedBackHealthCheckUp.ServiceReason = txtServiceReason.Text.Trim();
            tblFeedBackHealthCheckUp.StaffPerformance = txtStaff.Text.Trim();
            tblFeedBackHealthCheckUp.AreaofWork = txtAreaofWork.Text.Trim();
            tblFeedBackHealthCheckUp.AreaReason = txtAreaReason.Text.Trim();


            if (ddlHealthCheckup.SelectedValue.Trim() == "Corporate")
            {
                tblFeedBackHealthCheckUp.HealthCheckup = txtCorporate.Text.Trim();
            }
            else
            {
                tblFeedBackHealthCheckUp.HealthCheckup = ddlHealthCheckup.SelectedValue.Trim();
            }

           


            string chk = string.Empty;
            for (int i = 0; chkHowDidYouCome.Items.Count > i; i++)
            {
                if (chkHowDidYouCome.Items[i].Selected == true)
                {
                    chk += chkHowDidYouCome.Items[i].Value + ",";
                }
            }
            if (chk != "")
            {
                chk = chk.Substring(0, chk.Length - 1);
            }
            tblFeedBackHealthCheckUp.HowDidYouComeToKnow = chk;


            /** How would you rate the way your appointment call was handled? **/

            if (rdbAppointmentHowwouldyourateExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.AppointmentHowwouldyourate = "Excellent";
            }
            if (rdbAppointmentHowwouldyourateGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.AppointmentHowwouldyourate = "Good";
            }
            if (rdbAppointmentHowwouldyourateFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.AppointmentHowwouldyourate = "Fair";
            }
            if (rdbAppointmentHowwouldyourateAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.AppointmentHowwouldyourate = "Average";
            }
            if (rdbAppointmentHowwouldyouratePoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.AppointmentHowwouldyourate = "Poor";
            }

            /** How was your document formalities and queries handled during Registration and payment? **/

            if (rdbRegistrationHowwasyourdocumentExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument = "Excellent";
            }
            if (rdbRegistrationHowwasyourdocumentGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument = "Good";
            }
            if (rdbRegistrationHowwasyourdocumentFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument = "Fair";
            }
            if (rdbRegistrationHowwasyourdocumentAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument = "Average";
            }
            if (rdbRegistrationHowwasyourdocumentPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument = "Poor";
            }

            /** Pathology (Blood Collection) **/

            if (rdbTestsPathologyBloodExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsPathologyBlood = "Excellent";
            }
            if (rdbTestsPathologyBloodGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsPathologyBlood = "Good";
            }
            if (rdbTestsPathologyBloodFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsPathologyBlood = "Fair";
            }
            if (rdbTestsPathologyBloodAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsPathologyBlood = "Average";
            }
            if (rdbTestsPathologyBloodPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsPathologyBlood = "Poor";
            }

            /** Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.) **/

            if (rdbTestsRadiology_XrayExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsRadiology_Xray = "Excellent";
            }
            if (rdbTestsRadiology_XrayGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsRadiology_Xray = "Good";
            }
            if (rdbTestsRadiology_XrayFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsRadiology_Xray = "Fair";
            }
            if (rdbTestsRadiology_XrayAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsRadiology_Xray = "Average";
            }
            if (rdbTestsRadiology_XrayPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsRadiology_Xray = "Poor";
            }

            /** Cardiology (ECG, Stress test, 2D Echo, etc)  **/

            if (rdbTestsCardiologyECGExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsCardiologyECG = "Excellent";
            }
            if (rdbTestsCardiologyECGGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsCardiologyECG = "Good";
            }
            if (rdbTestsCardiologyECGFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsCardiologyECG = "Fair";
            }
            if (rdbTestsCardiologyECGAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsCardiologyECG = "Average";
            }
            if (rdbTestsCardiologyECGPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsCardiologyECG = "Poor";
            }

            /** Spirometry **/

            if (rdbTestsSpirometryExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsSpirometry = "Excellent";
            }
            if (rdbTestsSpirometryGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsSpirometry = "Good";
            }
            if (rdbTestsSpirometryFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsSpirometry = "Fair";
            }
            if (rdbTestsSpirometryAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsSpirometry = "Average";
            }
            if (rdbTestsSpirometryPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.TestsSpirometry = "Poor";
            }

            /** General Physician **/

            if (rdbConsultationGeneralPhysicianExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGeneralPhysician = "Excellent";
            }
            if (rdbConsultationGeneralPhysicianGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGeneralPhysician = "Good";
            }
            if (rdbConsultationGeneralPhysicianFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGeneralPhysician = "Fair";
            }
            if (rdbConsultationGeneralPhysicianAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGeneralPhysician = "Average";
            }
            if (rdbConsultationGeneralPhysicianPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGeneralPhysician = "Poor";
            }

            /** Gynaec/ Surgeon **/

            if (rdbConsultationGynaecExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGynaec = "Excellent";
            }
            if (rdbConsultationGynaecGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGynaec = "Good";
            }
            if (rdbConsultationGynaecFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGynaec = "Fair";
            }
            if (rdbConsultationGynaecAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGynaec = "Average";
            }
            if (rdbConsultationGynaecPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationGynaec = "Poor";
            }

            /** Ophthalmology **/

            if (rdbConsultationOphthalmologyExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationOphthalmology = "Excellent";
            }
            if (rdbConsultationOphthalmologyGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationOphthalmology = "Good";
            }
            if (rdbConsultationOphthalmologyFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationOphthalmology = "Fair";
            }
            if (rdbConsultationOphthalmologyAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationOphthalmology = "Average";
            }
            if (rdbConsultationOphthalmologyPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationOphthalmology = "Poor";
            }

            /** ENT **/

            if (rdbConsultationENTExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationENT = "Excellent";
            }
            if (rdbConsultationENTGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationENT = "Good";
            }
            if (rdbConsultationENTFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationENT = "Fair";
            }
            if (rdbConsultationENTAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationENT = "Average";
            }
            if (rdbConsultationENTPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationENT = "Poor";
            }


            /** Dental **/

            if (rdbConsultationDentalExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDental = "Excellent";
            }
            if (rdbConsultationDentalGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDental = "Good";
            }
            if (rdbConsultationDentalFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDental = "Fair";
            }
            if (rdbConsultationDentalAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDental = "Average";
            }
            if (rdbConsultationDentalPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDental = "Poor";
            }

            /** Skin **/

            if (rdbConsultationSkinExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationSkin = "Excellent";
            }
            if (rdbConsultationSkinGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationSkin = "Good";
            }
            if (rdbConsultationSkinFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationSkin = "Fair";
            }
            if (rdbConsultationSkinAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationSkin = "Average";
            }
            if (rdbConsultationSkinPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationSkin = "Poor";
            }

            /** Dietition **/

            if (rdbConsultationDietitionExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDietition = "Excellent";
            }
            if (rdbConsultationDietitionGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDietition = "Good";
            }
            if (rdbConsultationDietitionFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDietition = "Fair";
            }
            if (rdbConsultationDietitionAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDietition = "Average";
            }
            if (rdbConsultationDietitionPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.ConsultationDietition = "Poor";
            }

            /** Housekeeping **/

            if (rdbHousekeepingOverallhygieneExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.HousekeepingOverallhygiene = "Excellent";
            }
            if (rdbHousekeepingOverallhygieneGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.HousekeepingOverallhygiene = "Good";
            }
            if (rdbHousekeepingOverallhygieneFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.HousekeepingOverallhygiene = "Fair";
            }
            if (rdbHousekeepingOverallhygieneAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.HousekeepingOverallhygiene = "Average";
            }
            if (rdbHousekeepingOverallhygienePoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.HousekeepingOverallhygiene = "Poor";
            }

            /** Staff **/

            if (rdbStaffExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.Staff = "Excellent";
            }
            if (rdbStaffGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.Staff = "Good";
            }
            if (rdbStaffFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.Staff = "Fair";
            }
            if (rdbStaffAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.Staff = "Average";
            }
            if (rdbStaffPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.Staff = "Poor";
            }

            /** Food & Beverages **/

            if (rdbFoodBeveragesExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.FoodBeverages = "Excellent";
            }
            if (rdbFoodBeveragesGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.FoodBeverages = "Good";
            }
            if (rdbFoodBeveragesFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.FoodBeverages = "Fair";
            }
            if (rdbFoodBeveragesAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.FoodBeverages = "Average";
            }
            if (rdbFoodBeveragesPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.FoodBeverages = "Poor";
            }

            /** Respect for your privacy. **/

            if (rdbRespectforyourExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.Respectforyour = "Excellent";
            }
            if (rdbRespectforyourGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.Respectforyour = "Good";
            }
            if (rdbRespectforyourFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.Respectforyour = "Fair";
            }
            if (rdbRespectforyourAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.Respectforyour = "Average";
            }
            if (rdbRespectforyourPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.Respectforyour = "Poor";
            }


            /** Time taken for health check up as against your expectation. **/

            if (rdbTimetakenforhealthcheckExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.Timetakenforhealthcheck = "Excellent";
            }
            if (rdbTimetakenforhealthcheckGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.Timetakenforhealthcheck = "Good";
            }
            if (rdbTimetakenforhealthcheckFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.Timetakenforhealthcheck = "Fair";
            }
            if (rdbTimetakenforhealthcheckAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.Timetakenforhealthcheck = "Average";
            }
            if (rdbTimetakenforhealthcheckPoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.Timetakenforhealthcheck = "Poor";
            }

            /** How would you rate your overall experience at Lilavati Hospital during health check up. **/

            if (rdbHowwouldyourateExcellent.Checked == true)
            {
                tblFeedBackHealthCheckUp.Howwouldyourate = "Excellent";
            }
            if (rdbHowwouldyourateGood.Checked == true)
            {
                tblFeedBackHealthCheckUp.Howwouldyourate = "Good";
            }
            if (rdbHowwouldyourateFair.Checked == true)
            {
                tblFeedBackHealthCheckUp.Howwouldyourate = "Fair";
            }
            if (rdbHowwouldyourateAverage.Checked == true)
            {
                tblFeedBackHealthCheckUp.Howwouldyourate = "Average";
            }
            if (rdbHowwouldyouratePoor.Checked == true)
            {
                tblFeedBackHealthCheckUp.Howwouldyourate = "Poor";
            }


            tblFeedBackHealthCheckUp.WhatdidYouLike = txtWhatdidYouLike.Text.Trim();
            tblFeedBackHealthCheckUp.WhatDidYouLikeLeast = txtWhatDidYouLikeLeast.Text.Trim();
            tblFeedBackHealthCheckUp.AnySuggestionYou = txtAnySuggestionYou.Text.Trim();
            //tblFeedBackHealthCheckUp.WouldYouLikeTOMention = txtWouldYouLikeTOMention.Text.Trim();

            cmsbal.SaveFeedBackHealthCheckUp(tblFeedBackHealthCheckUp);
            if (HttpContext.Current.Session["SavetblFeedBackHealthCheckUp"] == "Success")
            {
                //Response.Write("<script>alert('Thanks for your Valuable Feedback.')</script>");
                Reset();

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup3();", true);
            }
            else
            {
                Response.Write("<script>alert('Failed')</script>");
            }
        }
        catch (Exception ex)
        {

            //throw;
        }
    }

    public void Reset()
    {
        txtFirstName2.Text = string.Empty;
        txtLastName2.Text = string.Empty;
        txtLHNO2.Text = string.Empty;        
        txtDate2.Text = string.Empty;       
        txtContactNo2.Text = string.Empty;
        txtEmailID2.Text = string.Empty;
        //txtHealthCheckup2.Text = string.Empty;
        ddlHealthCheckup.SelectedValue = "";
        txtCorporate.Text = string.Empty;

        txtService.Text = string.Empty;
        txtServiceReason.Text = string.Empty;
        txtStaff.Text = string.Empty;
        txtAreaofWork.Text = string.Empty;
        txtAreaReason.Text = string.Empty;

        chkHowDidYouCome.SelectedIndex = -1;


        rdbAppointmentHowwouldyourateExcellent.Checked = false;
        rdbAppointmentHowwouldyourateGood.Checked = false;
        rdbAppointmentHowwouldyourateFair.Checked = false;
        rdbAppointmentHowwouldyourateAverage.Checked = false;
        rdbAppointmentHowwouldyouratePoor.Checked = false;

        rdbRegistrationHowwasyourdocumentExcellent.Checked = false;
        rdbRegistrationHowwasyourdocumentGood.Checked = false;
        rdbRegistrationHowwasyourdocumentFair.Checked = false;
        rdbRegistrationHowwasyourdocumentAverage.Checked = false;
        rdbRegistrationHowwasyourdocumentPoor.Checked = false;

        rdbTestsPathologyBloodExcellent.Checked = false;
        rdbTestsPathologyBloodGood.Checked = false;
        rdbTestsPathologyBloodFair.Checked = false;
        rdbTestsPathologyBloodAverage.Checked = false;
        rdbTestsPathologyBloodPoor.Checked = false;

        rdbTestsRadiology_XrayExcellent.Checked = false;
        rdbTestsRadiology_XrayGood.Checked = false;
        rdbTestsRadiology_XrayFair.Checked = false;
        rdbTestsRadiology_XrayAverage.Checked = false;
        rdbTestsRadiology_XrayPoor.Checked = false;

        rdbTestsCardiologyECGExcellent.Checked = false;
        rdbTestsCardiologyECGGood.Checked = false;
        rdbTestsCardiologyECGFair.Checked = false;
        rdbTestsCardiologyECGAverage.Checked = false;
        rdbTestsCardiologyECGPoor.Checked = false;

        rdbTestsSpirometryExcellent.Checked = false;
        rdbTestsSpirometryGood.Checked = false;
        rdbTestsSpirometryFair.Checked = false;
        rdbTestsSpirometryAverage.Checked = false;
        rdbTestsSpirometryPoor.Checked = false;

        rdbConsultationGeneralPhysicianExcellent.Checked = false;
        rdbConsultationGeneralPhysicianGood.Checked = false;
        rdbConsultationGeneralPhysicianFair.Checked = false;
        rdbConsultationGeneralPhysicianAverage.Checked = false;
        rdbConsultationGeneralPhysicianPoor.Checked = false;

        rdbConsultationGynaecExcellent.Checked = false;
        rdbConsultationGynaecGood.Checked = false;
        rdbConsultationGynaecFair.Checked = false;
        rdbConsultationGynaecAverage.Checked = false;
        rdbConsultationGynaecPoor.Checked = false;

        rdbConsultationOphthalmologyExcellent.Checked = false;
        rdbConsultationOphthalmologyGood.Checked = false;
        rdbConsultationOphthalmologyFair.Checked = false;
        rdbConsultationOphthalmologyAverage.Checked = false;
        rdbConsultationOphthalmologyPoor.Checked = false;

        rdbConsultationENTExcellent.Checked = false;
        rdbConsultationENTGood.Checked = false;
        rdbConsultationENTFair.Checked = false;
        rdbConsultationENTAverage.Checked = false;
        rdbConsultationENTPoor.Checked = false;

        rdbConsultationDentalExcellent.Checked = false;
        rdbConsultationDentalGood.Checked = false;
        rdbConsultationDentalFair.Checked = false;
        rdbConsultationDentalAverage.Checked = false;
        rdbConsultationDentalPoor.Checked = false;

        rdbConsultationSkinExcellent.Checked = false;
        rdbConsultationSkinGood.Checked = false;
        rdbConsultationSkinFair.Checked = false;
        rdbConsultationSkinAverage.Checked = false;
        rdbConsultationSkinPoor.Checked = false;

        rdbConsultationDietitionExcellent.Checked = false;
        rdbConsultationDietitionGood.Checked = false;
        rdbConsultationDietitionFair.Checked = false;
        rdbConsultationDietitionAverage.Checked = false;
        rdbConsultationDietitionPoor.Checked = false;

        rdbHousekeepingOverallhygieneExcellent.Checked = false;
        rdbHousekeepingOverallhygieneGood.Checked = false;
        rdbHousekeepingOverallhygieneFair.Checked = false;
        rdbHousekeepingOverallhygieneAverage.Checked = false;
        rdbHousekeepingOverallhygienePoor.Checked = false;

        rdbStaffExcellent.Checked = false;
        rdbStaffGood.Checked = false;
        rdbStaffFair.Checked = false;
        rdbStaffAverage.Checked = false;
        rdbStaffPoor.Checked = false;

        rdbFoodBeveragesExcellent.Checked = false;
        rdbFoodBeveragesGood.Checked = false;
        rdbFoodBeveragesFair.Checked = false;
        rdbFoodBeveragesAverage.Checked = false;
        rdbFoodBeveragesPoor.Checked = false;

        rdbRespectforyourExcellent.Checked = false;
        rdbRespectforyourGood.Checked = false;
        rdbRespectforyourFair.Checked = false;
        rdbRespectforyourAverage.Checked = false;
        rdbRespectforyourPoor.Checked = false;

        rdbTimetakenforhealthcheckExcellent.Checked = false;
        rdbTimetakenforhealthcheckGood.Checked = false;
        rdbTimetakenforhealthcheckFair.Checked = false;
        rdbTimetakenforhealthcheckAverage.Checked = false;
        rdbTimetakenforhealthcheckPoor.Checked = false;

        rdbHowwouldyourateExcellent.Checked = false;
        rdbHowwouldyourateGood.Checked = false;
        rdbHowwouldyourateFair.Checked = false;
        rdbHowwouldyourateAverage.Checked = false;
        rdbHowwouldyouratePoor.Checked = false;

        txtWhatdidYouLike.Text = string.Empty;
        txtWhatDidYouLikeLeast.Text = string.Empty;
        txtAnySuggestionYou.Text = string.Empty;
        //txtWouldYouLikeTOMention.Text = string.Empty;

    }
}