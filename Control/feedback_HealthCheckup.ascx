﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="feedback_HealthCheckup.ascx.cs" Inherits="Control_feedback_HealthCheckup" %>

<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<%--    <script type="text/javascript" src="/js/1.8.3_min.js"></script>--%>
<script type="text/javascript">
    $(document).ready(function () {

        $('.fancybox').fancybox();

    });
</script>

<script type="text/javascript">
    function ShowPopup3() {
        $(function () {
            //$("#apply_form1").html(message);
            //$("#apply_form1").dialog({
            //    title: "Feedback In-Patient",
            //    buttons: {
            //        Close: function () {
            //            $(this).dialog('close');
            //        }
            //    },
            //    modal: true
            //});                       

            $("#apply_form3").fancybox().trigger('click');

        });
    };

    //$(document).ready(function () {
    //    $("#apply_form1").fancybox().trigger('click');
    //});

</script>


<script>
    $(function () {
        $(".date2").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "c-100:c",
            dateFormat: "dd-mm-yy"
        });


        $('#<%=txtDate2.ClientID%>').change(function () {

            var data = $('#<%=txtDate2.ClientID%>').val();
            if (data.length != 0 || data != "") {

                $("#date2").removeClass("error");
                $("#date2").addClass("populated");
            }
            else {
                $("#date2").addClass("error");
            }
        });
    });

</script>

<script>
    function CheckSave2() {
        var FirstName2 = $('#<%=txtFirstName2.ClientID%>').val();
        var LastName2 = $('#<%=txtLastName2.ClientID%>').val();
        var LHNo2 = $('#<%=txtLHNO2.ClientID%>').val();
        var Date2 = $('#<%=txtDate2.ClientID%>').val();
        var ContactNo2 = $('#<%=txtContactNo2.ClientID%>').val();
        var EmailID2 = $('#<%=txtEmailID2.ClientID%>').val();
        <%-- var HealthCheckup2 = $('#<%=txtHealthCheckup2.ClientID%>').val();--%>
         var HealthCheckup2 = $('#<%=ddlHealthCheckup.ClientID%>').val();
        var blank = false;

        if (FirstName2 == '') {
            $("#First2").addClass("error");
            blank = true;
        }
        else {
            $("#First2").removeClass("error");
        }

        if (LastName2 == '') {
            $("#Last2").addClass("error");
            blank = true;
        }
        else {
            $("#Last2").removeClass("error");
        }

        if (LHNo2 == '') {
            $("#LHNo2").addClass("error");
            blank = true;
        }
        else {
            $("#LHNo2").removeClass("error");
        }


        if (Date2 == '') {
            $("#date2").addClass("error");
            blank = true;
        }
        else {
            $("#date2").removeClass("error");
        }

        if (ContactNo2 == '') {
            $("#ContactNo2").addClass("error");
            blank = true;
        }
        else {
            if (ContactNo2.length < 10) {
                $("#ContactNo2").addClass("error");
                blank = true;
            }
            else {
                $("#ContactNo2").removeClass("error");
            }
        }

        if (EmailID2 == '') {
            $("#EmailId2").addClass("error");
            blank = true;
        }
        else {
            var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText.test(EmailID2)) {
                $("#EmailId2").addClass("error");
                alert("Valid Email ID");
                blank = true;
            }
            else {
                $("#EmailId2").removeClass("error");
            }
        }

        if (HealthCheckup2 == '') {
            $("#HealthCheckup2").addClass("error");
            blank = true;
        }
        else {
            $("#HealthCheckup2").removeClass("error");
        }

         if (HealthCheckup2 == 'Corporate') {
             $("#Corporate").addClass("error");
            }
            else {
             $("#Corporate").removeClass("error");
            }
        

        if (blank) {
            return false;
        }
        else {
            return true;
        }

    }
</script>

<script type="text/javascript">
    function onlyNumbers(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function NokeyPress(e) {
        alert("adf");
        e.preventDefault();
    }

    $(document).ready(function () {
        $('.date2').keypress(function (event) {
            event.preventDefault();
        });

    });



</script>

<script>
    $(document).ready(function () {
        
        $('#ctl00_ContentPlaceHolder1_HealthCheckUp_btnSubmit2').click(function (event) {

            var FirstName = $('#<%=txtFirstName2.ClientID%>').val();
            var LastName = $('#<%=txtLastName2.ClientID%>').val();
            var LHNo = $('#<%=txtLHNO2.ClientID%>').val();
            var Date2 = $('#<%=txtDate2.ClientID%>').val();
            var ContactNo = $('#<%=txtContactNo2.ClientID%>').val();
            var EmailId = $('#<%=txtEmailID2.ClientID%>').val();
            <%-- var HealthCheckup2 = $('#<%=txtHealthCheckup2.ClientID%>').val();--%>
             var HealthCheckup2 = $('#<%=ddlHealthCheckup.ClientID%>').val();

            if (FirstName != '' && LastName != '' && LHNo != '' && Date2 != '' && ContactNo.length == '10' && EmailId != '' && HealthCheckup2 != '') {
                $('#health_validate').hide();

                var EmailText11 = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText11.test(EmailId)) {
                    var target = $($(this).attr('title'));
                    if (target.length) {
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top = '120'
                        }, 1000);
                    }
                }
                else {
                    $('#health_validate').hide();
                }

            }
            else {

                $('#health_validate').show();

                var target = $($(this).attr('title'));
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top = '120'
                    }, 1000);
                }
            }

        }); 

    });

    $(document).ready(function () {
        $('#<%=ddlHealthCheckup.ClientID%>').change(function () {
            debugger;
            if ($('#<%=ddlHealthCheckup.ClientID%>').val() == 'Corporate') {
                $('#Corporate').show();
            }
            else {
                $('#Corporate').hide();
            }
        });
    });
</script>


<div class="tab_content">

    <div class="feedback_form FlowupLabels">

        <div class="validate_msg" id="health_validate" style="display: none;">Fields marked in red are complusory.</div>

        <div class="form_field fl_wrap" id="First2">
            <label class="fl_label">First Name</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtFirstName2" runat="server" class="fl_input"></asp:TextBox>
        </div>

        <div class="form_field fl_wrap" id="Last2">
            <label class="fl_label">Last Name</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtLastName2" runat="server" class="fl_input"></asp:TextBox>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="LHNo2">
            <label class="fl_label">LH No.</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtLHNO2" runat="server" class="fl_input"></asp:TextBox>
        </div>

        <div class="form_field fl_wrap" id="date2">
            <label class="fl_label">Date</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtDate2" runat="server" class="fl_input date2"></asp:TextBox>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap" id="ContactNo2">
            <label class="fl_label">Contact No.</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtContactNo2" runat="server" class="fl_input" MaxLength="10" onKeyPress='javascript:return onlyNumbers();'></asp:TextBox>
        </div>

        <div class="form_field fl_wrap" id="EmailId2">
            <label class="fl_label">Email ID</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtEmailID2" runat="server" class="fl_input"></asp:TextBox>
        </div>

        <div class="clear"></div>


        <div class="form_field fl_wrap" id="HealthCheckup2">
            <label class="fl_label">Health Checkup scheme availed</label>
            <%--<input class="fl_input" type="text" />--%>
            <%--<asp:TextBox ID="txtHealthCheckup2" runat="server" class="fl_input"></asp:TextBox>--%>
            <asp:DropDownList ID="ddlHealthCheckup" runat="server" class="js-example-basic-single fl_input specialityclass">
             <asp:ListItem Value=""></asp:ListItem>  
            <asp:ListItem>Basic </asp:ListItem>  
            <asp:ListItem>Cardiac</asp:ListItem>  
            <asp:ListItem>Pediatric</asp:ListItem>  
            <asp:ListItem>Adams</asp:ListItem>  
            <asp:ListItem>Adams plus</asp:ListItem> 
            <asp:ListItem>Eves</asp:ListItem>  
            <asp:ListItem>Eves plus</asp:ListItem>  
            <asp:ListItem>Silver</asp:ListItem>  
            <asp:ListItem>Senior Citizen</asp:ListItem>  
            <asp:ListItem>Gold</asp:ListItem>  
            <asp:ListItem>Gold plus</asp:ListItem>  
            <asp:ListItem>Platinum</asp:ListItem>  
            <asp:ListItem>Solitaire</asp:ListItem>  
            <asp:ListItem>Solitaire plus</asp:ListItem>  
            <asp:ListItem>Premium</asp:ListItem>  
            <asp:ListItem>Corporate</asp:ListItem>  
            </asp:DropDownList>
        </div>

          <div class="form_field fl_wrap" id="Corporate" style="display:none">
            <label class="fl_label">Enter the Name of the Corporate</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtCorporate" runat="server" class="fl_input"></asp:TextBox>
        </div>

        <div class="clear" style="height: 20px;"></div>

        <p><strong class="green_text uppercase">Guidelines </strong></p>
        <ul class="bullet">
            <li>To rate the following services that you received during your visit kindly tick in the box that represents your opinion. In case you did not receive any particular service; skip to the next question.</li>
            <li>For rating Fair and below; we request you to give your suggestion for improvement.</li>
        </ul>

        <p><strong class="green_text uppercase">How did you hear about Lilavati Hospital health check up?  </strong></p>

        <%--<p class="ez_checkbox fdbk_know">
            <input type="checkbox" id="hc_know1">
            <label for="hc_know1">Lilavati Hospital Doctor </label>
            <br>
            <input type="checkbox" id="hc_know2">
            <label for="hc_know2">Reference of family doctor </label>
            <br>
            <input type="checkbox" id="hc_know3">
            <label for="hc_know3">Corporate Tie Up </label>
            <br>
            <input type="checkbox" id="hc_know4">
            <label for="hc_know4">Website </label>
            <br>
            <input type="checkbox" id="hc_know5">
            <label for="hc_know5">Hospital employee </label>
            <br>
            <input type="checkbox" id="hc_know6">
            <label for="hc_know6">Relative/ Friend </label>
            <br>
            <input type="checkbox" id="hc_know7">
            <label for="hc_know7">Any Other </label>
        </p>--%>

        <asp:CheckBoxList ID="chkHowDidYouCome" runat="server" class="ez_checkbox fdbk_know">
            <asp:ListItem>Lilavati Hospital Doctor</asp:ListItem>
            <asp:ListItem>Reference of family doctor</asp:ListItem>
            <asp:ListItem>Corporate Tie Up </asp:ListItem>
            <asp:ListItem>Website</asp:ListItem>
            <asp:ListItem>Hospital employee</asp:ListItem>
            <asp:ListItem>Relative/ Friend</asp:ListItem>
            <asp:ListItem>Any Other</asp:ListItem>
        </asp:CheckBoxList>

        <div class="clear"></div>

        <div class="fdbk_box">
            <div class="fdbk_left"></div>
            <div class="fdbk_right green_text uppercase">
                <ul>
                    <li>Excellent</li>
                    <li>Good</li>
                    <li>Fair</li>
                    <li>Average</li>
                    <li>Poor</li>
                </ul>
            </div>
        </div>
              <div class="fdbk_box">
            <div class="fdbk_left fdbk_subtl green_text uppercase" data-no="A." style="font-weight: bold;">How would you rate your overall experience at Lilavati Hospital during health check up.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbHowwouldyourateExcellent" runat="server" GroupName="Howwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowwouldyourateGood" runat="server" GroupName="Howwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowwouldyourateFair" runat="server" GroupName="Howwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowwouldyourateAverage" runat="server" GroupName="Howwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHowwouldyouratePoor" runat="server" GroupName="Howwouldyourate" />
                    </li>
                </ul>
            </div>
        </div>


        <div class="fdbk_subtl green_text uppercase"><strong><span>B.</span> Appointment </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">How would you rate the way your appointment call was handled?</div>
            <div class="fdbk_right">
                <%-- <ul>
                    <li>
                        <input type="radio" name="hc_a_1"></li>
                    <li>
                        <input type="radio" name="hc_a_1"></li>
                    <li>
                        <input type="radio" name="hc_a_1"></li>
                    <li>
                        <input type="radio" name="hc_a_1"></li>
                    <li>
                        <input type="radio" name="hc_a_1"></li>
                </ul>--%>
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbAppointmentHowwouldyourateExcellent" runat="server" GroupName="AppointmentHowwouldyourate" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbAppointmentHowwouldyourateGood" runat="server" GroupName="AppointmentHowwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbAppointmentHowwouldyourateFair" runat="server" GroupName="AppointmentHowwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbAppointmentHowwouldyourateAverage" runat="server" GroupName="AppointmentHowwouldyourate" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbAppointmentHowwouldyouratePoor" runat="server" GroupName="AppointmentHowwouldyourate" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>C.</span> Registration & Billing </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">How was your document formalities and queries handled during Registration and payment? </div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbRegistrationHowwasyourdocumentExcellent" runat="server" GroupName="RegistrationHowwasyourdocument" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbRegistrationHowwasyourdocumentGood" runat="server" GroupName="RegistrationHowwasyourdocument" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRegistrationHowwasyourdocumentFair" runat="server" GroupName="RegistrationHowwasyourdocument" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRegistrationHowwasyourdocumentAverage" runat="server" GroupName="RegistrationHowwasyourdocument" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRegistrationHowwasyourdocumentPoor" runat="server" GroupName="RegistrationHowwasyourdocument" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>D.</span> Tests & Investigations </strong></div>

          <div class="fdbk_box">
            <div class="fdbk_left" data-no="">How was your experience with technical staff (Doctor,Technician) during tests and investigation (In terms of guidance,Behavior & Courtesy)? </div>
            <div class="fdbk_right">
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Pathology (Blood Collection)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTestsPathologyBloodExcellent" runat="server" GroupName="TestsPathologyBlood" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTestsPathologyBloodGood" runat="server" GroupName="TestsPathologyBlood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsPathologyBloodFair" runat="server" GroupName="TestsPathologyBlood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsPathologyBloodAverage" runat="server" GroupName="TestsPathologyBlood" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsPathologyBloodPoor" runat="server" GroupName="TestsPathologyBlood" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Radiology (X ray, Sonography, CT ,Mammography, BMD)</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTestsRadiology_XrayExcellent" runat="server" GroupName="TestsRadiology_Xray" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTestsRadiology_XrayGood" runat="server" GroupName="TestsRadiology_Xray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsRadiology_XrayFair" runat="server" GroupName="TestsRadiology_Xray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsRadiology_XrayAverage" runat="server" GroupName="TestsRadiology_Xray" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsRadiology_XrayPoor" runat="server" GroupName="TestsRadiology_Xray" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Cardiology (ECG, Stress test, 2D Echo, etc) </div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTestsCardiologyECGExcellent" runat="server" GroupName="TestsCardiologyECG" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTestsCardiologyECGGood" runat="server" GroupName="TestsCardiologyECG" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsCardiologyECGFair" runat="server" GroupName="TestsCardiologyECG" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsCardiologyECGAverage" runat="server" GroupName="TestsCardiologyECG" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsCardiologyECGPoor" runat="server" GroupName="TestsCardiologyECG" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="4.">Spirometry</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTestsSpirometryExcellent" runat="server" GroupName="Courtesy" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbTestsSpirometryGood" runat="server" GroupName="TestsSpirometry" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsSpirometryFair" runat="server" GroupName="TestsSpirometry" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsSpirometryAverage" runat="server" GroupName="TestsSpirometry" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTestsSpirometryPoor" runat="server" GroupName="TestsSpirometry" />
                    </li>
                </ul>
            </div>
        </div>

              <div class="fdbk_box">
            <div class="fdbk_left fdbk_subtl green_text uppercase" data-no="E." style="font-weight: bold;">Staff</div>
            <div class="fdbk_right">
            <%--    <ul>
                    <li>
                        <asp:RadioButton ID="rdbStaffExcellent" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffGood" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffFair" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffAverage" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffPoor" runat="server" GroupName="Staff" />
                    </li>
                </ul>--%>
            </div>
        </div>

                <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Behavior of customer care supervisors and guidance provided by them during health check up.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbStaffExcellent" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffGood" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffFair" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffAverage" runat="server" GroupName="Staff" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbStaffPoor" runat="server" GroupName="Staff" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>F.</span> Consultation/s </strong></div>
           <div class="fdbk_box">
            <div class="fdbk_left" data-no="">How was your experience with our doctors (were all your queries attended properly)?</div>
            <div class="fdbk_right">
                  </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">General Physician</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGeneralPhysicianExcellent" runat="server" GroupName="ConsultationGeneralPhysician" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbConsultationGeneralPhysicianGood" runat="server" GroupName="ConsultationGeneralPhysician" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGeneralPhysicianFair" runat="server" GroupName="ConsultationGeneralPhysician" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGeneralPhysicianAverage" runat="server" GroupName="ConsultationGeneralPhysician" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGeneralPhysicianPoor" runat="server" GroupName="ConsultationGeneralPhysician" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="2.">Gynaec/ Surgeon</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGynaecExcellent" runat="server" GroupName="ConsultationGynaec" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGynaecGood" runat="server" GroupName="ConsultationGynaec" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGynaecFair" runat="server" GroupName="ConsultationGynaec" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGynaecAverage" runat="server" GroupName="ConsultationGynaec" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationGynaecPoor" runat="server" GroupName="ConsultationGynaec" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="3.">Ophthalmology</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationOphthalmologyExcellent" runat="server" GroupName="ConsultationOphthalmology" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationOphthalmologyGood" runat="server" GroupName="ConsultationOphthalmology" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationOphthalmologyFair" runat="server" GroupName="ConsultationOphthalmology" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationOphthalmologyAverage" runat="server" GroupName="ConsultationOphthalmology" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationOphthalmologyPoor" runat="server" GroupName="ConsultationOphthalmology" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="4.">ENT</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationENTExcellent" runat="server" GroupName="ConsultationENT" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationENTGood" runat="server" GroupName="ConsultationENT" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationENTFair" runat="server" GroupName="ConsultationENT" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationENTAverage" runat="server" GroupName="ConsultationENT" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationENTPoor" runat="server" GroupName="ConsultationENT" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="5.">Dental</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDentalExcellent" runat="server" GroupName="ConsultationDental" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDentalGood" runat="server" GroupName="ConsultationDental" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDentalFair" runat="server" GroupName="ConsultationDental" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDentalAverage" runat="server" GroupName="ConsultationDental" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDentalPoor" runat="server" GroupName="ConsultationDental" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="6.">Skin</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationSkinExcellent" runat="server" GroupName="ConsultationSkin" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationSkinGood" runat="server" GroupName="ConsultationSkin" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationSkinFair" runat="server" GroupName="ConsultationSkin" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationSkinAverage" runat="server" GroupName="ConsultationSkin" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationSkinPoor" runat="server" GroupName="ConsultationSkin" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="7.">Dietition</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDietitionExcellent" runat="server" GroupName="ConsultationDietition" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDietitionGood" runat="server" GroupName="ConsultationDietition" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDietitionFair" runat="server" GroupName="ConsultationDietition" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDietitionAverage" runat="server" GroupName="ConsultationDietition" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbConsultationDietitionPoor" runat="server" GroupName="ConsultationDietition" />
                    </li>
                </ul>
            </div>
        </div>

            <div class="fdbk_box">
            <div class="fdbk_left fdbk_subtl green_text uppercase" data-no="G." style="font-weight: bold;">Food & Beverages</div>
            <div class="fdbk_right">
            <%--    <ul>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesExcellent" runat="server" GroupName="FoodBeverages" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesGood" runat="server" GroupName="FoodBeverages" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesFair" runat="server" GroupName="FoodBeverages" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesAverage" runat="server" GroupName="FoodBeverages" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesPoor" runat="server" GroupName="FoodBeverages" />
                    </li>
                </ul>--%>
            </div>
        </div>

               <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Quality of Food and Beverage served.</div>
            <div class="fdbk_right">
                   <ul>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesExcellent" runat="server" GroupName="FoodBeverages" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesGood" runat="server" GroupName="FoodBeverages" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesFair" runat="server" GroupName="FoodBeverages" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesAverage" runat="server" GroupName="FoodBeverages" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbFoodBeveragesPoor" runat="server" GroupName="FoodBeverages" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_subtl green_text uppercase"><strong><span>H.</span> Housekeeping </strong></div>

        <div class="fdbk_box">
            <div class="fdbk_left" data-no="1.">Overall hygiene and cleanliness of the health check up department.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbHousekeepingOverallhygieneExcellent" runat="server" GroupName="HousekeepingOverallhygiene" />
                    </li>

                    <li>
                        <asp:RadioButton ID="rdbHousekeepingOverallhygieneGood" runat="server" GroupName="HousekeepingOverallhygiene" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHousekeepingOverallhygieneFair" runat="server" GroupName="HousekeepingOverallhygiene" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHousekeepingOverallhygieneAverage" runat="server" GroupName="HousekeepingOverallhygiene" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbHousekeepingOverallhygienePoor" runat="server" GroupName="HousekeepingOverallhygiene" />
                    </li>
                </ul>
            </div>
        </div>

  

    

        <div class="fdbk_box">
            <div class="fdbk_left fdbk_subtl green_text uppercase" data-no="I." style="font-weight: bold;">Respect for your privacy.</div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbRespectforyourExcellent" runat="server" GroupName="Respectforyour" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectforyourGood" runat="server" GroupName="Respectforyour" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectforyourFair" runat="server" GroupName="Respectforyour" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectforyourAverage" runat="server" GroupName="Respectforyour" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbRespectforyourPoor" runat="server" GroupName="Respectforyour" />
                    </li>
                </ul>
            </div>
        </div>

        <div class="fdbk_box">
            <div class="fdbk_left fdbk_subtl green_text uppercase" data-no="J." style="font-weight: bold;">Time taken for health check up as against your expectation. </div>
            <div class="fdbk_right">
                <ul>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforhealthcheckExcellent" runat="server" GroupName="Timetakenforhealthcheck" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforhealthcheckGood" runat="server" GroupName="Timetakenforhealthcheck" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforhealthcheckFair" runat="server" GroupName="Timetakenforhealthcheck" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforhealthcheckAverage" runat="server" GroupName="Timetakenforhealthcheck" />
                    </li>
                    <li>
                        <asp:RadioButton ID="rdbTimetakenforhealthcheckPoor" runat="server" GroupName="Timetakenforhealthcheck" />
                    </li>
                </ul>
            </div>
        </div>

  
        <div class="clear" style="height: 20px;"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">What did you like the most about Lilavati hospital?</label>
            <%--<textarea class="fl_input"></textarea>--%>
            <asp:TextBox ID="txtWhatdidYouLike" runat="server" TextMode="MultiLine" class="fl_input"></asp:TextBox>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">What did you like the least about Lilavati hospital?</label>
            <%--<textarea class="fl_input"></textarea>--%>
            <asp:TextBox ID="txtWhatDidYouLikeLeast" runat="server" TextMode="MultiLine" class="fl_input"></asp:TextBox>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">Any suggestion you would like to put forward which will help us to serve you better</label>
            <%--<textarea class="fl_input"></textarea>--%>
            <asp:TextBox ID="txtAnySuggestionYou" runat="server" TextMode="MultiLine" class="fl_input"></asp:TextBox>
        </div>
        <div class="clear"></div>

        <div class="full_width fl_wrap">
            <label class="fl_label">Would you like to mention of our services for it’s outstanding quality and/ recommend any staff for their outstanding Performance?</label>
            <%--<textarea class="fl_input"></textarea>--%>
           <%-- <asp:TextBox ID="txtWouldYouLikeTOMention" runat="server" TextMode="MultiLine" class="fl_input"></asp:TextBox>--%>
        </div>
        <div class="clear"></div>

            <div class="form_field fl_wrap">
            <label class="fl_label">Service</label>
            <asp:TextBox ID="txtService" runat="server" class="fl_input Fn1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap">
            <label class="fl_label">Reason</label>
            <asp:TextBox ID="txtServiceReason" runat="server" class="fl_input Ln1 commonclass1" TextMode="MultiLine"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

        <div class="form_field fl_wrap">
            <label class="fl_label">Staff</label>
            <asp:TextBox ID="txtStaff" runat="server" class="fl_input Lh1 commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="form_field fl_wrap">
            <label class="fl_label">Area of Work</label>
            <asp:TextBox ID="txtAreaofWork" runat="server" class="fl_input ND commonclass1"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

          <div class="clear"></div>
         <div class="form_field fl_wrap">
            <label class="fl_label">Reason</label>
            <asp:TextBox ID="txtAreaReason" runat="server" class="fl_input Ln1 commonclass1" TextMode="MultiLine"></asp:TextBox>
            <%--<input class="fl_input" type="text" />--%>
        </div>

        <div class="clear"></div>

                <p><strong>I hereby give my consent to the hospital for publishing/ producing/ utilizing my feedback for service improvement/ promotion/ advertisement purpose.</strong></p>

              <div class="clear"></div>
        <div class="form_centering_div">
            <p align="center"><strong class="green_text">Thank you for your co-operation.</strong></p>
            <%--<input class="submit_btn fade_anim uppercase" type="submit" value="SUBMIT" title="#health_validate" />--%>
            <asp:Button ID="btnSubmit2" runat="server" Text="SUBMIT" class="submit_btn fade_anim uppercase" OnClick="btnSubmit2_Click" OnClientClick='javascript:return CheckSave2();' title="#health_validate" />
             <hr style="margin-top:30px" />
        <p>For grievances write to us on <a href="mailto:complaints@lilavatihospital.com">complaints@lilavatihospital.com</a></p>
        </div>
    </div>

    <div id="apply_form3" style="display: none;" class="fancybox">
        <div class="form_apply_main">
            <div class="apply_form_tl uppercase green_text">
                Thank You!!
            </div>
            <div class="clear">
            </div>
            <div class="form_centering_div" style="font-size: 12px; line-height: 18px; color: #000; border-top: 1px solid #ccc; padding: 10px 15px 15px; text-align: center; background: #eee;">
                Your Information Has Been Submitted.                
            </div>
        </div>
    </div>

</div>
