﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class media_press_releases : System.Web.UI.Page
{
    CMSBAL cmsbal = new CMSBAL();
    MediaPress tblmediapress = new MediaPress();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //BindRepeater();
            TabName();
        }
    }

    //public void BindRepeater()
    //{
    //    DataTable dt = new DataTable();
    //    dt = cmsbal.BindYear();
    //    if (dt.Rows.Count > 0)
    //    {
    //        rptMediaPress.DataSource = dt;
    //        rptMediaPress.DataBind();
    //    }
    //}
     
    //protected void rptMediaPress_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        HiddenField hfValueYouNeed = (HiddenField)e.Item.FindControl("hdnId");
    //        string value = hfValueYouNeed.Value;

    //        tblmediapress.YearName=value;
    //        //cmsbal.GETYearWiseMonth(tblmediapress);
                       
    //        Repeater rptChild = (Repeater)e.Item.FindControl("rptInnerRepeater");

    //        rptChild.DataSource = cmsbal.GETYearWiseMonth(tblmediapress);          
    //        rptChild.DataBind();

            
    //    }
    //}
    //protected void rptInnerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    HiddenField hdnIdMonth = (HiddenField)e.Item.FindControl("hdnIdMonth");
    //    tblmediapress.Month = hdnIdMonth.Value;

    //    //cmsbal.GETYearWiseMonthWiseDescrption(tblmediapress);

    //    Repeater rptDesc = (Repeater)e.Item.FindControl("rptDesc");

    //    rptDesc.DataSource = cmsbal.GETYearWiseMonthWiseDescrption(tblmediapress);
    //    rptDesc.DataBind();
    //}

    public void TabName()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Media_Press 'MEDIA_TAB'");
        if (dt.Rows.Count == 1)
        {
            //rptTab.DataSource = dt;
            //rptTab.DataBind();

            StringBuilder strBuild = new StringBuilder();

            strBuild.Append("<ul id='tab_list' class='tab_list' style='display:none;'>");
            foreach (DataRow row in dt.Rows)
            {
                strBuild.Append("<li><a href='#' rel='tab_" + row["PUBLICATION"] + "'>" + row["PUBLICATION"] + " </a></li>");
            }
            strBuild.Append("</ul>");

            litTab.Text = strBuild.ToString();

            rptDesc.DataSource = dt;
            rptDesc.DataBind();
        }
        else
        {
            StringBuilder strBuild1 = new StringBuilder();

            strBuild1.Append("<ul id='tab_list' class='tab_list'>");
            foreach (DataRow row in dt.Rows)
            {
                strBuild1.Append("<li><a href='#' rel='tab_" + row["PUBLICATION"] + "'>" + row["PUBLICATION"] + " </a></li>");
            }
            strBuild1.Append("</ul>");

            litTab.Text = strBuild1.ToString();

            rptDesc.DataSource = dt;
            rptDesc.DataBind();
        }
    }
        
    protected void rptDesc_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnPUBLICATION = (HiddenField)e.Item.FindControl("hdnPUBLICATION");
            string value = hdnPUBLICATION.Value;
            tblmediapress.Publication = value;

            DataTable dt = new DataTable();
            dt = cmsbal.BindYear();

            Repeater rptYearName = (Repeater)e.Item.FindControl("rptYearName");
            if (dt.Rows.Count > 0)
            {
                rptYearName.DataSource = dt;
                rptYearName.DataBind();
            }

        }
    }
    protected void rptYearName_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnYear = (HiddenField)e.Item.FindControl("hdnYear");
            string value = hdnYear.Value;

            tblmediapress.YearName = value;
            //cmsbal.GETYearWiseMonth(tblmediapress);

            Repeater rptMonth = (Repeater)e.Item.FindControl("rptMonth");
            DataTable dt = new DataTable();
            dt = cmsbal.GETYearWiseMonth(tblmediapress);

            if (dt.Rows.Count > 0)
            {
                rptMonth.DataSource = dt;
                rptMonth.DataBind();
            }
            else
            {
                ((HtmlGenericControl)(e.Item.FindControl("divId"))).Visible = false;
      
            }
        }
    }
    protected void rptMonth_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField hdnIdMonth = (HiddenField)e.Item.FindControl("hdnIdMonth");
        tblmediapress.Month = hdnIdMonth.Value;

        //cmsbal.GETYearWiseMonthWiseDescrption(tblmediapress);

        Repeater rptDescription = (Repeater)e.Item.FindControl("rptDescription");

        rptDescription.DataSource = cmsbal.GETYearWiseMonthWiseDescrption(tblmediapress);
        rptDescription.DataBind();
    }
}