﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="search_results.aspx.cs" Inherits="search_results" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        ul.search_result li {
            float: none;
            list-style: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">
                Search Result for : 
	                <span style="display: inline-block; text-transform: none;">
                        <asp:Label ID="lblTittle" runat="server"></asp:Label>
                    </span>
            </div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <p id="pError" runat="server" visible="false" align="center">
                No matching result found.<br>
                Please modify your search criteria and try searching again.
            </p>

            <ul class="search_result bullet">

                <asp:Repeater ID="rptSearch" runat="server">
                    <ItemTemplate>
                        <li>
                            <a href='/<%#Eval("PAGE_NAME") %>' class="search_hd"><%#Eval("HEADING") %></a>
                            <asp:Literal ID="litDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Literal>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>

                <%--  <li>
                    <a href="#" class="search_hd">Page Name</a>
                    <p>We are a premier multi-specialty tertiary care hospital of India and have been acknowledged globally as the centre of medical excellence. Over the years, Lilavati Hospital & Research Centre has developed unmatched trust with its patients on the basis</p>
                </li>
                <li>
                    <a href="#" class="search_hd">Page Name</a>
                    <p>We are a premier multi-specialty tertiary care hospital of India and have been acknowledged globally as the centre of medical excellence. Over the years, Lilavati Hospital & Research Centre has developed unmatched trust with its patients on the basis</p>
                </li>
                <li>
                    <a href="#" class="search_hd">Page Name</a>
                    <p>We are a premier multi-specialty tertiary care hospital of India and have been acknowledged globally as the centre of medical excellence. Over the years, Lilavati Hospital & Research Centre has developed unmatched trust with its patients on the basis</p>
                </li>
                <li>
                    <a href="#" class="search_hd">Page Name</a>
                    <p>We are a premier multi-specialty tertiary care hospital of India and have been acknowledged globally as the centre of medical excellence. Over the years, Lilavati Hospital & Research Centre has developed unmatched trust with its patients on the basis</p>
                </li>
                <li>
                    <a href="#" class="search_hd">Page Name</a>
                    <p>We are a premier multi-specialty tertiary care hospital of India and have been acknowledged globally as the centre of medical excellence. Over the years, Lilavati Hospital & Research Centre has developed unmatched trust with its patients on the basis</p>
                </li>--%>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</asp:Content>

