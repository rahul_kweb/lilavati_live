﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="Pediatrics_and_paediatric_surgery.aspx.cs" Inherits="Pediatrics_and_paediatric_surgery" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Pediatric and Paediatric Surgery</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Pediatrics.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>

                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server" OnItemDataBound="rptContent_ItemDataBound">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                                <asp:HiddenField ID="hdnDesc" runat="server" Value='<%#Eval("DESCRIPTION") %>' />
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="clear"></div>
                </div>
                <%-- <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server" OnItemDataBound="rptContent_ItemDataBound">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnDesc" runat="server" Value='<%#Eval("DESCRIPTION") %>' />
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>Pediatric</h2>

                        <div class="tab_content">
                            <p>The Department of Pediatrics is located on the 10th floor of the hospital building in the ‘B’ wing which is dedicated exclusively for child care.</p>
                            <p>The department has few of the most senior consultants in Pediatrics in the city. These senior consultants are pioneer in their specialties and have been teachers to many practicing Pediatricians in the city as well as the state.</p>
                            <p>Pediatric services are strengthened by the 24 x 7 availability of fully qualified Pediatricians available in the campus to tackle any kind of emergencies.</p>
                            <p>Our children friendly nursing staff are well trained to take care of sick children, administer medications and assist the doctors during procedures specific to Pediatrics.</p>
                            <p>State of the art facilities are available for the purpose of diagnosis and management of backed up by round the clock availability of Pathology and Radiology Services.</p>
                            <p>The department of Pediatrics is a recognized for post graduate training in Pediatrics and as an examination centre by the National Board of Examinations, New Delhi since 2006.</p>


                            <div class="accd_box">
                                <div class="hd c_expand_hd">Professionals</div>
                                <div class="newsbox accd_content c_expand_box">
                                    PediatricDoctorDetails
                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Paediatric Surgery</h2>

                        <div class="tab_content">

                            <p>The Department of Paediatric Surgery is a full-fledged unit offering state-of-the-art tertiary care for all surgical problems in newborn, infants, older children and adolescents. The Surgical team is led by senior and experienced Consultants of national and International repute, who have been trained in leading hospitals in India and overseas. The Consultants are backed by a team of competent and qualified Surgical Clinical Associates and Resident doctors. The Service is amply supported by the some of the finest Paediatric Anaesthetists, Intensivist and Nursing staff. Our surgical results, in many difficult surgical conditions and anomalies in children, are comparable to leading Paediatric Surgical centres in the world.</p>
                            <p>Our department is a teaching centre for the six year Pediatric Surgery course with accreditation by National Board of Examination, New Delhi It is the only Paediatric Surgical department in the Private sector in Mumbai to be recognised as a Postgraduate teaching centre by the National BoardStudents trained at our centre are working in leading centers in India and Developed countries. During their training period, our students present research papers at international and national conferences and have won awards as well. Department regularly publishes many research papers in reputed scientific journals on an ongoing basis.</p>
                            <p>Our department is keenly conscious of our social responsibilities. We have our consultants and residents who are working in public hospitals and charitable hospitals. National programs like the “Go folic” (folic acid awareness initiative) have been launched under the guidance of our consultants. For the deserving economically backward classes, with the active cooperation of Lilavati Hospital, we are able to provide world class care at almost no cost.</p>


                            <div class="accd_box">
                                <div class="hd c_expand_hd">Services</div>
                                <div class="newsbox accd_content c_expand_box">

                                    <ul class="bullet">
                                        <li><strong style="text-transform: capitalize;">Fetal Anomalies</strong><br>
                                            Many surgical problems in children are now first detected before birth. The department leads a team comprising of Ultrasonologists, Obstetricians and Paediatric Surgeons to diagnose and manage Antenatally detected Fetal surgical problems with a comprehensive and multi-disciplinary approach.</li>

                                        <li><strong>Neonatal & Infant Surgery</strong><br>
                                            Surgery during the neonatal period requires special skills and understanding of the newborn physiology. The department has successfully handled all types of Neonatal and Infant surgical problems such a trachea-esophageal fistula, diaphragmatic hernia, Laryngotracheoesophageal cleft, intestinal atresias, imperforate anus, biliary atresia, spina bifida, hydrocephalus, PU valves etc.</li>

                                        <li><strong>Pediatric Gastrointestinal and Hepatobiliary Surgery</strong><br>
                                            The division has special interest and expertise in the care of Paediatric gastrointestinal and Hepatobiliary surgical problems.  The surgeons of the department have worked in leading Paediatric GI and hepatobiliary centres and have a large personal experience in conditions such as Biliary Atresia etc. Complex GI Procedures such as Gastric Pull ups and surgery for Total colonic aganglionosis have been successfully performed.</li>

                                        <li><strong>Pediatric Urology</strong><br>
                                            Paediatric Urological surgery forms a major part of every Paediatric Surgical department. This department offers the best in Paediatric Urological surgery –by way of expertise and experience. The department is a leading referral centre for Urinary Incontinence, Neurogenic Bladders and Vesicoureteric reflux.</li>

                                        <li><strong>Pediatric Neurosurgery</strong><br>
                                            Paediatric Surgeons from the department have special expertise in the surgery and care of children with Spinal Dysraphism and Hydrocephalus.</li>

                                        <li><strong>Pediatric Thoracic Surgery</strong><br>
                                            Complicated Paediatric Thoracic surgical problems (excluding Paediatric Cardiac Surgery) are regularly managed successfully by the department. For e.g. – cases such a Lobar Emphysema, Lobectomy, Tracheo-esophageal anomalies, Thoracic cysts, etc.</li>

                                        <li><strong>Pediatric Endoscopy</strong><br>
                                            All types of endoscopy services such as Cystoscope, GI scope, Bronchoscope is available both for diagnostic and therapeutic purposes.</li>

                                        <li><strong>Pediatric Laparoscopy</strong><br>
                                            Laparoscopy in the paediatric age group is performed where it has distinct advantages over open procedures.</li>

                                        <li><strong>Major and Supramajor Surgery in Children</strong><br>
                                            The team approach followed in the department and the availability of state-of-the –art paediatric surgical facilities –both in the operative and preoperative period make it possible for the unit to successfully perform major and supramajor operations on children. These have included 10-11 hour procedures. Such surgeries are also possible because of the excellent Anaesthesia team and the backup of reliable Ventilator care and Intensive care.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Technology and Infrastructure</div>
                                <div class="newsbox accd_content c_expand_box">

                                    <ul class="bullet">
                                        <li>Pediatric gastroscopes, bronchoscopes, choledochoscopes and colonoscopes.</li>
                                        <li>Pediatricand neonatal cystoscopes & resectoscopes.</li>
                                        <li>Pediatric and neonatal laparoscopic instrumentation of international standards.</li>
                                        <li>State of art Pathology, Microbiology and Histopathology with special immunohistochemistry analysis and Acetylcholinesterase staining methods.</li>
                                        <li>Intensive care back up with full fledged Neonatal Intensive Care Unit and Pediatric Intensive Care Unit.</li>
                                        <li>Co-ordinated Pediatric radiology services like conventional radiology with image intensifier for contrast studies, sophisticated Ultrasound, CT scan, MRI, MRCP, Nuclear scan, PET-CT scan.</li>
                                        <li>Intra-operative radiological services.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Professionals</div>
                                <div class="newsbox accd_content c_expand_box">
                                    PaediatricSurgeryDoctorDetails
                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>


                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

