﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class covid_19_video_rehabilitation_services : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBanner();
            Content();
        }
    }

    public void Content()
    {
        cms.ID = 25;
        cmsbal.GETBYIDContent(cms);
        ltrContent.Text = cms.Description;
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrBanner.Text = bal.GetBannerForBinding(67);
    }
}