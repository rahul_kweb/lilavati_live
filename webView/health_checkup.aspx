﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webView/MainMaster.master" AutoEventWireup="true" CodeFile="health_checkup.aspx.cs" Inherits="health_checkup" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="~/Control/webviewservices.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital - "Stay healthy" schedule your health check up today</title>
    <meta name="description" content="Take the first step forward & detect early symptoms of diseases, schedule your health checkup online quickly with best health care provider in Mumbai. You can get customized packages as per what suits you the best.">

    <script type="text/javascript" src="/js/tabcontent.js"></script>
    <script>
        function CallEvent(value) {

            if (value == "1") {
                $('a[rel="tab_36"]').click();
            }
            else if (value == "2") {
                $('a[rel="tab_37"]').click();
            }
            else {
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Health Check Up</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Health_Checkup.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share" style="display:none;">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <%-- <div class="tabber">



                    <asp:Literal ID="litContent" runat="server"></asp:Literal>

                    <asp:Literal ID="litHealth" runat="server"></asp:Literal>

                </div>--%>
                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Literal ID="ltrltabname" runat="server"></asp:Literal>
                    </ul>
                    <asp:Literal ID="ltrldescription" runat="server"></asp:Literal>
                    <div class="clear"></div>
                </div>
                <!--  xxxxxxxxxxxx  -->
                

                <!-- xxxxxxxxxxxxxxxxxx -->





                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

