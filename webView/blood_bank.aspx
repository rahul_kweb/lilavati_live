﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webView/MainMaster.master" AutoEventWireup="true" CodeFile="blood_bank.aspx.cs" Inherits="blood_bank" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="~/Control/webviewservices.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital - Donate Blood & Save Life's | Blood Bank in Mumbai</title>
    <meta name="description" content="Volunteer for blood donation camps & help people in need. Every year around 6000 donors give blood to the bank which is split into various modules for medical & surgical purpose.">

    <script type="text/javascript" src="/js/equate.js"></script>
    <script type="text/javascript" src="/js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Blood Bank</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share" style="display:none;">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                                
                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

