﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class health_checkup : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    HealthPackagesDetails tblHealthPackagesDetails = new HealthPackagesDetails();
    string val = string.Empty;
    StringBuilder strBuild = new StringBuilder();
   StringBuilder strndescription=new StringBuilder();
   protected void Page_Load(object sender, EventArgs e)
   {
     
       if (!IsPostBack)
       {
           if (Session["tabactive"] != null)
           {
               string check = Session["tabactive"].ToString();
               if (Session["tabactive"].ToString() == "Instruction")
               {
                   ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "javascript:CallEvent(1);", true);
               }
               else if (Session["tabactive"].ToString() == "DescriptionKeyTest")
               {
                   ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "javascript:CallEvent(2);", true);
               }
               else
               {
               }
               Session.Remove("tabactive");
           }

           BindContent();
           BindBanner();
       }
   }

    //public void BindRepeater()
    //{
    //    tblcmsInnerPageMaster.PageName = "Health Check Up";
    //    DataTable dt = new DataTable();
    //    dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);
               

    //    if (dt.Rows.Count > 0)
    //    {
    //        //rptContent.DataSource = dt;
    //        //rptContent.DataBind();
    //    }
    //}

    //public void HealthPackageName()
    //{
    //    DataTable dt = new DataTable();
    //    dt = cmsbal.HealthPackagesNamePrice();

    //    if (dt.Rows.Count > 0)
    //    {
    //        rptPackagesName.DataSource = dt;
    //        rptPackagesName.DataBind();
    //    }
    //}

    public void BindContent()
    {


        tblcmsInnerPageMaster.PageName = "Health Check Up";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        foreach (DataRow row in dt.Rows)
        {

            //strBuild.Append("<div class='tabbertab'>");
            //strBuild.Append("<h2> " + row["Tab"] + " </h2>");
            //strBuild.Append("<div class='tab_content'>");
            //strBuild.Append(row["Description"]);
            //strBuild.Append("<div class='clear'></div>");
            //strBuild.Append("</div>");
            //strBuild.Append("</div>");
            strBuild.Append("<li>");
            strBuild.Append("<a href='#' rel='tab_" + row["inner_id"] + "'>");
            strBuild.Append("" + row["Tab"] + "");
            strBuild.Append("</a>");
            strBuild.Append("</li>");

            strndescription.Append("<div id='tab_" + row["inner_id"] + "'>");
            strndescription.Append(row["Description"]);
            strndescription.Append("<div class='clear'>");
            strndescription.Append("</div>");
            strndescription.Append("</div>");
            if (val == "")
            {
                HealthContent();
            }
        }

        ltrltabname.Text = strBuild.ToString();
        ltrldescription.Text = strndescription.ToString();

        //  litContent.Text = strBuild.ToString();        
    }

    public void HealthContent()
    {
        StringBuilder strBuild1 = new StringBuilder();
         StringBuilder strBuild2 = new StringBuilder();

        DataTable dt = new DataTable();
        dt = cmsbal.HealthPackagesNamePrice();

        strBuild.Append("<li>");
        strBuild.Append("<a href='#' rel='tab_2'>");
        strBuild.Append("Health Checkup Packages");
        strBuild.Append("</a>");
        strBuild.Append("</li>");
        strndescription.Append("<div id='tab_2'>");
        foreach (DataRow row in dt.Rows)
        {
          
            if (row["PACK_PRICE"].ToString() == "0")
            {
                strndescription.Append("<div class='pckg'><a href='web-Packages-Details/" + row["Health_Id"] + "'>" + row["PACKAGE_NAME"] + "" + "</a></div>");
            }
            else
            {
                strndescription.Append("<div class='pckg'><a href='web-Packages-Details/" + row["Health_Id"] + "'>" + row["PACKAGE_NAME"] + " - Rs. " + row["PACK_PRICE"] + "/-" + "</a></div>");
            }
            strndescription.Append("<div class='clear'></div>");
        
        }

        Utility utility = new Utility();
        SqlCommand cmd = new SqlCommand("SELECT H.HEALTH_ID,H.PACKAGE_NAME,P.PACK_PRICE,P.HEALTH_ID FROM HEALTH_CHECKUP H INNER JOIN HEALTH_PACKAGES_DETAILS P ON H.HEALTH_ID = P.HEALTH_ID WHERE H.ISACTIVE = 1 AND P.ISACTIVE = 1 AND P.PACK_PRICE<>'' AND H.HEALTH_ID IN (20,22) ORDER BY H.HEALTH_ID DESC");
        DataTable dt1 = new DataTable();
        dt1 = utility.Display(cmd);
        if (dt1.Rows.Count > 0)
        {
            foreach (DataRow dr1 in dt1.Rows)
            {
                if (dr1["PACK_PRICE"].ToString() == "0")
                {
                    strndescription.Append("<div class='pckg'><a href='web-Packages-Details/" + dr1["Health_Id"] + "'>" + dr1["PACKAGE_NAME"] + "" + "</a></div>");
                }
                else
                {
                    strndescription.Append("<div class='pckg'><a href='web-Packages-Details/" + dr1["Health_Id"] + "'>" + dr1["PACKAGE_NAME"] + " - Rs. " + dr1["PACK_PRICE"] + "/-" + "</a></div>");
                }
                strndescription.Append("<div class='clear'></div>");
            }           
        }

        strndescription.Append("</div>");
        val = "test";
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(8);
    }


}