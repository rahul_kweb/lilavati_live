﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class critical_care : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    string docValue = string.Empty;
    string docValue1 = string.Empty;
    string docValue2 = string.Empty;
    string docValue3 = string.Empty;
    string docValue4 = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            BindBanner();

        }
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Critical Care";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();
        }
        else
        {
            rptContent.DataSource = null;
            rptContent.DataBind();
        }
    }

   

    protected void rptContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnDesc = (HiddenField)e.Item.FindControl("hdnDesc");
            SICUDoctorDetails();
            ICCUDoctorDetails();
            PICUDoctorDetails();
            NICUDoctorDetails();
            ICUDoctorDetails();

            StringBuilder val = new StringBuilder(hdnDesc.Value);

            val = val.Replace("SICUDoctorDetails", docValue);
            val = val.Replace("ICCUDoctorDetails", docValue1);
            val = val.Replace("PICUDoctorDetails", docValue2);
            val = val.Replace("NICUDoctorDetails", docValue3);
            val = val.Replace("ICUDoctorDetails", docValue4);

            Label lblDesc = (Label)e.Item.FindControl("lblDesc");

            lblDesc.Text = val.ToString();
        }
    }

    public void SICUDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (176,326) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);

        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=/Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            strBuil.Append("<a href='/Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");    


        }
        docValue = strBuil.ToString();
    }
    
    public void ICCUDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (176,326) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);

        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=/Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            strBuil.Append("<a href='/Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");    




        }
        docValue1 = strBuil.ToString();
    }

    public void PICUDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (311,345) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);

        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=/Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            strBuil.Append("<a href='/Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");    

        }
        docValue2 = strBuil.ToString();
    }

    public void NICUDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (311,345) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);

        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=/Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            strBuil.Append("<a href='/Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");    

        }
        docValue3 = strBuil.ToString();
    }

    public void ICUDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (317,176) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);

        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=/Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<a href='/Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            strBuil.Append("<a href='/Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");

        }
        docValue4 = strBuil.ToString();
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(3);
    }
}