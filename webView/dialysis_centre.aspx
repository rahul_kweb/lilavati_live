﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webView/MainMaster.master" AutoEventWireup="true" CodeFile="dialysis_centre.aspx.cs" Inherits="dialysis_centre" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="~/Control/webviewservices.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="/js/equate.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Dialysis Centre</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--  <div class="inner_banner">
                    <img src="images/inner_banner/Dialysis.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share" style="display:none;">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%-- <div class="tabbertab">
                            <h2>Overview</h2>

                            <div class="tab_content">
                                <p>Dialysis unit is situated on 6th floor & it functions round the clock for patient care. On an average 45-50 dialysis are performed every day. A team of well-known Nephrologists and nursing staff look after this department. Asepsis is strictly maintained and monitored. </p>
                                <p>For appointments kindly contact on 26568650 / 26568651</p>

                                <div class="clear"></div>
                            </div>
                        </div>

                        <!-- xxxxxxxxxxxxxxxxxx -->

                        <div class="tabbertab">
                            <h2>Services</h2>

                            <div class="tab_content">
                                <ul class="bullet">
                                    <li><strong>Hemodialysis:</strong> About 25 machines for hemodialysis, 16 stations in AKD and remaining for ICU and ICCU.</li>
                                    <li><strong>Peritoneal dialysis:</strong> counselling, patient acceptance, options for manual or automated peritoneal dialysis and training for the same are provide with help of a special team. Peritoneal dialysis catheter insertion.</li>
                                    <li><strong>Kidney transplantation</strong> for living related donors & cadaver donors. Special team for transplantation includes trained Nephrologists, specially trained Surgeons and Anaesthetists. Trained nurses to look after the kidney transplant recipients in postoperative period, in ICU setting.</li>
                                    <li><strong>Vascular access:</strong> Patients opting for long term hemodialysis are evaluated for their vascular access, and if having problem, evaluated by a special team of vascular access specialist. This includes Nephrologists, vascular surgeon and interventional radiologist.</li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <!-- xxxxxxxxxxxxxxxxxx -->

                        <div class="tabbertab">
                            <h2>Technology and Infrastructure</h2>

                            <div class="tab_content">
                                <ul class="bullet">
                                    <li>16 state of art, latest dialysis machines.</li>
                                    <li>Gambro’s CRRT & ART Plus machine for patients with multi organ failure.</li>
                                </ul>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <!-- xxxxxxxxxxxxxxxxxx -->

                        <div class="tabbertab">
                            <h2>Professionals</h2>

                            <div class="tab_content">

                                <div class="clear"></div>
                            </div>
                        </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->



                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />
            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

