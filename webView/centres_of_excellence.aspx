﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webView/MainMaster.master" AutoEventWireup="true" CodeFile="centres_of_excellence.aspx.cs" Inherits="centres_of_excellence" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital – India’s finest surgical experts here for you</title>
    <meta name="description" content="Find best surgical experts in Mumbai, they can perform complex surgical procedures like Bariatric Surgery, Oncology & Onco Surgery, Neurology & Neuro Surgery">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Centres of Excellence</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="coe_list">

                <div class="coe_box">
                    <a href="web-Anesthesiology">
                        <img src="/images/coe/anesthesiology.png">
                        <span>Anesthesiology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Audiology-Speech-Therapy">
                        <img src="/images/coe/audiology_speech.png">
                        <span>Audiology & Speech Therapy</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Bariatric-Surgery">
                        <img src="/images/coe/bariatric.png">
                        <span>Bariatric Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Cardiology-CVTS">
                        <img src="/images/coe/cardiology.png">
                        <span>Cardiology & Cardio Vascular Thoracic Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Chest-Medicine">
                        <img src="/images/coe/chest_medicine.png">
                        <span>Chest Medicine</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Corneal-Transplant">
                        <img src="/images/coe/corneal_transplant.png">
                        <span>Corneal Transplant</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Colorectal-Surgery">
                        <img src="/images/coe/colorectal_surgery.png">
                        <span>Colorectal Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Dental">
                        <img src="/images/coe/dental.png">
                        <span>Dental</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Dermo-Cosmetology">
                        <img src="/images/coe/dermo_cosmetology.png">
                        <span>Dermo Cosmetology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Diabetology-Endocrinology">
                        <img src="/images/coe/diabetology_endocrinology.png">
                        <span>Diabetology & Endocrinology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Ent">
                        <img src="/images/coe/ent.png">
                        <span>ENT, Head & Neck Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Gastroenterology-Gastrointestinal-Surgery">
                        <img src="/images/coe/gastroenterology.png">
                        <span>Gastroenterology & Gastrointestinal Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-General-Surgery">
                        <img src="/images/coe/general_surgery.png">
                        <span>General Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Gynaecology-Obstetrics">
                        <img src="/images/coe/gynecology.png">
                        <span>Gynaecology & Obstetrics</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Haematology">
                        <img src="/images/coe/haematology.png">
                        <span>Haematology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Hair-Transplant">
                        <img src="/images/coe/hair_transplant.png">
                        <span>Hair Transplant</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Headache-Migraine-Clinic">
                        <img src="/images/coe/headache_migraine.png">
                        <span>Headache & Migraine Clinic</span></a>
                </div>

                <%--<div class="coe_box">
                    <a href="Hip-Replacement">
                        <img src="images/coe/hip_replacement.png">
                        <span>Hip Replacement</span></a>
                </div>--%>

                <div class="coe_box">
                    <a href="web-Infectious-Diseases">
                        <img src="/images/coe/infectious_diseases.png">
                        <span>Infectious Diseases</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Internal-Medicine">
                        <img src="/images/coe/internal_medicine.png">
                        <span>Internal Medicine</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-lactation">
                        <img src="/images/coe/ic_lactation.png">
                        <span>Lactation</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-liver-transplant">
                        <img src="/images/coe/liver_transplant.png">
                        <span>Liver Transplant</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Minimally-Invasive-Surgery">
                        <img src="/images/coe/laparoscopic.png">
                        <span>Minimally Invasive Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Nephrology">
                        <img src="/images/coe/nephrology.png">
                        <span>Nephrology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Neurology-Neuro-Surgery">
                        <img src="/images/coe/neurology.png">
                        <span>Neurology & Neuro Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Oncology">
                        <img src="/images/coe/oncology.png">
                        <span>Oncology & Onco Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Ophthalmology">
                        <img src="/images/coe/ophthalmology.png">
                        <span>Opthalmology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Orthopaedics">
                        <img src="/images/coe/orthopedics.png">
                        <span>Orthopaedics</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Pain-Management">
                        <img src="/images/coe/pain_management.png">
                        <span>Pain Management</span></a>
                </div>


                <div class="coe_box">
                    <a href="web-Pediatrics-And-Paediatric-Surgery">
                        <img src="/images/coe/pediatric_surgery.png">
                        <span>Pediatrics & Paediatric Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Plastic-Surgery">
                        <img src="/images/coe/plastic_surgery.png">
                        <span>Plastic Surgery</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Psychiatry-Psychology-Neuropsychology">
                        <img src="/images/coe/psychiatry.png">
                        <span>Psychiatry/ Psychology/ Neuropsychology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Rheumatology">
                        <img src="/images/coe/rheumatology.png">
                        <span>Rheumatology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Sleep-Medicine">
                        <img src="/images/coe/sleep_medicine.png">
                        <span>Sleep Medicine</span></a>
                </div>

                <%--<div class="coe_box">
                    <a href="#">
                        <img src="images/coe/transplant.png">
                        <span>Transplant: Corneal & Kidney</span></a>
                </div>--%>

                <div class="coe_box">
                    <a href="web-Urology">
                        <img src="/images/coe/urology.png">
                        <span>Urology/ Andrology</span></a>
                </div>

                <div class="coe_box">
                    <a href="web-Vascular-Endovascular-Surgery">
                        <img src="/images/coe/vascular.png">
                        <span>Vascular & Endovascular Surgery </span></a>
                </div>

                <div class="clear"></div>
            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

</asp:Content>

