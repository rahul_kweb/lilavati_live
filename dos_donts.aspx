﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="dos_donts.aspx.cs" Inherits="dos_donts" %>

<%@ Register TagName="Visitors" TagPrefix="menu" Src="Control/visitors.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">DOs & Don'ts</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<p><strong class="green_text uppercase">Do’s</strong></p>

                <ul class="bullet">
                    <li>Do ask your patient's permission to visit before you arrive.</li>
                    <li>Do wash your hands and sanitize them before you touch the patient or hand the patient something you've been touching.</li>
                    <li>Do turn off your cell phone, or at least turn the ringer off.</li>
                    <li>Do leave the room if the doctor or provider arrives to examine or talk to the patient</li>
                </ul>

                <p><strong class="green_text uppercase">Dont’s</strong></p>

                <ul class="bullet">
                    <li>Don't enter the hospital if you have any symptoms that could be contagious.</li>
                    <li>Don’t stay with the patient for a long time. Keep the visit short.</li>
                    <li>Don't bring outside food/fruits inside hospital for patients/patient's attendants. The hospital serves well balanced vegetarian meals.</li>
                    <li>Do not smoke; consume alcohol and non-vegetarian food in the hospital premises.</li>
                    <li>Don't bring flowers, bouquets for the patient.</li>
                    <li>Don't attempt photography or video shoot in the hospital premises.</li>
                </ul>--%>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Visitors ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

