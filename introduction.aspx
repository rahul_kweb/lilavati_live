﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="introduction.aspx.cs" Inherits="introduction" %>

<%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Introduction</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<h1 class="hd green_text uppercase">Lilavati Hospital & Research centre</h1>

                <p>We are a premier multi-specialty tertiary care hospital of India and have been acknowledged globally as the centre of medical excellence. Over the years, Lilavati Hospital & Research centre has developed unmatched trust with its patients on the basis of a strong foundation which includes the state-of-the-art facilities, best medical expertise, research, education and charitable endeavours. We are extremely proud that today, we serve patients from all walks of life and not only national but also international. We believe in <em>‘Sarvetra Sukhinah : Santu, Sarve Santu Niramaya:’</em> which means ‘Let all be blissful, Let all stay healthy’. Our approach and attitude have always been with a human touch; which truly reflects the essence of our motto “More than Healthcare, Human Care”.</p>

                <p>
                    Our journey began with a humble opening in 1997 with 10 beds and initially 22 doctors. Today, we have increased our capacity several folds and have a total of 314 beds with one of the largest Intensive Care Units (ICUs), 12 Operation Theatres with advanced facilities, more than 300 consultants and manpower of nearly 1,800 employees. Lilavati Hospital and Research Centre attends to nearly 300 In-patients and 1,500 Out-patients daily. Lilavati Hosptial & Research Centre is strategically located, as it is easily accessible from all parts of Mumbai and is very close to the Airport of Mumbai. 
                </p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

