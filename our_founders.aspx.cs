﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class our_founders : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
        }
    }

    public void Content()
    {
        cms.ID = 2;
        cmsbal.GETBYIDContent(cms);
        string cnt = cms.Description;
        cnt = cnt.Replace("../images", "images");
        litContent.Text = cnt;
    }
}