﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="dermo_cosmetology.aspx.cs" Inherits="dermo_cosmetology" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>

    <script type="text/javascript" src="js/tabcontent.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Dermo Cosmetology</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Dermocosmetology.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>



                <%-- <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Dermo-Cosmetology centre is one of a kind where dermatology and cosmetology have been amalgamated in a hospital setup. Dermo-Cosmetology centre at the Lilavati Hospital has Senior Consultants, fulltime junior consultants along with Aestheticians and nursing aide. The Department of Dermo-Cosmetology provides full medical and surgical dermatologic service to patients. Wide range of Dermo-Cosmetology procedures and Dermatological surgeries are carried out at the centre.Approximately 500 procedures are performed per month.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Chemical peels – glycolic peels , salicylic acid peels, black peels, yellow peels, TCA peel</li>
                                <li>Elctrocautery </li>
                                <li>Curretting</li>
                                <li>Skin Biopsy</li>
                                <li>Ear Lobe repair Surgery</li>
                                <li>Ear Piercing</li>
                                <li>Eryosurgery</li>
                                <li>Tattoo removal</li>
                                <li>Treatment of Birthmarks</li>
                                <li>Treatment of acne scars, Atriae (Stretch Marks)</li>
                                <li>Vetitigo grafting</li>
                                <li>Mole removal </li>
                                <li>Face Rejuvenation </li>
                                <li>Laxer Hair Seduction </li>
                                <li>Skin Polishing</li>
                                <li>UVA & NArowb and UVB therapy</li>
                                <li>Targeted phototherapy </li>
                                <li>Botox</li>
                                <li>Fillers</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Conmed Electrocoutary</li>
                                <li>Medlite C6 Laxer</li>
                                <li>Moxel Fractional Laxer</li>
                                <li>Cutera Long Pulsed Ndyay Laxer</li>
                                <li>Paavten Phototherapy Unit</li>
                                <li>Daartin Targeted Phototherapy unit</li>
                                <li>Radiofrequency unit</li>
                                <li>Ablactive CO2 Laxer </li>
                                <li>Resoderm Macrodermabraxion</li>
                                <li>Eryoenegical Unit</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">
                <menu:Services ID="menu" runat="server" />
            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

