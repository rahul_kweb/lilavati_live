﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="patients_rights_responsibilities.aspx.cs" Inherits="patients_rights_responsibilities" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Patients Rights & Responsibilities</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%-- <div class="tabbertab">
                        <h2>PATIENTS RIGHTS</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>To be informed and educated in a language that you can understand </li>
                                <li>To receive medical advice and treatment which fully meets the currently accepted standards of care & Quality</li>
                                <li>To be given a clear description of your medical condition, estimated cost of treatment and to involve you in decision about your care </li>
                                <li>To have your privacy & dignity and your religious and cultural beliefs respected </li>
                                <li>To have information regarding your medical condition kept confidential </li>
                                <li>To have information of your care providers </li>
                                <li>To have a safe and protected environment for you & your relatives </li>
                                <li>To make suggestions & voice complaints </li>
                                <li>To refuse treatment </li>
                                <li>To have access to your clinical records </li>
                                <li>To be protected from physical abuse and neglect</li>
                                <li>To obtain informed consent from you before any procedure/ surgery/ treatment/ anesthesia/ initiation of any research protocol/ blood & blood product transfusion</li>
                                <li>Right to seek an addition opinion regarding clinical care</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>PATIENTS RESPONSIBILITY</h2>

                        <div class="tab_content">

                            <ul class="bullet">
                                <li>Provide Complete and accurate information about his/ her health, including present condition, past illness, hospitalizations, medications, natural products and vitamins and any other matters that pertain to his/ her health </li>
                                <li>Provide complete and accurate information including full name, address and other information </li>
                                <li>To ask questions when he/ she does not understand what the doctor or other member of healthcare team tells about diagnosis or treatment. He/she should also inform the doctor if he/ she anticipates problems in following prescribed treatment or considering alternative therapies.</li>
                                <li>Abide by all hospital rules and regulations given in patient information brochure, more importantly;
    	                        <ul>
                                    <li>Comply with the no-smoking policy </li>
                                    <li>Comply with the visitor policies to ensure the rights and comfort of all patients. Be considerate of noise levels, privacy and safety. Weapons are prohibited on premises </li>
                                    <li>Treat hospital staff, other patients and visitors with courtesy and respect.</li>
                                </ul>
                                </li>
                                <li>To be on time in case of appointments. To cancel or reschedule as far in advance as possible in case of cancellation or rescheduling of the appointments.</li>
                                <li>Not to give medication prescribed for him/her to others </li>
                                <li>Provide complete and accurate information for insurance claims and work with the hospital staff and doctors to make payment arrangements</li>
                                <li>To communicate with the healthcare provider if his/her condition worsens or does not follow the expected course </li>
                                <li>To pay for services billed for in a timely manner as per the hospital policies </li>
                                <li>To respect that some other patients medical condition may be more urgent than yours and accept that your doctor may need to attend them first </li>
                                <li>To respect that admitted patient and patients requiring emergency care take priority for your doctor </li>
                                <li>To follow the prescribed treatment plan and carefully comply with the instructions given </li>
                                <li>To accept, where applicable, adaptations to the environment to ensure a safe and secure stay in hospital</li>
                                <li>To accept the measures taken by the hospital to ensure personal privacy and confidentiality of medical records </li>
                                <li>Not to take any medications without the knowledge of doctor and healthcare professionals</li>
                                <li>To understand the charter of rights and sleek clarifications, if any</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->


                    <div class="clear"></div>
                </div>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

