﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true"
    CodeFile="find_a_doctor.aspx.cs" Inherits="find_a_doctor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital - Book your appointment with best doctors in India</title>
    <meta name="description" content="Schedule your appointment with the best doctors & get proper guidance from the experts. Consult highly experienced neurologist in Mumbai & get correct diagnosis & medication.">

    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script>
        $(function () {
            $(".selectthis").selectmenu()
        });
    </script>
    <script type="text/javascript" src="/js/equate.js"></script>

    <link href="/css/select2.min.css" rel="stylesheet" />
    <script src="/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".js-example-basic-single").select2();
        });
    </script>
    <style type="text/css">
        .select2-container--default .select2-selection--single {
            background-color: #fff; /* border: 1px solid #aaa; */
            border-radius: 4px;
            border-bottom: 0px solid #ddd;
        }

        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('images/loader.gif') 50% 50% no-repeat rgb(249,249,249);
        }
    </style>

    <%--<script type="text/javascript">
        $(window).load(function () {
            $(".loader").fadeOut("slow");
            $(".loader").fadeOut(1000);
        })
    </script>--%>


    <script>

        function jScript() {
            $(".js-example-basic-single").select2();
            applyEquate();
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txtSearch.ClientID%>').on('keypress', function () {

                var selectedText = 'By Speciality';
                $('#ddlspecialty option').map(function () {
                    if ($(this).text() == selectedText) return this;
                }).attr('selected', 'selected');

                document.getElementById("select2-ddlspecialty-container").title = "By Speciality";
                document.getElementById("select2-ddlspecialty-container").textContent = "By Speciality";

                var selectedText1 = 'Doctor Name';
                $('#ctl00_ContentPlaceHolder1_ddldoctorlist option').map(function () {
                    if ($(this).text() == selectedText1) return this;
                }).attr('selected', 'selected');

                document.getElementById("select2-ctl00_ContentPlaceHolder1_ddldoctorlist-container").title = "Doctor Name";
                document.getElementById("select2-ctl00_ContentPlaceHolder1_ddldoctorlist-container").textContent = "Doctor Name";

                $(".hide").attr('disabled', 'disabled').text("Doctor Name");

            });


            $('#<%=ddlspecialty.ClientID%>').change(function () {

                $('.blank').attr("value", "");
            });

        });
    </script>

    <%--<script type="text/javascript">
        function jScript1() {
            setInterval(function () {
                $(window).ready(function () {
                    equalheight('.equate');
                    equalheight('.doc_div');
                    console.log("setInterval");

                });
            }, 1);
        }
    </script>--%>

    <%-- <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        alert(prm);
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    //$("#dvAccordian").accordion();
                    //$("#tabs").tabs();
                    equalheight('.doc_div');
                }
            });
        };
    </script>--%>

    <%--   <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txtSearch.ClientID%>').on('keypress', function () {

                var selectedText = 'By Speciality';
                $('#ddlspecialty option').map(function () {
                    if ($(this).text() == selectedText) return this;
                }).attr('selected', 'selected');

                document.getElementById("select2-ddlspecialty-container").title = "By Speciality";
                document.getElementById("select2-ddlspecialty-container").textContent = "By Speciality";

                var selectedText1 = 'By Doctor';
                $('#ctl00_ContentPlaceHolder1_ddldoctorlist option').map(function () {
                    if ($(this).text() == selectedText1) return this;
                }).attr('selected', 'selected');

                document.getElementById("select2-ctl00_ContentPlaceHolder1_ddldoctorlist-container").title = "By Doctor";
                document.getElementById("select2-ctl00_ContentPlaceHolder1_ddldoctorlist-container").textContent = "By Doctor";

                $(".hide").attr('disabled', 'disabled').text("By Doctor");

            });


            $('#<%=ddlspecialty.ClientID%>').change(function () {

                $('.blank').attr("value", "");
            });

        });
    </script>

    <script type="text/javascript">
        var pageIndex = 1;
        var pageCount;
        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                //GetRecords();
                             
                var Spe = $("#ddlspecialty option:selected").text();
                var Search = $('#<%=txtSearch.ClientID%>').val();
                if (Spe == 'By Speciality' && Search == '') {
                    setInterval(function () { GetRecords(); }, 1000);
                    console.log("on Scroll IF");
                }
                else {
                    console.log("on Scroll else if");
                }

            }
        });


        $(document).ready(function () {
            
            var Spe = $("#ddlspecialty option:selected").text();
            var Search = $('#<%=txtSearch.ClientID%>').val();

            var repeater = "";
            if (Spe == 'By Speciality' && Search == '') {
                GetRecords();
                var callCount = 1;
                repeater = setInterval(function () {
                    if (callCount < 7) {
                        GetRecords();
                        callCount += 1;
                    } else {
                        clearInterval(repeater);
                    }
                }, 100);
                console.log("if " + repeater);
            }
            else {
                console.log("else if " + repeater);
                clearInterval(repeater);                
            }
        });


        function GetRecords() {
            pageIndex++;
            if (pageIndex == 2 || pageIndex <= pageCount) {
                $("#loader").show();
                $.ajax({
                    type: "POST",
                    url: "find_a_doctor.aspx/GetCustomers",
                    data: '{pageIndex: ' + pageIndex + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        //alert(response.d);
                    },
                    error: function (response) {
                        //alert(response.d);
                    }
                });
            }
        }

        function OnSuccess(response) {
            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
            var customers = xml.find("Customers");
            customers.each(function () {
                var customer = $(this);
                var table = $("#dvCustomers ul").eq(0).clone(true);
                console.log($("#dvCustomers ul").eq(0).clone(true));               
                $(".pic", table).attr("src", "Admin/Doctors/" + customer.find("PHOTO_VCR").text());
                $(".docname", table).html(customer.find("DOCTOR_NAME_VCR").text());
                $(".Spec", table).html(customer.find("SPECIALITIES").text());
                $(".docId", table).attr("href", "Doctorprofile/" + customer.find("DOCTOR_ID_BINT").text() + "/" + "0");
                $("#dvCustomers").append(table);
            });
            $("#loader").hide();
        }
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="scrptmanager" runat="server"></asp:ScriptManager>
    <div class="inner_page_container">
        <div class="green_banner find_bg">
            <div class="doc_search_box">
                <span class="tl">FIND THE RIGHT DOCTOR</span>

                <%--<div class="loader"></div>--%>

                <div class="doc_search">
                    <div class="doc_select">
                        <asp:DropDownList ID="ddlspecialty" runat="server" OnSelectedIndexChanged="ddlspecialty_SelectedIndexChanged1"
                            CssClass="js-example-basic-single" ClientIDMode="Static" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>

                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <div class="doc_select">
                                <asp:DropDownList ID="ddldoctorlist" runat="server" CssClass="js-example-basic-single hide"
                                    AutoPostBack="true" Visible="true" OnSelectedIndexChanged="ddldoctorlist_SelectedIndexChanged"
                                    Enabled="false">
                                    <asp:ListItem Value="0">Doctor Name</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="doc_search_field">

                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Search" class="search_field autosuggest blank"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" class="doc_search_btn fade_anim" OnClick="btnSearch_Click" />

                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="container text_format fade_anim" style='position: relative;'>


            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="text-align: center; height: 100%; width: 100%; position: absolute; z-index: 10; background: url(images/white_overlay.png);">
                        <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loader.gif" AlternateText="Loading ..." ToolTip="Loading ..." Style="margin: 50px auto;" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="updatepaneldoctorlist" runat="server">
                <ContentTemplate>
                    <script type="text/javascript">
                        Sys.Application.add_load(jScript);
                    </script>

                    <asp:Button ID="btncall" runat="server" OnClientClick="Callfunction();return false;" Visible="true" />
                    <asp:Literal ID="ltrldoctors" runat="server"></asp:Literal>

                    <p id="pError" runat="server" visible="false" align="center">
                        No matching result found.<br>
                        Please modify your search criteria and try searching again.
                    </p>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlspecialty" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="ddldoctorlist" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />

                </Triggers>
            </asp:UpdatePanel>



            <%--  <asp:Literal ID="ltrldoctors" runat="server"></asp:Literal>--%>
            <%-- <ul class="doc_search_result">
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        MD, DNB, Pain Fellowship (Canada), Chronic Pain Management</span> <span class="view_btn">
                            View Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        MD, DNB, Pain Fellowship (Canada), </span><span class="view_btn">View Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        MD, DNB, Pain Fellowship (Canada), Chronic Pain Management</span> <span class="view_btn">
                            View Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        MD, DNB, Chronic Pain Management</span> <span class="view_btn">View Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        MD, DNB, Pain Fellowship (Canada), Chronic Pain Management</span> <span class="view_btn">
                            View Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        Pain Fellowship (Canada), Chronic Pain Management</span> <span class="view_btn">View
                            Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
                <li><a href="doctor_profile.aspx" class="doc_div">
                    <img src="images/doctor_pic.jpg">
                    <span class="name green_text"><strong>Dr. Jitendra Jain</strong></span> <span class="desig">
                        Pain Fellowship (Canada)</span> <span class="view_btn">View Profile</span>
                    <div class="clear">
                    </div>
                </a></li>
            </ul>--%>


            <img id="loader" alt="" src="images/docloader.gif" style="display: none; margin-left: 50px;" />

            <div class="clear">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
<%--        <script>
        $('#<%= btnSearch.ClientID %>').click(function () {

            var searchword = $('#<%= txtSearch.ClientID %>').val();
            var ddlspclvalue = $('#<%= ddlspecialty.ClientID %>').val();

            if (searchword == "" && ddlspclvalue=="By Speciality") {
                alert("Please modify your search criteria and try searching again.");
            }
        });
        </script>--%>

</asp:Content>
