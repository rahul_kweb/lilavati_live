﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class diagnostics : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string docValue = string.Empty;
    string docValue1 = string.Empty;
    string docValue2 = string.Empty;
    string docValue3 = string.Empty;
    string UrodynamicsDoc = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            BindBanner();            
        }
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Diagnostics";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();

            rptContent1.DataSource = dt;
            rptContent1.DataBind();
        }


    }
    protected void rptContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnDesc = (HiddenField)e.Item.FindControl("hdnDesc");
            PathologyDoctorDetails();
            RadiologyAndImagingDoctorDetails();
            NuclearMedicineDoctorDetails();
            NonInvasiveCardiologyDoctorDetails();
            UrodynamicsDoctorDetails();

            StringBuilder val = new StringBuilder(hdnDesc.Value);

            val = val.Replace("PathologyDoctorDetails", docValue);
            val = val.Replace("RadiologyAndImagingDoctorDetails", docValue1);
            val = val.Replace("NuclearMedicineDoctorDetails", docValue2);
            val = val.Replace("NonInvasiveCardiologyDoctorDetails", docValue3);
            val = val.Replace("UrodynamicsDoctorDetails", UrodynamicsDoc); 

            Label lblDesc = (Label)e.Item.FindControl("lblDesc");

            lblDesc.Text = val.ToString();
        }
    }

    public void PathologyDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        //DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,23");
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT,DirectAvailable FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (36,101,102,68,436,262) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            if (dr["DOCTOR_ID_BINT"].ToString() != "98")
            {

                strBuil.Append("<li>");
                strBuil.Append("<div class='doc_div inner'>");
                strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
                strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
                strBuil.Append("</a>");
                strBuil.Append("<div class='docBtns'>");
                strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                //if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
				if(false)
                {
                    strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                }
                strBuil.Append("</div>");
                //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn' style='width: 99% !important;'>View<br> Profile</a>");
                // strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                strBuil.Append("<div class='clear'></div>");
                strBuil.Append("</div>");
                strBuil.Append("</li>");

            }
        }
        
        docValue = strBuil.ToString();
    }

    public void RadiologyAndImagingDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,28");
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
			if(false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn' style='width: 99% !important;'>View<br> Profile</a>");
            // strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");   
        }

        docValue1 = strBuil.ToString();
    }

    public void NuclearMedicineDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,38");
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");


            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn' style='width: 99% !important;'>View<br> Profile</a>");
            // strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");  
        }

        docValue2 = strBuil.ToString();
    }

    public void NonInvasiveCardiologyDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT,DirectAvailable FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (70) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);

        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            //string consdrid = string.Empty;
            //if (dr["CONSDOCID"].ToString() != "")
            //{
            //    consdrid = dr["CONSDOCID"].ToString();
            //}
            //else
            //{
            //    consdrid = "0";
            //}

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn' style='width: 99% !important;'>View<br> Profile</a>");
            // strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");  

        }
        docValue3 = strBuil.ToString();
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(5);
    }

    public void UrodynamicsDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        //DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,62");
		
		SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT,DirectAvailable FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (400) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);
		
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }            

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");

      

            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");

            if (dr["DOCTOR_ID_BINT"].ToString() == "222")
            {
                strBuil.Append("<div class='docBtns'>");
                strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
                {
                    strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                }
                strBuil.Append("</div>");
                //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            else
            {
                strBuil.Append("<div class='docBtns'>");
                strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
                {
                    strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                }
                strBuil.Append("</div>");
                //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn' style='width: 99% !important;'>View<br> Profile</a>");
            }

            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");
        }

        UrodynamicsDoc = strBuil.ToString();
    }
}