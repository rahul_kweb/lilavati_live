﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class patients_education_brochure : System.Web.UI.Page
{
    PatientsEducationBrochure tblPatientsEducationBrochure = new PatientsEducationBrochure();
    PatientsEducation tblPatientsEducation = new PatientsEducation();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            //RepeateTittle();

            BindBanner();
        }
    }

    public void BindRepeater()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetPatientsEducation();
        if (dt.Rows.Count > 0)
        {            
            rptPatientBrochure.DataSource = dt;
            rptPatientBrochure.DataBind();            
        }
        else
        {
            rptPatientBrochure.DataSource = null;
            rptPatientBrochure.DataBind();
        }
    }

    /** Commemted Data **/

    public void RepeateTittle()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.TopPatientsEducationBrochure();

        if (dt.Rows.Count > 0)
        {
            rptTittle.DataSource = dt;
            rptTittle.DataBind();
        }
    }

    protected void rptTittle_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnId = (HiddenField)e.Item.FindControl("hdnId");

            tblPatientsEducationBrochure.Pat_Id = int.Parse(hdnId.Value);
            DataTable dt = new DataTable();
            dt = cmsbal.BroucherPatientsEducationBrochure(tblPatientsEducationBrochure);

            Repeater rptbrochure = (Repeater)e.Item.FindControl("rptBrochure");

            Image img = (Image)e.Item.FindControl("imgfirst");
            HyperLink hylnk = (HyperLink)e.Item.FindControl("hyplnk");
            tblPatientsEducationBrochure.Brochure = dt.Rows[0]["BROCHURE"].ToString();
            img.ImageUrl = string.Format("uploads/brochure/{0}", tblPatientsEducationBrochure.Brochure).ToString();
            hylnk.NavigateUrl = string.Format("uploads/brochure/{0}", tblPatientsEducationBrochure.Brochure).ToString();

            if (dt.Rows.Count > 0)
            {               
                dt.Rows[0].Delete();
                dt.AcceptChanges();

                rptbrochure.DataSource = dt;
                rptbrochure.DataBind();
            }
        }
    }

    /** End  **/

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(53);
    }

}