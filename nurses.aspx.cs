﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class nurses : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Content();
            BindBanner();
        }
    }

    public void Content()
    {
        cms.ID = 18;
        cmsbal.GETBYIDContent(cms);
        StringBuilder strBuil = new StringBuilder(cms.Description);
        strBuil.Replace("&nbsp;", "");
        litContent.Text = strBuil.ToString();
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(61);
    }



}