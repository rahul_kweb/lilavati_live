﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[ScriptService]
public class WebService : System.Web.Services.WebService {

    Utility uti = new Utility();
    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /*xxxxxxxxxxxxxxxxxxx Login xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UserLogin(string UserName, string Password)
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "APP_USER_LOGIN");
                cmd.Parameters.AddWithValue("@USERNAME", UserName);
                cmd.Parameters.AddWithValue("@PASSWORD", Password);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }

                        row.Add("Message", "Login successful.");
                        row.Add("Status", true);

                        rows.Add(row);
                    }

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));

                }
                else
                {
                    using (SqlCommand cmd1 = new SqlCommand("Proc_PCipopRegistrationMaster", con))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@PARA", "APP_USER_LOGIN_ACTIVATE_ACCOUNT");
                        cmd1.Parameters.AddWithValue("@USERNAME", UserName);
                        cmd1.Parameters.AddWithValue("@PASSWORD", Password);
                        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                        DataTable dt1 = new DataTable();
                        da1.Fill(dt1);
                        if (dt1.Rows.Count > 0)
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row = new Dictionary<string, object>();                           

                            row.Add("Message", "Please activate your account.");
                            row.Add("Status", false);

                            rows.Add(row);

                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));

                        }
                        else
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                            Dictionary<string, object> row = new Dictionary<string, object>();

                            row.Add("Message", "Invalid User Id/Password.");
                            row.Add("Status", false);
                            rows.Add(row);

                            this.Context.Response.ContentType = "application/json; charset=utf-8";
                            this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UserChangePassword(string AddressIndex, string OldPassword, string NewPassword)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "APP_CHANGE_PASSWORD");
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", AddressIndex);
            cmd.Parameters.AddWithValue("@PASSWORD", OldPassword);
            cmd.Parameters.AddWithValue("@NEWPASSWORD", NewPassword);
            if (uti.Execute(cmd))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Your password has been changed successfully.");
                row.Add("Status", true);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Sorry ! password could not be changed successfully.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
    }

    /*xxxxxxxxxxxxxxxxxxx Sign Up xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void UsernameValidation(string Username)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("select * from GAadAddressMaster where UserName = '" + Username + "'");
            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "This user id already exists.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                //row.Add("Message", "");
                row.Add("Status", true);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception)
        {
            
            throw;
        }
       
        
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SendOTP(string MobileNo)
    {
        try
        {
            string strOTP = "";
            strOTP = GenerateOTP();

            string TO = MobileNo;
            string username = "9920874020";
            string password = "jddgd";
            string msg = "" + strOTP + " is the Onetime password (OTP) for your web-registration at Lilavati Hospital. Pls do not share this with anyone.";

            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create("http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=341739&username=" + username + "&password=" + password + "&To=" + TO + "&Text=" + msg);
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = new Dictionary<string, object>();

            if (responseString.Contains("Invalid mobile number"))
            {
                row.Add("Message", "Invalid mobile number.");
                row.Add("Status", false);
                rows.Add(row);
            }
            else
            {
                row.Add("Message", "Your message has been sent successfully!");
                row.Add("Status", true);
                rows.Add(row);
            }

            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));

        }
        catch (Exception ex)
        {
            //this.Title = ex.Message;
        }
    }
  
    public string GenerateOTP()
    {
        try
        {            
            char[] charArr = "0123456789".ToCharArray();
            string strrandom = string.Empty;
            Random objran = new Random();
            int noofcharacters = Convert.ToInt32(6);
            for (int i = 0; i < noofcharacters; i++)
            {
                //It will not allow Repetation of Characters
                int pos = objran.Next(1, charArr.Length);
                if (!strrandom.Contains(charArr.GetValue(pos).ToString()))
                    strrandom += charArr.GetValue(pos);
                else
                    i--;
            }
            return strrandom;
        }
        catch (Exception)
        {
            
            throw;
        }
        
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SignUp(string UserName,string Title, string Surname, string FirstName,string MiddleName,string Email,string DOB,string Sex,string BloodGroup,string MaritalStatus,string Religion,string Occupation,string Address, string Country,string State,string City,string Pincode,string MobileNo,string AlternateContactNo,string LHNO,string DeviceType,string DeviceId)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster", con))
        {
            string name = FirstName.Trim();
            string val = string.Empty;
            string Password = string.Empty;
            string TLHNO = string.Empty;

            /*** Create Random Password ***/
            string Pwd = string.Empty;
            Pwd = CreateRandomPassword(6);

            if (name.Length >= 2)
            {
                val = CreatePassOnlyTwoDigit(2, name);
            }
            else
            {
                val = "lh";
            }
            
            Password = val + "" + Pwd;

            /*** Create Random TLHNO ***/

            TLHNO = CreateRandomTLHNO(4);
            TLHNO = "TLHNO" + TLHNO;

            /*** End Random LHNO ***/

            string ActivationCode = Guid.NewGuid().ToString();

            if (DOB != "")
            {
                DOB = DateTime.ParseExact(DOB, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
            }
            

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "APP_SIGN_UP");
            cmd.Parameters.AddWithValue("@LASTNAME", Surname);
            cmd.Parameters.AddWithValue("@FIRSTNAME", FirstName);
            cmd.Parameters.AddWithValue("@MIDDLENAME", MiddleName);
            cmd.Parameters.AddWithValue("@SEX", Sex);
            cmd.Parameters.AddWithValue("@DATEOFBIRTH", DOB);
            cmd.Parameters.AddWithValue("@MARITALSTATUS", MaritalStatus);
            cmd.Parameters.AddWithValue("@RELIGION", Religion);
            cmd.Parameters.AddWithValue("@OCCUPATION", Occupation);
            cmd.Parameters.AddWithValue("@RESIDENCE", AlternateContactNo);
            cmd.Parameters.AddWithValue("@ADDRESS", Address);
            cmd.Parameters.AddWithValue("@MOBILENO", MobileNo);
            cmd.Parameters.AddWithValue("@EMAIL", Email);
            cmd.Parameters.AddWithValue("@PASSWORD", Password);
            cmd.Parameters.AddWithValue("@CITY", City);
            cmd.Parameters.AddWithValue("@STATE", State);
            cmd.Parameters.AddWithValue("@COUNTRY", Country);
            cmd.Parameters.AddWithValue("@PINCODE", Pincode);
            cmd.Parameters.AddWithValue("@LHNO", TLHNO);
            cmd.Parameters.AddWithValue("@UserName", UserName);
            cmd.Parameters.AddWithValue("@TITLE", Title);
            cmd.Parameters.AddWithValue("@BloodGroup", BloodGroup);
            cmd.Parameters.AddWithValue("@LH_NO", LHNO);
            cmd.Parameters.AddWithValue("@DEVICETYPE", DeviceType);
            cmd.Parameters.AddWithValue("@DEVICEID", DeviceId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = new Dictionary<string, object>();

            if (dt.Rows.Count > 0)
            {
                //HttpContext.Current.Session["msg"] = "Success";

                string AddressId = dt.Rows[0]["ADDRESSINDEX"].ToString();
                AddUserActivation(AddressId, ActivationCode);

                /*** SMS Verification ***/
                UserSMSVerificaton(MobileNo, UserName, Password, ActivationCode);
                /*** SMS Verification End ***/

                /*** Email Send ***/
                uti.SendEmail(Email, Password, ActivationCode, TLHNO, FirstName, UserName);
                /*** End ***/

                row.Add("Message", "Thank you for registration with Lilavati Hospital. Your user-Id and password is sent to your email address / mobile no.");
                row.Add("Status", true);
                rows.Add(row);
            }
            else
            {
                row.Add("Message", "Registration failed.");
                row.Add("Status", false);
                rows.Add(row);
            }

            this.Context.Response.ContentType = "application/json; charset=utf-8";
            this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
        }
        
    }

    public static string CreateRandomPassword(int PasswordLength)
    {
        try
        {
            //string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";        
            string _allowedChars = "0123456789";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public static string CreatePassOnlyTwoDigit(int PasswordLength, string Name)
    {
        try
        {
            string _allowedChars = Name;

            string chars = string.Empty;
            for (int i = 0; i < _allowedChars.Length; i++)
            {
                if (i >= 0 && i < 2)
                {
                    chars += _allowedChars[i].ToString();
                }
            }
            return chars.ToLower();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static string CreateRandomTLHNO(int PasswordLength)
    {
        try
        {
            //string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";        
            string _allowedChars = "0123456789" + DateTime.Now.Second.ToString("00");
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UserSMSVerificaton(string strMobileNo, string strUserId, string strPwd, string strCode)
    {
        try
        {
            string TO = strMobileNo;
            string username = "9920874020";
            string password = "jddgd";
            string msg = "UserID : " + strUserId + "\n" + "Pwd : " + strPwd + "\n" + "Pls click http://lilavatihospital.com/Act?Ac=" + strCode + "\n" + "to activate your account.";

            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create("http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=341739&username=" + username + "&password=" + password + "&To=" + TO + "&Text=" + msg);
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();
            //this.Title = "Sms Send SuccessFully";
        }
        catch (Exception ex)
        {
            //this.Title = ex.Message;           
        }
    }

    public string AddUserActivation(string AddressID, string ActivationCode)
    {
        string result = "";
        using (SqlCommand cmd = new SqlCommand("Proc_UserActivation"))
        {            
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "APP_SIGN_UP");
            cmd.Parameters.AddWithValue("@ACTIVATIONCODE", ActivationCode);
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", AddressID);
            if (uti.ExecuteOnlineLHRC(cmd))
            {
                result = "Done";
            }
            else
            {
                result = "failed";
            }
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AreYouRegiteredWithLHRCHospital(string LHNO,string MobileNo,string EmailId)
    {
        DataSet ds = new DataSet();
        ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");
        ds = obj.RegistrationdetailGet(LHNO, MobileNo, EmailId);

        JavaScriptSerializer serializer = new JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row = new Dictionary<string, object>();

        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            DataTable dt1 = new DataTable();
            dt1 = uti.Display("select * from GAadAddressMaster where LH_NO = '" + dt.Rows[0]["RegistrationNumber"].ToString() + "'");
            if (dt1.Rows.Count > 0)
            {

                row.Add("Message", "This information is already registered.");
                row.Add("Status", false);
                rows.Add(row);
            }
            else
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("Title", typeof(string));
                dt2.Columns.Add("FirstName", typeof(string));
                dt2.Columns.Add("MiddleName", typeof(string));
                dt2.Columns.Add("LastName", typeof(string));
                dt2.Columns.Add("Email", typeof(string));
                dt2.Columns.Add("DateOfBirth", typeof(string));
                dt2.Columns.Add("Sex", typeof(string));
                dt2.Columns.Add("BloodGroup", typeof(string));
                dt2.Columns.Add("MaritalStatus", typeof(string));
                dt2.Columns.Add("Religion", typeof(string));
                dt2.Columns.Add("Occupation", typeof(string));
                dt2.Columns.Add("Address", typeof(string));
                dt2.Columns.Add("Country", typeof(string));
                dt2.Columns.Add("State", typeof(string));
                dt2.Columns.Add("City", typeof(string));
                dt2.Columns.Add("PinCode", typeof(string));
                dt2.Columns.Add("MobileNo", typeof(string));
                dt2.Columns.Add("Residence", typeof(string));
                dt2.Columns.Add("LH_NO", typeof(string));

                string Sex = string.Empty;
                if (dt.Rows[0]["Sex"].ToString() == "M")
                {
                    Sex = "Male";
                }
                else if (dt.Rows[0]["Sex"].ToString() == "F")
                {
                    Sex = "Female";
                }
                else
                {

                }

                dt2.Rows.Add(dt.Rows[0]["Title"].ToString(), dt.Rows[0]["FirstName"].ToString(), dt.Rows[0]["MiddleName"].ToString(), dt.Rows[0]["LastName"].ToString(), dt.Rows[0]["Email"].ToString(), dt.Rows[0]["DOB"].ToString(), Sex, dt.Rows[0]["BloodGroup"].ToString(), dt.Rows[0]["MaritalStatus"].ToString(), dt.Rows[0]["Religion"].ToString(), dt.Rows[0]["Occupation"].ToString(), dt.Rows[0]["Address"].ToString(), dt.Rows[0]["Country"].ToString(), dt.Rows[0]["State"].ToString(), dt.Rows[0]["City"].ToString(), dt.Rows[0]["PinCode"].ToString(), dt.Rows[0]["MobileNo"].ToString(), dt.Rows[0]["TelNoResidence"].ToString(), dt.Rows[0]["RegistrationNumber"].ToString());

                foreach (DataRow dr in dt2.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt2.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }

                    row.Add("Status", true);
                    rows.Add(row);
                }                
            }           
        }
        else
        {
            row.Add("Message", "The information that you have provided does not match our records.");
            row.Add("Status", false);
            rows.Add(row);
        }

        this.Context.Response.ContentType = "application/json; charset=utf-8";
        this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));

    }
    
    /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx Find Doctor xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void FindDoctor_GetSpeciality()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'APP_FIND_DOC_GET_SPECIALTY_DDL'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["SpecialityId"].ToString() != "61")
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        row.Add("Status", true);
                        rows.Add(row);
                    }
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void FindDoctor_BySpeciality(string SpecialityId)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'APP_FIND_DOC_GET_DR_BY_SPECIALTY',0,0,'" + SpecialityId + "'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void FindDoctor_SearchByDoctorList(string DoctorName)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetDoctorSpecialityMaster"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "APP_FIND_DOC_SEARCH_BY_DOC");
                cmd.Parameters.AddWithValue("@searchKey", DoctorName);
                DataTable dt = new DataTable();
                dt = uti.Display(cmd);
                if (dt.Rows.Count > 0)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;

                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        row.Add("Status", true);
                        rows.Add(row);
                    }

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
                else
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = new Dictionary<string, object>();

                    row.Add("Message", "Record not found.");
                    row.Add("Status", false);
                    rows.Add(row);

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
            }

        }
        catch (Exception ex)
        {            
            throw ex;
        }        
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void FindDoctor_ViewDoctorProfile(string DoctorId)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetDoctorSpecialityMaster"))
            {
                DataTable dt = new DataTable();
                dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'APP_FIND_DOC_VIEW_DETAILS',0,'" + DoctorId + "'");
                if (dt.Rows.Count > 0)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;

                    int count = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (count == 0)
                        {
                            count++;
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                string Speciality = string.Empty;
                                //if (col.ColumnName == "SpecialityName")
                                //{
                                //    //foreach (DataRow dr1 in dt.Rows)
                                //    //{
                                //    //    Speciality += dr1["SpecialityName"].ToString() + ", ";
                                //    //}
                                //    //Speciality = Speciality.Substring(0, Speciality.Length - 2);
                                //    //dr[col] = "Department of " + Speciality;
                                //}

                                if (col.ColumnName == "Mon")
                                {
                                    string Mon = BindMondayTime(dr["DoctorId"].ToString());
                                    dr[col] = Mon;
                                }
                                if (col.ColumnName == "Tue")
                                {
                                    string Tue = BindTuesdayTime(dr["DoctorId"].ToString());
                                    dr[col] = Tue;
                                }
                                if (col.ColumnName == "Wed")
                                {
                                    string Wed = BindWednesdayTime(dr["DoctorId"].ToString());
                                    dr[col] = Wed;
                                }
                                if (col.ColumnName == "Thu")
                                {
                                    string Thu = BindThursdayTime(dr["DoctorId"].ToString());
                                    dr[col] = Thu;
                                }
                                if (col.ColumnName == "Fri")
                                {
                                    string Fri = BindFridayTime(dr["DoctorId"].ToString());
                                    dr[col] = Fri;
                                }
                                if (col.ColumnName == "Sat")
                                {
                                    string Sat = BindSaturdayTime(dr["DoctorId"].ToString());
                                    dr[col] = Sat;
                                }

                                row.Add(col.ColumnName, dr[col]);
                            }
                            row.Add("Status", true);
                            rows.Add(row);
                        }
                    }

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
                else
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = new Dictionary<string, object>();

                    row.Add("Message", "Record not found.");
                    row.Add("Status", false);
                    rows.Add(row);

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string BindMondayTime(string id)
    {
        DataTable dt = new DataTable();        
        dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string mondaytime = dt.Rows[0]["AVAIL_TIME_MON_VCR"].ToString();
        char[] monday = new char[] { '#' };
        string[] monday1;
        string monday2 = string.Empty;
        if (mondaytime.Contains("#") == true)
        {
            monday1 = mondaytime.Split(monday);
            foreach (string time in monday1)
            {
                monday2 += time.ToString() + " | ";
            }
            monday2 = monday2.Substring(0, monday2.Length - 4);
        }
        else
        {
            monday2 = dt.Rows[0]["AVAIL_TIME_MON_VCR"].ToString();
            string checkvalue = dt.Rows[0]["Avail_Time_Mon_vcr1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                monday2 += " | " + dt.Rows[0]["Avail_Time_Mon_vcr1"].ToString();
                monday2 = monday2.Replace("\t", "");
            }
            else
            {

            }
        }
        return monday2.ToUpper();
    }
    public string BindTuesdayTime(string id)
    {
        DataTable dt = new DataTable();        
        dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string tuesdaytime = dt.Rows[0]["AVAIL_TIME_TUE_VCR"].ToString();
        char[] tuesday = new char[] { '#' };
        string[] tuesday1;
        string tuesday2 = string.Empty;
        if (tuesdaytime.Contains("#") == true)
        {
            tuesday1 = tuesdaytime.Split(tuesday);
            foreach (string time in tuesday1)
            {
                tuesday2 += time.ToString() + " | ";
            }
            tuesday2 = tuesday2.Substring(0, tuesday2.Length - 4);
        }
        else
        {
            tuesday2 = dt.Rows[0]["AVAIL_TIME_TUE_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_TUE_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                tuesday2 += " | " + dt.Rows[0]["AVAIL_TIME_TUE_VCR1"].ToString();
                tuesday2 = tuesday2.Replace("\t", "");
            }
            else
            {

            }


        }
        return tuesday2.ToUpper();
    }
    public string BindWednesdayTime(string id)
    {
        DataTable dt = new DataTable();        
        dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");


        string wednesdaytime = dt.Rows[0]["AVAIL_TIME_WED_VCR"].ToString();
        char[] wednesday = new char[] { '#' };
        string[] wednesday1;
        string wednesday2 = string.Empty;
        if (wednesdaytime.Contains("#") == true)
        {
            wednesday1 = wednesdaytime.Split(wednesday);
            foreach (string time in wednesday1)
            {
                wednesday2 += time.ToString() + " | ";
            }
            wednesday2 = wednesday2.Substring(0, wednesday2.Length - 4);
        }
        else
        {
            wednesday2 = dt.Rows[0]["AVAIL_TIME_WED_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_WED_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                wednesday2 += " | " + dt.Rows[0]["AVAIL_TIME_WED_VCR1"].ToString();
                wednesday2 = wednesday2.Replace("\t", "");
            }
            else
            {

            }

        }
        return wednesday2.ToUpper();
    }
    public string BindThursdayTime(string id)
    {
        DataTable dt = new DataTable();        
        dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string thursdaytime = dt.Rows[0]["AVAIL_TIME_THU_VCR"].ToString();
        char[] thursday = new char[] { '#' };
        string[] thursday1;
        string thursday2 = string.Empty;
        if (thursdaytime.Contains("#") == true)
        {
            thursday1 = thursdaytime.Split(thursday);
            foreach (string time in thursday1)
            {
                thursday2 += time.ToString() + " | ";
            }
            thursday2 = thursday2.Substring(0, thursday2.Length - 4);
        }
        else
        {
            thursday2 = dt.Rows[0]["AVAIL_TIME_THU_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_THU_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                thursday2 += " | " + dt.Rows[0]["AVAIL_TIME_THU_VCR1"].ToString();
                thursday2 = thursday2.Replace("\t", "");
            }
            else
            {

            }
        }
        return thursday2.ToUpper();
    }
    public string BindFridayTime(string id)
    {
        DataTable dt = new DataTable();        
        dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string fridaytime = dt.Rows[0]["AVAIL_TIME_FRI_VCR"].ToString();
        char[] friday = new char[] { '#' };
        string[] friday1;
        string friday2 = string.Empty;
        if (fridaytime.Contains("#") == true)
        {
            friday1 = fridaytime.Split(friday);
            foreach (string time in friday1)
            {
                friday2 += time.ToString() + " | ";
            }
            friday2 = friday2.Substring(0, friday2.Length - 4);
        }
        else
        {
            friday2 = dt.Rows[0]["AVAIL_TIME_FRI_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_FRI_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                friday2 += " | " + dt.Rows[0]["AVAIL_TIME_FRI_VCR1"].ToString();
                friday2 = friday2.Replace("\t", "");
            }
            else
            {

            }
        }
        return friday2.ToUpper();
    }
    public string BindSaturdayTime(string id)
    {
        DataTable dt = new DataTable();        
        dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_TIME_FOR_TEMPTIME',0,'" + id + "'");

        string saturdaytime = dt.Rows[0]["AVAIL_TIME_SAT_VCR"].ToString();
        char[] saturday = new char[] { '#' };
        string[] saturday1;
        string saturday2 = string.Empty;
        if (saturdaytime.Contains("#") == true)
        {
            saturday1 = saturdaytime.Split(saturday);
            foreach (string time in saturday1)
            {
                saturday2 += time.ToString() + " | ";
            }
            saturday2 = saturday2.Substring(0, saturday2.Length - 4);
        }
        else
        {
            saturday2 = dt.Rows[0]["AVAIL_TIME_SAT_VCR"].ToString();

            string checkvalue = dt.Rows[0]["AVAIL_TIME_SAT_VCR1"].ToString();
            if (checkvalue != "N.A" && checkvalue != "N.A." && checkvalue != "----" && checkvalue != "")
            {
                saturday2 += " | " + dt.Rows[0]["AVAIL_TIME_SAT_VCR1"].ToString();
                saturday2 = saturday2.Replace("\t", "");
            }
            else
            {

            }
        }
        return saturday2.ToUpper();
    }

    /*xxxxxxxxxxxxxxxxxxxxxxxxx Book An Appointment xxxxxxxxxxxxxxxxxxxxxx*/
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_GetSpeciality()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("EXEC AddUpdateGetSpecialtyMaster 'APP_BOOK_AN_APP_GET_SPECIALITY'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_DrListBySpeciality(string SpecialityId)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'APP_BOOK_AN_APP_DR_LIST_BY_SPECIALITY',0,0,'" + SpecialityId + "'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_AvailableTiming(string DoctorId)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("EXEC AddUpdateGetDoctorMaster 'APP_BOOK_AN_APP_Get_By_Doc_Id_For_Apmnt','" + DoctorId + "'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        string value = dr[col].ToString();
                        value = value.Replace("\t", "");
                        row.Add(col.ColumnName, value);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_CheckDoctorAvailability(string Date, string ConsDocId)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = CheckDoctorAvailability(Date, ConsDocId);

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();

                    row.Add("Message", "We apologize for the inconvenience " + dt.Rows[0]["Dr Name"].ToString() +" is not available from " + dt.Rows[0]["FromDate"].ToString() + " to " + dt.Rows[0]["TODATE"].ToString() + " date.");
                    row.Add("Status", false);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Doctor is available.");
                row.Add("Status", true);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    public DataTable CheckDoctorAvailability(string getdate, string drconsid)
    {
        ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");

        obj.DoctorAvailabilityGet(getdate, drconsid);

        DataSet ds = new DataSet();
        ds = obj.DoctorAvailabilityGet(getdate, drconsid);

        DataTable dt = ds.Tables[0];

        return dt;       
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_PreferredTime(string AvailableTime, string DoctorId, string Date)
    {        
        string timeDiff = string.Empty;
        try
        {
            /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx----get day and time start----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            
            string Daywisetime = string.Empty;
            char[] times = { '-', ' ' };

            #region Store value of Dropdown in String

            if (AvailableTime != "" && AvailableTime != null)
            {
                Daywisetime = AvailableTime;
            }
            else
            {
                Daywisetime = AvailableTime;
            }
            #endregion

            #region Replacement mon,tue,wed,thur,frid,sat

            Daywisetime = Daywisetime.Replace("Mon :", "").Replace("Tue :", "").Replace("Wed :", "").Replace("Thu :", "").Replace("Fri :", "").Replace("Sat :", "").Trim();

            #endregion


            #region get actual from and to time

            string[] timesplit = Daywisetime.Split(times);
            string timestore = string.Empty;
            string timestoreampm = string.Empty;            

            foreach (string time in timesplit)
            {
                if (time == "0" || time == "1" || time == "2" || time == "3" || time == "4" || time == "5" || time == "6" || time == "7" || time == "8" || time == "9" || time == "10" || time == "11" || time == "12")
                {
                    timestore += time + ",";                    
                }
                else
                {
                    timestoreampm += time + ",";
                }
            }
            timestore = timestore.Substring(0, timestore.Length - 1);
            timestoreampm = timestoreampm.Substring(0, timestoreampm.Length - 1);
            #endregion
            /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx----get day and time End----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

            #region diffrence between to and from time

            string to = string.Empty;
            string from = string.Empty;
            string toampm = string.Empty;
            string fromampm = string.Empty;

            char[] tofromsplit = { ',' };

            string[] tofromtimes = timestore.Split(tofromsplit);
            string[] tofromtimesampm = timestoreampm.Split(tofromsplit);

            foreach (string tofromtime in tofromtimes)
            {
                if (from == "")
                {
                    from = tofromtime.ToString();
                }
                else if (to == "")
                {
                    to = tofromtime.ToString();
                }
                else
                {

                }
            }

            foreach (string tofromtimeampm in tofromtimesampm)
            {
                if (fromampm == "")
                {
                    fromampm = tofromtimeampm.ToString();
                }
                else if (toampm == "")
                {
                    toampm = tofromtimeampm.ToString();
                }
                else
                {

                }
            }

            string sDateFrom = from.ToString() + ":00:00 " + fromampm.ToString().ToUpper();
            string sDateTo = to.ToString() + ":00:00 " + toampm.ToString().ToUpper(); ;
                      
            #endregion


            #region set value in dropdown list

            SqlCommand cmd = new SqlCommand("select * from DoctorMaster where Doctor_Id_bint ='" + DoctorId + "'");
            DataTable dt = new DataTable();
            dt = uti.Display(cmd);

            if (dt.Rows.Count > 0)
            {
                DateTime start = DateTime.Parse(sDateFrom);
                DateTime end = DateTime.Parse(sDateTo);
                string slot = dt.Rows[0]["Slotbooking"].ToString();
                double duration = double.Parse(slot);

                List<string> listslot = new List<string>();

                while (true)
                {
                    DateTime dtNext = start.AddMinutes(duration);
                    if (start > end || dtNext > end)
                        break;
                    if (start < DateTime.Parse("12:00 PM"))
                    {
                        listslot.Add(start.ToShortTimeString() + "-" + dtNext.ToShortTimeString());

                    }
                    else
                    {
                        listslot.Add(start.ToShortTimeString() + "-" + dtNext.ToShortTimeString());
                    }
                    start = dtNext;
                }

                if (listslot.Count > 0)
                {
                    string date = DateTime.ParseExact(Date, "dd/MM/yyyy", null).ToString("dd-MM-yyyy");

                    //SqlCommand cmd1 = new SqlCommand("select App_Visit_Time from OnlineAppointmentMaster where convert(varchar(10),App_Date,105)='" + date + "' and ConsDocId='" + ConsDocId + "' and IsActive=1");
                    SqlCommand cmd1 = new SqlCommand("select App_Visit_Time from OnlineAppointmentMaster where convert(varchar(10),App_Date,105)='" + date + "' and Doctor_Id_bint='" + DoctorId + "' and IsActive=1");

                    DataTable dt1 = new DataTable();
                    dt1 = uti.DisplayOnlineLHRC(cmd1);
                    List<string> slotrowas = new List<string>();
                    if (dt1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt1.Rows)
                        {
                            slotrowas.Add(dr["App_Visit_Time"].ToString());
                            listslot.Remove(dr["App_Visit_Time"].ToString());
                        }
                    }

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;

                    foreach (string lists in listslot)
                    {
                        row = new Dictionary<string, object>();

                        row.Add("PreferredTime", lists);
                        row.Add("Status", true);
                        rows.Add(row);
                    }

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));

                }
                else
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = new Dictionary<string, object>();

                    row.Add("Message", "Record not found.");
                    row.Add("Status", false);
                    rows.Add(row);

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }


            /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx----get diffrence between to and from time End----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            #endregion
        }
        catch (Exception ex)
        {           
            throw ex;
        }
        
    }

    string attachmentname = string.Empty;
    
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_BookAppointment(string AddressIndex, string SpecialityId, string SpecialityName, string DoctorId, string DoctorName, string AvailableTime, string Date, string PreferredTime, string ConsDocId, string OrderId, string TrackingId, string BankrefNo, string OrderStatus, string PaymentMode, string CardName, string Amount, string Currency)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_OnlineAppointmentMaster"))
            {
                string Datess = string.Empty;
                if (Date != "")
                {
                    Datess = DateTime.ParseExact(Date, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "APP_BOOK_AN_APP_ADD");
                cmd.Parameters.AddWithValue("@SPECIALITY_ID_BINT", SpecialityId);
                cmd.Parameters.AddWithValue("@DOCTOR_ID_BINT", DoctorId);
                cmd.Parameters.AddWithValue("@ADDRESSINDEX", AddressIndex);
                cmd.Parameters.AddWithValue("@APP_AVAI_TIME", AvailableTime);
                cmd.Parameters.AddWithValue("@APP_DATE", Datess);
                cmd.Parameters.AddWithValue("@APP_VISIT_TIME", PreferredTime);
                cmd.Parameters.AddWithValue("@SPECIALITY_VCR", SpecialityName);
                cmd.Parameters.AddWithValue("@DOCTOR_NAME_VCR", DoctorName);
                cmd.Parameters.AddWithValue("@CONSDOCID", ConsDocId);
                cmd.Parameters.AddWithValue("@APP_STATUS", "Confirmed");

                if (uti.ExecuteOnlineLHRC(cmd))
                {
                    string orderid = OrderId;
                    string trackingid = TrackingId;
                    string bankrefno = BankrefNo;
                    string order_status = OrderStatus;
                    string paymentmode = PaymentMode;
                    string card_name = CardName;
                    string amount = Amount;
                    string currency = Currency;
                    string patiestid = AddressIndex;
                    string doctorname = DoctorName;
                    string appointmentdate = Date;
                    string doctorconsid = ConsDocId;

                    if (appointmentdate != "")
                    {
                        appointmentdate = DateTime.ParseExact(appointmentdate, "dd/MM/yyyy", null).ToString("yyyy/MM/dd");
                    }

                    string InsertResult = InsertIntoPaymentTable(patiestid, doctorname, orderid, trackingid, bankrefno, order_status, paymentmode, card_name, amount, currency);
                    // string InsertResult = "success";
                    if (InsertResult == "success")
                    {

                        DataTable getpatientdetail = new DataTable();
                        getpatientdetail = uti.Display("Proc_PCipopRegistrationMaster 'GET_BY_ID','" + patiestid + "'");
                        if (getpatientdetail.Rows.Count > 0)
                        {
                            #region Call Web services and update LHNO into database

                            ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");
                            DataSet ds = new DataSet();
                            //ds = obj.webappointmentprocess("LH00057131", "Mr", "Sunil", "Dinesh", "Tiwari", "Male", "1994/05/04", "Single", "B+", "Hindu", "Indian", "India", "Software Developer","TLHNO6603", "Mahalaxmi", "Mumbai", "Maharashtra", "India", "400011", "9594907366", "sunil.t@kwebmaker.com", "9594907366", 0,"1210318", "2017/05/04", "106203791411", "");
                            ds = obj.webappointmentprocess(getpatientdetail.Rows[0]["LH_NO"].ToString(), getpatientdetail.Rows[0]["TITLE"].ToString(), getpatientdetail.Rows[0]["FIRSTNAME"].ToString(), getpatientdetail.Rows[0]["MIDDLENAME"].ToString(), getpatientdetail.Rows[0]["LASTNAME"].ToString(), getpatientdetail.Rows[0]["SEX"].ToString(), getpatientdetail.Rows[0]["DATEOFBIRTHNEW"].ToString(), getpatientdetail.Rows[0]["MARITALSTATUS"].ToString(), getpatientdetail.Rows[0]["BLOODGROUP"].ToString(), getpatientdetail.Rows[0]["RELIGION"].ToString(), getpatientdetail.Rows[0]["NATIONALITY"].ToString(), getpatientdetail.Rows[0]["RESIDENCE"].ToString(), getpatientdetail.Rows[0]["OCCUPATION"].ToString(), getpatientdetail.Rows[0]["LHNO"].ToString(), getpatientdetail.Rows[0]["ADDRESS"].ToString(), getpatientdetail.Rows[0]["CITY"].ToString(), getpatientdetail.Rows[0]["STATE"].ToString(), getpatientdetail.Rows[0]["COUNTRY"].ToString(), getpatientdetail.Rows[0]["PINCODE"].ToString(), getpatientdetail.Rows[0]["TELNORESIDENCE"].ToString(), getpatientdetail.Rows[0]["EMAIL"].ToString(), getpatientdetail.Rows[0]["MOBILENO"].ToString(), float.Parse(amount), doctorconsid, appointmentdate, orderid, "");
                            DataTable dt123 = ds.Tables[0];
                            if (dt123.Columns.Contains("output"))
                            {
                                using (SqlCommand cmdfailed = new SqlCommand("Proc_FailedAppointment"))
                                {
                                    cmdfailed.CommandType = CommandType.StoredProcedure;
                                    cmdfailed.Parameters.AddWithValue("@PARA", "ADD");
                                    cmdfailed.Parameters.AddWithValue("@ADDRESSINDEX", AddressIndex);
                                    cmdfailed.Parameters.AddWithValue("@DOCTORNAME", DoctorName);
                                    cmdfailed.Parameters.AddWithValue("@SPECIALITYNAME", SpecialityName);
                                    cmdfailed.Parameters.AddWithValue("@DATEOFAPP", Date);
                                    cmdfailed.Parameters.AddWithValue("@AVAILABLETIME", AvailableTime);
                                    cmdfailed.Parameters.AddWithValue("@PREFERREDTIME", PreferredTime);
                                    cmdfailed.Parameters.AddWithValue("@AMOUNT", Amount);
                                    cmdfailed.Parameters.AddWithValue("@CONSDOCID", ConsDocId);
                                    cmdfailed.Parameters.AddWithValue("@ORDERID", OrderId);
                                    cmdfailed.Parameters.AddWithValue("@FAILEDFROM", "Appp");
                                    uti.Execute(cmdfailed);
                                }
                            }
                            else
                            {
                                using (SqlCommand cmdnew = new SqlCommand("Proc_PCipopRegistrationMaster"))
                                {
                                    cmdnew.CommandType = CommandType.StoredProcedure;
                                    cmdnew.Parameters.AddWithValue("@PARA", "UPDATE_OG_LHNO_AFTER_WEBSERVICES_RESP");
                                    cmdnew.Parameters.AddWithValue("@ADDRESSINDEX", patiestid);
                                    if (dt123.Rows[0]["RegistrationNumber"] != "")
                                    {
                                        cmdnew.Parameters.AddWithValue("@LH_NO", dt123.Rows[0]["RegistrationNumber"].ToString());
                                    }
                                    else
                                    {
                                        cmdnew.Parameters.AddWithValue("@LH_NO", "-");
                                    }
                                    uti.Execute(cmdnew);

                                    CreateInvoiceForAppointment(dt123.Rows[0]["Reg NO"].ToString(), dt123.Rows[0]["Op No"].ToString(), dt123.Rows[0]["Doc No"].ToString(), dt123.Rows[0]["RegistrationNumber"].ToString(), dt123.Rows[0]["Firstname"].ToString(), dt123.Rows[0]["Middlename"].ToString(), dt123.Rows[0]["Lastname"].ToString(), doctorname, Date, PreferredTime, Amount);

                                    #region send mail with attachment to patient
                                    DataTable dt = new DataTable();
                                    dt = uti.Display("SELECT * FROM GAADADDRESSMASTER WHERE ADDRESSINDEX = '" + int.Parse(AddressIndex) + "'");
                                    if (dt.Rows.Count > 0)
                                    {
                                        string EMailTo = dt.Rows[0]["Email"].ToString();
                                        BookAnAppointment appointment = new BookAnAppointment();
                                        appointment.EmailAppointment(SpecialityName, DoctorName, AvailableTime, Date, PreferredTime, EMailTo, attachmentname);
                                    }

                                    #endregion

                                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                                    Dictionary<string, object> row = new Dictionary<string, object>();

                                    row.Add("Message", "Appointment Fixed Successfully.");
                                    row.Add("Status", true);
                                    rows.Add(row);

                                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                                }
                            }
                            #endregion                 
                        }        

                    }
                    else
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row = new Dictionary<string, object>();

                        row.Add("Message", "Appointment Could Not Fixed Successfully.");
                        row.Add("Status", false);
                        rows.Add(row);

                        this.Context.Response.ContentType = "application/json; charset=utf-8";
                        this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                    }
                }
                else
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row = new Dictionary<string, object>();

                    row.Add("Message", "Appointment Could Not Fixed Successfully.");
                    row.Add("Status", false);
                    rows.Add(row);

                    this.Context.Response.ContentType = "application/json; charset=utf-8";
                    this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region Payment  Process
    public string InsertIntoPaymentTable(string patientid, string DrName, string orderid, string trackingid, string bankrefno, string order_status, string paymentmode, string card_name, string amount, string currency)
    {
        string response = "";

        using (SqlCommand cmd = new SqlCommand("Proc_PaymentTable"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "add");
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", patientid);
            cmd.Parameters.AddWithValue("@DOCTOR_NAME_VCR", DrName);
            cmd.Parameters.AddWithValue("@ORDER_ID", orderid.ToString());
            cmd.Parameters.AddWithValue("@TRACKING_ID", trackingid.ToString());
            cmd.Parameters.AddWithValue("@BANK_REF_NO", bankrefno.ToString());
            cmd.Parameters.AddWithValue("@ORDER_STATUS", order_status.ToString());
            cmd.Parameters.AddWithValue("@PAYMENT_MODE", paymentmode.ToString());
            cmd.Parameters.AddWithValue("@CARD_NAME", card_name.ToString());
            cmd.Parameters.AddWithValue("@AMOUNT", amount.ToString());
            cmd.Parameters.AddWithValue("@CURRENCY", currency.ToString());
            if (uti.Execute(cmd))
            {
                response = "success";
            }
            else
            {
                response = "fail";
            }
            return response;
        }
    }
       
    #endregion endprocess

    #region Send mail and attachment

    public void CreateInvoiceForAppointment(string regno, string opno, string docno, string lhno, string fname, string middlename, string lastname, string docname, string AppDate, string PreferredTime,string Amount)
    {
        #region Create invoice for PDF by Arvind
        StringBuilder ltrlpatientinvoice = new StringBuilder();
        #region get data into litrel
        string regno1 = regno; string opno1 = opno; string docno1 = docno; string lhno1 = lhno; string fname1 = fname; string middlename1 = middlename; string lastname1 = lastname; string docname1 = docname;
        ltrlpatientinvoice.Append(AppointmentInvoiceStructure(regno1, opno1, docno1, lhno1, fname1, middlename1, lastname1, docname1, AppDate, PreferredTime, Amount));
        #endregion

        string strFileName = fname + "_" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + ".pdf";

        //HttpContext.Current.Session["attachmentname"] = strFileName;
        attachmentname = strFileName;

        //string fileName = "pdfDocument" + DateTime.Now.Ticks + ".pdf";
        //string strPath = Request.PhysicalApplicationPath + "\\PDF\\" + fileName;

        StringReader sr = new StringReader(ltrlpatientinvoice.ToString());
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

        var output = new FileStream(Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads\\visa_invitation\\", strFileName), FileMode.Create);

        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, output);
        pdfDoc.Open();
        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
        pdfDoc.Close();

        #endregion
    }

    public string AppointmentInvoiceStructure(string regno, string opno, string docno, string lhno, string fname, string middlename, string lastname, string docname, string AppDate, string PreferredTime, string Amount)
    {
        if (regno == "") { regno = "-"; } if (opno == "") { opno = "-"; } if (docno == "") { docno = "-"; } if (lhno == "") { lhno = "-"; }

        fname = fname + " " + middlename + " " + lastname;


        /// DateTime dateofappointment = Convert.ToDateTime(Request.Cookies["App_date_dtm"].Value);
        string dateofappointment = AppDate + " " + PreferredTime;

        StringBuilder dbinvoice = new StringBuilder();


        dbinvoice.Append("<table id='tblMain' runat='server' width='100%' cellpadding='0' cellspacing='0' style='border:1px solid #ccc; border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; color:#333;font-size:5px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='10'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td><img src='http://lilavatihospital.com/images/invoice_logo.png' title='Lilavati Hospital & Research Center' width='300px' /></td>");
        dbinvoice.Append("<td width='50%' align='center' style='font-size:10px;' valign='middle'><strong>OPD Invoice</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center' height='25'><hr width='96%' color='#04869a' /></td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' cellpadding='5' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td width='39.33%'>Reg. No.: <strong>" + regno + "</strong></td>");
        dbinvoice.Append("<td width='30.33%'>OP No.: <strong>" + opno + "</strong></td>");
        dbinvoice.Append("<td width='30.33%'>Document No.: <strong>" + docno + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td colspan='2'>Patient Name: <strong>" + fname + "</strong></td>");
        dbinvoice.Append("<td>Date: <strong>" + dateofappointment + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td colspan='2'>Referring Dr: <strong>No Reference</strong></td>");
        dbinvoice.Append("<td>New LHNO: <strong>" + lhno + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='30'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='100%' cellpadding='7' style='font-size:10px;'>");
        dbinvoice.Append("<tr bgcolor='#04869a' style='background-color:#04869a;color:#fff;'>");
        dbinvoice.Append("<td><strong>Department</strong></td>");
        dbinvoice.Append("<td><strong>Charge Description</strong></td>");
        dbinvoice.Append("<td width='100'><strong>Qty.</strong></td>");
        dbinvoice.Append("<td><strong>Rs.</strong></td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td>OPD Services</td>");
        dbinvoice.Append("<td>Consultation - Hospital OPD<br />" + docname + "</td>");
        dbinvoice.Append("<td>1</td>");
        dbinvoice.Append("<td>" + Amount + "</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr bgcolor='#eeeeee' style='background-color:#eeeeee;color:#000;'>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td><strong>TOTAL (Rs.)</strong></td>");
        dbinvoice.Append("<td><strong>" + Amount + "</strong></td>");
        dbinvoice.Append("</tr>");


        //dbinvoice.Append("<tr>");
        //dbinvoice.Append("<td colspan='4'>Rupees one thousand five hundred only received in cash</td>");
        //dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("<hr color='#04869a' width='96%' />");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='15'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' cellpadding='5' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td width='69.66%'>Bill Prepared by: <strong>Web Appointment</strong></td>");
        dbinvoice.Append("<td width='30.33%'>Conf By: <strong>Web Appointment</strong></td>");
        dbinvoice.Append("</tr>");
        //dbinvoice.Append("<tr>");
        //dbinvoice.Append("<td>Pan No. <strong>AAATL1398Q</strong></td>");
        //dbinvoice.Append("<td></td>");
        //dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='50'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td valign='top' width='45'><strong>NOTE:</strong> </td>");
        dbinvoice.Append("<td>Please preserve this original invoice carefully and produce it while collecting your investgation reports.<br /> Enclosed Requisition Slip(s) should be handed over to the hospital departments where service is availed.</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td>This is an electronic generated invoice.</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='15'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");

        return dbinvoice.ToString();
    }

    #endregion

    /*xxxxxxxxxxxxxxxxxxxxx View Profile xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_ViewProfile(string AddressIndex)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("Exec Proc_PCipopRegistrationMaster 'APP_GET_VIEW_PROFILE_BY_ADDRESSINDEX','" + AddressIndex + "'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    /*xxxxxxxxxxxxxxxxxxxxx Appointment History xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_AppointmentList(string AddressIndex)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("Exec Proc_PCipopRegistrationMaster 'APP_APPOINTMENT_HISTORY_LIST','" + AddressIndex + "'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void BookAnAppointment_AppointmentListById(string AppointmentId)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = uti.Display("Exec Proc_PCipopRegistrationMaster 'APP_APPOINTMENT_HISTORY_LIST_BY_ID',0,'','','','',0,'','','','','','','','','','','','','','',0,'','','','','','','','','','','','" + AppointmentId + "'");

            if (dt.Rows.Count > 0)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;

                foreach (DataRow dr in dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    row.Add("Status", true);
                    rows.Add(row);
                }

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
            else
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row = new Dictionary<string, object>();

                row.Add("Message", "Record not found.");
                row.Add("Status", false);
                rows.Add(row);

                this.Context.Response.ContentType = "application/json; charset=utf-8";
                this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
     
}
