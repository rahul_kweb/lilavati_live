﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="media_press_releases.aspx.cs" Inherits="media_press_releases" %>

<%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">
        <div class="green_banner">
            <div class="page_title">Media/ Press Release</div>
        </div>

        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">
                              
                <div class="tabs_main">

                    <asp:Literal ID="litTab" runat="server"></asp:Literal>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptDesc" runat="server" OnItemDataBound="rptDesc_ItemDataBound">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnPUBLICATION" runat="server" Value='<%#Eval("PUBLICATION") %>' />
                            <div id='tab_<%#Eval("PUBLICATION") %>'>

                                <asp:Repeater ID="rptYearName" runat="server" OnItemDataBound="rptYearName_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="accd_box" id="divId" runat="server">

                                            <asp:HiddenField ID="hdnYear" runat="server" Value='<%#Eval("YEAR_NAME") %>' />
                                            <div class="hd expand_hd">YEAR <%#Eval("YEAR_NAME") %></div>

                                            

                                            <div class="newsbox accd_content expand_box">

                                                <asp:Repeater ID="rptMonth" runat="server" OnItemDataBound="rptMonth_ItemDataBound">
                                                    <ItemTemplate>

                                                        <h2 class="green_text uppercase">
                                                            <asp:HiddenField ID="hdnIdMonth" runat="server" Value='<%#Eval("Year_WiseName") %>' />
                                                            <%#Eval("Year_WiseName") %>
                                                        </h2>

                                                        <ul class="bullet">
                                                            <asp:Repeater ID="rptDescription" runat="server">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <%--<a href='<%#Eval("PDF_FILE","uploads/media_press/{0}") %>' target="_blank"><%#Eval("DESCRIPTION") %> </a>--%>
                                                                        <a class="<%#Eval("Publication").ToString()=="Video Gallery"?"fancybox-media":"" %>" href='<%#Eval("Publication").ToString()=="Video Gallery"?Eval("PDF_FILE"):Eval("PDF_FILE","uploads/media_press/{0}")  %>' target="_blank"><%#Eval("DESCRIPTION") %> </a>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                <div class="clear"></div>
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <div class="clear"></div>
                            </div>

                            <div class="clear"></div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>

                
                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox-media')
                     .attr('rel', 'media-gallery')
                     .fancybox({
                         openEffect: 'none',
                         closeEffect: 'none',
                         prevEffect: 'none',
                         nextEffect: 'none',

                         arrows: false,
                         helpers: {
                             thumbs: { width: 75, height: 50 },
                             media: {},
                             buttons: {}
                         }
                     });

        });
    </script>
</asp:Content>

