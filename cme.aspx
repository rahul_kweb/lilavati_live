﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="cme.aspx.cs" Inherits="cme" %>

<%@ Register TagName="Professionals" TagPrefix="menu" Src="Control/professionals.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital – Professionals | (CME) Continues Medical Education</title>
    <meta name="description" content="Maharashtra Medical Council (MMC) has accredited Lilavati Hospital for conducting Continues Medical Education to share the knowledge and to have an interactive session with the participants. ">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">CME</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <asp:Literal ID="litTable" runat="server"></asp:Literal>
                <%--   <p>
                    <strong class="uppercase green_text">Upcoming CME</strong>
                </p>
                <table>
                    <thead>
                        <tr>
                            <td>Sr.</td>
                            <td>Date &amp; Month</td>
                            <td>Topic</td>
                            <td>Department</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>28th Sept, 2016</td>
                            <td>Molecular Imaging in Infection &amp; Inflammation</td>
                            <td>Nuclear Medicine</td>
                        </tr>
                    </tbody>
                </table>--%>

                <%--<p>Lilavati Hospital is accredited Institute by Maharashtra Medical Council (MMC) for conducting Continues Medical Education. The events are conducted by expert consultants of their specialties to share the knowledge and to have an interactive session with the participants. The knowledge sharing is the main focus of the programme which enables the participants to get the recent and latest update on the modern health care developments through such academic programme. CME’s at Lilavati Hospital are conducted on Wednesdays and invitations are sent through SMS’ to the IMA registered members by the Association and from the hospital. The allocated credit hours are presented to the attendees along with a certificate.</p>

                <p>Upcoming CME - <a href="#">Click here</a>.</p>

                <p>
                    Interested members can send email on <a href="mailto:cme@lilavatihospital.com">cme@lilavatihospital.com</a> and register.
                </p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Professionals ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

