﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class packages_details : System.Web.UI.Page
{
    CMSBAL cmsbal = new CMSBAL();
    HealthPackagesDetails tblHealthPackagesDetails = new HealthPackagesDetails();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            BindContent();
            PackageName();
            RepeatePackageMenu();

            BindDropDownPackages();
            ddlPreferredPackage.Items.Insert(0, "");
        }
    }

    public void BindContent()
    {
        tblHealthPackagesDetails.Health_Id = int.Parse(Page.RouteData.Values["Id"].ToString());
        //tblHealthPackagesDetails.Health_Id = int.Parse(Request.QueryString["Id"].ToString());
        cmsbal.PackagesDetails(tblHealthPackagesDetails);

        litTittle.Text = tblHealthPackagesDetails.Tittle;
        lblPrice.Text = tblHealthPackagesDetails.Pack_Price;
        litDesc.Text = tblHealthPackagesDetails.Description;
        litNote.Text = tblHealthPackagesDetails.Note;

        DataTable dt = new DataTable();
        dt = cmsbal.PackagesDetails(tblHealthPackagesDetails);
        if (dt.Rows.Count > 0)
        {
            rptTabContent.DataSource = dt;
            rptTabContent.DataBind();

            rptTabContent1.DataSource = dt;
            rptTabContent1.DataBind();
        }

        if (tblHealthPackagesDetails.Tab_Name != "")
        {
            divRep.Visible = true;
            divDesc.Visible = false;
        }
        else
        {
            divRep.Visible = false;
            divDesc.Visible = true;
        }

        if (tblHealthPackagesDetails.Pack_Price == "0")
        {
            divPrice.Visible = false;
        }
        else
        {
            divPrice.Visible = true;
        }

    }

    public void PackageName()
    {
        //tblHealthPackagesDetails.Health_Id = int.Parse(Request.QueryString["Id"].ToString());
        tblHealthPackagesDetails.Health_Id = int.Parse(Page.RouteData.Values["Id"].ToString());
        cmsbal.PackagesName(tblHealthPackagesDetails);

        lblPackageName.Text = tblHealthPackagesDetails.Package_Name;
    }


    public void RepeatePackageMenu()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.PackagesMenu();

        if (dt.Rows.Count > 0)
        {
            rptPackageMenu.DataSource = dt;
            rptPackageMenu.DataBind();

            BindSingleDataOfHealthCheckup();
        }
    }


    protected void rptPackageMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnId = (HiddenField)e.Item.FindControl("hdnMenu");
            string value = hdnId.Value;

            //if (value == Request.QueryString["Id"].ToString())
            if (value == Page.RouteData.Values["Id"].ToString())
            {
                HtmlGenericControl cls = (HtmlGenericControl)e.Item.FindControl("liactive");

                cls.Attributes.Add("class", "active");
            }

        }
    }

    public void BindDropDownPackages()
    {
        // Hidden 23 and 24 Health_Id
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_HealthCheckupApp 'BIND_PACKAGE'");
        if (dt.Rows.Count > 0)
        {
            ddlPreferredPackage.DataSource = dt;
            ddlPreferredPackage.DataValueField = "Health_Id";
            ddlPreferredPackage.DataTextField = "Package_Name";
            ddlPreferredPackage.DataBind();
        }
        else
        {
            ddlPreferredPackage.DataSource = null;           
            ddlPreferredPackage.DataBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        MAkeAppointemt();
    }

    public void MAkeAppointemt()
    {
        using (SqlCommand cmd = new SqlCommand("Proc_HealthCheckupApp"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@NAME", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@DOB", DateTime.ParseExact(txtDOB.Text, "dd-MM-yyyy", null).ToString("MM/dd/yyyy"));
            cmd.Parameters.AddWithValue("@PREFERRED_DOA", DateTime.ParseExact(txtPreferredDOA.Text, "dd-MM-yyyy", null).ToString("MM/dd/yyyy"));
            cmd.Parameters.AddWithValue("@PREFERRD_PACKAGE", ddlPreferredPackage.SelectedItem.Text);
            cmd.Parameters.AddWithValue("@MOBILE_NO", txtContact.Text.Trim());
            cmd.Parameters.AddWithValue("@EMAIL_ID", txtEmail.Text.Trim());
            if (utility.Execute(cmd))
            {
                HealthCheckUpAppMail health = new HealthCheckUpAppMail();
                health.EmailHealthApp(txtName.Text.Trim(), ddlPreferredPackage.SelectedItem.Text, txtDOB.Text, txtPreferredDOA.Text, txtEmail.Text.Trim(), txtContact.Text.Trim());

                Reset();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "show", "ShowPopUp();", true);
            }
        }
    }

    public void Reset()
    {
        txtName.Text = string.Empty;
        txtDOB.Text = string.Empty;
        txtPreferredDOA.Text = string.Empty;
        ddlPreferredPackage.SelectedIndex = -1;
        txtContact.Text = string.Empty;
        txtEmail.Text = string.Empty;
    }

    protected void lnkbtndescriptionkeytest_Click(object sender, EventArgs e)
    {
        Session["tabactive"] = "DescriptionKeyTest";
        Response.Redirect("/Health-Checkup");
    }
    protected void lnkbtnInstruction_Click(object sender, EventArgs e)
    {
       
        Session["tabactive"] = "Instruction";
        Response.Redirect("/Health-Checkup");
    }

    public void BindSingleDataOfHealthCheckup()
    {       
        StringBuilder sb = new StringBuilder();
        Utility utility = new Utility();
        SqlCommand cmd = new SqlCommand("SELECT * FROM HEALTH_CHECKUP 	WHERE	ISACTIVE = 1 and Health_Id in (1,22,20) ORDER BY HEALTH_ID DESC");
        DataTable dt = new DataTable();
        dt = utility.Display(cmd);
        if (dt.Rows.Count > 0)
        {
            string checkurlvalue = Page.RouteData.Values["Id"].ToString();
            foreach (DataRow dr in dt.Rows)
            {               
                if (dr[0].ToString() == checkurlvalue)
                {

                    sb.Append("<li  class='active'>");
                }
                else
                {
                    sb.Append("<li>");
                }
                sb.Append("<a href='" + dr["HEALTH_ID"].ToString() + "'>");
                sb.Append(dr["PACKAGE_NAME"].ToString());
                sb.Append(" </a>");
                sb.Append("</li>");
            }
           

            ltrlget.Text = sb.ToString();
        }
    }

    [System.Web.Services.WebMethod]
    public static string GetTimeForBind()
    {
        DataTable dt = new DataTable();
        Utility utilil = new Utility();
        dt = utilil.Display("EXEC PROC_DRNOTAVAILABETABLE 'GET_DATE_FOR_FRONTEND'");
        foreach (DataRow date in dt.Rows)
        {
            string val = date[1].ToString();
            val = val.TrimStart('0');

            Char delimiter = '-';
            string[] arr = val.Split(delimiter);

            string mm = arr[0];
            string dy = arr[1].TrimStart('0');
            string yy = arr[2];

            //date[1] = val;
            date[1] = mm + "-" + dy + "-" + yy;
        }
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds.GetXml();
    }

}