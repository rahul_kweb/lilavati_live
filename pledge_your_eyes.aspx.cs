﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pledge_your_eyes : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
        }
    }

    public void Content()
    {
        cms.ID = 16;
        cmsbal.GETBYIDContent(cms);
        litContent.Text = cms.Description;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PledgeYourEyes"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@NAME", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@Dob", DateTime.ParseExact(txtDOB.Text, "dd-MM-yyyy", null).ToString("MM/dd/yyyy"));
            cmd.Parameters.AddWithValue("@MobileNumber", txtMobileNumber.Text.Trim());
            cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
            if (utility.Execute(cmd))
            {
                AppealEmail mailer = new AppealEmail();
                mailer.EmailHealthApp(txtName.Text.Trim(), txtMobileNumber.Text.Trim(), txtDOB.Text.Trim(), txtCity.Text.Trim());

                Reset();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "show", "ShowPopUp();", true);
            }
        }
    }

    public void Reset()
    {
        txtName.Text = string.Empty;
        txtDOB.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtMobileNumber.Text = string.Empty;
    }
}
