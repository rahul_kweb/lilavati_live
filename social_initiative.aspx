﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="social_initiative.aspx.cs" Inherits="social_initiative" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital – SEWA | Free OPD services for needy people</title>
    <meta name="description" content="Our main initiative is to provide better healthcare for all needy people irrespective of their class, caste, religion or social background, we always lookout for more ways to help needy people.">

    <script type="text/javascript" src="js/tabcontent.js"></script>

    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Social Initiatives</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Social_initiative.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <%--<div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>SEWA is the social service wing of our hospital. The aim of this department is to make the latest technology available to the underprivileged and make modern healthcare more accessible to the needy. The activities of the department include free OPD for senior citizens, free health check mobile vans for Adivasi areas. </p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">SEWA – Free OPD</strong></p>

                            <img src="images/social_initiative/opdimg.jpg" class="img_left" />
                            <ul style="float: left;">
                                <li>Consultation at this OPD is offered between 2 PM to 4.30 PM every day.</li>
                                <li>A minimal fee of Rs. 20/- is charged for permanent registration and I-Card. Follow-up charges Rs. 10/-.</li>
                                <li>Free Medicines are also provided.</li>
                            </ul>
                            <p></p>
                            <div class="clear"></div>

                            <p><strong class="green_text uppercase">SEWA Nana-Nani</strong></p>
                            <p>Senior citizens need the most attention and we believe they deserve it all. In order to help them to avoid long queues and waiting, we decided to reach out to them by taking our mobile vans to senior citizen parks in Juhu, Versova and Girgaon Chowpatty. Through these drives, we examine the blood pressure and sugar levels of senior citizens for free. To add to it, we also offer them refreshments. We also conduct free General & Cardiac Health check-up camps in various places in Mumbai.</p>
                            <div class="clear"></div>

                            <p><strong class="green_text uppercase">SEWA - Mobile Clinic</strong></p>
                            <p>
                                <img src="images/social_initiative/mobileclinicimg.jpg" class="img_left" />
                                As part of the ‘Community Outreach’ programme, we have initiated the facility of Mobile Clinics to take the hospital closer to people’s home in and around the city. The Sewa Mobile Vans provide primary medical care to patients by charging a nominal fee of Rs. 10/-. These vans are accompanied by experienced doctors and nurses who provide consultation and medication. The basic medications are made available free of cost for 6 months.
                            </p>
                            <div class="clear"></div>

                            <p><strong class="green_text uppercase">SEWA – Swastha Bachpan</strong></p>
                            <p>
                                <img src="images/social_initiative/savegirlchild.jpg" class="img_left" />
                                Children are the future of the nation. They are the next scientists, doctors, engineers, pilots and hence it is essential to ensure they are healthy in their crucial growing years. Through SEWA, we have taken the initiative to help poor & underprivileged children receive free health check-ups, admission and surgeries. Around 50 children have been admitted under this initiative for various surgeries such as kidney, orthopaedic & cancer.
                            </p>
                            <div class="clear"></div>
                        </div>
                    </div>-->

                    <!-- xxxxxxxxxxxxxxxxxx -->


                    <div class="clear"></div>
                </div>--%>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

