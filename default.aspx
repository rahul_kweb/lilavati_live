﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome To Lilavati Hospital</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap');
	html,body {font-family: 'Lato', sans-serif; margin:0; padding:0; color:#666; font-size:16px; background: #eaf9fb;height:100%; }
	.fade_anim {-webkit-transition: all .3s ease-in-out;
    -moz-transition: all .3s ease-in-out;
    -o-transition: all .3s ease-in-out;
    transition: all .3s ease-in-out;}
	.um_arp {max-width:400px;}
	h1 {font-weight:700; color:#250d6a; font-size:65px;}
	.box {width:90%; height:90%;  margin:auto; background:#fff; -webkit-box-shadow: 2px 2px 11px 0px rgba(0,0,0,0.1);
    -moz-box-shadow: 2px 2px 11px 0px rgba(0,0,0,0.1);
    box-shadow: 2px 2px 11px 0px rgba(0,0,0,0.1);
}

	.social li {width:25px; height:25px;  display:block; margin-left:5px;}
	.social li a {display:block; height:100%; background-image: url(demo/images/social_icons.png); background-repeat: no-repeat; border-radius: 3px;}
	.social li a.fb {background-position: 0 0;}
	.social li a.fb:hover {background-position: 0 -25px; background-color:#3b5998;}
        
	.social li a.tw {background-position: -25px 0;}
	.social li a.tw:hover {background-position: -25px -25px; background-color:#00aced;}

	.social li a.yt {background-position: -50px 0;}
	.social li a.yt:hover {background-position: -50px -25px; background-color:#f00;}

	.social li a.in {background-position: -75px 0;}
	.social li a.in:hover {background-position: -75px -25px; background-color:#0077B5;}

	.social li a.ln {background-position: -100px 0;}
	.social li a.ln:hover {background-position: -100px -25px; background-color:#125688;}

	@media (max-width:991px)
	{
	h1 {font-size:50px;}	
	}

	@media (max-width:767px)
	{
		html,body  {padding:10px; height: auto;}
	.box {height:auto; padding: 15px!important; width: 100%;}	
	.top {margin-bottom:30px; display:block!important;}
	.social {margin-top:30px!important;}
	.social li {margin:0 20px 0 0;}
	h1 {font-size:35px;}
	}
</style> 

</head>

<body>
		

	<div class="h-100 d-flex">

		<div class="row m-auto box p-5 justify-content-between">

			<div class="col-12 d-flex justify-content-between top">
				<div><img src="demo/images/logo.gif" class="img-fluid d-block" ></div>
				<div>
					
					<ul class="list-unstyled p-0 m-0 d-flex social">
						 <li><a href="https://www.facebook.com/OfficialLilavatiHospital/" target="_blank" title="Facebook" class="fb fade_anim"></a></li>
        				<li><a href="https://twitter.com/LilavatiHRC" title="Twitter" target="_blank" class="tw fade_anim"></a></li>
        				<li><a href="https://youtu.be/56zfwgMD2U4" target="_blank" title="YouTube" class="yt fade_anim"></a></li>
        				<li><a href="https://instagram.com/lilavatihospital.offcial?utm_source=ig_profile_share&amp;igshid=1xivqjffqcg7a" target="_blank" title="Instagram" class="in fade_anim"></a></li>
        				<li><a href="https://www.linkedin.com/company/18147846/" target="_blank" title="LinkedIn" class="ln fade_anim"></a></li>

					</ul>

				</div>	
			</div>	

			<div class="col-md-6 col-lg-6 align-self-center">
				
				<h1>Under <br> Maintenance</h1>

				<!-- <div class="um_arp mb-2"> <img src="images/uc.jpg" class="img-fluid d-block"> </div> -->

				<p>Our website is currently undergoing scheduled maintenance. <br> We should be back shortly.</p>

				<p> <strong>For more info Please contact us </strong>  <br> <strong>Toll Free </strong> 18002678612<br>

				<strong> Boardline </strong> 022-68658000 / 68651000</p>

			</div>

			<div class="col-md-6 col-lg-5 align-self-center	"><img src="demo/images/main.jpg" class="img-fluid d-block"></div>

		</div>

	</div>

</body>
</html>
