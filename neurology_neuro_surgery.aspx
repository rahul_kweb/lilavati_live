﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="neurology_neuro_surgery.aspx.cs" Inherits="neurology_neuro_surgery" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>

    <script type="text/javascript" src="js/tabcontent.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Neurology & Neuro Surgery</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Neurology.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%--<li><a href="#" rel='tab_Dr'>Professionals</a></li>--%>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server" OnItemDataBound="rptContent1_ItemDataBound">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%-- <%#Eval("DESCRIPTION") %>--%>
                                <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                                <asp:HiddenField ID="hdnDesc" runat="server" Value='<%#Eval("DESCRIPTION") %>' />
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <%--<div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>--%>
                    <div class="clear"></div>
                </div>

                <%--<div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>The department of Neuro and Neuro Spinal has experienced surgeons, all having interest in a different branch of the speciality. It is backed by neurology, neuropathology, neuroradiology departments and other ancillary services.</p>
                            <p>The department is accredited for training residents in Diplomate of National Board (DNB) and conducts regular scientific activities like lectures, seminars, workshops, hands on cadaver workshop and invite guest lectures. The department is actively involved in working closely with the Research department of the hospital and has published several papers.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <ul class="bullet">
                                <li><strong>Trauma:</strong> The department looks after all types of Neuro trauma 24 hours as and when the need arises.</li>
                                <li><strong>Cranial surgery:</strong> All types of tumors, vascular malformations, intra cranial bleeds and other pathologies like hydrocephalus are dealt with.</li>
                                <li><strong>Spinal Surgery:</strong> All varieties of spinal surgery including CV Junction surgery and anterior and posterior surgery, functional and reconstructive surgery, spinal cord tumors, congenital anomalies, all types of fusions, minimally invasive and endoscopic spinal surgeries are regularly carried out.</li>
                                <li><strong>Vascular:</strong> Digital subtraction angiography, treatment of aneurysm, arterio venous malformations, intracranial artery stenosis, carotid stenosis, mechanical thrombectomy etc.</li>
                                <li><strong>Skull base surgery:</strong> Variety of lesions is dealt with lateral, pterional, Trans labyrinthine, trans sphenoidal, trans oral approaches.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

