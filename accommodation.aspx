﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="accommodation.aspx.cs" Inherits="accommodation" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox({
                loop: false,
                padding: 1,
                //		afterLoad : function() {
                //			this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                //		}			
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Accommodation</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--  <div class="tabbertab">
                        <h2>Common</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/common.jpg" class="fancybox" title="Common Room">
                                    <img src="images/accommodation/common.jpg"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>Six patients per room</strong> </li>
                                    <li>Toilet & Washroom : <strong>Shared Facility outside the room</strong> </li>
                                    <li>Bed for Attendant :	<strong>Not Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>One Chair</strong> </li>
                                    <li>Type of Bed for patients : <strong>Manually Operated</strong></li>
                                    <li>Television : <strong>Not Available</strong></li>
                                    <li>Telephone : <strong>One Telephone for Six patients</strong></li>
                                    <li>Refrigerator : <strong>Not Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 1 Pass & Visiting Hrs  – 1 Pass</strong></li>
                                    <li>Internet Access : <strong>No</strong></li>
                                </ul>
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Economy</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/economy.jpg" class="fancybox" title="Economy Room">
                                    <img src="images/accommodation/economy.jpg"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>Three patients per room</strong> </li>
                                    <li>Toilet & Washroom : <strong>Attached Toilet-cum-Bath</strong> </li>
                                    <li>Bed for Attendant :	<strong>Not Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>Easy Chair</strong> </li>
                                    <li>Type of Bed for patients : <strong>Manually Operated</strong></li>
                                    <li>Television : <strong>Not Available</strong></li>
                                    <li>Telephone : <strong>Two Telephones for Three patients</strong></li>
                                    <li>Refrigerator : <strong>Not Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 1 Pass & Visiting Hrs  – 1 Pass</strong></li>
                                    <li>Internet Access : <strong>No</strong></li>
                                </ul>

                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Twin Sharing</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/twin_sharing.jpg" class="fancybox" title="Twin Sharing Rooms">
                                    <img src="images/accommodation/twin_sharing.jpg"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>Two patients per room</strong> </li>
                                    <li>Toilet & Washroom : <strong>Attached Toilet-cum-Bath</strong> </li>
                                    <li>Bed for Attendant :	<strong>Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>One Chair</strong> </li>
                                    <li>Type of Bed for patients : <strong>Manually Operated</strong></li>
                                    <li>Television : <strong>One Television per patient</strong></li>
                                    <li>Telephone : <strong>One Telephone per patient</strong></li>
                                    <li>Refrigerator : <strong>Not Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 1 Pass & Visiting Hrs  – 1 Pass</strong></li>
                                    <li>Internet Access : <strong>No</strong></li>
                                </ul>

                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Special</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/special.jpg" class="fancybox" title="Special Room">
                                    <img src="images/accommodation/special.jpg"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>One patients per room</strong> </li>
                                    <li>Toilet & Washroom : <strong>Attached Toilet-cum-Bath</strong> </li>
                                    <li>Bed for Attendant :	<strong>Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>One Chair</strong> </li>
                                    <li>Type of Bed for patients : <strong>Electrically Operated</strong></li>
                                    <li>Television : <strong>One Television</strong></li>
                                    <li>Telephone : <strong>One Telephone</strong></li>
                                    <li>Refrigerator : <strong>Not Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 1 Pass & Visiting Hrs  – 2 Pass</strong></li>
                                    <li>Internet Access : <strong>No</strong></li>
                                </ul>

                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Deluxe</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/deluxe.jpg" class="fancybox" title="Deluxe Room">
                                    <img src="images/accommodation/deluxe.jpg"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>One patients per room</strong> </li>
                                    <li>Toilet & Washroom : <strong>Attached Toilet-cum-Bath</strong> </li>
                                    <li>Bed for Attendant :	<strong>Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>Sofa Set and Two Chairs</strong> </li>
                                    <li>Type of Bed for patients : <strong>Electrically Operated</strong></li>
                                    <li>Television : <strong>Available</strong></li>
                                    <li>Telephone : <strong>Available</strong></li>
                                    <li>Refrigerator : <strong>Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 1 Pass & Visiting Hrs  – 2 Pass</strong></li>
                                    <li>Internet Access : <strong>No</strong></li>
                                </ul>

                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Super Deluxe</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/super_deluxe.jpg" class="fancybox" title="Super Deluxe Room">
                                    <img src="images/accommodation/super_deluxe.jpg"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>One patients per room</strong> </li>
                                    <li>Toilet & Washroom : <strong>Attached Toilet-cum-Bath</strong> </li>
                                    <li>Bed for Attendant :	<strong>Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>2 Chairs</strong> </li>
                                    <li>Type of Bed for patients : <strong>Electrically Operated</strong></li>
                                    <li>Television : <strong>1 Plasma + 1 VCD player</strong></li>
                                    <li>Telephone : <strong>Available</strong></li>
                                    <li>Refrigerator : <strong>Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 2 Pass & Visiting Hrs  – 3 Pass</strong></li>
                                    <li>Internet Access : <strong>On Request</strong></li>
                                </ul>

                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Executive Suite</h2>

                        <div class="tab_content">

                            <div class="acd_img">
                                <a href="images/accommodation/executive_suite_patient_room.jpg" class="fancybox" data-fancybox-group="gallery" title="Executive Suite - Patient Room">
                                    <img src="images/accommodation/executive_suite_patient_room.jpg"></a>
                                <a href="images/accommodation/executive_room_guest_room.jpg" class="fancybox" data-fancybox-group="gallery" title="Executive Suite - Guest Room" style="display: none;"></a>
                            </div>

                            <div class="acd_table">
                                <ul class="bullet">
                                    <li>Number of Beds/ Room : <strong>One patient per room + additional room for visitors</strong> </li>
                                    <li>Toilet & Washroom : <strong>Attached Toilet-cum-Bath</strong> </li>
                                    <li>Bed for Attendant :	<strong>Available</strong> </li>
                                    <li>Sofa Set/ Chairs : <strong>4 Chairs</strong> </li>
                                    <li>Type of Bed for patients : <strong>Electrically Operated</strong></li>
                                    <li>Television : <strong>2 Plasma + 1 VCD player</strong></li>
                                    <li>Telephone : <strong>2 Nos. Available</strong></li>
                                    <li>Refrigerator : <strong>Available</strong></li>
                                    <li>No. of Passes : <strong>24 Hrs. – 2 Pass & Visiting Hrs  – 3 Pass</strong></li>
                                    <li>Internet Access : <strong>On Request</strong></li>
                                </ul>

                            </div>

                            <div class="clear"></div>
                        </div>

                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>


                <div class="clear"></div>
            </div>


            <div class="box_aside">
                <menu:Patients ID="menu" runat="server" />
            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

