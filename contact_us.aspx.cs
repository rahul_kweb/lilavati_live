﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class contact_us : System.Web.UI.Page
{
    CMSBAL cmsbal = new CMSBAL();
    ContactUs tblContactUs = new ContactUs();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindContact();
        }
    }

    public void BindContact()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetContactUS(tblContactUs);

        string addrs = dt.Rows[0]["Address"].ToString();

        addrs = addrs.Replace("Centre", "Centre<br>");
        dt.Rows[0]["Address"] = addrs;

        if (dt.Rows.Count > 0)
        {
            rptContactUS.DataSource = dt;
            rptContactUS.DataBind();
        }
    }
}