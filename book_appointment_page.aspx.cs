﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class book_appointment_page : System.Web.UI.Page
{
    DataTable dt = new DataTable();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["ID"] != null)
            {
                string id = Page.RouteData.Values["ID"].ToString();
                BindSpeciality1(id);
                BindDr1(id);
                Bindavailabletime1(id);
                BindCountry();
                ddlCountry.Items.Insert(0, "");
                // BindSpeciality();

            }
            else
            {


                BindSpeciality();
                BindCountry();
                ddlSpeciality.Items.Insert(0, "");
                ddlCountry.Items.Insert(0, "");

            }
        }

    }

    public void BindSpeciality1(string id)
    {
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        if (dt.Rows.Count > 0)
        {
            ddlSpeciality.DataSource = dt;
            ddlSpeciality.DataValueField = "Speciality_Id_bint";
            ddlSpeciality.DataTextField = "Speciality_vcr";
            ddlSpeciality.DataBind();
            ddlSpeciality.Enabled = false;
            // ddlSpeciality.SelectedValue = true;
            // ddlSpeciality.Items.Insert(0, "");
        }
    }
    public void BindDr1(string id)
    {


        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + id + "'");
        if (dt.Rows.Count > 0)
        {
            ddldrName.DataSource = dt;
            ddldrName.DataValueField = "Doctor_Id_bint";
            ddldrName.DataTextField = "Doctor_Name_vcr";
            ddldrName.DataBind();
            ddldrName.Enabled = false;
            // ddlSpeciality.SelectedValue = true;
            // ddlSpeciality.Items.Insert(0, "");
        }
    }
    public void Bindavailabletime1(string id)
    {
        dt = utility.Display("Execute AddUpdateGetDoctorMaster 'Get_By_Doc_Id_For_Apmnt','" + id + "'");
        if (dt.Rows.Count > 0)
        {
            ddlAvailableTime.DataSource = dt;
            ddlAvailableTime.DataValueField = "Day";
            ddlAvailableTime.DataTextField = "Avail_Time";
            ddlAvailableTime.DataBind();
            //ddlSpeciality.Enabled = false;
            // ddlSpeciality.SelectedValue = true;
            foreach (ListItem li in ddlAvailableTime.Items)
            {
                string len = li.Text.Length.ToString();

                if (li.Text.Contains("--") || li.Text.Contains("N.A") || li.Text == "" || len == "6")
                {
                    li.Attributes.Add("disabled", "true");
                    //li.Attributes.Add("class", "availa");
                }
            }
            ddlAvailableTime.Items.Insert(0, "");
        }
    }
    public void BindSpeciality()
    {
        //dt = utility.Display("EXEC AddUpdateGetSpecialtyMaster 'Get'");

        // added on 30_06_2021 (Speciality change in Book An Appointment)
        dt = utility.Display("EXEC AddUpdateGetSpecialtyMaster 'GetBookAppointmentSpeciality'");
        if (dt.Rows.Count > 0)
        {
            ddlSpeciality.DataSource = dt;
            ddlSpeciality.DataValueField = "Speciality_Id_bint";
            ddlSpeciality.DataTextField = "Speciality_vcr";
            ddlSpeciality.DataBind();

            ListItem itemToRemove = ddlSpeciality.Items.FindByValue("61");
            if (itemToRemove != null)
            {
                ddlSpeciality.Items.Remove(itemToRemove);
            }
        }
    }

    public void BindCountry()
    {
        dt = utility.Display("EXEC AddUpdateGetCountryMaster 'Get'");
        if (dt.Rows.Count > 0)
        {
            ddlCountry.DataSource = dt;
            ddlCountry.DataValueField = "Country_Id_bint";
            ddlCountry.DataTextField = "Country_vcr";
            ddlCountry.DataBind();
        }

        int i = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText(" India"));
        ddlCountry.SelectedIndex = i;

    }

    protected void ddlSpeciality_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSpeciality.SelectedIndex > 0)
        {
            string SpeValue = ddlSpeciality.SelectedValue.ToString();
            dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'Get_For_DataList_By_SplId',0,0,'" + SpeValue + "'");

            ddldrName.DataSource = dt;
            ddldrName.DataValueField = "Doctor_Id_bint";
            ddldrName.DataTextField = "Doctor_Name_vcr";
            ddldrName.DataBind();

            ddldrName.Items.Insert(0, "");

            ddlAvailableTime.Items.Clear();
            ddlAvailableTime.Items.Insert(0, "");

            //ddlVisitTime.Items.Clear();
            //ddlVisitTime.Items.Insert(0, "");

        }
        else
        {
            ddldrName.Items.Clear();
            ddlAvailableTime.Items.Clear();
            //ddlVisitTime.Items.Clear();
        }

    }

    protected void ddldrName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddldrName.SelectedIndex > 0)
        {
            string DocValue = ddldrName.SelectedValue.ToString();
            dt = utility.Display("EXEC AddUpdateGetDoctorMaster 'Get_By_Doc_Id_For_Apmnt','" + DocValue + "'");
            ddlAvailableTime.DataSource = dt;
            ddlAvailableTime.DataValueField = "Day";
            ddlAvailableTime.DataTextField = "Avail_Time";
            ddlAvailableTime.DataBind();

            foreach (ListItem li in ddlAvailableTime.Items)
            {
                string len = li.Text.Length.ToString();

                if (li.Text.Contains("--") || li.Text.Contains("N.A") || li.Text == "" || len == "6")
                {
                    li.Attributes.Add("disabled", "true");
                    //li.Attributes.Add("class", "availa");
                }
            }

            ddlAvailableTime.Items.Insert(0, "");

            //ddlVisitTime.Items.Clear();
            //ddlVisitTime.Items.Insert(0, "");
        }
        else
        {
            ddlAvailableTime.Items.Clear();
            //ddlVisitTime.Items.Clear();
        }

        DataTable dt1 = new DataTable();
        string DocValue1 = ddldrName.SelectedValue.ToString();
        dt1 = utility.Display("Exec AddUpdateGetDoctorMaster 'Get_Doc_Contact_By_Name','" + DocValue1 + "' ");
        ltrpopUp.Text = "";
        //string DirectAppointment = dt.Rows[0]["DirectAvailable"].ToString();
        if (bool.Parse(dt1.Rows[0]["DirectAvailable"].ToString()) == true)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('For Appointment contact:" + dt1.Rows[0]["ContactNo"].ToString() + "');", true);

            StringBuilder strbuild = new StringBuilder();
            strbuild.Append("<div id='consolPopup' style='display:none'>");
            strbuild.Append("<p style='text-align:center'>For Appointment with <b>Dr." + dt1.Rows[0]["Doctor_Name_vcr"].ToString() + "</b> Please contact on :</p>");
            strbuild.Append("<p style='text-align:center'><b>" + dt1.Rows[0]["ContactNo"].ToString() + "</b></p>");
            if (dt1.Rows[0]["DirectTimeFrom"].ToString() != string.Empty)
            {
                strbuild.Append("<div style='text-align:center'>");
                strbuild.Append(dt1.Rows[0]["DirectTimeFrom"].ToString());
                strbuild.Append("</div>");
            }
            strbuild.Append("</div>");

            ltrpopUp.Text = strbuild.ToString();

            string script = "window.onload = function() {DisableFields(); };";
            ClientScript.RegisterStartupScript(this.GetType(), "DisableFields", script, true);
        }
    }

    protected void btnFixAnAppointment_Click(object sender, EventArgs e)
    {
        MakeAppointment();
    }

    private void MakeAppointment()
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetAppointmentMaster"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Speciality_Id_bint ", int.Parse(ddlSpeciality.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@Doctor_Id_bint", int.Parse(ddldrName.SelectedValue.ToString()));
                cmd.Parameters.AddWithValue("@App_Time_vcr", ddlAvailableTime.SelectedItem.Text.Trim());
                cmd.Parameters.AddWithValue("@App_date_dtm", DateTime.ParseExact(txtDateOfAppointment.Text, "dd-MM-yyyy", null).ToString("MM/dd/yyyy"));
                cmd.Parameters.AddWithValue("@Patient_Name_vcr", txtPatientName.Text.Trim());
                cmd.Parameters.AddWithValue("@Address_vcr", txtAddress.Text.Trim());
                if (ddlCountry.SelectedIndex > 0)
                {
                    cmd.Parameters.AddWithValue("@Country_Id_bint", int.Parse(ddlCountry.SelectedValue.ToString()));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Country_Id_bint", 100);
                }

                if (ddlCountry.SelectedValue.ToString() == "100")
                {
                    cmd.Parameters.AddWithValue("@State_vcr", ddState.SelectedItem.Text.Trim());
                    cmd.Parameters.AddWithValue("@City_vcr", ddlCity.SelectedItem.Text.Trim());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@State_vcr", txtState.Text.Trim());
                    cmd.Parameters.AddWithValue("@City_vcr", txtCity.Text.Trim());
                }

                cmd.Parameters.AddWithValue("@Postal_vcr", txtPincode.Text.Trim());
                cmd.Parameters.AddWithValue("@Mobile_No_vcr", txtMobileNo.Text);
                cmd.Parameters.AddWithValue("@Email_Id_vcr", txtEmailId.Text);
                cmd.Parameters.AddWithValue("@Visit_vcr", ddlVisitTime.Text);

                if (utility.Execute(cmd))
                {

                    OfflineAppMail offlinemail = new OfflineAppMail();

                    string Speciality = ddlSpeciality.SelectedItem.Text;
                    string Doctors = ddldrName.SelectedItem.Text;
                    string Avail = ddlAvailableTime.SelectedItem.Text.Trim();
                    string Country = ddlCountry.SelectedItem.Text;
                    string Visit = ddlVisitTime.SelectedItem.Text;

                    string State = string.Empty;
                    string City = string.Empty;
                    if (ddlCountry.SelectedValue.ToString() == "100")
                    {
                        State = ddState.SelectedItem.Text.Trim();
                        City = ddlCity.SelectedItem.Text.Trim();
                    }
                    else
                    {
                        State = txtState.Text.Trim();
                        City = txtCity.Text.Trim();
                    }

                    offlinemail.EmailOfflineApp(Speciality, Doctors, Avail, txtDateOfAppointment.Text, txtPatientName.Text.Trim(), txtEmailId.Text.Trim(), txtMobileNo.Text, txtAddress.Text.Trim(), Country, State, City, txtPincode.Text, Visit);

                    Reset();

                    pnlForm.Visible = false;
                    pnlThank.Visible = true;

                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup();", true);

                }
                else
                {
                    //Response.Write("<script>alert('Failed');</script>");
                }
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    private void Reset()
    {
        ddlSpeciality.SelectedIndex = 0;

        ddldrName.Items.Clear();

        ddlAvailableTime.Items.Clear();

        txtDateOfAppointment.Text = string.Empty;

        txtPatientName.Text = string.Empty;
        txtAddress.Text = string.Empty;
        ddState.SelectedIndex = 0;
        ddlCity.SelectedIndex = 0;
        txtPincode.Text = string.Empty;
        txtMobileNo.Text = string.Empty;
        txtEmailId.Text = string.Empty;
        ddlVisitTime.SelectedIndex = 0;

        txtState.Text = string.Empty;
        txtCity.Text = string.Empty;

    }

    [System.Web.Services.WebMethod]
    public static string GetTimeForBind()
    {
        DataTable dt = new DataTable();
        Utility utilil = new Utility();
        dt = utilil.Display("EXEC PROC_DRNOTAVAILABETABLE 'GET_DATE_FOR_FRONTEND'");
        foreach (DataRow date in dt.Rows)
        {
            string val = date[1].ToString();
            val = val.TrimStart('0');

            Char delimiter = '-';
            string[] arr = val.Split(delimiter);

            string mm = arr[0];
            string dy = arr[1].TrimStart('0');
            string yy = arr[2];

            //date[1] = val;
            date[1] = mm + "-" + dy + "-" + yy;
        }
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds.GetXml();
    }

}