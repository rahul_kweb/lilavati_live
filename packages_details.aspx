﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="packages_details.aspx.cs" Inherits="packages_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <link href="/select2/select2.min.css" rel="stylesheet" />
    <script src="/select2/select2.min.js"></script>

    <script type="text/javascript" src="../js/tabcontent.js"></script>


    <%--<script>

        $(window).load(function () {
            // code for initialization in each and every partial postback
            var $example = $('#<%=ddlPreferredPackage.ClientID%>').val();
            $example.select2("open");
        });
    </script>--%>


    <%--<script>
        $(document).ready(function () {
            //$(".js-example-basic-single").select2();

            $('#spl').click(function () {
                var $example = $('#<%=ddlPreferredPackage.ClientID%>').select2();
                $example.select2("open");
            });

        });
    </script>--%>

    <script>

        $(document).ready(function () {

            var d = new Date();
            var hours = d.getHours();
            if (hours >= 16) {
                console.log("date if : " + hours);
                $(".doa").datepicker({
                    minDate: 2,
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-mm-yy',
                    maxDate: 360,
                    beforeShowDay: DisableSpecificDates
                });
            }
            else {
                console.log("date else : " + hours);
                $(".doa").datepicker({
                    minDate: 1,
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: 'dd-mm-yy',
                    maxDate: 360,
                    beforeShowDay: DisableSpecificDates
                });
            }
        });


        /** Days to be disabled as an array */
        // var disableddates = ["8-15-2016", "8-25-2016", "9-5-2016", "9-15-2016", "10-11-2016", "11-1-2016", "1-26-2017"];
        var disableddates = "";

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "/packages_details.aspx/GetTimeForBind",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    // alert(response.d);
                    // console.log(response.d);

                    var xmlDoc = $.parseXML(response.d);
                    var xml = $(xmlDoc);
                    // var alldate = xml.find("Table1");

                    // alert(customers);
                    // console.log(alldate);
                    var check = "";
                    var getdisabledate = [];


                    xml.find("Table1").each(function () {

                        // check += '"' + $(this).find("NOTAVAILBLEDATE").text().replace("/", "-").replace("/", "-") + '"' + ',';
                        check = $(this).find("NOTAVAILBLEDATE").text().replace("/", "-").replace("/", "-");
                        getdisabledate.push(check);

                    });
                    // var finaldate = check.substring(0, check.length - 1);

                    //  finaldate = '[' + finaldate + ']';
                    //finaldate = finaldate;
                    console.log(getdisabledate);
                    if (getdisabledate != '') {
                        //console.log(disableddates + "if");
                        disableddates = getdisabledate;
                    }
                    else {
                        disableddates = ["8-15-2016"];
                        // console.log(disableddates + "else");

                    }



                    //check = check.substring(0, check, length - 1);
                    //console.log(idcontainer);

                }
            });
        });

        function DisableSpecificDates(date) {

            var day = date.getDay();

            var m = date.getMonth();
            var d = date.getDate();
            var y = date.getFullYear();

            // First convert the date in to the mm-dd-yyyy format 
            // Take note that we will increment the month count by 1 
            var currentdate = (m + 1) + '-' + d + '-' + y;

            //console.log(currentdate + " mo :" + m + " disa : " + disableddates);

            // We will now check if the date belongs to disableddates array 
            for (var i = 0; i < disableddates.length; i++) {

                // Now check if the current date is in disabled dates array. 
                if ($.inArray(currentdate, disableddates) != -1 || day == 0) {
                    return [false];
                } else {
                    return [true];
                }
            }

        }

        //function DisableSun(date) {
        //    var day = date.getDay();
        //    // If day == 1 then it is MOnday
        //    if (day != selectedday) {
        //        return [false];
        //    } else {
        //        return [true];
        //    }
        //}


        $(function () {
            $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: 'dd-mm-yy'

            });

            //$(".doa").datepicker({
            //    minDate: 1,
            //    changeMonth: true,
            //    numberOfMonths: 1,
            //    dateFormat: 'dd-mm-yy'                
            //});



            $(".doa").change(function () {
                var data = $(".doa").val();
                if (data.length != 0 || data != "") {
                    $("#PrefferedDOA").removeClass("error");
                    $("#PrefferedDOA").addClass("populated");
                }
                else {
                    $("#PrefferedDOA").addClass("error");
                }


            });

            $(".dob").change(function () {
                var data = $(".dob").val();
                if (data.length != 0 || data != "") {
                    $("#dob").removeClass("error");
                    $("#dob").addClass("populated");
                }
                else {
                    $("#dob").addClass("error");
                }
            });

        });
    </script>

    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" />
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
        });
    </script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox({
                width: 200,
                afterShow: function () {
                    $('.js-example-basic-hide-search').select2({
                        minimumResultsForSearch: Infinity
                    });

                    $('#Pack').click(function () {
                        var $example = $('#<%=ddlPreferredPackage.ClientID%>').select2({
                            minimumResultsForSearch: Infinity
                        });
                        $example.select2("open");

                    });

                    $('#Pack').click(function () {
                        //$('#Pack').addClass("populated");

                        $('#<%=ddlPreferredPackage.ClientID%>').change(function () {
                            var data = $(this).val();
                            if (data.length > 0 && data != "") {
                                $('#Pack').addClass("populated");
                            }
                            else {
                                $('#Pack').removeClass("populated");
                            }
                            //console.log("sfd " + data);
                        })
                   })

                }
            });
        });
    </script>

    <script>
        function CheckSave() {
            var Name = $('#<%=txtName.ClientID%>').val();
            var PrefferedPack = $('#<%=ddlPreferredPackage.ClientID%>').val();
            var DOB = $('#<%=txtDOB.ClientID%>').val();
            var PrefferedDOA = $('#<%=txtPreferredDOA.ClientID%>').val();
            var Email = $('#<%=txtEmail.ClientID%>').val();
            var Contact = $('#<%=txtContact.ClientID%>').val();
            var blank = false;

            if (Name == '') {
                $("#Name").addClass("error");
                blank = true;
            }
            else {
                $("#Name").removeClass("error");
            }

            if (PrefferedPack == '') {
                $("#Pack").addClass("error");
                blank = true;
            }
            else {
                $("#Pack").removeClass("error");
            }

            if (DOB == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (PrefferedDOA == '') {
                $("#PrefferedDOA").addClass("error");
                blank = true;
            }
            else {
                $("#PrefferedDOA").removeClass("error");
            }

            if (Email == '') {
                $("#Email").addClass("error");
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $("#Email").addClass("error");
                    alert("Valid Email ID");
                    blank = true;
                }
                else {
                    $("#Email").removeClass("error");
                }
            }

            if (Contact == '') {
                $("#Contact").addClass("error");
                blank = true;
            }
            else {
                if (Contact.length < 10) {
                    $("#Contact").addClass("error");
                    blank = true;
                }
                else {
                    $("#Contact").removeClass("error");
                }
            }


            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function ShowPopUp() {
            $(function () {
                $("#showmsg").fancybox().trigger('click');
            });
        }

        $(document).ready(function () {
            $('#<%=txtPreferredDOA.ClientID%>').keypress(function (e) {
                e.preventDefault();
            });
        })

    </script>

    <style>
        .select2-container--default .select2-selection--single {
            border-bottom: 1px solid #ddd;
            background-color: #f9f9f9;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">
                <asp:Label ID="lblPackageName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <asp:Literal ID="litTittle" runat="server"></asp:Literal>

                <div class="pckg_price green_text" id="divPrice" runat="server">
                    <span class="fa fa-inr"></span>
                    <asp:Label ID="lblPrice" runat="server"></asp:Label>/-
                </div>

                <div id="divRep" runat="server" visible="false">

                    <%--<div class="tabber">

                        <asp:Repeater ID="rptTabContent" runat="server">
                            <ItemTemplate>
                                <div class="tabbertab">
                                    <h2><%#Eval("TAB_NAME") %>  </h2>

                                    <div class="tab_content">
                                        <%#Eval("DESCRIPTION") %>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="clear"></div>
                    </div>--%>

                    <div class="tabs_main">
                        <ul id="tab_list" class="tab_list">
                            <asp:Repeater ID="rptTabContent" runat="server">
                                <ItemTemplate>
                                    <li><a href="#" rel='tab_<%#Container.ItemIndex+1 %>'><%#Eval("TAB_NAME") %> </a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>

                        <!--  xxxxxxxxxxxx  -->

                        <asp:Repeater ID="rptTabContent1" runat="server">
                            <ItemTemplate>
                                <div id='tab_<%#Container.ItemIndex+1 %>'>
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div class="clear"></div>
                    </div>

                </div>

                <div id="divDesc" runat="server" visible="false">
                    <asp:Literal ID="litDesc" runat="server"></asp:Literal>
                </div>
                <p>
                    <asp:LinkButton ID="lnkbtnInstruction" runat="server" CssClass="uppercase" Font-Bold="true" OnClick="lnkbtnInstruction_Click">Instruction</asp:LinkButton>
                </p>
                <div class="clear"></div>
                <div class="clear"></div>
                <p>
                    <asp:LinkButton ID="lnkbtndescriptionkeytest" runat="server" CssClass="uppercase" Font-Bold="true" OnClick="lnkbtndescriptionkeytest_Click">
Description Key Test</asp:LinkButton>
                </p>


                <asp:Literal ID="litNote" runat="server"></asp:Literal>

                <a href="#health_chkup_form" class="submit_btn fade_anim fancybox" style="color: #FFF; text-align: center">Request a Health Checkup Appointment</a>

                <div class="clear"></div>


            </div>

            <div id="health_chkup_form" style="display: none">

                <div class="hc_form_div">

                    <div class="apply_form_tl uppercase green_text">Health Checkup Appointment </div>
                    <div class="clear"></div>

                    <div class="FlowupLabels hc_form text_format">

                        <p><strong>Kindly fill in the details given below to enable us to verify the availability of your requested date:</strong></p>

                        <div class="form_field fl_wrap" id="Name">
                            <label class="fl_label uppercase">Name</label>
                            <asp:TextBox ID="txtName" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="Pack">
                            <label id="spl" class="fl_label uppercase">Preferred Package</label>
                            <asp:DropDownList ID="ddlPreferredPackage" runat="server" class="js-example-basic-hide-search fl_input" autocomplete="off">
                            </asp:DropDownList>
                        </div>

                        <div class="clear"></div>

                        <div class="form_field fl_wrap" id="dob">
                            <label class="fl_label uppercase" for="dob">Date of Birth</label>
                            <asp:TextBox ID="txtDOB" runat="server" class="fl_input dob" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="PrefferedDOA">
                            <label class="fl_label uppercase" for="doa">Preferred Date of Appointment</label>
                            <asp:TextBox ID="txtPreferredDOA" runat="server" class="fl_input doa" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>

                        <div class="form_field fl_wrap" id="Email">
                            <label class="fl_label uppercase">Email</label>
                            <asp:TextBox ID="txtEmail" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="Contact">
                            <label class="fl_label uppercase">Contact No.</label>
                            <asp:TextBox ID="txtContact" runat="server" class="fl_input" MaxLength="10" onKeyPress='javascript:return onlyNumbers();' autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="submit_btn fade_anim uppercase" Style="cursor: pointer;" OnClick="btnSubmit_Click" OnClientClick='javascript:return CheckSave();' />

                    </div>






                    <div class="clear"></div>
                </div>

            </div>

            <div id="showmsg" class="hc_form_div" style="display: none;">

                <div class="apply_form_tl uppercase green_text">Health Checkup Appointment </div>
                <div class="clear"></div>

                <div class="hc_thanks text_format">
                    <p><strong class="green_text">Thank you for requesting healthcheckup appointment with us.</strong></p>
                    <p>Please do not consider your request as a confirmation of the appointment. We will revert back to you with the confirmation vide email or telephonic call.</p>
                </div>

                <div class="clear"></div>
            </div>



            <div class="box_aside">

                <div class="rel_menu fade_anim">
                    <ul>

                        <asp:Repeater ID="rptPackageMenu" runat="server" OnItemDataBound="rptPackageMenu_ItemDataBound">
                            <ItemTemplate>
                                <li id="liactive" runat="server">

                                    <%--<a id="idPackMenu" runat="server" href="packages_details.aspx"><%#Eval("PACKAGE_NAME") %></a>--%>

                                    <asp:HyperLink ID="hypLnk" runat="server" NavigateUrl='<%#Eval("Health_Id","Packages-Details/{0}") %>'><%#Eval("PACKAGE_NAME") %></asp:HyperLink>

                                    <asp:HiddenField ID="hdnMenu" runat="server" Value='<%#Eval("Health_Id") %>' />

                                </li>
                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Literal ID="ltrlget" runat="server"></asp:Literal>
                    </ul>
                </div>

                <div class="ad_box">
                    <img src="/images/ad_img.jpg" />
                </div>

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="/js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="/js/FlowupLabels_plugin.js"></script>

    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(false)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
</asp:Content>

