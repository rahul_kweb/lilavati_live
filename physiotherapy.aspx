﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="physiotherapy.aspx.cs" Inherits="physiotherapy" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital - Newly Upgraded Physiotherapy Department</title>
    <meta name="description" content="Lilavati hospital has the best physiotherapy department in all over Mumbai with latest technologies & hardworking physiotherapist to help people with neurological, sport related injuries & other complex problems.">

    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Physiotherapy</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/physiotherapy.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>

                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litPhysiotherapy" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%--  <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litPhysiotherapy" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>




                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>
                <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>The Physiotherapy Department at Lilavati Hospital and  Research Centre incorporates evidence based practice in the rehabilitation of a range of musculoskeletal, neurological, cardiorespiratory, paediatric, sport related injuries & other complex problems. The process involves maximizing the functional potential of anindividual with disease, dysfunctin or injury & also facilitates health promotion, lifestyle modification& fitness training. The physiotherapy services are provided in the intensive care unit (surgical, medical & paediatric), In Patient Department (IPD) and OutPatientDepartment (OPD). Patient & family education being an integral component of the management process, a discharge counselling session is given to all our In patients. Furthermore, patient education sessions are held once a month for our cardiac & pulmonary patients.</p>


                            <p><strong class="green_text uppercase">Location</strong></p>
                            <p>The outpatient department is located in the basement floor of the hospital.</p>

                            <p><strong class="green_text uppercase">Appointment timings</strong></p>
                            <p>
                                Monday – Saturday 08.00 am to 08.00 pm<br>
                                OPD patients by prior appointment only 
                            </p>

                            <p><strong class="green_text uppercase">Contact us</strong></p>
                            <p>For queries / appointments contact on +91-22 26751536 or email us at <a href="mailto:physiotherapy@lilavatihospital.com">physiotherapy@lilavatihospital.com</a>.</p>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Musculoskeletal and sports Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>The physiotherapy department sees a large variety of musculoskeletal (orthopedic) conditions in IPD as well as OPD. The rehabilitation process incorporates assessment, goal setting, treatment, education as well as preventive aspect. Some of the common musculoskeletal conditions seen are:</p>
                                    <ul class="bullet">
                                        <li>Neck & back pain, including post operative cases</li>
                                        <li>Degenerative conditions</li>
                                        <li>Complex hand & foot injuries / crush injuries</li>
                                        <li>Shoulder - cuff tears, bankarts repair, adhesive capsulitis, painful shoulder.</li>
                                        <li>Joint replacement & ligament injuries</li>
                                        <li>Trauma and acute post operative care</li>
                                    </ul>

                                    <p><strong class="green_text uppercase">Treatment Techniques:</strong></p>
                                    <ul class="bullet">
                                        <li>Mechanical Diagnosis and Therapy (MDT)/ McKenzie method: Each patient is evaluated thoroughly using the MDT assessment forms and classified into one of the syndromes namely derangement, dysfunction, posture or other (if they don’t fit into any syndrome). Treatment is then provided as per the classification; which includes an active exercise component & an education component emphasizing posture awareness & prophylaxis.<li>
                                    </ul>

                                    <p><strong class="green_text uppercase">Other treatment methods.</strong></p>
                                    <ul class="bullet">
                                        <li>Kinetic control & movement therapy</li>
                                        <li>Mulligan & Maitland mobilization</li>
                                        <li>Taping & muscle activation</li>
                                        <li>Soft tissue mobilization.</li>
                                        <li>Strengthening by weight cuffs, ARKE, Kinesis &Theraband.</li>
                                        <li>Electrotherapy used as an adjunct.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Cardiac Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Cardiac Rehabilitation is provided by cardio respiratory therapists to enhance and maintain cardiovascular health. This Cardiac Rehabilitation program includes exercise testing, education on heart healthy living and counseling to reduce stress and help the patient to return to an active life. Cardiac Rehabilitation is indicated in post bypass surgery, angioplasty, myocardial infarction, valve replacement surgery and peripheral vascular diseases.</p>
                                    <p>Cardiac Rehabilitation is a customized exercise program based on functional testing which includes the following stages -:</p>
                                    <p>PHASE I - 	In-patient Rehab -This starts immediately when the patient is admitted in the hospital for the surgery.</p>
                                    <p>PHASE II -	Out-patient Rehab - Post suture removal and after discharge from hospital.</p>
                                    <p>PHASE III - 	Out -patient Rehab- Custom made for the patient to attain previous level of fitness.</p>

                                    <p>Equipments used for Cardiac Rehabilitation at Lilavati Hospital include high end medical equipments like Run excite med (Treadmill), bike excite med (cycle) and cross trainer. It is a supervised exercise program using telemetry ECG System and polar heart belt for monitoring vital parameters ensuring patient safety.</p>
                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Pulmonary Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Pulmonary rehabilitation at Lilavati is a program incorporating exercise, education and support to help pulmonary patients  breathe better and function at the highest level possible. The patient works with a team of specialists who help in improving the overall physical condition. This program is beneficial for patients suffering from COPD, bronchiectasis, asthma, interstitial lung diseases, sarcoidosis and post lung surgeries.</p>
                                    <p>Pulmonary Rehabilitation Program incorporates:</p>
                                    <ul class="bullet">
                                        <li>Detailed patient assessment</li>
                                        <li>Custom made exercise program which includes </li>
                                    </ul>
                                    <ul>
                                        <li>Breathing exercises, chest clearance techniques (using postural drainage bed, nebulization ,vibrator) in an isolated room. Assistive devices like spirometer, acapella, flutter, ezypap may be a used as and when necessary.</li>
                                        <li>Conditioning, strength training and functional training.</li>
                                        <li>Patient education sessions are conducted by cardiac and pulmonary rehabilitation specialists on a monthly basis. These sessions cover topics like breathing retraining, lifestyle modification, etc. and encourage patients to share their experiences.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Neurological Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Our Neurosciences specialized team offers supervised programs designed for people with neurological disorders in the Intensive Care Unit, In Patient Stroke Rehabilitation Unit and Outpatient Neurorehabilitation department in order to achieve the following goals:-</p>
                                    <ul class="bullet">
                                        <li>Maximize functional independence in transfers and activities of daily living and thus improve quality of life.</li>
                                        <li>Improve voluntary control, strength, balance, co-ordination and gait.</li>
                                    </ul>
                                    <p>This program caters to people with neurological problems like stroke, Parkinson’s disease, Multiple sclerosis, Traumatic brain injury, Spinal cord injury, nerve palsy, polyneuropathy and other imbalance and co-ordination issues.</p>
                                    <p><strong class="green_text uppercase">Treatment plan includes:-</strong></p>
                                    <ul class="bullet">
                                        <li>Bobath, Neurodevelopmental therapy, Proprioceptive neuromuscular facilitation and  Sensory integration</li>
                                        <li>Motor control and postural control training</li>
                                        <li>Balance and co ordination training using balance master, swiss ball, wobble board, balance beam & foam.</li>
                                        <li>Gait training using partial body support treadmill system</li>
                                        <li>Hand rehabilitation  exercises using various functional training equipments and peg boards.</li>
                                        <li>Mirror therapy using mirror box.</li>
                                        <li>Constrained induced movement therapy</li>
                                        <li>Tilt table standing .</li>
                                        <li>Skifit for upper and lower limb work out </li>
                                        <li>Arke and Kinesis for core training and strengthening</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Pediatric Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Our Pediatric physiotherapists work with children with special needs and help them achieve age appropriate milestones and maximal functional independence with the following goals:</p>

                                    <ul class="bullet">
                                        <li>Help facilitate & guide movement aiming towards correct posture & movement pattern</li>
                                        <li>Improve strength, flexibility, balance & co-ordination</li>
                                        <li>Provide opportunities of sensory experiences in a controlled fashion in order to enhance motor behaviour, social interaction & active participation.</li>
                                        <li>In the NICU, help in maintaining state regulation, range of motion and Oro-motor stimulation</li>
                                    </ul>

                                    <p><strong class="green_text uppercase">Target population:</strong></p>
                                    <ul class="bullet">
                                        <li>Developmental Delays</li>
                                        <li>Cerebral Palsy</li>
                                        <li>Downs Syndrome</li>
                                        <li>Muscular Dystrophies</li>
                                        <li>Autism Spectrum Disorder</li>
                                        <li>ADHD</li>
                                        <li>Erb’s palsy</li>
                                        <li>Post orthopedic surgeries and Botox</li>
                                    </ul>

                                    <p>Treatment plan includes a range of approaches like Bobath, Neurodevelopmental facilitation, Sensory integration, Early Intervention and postural control training with use of appropriate orthotics and walking aids. We work in partnership with parents and other health care professionals like Neurologists, Pediatricians, Orthopedic consultant, Speech therapist and Orthotist to ensure that the best care is given to children requiring Neuro and ortho rehabilitation.</p>


                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Vestibular Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Vestibular Rehabilitation is an exercise based program for reducing symptoms of disequilibrium and dizziness associated with vestibular pathology with the following Goals:</p>

                                    <ul class="bullet">
                                        <li>To reduce  dizziness & visual symptoms</li>
                                        <li>To improve balance and gait</li>
                                        <li>To improve general activity levels</li>
                                    </ul>

                                    <p><strong class="green_text uppercase">It caters to patients with:</strong></p>
                                    <ul class="bullet">
                                        <li>BPPV (Being paroxysmal positional vertigo)</li>
                                        <li>Vestibular hypofunction</li>
                                        <li>Meniere’s disease</li>
                                        <li>Labyrinthitis</li>
                                        <li>Functional training</li>
                                        <li>Vestibular neuritis</li>
                                    </ul>
                                    <p>Vestibular Rehabilitation include Barndfdaroff exercise, Epley’s maneuvers and Semontmaneuvers along with Gaze stability exercises, balance and gait training and counseling to increase confidence levels.</p>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Geriatric Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>With increased age patients often face many physical changes that can affect level of function and well being. The primary purpose of rehabilitation is to enable people to function at the highest possible levels despite physical impairments.</p>
                                    <p>This program caters to the elderly population who have problems  like multi joint pain, frequent falls, osteoporosis, imbalance &mobility issues, arthritis, incontinence etc.</p>
                                    <p>Goals of management include</p>
                                    <ul class="bullet">
                                        <li>Improving strength and endurance, flexibility, balance and coordination</li>
                                        <li>Promoting functional independence and improving quality of life</li>
                                        <li>Reducing the number of falls</li>
                                        <li>Decreasing pain</li>
                                    </ul>

                                    <p><strong class="green_text uppercase">Treatment plan includes:</strong> </p>
                                    <ul class="bullet">
                                        <li>Sarah Meek’s protocol for Osteoporosis</li>
                                        <li>Strength training using theraband station and weight cuffs.</li>
                                        <li>Balance training with balance master</li>
                                        <li>Gait training</li>
                                        <li>Aerobic exercise program including treadmill and elliptical<li>
                                    </ul>


                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Cancer Rehabilitation</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Physiotherapy is provided for cancer patients to reduce the complications arising due to its medical and surgical treatment, thus promoting an increase in the level of activity thereby improving the quality of life.</p>
                                    <p>Lymphedema post breast surgery is managed in the physiotherapy department in the following manner:</p>
                                    <ul class="bullet">
                                        <li>Manual lymphatic drainage and nodal stimulation</li>
                                        <li>Soft tissue and scar mobilization.</li>
                                        <li>Range of movement, stretching and strengthening </li>
                                        <li>Posture re-education</li>
                                        <li>Pressure garments.</li>
                                    </ul>


                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Wound management with Ozone therapy</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Ozone therapy is used to enhance wound healing & is provided for In Patients &Out Patients.</li>
                                        <li>The treatment time depends upon the size of the wound.</li>
                                        <li>On completion of treatment, wound dressing is done.</li>
                                        <li>Aseptic precautions are followed at every stage of the treatment.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Health Promotion and Lifestyle Modification</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>Although most patients understand the importance of physical activity and healthy eating, many seem unable to change their unhealthy behavior to reduce weight and improve chronic conditions like abnormal lipids (cholesterol or triglycerides), High blood pressure, Diabetes etc. These chronic conditions & sedentary lifestyle increase the risk for heart attack, stroke and peripheral artery disease. Lifestyle changes with Fitness program have shown to significantly reduce morbidity and mortality rates.</p>
                                    <p>Components of fitness program In Lilavati hospital include: </p>
                                    <ul class="bullet">
                                        <li>Assessment of functional capacity. </li>
                                        <li>Exercises tailored to patients’ abilities and interests.</li>
                                        <li>Fitness training on treadmill, sci- fit and elliptical under continuous monitoring and supervision by the therapist.</li>
                                        <li>Strength training using ARKE, theraband, pilates, kinesis.</li>
                                        <li>Education and counseling regarding lifestyle changes to lower the risk of heart/medical problems.</li>
                                    </ul>

                                    <p>Pelvic floor exercises (Keigel’s exercises) and/ or electrical stimulation are used to strengthen the pelvic floor muscles.</p>
                                    <p>Infrared radiation therapy is given post normal deliveries and vaginal hysterectomy for suture healing and pain relief.</p>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">

                            <p>The Physiotherapy Department has been completely renovated structurally & has added pulmonary & cardiac rehabilitation to its services. The newly upgraded department is well equipped with state of the art specialized systems like</p>
                            <ul class="bullet">
                                <li>Balance Master: It is a computerized system of objective assessment, sequence training & custom based training used for evaluation and treatment of balance using visual feedback. It provides a numeric and comprehensive progress report.</li>
                                <li>Treadmill with body weight support system: The unweighing system enables partial weight bearing therapy to be conducted with the assurance of patient control, safety and with convenient access to the patient for manual observation and assistance. The electrical unweighing system is designed to apply vertical support to remove the stress of bearing body weight and promotes proper posture and balance. The treadmill is specially designed to have a zero start capability which is a must for rehabilitation.</li>
                                <li>Scifit: It is a recumbent cycle which provides isolated training of upper and lower body or a total body workout with various programs namely Fitquik, Constant work, Isostrength, Hills and Power fit.</li>
                                <li>ARKE: The exercise system comprises of balls and weights for a core-centric training method to achieve better physical movement, co-ordination, strength, stability & balance.</li>
                                <li>Kinesis: is a slick set of cable & pulley resistance exercise system for various exercise routines at varying intensity levels. Unlike traditional weighted cable equipment this machine provides free movements which prove to be more challenging for a workout.</li>
                                <li>Theraband wall station: is a compact total body rehabilitation system designed for strength training, balance & core-training with the help of resistance bands & tubes, stability trainers, rockers board & wobble board.</li>
                                <li>High-end medical equipment for cardiac & pulmonary rehabilitation patients from Technogym.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>--%>





                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

