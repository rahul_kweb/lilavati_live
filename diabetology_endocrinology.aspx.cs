﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class diabetology_endocrinology : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();

    string docValue = string.Empty;
    string docValue1 = string.Empty;
    string docValue2 = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            BindBanner();
        }
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Diabetology Endocrinology";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        string dsc2 = dt.Rows[2]["Description"].ToString();

        dsc2 = dsc2.Replace("&nbsp;", "");
        string val2 = dsc2;

        dt.Rows[2]["Description"] = val2;


        string dsc = dt.Rows[1]["Description"].ToString();

        dsc = dsc.Replace("&nbsp;", "");
        string val = dsc;

        dt.Rows[1]["Description"] = val;

        string dsc1 = dt.Rows[0]["Description"].ToString();

        dsc1 = dsc1.Replace("&nbsp;", "");
        string val1 = dsc1;

        dt.Rows[0]["Description"] = val1;

        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();

            rptContent1.DataSource = dt;
            rptContent1.DataBind();
        }
    }

    public void DiabetolotyDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,8");
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>"); ;

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");

        }

        docValue = strBuil.ToString();
        //litDocDetails.Text = strBuil.ToString();
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(23);
    }
    protected void rptContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnDesc = (HiddenField)e.Item.FindControl("hdnDesc");
            DiabetolotyDoctorDetails();
            EndocrineSurgeryDoctorDetails();
            DiabeticFootDoctorDetail();
            StringBuilder val = new StringBuilder(hdnDesc.Value);
            val = val.Replace("DiabetolotyDoctorDetails", docValue);
            val = val.Replace("EndocrineSurgeryDoctorDetails", docValue1);
            val = val.Replace("DiabeticFootDoctorDetail", docValue2);

            Label lblDesc = (Label)e.Item.FindControl("lblDesc");
            lblDesc.Text = val.ToString();
        }
    }

    public void EndocrineSurgeryDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,61");
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>"); ;

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");

        }

        docValue1 = strBuil.ToString();
    }


    public void DiabeticFootDoctorDetail()
    {
        StringBuilder strBuil2 = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,62");
        strBuil2.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>"); ;

            strBuil2.Append("<li>");
            strBuil2.Append("<div class='doc_div inner'>");
            strBuil2.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil2.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil2.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil2.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil2.Append("</a>");
            strBuil2.Append("<div class='docBtns'>");
            strBuil2.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil2.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil2.Append("</div>");
            //strBuil2.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil2.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil2.Append("<div class='clear'></div>");
            strBuil2.Append("</div>");
            strBuil2.Append("</li>");

        }

        docValue2 = strBuil2.ToString();
    }


}