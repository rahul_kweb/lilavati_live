﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="patients_education_brochure.aspx.cs" Inherits="patients_education_brochure" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox({
                loop: false,
                afterLoad: function () {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Patients Education Brochure</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <p>We publish brochures frequently to help our patients with material on care and precautionary measures, new updates in technology &amp; various other medical issues. These brochures contain highly worthwhile information and are recommended to be read by all.</p>

                <ul class="ped fade_anim">
                    <asp:Repeater ID="rptPatientBrochure" runat="server">
                        <ItemTemplate>
                            <li>
                                <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                    <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                    <span><%#Eval("TITTLE") %> </span></a>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>

                    <!-- Commented This Repeater -->

                    <asp:Repeater ID="rptTittle" runat="server" OnItemDataBound="rptTittle_ItemDataBound" Visible="false">
                        <ItemTemplate>
                            <li>
                                <asp:HyperLink ID="hyplnk" runat="server" class="fancybox" data-fancybox-group='<%#Eval("TITTLE") %>'>
                                    <asp:Image ID="imgfirst" runat="server" />
                                    <span><%#Eval("TITTLE") %>
                                        <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("PAT_ID") %>' />
                                    </span>
                                </asp:HyperLink>
                                <div style="display: none">

                                    <asp:Repeater ID="rptBrochure" runat="server">
                                        <ItemTemplate>
                                            <a href='<%#Eval("BROCHURE","uploads/brochure/{0}") %>' class="fancybox" data-fancybox-group='<%#Eval("TITTLE") %>'></a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>

                    <!-- End -->

                    <%--<li><a href="images/brochure/aps_labour/Pg1.jpg" class="fancybox" data-fancybox-group="gallery">
                        <img src="images/brochure/aps_labour/Pg1.jpg">
                        <span>APS Labour Analgesia</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/aps_labour/Pg2.jpg" class="fancybox" data-fancybox-group="gallery"></a>
                            <a href="images/brochure/aps_labour/Pg3.jpg" class="fancybox" data-fancybox-group="gallery"></a>
                            <a href="images/brochure/aps_labour/Pg4.jpg" class="fancybox" data-fancybox-group="gallery"></a>
                            <a href="images/brochure/aps_labour/Pg5.jpg" class="fancybox" data-fancybox-group="gallery"></a>
                            <a href="images/brochure/aps_labour/Pg6.jpg" class="fancybox" data-fancybox-group="gallery"></a>
                        </div>
                    </li>
                    <li><a href="images/brochure/ecp/Pg1.jpg" class="fancybox" class="fancybox" data-fancybox-group="gallery2">
                        <img src="images/brochure/ecp/Pg1.jpg">
                        <span>ECP</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/ecp/Pg2.jpg" class="fancybox" data-fancybox-group="gallery2"></a>
                            <a href="images/brochure/ecp/Pg3.jpg" class="fancybox" data-fancybox-group="gallery2"></a>
                            <a href="images/brochure/ecp/Pg4.jpg" class="fancybox" data-fancybox-group="gallery2"></a>
                            <a href="images/brochure/ecp/Pg5.jpg" class="fancybox" data-fancybox-group="gallery2"></a>
                            <a href="images/brochure/ecp/Pg6.jpg" class="fancybox" data-fancybox-group="gallery2"></a>
                        </div>
                    </li>
                    <li><a href="images/brochure/food_drug/Pg1.jpg" class="fancybox" class="fancybox" data-fancybox-group="gallery3">
                        <img src="images/brochure/food_drug/Pg1.jpg">
                        <span>Food & Drug Interaction</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/food_drug/Pg2.jpg" class="fancybox" data-fancybox-group="gallery3"></a>
                            <a href="images/brochure/food_drug/Pg3.jpg" class="fancybox" data-fancybox-group="gallery3"></a>
                            <a href="images/brochure/food_drug/Pg4.jpg" class="fancybox" data-fancybox-group="gallery3"></a>
                        </div>
                    </li>
                    <li><a href="images/brochure/paediatric/Pg1.jpg" class="fancybox" class="fancybox" data-fancybox-group="gallery4">
                        <img src="images/brochure/paediatric/Pg1.jpg">
                        <span>Paediatric</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/paediatric/Pg2.jpg" class="fancybox" data-fancybox-group="gallery4"></a>
                            <a href="images/brochure/paediatric/Pg3.jpg" class="fancybox" data-fancybox-group="gallery4"></a>
                            <a href="images/brochure/paediatric/Pg4.jpg" class="fancybox" data-fancybox-group="gallery4"></a>
                        </div>
                    </li>
                    <li><a href="images/brochure/pain_mngt/Pg1.jpg" class="fancybox" class="fancybox" data-fancybox-group="gallery5">
                        <img src="images/brochure/pain_mngt/Pg1.jpg">
                        <span>Pain Management</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/pain_mngt/Pg2.jpg" class="fancybox" data-fancybox-group="gallery5"></a>
                            <a href="images/brochure/pain_mngt/Pg3.jpg" class="fancybox" data-fancybox-group="gallery5"></a>
                            <a href="images/brochure/pain_mngt/Pg4.jpg" class="fancybox" data-fancybox-group="gallery5"></a>
                            <a href="images/brochure/pain_mngt/Pg5.jpg" class="fancybox" data-fancybox-group="gallery5"></a>
                            <a href="images/brochure/pain_mngt/Pg6.jpg" class="fancybox" data-fancybox-group="gallery5"></a>
                        </div>
                    </li>
                    <li><a href="images/brochure/pca/Pg1.jpg" class="fancybox" class="fancybox" data-fancybox-group="gallery6">
                        <img src="images/brochure/pca/Pg1.jpg">
                        <span>Patient Controlled Analgesia</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/pca/Pg2_3.jpg" class="fancybox" data-fancybox-group="gallery6"></a>
                            <a href="images/brochure/pca/Pg4.jpg" class="fancybox" data-fancybox-group="gallery6"></a>
                            <a href="images/brochure/pca/Pg5.jpg" class="fancybox" data-fancybox-group="gallery6"></a>
                            <a href="images/brochure/pca/Pg6.jpg" class="fancybox" data-fancybox-group="gallery6"></a>
                        </div>
                    </li>
                    <li><a href="images/brochure/eye_donation/Pg1.jpg" class="fancybox" class="fancybox" data-fancybox-group="gallery7">
                        <img src="images/brochure/eye_donation/Pg1.jpg">
                        <span>Eye Donation</span> </a>
                        <div style="display: none">
                            <a href="images/brochure/eye_donation/Pg2.jpg" class="fancybox" data-fancybox-group="gallery7"></a>
                            <a href="images/brochure/eye_donation/Pg3.jpg" class="fancybox" data-fancybox-group="gallery7"></a>
                            <a href="images/brochure/eye_donation/Pg4.jpg" class="fancybox" data-fancybox-group="gallery7"></a>
                        </div>
                    </li>--%>
                </ul>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</asp:Content>

