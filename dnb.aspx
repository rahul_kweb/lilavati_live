﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="dnb.aspx.cs" Inherits="dnb" %>

<%@ Register TagName="Professionals" TagPrefix="menu" Src="Control/professionals.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">DNB</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<p><a href="docs/Congratulations_Radiology_Doctors.doc" target="_blank">DNB Trainees Awarded at EDURAD Grand Teaching Files Case Presentation</a></p>

                <p>
                    Lilavati Hospital is committed towards developing competent human resources in the medical arena.<br>
                    <br>
                    The hospital is a teaching institute and accredited by National Board of Examination for imparting training on different Broad and Superspeciality.
                </p>


                <table class="dnb">
                    <thead>
                        <tr>
                            <td>Sr.</td>
                            <td>Name of the Course</td>
                            <td>Eligibility</td>
                            <td>Duration</td>
                            <td>Course Commences</td>
                            <td>No. of Seats</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Sr.">1.</td>
                            <td data-title="Name of the Course">General Medicine</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">July</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">2.</td>
                            <td data-title="Name of the Course">Pediatrics – Primary</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">July</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">3.</td>
                            <td data-title="Name of the Course">Pediatrics – Secondary</td>
                            <td data-title="Eligibility">DCH + PDCET</td>
                            <td data-title="Duration">2 Years</td>
                            <td data-title="Course Commences">July</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">4</td>
                            <td data-title="Name of the Course">Pediatric Surgery</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">6 Years</td>
                            <td data-title="Course Commences">July</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">5</td>
                            <td data-title="Name of the Course">Radiology – Primary</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">July</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">6</td>
                            <td data-title="Name of the Course">Radiology – Secondary</td>
                            <td data-title="Eligibility">DMRD + PDCET</td>
                            <td data-title="Duration">2 Years</td>
                            <td data-title="Course Commences">July</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">7</td>
                            <td data-title="Name of the Course">Chest Medicine - Primary</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">8</td>
                            <td data-title="Name of the Course">Chest Medicine – Secondary</td>
                            <td data-title="Eligibility">DTCD + PDCET</td>
                            <td data-title="Duration">2 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">9</td>
                            <td data-title="Name of the Course">General Surgery</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">10</td>
                            <td data-title="Name of the Course">Orthopedics</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">11</td>
                            <td data-title="Name of the Course">Obs & Gynaecology – Primary</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">12</td>
                            <td data-title="Name of the Course">Obs & Gynaecology – Secondary</td>
                            <td data-title="Eligibility">DGO + PDCET</td>
                            <td data-title="Duration">2 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">13</td>
                            <td data-title="Name of the Course">Urology</td>
                            <td data-title="Eligibility">MS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">14</td>
                            <td data-title="Name of the Course">Neuro Surgery</td>
                            <td data-title="Eligibility">MBBS + DNB CET</td>
                            <td data-title="Duration">6 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">15</td>
                            <td data-title="Name of the Course">Nephrology</td>
                            <td data-title="Eligibility">MD + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">16</td>
                            <td data-title="Name of the Course">G.I. Surgery</td>
                            <td data-title="Eligibility">MS + DNB CET</td>
                            <td data-title="Duration">3 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">01</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">17</td>
                            <td data-title="Name of the Course">Critical Care Medicine</td>
                            <td data-title="Eligibility">MD + FET</td>
                            <td data-title="Duration">2 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                        <tr>
                            <td data-title="Sr.">18</td>
                            <td data-title="Name of the Course">Reproductive Medicine</td>
                            <td data-title="Eligibility">MS + FET</td>
                            <td data-title="Duration">2 Years</td>
                            <td data-title="Course Commences">January</td>
                            <td data-title="No. of Seats">02</td>
                        </tr>

                    </tbody>
                </table>

                <p>
                    For further details contact –<br>
                    Name: <strong>Ms. Apoorva Prabhu</strong><br>
                    Phone Number : 022- 2656 8336<br>
                    Email Address : <a href="mailto:careers@lilavatihospital.com">careers@lilavatihospital.com</a>
                </p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Professionals ID="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

