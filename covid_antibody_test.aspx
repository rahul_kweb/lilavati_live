﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="covid_antibody_test.aspx.cs" Inherits="covid_antibody_test" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">COVID Antibody Test</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>


                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

<%--                <asp:Literal ID="ltrContent" runat="server"></asp:Literal>--%>



                <div class="accd_box">
                    <div class="hd c_expand_hd">
                       Test name
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Qualitative Detection of antibodies (including IgG) to Severe Acute Respiratory Syndrome  Coronavirus 2 (SARS‑CoV‑2)</p>         
                        </div>
                    <div class="clear">
                    </div>
                </div>

                <!-- xxxxxxxxxxxxxxxxxx -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">
                   Methodology
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>ECLIA</p>                 
                    </div>
                    <div class="clear">
                    </div>
                </div>

                <!-- xxxxxxxxxxxxxxxxxx -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">
                        About the test
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <div class="tab_content">
                            <ul class="bullet">
                              <li>Only for surveillance and not diagnosis</li>
                                <li>Serological tests detect antibodies present in the blood when the body is responding to a specific infection, like COVID-19.  They detect the body's immune  response to the infection caused by the virus rather than detecting the virus itself.</li>
                                <li>Positive antibody test is presumed to mean a person has been infected with SARS-CoV-2, the virus that causes COVID-19, at some point in the past and has recovered. It does not mean they are currently infected.</li>
                                <li>Antibodies start developing within 1 to 3 weeks after infection.</li>
                                 </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                <!-- xxxxxxxxxxxxxxxxxx -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">
                        What are the charges for the test?
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Rs.950/- only (excluding home sample collection charges)</p>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                  <div class="accd_box">
                    <div class="hd c_expand_hd">
                        Is home sample collection available for this test?
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Yes. But at an additional cost. Please contact 8879677193/8879677196 for booking your home sample collection appointment (between 9am to 9pm from Mon to Sat and from 9am to 3pm on Sundays and Public Holidays)</p>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                   <div class="accd_box">
                    <div class="hd c_expand_hd">
                        Can I come to hospital for the test?
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Walkin between 9am to 5pm at ground floor pathology department</p>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                  <div class="accd_box">
                    <div class="hd c_expand_hd">
                        What are the pre requisite for the test?
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>No fasting is required.A general form pertaining symptoms,contact details,Adhar number is to be filled while giving sample</p>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                  <div class="accd_box">
                    <div class="hd c_expand_hd">
                        When can we get the results of this test?
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Next day morning .You can share your email id for getting the soft copy of the report</p>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                   <div class="accd_box">
                    <div class="hd c_expand_hd">
                        Who should do the test
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>With recent ICMR Advisory-Newer additional testing strategies dated 23/6/2020 this test can be done for possible groups /community/ population based on specific requirement for serosurvey like:</p>
                    <ul class="bullet">
                        <li>Immunocompromised patients</li>
                        <li>Individuals in containment zone</li>
                        <li>Health care workers</li>
                        <li>Security persons</li>
                        <li>Police and paramilitary personnel civil defence & volunteers</li>
                        <li>Press corps</li>
                          <li>Migrant workers</li>
                        <li>Industrial workers and Labour force</li>
                        <li>Farmers and Vendors visiting Large markets</li>
                        <li>Staff in Municipal body</li>
                        <li>Drivers</li>
                        <li>Banks, post, couriers, telecom offices</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>


                     <div class="accd_box">
                    <div class="hd c_expand_hd">
                        Why is the test useful
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p> Antibody test is to be used for serosurveys. This test can contribute to identify individuals exposed to SARS-CoV 2  virus and assess the extent of exposure of a population.</p>
                    <p><i>*This test is not to be used for Diagnosis of COVID 19 infection</i></p>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                   <div class="accd_box">
                    <div class="hd c_expand_hd">
                        What if the test comes reactive?
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                       <ul class="bullet">
                           <li>No need for isolation</li>
                           <li>If test results show that you have antibodies, it indicates that you were likely infected with COVID-19 at some time in the past. It may also mean that you have some immunity.</li>
                           <li>You are eligible to donate plasma, a part of their blood. This plasma could be used to treat others with severe disease and boost the ability to fight the virus</li>
                       </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>

                    <div class="accd_box">
                    <div class="hd c_expand_hd">
                        How is our test better than Rapid test done by other centres
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>ICMR  the regulatory body has strictly advised  to use ELISA and CLIA assay to do antibody testing  in its advisory dated 23/6/2020 which is available on ICMR website.</p>
                    </div>
                    <div class="clear">
                    </div>
                </div>


                <!-- xxxxxxxxxxxxxxxxxx -->
                
                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />


            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

