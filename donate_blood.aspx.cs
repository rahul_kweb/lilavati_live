﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class donate_blood : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
            BindRepeater();
        }
    }

    public void Content()
    {
        cms.ID = 15;
        cmsbal.GETBYIDContent(cms);
        litContent.Text = cms.Description;
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Donate Blood";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();
        }
    }
}