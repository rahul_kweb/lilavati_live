﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="photo_gallery.aspx.cs" Inherits="photo_gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox({
                loop: false,
                padding: 1,
                // afterLoad: function () {
                //      this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                //  }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Photo Gallery</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="print_share">
                <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                <div class="addthis_native_toolbox"></div>
                <div class="clear"></div>
            </div>

            <ul class="gallery">

                <asp:Repeater ID="rptGallery" runat="server">
                    <ItemTemplate>
                        <li><a href='<%#Eval("IMAGE","uploads/gallery/{0}") %>' class="fancybox" data-fancybox-group="gallery" title='<%#Eval("TITTLE") %>'>
                            <div>
                                <img src='<%#Eval("IMAGE","uploads/gallery/{0}") %>'>
                            </div>
                            <span style="display: <%#Eval("TITTLE").ToString()==""?"none":"block" %>"><%#Eval("TITTLE") %></span>
                        </a></li>
                    </ItemTemplate>
                </asp:Repeater>

                <%--  <li><a href="images/gallery/1.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/1.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/2.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/2.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/3.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/3.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/4.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/4.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/5.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/5.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/6.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/6.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/7.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/7.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/8.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/8.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/9.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/9.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/10.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/10.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/11.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/11.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/12.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/12.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/13.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/13.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/14.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/14.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/15.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/15.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/16.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/16.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/17.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/17.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/18.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/18.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/19.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/19.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/20.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/20.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/21.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/21.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/22.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/22.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/23.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/23.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/24.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/24.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/25.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/25.jpg">
                    </div>
                </a></li>
                <li><a href="images/gallery/26.jpg" class="fancybox" data-fancybox-group="gallery">
                    <div>
                        <img src="images/gallery/26.jpg">
                    </div>
                </a></li>--%>
            </ul>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

