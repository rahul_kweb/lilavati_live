﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="contact_us.aspx.cs" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script type="text/javascript" src="js/modernizr.custom.71422.js"></script>
    <script type="text/javascript" src="js/pushy.min.js"></script>--%>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQbt8fBXZu0RHPqPCRM3GPMRklOt-9Als&callback=initMap"></script>

    <%--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>--%>
    <script type="text/javascript" src="js/mapmarker.jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var myMarkers = {
                "markers": [
                { "latitude": "19.051166", "longitude": "72.828956", "icon": "images/mappointer.png", "baloon_text": '<strong>Lilavati Hospital & Research Center</strong> <p>A-791, Bandra Reclamation,<br> Bandra (W), Mumbai - 400050.<br> India.</p>' },
                ]
            };

            $(".map_div").mapmarker({
                zoom: 16,
                center: '19.051166, 72.828956',
                markers: myMarkers
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Contact Us</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <%--          <div class='inner_banner'>
        <img src="/images/contact_new_banner.jpg"/>
          </div>--%>


            <div class="print_share">
                <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                <div class="addthis_native_toolbox"></div>
                <div class="clear"></div>
            </div>

            <div class="contact_table">
                <asp:Repeater ID="rptContactUS" runat="server">
                    <ItemTemplate>

                        <div class="contact_left">
                            <div class="uppercase green_text cont_tl">
                                <img src="images/ic_address.png">
                                How to reach us
                            </div>
                            <div class="clear"></div>
                            <p>
                                <%#Eval("ADDRESS") %>
                                <%-- Lilavati Hospital & Research Centre<br>
                        A-791, Bandra Reclamation, Bandra (W), Mumbai - 400050. India.--%>
                            </p>

                            <div class="map_div no_anim"></div>

                        </div>


                        <div class="contact_right">
                            <div class="uppercase green_text cont_tl">
                                <img src="images/ic_nos.png">
                                Important Contact Numbers
                            </div>
                            <div class="clear"></div>

                            <table class="contact_nos">
                               <%-- <tr>
                                    <td>Toll Free</td>
                                    <td><span><%#Eval("TollFree") %></span></td>
                                </tr>--%>
                                <tr>
                                    <td>Ambulance</td>
                                    <td><span><%#Eval("AMBULANCE") %> </span></td>
                                </tr>
                                <tr>
                                    <td>Boardline</td>
                                    <td><span><%#Eval("HOSPITAL_BOARD_LINE") %></span></td>
                                </tr>

                                <tr>
                                    <td>- Emergency/ Casualty</td>
                                    <td><span><%#Eval("EMERGENCY_CASUALTY") %> </span></td>
                                </tr>
                                <tr>
                                    <td>- Admission Department</td>
                                    <td><span><%#Eval("ADMISSION_DEPARTMENT") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Appointment - OPD</td>
                                    <td><span><%#Eval("APPOINTMENT_OPD") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Billing - Inpatient</td>
                                    <td><span><%#Eval("BILLING_INPATIENT") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Billing - OPD</td>
                                    <td><span><%#Eval("BILLING_OPD") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Blood Bank</td>
                                    <td><span><%#Eval("BLOOD_BANK") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Cardiology</td>
                                    <td><span><%#Eval("Cardiology") %></span></td>
                                </tr>
                                <tr>
                                    <td>- CT Scan Department</td>
                                    <td><span><%#Eval("CT_SCAN_DEPARTMENT") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Dental</td>
                                    <td><span><%#Eval("Dental") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Dermatology</td>
                                    <td><span><%#Eval("Dermatology") %></span></td>
                                </tr>
                                <tr>
                                    <td>- EMG/ EEG</td>
                                    <td><span><%#Eval("EMG_EEG") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Endoscopy</td>
                                    <td><span><%#Eval("Endoscopy") %></span></td>
                                </tr>
                                <tr>
                                    <td>- ENT/ Audiometry</td>
                                    <td><span><%#Eval("ENT_Audiometry") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Health Check-up Department</td>
                                    <td><span><%#Eval("HEALTH_CHECK_UP_DEPARTMENT") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Hospital Fax</td>
                                    <td><span><%#Eval("HOSPITAL_FAX") %></span></td>
                                </tr>
                                <tr>
                                    <td>- IVF</td>
                                    <td><span><%#Eval("IVF") %></span></td>
                                </tr>
                                <tr>
                                    <td>- MRI Department</td>
                                    <td><span><%#Eval("MRI_DEPARTMENT") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Nuclear Medicine</td>
                                    <td><span><%#Eval("Nuclear_Medicine") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Ophthalmology</td>
                                    <td><span><%#Eval("Ophthalmology") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Physiotherapy</td>
                                    <td><span><%#Eval("Physiotherapy") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Report Dispatch Counter</td>
                                    <td><span><%#Eval("REPORT_DISPATCH_COUNTER") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Sample Collection Room</td>
                                    <td><span><%#Eval("Sample_collection_room") %></span></td>
                                </tr>
                                <tr>
                                    <td>- TPA Cell</td>
                                    <td><span><%#Eval("TPA_CELL") %></span></td>
                                </tr>

                                <tr>
                                    <td>- X-Ray, Sonography Department</td>
                                    <td><span><%#Eval("X_RAY_SONOGRAPHY_DEPARTMENT") %></span></td>
                                </tr>
                                <tr>
                                    <td>- Visa Section</td>
                                    <td><span><%#Eval("Visa_Section") %></span></td>
                                </tr>
                            
                            </table>

                            <div class="uppercase green_text cont_tl">
                                <img src="images/ic_email.png">
                                Email Address
                            </div>
                            <%--  <div class="clear"></div>

                            <a href="mailto:<%#Eval("EMAIL_ADDRESS") %>" style="color: #000;"><%#Eval("EMAIL_ADDRESS") %></a>



                        </div>--%>


                            <table class="contact_nos">
                                   <tr>
                                    <td>For feedback /suggestions:</td>

                                    <%-- static mail address--%>
                                    <td><a href="mailto:feedback@lilavatihospital.com" style="color: #000;">feedback@lilavatihospital.com</a></td>

                                </tr>
                                <tr>
                                    <td>For grievances:</td>

                                    <%-- static mail address--%>
                                    <td><a href="mailto:complaints@lilavatihospital.com " style="color: #000;">complaints@lilavatihospital.com </a></td>

                                </tr>
                                <tr>
                                    <td>For any other queries</td>
                                    <%-- Dynamic mail address--%>
                                    <td><a href="mailto:<%#Eval("EMAIL_ADDRESS") %>" style="color: #000;"><%#Eval("EMAIL_ADDRESS") %></a></td>
                                </tr>
                            </table>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

