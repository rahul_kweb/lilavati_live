﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="book_appointment_page.aspx.cs" Inherits="book_appointment_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <link href="/select2/select2.min.css" rel="stylesheet" />
        <link href="/css/jquery.fancybox.min.css" rel="stylesheet" />

    <script src="/select2/select2.min.js"></script>
      <script src="/js/jquery.fancybox.min.js"></script>


    <script>
        $(document).ready(function () {
            $(".js-example-basic-single").select2();

            $(".js-example-basic-hide-search").select2({
                minimumResultsForSearch: Infinity
            });


            $('#spl').click(function () {
                var $example = $('#<%=ddlSpeciality.ClientID%>').select2();
                $example.select2("open");
            });

            $('#docname').click(function () {
                var $example = $('#<%=ddldrName.ClientID%>').select2();
                $example.select2("open");
            });

            $('#avltime').click(function () {
                var $example = $('#<%=ddlAvailableTime.ClientID%>').select2();
                $example.select2("open");
            });

            $('#vis').click(function () {
                var $example = $('#<%=ddlVisitTime.ClientID%>').select2("open").parent();
                //$("#visitfo").addClass("populated");

                $('#<%=ddlVisitTime.ClientID%>').change(function () {
                    var Visit = $(this).val();
                    if (Visit != '') {
                        $("#visitfo").addClass("populated");
                    }
                    else {
                        $("#visitfo").removeClass("populated");
                    }
                });

                //var $example = $('#<%=ddlVisitTime.ClientID%>').select2();
                //$example.select2("open");
            });


            $('#country').click(function () {
                var $example = $('#<%=ddlCountry.ClientID%>').select2();
                $example.select2("open");
            });

            $('#<%=ddlCountry.ClientID%>').change(function () {
                var country = $(this).val();
                if (country != '') {
                    $("#Count").addClass("populated");
                }
                else {
                    $("#Count").removeClass("populated");
                }
            });

            $('#<%=ddlSpeciality.ClientID%>').change(function () {
                var Speciality = $(this).val();
                if (Speciality != '') {
                    $("#Sqlfo").addClass("populated");
                }
                else {
                    $("#Sqlfo").removeClass("populated");
                }
            });

            $('#<%=ddldrName.ClientID%>').change(function () {
                var Doc = $(this).val();
                if (Doc != '') {
                    $("#docfo").addClass("populated");
                }
                else {
                    $("#docfo").removeClass("populated");
                }
            });

            $('#<%=ddlVisitTime.ClientID%>').change(function () {
                var Visit = $(this).val();
                if (Visit != '') {
                    $("#visitfo").addClass("populated");
                }
                else {
                    $("#visitfo").removeClass("populated");
                }
            });


            $('#<%=ddlAvailableTime.ClientID%>').change(function () {
                var Avail = $(this).val();
                if (Avail != '') {
                    $("#availfo").addClass("populated");
                }
                else {
                    $("#availfo").removeClass("populated");
                }
            });

            $('#<%=ddState.ClientID%>').change(function () {
                var state = $(this).val();
                if (state != '') {
                    $("#state").addClass("populated");
                }
                else {
                    $("#state").removeClass("populated");
                }
            });

            $('#<%=ddlCity.ClientID%>').change(function () {
                var city = $(this).val();
                if (city != '') {
                    $("#city").addClass("populated");
                }
                else {
                    $("#city").removeClass("populated");
                }
            });

            $('.doa').keypress(function (event) {
                event.preventDefault();
            });

        });


    </script>

    <script>

        $(document).ready(function () {
            var country = $('#<%=ddlCountry.ClientID%>').val();
            if (country != '') {
                $("#Count").addClass("populated");
            }
            else {
                $("#Count").removeClass("populated");
            }

        });


        $(function () {
            $(".cnty").change(function () {
                var data = $(this).val();
                //var data = $(this).find("option:selected").text();
                if (data == "100") {

                    $("#divstate1").show();
                    $("#divstate2").hide();

                    $("#divcity1").show();
                    $("#divcity2").hide();

                    var selectedText = '';
                    $('#ctl00_ContentPlaceHolder1_ddState option').map(function () {
                        if ($(this).text() == selectedText) return this;
                    }).attr('selected', 'selected');

                    document.getElementById("select2-ctl00_ContentPlaceHolder1_ddState-container").title = "";
                    document.getElementById("select2-ctl00_ContentPlaceHolder1_ddState-container").textContent = "";

                    var selectedText1 = '';
                    $('#ctl00_ContentPlaceHolder1_ddlCity option').map(function () {
                        if ($(this).text() == selectedText1) return this;
                    }).attr('selected', 'selected');

                    document.getElementById("select2-ctl00_ContentPlaceHolder1_ddlCity-container").title = "";
                    document.getElementById("select2-ctl00_ContentPlaceHolder1_ddlCity-container").textContent = "";

                    $("#state").removeClass("populated");
                    $("#city").removeClass("populated");

                    console.log("if" + data + " state : ");
                } else {
                    $("#divstate1").hide();
                    $("#divstate2").show();

                    $("#divcity1").hide();
                    $("#divcity2").show();

                    $('#<%=txtState.ClientID%>').val('');
                    $('#<%=txtCity.ClientID%>').val('');

                    $("#state").removeClass("populated");
                    $("#city").removeClass("populated");
                    console.log("else if" + data);
                }
            });
        });

        function ShowPopup() {
            $(function () {
                alert("Successfully");
                //$("#apply_form1").fancybox().trigger('click');

            });
        };



        var selectedday;
        var ddAvailTime = "";
        $(document).ready(function () {

            $('#<%=ddlAvailableTime.ClientID%>').on("change", function () {

                ddAvailTime = $('#<%=ddlAvailableTime.ClientID%> option:selected').text();

                $('#<%=txtDateOfAppointment.ClientID%>').val("");
                $('#dob').removeClass("populated");

                var OnlyThreeChar = ddAvailTime.substring(0, 3);

                var arr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

                if (arr[1] == OnlyThreeChar) {
                    selectedday = 1;
                }
                else if (arr[2] == OnlyThreeChar) {
                    selectedday = 2;
                }
                else if (arr[3] == OnlyThreeChar) {
                    selectedday = 3;
                }
                else if (arr[4] == OnlyThreeChar) {
                    selectedday = 4;
                }
                else if (arr[5] == OnlyThreeChar) {
                    selectedday = 5;
                }
                else if (arr[6] == OnlyThreeChar) {
                    selectedday = 6;
                }
                else if (OnlyThreeChar == "") {
                    selectedday = 8;
                    $('#availfo').removeClass("populated");
                }
                // console.log("values : " + ddAvailTime + " Only Three char : " + OnlyThreeChar + "Array : " + arr[0]);

            });

            /** On Page Load **/

            ddAvailTime = $('#<%=ddlAvailableTime.ClientID%> option:selected').text();
            var OnlyThreeChar = ddAvailTime.substring(0, 3);
            var arr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

            if (arr[1] == OnlyThreeChar) {
                selectedday = 1;
            }
            else if (arr[2] == OnlyThreeChar) {
                selectedday = 2;
            }
            else if (arr[3] == OnlyThreeChar) {
                selectedday = 3;
            }
            else if (arr[4] == OnlyThreeChar) {
                selectedday = 4;
            }
            else if (arr[5] == OnlyThreeChar) {
                selectedday = 5;
            }
            else if (arr[6] == OnlyThreeChar) {
                selectedday = 6;
            }
            //console.log("load " + selectedday + " ava : " + ddAvailTime.length + " last three char : " + OnlyThreeChar + " final : " + selectedday);

            /** End **/

        });



        /** Days to be disabled as an array */
        // var disableddates = ["8-15-2016", "8-25-2016", "9-5-2016", "9-15-2016", "10-11-2016", "11-1-2016", "1-26-2017"];
        var disableddates = "";

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "/book_appointment_page.aspx/GetTimeForBind",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    // alert(response.d);
                    // console.log(response.d);

                    var xmlDoc = $.parseXML(response.d);
                    var xml = $(xmlDoc);
                    // var alldate = xml.find("Table1");

                    // alert(customers);
                    // console.log(alldate);
                    var check = "";
                    var getdisabledate = [];


                    xml.find("Table1").each(function () {

                        // check += '"' + $(this).find("NOTAVAILBLEDATE").text().replace("/", "-").replace("/", "-") + '"' + ',';
                        check = $(this).find("NOTAVAILBLEDATE").text().replace("/", "-").replace("/", "-");
                        getdisabledate.push(check);

                    });
                    // var finaldate = check.substring(0, check.length - 1);

                    //  finaldate = '[' + finaldate + ']';
                    //finaldate = finaldate;
                    console.log(getdisabledate);
                    if (getdisabledate != '') {
                        //console.log(disableddates + "if");
                        disableddates = getdisabledate;
                    }
                    else {
                        disableddates = ["8-15-2016"];
                        // console.log(disableddates + "else");

                    }



                    //check = check.substring(0, check, length - 1);
                    //console.log(idcontainer);

                }
            });
        });







        function DisableSpecificDates(date) {

            var day = date.getDay();

            var m = date.getMonth();
            var d = date.getDate();
            var y = date.getFullYear();

            // First convert the date in to the mm-dd-yyyy format 
            // Take note that we will increment the month count by 1 
            var currentdate = (m + 1) + '-' + d + '-' + y;

            //console.log(currentdate + " mo :" + m + " disa : " + disableddates);

            // We will now check if the date belongs to disableddates array 
            for (var i = 0; i < disableddates.length; i++) {

                // Now check if the current date is in disabled dates array. 
                if ($.inArray(currentdate, disableddates) != -1 || day != selectedday) {
                    return [false];
                } else {
                    return [true];
                }
            }

        }

        $(function () {
            $(".doa").datepicker({
                minDate: 1,
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'dd-mm-yy',
                maxDate: 90,
                beforeShowDay: DisableSpecificDates

            });

            $(".doa").change(function () {
                var data = $(".doa").val();
                if (data.length != 0 || data != "") {
                    $("#dob").removeClass("error");
                    $("#dob").addClass("populated");
                }
                else {
                    $("#dob").addClass("error");
                }
            });

        });

    </script>

    <script>
        function CheckSave() {
            var Speciality = $('#<%=ddlSpeciality.ClientID%>').val();
            var Doctor = $('#<%=ddldrName.ClientID%>').val();
            var Visit = $('#<%=ddlVisitTime.ClientID%>').val();
            var DateOfAppointment = $('#<%=txtDateOfAppointment.ClientID%>').val();
            var AvailTm = $('#<%=ddlAvailableTime.ClientID%>').val();
            var PatientNAme = $('#<%=txtPatientName.ClientID%>').val();
            var Email = $('#<%=txtEmailId.ClientID%>').val();
            var Mobile = $('#<%=txtMobileNo.ClientID%>').val();
            var Address = $('#<%=txtAddress.ClientID%>').val();
            var blank = false;

            if (Speciality == '') {
                $("#Sqlfo").addClass("error");
                blank = true;
            }
            else {
                $("#Sqlfo").removeClass("error");
            }

            if (Doctor == '') {
                $("#docfo").addClass("error");
                blank = true;
            }
            else {
                $("#docfo").removeClass("error");
            }

            if (Visit == '') {
                $("#visitfo").addClass("error");
                blank = true;
            }
            else {
                $("#visitfo").removeClass("error");
            }

            if (DateOfAppointment == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (AvailTm == '') {
                $("#availfo").addClass("error");
                blank = true;
            }
            else {
                $("#availfo").removeClass("error");
            }

            if (PatientNAme == '') {
                $("#PatientNAme").addClass("error");
                blank = true;
            }
            else {
                $("#PatientNAme").removeClass("error");
            }

            if (Email == '') {
                $("#Email").addClass("error");
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $("#Email").addClass("error");
                    alert("Valid Email ID");
                    blank = true;
                }
                else {
                    $("#Email").removeClass("error");
                }
            }

            if (Mobile == '') {
                $("#Mobile").addClass("error");
                blank = true;
            }
            else {
                if (Mobile.length < 10) {
                    $("#Mobile").addClass("error");
                    blank = true;
                }
                else {
                    $("#Mobile").removeClass("error");
                }
            }

            if (Address == '') {
                $("#Address").addClass("error");
                blank = true;
            }
            else {
                $("#Address").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>

    <script>
        function SpecialityFun() {
            var Speciality = $('#<%=ddlSpeciality.ClientID%>').val();
            var blank = false;

            if (Speciality == '') {
                $("#Sqlfo").addClass("error");
                blank = true;
            }
            else {
                $("#Sqlfo").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function DoctorFun() {
            var Doctor = $('#<%=ddldrName.ClientID%>').val();
            var blank = false;

            if (Doctor == '') {
                $("#docfo").addClass("error");
                blank = true;
            }
            else {
                $("#docfo").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function VisitFun() {
            var Visit = $('#<%=ddlVisitTime.ClientID%>').val();
            var blank = false;

            if (Visit == '') {
                $("#visitfo").addClass("error");
                blank = true;
            }
            else {
                $("#visitfo").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function AvailTmFun() {
            var AvailTm = $('#<%=ddlAvailableTime.ClientID%>').val();
            var blank = false;

            if (AvailTm == '') {
                $("#availfo").addClass("error");
                blank = true;
            }
            else {
                $("#availfo").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function PatientNameFun() {
            var PatientNAme = $('#<%=txtPatientName.ClientID%>').val();
            var blank = false;

            if (PatientNAme == '') {
                $("#PatientNAme").addClass("error");
                blank = true;
            }
            else {
                $("#PatientNAme").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function EmailFun() {
            var Email = $('#<%=txtEmailId.ClientID%>').val();
            var blank = false;

            if (Email == '') {
                $("#Email").addClass("error");
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $("#Email").addClass("error");
                    alert("Valid Email ID");
                    blank = true;
                }
                else {
                    $("#Email").removeClass("error");
                }
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function MobileFun() {
            var Mobile = $('#<%=txtMobileNo.ClientID%>').val();
            var blank = false;

            if (Mobile == '') {
                $("#Mobile").addClass("error");
                blank = true;
            }
            else {
                if (Mobile.length < 10) {
                    $("#Mobile").addClass("error");
                    blank = true;
                }
                else {
                    $("#Mobile").removeClass("error");
                }
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }

        }

        function AddressFun() {
            var Address = $('#<%=txtAddress.ClientID%>').val();
            var blank = false;

            if (Address == '') {
                $("#Address").addClass("error");
                blank = true;
            }
            else {
                $("#Address").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function DateOFAppFun() {
            var DateOfAppointment = $('#<%=txtDateOfAppointment.ClientID%>').val();
            var blank = false;

            if (DateOfAppointment == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        $(document).ready(function () {

            $('#<%=ddlSpeciality.ClientID%>').on("change", function () {
                SpecialityFun();
            });

            $('#<%=ddldrName.ClientID%>').on("change", function () {
                DoctorFun();
            });

            $('#<%=ddlVisitTime.ClientID%>').on("change", function () {
                VisitFun();
            });

            $('#<%=ddlAvailableTime.ClientID%>').on("change", function () {
                AvailTmFun();
            });

            $('#<%=txtPatientName.ClientID%>').on("click", function () {
                PatientNameFun();
            });
            $('#<%=txtPatientName.ClientID%>').on("keypress", function () {
                PatientNameFun();
            });

            $('#<%=txtEmailId.ClientID%>').on("click", function () {
                EmailFun();
            });
            $('#<%=txtEmailId.ClientID%>').on("change", function () {
                EmailFun();
            });

            $('#<%=txtMobileNo.ClientID%>').on("click", function () {
                MobileFun();
            });
            $('#<%=txtMobileNo.ClientID%>').on("keypress", function () {
                MobileFun();
            });
            $('#<%=txtMobileNo.ClientID%>').on("change", function () {
                MobileFun();
            });

            $('#<%=txtAddress.ClientID%>').on("click", function () {
                AddressFun();
            });
            $('#<%=txtAddress.ClientID%>').on("keypress", function () {
                AddressFun();
            });
            $('#<%=txtAddress.ClientID%>').on("change", function () {
                AddressFun();
            });

            $('#<%=txtDateOfAppointment.ClientID%>').on("click", function () {
                DateOFAppFun();
            });
            $('#<%=txtDateOfAppointment.ClientID%>').on("change", function () {
                DateOFAppFun();
            });

        });

    </script>

    <script>

        $(document).ready(function () {

            var data = $(".specialityclass").val();
            if (data.length != 0 || data != "") {
                //$("#Sqlfo").removeClass("error");
                $("#Sqlfo").addClass("populated");
                //alert("sxjsxjsbxc");
            }
            else {
                //  $("#Sqlfo").addClass("error");
            }

            var data1 = $(".drclass").val();
            if (data1.length != 0 || data1 != "") {
                //$("#docfo").removeClass("error");
                $("#docfo").addClass("populated");
                //alert("sxjsxjsbxc");
            }
            else {
                //$("#docfo").addClass("error");
            }



        });

    </script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Book An Appointment</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <asp:Panel ID="pnlForm" runat="server" Visible="true">
                <div class="form_div FlowupLabels">

                    <div class="form_field fl_wrap" id="Sqlfo">
                        <label id="spl" class="fl_label uppercase spl">Speciality</label>
                        <asp:DropDownList ID="ddlSpeciality" runat="server" OnSelectedIndexChanged="ddlSpeciality_SelectedIndexChanged" AutoPostBack="true" class="js-example-basic-single fl_input specialityclass">
                        </asp:DropDownList>
                    </div>

                    <div class="form_field fl_wrap" id="docfo">
                        <label id="docname" class="fl_label uppercase">Name of Doctor</label>
                        <asp:DropDownList ID="ddldrName" runat="server" OnSelectedIndexChanged="ddldrName_SelectedIndexChanged" AutoPostBack="true" class="js-example-basic-single fl_input drclass">
                        </asp:DropDownList>
                    </div>

                    <div class="form_field fl_wrap" id="visitfo">
                        <label id="vis" class="fl_label uppercase">Visit</label>
                        <asp:DropDownList ID="ddlVisitTime" runat="server" class="js-example-basic-hide-search fl_input">
                            <asp:ListItem></asp:ListItem>
                            <asp:ListItem>First</asp:ListItem>
                            <asp:ListItem>Follow Up</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="clear"></div>

                    <div class="form_field fl_wrap" id="availfo">
                        <label id="avltime" class="fl_label uppercase">Available Timing</label>
                        <asp:DropDownList ID="ddlAvailableTime" runat="server" class="js-example-basic-single fl_input">
                        </asp:DropDownList>
                    </div>

                    <div class="form_field fl_wrap" id="dob">
                        <label class="fl_label uppercase" for="doa">Date of Appointment</label>
                        <asp:TextBox ID="txtDateOfAppointment" runat="server" class="fl_input doa" autocomplete="off"></asp:TextBox>
                    </div>


                    <%--<div class="form_field fl_wrap">
                    <label class="fl_label uppercase">Preferred Time</label>
                    <select class="js-example-basic-single fl_input">
                        <option></option>
                        <option>Male</option>
                        <option>Female</option>
                    </select>
                </div>--%>

                    <div class="form_field fl_wrap" id="PatientNAme">
                        <label class="fl_label uppercase">Patient's Name</label>
                        <asp:TextBox ID="txtPatientName" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="form_field fl_wrap" id="Email">
                        <label class="fl_label uppercase">Email Id</label>
                        <asp:TextBox ID="txtEmailId" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="form_field fl_wrap" id="Mobile">
                        <label class="fl_label uppercase">Mobile No.</label>
                        <asp:TextBox ID="txtMobileNo" runat="server" class="fl_input" MaxLength="10" onKeyPress='javascript:return onlyNumbers();' autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="clear"></div>

                    <div class="full_width fl_wrap" id="Address">
                        <label class="fl_label uppercase">Address</label>
                        <asp:TextBox ID="txtAddress" runat="server" class="fl_input" TextMode="MultiLine" autocomplete="off"></asp:TextBox>
                    </div>

                    <div class="clear"></div>

                    <div class="form_field fl_wrap" id="Count">
                        <label id="country" class="fl_label uppercase">Country</label>
                        <asp:DropDownList ID="ddlCountry" runat="server" class="fl_input js-example-basic-single cnty">
                        </asp:DropDownList>
                    </div>

                    <div class="form_field fl_wrap" id="state">
                        <label class="fl_label uppercase">State</label>
                        <div id="divstate1" style="display: block;">
                            <asp:DropDownList ID="ddState" runat="server" class="fl_input js-example-basic-single sta">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Maharashtra</asp:ListItem>
                                <asp:ListItem>Andaman and Nicobar Islands</asp:ListItem>
                                <asp:ListItem>Andhra Pradesh</asp:ListItem>
                                <asp:ListItem>Arunachal Pradesh</asp:ListItem>
                                <asp:ListItem>Assam</asp:ListItem>
                                <asp:ListItem>Bihar</asp:ListItem>
                                <asp:ListItem>Chandigarh</asp:ListItem>
                                <asp:ListItem>Chattisgarh</asp:ListItem>
                                <asp:ListItem>Dadra and Nagar Haveli</asp:ListItem>
                                <asp:ListItem>Daman and Diu</asp:ListItem>
                                <asp:ListItem>Delhi</asp:ListItem>
                                <asp:ListItem>Goa</asp:ListItem>
                                <asp:ListItem>Gujarat</asp:ListItem>
                                <asp:ListItem>Haryana</asp:ListItem>
                                <asp:ListItem>Himachal Pradesh</asp:ListItem>
                                <asp:ListItem>Jammu and Kashmir</asp:ListItem>
                                <asp:ListItem>Jharkhand</asp:ListItem>
                                <asp:ListItem>Karnataka</asp:ListItem>
                                <asp:ListItem>Kerala</asp:ListItem>
                                <asp:ListItem>Lakshadweep</asp:ListItem>
                                <asp:ListItem>Madhya Pradesh</asp:ListItem>
                                <asp:ListItem>Manipur</asp:ListItem>
                                <asp:ListItem>Meghalaya</asp:ListItem>
                                <asp:ListItem>Mizoram</asp:ListItem>
                                <asp:ListItem>Nagaland</asp:ListItem>
                                <asp:ListItem>Orissa</asp:ListItem>
                                <asp:ListItem>Pondicherry</asp:ListItem>
                                <asp:ListItem>Punjab</asp:ListItem>
                                <asp:ListItem>Rajasthan</asp:ListItem>
                                <asp:ListItem>Sikkim</asp:ListItem>
                                <asp:ListItem>Tamil Nadu</asp:ListItem>
                                <asp:ListItem>Tripura</asp:ListItem>
                                <asp:ListItem>Uttarakhand</asp:ListItem>
                                <asp:ListItem>Uttaranchal</asp:ListItem>
                                <asp:ListItem>Uttar Pradesh</asp:ListItem>
                                <asp:ListItem>West Bengal</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div id="divstate2" style="display: none;">
                            <asp:TextBox ID="txtState" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form_field fl_wrap" id="city">
                        <label class="fl_label uppercase">City</label>
                        <div id="divcity1" style="display: block;">
                            <asp:DropDownList ID="ddlCity" runat="server" class="fl_input js-example-basic-single">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem>Mumbai</asp:ListItem>
                                <asp:ListItem>Delhi</asp:ListItem>
                                <asp:ListItem>Bangalore</asp:ListItem>
                                <asp:ListItem>Hyderabad</asp:ListItem>
                                <asp:ListItem>Ahmedabad</asp:ListItem>
                                <asp:ListItem>Chennai</asp:ListItem>
                                <asp:ListItem>Kolkata</asp:ListItem>
                                <asp:ListItem>Surat</asp:ListItem>
                                <asp:ListItem>Pune</asp:ListItem>
                                <asp:ListItem>Jaipur</asp:ListItem>
                                <asp:ListItem>Lucknow</asp:ListItem>
                                <asp:ListItem>Kanpur</asp:ListItem>
                                <asp:ListItem>Nagpur</asp:ListItem>
                                <asp:ListItem>Indore</asp:ListItem>
                                <asp:ListItem>Thane</asp:ListItem>
                                <asp:ListItem>Bhopal</asp:ListItem>
                                <asp:ListItem>Visakhapatnam</asp:ListItem>
                                <asp:ListItem>Patna</asp:ListItem>
                                <asp:ListItem>Vadodara</asp:ListItem>
                                <asp:ListItem>Ghaziabad</asp:ListItem>
                                <asp:ListItem>Ludhiana</asp:ListItem>
                                <asp:ListItem>Agra</asp:ListItem>
                                <asp:ListItem>Nashik</asp:ListItem>
                                <asp:ListItem>Faridabad</asp:ListItem>
                                <asp:ListItem>Meerut</asp:ListItem>
                                <asp:ListItem>Rajkot</asp:ListItem>
                                <asp:ListItem>Kalyan-Dombivali</asp:ListItem>
                                <asp:ListItem>Vasai-Virar</asp:ListItem>
                                <asp:ListItem>Varanasi</asp:ListItem>
                                <asp:ListItem>Srinagar</asp:ListItem>
                                <asp:ListItem>Aurangabad</asp:ListItem>
                                <asp:ListItem>Dhanbad</asp:ListItem>
                                <asp:ListItem>Amritsar</asp:ListItem>
                                <asp:ListItem>Navi Mumbai</asp:ListItem>
                                <asp:ListItem>Allahabad</asp:ListItem>
                                <asp:ListItem>Ranchi</asp:ListItem>
                                <asp:ListItem>Howrah</asp:ListItem>
                                <asp:ListItem>Coimbatore</asp:ListItem>
                                <asp:ListItem>Jabalpur</asp:ListItem>
                                <asp:ListItem>Gwalior</asp:ListItem>
                                <asp:ListItem>Vijayawada</asp:ListItem>
                                <asp:ListItem>Jodhpur</asp:ListItem>
                                <asp:ListItem>Madurai</asp:ListItem>
                                <asp:ListItem>Raipur</asp:ListItem>
                                <asp:ListItem>Kota</asp:ListItem>
                                <asp:ListItem>Guwahati</asp:ListItem>
                                <asp:ListItem>Chandigarh</asp:ListItem>
                                <asp:ListItem>Solapur</asp:ListItem>
                                <asp:ListItem>Hubballi-Dharwad</asp:ListItem>
                                <asp:ListItem>Bareilly</asp:ListItem>
                                <asp:ListItem>Moradabad</asp:ListItem>
                                <asp:ListItem>Mysore</asp:ListItem>
                                <asp:ListItem>Gurgaon</asp:ListItem>
                                <asp:ListItem>Aligarh</asp:ListItem>
                                <asp:ListItem>Jalandhar</asp:ListItem>
                                <asp:ListItem>Tiruchirappalli</asp:ListItem>
                                <asp:ListItem>Bhubaneswar</asp:ListItem>
                                <asp:ListItem>Salem</asp:ListItem>
                                <asp:ListItem>Mira-Bhayandar</asp:ListItem>
                                <asp:ListItem>Thiruvananthapuram</asp:ListItem>
                                <asp:ListItem>Bhiwandi</asp:ListItem>
                                <asp:ListItem>Saharanpur</asp:ListItem>
                                <asp:ListItem>Gorakhpur</asp:ListItem>
                                <asp:ListItem>Guntur</asp:ListItem>
                                <asp:ListItem>Bikaner</asp:ListItem>
                                <asp:ListItem>Amravati</asp:ListItem>
                                <asp:ListItem>Noida</asp:ListItem>
                                <asp:ListItem>Jamshedpur</asp:ListItem>
                                <asp:ListItem>Bhilai</asp:ListItem>
                                <asp:ListItem>Warangal</asp:ListItem>
                                <asp:ListItem>Cuttack</asp:ListItem>
                                <asp:ListItem>Firozabad</asp:ListItem>
                                <asp:ListItem>Kochi</asp:ListItem>
                                <asp:ListItem>Bhavnagar</asp:ListItem>
                                <asp:ListItem>Dehradun</asp:ListItem>
                                <asp:ListItem>Durgapur</asp:ListItem>
                                <asp:ListItem>Asansol</asp:ListItem>
                                <asp:ListItem>Nanded</asp:ListItem>
                                <asp:ListItem>Kolhapur</asp:ListItem>
                                <asp:ListItem>Ajmer</asp:ListItem>
                                <asp:ListItem>Gulbarga</asp:ListItem>
                                <asp:ListItem>Jamnagar</asp:ListItem>
                                <asp:ListItem>Ujjain</asp:ListItem>
                                <asp:ListItem>Loni</asp:ListItem>
                                <asp:ListItem>Siliguri</asp:ListItem>
                                <asp:ListItem>Jhansi</asp:ListItem>
                                <asp:ListItem>Ulhasnagar</asp:ListItem>
                                <asp:ListItem>Nellore</asp:ListItem>
                                <asp:ListItem>Jammu</asp:ListItem>
                                <asp:ListItem>Sangli-Miraj & Kupwad</asp:ListItem>
                                <asp:ListItem>Belgaum</asp:ListItem>
                                <asp:ListItem>Mangalore</asp:ListItem>
                                <asp:ListItem>Ambattur</asp:ListItem>
                                <asp:ListItem>Tirunelveli</asp:ListItem>
                                <asp:ListItem>Malegaon</asp:ListItem>
                                <asp:ListItem>Gaya</asp:ListItem>
                                <asp:ListItem>Jalgaon</asp:ListItem>
                                <asp:ListItem>Maheshtala</asp:ListItem>
                                <asp:ListItem>Tirupur</asp:ListItem>
                                <asp:ListItem>Davanagere</asp:ListItem>
                                <asp:ListItem>Kozhikode</asp:ListItem>
                                <asp:ListItem>Akola</asp:ListItem>
                                <asp:ListItem>Kurnool</asp:ListItem>
                                <asp:ListItem>Rajpur Sonarpur</asp:ListItem>
                                <asp:ListItem>Bokaro</asp:ListItem>
                                <asp:ListItem>South Dumdum</asp:ListItem>
                                <asp:ListItem>Bellary</asp:ListItem>
                                <asp:ListItem>Patiala</asp:ListItem>
                                <asp:ListItem>Gopalpur</asp:ListItem>
                                <asp:ListItem>Agartala</asp:ListItem>
                                <asp:ListItem>Bhagalpur</asp:ListItem>
                                <asp:ListItem>Muzaffarnagar</asp:ListItem>
                                <asp:ListItem>Bhatpara</asp:ListItem>
                                <asp:ListItem>Panihati</asp:ListItem>
                                <asp:ListItem>Latur</asp:ListItem>
                                <asp:ListItem>Dhule</asp:ListItem>
                                <asp:ListItem>Rohtak</asp:ListItem>
                                <asp:ListItem>Korba</asp:ListItem>
                                <asp:ListItem>Bhilwara</asp:ListItem>
                                <asp:ListItem>Brahmapur</asp:ListItem>
                                <asp:ListItem>Muzaffarpur</asp:ListItem>
                                <asp:ListItem>Ahmednagar</asp:ListItem>
                                <asp:ListItem>Mathura</asp:ListItem>
                                <asp:ListItem>Kollam</asp:ListItem>
                                <asp:ListItem>Avadi</asp:ListItem>
                                <asp:ListItem>Rajahmundry</asp:ListItem>
                                <asp:ListItem>Kadapa</asp:ListItem>
                                <asp:ListItem>Kamarhati</asp:ListItem>
                                <asp:ListItem>Bilaspur</asp:ListItem>
                                <asp:ListItem>Shahjahanpur</asp:ListItem>
                                <asp:ListItem>Bijapur</asp:ListItem>
                                <asp:ListItem>Rampur</asp:ListItem>
                                <asp:ListItem>Shivamogga(Shimoga)</asp:ListItem>
                                <asp:ListItem>Chandrapur</asp:ListItem>
                                <asp:ListItem>Junagadh</asp:ListItem>
                                <asp:ListItem>Thrissur</asp:ListItem>
                                <asp:ListItem>Alwar</asp:ListItem>
                                <asp:ListItem>Bardhaman</asp:ListItem>
                                <asp:ListItem>Kulti</asp:ListItem>
                                <asp:ListItem>Kakinada</asp:ListItem>
                                <asp:ListItem>Nizamabad</asp:ListItem>
                                <asp:ListItem>Parbhani</asp:ListItem>
                                <asp:ListItem>Tumkur</asp:ListItem>
                                <asp:ListItem>Hisar</asp:ListItem>
                                <asp:ListItem>Ozhukarai</asp:ListItem>
                                <asp:ListItem>Bihar Sharif</asp:ListItem>
                                <asp:ListItem>Panipat</asp:ListItem>
                                <asp:ListItem>Darbhanga</asp:ListItem>
                                <asp:ListItem>Bally</asp:ListItem>
                                <asp:ListItem>Aizawl</asp:ListItem>
                                <asp:ListItem>Dewas</asp:ListItem>
                                <asp:ListItem>Ichalkaranji</asp:ListItem>
                                <asp:ListItem>Tirupati</asp:ListItem>
                                <asp:ListItem>Karnal</asp:ListItem>
                                <asp:ListItem>Bathinda</asp:ListItem>
                                <asp:ListItem>Jalna</asp:ListItem>
                                <asp:ListItem>Barasat</asp:ListItem>
                                <asp:ListItem>Kirari Suleman Nagar</asp:ListItem>
                                <asp:ListItem>Purnia</asp:ListItem>
                                <asp:ListItem>Satna</asp:ListItem>
                                <asp:ListItem>Mau</asp:ListItem>
                                <asp:ListItem>Sonipat</asp:ListItem>
                                <asp:ListItem>Farrukhabad</asp:ListItem>
                                <asp:ListItem>Sagar</asp:ListItem>
                                <asp:ListItem>Rourkela</asp:ListItem>
                                <asp:ListItem>Durg</asp:ListItem>
                                <asp:ListItem>Imphal</asp:ListItem>
                                <asp:ListItem>Ratlam</asp:ListItem>
                                <asp:ListItem>Hapur</asp:ListItem>
                                <asp:ListItem>Anantapur</asp:ListItem>
                                <asp:ListItem>Arrah</asp:ListItem>
                                <asp:ListItem>Karimnagar</asp:ListItem>
                                <asp:ListItem>Etawah</asp:ListItem>
                                <asp:ListItem>Ambernath</asp:ListItem>
                                <asp:ListItem>North Dumdum</asp:ListItem>
                                <asp:ListItem>Bharatpur</asp:ListItem>
                                <asp:ListItem>Begusarai</asp:ListItem>
                                <asp:ListItem>New Delhi</asp:ListItem>
                                <asp:ListItem>Gandhidham</asp:ListItem>
                                <asp:ListItem>Baranagar</asp:ListItem>
                                <asp:ListItem>Tiruvottiyur</asp:ListItem>
                                <asp:ListItem>Puducherry</asp:ListItem>
                                <asp:ListItem>Sikar</asp:ListItem>
                                <asp:ListItem>Thoothukudi</asp:ListItem>
                                <asp:ListItem>Mirzapur</asp:ListItem>
                                <asp:ListItem>Raichur</asp:ListItem>
                                <asp:ListItem>Ramagundam</asp:ListItem>
                                <asp:ListItem>Vizianagaram</asp:ListItem>
                                <asp:ListItem>Katihar</asp:ListItem>
                                <asp:ListItem>Haridwar</asp:ListItem>
                                <asp:ListItem>Sri Ganganagar</asp:ListItem>
                                <asp:ListItem>Karawal Nagar</asp:ListItem>
                                <asp:ListItem>Nagercoil</asp:ListItem>
                                <asp:ListItem>Mango</asp:ListItem>
                                <asp:ListItem>Bulandshahr</asp:ListItem>
                                <asp:ListItem>Thanjavur</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div id="divcity2" style="display: none;">
                            <asp:TextBox ID="txtCity" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form_field fl_wrap">
                        <label class="fl_label uppercase">Pincode</label>
                        <asp:TextBox ID="txtPincode" runat="server" class="fl_input" MaxLength="10" onKeyPress='javascript:return onlyNumbers();' autocomplete="off"></asp:TextBox>
                    </div>


                    <div class="clear" style="height: 10px;"></div>

                    <div class="form_centering_div">
                        <asp:Button ID="btnFixAnAppointment" runat="server" class="submit_btn fade_anim uppercase" Text="Fix the Appointment" OnClick="btnFixAnAppointment_Click" OnClientClick='javascript:return CheckSave();' />
                    </div>

                </div>
            </asp:Panel>


            <asp:Literal ID="ltrpopUp" runat="server"></asp:Literal>
            <div class="clear"></div>

            <asp:Panel ID="pnlThank" runat="server" Visible="false">
                <%--<div style="text-align: center; font-size: 14px; line-height: 25px; font-weight: bold; color: #04869a;">
                    Thanks for booking an appointment with us.<br>
                    We will get back to you very soon.
                </div>--%>
                <div style="text-align: center; font-size: 14px; line-height: 25px; font-weight: bold; color: #04869a;">
                    Thank you for your request.<br />
                    Please do not consider this as your confirmed appointment.<br />
                    We shall get back to you very soon.<br />                   
                </div>
            </asp:Panel>

        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="/js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="/js/FlowupLabels_plugin.js"></script>

      <script>
        function DisableFields()
        {
            $('#<%=ddlVisitTime.ClientID %>').prop('disabled', true); 
            $('#<%=ddlAvailableTime.ClientID %>').prop('disabled', true); 
            $('#<%=txtDateOfAppointment.ClientID %>').prop('disabled', true); 
            $('#<%=txtPatientName.ClientID %>').prop('disabled', true); 
            $('#<%=txtEmailId.ClientID %>').prop('disabled', true); 
            $('#<%=txtMobileNo.ClientID %>').prop('disabled', true); 
            $('#<%=txtAddress.ClientID %>').prop('disabled', true); 
            $('#<%=ddlCountry.ClientID %>').prop('disabled', true); 
            $('#<%=ddState.ClientID %>').prop('disabled', true); 
            $('#<%=ddlCity.ClientID %>').prop('disabled', true); 
            $('#<%=txtPincode.ClientID %>').prop('disabled', true); 
            $('#<%=btnFixAnAppointment.ClientID %>').prop('disabled', true); 
            

            $(document).ready(function () {
                $("#consolPopup").fancybox({
                    'overlayShow': true
                }).trigger('click');
            });
        }

    </script>

</asp:Content>

