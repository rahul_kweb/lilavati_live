﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class video_consultation : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
            BindBanner();
        }
    }

    public void Content()
    {
        cms.ID = 23;
        cmsbal.GETBYIDContent(cms);
        ltrContent.Text = cms.Description;
    }
    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrBanner.Text = bal.GetBannerForBinding(67);
    }
}