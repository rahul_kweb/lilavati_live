﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="Feedback_InPatient.aspx.cs" Inherits="Feedback_InPatient" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<%@ Register TagPrefix="site" TagName="InPatient" Src="Control/feedback_InPatient.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
    <script type="text/javascript" src="js/jquery-ui.js"></script>


    <script type="text/javascript" src="js/jquery.ezmark.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.fdbk_right input[type="radio"]').ezMark();
            $('.ez_checkbox input[type="checkbox"]').ezMark({ checkboxCls: 'ez-checkbox', checkedCls: 'ez-checked-select' });
        });
    </script>

    <style type="text/css">
        .text_format table td {
            vertical-align: top;
            padding: 0px 5px 0px 0px;
            border: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">InPatient Feedback</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            
            <div class="content_main_box text_format">
                    <div class="tabbertab">
<%--                        <h2>InPatient</h2>--%>

                        <site:InPatient ID="InPatient" runat="server" />

                        <div class="clear"></div>
                    </div>
                </div>
               <div class="box_aside">


                <menu:Patients ID="menu" runat="server" />

            </div>
            </div>
           </div>
        
    <script type="text/javascript" src="js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="js/FlowupLabels_plugin.js"></script>
</asp:Content>

