﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="donate_blood.aspx.cs" Inherits="donate_blood" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Donate Blood</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="print_share">
                <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                <div class="addthis_native_toolbox"></div>
                <div class="clear"></div>
            </div>

            <asp:Literal ID="litContent" runat="server"></asp:Literal>

            <%--<div class="blood_tagline_box">
                The drop that counts the precious gift of life join the life savers together we can save lives. Be a blood donor.
            </div>

            <p><strong class="green_text uppercase">FAQ’s about blood donation</strong></p>

            <p>
                <strong>Who can be a blood donor?</strong><br>
                You can donate if:
            </p>

            <ul class="bullet">
                <li>You are between age group of 18-60 years</li>
                <li>Your weight is more than 55 kg</li>
                <li>Your Haemoglobin is more than or equal to 12.5gm%</li>
                <li>Your last donation was more than 3 months ago</li>
            </ul>


            <p>
                <strong>How long will the blood donation take?</strong><br>
                On an average, the donation takes only about 10 minutes and the entire process from registration to resting time last about 30 minutes.
            </p>

            <p><strong>What is the procedure for Blood Donation?</strong></p>

            <ul class="bullet">
                <li><strong>Registration - Room No. 1</strong><br>
                    &bull; Registration form must be filled and a local address and contact no. must be provided<br>
                    &bull; A health assessment questionnaire will be given to answer and personal particulars recorded
                </li>
                <li><strong>Medical Screening - Room No. 2</strong><br>
                    Your questionnaire will be checked in detail.You will need to sign a declaration to confirm that information given is true .A physical examination,including checking of blood haemoglobin leavel, weight, blood pressure will be carried out.</li>
                <li><strong>Blood Donation - Room No. 3</strong><br>
                    A trained medical person will draw the blood.All blood collection equipments,including needles are use only once and disposed off after each case</li>
                <li><strong>Rest and Refreshment - Room No. 4</strong><br>
                    Refreshment will be served after the donation.You should rest for atleast 10 minutes before leaving</li>
            </ul>

            <p>
                <strong>What happens to Blood You have donated?</strong><br>
                Every unit of blood goes through a series of stringent tests, for infectious diseases such as HIV/ AIDS, Hepatitis , Syphilis and Malaria.
            </p>
            <p>The Blood Bank makes components like: Packaged RBC’s, Fresh Frozen Plasma, Platelet Concentrate, Cryo Precipitate, Single Donor Platelet; And issues them as per the need of the patient on the request of the treating Doctor.</p>


            <p><strong class="green_text">PRE DONATION INFORMATION</strong></p>

            <ul class="bullet">
                <li>There is no substitute for blood</li>
                <li>Donating blood does not take a long time. The actual blood collection procedure takes about 8-10 minutes</li>
                <li>An individual can safety donate blood from age of 18 to 65 years, ( Weight more than 45 kg )</li>
                <li>Do not donate blood empty stomach</li>
                <li>Trauma victims, cancer patients and those with inherited blood disorders require most of the donated blood</li>
                <li>Whole blood donation can be made safety at an interval of 3 months. Repeated blood donation at this interval does not cause any sort of weakness</li>
                <li>People who are anemic cannot donate blood. However, they should undergo treatment for anemia and can donate blood once the hemoglobin is within the normal range ( More than 12.5gm%)</li>
                <li>The donor is examined before donation for his/her suitability to give blood. And many tests will be carried out on the donated blood to ensure blood safety
There is absolutely no risk to acquiring AIDs or any other disease ( such as Hepatitis B or C ) from donation blood.</li>
                <li>Individuals who have suffered or suffer from Hepatitis B, Hepatitis C or AIDs should not donate blood.</li>
                <li>As a blood donor, it is your moral responsibility to ensure that the blood you donate is safe and not likely to transmit any infection you may be carrying. To ensure good donor selection,you will have to answer few questions in confidence about your lifestyle and your sexual history.The purose of asking these questions is to select a healthy and safe donor for needy and sick patients and to collect blood which is safe and unlikely to transmit any infection.You must answer these questions carefully,truthfully and as correctly as possibly,because we know that you want to help rather than harm a patient in real need.This will help to protect you and the patient who receives your blood.</li>
                <li>Please enroll yourself as a voluntary donor. You can regularly donate blood in a blood bank or blood donations camp</li>
            </ul>

            <p><strong class="green_text">POST DONATION CARE</strong></p>

            <ul class="bullet">
                <li>Drink plenty of Fluids (Water/ Juice)</li>
                <li>No extra diet is reuired</li>
                <li>Take rest for 10 - 15min</li>
                <li>Do not Smoke or chew Tobacco/ Ghutaka for 3 hrs</li>
                <li>Avoid alcool for 6 hrs</li>
                <li>Do not lift heavy objects with the arm used for donation for few hours</li>
                <li>Avoid strenuous exercse for 24hrs</li>
                <li>If bleed reoccurs, press firmly on the area for 5-10 min & raise arm</li>
                <li>If Dizziness/ Fainting occurs,lie down & raise legs</li>
                <li>Avoid driving for 30 min</li>
                <li>After donation you can do your routine work</li>
                <li>You can donate blood again after 3 months</li>
            </ul>--%>

            <div class="tabber">

                <asp:Repeater ID="rptContent" runat="server">
                    <ItemTemplate>
                        <div class="tabbertab">
                            <h2><%#Eval("TAB") %>  </h2>

                            <div class="tab_content">
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

