﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="statutory_compilance.aspx.cs" Inherits="statutory_compilance" %>

<%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>

    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Statutory Compliance</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <li><a href="#" rel='tab_1'>Bio-Medical Waste</a></li>
                        <li><a href="#" rel='tab_2'>Stent Price List</a></li>
                        <li><a href="#" rel='tab_3'>Notifications - COVID 19</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <div id='tab_1'>
                        <asp:Literal ID="litContent" runat="server"></asp:Literal>

                    </div>
                    <div id='tab_2'>
                        <table class="visa_certf">
                            <thead>
                                <tr>
                                    <td>NAME OF THE STENT</td>
                                    <td>COMPANY NAME</td>
                                    <td>DISTRIBUTOR NAME</td>
                                    <td>M.R.P(Inclusive of all Taxes)</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="Certificate">PROMUS ELEMENT PLUS</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 23625 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">PROMUS ELEMENT</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 23625 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">PROMUS PREMIER</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">SYNERGY</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">YUKON CHOICE</td>
                                    <td data-title="Age">TRANSLUMINIA</td>
                                    <td data-title="Exam">RIDDHI SIDDHI HEALTHCARE</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">DRUG ELUTING STENT  ENDEAVOR - RESOLUTE INTEGRITY</td>
                                    <td data-title="Age">MEDTRONIC</td>
                                    <td data-title="Exam">INDIA MEDTRONIC</td>
                                    <td data-title="Charges">Rs. 23625 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">DRUG ELUTING STENT -  RESOLUTE ONYX</td>
                                    <td data-title="Age">MEDTRONIC</td>
                                    <td data-title="Exam">INDIA MEDTRONIC</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">CORONARY STENT-ULTIMASTER</td>
                                    <td data-title="Age">TERUMO</td>
                                    <td data-title="Exam">BIOMEDICON CARE PVT. LTD.</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BIOMATRIX FLEX-BA9 ELUTING DES</td>
                                    <td data-title="Age">BIOSENSOR</td>
                                    <td data-title="Exam">T A TECHNOLOGIES & HEALTHCARE</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BIOFREEDOM</td>
                                    <td data-title="Age">BIOSENSOR</td>
                                    <td data-title="Exam">T A TECHNOLOGIES & HEALTHCARE</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">ORSIRO (DES) STENT</td>
                                    <td data-title="Age">BIOTRONIK</td>
                                    <td data-title="Exam">SHRIGUN PHARMACHEM PVT LTD.</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BIOMIME / (BIOMIME MORPH)</td>
                                    <td data-title="Age">MERIL LIFE SCIENCE</td>
                                    <td data-title="Exam">AADVENT</td>
                                    <td data-title="Charges">Rs. 31,584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">XIENCE PRIME</td>
                                    <td data-title="Age">ABBOTT</td>
                                    <td data-title="Exam">SINO CARE SURGICALS PVT. LTD</td>
                                    <td data-title="Charges">Rs.24999 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">XIENCE XPEDITION</td>
                                    <td data-title="Age">ABBOTT</td>
                                    <td data-title="Exam">SINO CARE SURGICALS PVT. LTD</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BMS(includes Vision / MiniVision ,Zeta,Jostent Graftmaster & ML8)</td>
                                    <td data-title="Age">ABBOTT</td>
                                    <td data-title="Exam">SINO CARE SURGICALS PVT. LTD</td>
                                    <td data-title="Charges">Rs. 8674.05</td>
                                </tr>

                            </tbody>
                        </table>

                        <div class="clear"></div>
                    </div>
                    <div id='tab_3'>

                        <ul>
                          <%--  <li><a href="images/covid19/Annexcure C.jpg" target="_blank">Annexure C</a></li>
                            <li><a href="images/covid19/Exclusions_for_Annexure_C.jpg" target="_blank">Exclusions for Annexure C</a></li>--%>
                                                <li><a href="images/covid19/Notification 01.06.2021.pdf" target="_blank">Notification related to rates for COVID patients</a></li>
                            <li><a href="images/covid19/COVID Bed strength.jpg" target="_blank">Bed Strength</a></li>
                            <li><a href="images/covid19/Govt.Regulated%20Concesstional%20Beds.jpg" target="_blank">Govt.Regulated Concesstional Beds</a></li>
                             <li><a href="images/covid19/Fixation of price per dose fo COVID 19 vaccination.pdf" target="_blank">Notification related to COVID-19 Vaccination</a></li>
                             <li><a href="images/covid19/Notification for Mucormycosis patients.pdf" target="_blank">Notification for Mucormycosis patients</a></li>
                        </ul>

                    </div>

                    <div class="clear"></div>
                </div>



                <%-- <h1 class="hd green_text uppercase">Biomedical Department</h1>

                <ul class="bullet">
                    <li>Yearly Quality Assurance testing for all Xray based systems installed at the hospital</li>
                    <li>AERB (Atomic Energy Regulatory Board) Licensing and registration of all xray systems installed at the hospital</li>
                    <li>Radiation workers (technicians and professionals) – TLD badge, Quaterly  monitoring.</li>
                    <li>Registration of workers in xray departments (technicians and professionals)</li>
                    <li>Yearly - Lead Apron Quality Assurance  testing</li>
                    <li>Calibration validations  of medical equipment’s installed in the Blood Bank and Pathology Lab.</li>
                </ul>--%>

                <%--<div class="accd_box">
                    <div class="hd c_expand_hd">
                        Stent Price List
                    </div>
                    <div class="newsbox accd_content c_expand_box">
                        <table class="visa_certf">
                            <thead>
                                <tr>
                                    <td>NAME OF THE STENT</td>
                                    <td>COMPANY NAME</td>
                                    <td>DISTRIBUTOR NAME</td>
                                    <td>M.R.P(Inclusive of all Taxes)</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="Certificate">PROMUS ELEMENT PLUS</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 23625 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">PROMUS ELEMENT</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 23625 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">PROMUS PREMIER</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">SYNERGY</td>
                                    <td data-title="Age">BOSTON SCIENTIFIC</td>
                                    <td data-title="Exam">RUSHABH BIOMED</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">YUKON CHOICE</td>
                                    <td data-title="Age">TRANSLUMINIA</td>
                                    <td data-title="Exam">RIDDHI SIDDHI HEALTHCARE</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">DRUG ELUTING STENT  ENDEAVOR - RESOLUTE INTEGRITY</td>
                                    <td data-title="Age">MEDTRONIC</td>
                                    <td data-title="Exam">INDIA MEDTRONIC</td>
                                    <td data-title="Charges">Rs. 23625 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">DRUG ELUTING STENT -  RESOLUTE ONYX</td>
                                    <td data-title="Age">MEDTRONIC</td>
                                    <td data-title="Exam">INDIA MEDTRONIC</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">CORONARY STENT-ULTIMASTER</td>
                                    <td data-title="Age">TERUMO</td>
                                    <td data-title="Exam">BIOMEDICON CARE PVT. LTD.</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BIOMATRIX FLEX-BA9 ELUTING DES</td>
                                    <td data-title="Age">BIOSENSOR</td>
                                    <td data-title="Exam">T A TECHNOLOGIES & HEALTHCARE</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BIOFREEDOM</td>
                                    <td data-title="Age">BIOSENSOR</td>
                                    <td data-title="Exam">T A TECHNOLOGIES & HEALTHCARE</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">ORSIRO (DES) STENT</td>
                                    <td data-title="Age">BIOTRONIK</td>
                                    <td data-title="Exam">SHRIGUN PHARMACHEM PVT LTD.</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BIOMIME / (BIOMIME MORPH)</td>
                                    <td data-title="Age">MERIL LIFE SCIENCE</td>
                                    <td data-title="Exam">AADVENT</td>
                                    <td data-title="Charges">Rs. 31,584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">XIENCE PRIME</td>
                                    <td data-title="Age">ABBOTT</td>
                                    <td data-title="Exam">SINO CARE SURGICALS PVT. LTD</td>
                                    <td data-title="Charges">Rs.24999 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">XIENCE XPEDITION</td>
                                    <td data-title="Age">ABBOTT</td>
                                    <td data-title="Exam">SINO CARE SURGICALS PVT. LTD</td>
                                    <td data-title="Charges">Rs. 31584 /-</td>
                                </tr>

                                <tr>
                                    <td data-title="Certificate">BMS(includes Vision / MiniVision ,Zeta,Jostent Graftmaster & ML8)</td>
                                    <td data-title="Age">ABBOTT</td>
                                    <td data-title="Exam">SINO CARE SURGICALS PVT. LTD</td>
                                    <td data-title="Charges">Rs. 8674.05</td>
                                </tr>

                            </tbody>
                        </table>
                        <div class="clear">
                            &nbsp;
                        </div>
                    </div>
                    <div class="clear">
                        &nbsp;
                    </div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
</asp:Content>

