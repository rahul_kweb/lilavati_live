﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyMessageBox.ascx.cs" Inherits="MyMessageBox" %>

<div class="MessageContainer">
    <asp:Panel ID="MessageBox" runat="server">
        <asp:HyperLink runat="server" ID="CloseButton" class="MessageContainerClose" ToolTip="Close" >
            ×
        </asp:HyperLink>
      <p><asp:Literal ID="litMessage" runat="server"></asp:Literal></p>
    </asp:Panel>
</div>