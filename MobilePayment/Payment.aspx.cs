﻿using CCA.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AppPayment_Payment : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
       

        #region Live credential
        string workingKey = "6C308E27F7EE5C56BD2B579DA151F471";//put in the 32bit alpha numeric key in the quotes provided here
        #endregion

        CCACrypto ccaCrypto = new CCACrypto();
        string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);

        //NameValueCollection Params = new NameValueCollection();
        string[] segments = encResponse.Split('&');


        #region Get Status (Getting problem when using if else condition overhere )


        string checkstatus = segments[3];

        string[] parts = checkstatus.Split('=');

        string getmsg = "";

        foreach (string parts1 in parts)
        {

            getmsg = parts1.ToString();
        }


        #endregion

        #region Store all array Record in session

        string orderid = segments[0];
        string[] orderids = orderid.Split('=');
        foreach (string orderid1 in orderids)
        {
            Session["app_order_id"] = orderid1;
        }

        string trackingid = segments[1];
        string[] trackingids = trackingid.Split('=');
        foreach (string trackingid1 in trackingids)
        {
            Session["app_tracking_id"] = trackingid1;
        }

        string bankrefno = segments[2];
        string[] bankrefnos = bankrefno.Split('=');
        foreach (string bankrefno1 in bankrefnos)
        {
            Session["app_bank_ref_no"] = bankrefno1;
        }

        string orderstatus = segments[3];
        string[] orderstatuss = orderstatus.Split('=');
        foreach (string orderstatus1 in orderstatuss)
        {
            Session["app_order_status"] = orderstatus1;
        }

        string paymentmode = segments[5];
        string[] paymentmodes = paymentmode.Split('=');
        foreach (string paymentmode1 in paymentmodes)
        {
            Session["app_payment_mode"] = paymentmode1;
        }

        string cardname = segments[6];
        string[] cardnames = cardname.Split('=');
        foreach (string cardname1 in cardnames)
        {
            Session["app_card_name"] = cardname1;
        }

        string amount = segments[10];
        string[] amounts = amount.Split('=');
        foreach (string amount1 in amounts)
        {
            Session["app_amount"] = amount1;
        }

        string currency = segments[9];
        string[] currencys = currency.Split('=');
        foreach (string currency1 in currencys)
        {
            Session["app_currency"] = currency1;
        }

        #endregion
       
        GetStatus(getmsg);
    }

    public void GetStatus(string msg)
    {
        if (msg == "Success")
        {
            //string varparameter = Session["app_order_id"].ToString() + " " + Session["app_tracking_id"].ToString() + " " + Session["app_bank_ref_no"].ToString() + " " + Session["app_order_status"].ToString() + " " + Session["app_card_name"].ToString() + " " + Session["app_amount"].ToString() + " " + Session["app_currency"].ToString() + " " + Session["app_payment_mode"].ToString();
            //Response.Write("alert('" + varparameter + "')");

            using (SqlCommand cmd = new SqlCommand("Proc_PaymentTable"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "APP_UPDATE_ORDER_ID");
                cmd.Parameters.AddWithValue("@TRACKING_ID", Session["app_tracking_id"].ToString());
                cmd.Parameters.AddWithValue("@BANK_REF_NO", Session["app_bank_ref_no"].ToString());
                cmd.Parameters.AddWithValue("@ORDER_STATUS", Session["app_order_status"].ToString());
                cmd.Parameters.AddWithValue("@AMOUNT", Session["app_amount"].ToString());
                cmd.Parameters.AddWithValue("@CURRENCY", Session["app_currency"].ToString());
                cmd.Parameters.AddWithValue("@PAYMENT_MODE", Session["app_payment_mode"].ToString());
                cmd.Parameters.AddWithValue("@CARD_NAME", Session["app_card_name"].ToString());
                cmd.Parameters.AddWithValue("@ORDER_ID", Session["app_order_id"].ToString());
                if (utility.Execute(cmd))
                {
                    Response.Redirect("Success.aspx");

                    //Response.Write("alert('sucess')");
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    //List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    //Dictionary<string, object> row = new Dictionary<string, object>();

                    //row.Add("Message", "Payment is done successfully.");
                    //row.Add("Status", true);
                    //rows.Add(row);

                    //this.Context.Response.ContentType = "application/json; charset=utf-8";
                    //this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
                }
            }    
        }
        else
        {
                Response.Redirect("Fail.aspx");

             //Response.Write("alert('failed')");
             //Response.Write("Your Payment Could Not Done SuccessFully!!!!");
             //JavaScriptSerializer serializer = new JavaScriptSerializer();
             //List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
             //Dictionary<string, object> row = new Dictionary<string, object>();

             //row.Add("Message", "failed.");
             //row.Add("Status", false);
             //rows.Add(row);

             //this.Context.Response.ContentType = "application/json; charset=utf-8";
             //this.Context.Response.Write(new JavaScriptSerializer().Serialize(rows));
        }      
    }

}