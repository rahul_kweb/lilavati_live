﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AppPayment_PaymentProcessing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Utility utility = new Utility();
        if (Request.QueryString["OrderId"] != null && Request.QueryString["AddressIndex"]!=null)
        {
            string OrderId = Request.QueryString["OrderId"].ToString();
            string AddressIndex = Request.QueryString["AddressIndex"].ToString();
            //Response.Write("alert('" + OrderId + "')");
            DataTable dtnewuserdetail = new DataTable();
            dtnewuserdetail = utility.Display("Proc_PCipopRegistrationMaster 'GET_BY_ID','" + AddressIndex + "'");
            if (dtnewuserdetail.Rows.Count > 0)
            {
                string patientname = dtnewuserdetail.Rows[0]["FIRSTNAME"].ToString() + " " + dtnewuserdetail.Rows[0]["LASTNAME"].ToString();
                Response.Redirect("dataFrom.htm?billing_name=" + patientname + "&&billing_address=" + dtnewuserdetail.Rows[0]["ADDRESS"].ToString() + "&&billing_city=" + dtnewuserdetail.Rows[0]["CITY"].ToString() + "&&billing_state=" + dtnewuserdetail.Rows[0]["STATE"].ToString() + "&&billing_zip=" + dtnewuserdetail.Rows[0]["PINCODE"].ToString() + "&&billing_country=" + dtnewuserdetail.Rows[0]["COUNTRY"].ToString() + "&&billing_tel=" + dtnewuserdetail.Rows[0]["MOBILENO"].ToString() + "&&billing_email=" + dtnewuserdetail.Rows[0]["EMAIL"].ToString() + "&&order_id=" + OrderId + "&&addressindex=" + AddressIndex);
            }


        }
    }
}