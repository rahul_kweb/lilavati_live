﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using CCA.Util;
using System.Data.SqlClient;
using System.Data;

public partial class ResponseHandler : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        //string workingKey = "6C308E27F7EE5C56BD2B579DA151F471";//put in the 32bit alpha numeric key in the quotes provided here
        //CCACrypto ccaCrypto = new CCACrypto();
        //string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"],workingKey);

        ////NameValueCollection Params = new NameValueCollection();
        //string[] segments = encResponse.Split('&');


        //#region Get Status (Getting problem when using if else condition overhere )


        //string checkstatus = segments[3];

        //string[] parts = checkstatus.Split('=');

        //string getmsg = "";

        //foreach (string parts1 in parts)
        //{

        //    getmsg = parts1.ToString();
        //}


        //#endregion

        //#region Store all array Record in session

        //string orderid = segments[0];
        //string[] orderids = orderid.Split('=');
        //foreach (string orderid1 in orderids)
        //{
        //    Session["order_id"] = orderid1;
        //}

        //string trackingid = segments[1];
        //string[] trackingids = trackingid.Split('=');
        //foreach (string trackingid1 in trackingids)
        //{
        //    Session["tracking_id"] = trackingid1;
        //}

        //string bankrefno = segments[2];
        //string[] bankrefnos = bankrefno.Split('=');
        //foreach (string bankrefno1 in bankrefnos)
        //{
        //    Session["bank_ref_no"] = bankrefno1;
        //}

        //string orderstatus = segments[3];
        //string[] orderstatuss = orderstatus.Split('=');
        //foreach (string orderstatus1 in orderstatuss)
        //{
        //    Session["order_status"] = orderstatus1;
        //}

        //string paymentmode = segments[5];
        //string[] paymentmodes = paymentmode.Split('=');
        //foreach (string paymentmode1 in paymentmodes)
        //{
        //    Session["payment_mode"] = paymentmode1;
        //}

        //string cardname = segments[6];
        //string[] cardnames = cardname.Split('=');
        //foreach (string cardname1 in cardnames)
        //{
        //    Session["card_name"] = cardname1;
        //}

        //string amount = segments[10];
        //string[] amounts = amount.Split('=');
        //foreach (string amount1 in amounts)
        //{
        //    Session["amount"] = amount1;
        //}

        //string currency = segments[9];
        //string[] currencys = currency.Split('=');
        //foreach (string currency1 in currencys)
        //{
        //    Session["currency"] = currency1;
        //}

        //#endregion

        //GetStatus(getmsg);


        //#region commented Code

        ////foreach (string seg in segments)
        ////{

        ////    string[] parts = seg.Split('=');
        ////    if (parts.Length > 0)
        ////    {
        ////        string Key = parts[0].Trim();
        ////        string Value = parts[1].Trim();
        ////        Params.Add(Key, Value);
        ////    }
        ////}

        ////for (int i = 0; i < Params.Count; i++)
        ////{
        ////    string test = Params[i];

        ////    Response.Write(Params.Keys[i] + " = " + Params[i] + "<br>");
        ////}

        //#endregion

    }

    public string InsertIntoPaymentTable(string patientid, string DrName, string orderid, string trackingid, string bankrefno, string order_status, string paymentmode, string card_name, string amount, string currency)
    {
        string response = "";

        using (SqlCommand cmd = new SqlCommand("Proc_PaymentTable"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "add");
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", patientid.ToString());
            cmd.Parameters.AddWithValue("@DOCTOR_NAME_VCR", DrName.ToString());
            cmd.Parameters.AddWithValue("@ORDER_ID", orderid.ToString());
            cmd.Parameters.AddWithValue("@TRACKING_ID", trackingid.ToString());
            cmd.Parameters.AddWithValue("@BANK_REF_NO", bankrefno.ToString());
            cmd.Parameters.AddWithValue("@ORDER_STATUS", order_status.ToString());
            cmd.Parameters.AddWithValue("@PAYMENT_MODE", paymentmode.ToString());
            cmd.Parameters.AddWithValue("@CARD_NAME", card_name.ToString());
            cmd.Parameters.AddWithValue("@AMOUNT", amount.ToString());
            cmd.Parameters.AddWithValue("@CURRENCY", currency.ToString());
            if (utility.Execute(cmd))
            {
                response = "success";
            }
            else
            {
                response = "fail";
            }
            return response;
        }
    }

    public string GetStatus(string msg)
    {
        if (msg == "Success")
        {
            Session["patientsId"] = Session["Doctor_Id_bint"].ToString(); //get record from get appointment page
            Session["DoctorName"] = Session["Doctors"].ToString(); //get record from get appointment page
            string patientid = Session["patientsId"].ToString();
            //string DrName = Session["DoctorName"].ToString();
            string DrName = Session["Doctors"].ToString();

            string orderid = Session["order_id"].ToString();
            string trackingid = Session["tracking_id"].ToString();
            string bankrefno = Session["bank_ref_no"].ToString();
            string order_status = Session["order_status"].ToString();
            string paymentmode = Session["payment_mode"].ToString();
            string card_name = Session["card_name"].ToString();
            string amount = Session["amount"].ToString();
            string currency = Session["currency"].ToString();

            MakeAppointment();
            string InsertResult = InsertIntoPaymentTable(patientid, DrName, orderid, trackingid, bankrefno, order_status, paymentmode, card_name, amount, currency);

            if (InsertResult == "success")
            {
                ResetAllSessionValue();
                Response.Write("Your Payment Done SuccessFully!!!!");
            }
            else
            {
                Response.Write("Your Payment Could Not Done SuccessFully!!!!");
            }
        }
        else
        {
            Response.Write("Your Payment Could Not Done SuccessFully!!!!");
        }
        return msg;
    }

    private void MakeAppointment()
    {
        try
        {
            #region Offline appointment code
            //using (SqlCommand cmd = new SqlCommand("AddUpdateGetAppointmentMaster"))
            //{
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.AddWithValue("@Para_vcr", "Add");
            //    cmd.Parameters.AddWithValue("@Speciality_Id_bint ", Session["Speciality_Id_bint"].ToString());
            //    cmd.Parameters.AddWithValue("@Doctor_Id_bint", Session["Doctor_Id_bint"].ToString());
            //    cmd.Parameters.AddWithValue("@App_Time_vcr", Session["App_Time_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@App_date_dtm", Session["App_date_dtm"].ToString());
            //    cmd.Parameters.AddWithValue("@Patient_Name_vcr", Session["Patient_Name_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@Address_vcr", Session["Address_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@Country_Id_bint", Session["Country_Id_bint"].ToString());
            //    cmd.Parameters.AddWithValue("@State_vcr", Session["State_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@City_vcr", Session["City_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@Postal_vcr", Session["Postal_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@Mobile_No_vcr", Session["Mobile_No_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@Email_Id_vcr", Session["Email_Id_vcr"].ToString());
            //    cmd.Parameters.AddWithValue("@Visit_vcr", Session["Visit_vcr"].ToString());



            //    if (utility.Execute(cmd))
            //    {

            //        OfflineAppMail offlinemail = new OfflineAppMail();

            //        string Speciality = Session["Speciality"].ToString();
            //        string Doctors = Session["Doctors"].ToString();
            //        string Avail = Session["Avail"].ToString();
            //        string Country = Session["Country"].ToString();
            //        string Visit = Session["Visit"].ToString();
            //        string State = Session["State"].ToString();
            //        string City = Session["City"].ToString();

            //        offlinemail.EmailOfflineApp(Speciality, Doctors, Avail, Session["App_date_dtm"].ToString(), Session["Patient_Name_vcr"].ToString(), Session["Email_Id_vcr"].ToString(), Session["Mobile_No_vcr"].ToString(), Session["Address_vcr"].ToString(), Country, State, City, Session["Postal_vcr"].ToString(), Visit);
            //    }
            //    else
            //    {
            //        //Response.Write("<script>alert('Failed');</script>");
            //    }
            //}
            #endregion

            #region Online Appointmnet
            AppointmentTable apptbl = new AppointmentTable();
            BAL bal = new BAL();
            apptbl.Speciality_Id_bint = int.Parse(Session["Speciality_Id_bint"].ToString());

            apptbl.Doctor_Id_bint = int.Parse(Session["Doctor_Id_bint"].ToString());
            apptbl.App_Avai_Time = Session["App_Time_vcr"].ToString();
            apptbl.App_Visit_Time = Session["App_Visit_Time"].ToString();

            string date = DateTime.ParseExact(Session["App_date_dtm"].ToString(), "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
            apptbl.App_Date = date;

            apptbl.AddressIndex = int.Parse(Session["patientsId"].ToString());
            apptbl._Specialityname = Session["Speciality"].ToString();
            apptbl._Doctor_Name_vcr = Session["Doctors"].ToString();
            apptbl._ConsDrId = Session["doctorconsdocid"].ToString();


            bal.AddAppointment(apptbl);

            if (Session["AppMsg"] == "Success")
            {
                DataTable dt = new DataTable();
                dt = utility.Display("SELECT * FROM GAADADDRESSMASTER WHERE ADDRESSINDEX = '" + int.Parse(HttpContext.Current.Session["patientsId"].ToString()) + "'");
                if (dt.Rows.Count > 0)
                {
                    string EMailTo = dt.Rows[0]["Email"].ToString();
                    BookAnAppointment appointment = new BookAnAppointment();
                    appointment.EmailAppointment(Session["Speciality"].ToString(), Session["Doctors"].ToString(), Session["App_Time_vcr"].ToString(), Session["App_date_dtm"].ToString(), Session["App_Visit_Time"].ToString(), EMailTo);
                }

                Session.Remove("appointmentstart");
            }
            else
            {
                //divMsg.Visible = true;

                //lblmsg.ForeColor = System.Drawing.Color.Red;
                //lblmsg.Text = "Appointment Could Not Fixed Successfully";
            }
            #endregion

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    public void ResetAllSessionValue()
    {
        Session.Remove("Doctors");
        Session.Remove("order_id");
        Session.Remove("tracking_id");
        Session.Remove("bank_ref_no");
        Session.Remove("order_status");
        Session.Remove("payment_mode");
        Session.Remove("card_name");
        Session.Remove("amount");
        Session.Remove("currency");

        Session.Remove("Speciality_Id_bint");
        Session.Remove("Doctor_Id_bint");
        Session.Remove("App_Time_vcr");
        Session.Remove("App_date_dtm");
        Session.Remove("Patient_Name_vcr");
        Session.Remove("Address_vcr");
        Session.Remove("Country_Id_bint");
        Session.Remove("State_vcr");
        Session.Remove("City_vcr");
        Session.Remove("Postal_vcr");
        Session.Remove("Mobile_No_vcr");
        Session.Remove("Email_Id_vcr");
        Session.Remove("Visit_vcr");

        Session.Remove("Speciality");
        Session.Remove("Avail");
        Session.Remove("Country");
        Session.Remove("Visit");
        Session.Remove("State");
        Session.Remove("City");

       Session.Remove("doctorconsdocid");
    }

}
