﻿﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CCA.Util;


public partial class SubmitData : System.Web.UI.Page
{
    CCACrypto ccaCrypto = new CCACrypto();

    #region Test Credential
    string workingKey = "6C308E27F7EE5C56BD2B579DA151F471";//put in the 32bit alpha numeric key in the quotes provided here 	
    string ccaRequest = "";
    public string strEncRequest = "";
    public string strAccessCode = "AVOW67DI16CE46WOEC";// put the access key in the quotes provided here.
    #endregion

    //#region OldLive Credential
    //string workingKey = "D6181BBAEA9A1185472E2B458102C3BD";//put in the 32bit alpha numeric key in the quotes provided here 	
    //string ccaRequest = "";
    //public string strEncRequest = "";
    //public string strAccessCode = "AVUZ67DJ37AY46ZUYA";// put the access key in the quotes provided here.
    //#endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (string name in Request.Form)
            {
                if (name != null)
                {
                    if (!name.StartsWith("_"))
                    {
                        ccaRequest = ccaRequest + name + "=" + Request.Form[name] + "&";
                        /* Response.Write(name + "=" + Request.Form[name]);
                          Response.Write("</br>");*/
                    }
                }
            }
            strEncRequest = ccaCrypto.Encrypt(ccaRequest, workingKey);
        }
    }
}

