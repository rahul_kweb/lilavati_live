﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testimonials : System.Web.UI.Page
{
    CMSBAL cmsbal = new CMSBAL();
    Testimonial tblTestimonial = new Testimonial();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeaterCelebrity();
            BindRepeaterCommonPeople();
        }
    }

    public void BindRepeaterCelebrity()
    {
        DataTable dt = new DataTable();
        tblTestimonial.Type = "Celebrity";
        dt = cmsbal.TestimonialCelebrityCommonPeople(tblTestimonial);

        if (dt.Rows.Count > 0)
        {
            rptCelebrity.DataSource = dt;
            rptCelebrity.DataBind();
        }
    }

    public void BindRepeaterCommonPeople()
    {
        DataTable dt = new DataTable();
        tblTestimonial.Type = "Common People";
        dt = cmsbal.TestimonialCelebrityCommonPeople(tblTestimonial);

        if (dt.Rows.Count > 0)
        {
            rptCommonPeople.DataSource = dt;
            rptCommonPeople.DataBind();
        }
    }
}