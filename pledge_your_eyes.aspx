﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="pledge_your_eyes.aspx.cs" Inherits="pledge_your_eyes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script type="text/javascript" src="/js/jquery-ui.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" />
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>

    <script type="text/javascript" src="../js/tabcontent.js"></script>



    <script>
        $(function () {

            //dob
            $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: 'dd-mm-yy'

            });

            $(".dob").change(function () {
                var data = $(".dob").val();
                if (data.length != 0 || data != "") {
                    $("#dob").removeClass("error");
                    $("#dob").addClass("populated");
                }
                else {
                    $("#dob").addClass("error");
                }
            });

            //fancy box
            $('.fancybox').fancybox({
                width: 200
            });

        });

        //number validation
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        //form validation
        function CheckSave() {
            var Name = $('#<%=txtName.ClientID%>').val();
            var Mobile = $('#<%=txtMobileNumber.ClientID%>').val();
            var DOB = $('#<%=txtDOB.ClientID%>').val();
            var City = $('#<%=txtCity.ClientID%>').val();
           
            var blank = false;

            if (Name == '') {
                $("#Name").addClass("error");
                blank = true;
            }
            else {
                $("#Name").removeClass("error");
            }

            if (Mobile == '') {
                $("#Mobile").addClass("error");
                blank = true;
            }
            else {
                if (Mobile.length < 10) {
                    $("#Mobile").addClass("error");
                    blank = true;
                }
                else {
                    $("#Mobile").removeClass("error");
                }
            }

            if (DOB == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (City == '') {
                $("#City").addClass("error");
                blank = true;
            }
            else {
                $("#City").removeClass("error");
            }
                       

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        //success msg
        function ShowPopUp() {
            $(function () {
                $("#showmsg").fancybox().trigger('click');
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Pledge Your Eyes</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="print_share">
                <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                <div class="addthis_native_toolbox"></div>
                <div class="clear"></div>
            </div>

            <asp:Literal ID="litContent" runat="server"></asp:Literal>

            <%--<div class="tagline_box">
                “<strong>One Eye</strong> Donation can make <strong>two people see</strong>.<br>
                Let’s make “Eye Donation” a family tradition”
            </div>

            <p><strong class="green_text uppercase">SEWA-ROSHNI EYE BANK</strong></p>

            <p>One of SEWA’s largest initiatives is the establishment of the SEWA ROSHNI Eye Bank at the Lilavati Hospital. This charitable, nonprofit organization is dedicated to restoring vision through corneal transplantation and medical research. We have organized corneal Grafting Diagnostic Camp followed by free Keratoplasties (Corneal Transplant Surgeries) at Lilavati Hospital & Research Centre.</p>
            <p>The Directorate of Health Services has honored Sewa Roshni Eye Bank in the year 2005-2006 & 2006-2007 with Certificates of Appreciation. A recurring grant is sanctioned to us by District Blindness Control Program, which is an ongoing process.</p>
            <p><strong>“God gave me life but SEWA Roshni Eye Bank gave me EYES to enjoy the beauty of this world”, so said baby Neha 10 year old school going child who corneal blindness following fire cracker injury.</strong></p>

            <p><strong class="green_text uppercase">Contact SEWA - ROSHNI Eye Bank</strong></p>
            <ul class="bullet">
                <li>To register your name as donor</li>
                <li>To call a team of doctors to collect the donated eyes of a family member</li>
                <li>For more information/to clear any doubts about eye donation</li>
            </ul>

            <p>
                <strong>SEWA ROSHNI EYE BANK</strong><br>
                Tel: 26751000 / 26568000 / 98197 78703
            </p>

            <p><strong class="green_text uppercase">BE A SIGHT AMBASSADOR </strong></p>
            <p>India has million corneally blind citizens. 60% of these are children. Only Thousands of pairs of eyes are donated each year. Corneally blind patients wait months and often years to receive a corneal transplant.</p>

            <p><strong class="green_text uppercase">FACTS ABOUT EYE DONATION</strong></p>
            <ul class="bullet">
                <li>Everyone can be a donor irrespective of age, sex or blood group.</li>
                <li>All religions support eye donation as a humanitarian gesture</li>
                <li>Eye donation does not disfigure the face or interfere with funeral arrangements</li>
                <li>After death, eyes should be donated as soon as possible. Eyes can be donated up to 6 hours after death.</li>
                <li>The deceased need not be shifted, as eye bank personnel come to the donor’s home.</li>
                <li>General eye conditions such as cataracts, spectacle number or others do not make the eyes unsuitable for donation.</li>
                <li>Even donors with diabetes or blood pressure can donate their eyes.</li>
                <li>An eye bank is a non-profit organization recognized by the Government to surgically retrieve eyes, antiseptically preserve them, assess their suitability for transplantation and then distribute them to patients requiring corneal transplants.</li>
                <li>Eye donation is free to the donor.</li>
            </ul>

            <p><strong class="green_text uppercase">MORE ABOUT CORNEAL TRANSPLANTATION </strong></p>

            <p>
                <strong>What is the Cornea?</strong>
                <br>
                The cornea is the transparent dome-shaped front surface of the eye that allows rays of light to pass through and be focused on the retina. It is generally the cornea that is used for transplantation.
            </p>

            <p>
                <strong>Who needs a Corneal Transplant? </strong>
                <br>
                Injuries, infections, inherited conditions or aging may cause clouding or distortion of the cornea, with resultant decreased or distorted vision. People with such reduced vision need a cornea transplant to improve vision.
            </p>

            <p>
                <strong>What is Corneal Transplant? </strong>
                <br>
                During corneal transplantation surgery, the central portion of the diseased cornea is removed and replaced by a clear cornea donated by a deceased person. It is generally performed on an out-patient basis, with no overnight hospital stay. It is done under local or general anesthesia and may take 1 to 3 hours.
            </p>

            <p>
                <strong>How are donor eyes obtained? </strong>
                <br>
                Donor eyes come from a recently deceased person and his family who make the humanitarian decision to donate eyes to aid in the restoration of sight. The donor eyes are placed in a special preservative solution that maintains the health of the donor cornea until the time of surgery.
            </p>

            <p>
                <strong>How successful is Cornea Transplantation? </strong>
                <br>
                Corneal transplantation has the highest success rate of all organ transplants.
            </p>

            <p>
                <strong>What about rejection of the transplant? </strong>
                <br>
                Corneas have a very low risk of rejection. If rejection occurs, it can be suppressed by timely medication. The signs and symptoms of rejection are redness, increased light sensitivity, decreased vision and pain.
            </p>


            <strong>Do Corneal Transplants guarantee sight to all blind people?</strong>
            <br>
            No. Transplants help when the loss of sight is solely due to corneal defect and the rest of the eye’s mechanism is intact.</strong>

                    <p><strong class="green_text uppercase">IN THE EVENT OF DEATH </strong></p>

            <p>STEP 1. Close the eyes.</p>
            <p>STEP 2. Cover the closed eyes with moist cotton or cubes of ice. </p>
            <p>STEP 3. Raise the head with two pillows.</p>
            <p>STEP 4. Switch off the fan. (Switch on the air conditioner, if possible)</p>
            <p>STEP 5. Get the death certificate ready </p>
            <p>STEP 6. Contact Roshni eye bank immediately. Eye bank is open for 24 hours.</p>


            <p><a href="patients_education_brochure.aspx" class="uppercase submit_btn" style="color: #FFF;">Appeal</a></p>

            <ul class="bullet">
                <li>Pledge your eyes with the SEWA - ROSHNI Eye bank.</li>
                <li>Your contributions will help provide sight to those lost in perpetual darkness.</li>
                <li>Your support will help in SEWA - ROSHNI’s commitment to restore sight.</li>
            </ul>--%>

            <%--<p><a href="#pledge_form" class="uppercase submit_btn fancybox" style="color: #FFF;">Appeal</a></p>--%>

            <div id="pledge_form" style="display: none">

                <div class="hc_form_div">

                    <div class="apply_form_tl uppercase green_text">Appeal</div>
                    <div class="clear"></div>

                    <div class="FlowupLabels hc_form text_format">

                        <div class="form_field fl_wrap" id="Name">
                            <label class="fl_label uppercase">Name of the donor</label>
                            <asp:TextBox ID="txtName" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="Mobile">
                            <label id="spl" class="fl_label uppercase">Mobile number</label>
                            <asp:TextBox ID="txtMobileNumber" runat="server" class="fl_input" MaxLength="10" onKeyPress='javascript:return onlyNumbers();' autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>

                        <div class="form_field fl_wrap" id="dob">
                            <label class="fl_label uppercase" for="dob">Date of Birth</label>
                            <asp:TextBox ID="txtDOB" runat="server" class="fl_input dob" autocomplete="off" onKeyPress='javascript:return onlyNumbers();'></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="City">
                            <label class="fl_label uppercase" for="city">City</label>
                            <asp:TextBox ID="txtCity" runat="server" class="fl_input doa" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>

                        <p class="green_text"><strong>I hereby give my consent to donate my eyes immediately after the death</strong></p>

                        <p class="green_text"><strong>*To ensure that your pledge is fulfilled, discuss this important decision with your Next of Kin , family doctor and someone close to you.</strong></p>


                        <div class="clear"></div>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="submit_btn fade_anim uppercase" Style="cursor: pointer;" OnClick="btnSubmit_Click" OnClientClick='javascript:return CheckSave();' />

                    </div>






                    <div class="clear"></div>
                </div>

            </div>

            <div id="showmsg" class="hc_form_div" style="display: none;">

                <div class="apply_form_tl uppercase green_text">Appeal </div>
                <div class="clear"></div>

                <div class="hc_thanks text_format">
                    <p><strong class="green_text">Thank you for requesting with us.</strong></p>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="/js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="/js/FlowupLabels_plugin.js"></script>

    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

