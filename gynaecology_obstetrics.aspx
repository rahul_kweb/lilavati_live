﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="gynaecology_obstetrics.aspx.cs" Inherits="gynaecology_obstetrics" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Gynaecology & Obstetrics</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Gynaecology.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>



                <%--   <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>


                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>The department of Gynaecology and Obstetrics offers a spectrum of services with a view to expanding the horizon of hi-tech care. The latest advances in both the field of Obstetrics and Gynaecology accompanied by highly qualified, senior and experienced gynaecologists make this department one of the best. The department can handle gynaecology or obstetric emergency besides handling routine problems and problems of adolescence, reproductive age group patients and problems related to menopausal women. The department has developed into a tertiary care centre in a short while and has also emerged as a primary referral centre for high risk Obstetric care in Mumbai. The department is accredited for training residents in Diploma of National Board (DNB) and conducts regular scientific activities like lectures, seminars, workshops, hands on cadaver workshop and invite guest lectures. It also works closely with the Research department of the hospital and has published several papers.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Painless Deliveries</li>
                                <li>Normal and Assisted Vaginal Deliveries </li>
                                <li>Routine Gyneac and Obstetric Surgery </li>
                                <li>Caesarean Section</li>
                                <li>Hysterectomies </li>
                                <li>Repair of Prolapse </li>
                                <li>Removal of Ovarian, tumor, fibroid </li>
                                <li>Advances Endoscopic Surgeries </li>
                                <li>Urogynocological procedures </li>
                                <li>Management of high risk pregnancies </li>
                                <li>Genetic counseling and diagnosis </li>
                                <li>Family Planning</li>
                                <li>Adolescent Gynaec Problems</li>
                                <li>Well Baby Clinic </li>
                                <li>Cancer Screening in Gynecology</li>
                                <li>Gynacological Cancer Surgeries </li>
                                <li>Infertility –One of the Best IVF center</li>
                                <li>Counseling – Premarital, Prenatal , Postnatal, etc </li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>State of the art Endoscopic theater with 3D Cameras, Monitors and ancillary equipments.</li>
                                <li>High Tech Labour room with birthing chair</li>
                                <li>Opearting Hysteroscopes , Versapoint, Harmonic ultrasound Ligasure vessel sealing devices, Latest Unipolar and Bipolar coagulation colposcope</li>
                                <li>High risk pregnancy for continuous foetal Monitors </li>
                                <li>Epidural Anesthesia for painless delivery </li>
                                <li>Baby friendly nursing care with lactation management </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

