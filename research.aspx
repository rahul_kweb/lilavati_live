﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="research.aspx.cs" Inherits="research" %>

<%@ Register TagName="Professionals" TagPrefix="menu" Src="Control/professionals.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Research</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

            <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <li><a href="#" rel='tab_overview'>Overview</a></li>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>


                    <div id='tab_overview'>
                        <asp:Literal ID="litContent" runat="server"></asp:Literal>
                    </div>

                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>
                    </div>

                    <div class="clear"></div>
                </div>


                <%--<p>
                    The Lilavati Kirtilal Mehta Medical Trust Research Centre is a Scientific and Industrial Research Organization approved by Ministry of Science and Technology (Govt. of India).
                </p>

                <p>The Research Centre under guidelines of Department of Science & Technology works in close collaboration in evaluating and developing technologies for better health care to the sick people. The research centre have undertaken multidisciplinary researches in the fields of Cardiology, Radiology, Cerebrovascular Diseases (Stroke), Ophthalmology, Chest Medicine, Nuclear Medicine, Pathology, Oncology, Orthopedics etc, to cite a few.</p>

                <p>One of the important aims of the Research Centre is to establish Community based epidemiological researches in Cerebrovascular disease in stroke. As a policy Drug and Device Trials are not undertaken at the Research Centre.</p>

                <ul class="bullet">
                    <li><a href="docs/Dr._Lele's_articles.docx"><strong>Publication of Dr. Lele</strong></a></li>
                    <li><a href="docs/MR_Lokeshwar_PUBLICATION_2015.docx"><strong>Publication of Dr. MR Lokeshwar</strong></a></li>
                </ul>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Professionals ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
          <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

