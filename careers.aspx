﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true"
    CodeFile="careers.aspx.cs" Inherits="careers" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox();

        });
    </script>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
    <script type="text/javascript" src="js/jquery-ui.js"></script>

    <link href="select2/select2.min.css" rel="stylesheet" />
    <script src="select2/select2.min.js"></script>
    <script>
        $(document).ready(function () {

            $(".js-example-basic-hide-search").select2({
                minimumResultsForSearch: Infinity
            });

        });

        $(function () {

            $('#dep').click(function () {
                $('#<%=ddlDepartment.ClientID%>').select2('open');
            })

            $('#pos').click(function () {
                $('#<%=ddlPosition.ClientID%>').select2('open');
            })

            var data = $('#<%=ddlDepartment.ClientID%>').val();
            if (data.length > 0 && data != "") {
                $('#dep').addClass("populated");
            } else {
                $('#dep').removeClass("populated");
            }

            var posdata = $('#<%=ddlPosition.ClientID%>').val();
            if (posdata.length > 0 && posdata != "") {
                $('#pos').addClass("populated");
            } else {
                $('#pos').removeClass("populated");
            }
        })

    </script>

    <%--<script>
        $(function () {
            $("#dob").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: "dd-mm-yy"
            });
        });
    </script>--%>
    <%-- ================================================ Fill auto Dropdown  =================================== --%>
    <script type="text/javascript">
        function FillDropDown(getid) {

            var id = getid;
            $(document.body).append('<div class="AjaxLoading"><div></div><div>');

            $.ajax({
                type: "POST",
                url: "careers.aspx/BindDropDown",
                data: '{id:"' + id + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,

                success: function (response) {
                    //$("#dvCustomers").show();
                    // console.log(response.d);
                    // $(".AjaxLoading").remove();
                    var xmlDoc = $.parseXML(response.d);
                    var xml = $(xmlDoc);
                    var customers = xml.find("Table1");
                    //console.log("hello santy"+customers[0].["HEADING"]);
                    // console.log(xml);
                    var data = "";
                    // var ddlCustomers = $("[id*=ddlPosition]");
                    $('#<%=ddlDepartment1.ClientID %>').empty();
                    $('#<%=ddlPosition1.ClientID %>').empty();

                    xml.find("Table1").each(function () {
                        ////                        console.log($(this).find("Post_Id").text() + " Position Id");
                        ////                        console.log($(this).find("Position").text() + " Position");
                        ////                        console.log($(this).find("Job_Area").text() + " Department");


                        $('#<%=ddlDepartment1.ClientID %>').append($("<option></option>").val($(this).find("Job_Id").text()).html($(this).find("Job_Area").text()));
                        $('#<%=ddlPosition1.ClientID %>').append($("<option></option>").val($(this).find("Post_Id").text()).html($(this).find("Position").text()));



                        /*Remove Over Laping on Dropdown*/

                        var data = $('#<%=ddlDepartment1.ClientID%>').val();
                        if (data.length != 0 || data != "") {

                            $("#Department").removeClass("error");
                            $("#Department").addClass("populated");
                        }
                        else {
                            $("#Department").addClass("error");
                        }

                        var data = $('#<%=ddlPosition1.ClientID%>').val();
                        if (data.length != 0 || data != "") {

                            $("#Position").removeClass("error");
                            $("#Position").addClass("populated");
                        }
                        else {
                            $("#Position").addClass("error");
                        }

                        /*Remove Over Laping on Dropdown*/

                        // alert("WORKING");
                        var departmentName = $(this).find("Job_Area").text()
                        var departmentId = $(this).find("Job_Id").text()
                        var Positionname = $(this).find("Position").text()

                        //  alert(departmentid);
                        $('#<%=hdndepartmentname.ClientID %>').val(departmentName);
                        $('#<%=hdndepartmentid.ClientID %>').val(departmentId);
                        $('#<%=hdnpositionname.ClientID %>').val(Positionname);



                    });

                    //$("tbody").html(data);

                    $(".AjaxLoading").remove();
                },
                failure: function (response) {
                    $(".AjaxLoading").remove();
                    alert('fail' + response.d);
                },
                error: function (jqXHR, exception) {
                    $(".AjaxLoading").remove();
                    alert(jqXHR.responseText);

                }
            });



        } </script>
    <%-- ========================================= Store File upload value in textbox ============================ --%>
    <script type="text/javascript">

        $(function () {
            $("input:file").change(function () {
                var fileName = $(this).val();

                $('#<%=fakefilepc.ClientID %>').val(fileName);

                var data = $('#<%=fakefilepc.ClientID%>').val();
                if (data.length != 0 || data != "") {

                    $("#fileupload").removeClass("error");
                    $("#fileupload").addClass("populated");
                }
                else {
                    $("#fileupload").addClass("error");
                }

            });
        });



    </script>
    <%-- ========================== Validatation of all Fields ================================= --%>
    <script type="text/javascript">
        function Checkfields() {
            var Fname = $('#<%=txtfname.ClientID %>').val();
            var Lname = $('#<%=txtlname.ClientID %>').val();
            var Dob = $('#<%=txtDob.ClientID %>').val();
            var Gender = $('#<%=ddlgender.ClientID %>').val();
            var contactno = $('#<%=txtcontactno.ClientID %>').val();
            var email = $('#<%=txtemail.ClientID %>').val();
            var residentof = $('#<%=txtResident.ClientID %>').val();
            var presentworkinglocation = $('#<%=txtpresentworkinglocation.ClientID %>').val();
            var relaventexp = $('#<%=txtrelaventexp.ClientID %>').val();
            var totalexp = $('#<%=txttotalexp.ClientID %>').val();
            var monthexpsalary = $('#<%=txtmonthexpsalary.ClientID %>').val();
            var qualification = $('#<%=txtqualification.ClientID %>').val();
            var ddlhostelaccommodation = $('#<%=ddlhostelaccommodation.ClientID %>').val();
            var ddlregistrationwithmmc = $('#<%=ddlregistrationwithmmc.ClientID %>').val();
            var fileupload = $('#<%=fakefilepc.ClientID %>').val();

            console.log(Dob);
            var Blank = false;


            if (Fname == '') {
                $("#fnamevalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#fnamevalidate").removeClass("error");
            }

            if (Lname == '') {
                $("#lastnamevalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#lastnamevalidate").removeClass("error");
            }
            if (Dob == '') {
                $("#DateofBirth").addClass("error");
                Blank = true;
            }
            else {
                $("#DateofBirth").removeClass("error");
            }
            if (Gender == '') {
                $("#gendervalidation").addClass("error");
                Blank = true;
            }
            else {
                $("#gendervalidation").removeClass("error");
            }
            if (contactno == '') {
                $("#contactnovalidate").addClass("error");
                Blank = true;
            }
            else {
                var mob = /^[1-9]{1}[0-9]{9}$/;
                console.log(mob.test(contactno));

                if (mob.test(contactno) == false) {

                    $("#contactnovalidate").addClass("error");
                    Blank = true;
                }
                else {
                    $("#contactnovalidate").removeClass("error");
                }
            }

            if (email == '') {
                $("#emailidvalidate").addClass("error");
                Blank = true;
            }
            else {

                var emailext = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (emailext.test(email) == false) {
                    $("#emailidvalidate").addClass("error");
                    Blank = true;
                }
                else {

                    $("#emailidvalidate").removeClass("error");
                }
            }
            if (residentof == '') {
                $("#residentofvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#residentofvalidate").removeClass("error");
            }
            if (presentworkinglocation == '') {
                $("#presentworkinglocationvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#presentworkinglocationvalidate").removeClass("error");
            }
            if (relaventexp == '') {
                $("#relaventexpvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#relaventexpvalidate").removeClass("error");
            }
            if (totalexp == '') {
                $("#totalexpvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#totalexpvalidate").removeClass("error");
            }
            if (monthexpsalary == '') {
                $("#monthexpsalaryvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#monthexpsalaryvalidate").removeClass("error");
            }
            if (qualification == '') {
                $("#qualificationvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#qualificationvalidate").removeClass("error");
            }
            if (ddlhostelaccommodation == '') {
                $("#ddlhostelaccommodationvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#ddlhostelaccommodationvalidate").removeClass("error");
            }
            if (ddlregistrationwithmmc == '') {
                $("#ddlregistrationwithmmcvalidate").addClass("error");
                Blank = true;
            }
            else {
                $("#ddlregistrationwithmmcvalidate").removeClass("error");
            }
            if (fileupload == '') {
                $("#fileupload").addClass("error");
                Blank = true;
            }
            else {
                $("#fileupload").removeClass("error");
            }

            if (Blank) {
                // alert("sxbsjbxs");
                //                console.log("error");
                $(".AjaxLoading").remove();
                return false;
            }
            else {
                $(".AjaxLoading").shoe();
                // alert("ok");
                return true;

            }
        }
    </script>
    <%-- ========================== Reset  all Fields ================================= --%>
    <script type="text/javascript">
        function ResetFields() {
            $('#<%=txtfname.ClientID %>').val('');
            $('#<%=txtlname.ClientID %>').val('');
            $('#<%=txtDob.ClientID %>').val('');
            $('#<%=ddlgender.ClientID %>').val('');
            $('#<%=txtcontactno.ClientID %>').val('');
            $('#<%=txtemail.ClientID %>').val('');
            $('#<%=txtResident.ClientID %>').val('');
            $('#<%=txtpresentworkinglocation.ClientID %>').val('');
            $('#<%=txtrelaventexp.ClientID %>').val('');
            $('#<%=txttotalexp.ClientID %>').val('');
            $('#<%=txtmonthexpsalary.ClientID %>').val('');
            $('#<%=txtqualification.ClientID %>').val('');
            $('#<%=ddlhostelaccommodation.ClientID %>').val('');
            $('#<%=ddlregistrationwithmmc.ClientID %>').val('');
            $('#<%=fakefilepc.ClientID %>').val('');
            return false;
        }

    </script>
    <script type="text/javascript">
        function ShowPopUp() {
            $(function () {
                $("#apply_form1").fancybox().trigger('click');
            });
        }

        function OnlineFormRemoveFunction() {
            /** Department **/
            $("#ctl00_ContentPlaceHolder1_ddlDepartment1 option:selected").text("");
            $("#ctl00_ContentPlaceHolder1_ddlDepartment1 option:selected").val("");
            $("#Department").removeClass("populated");
            $("#ctl00_ContentPlaceHolder1_ddlDepartment1").prop("disabled", true);

            /** Position **/
            $("#ctl00_ContentPlaceHolder1_ddlPosition1 option:selected").text("");
            $("#ctl00_ContentPlaceHolder1_ddlPosition1 option:selected").val("");
            $("#Position").removeClass("populated");
            $("#ctl00_ContentPlaceHolder1_ddlPosition1").prop("disabled", true);
        }

        $(document).ready(function () {
            var dep = $("#ctl00_ContentPlaceHolder1_ddlDepartment option:selected").text();
            if (dep == "") {
                $("#ctl00_ContentPlaceHolder1_ddlPosition").prop("disabled", true);
            }
            else {
                $("#ctl00_ContentPlaceHolder1_ddlPosition").prop("disabled", false);
            }
        });

    </script>
    <style>
        /* Change the white to any color ;) */
        input:-webkit-autofill {
            -webkit-box-shadow: 0 0 0px 1000px #F9F9F9 inset;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">
        <div class="green_banner">
            <div class="page_title">
                Careers
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="container text_format fade_anim">
            <p>Lilavati Hospital &amp; Research Centre offers a great career prospect in the area of medicine and healthcare management. The hospital adheres to international norms, which provides our staff with exposure to international standards of healthcare management. A great career is not only about the amount of learning and growth but is also about the remuneration &amp; we provide the best in the market. We have several opportunities awaiting you; please go through them below:</p>
            <div class="form_div career_filters FlowupLabels">

                <div class="form_field fl_wrap" id="dep">
                    <label class="fl_label uppercase">
                        Department</label>
                    <asp:DropDownList ID="ddlDepartment" runat="server" class="fl_input" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                        AutoPostBack="true" CssClass="js-example-basic-hide-search" Style="width: 100%;">
                    </asp:DropDownList>
                    <%--<select class="fl_input">
                        <option></option>
                        <option>Medical</option>
                        <option>Para-Medical</option>
                        <option>Non-Medical</option>
                    </select>--%>
                </div>
                <div class="form_field fl_wrap" id="pos">
                    <label class="fl_label uppercase">
                        Position</label>
                    <asp:DropDownList ID="ddlPosition" runat="server" class="fl_input" OnSelectedIndexChanged="ddlPosition_SelectedIndexChanged"
                        AutoPostBack="true" CssClass="js-example-basic-hide-search" Style="width: 100%;">
                    </asp:DropDownList>
                    <%--<select class="fl_input">
                        <option></option>
                        <option>Medical</option>
                        <option>Para-Medical</option>
                        <option>Non-Medical</option>
                    </select>--%>
                </div>
                <asp:Button ID="btnViewAll" runat="server" Text="View All" OnClick="btnViewAll_Click"
                    class="submit_btn fade_anim uppercase" Style="float: left; cursor: pointer;" />
                <%--<input class="submit_btn fade_anim uppercase" type="reset" value="View All" style="float: left; cursor: pointer;" />--%>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
            <table class="careers">
                <thead>
                    <asp:Panel ID="pnlCareer" runat="server">
                        <tr>
                            <td style="width: 150px;">Department
                            </td>
                            <td style="width: 180px">Position
                            </td>
                            <td>Description
                            </td>
                            <td style="width: 90px">Vacancy
                            </td>
                            <td style="width: 90px;"></td>
                        </tr>
                    </asp:Panel>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptOpening" runat="server" OnItemDataBound="rptOpening_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td data-title="Department">
                                    <%#Eval("JOB_AREA") %>
                                </td>
                                <td data-title="Position">
                                    <%#Eval("POSITION") %>
                                </td>
                                <td data-title="Description">
                                    <%#Eval("SPECIFICATION") %>
                                </td>
                                <td data-title="Vacancy">
                                    <%#Eval("VACANCY") %>
                                </td>
                                <td>
                                    <a href="#apply_form" class="fancybox apply_btn" onclick="FillDropDown('<%#Eval("POST_ID") %>')">APPLY
                                        <asp:HiddenField ID="hdnId" runat="server" Value='<%#Eval("POST_ID") %>' />
                                    </a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="clear">
            </div>
            <a href="#apply_form" onclick="OnlineFormRemoveFunction();" class="fancybox uppercase green_text online_app_form">Apply Online Application Form</a>
            <div class="clear">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div id="apply_form" style="display: none;">
        <asp:Label ID="lblid" runat="server"></asp:Label>
        <div class="form_apply_main">
            <div class="apply_form_tl uppercase green_text">
                Apply Online Application Form
            </div>
            <div class="clear">
            </div>
            <div class="apply_form_div FlowupLabels">
                <div class="form_field fl_wrap" id="Department">
                    <label class="fl_label">
                        Department applied for</label>
                    <asp:DropDownList ID="ddlDepartment1" runat="server" class="fl_input">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdndepartmentname" runat="server" />
                    <asp:HiddenField ID="hdndepartmentid" runat="server" />
                    <%--<select class="fl_input">
                        <option></option>
                        <option>Medical</option>
                        <option>Para-medical</option>
                        <option>Non-medical</option>
                    </select>--%>
                </div>
                <div class="form_field fl_wrap" id="Position">
                    <label class="fl_label">
                        Position applied for</label>
                    <asp:DropDownList ID="ddlPosition1" runat="server" class="fl_input">
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnpositionname" runat="server" />
                    <%--<select class="fl_input">
                        <option></option>
                        <option>Medical</option>
                        <option>Para-medical</option>
                        <option>Non-medical</option>
                    </select>--%>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="fnamevalidate">
                    <label class="fl_label">
                        First Name</label>
                    <%--  <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtfname" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form_field fl_wrap" id="lastnamevalidate">
                    <label class="fl_label">
                        Last Name</label>
                    <%-- <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtlname" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="DateofBirth">
                    <label class="fl_label" for="dob">
                        Date of Birth</label>
                    <input class="fl_input dob" type="text" />
                    <asp:TextBox ID="txtDob" runat="server" class="fl_input dob" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form_field fl_wrap" id="gendervalidation">
                    <label class="fl_label">
                        Gender</label>
                    <asp:DropDownList ID="ddlgender" runat="server" class="fl_input">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="Male">Male</asp:ListItem>
                        <asp:ListItem Value="Female">Female</asp:ListItem>
                    </asp:DropDownList>
                    <%-- <select class="fl_input">
                        <option></option>
                        <option>Male</option>
                        <option>Female</option>
                    </select>--%>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="contactnovalidate">
                    <label class="fl_label">
                        Contact No.</label>
                    <%-- <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtcontactno" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form_field fl_wrap" id="emailidvalidate">
                    <label class="fl_label">
                        Email</label>
                    <%--   <input class="fl_input" type="email" />--%>
                    <asp:TextBox ID="txtemail" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="residentofvalidate">
                    <label class="fl_label">
                        Resident of</label>
                    <%-- <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtResident" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form_field fl_wrap" id="presentworkinglocationvalidate">
                    <label class="fl_label">
                        Presently working location</label>
                    <%-- <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtpresentworkinglocation" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="relaventexpvalidate">
                    <label class="fl_label">
                        Relevant Experience in years</label>
                    <%--<input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtrelaventexp" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form_field fl_wrap" id="totalexpvalidate">
                    <label class="fl_label">
                        Total Experience</label>
                    <%--  <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txttotalexp" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="monthexpsalaryvalidate">
                    <label class="fl_label">
                        Monthly expected Salary</label>
                    <%-- <input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtmonthexpsalary" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form_field fl_wrap" id="qualificationvalidate">
                    <label class="fl_label">
                        Qualification
                    </label>
                    <%--<input class="fl_input" type="text" />--%>
                    <asp:TextBox ID="txtqualification" runat="server" CssClass="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="ddlhostelaccommodationvalidate">
                    <label class="fl_label">
                        Hostel Accommodation required</label>
                    <asp:DropDownList ID="ddlhostelaccommodation" runat="server" class="fl_input">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem disabled="disabled">(Only for Nurses & Doctors)</asp:ListItem>
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                        <asp:ListItem Value="Not Applicable">Not Applicable</asp:ListItem>
                    </asp:DropDownList>
                    <%--  <select class="fl_input">
                        <optgroup label="(Only for Nurses & Doctors)">
                            <option></option>
                            <option>Yes</option>
                            <option>No</option>
                        </optgroup>
                    </select>--%>
                </div>
                <div class="form_field fl_wrap" id="ddlregistrationwithmmcvalidate">
                    <label class="fl_label">
                        Registration with MMC/ MNC</label>
                    <asp:DropDownList ID="ddlregistrationwithmmc" runat="server" class="fl_input">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                        <asp:ListItem Value="No">No</asp:ListItem>
                        <asp:ListItem Value="Not Applicable">Not Applicable</asp:ListItem>
                    </asp:DropDownList>
                    <%--  <select class="fl_input">
                        <option></option>
                        <option>Yes</option>
                        <option>No</option>
                        <option>Not Applicable</option>
                    </select>--%>
                </div>
                <div class="clear">
                </div>
                <div class="form_field fl_wrap" id="fileupload">
                    <label class="fl_label">
                        Upload Resume
                    </label>
                    <%--<input class="upload_btn" type="file" name="filepc" id="filepc" onchange="document.getElementById('fakefilepc').value = this.value;" />--%>
                    <%--<input class="fl_input" type="text" name="fakefilepc" id="fakefilepc" />--%>
                    <asp:FileUpload ID="filepc" class="upload_btn" runat="server" />
                    <asp:TextBox ID="fakefilepc" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                </div>
                <div class="clear">
                </div>
                <div class="form_centering_div" style="font-size: 12px; position: relative; line-height: 18px; color: #000; border-top: 1px solid #ccc; padding: 10px 15px 15px; margin: 0px -15px 0px; text-align: center; background: #eee;">
                    I declare that the information given is true and correct and I am fully aware that
                    it will form the basis for shortlisting for interview, and anytime if it is found
                    incorrect I will qualify for "disqualification".
                    <div class="clear" style="height: 10px;">
                    </div>
                    <%-- <input class="submit_btn fade_anim uppercase" type="submit" value="SUBMIT" />--%>
                    <asp:Button ID="btnsubmit" runat="server" Text="SUBMIT" class="submit_btn fade_anim uppercase"
                        OnClientClick="javascript:return Checkfields();" OnClick="btnsubmit_Click" />
                    <br />
                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClientClick="javascript:return ResetFields();"
                        class="submit_btn fade_anim uppercase" Visible="false" />
                    <asp:Button ID="btnmsggclick" runat="server" Text="test" Visible="false" />
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <%--<a id="anchoridformsg" runat="server" class="fancybox apply_btn" href="#apply_form1">
        click here </a>--%>
    <div id="apply_form1" style="display: none;">
        <div class="form_apply_main">
            <div class="apply_form_tl uppercase green_text">
                Thank you, for your interest!
            </div>
            <div class="clear">
            </div>
            <div class="form_centering_div" style="font-size: 12px; line-height: 18px; color: #000; border-top: 1px solid #ccc; padding: 10px 15px 15px; text-align: center; background: #eee;">
                Our HR executive will get back to you shortly,
                <br />
                if your resume is found eligible for the post applied.
            </div>
        </div>
    </div>
    <%--<div id="apply_form1" style="display: none;">
        <div class="careers_thanks text_format" style="display: none;">
            <p>
                <strong class="green_text uppercase">Thank you, for your interest!</strong></p>
            <p>
                Our HR executive will get back to you shortly, if your resume is found eligible
                for the post applied.</p>
        </div>
    </div>--%>
    <script type="text/javascript">
        $(function () {
            $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: "dd-mm-yy"
            });

            $(".dob").change(function () {
                var data = $(this).val();
                if (data.length != 0 || data != "") {
                    $("#DateofBirth").removeClass("error");
                    $("#DateofBirth").addClass("populated");
                }
                else {
                    $("#DateofBirth").addClass("error");
                }
            });
        });
    </script>
    <script type="text/javascript" src="js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="js/FlowupLabels_plugin.js"></script>
</asp:Content>
