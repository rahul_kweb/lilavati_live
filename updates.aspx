﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="updates.aspx.cs" Inherits="updates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>

    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox({
                loop: false,
                padding: 1,
                afterLoad: function () {
                    this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                }
            });

        });
    </script>

    <%--<script>
        ddaccordion.init({
            headerclass: "c_expand_hd",
            contentclass: "c_expand_box",
            revealtype: "click",
            mouseoverdelay: 200,
            collapseprev: true,
            defaultexpanded: [],
            onemustopen: false,
            scrolltoheader: true,
            animatedefault: false,
            persiststate: false,
            toggleclass: ["", "openheader"],
            togglehtml: ["prefix", "", ""],
            animatespeed: "normal",
            oninit: function (headers, expandedindices) {
            },
            onopenclose: function (header, index, state, isuseractivated) {
                //alert("hii");

            }
        })
    </script>

    <script src="select2/animatedcollapse.js"></script>

    <script type="text/javascript">

        animatedcollapse.addDiv('text1', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text2', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text3', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text4', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text5', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text6', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text7', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text8', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text9', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text10', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text11', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text12', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text13', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text14', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text15', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text16', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text17', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text18', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text19', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text20', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text21', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text22', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text23', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text24', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text25', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text26', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text27', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text28', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text29', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.addDiv('text30', 'fade=0,speed=400,group=pets,hide=1')
        animatedcollapse.ontoggle = function ($, divobj, state) { //fires each time a DIV is expanded/contracted
            //$: Access to jQuery
            //divobj: DOM reference to DIV being expanded/ collapsed. Use "divobj.id" to get its ID
            //state: "block" or "none", depending on state
        }

        animatedcollapse.init()


        //$(document).ready(function () {

        //    $('.c_expand_hd').click(function () {                
        //        //$(this).find('#text3').hide();

        //        var input = window.location.href;
        //        var index = input.lastIndexOf("=");
        //        if (index > 0)
        //            input = input.substring(0, index);
        //        console.log(input);

        //       // $("#text3").toggle();
        //    });

        //});



    </script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Updates</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format">


         <%--   <div class="accd_box">
                <div class="hd c_expand_hd" href="javascript:animatedcollapse.toggle('text1')" rel='toggle["text1"]'>Lilavati Hospital Introduces PET Scan Facility</div>
                <div id='text1' class="update_box accd_content c_expand_box">

                    <p>The Lilavati hospital Nuclear Medicine department has installed a new SIEMENS BIOGRAPH PET-CT machine. It has high resolution of 2mm which produces high definition images and has large bore in its gantry and the patient’s bed is comfortable with smooth motion due to a magnetic principle incorporated in it. These THREE points make it different than the currently existing PET-CT machines in the city of Mumbai. The Time-Of-Flight technique has increased sensitivity resulting in use of less isotope and thereby reduced radiation burden to the patient, respiratory gating sensor based techniques have reduced artefacts.</p>
                    <p>The depth of nuclear medicine will be conducting research work in close collaboration with BARC using new isotopes in the field of Breast Cancer, infections and prostate cancer. The dept will install Gallium based PET scan procedures shortly for use in Neuroendocrine Tumors, Prostate cancers and certain types of Brain Tumors.</p>
                    <p>
                        <img src="images/updates/Pet_scan.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>

            <div class="accd_box">
                <div class="hd c_expand_hd" href="javascript:animatedcollapse.toggle('text2')" rel='toggle["text2"]'>Pathology OPD Services 24 X 7 </div>
                <div id='text2' class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/Pathology_OPD_Services.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>

            <div class="accd_box">
                <div class="hd c_expand_hd" href="javascript:animatedcollapse.toggle('text3')" rel='toggle["text3"]'>Lilavati hospital rank in Top 10, THE WEEK </div>
                <div id='text3' class="update_box accd_content c_expand_box">
                    <p>“<strong>THE WEEK-NIELSEN</strong>” survey for rating the best hospitals in the country has yet again adjudged “<strong>Lilavati Hospital</strong>” amongst the best hospital ranking 8th in the country. Eleven other specialties of our hospital are also ranked amongst the Top 15.</p>
                    <p>The survey was conducted on stringent quality measures by 2308 doctors, covering 19 cities from September 15 to November 4, 2015. The parameters for selection were Competency of doctors, quality of patient care, availability of multi-speciality facilities, overall reputation, infrastructure, innovation and hospital environment.</p>
                    <p><strong class="green_text uppercase">Snapshot –</strong></p>
                    <ul class="bullet">
                        <li><strong>IN INDIA -  Ranked No. 8 “Best Multi-Speciality Hospital” </strong></li>
                        <li><strong>IN MUMBAI -  Ranked No. 2 “Best Multi-Speciality Hospital” </strong></li>
                        <li><strong>IN WEST ZONE -  Ranked No. 2 “Best Multi-Speciality Hospital” </strong></li>
                    </ul>
                    <p><strong class="green_text uppercase">OTHER SPECIALITIES –</strong></p>
                    <ul class="bullet">
                        <li>Research Facilities – Ranked 9th in India</li>
                        <li>Orthopaedics – Ranked 11th in India</li>
                        <li>Cardiology – Ranked 10th in India</li>
                        <li>Opthalmology – Ranked 7th in India</li>
                        <li>Pulmonology – Ranked 10th in India</li>
                        <li>Oncology – Ranked 11th in India</li>
                        <li>Gastroenterology – Ranked 9th in India</li>
                        <li>Paediatrics – Ranked 14th in India</li>
                        <li>Diabetic Care - Ranked 7th in India</li>
                        <li>Gynaecology – Ranked 3rd in India</li>
                        <li>Neurology – Ranked 10th in India</li>
                    </ul>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>


            <asp:Repeater ID="rptUpdates" runat="server" OnItemDataBound="rptUpdates_ItemDataBound">
                <ItemTemplate>
                    <div class="accd_box">
                        <div class="hd c_expand_hd openheader" ><%#Eval("UPD_TITTLE") %> </div>
                        <div class="update_box accd_content c_expand_box">

                            <asp:Literal ID="litContent" runat="server"></asp:Literal>
                            <asp:HiddenField ID="hdnConent" runat="server" Value='<%#Eval("UPD_CONTENT") %>' />                          
                            <p>
                                <img src='<%#Eval("UPD_IMAGES","uploads/updates/{0}") %>' style="display: <%#Eval("UPD_IMAGES").ToString()==""?"none":"block"%>" />
                            </p>
                            <p>
                                <a href='<%#Eval("UPD_PDF","uploads/updates/{0}") %>' style="display: <%#Eval("UPD_PDF").ToString()==""?"none":"inline-block"%>" target="_blank">View Pdf</a>
                            </p>
                            <p>
                                <a href='<%#Eval("UPD_DOC","uploads/updates/{0}") %>' style="display: <%#Eval("UPD_DOC").ToString()==""?"none":"inline-block"%>" target="_blank">View Doc</a>
                            </p>

                            <div class="clear"></div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
                      
            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital Introduces PET Scan Facility</div>
                <div class="update_box accd_content c_expand_box">

                    <p>The Lilavati hospital Nuclear Medicine department has installed a new SIEMENS BIOGRAPH PET-CT machine. It has high resolution of 2mm which produces high definition images and has large bore in its gantry and the patient’s bed is comfortable with smooth motion due to a magnetic principle incorporated in it. These THREE points make it different than the currently existing PET-CT machines in the city of Mumbai. The Time-Of-Flight technique has increased sensitivity resulting in use of less isotope and thereby reduced radiation burden to the patient, respiratory gating sensor based techniques have reduced artefacts.</p>
                    <p>The depth of nuclear medicine will be conducting research work in close collaboration with BARC using new isotopes in the field of Breast Cancer, infections and prostate cancer. The dept will install Gallium based PET scan procedures shortly for use in Neuroendocrine Tumors, Prostate cancers and certain types of Brain Tumors.</p>
                    <p>
                        <img src="images/updates/Pet_scan.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Pathology OPD Services 24 X 7 </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/Pathology_OPD_Services.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati hospital rank in Top 10, THE WEEK </div>
                <div class="update_box accd_content c_expand_box">
                    <p>“<strong>THE WEEK-NIELSEN</strong>” survey for rating the best hospitals in the country has yet again adjudged “<strong>Lilavati Hospital</strong>” amongst the best hospital ranking 8th in the country. Eleven other specialties of our hospital are also ranked amongst the Top 15.</p>
                    <p>The survey was conducted on stringent quality measures by 2308 doctors, covering 19 cities from September 15 to November 4, 2015. The parameters for selection were Competency of doctors, quality of patient care, availability of multi-speciality facilities, overall reputation, infrastructure, innovation and hospital environment.</p>
                    <p><strong class="green_text uppercase">Snapshot –</strong></p>
                    <ul class="bullet">
                        <li><strong>IN INDIA -  Ranked No. 8 “Best Multi-Speciality Hospital” </strong></li>
                        <li><strong>IN MUMBAI -  Ranked No. 2 “Best Multi-Speciality Hospital” </strong></li>
                        <li><strong>IN WEST ZONE -  Ranked No. 2 “Best Multi-Speciality Hospital” </strong></li>
                    </ul>
                    <p><strong class="green_text uppercase">OTHER SPECIALITIES –</strong></p>
                    <ul class="bullet">
                        <li>Research Facilities – Ranked 9th in India</li>
                        <li>Orthopaedics – Ranked 11th in India</li>
                        <li>Cardiology – Ranked 10th in India</li>
                        <li>Opthalmology – Ranked 7th in India</li>
                        <li>Pulmonology – Ranked 10th in India</li>
                        <li>Oncology – Ranked 11th in India</li>
                        <li>Gastroenterology – Ranked 9th in India</li>
                        <li>Paediatrics – Ranked 14th in India</li>
                        <li>Diabetic Care - Ranked 7th in India</li>
                        <li>Gynaecology – Ranked 3rd in India</li>
                        <li>Neurology – Ranked 10th in India</li>
                    </ul>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Home sample collection services </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/Home_sample_collection.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Latest SICU at Lilavati Hospital - 2015 </div>
                <div class="update_box accd_content c_expand_box">
                    <ul class="img_gallery">
                        <li><a href="images/updates/SICU_1.jpg" class="fancybox" data-fancybox-group="gallery">
                            <img src="images/updates/SICU_1.jpg"></a></li>
                        <li><a href="images/updates/SICU_2.jpg" class="fancybox" data-fancybox-group="gallery">
                            <img src="images/updates/SICU_2.jpg"></a></li>
                        <li><a href="images/updates/SICU_3.jpg" class="fancybox" data-fancybox-group="gallery">
                            <img src="images/updates/SICU_3.jpg"></a></li>
                    </ul>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Latest ICCU at Lilavati Hospital - 2015 </div>
                <div class="update_box accd_content c_expand_box">
                    <ul class="img_gallery">
                        <li><a href="images/updates/ICCU_1.jpg" class="fancybox" data-fancybox-group="gallery2">
                            <img src="images/updates/ICCU_1.jpg"></a></li>
                        <li><a href="images/updates/ICCU_2.jpg" class="fancybox" data-fancybox-group="gallery2">
                            <img src="images/updates/ICCU_2.jpg"></a></li>
                        <li><a href="images/updates/ICCU_3.jpg" class="fancybox" data-fancybox-group="gallery2">
                            <img src="images/updates/ICCU_3.jpg"></a></li>
                    </ul>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital receives India's Most Trusted Brand Award - 2015 </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/COO-Award-pic.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Coronary Graft Petency Flometer - State of Art technology </div>
                <div class="update_box accd_content c_expand_box">
                    <p><strong class="green_text">Another addition to our state of the art technology for efficient patient care</strong></p>
                    <p>Lilavati Hospital is now equipped with Medistim VeriQC,Intraoperative ultrasound imaging machine system which first of its kind in India. This imaging system is used in Cardiac surgery to assess GRAFT flow/ perfusion in coronary bypass surgery. It enables the surgeon to locate intramyocardial coronaries which are not seen on surface of the heart.Cardiac surgeon can now assess the percentage of coronary block/ stenosis by epicardial Doppler probe to decide about the graftability of coronary vessel. Epiaortic ultrasound helps to avoid brain stroke from atherosclerotic plaques in ascending aorta andmeasurement of graft flow intraoperatively.</p>
                    <p>All these functional Imaging and blood flow measurements allows Cardiac surgeons to optimize surgical outcome in Heart surgery and prevent avoidable complications more accurately.</p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Special Packages </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/special_pckg.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital Blood Bank </div>
                <div class="update_box accd_content c_expand_box">

                    <p><strong class="green_text">Majority of blood banks in city are cheating patients</strong></p>
                    <p>By Jyoti Shelar, Mumbai Mirror | Mar 31, 2015, 12.00 AM IST</p>
                    <p>
                        <img src="images/updates/Lilavati_Hospital_Blood_Bank .jpg">
                    </p>
                    <p>An FDA probe against 19 blood banks, including those attached to leading hospitals, has revealed that just three of them - Hinduja, Lilavati, and BD Petit Parsee Hospital - were charging patients correctly.</p>
                    <p>The Food and Drug Administration (FDA) probe into 19 blood banks in the city has found that majority of them have been charging more than the rates prescribed by the state government.</p>
                    <p>According to the FDA's joint commissioner, Sanjay Patil, only three blood banks- attached to Hinduja Hospital, <strong>Lilavati Hospital</strong> and BD Petit Parsee General Hospital - were found to be accurately charging patients. "The remaining 16 blood banks were over charging anywhere between Rs. 50 and Rs. 500," Patil said.</p>
                    <p>A government resolution dated June 2014 has set Rs. 1,450 as the price for one unit of whole blood, Rs. 1,450 for packed red cells, Rs. 400 for fresh frozen plasma, Rs. 400 for one unit of platelets, and Rs. 250 for cryo precipitate.</p>
                    <p>In November last year, the FDA officials inspected 309 blood banks across the state, of which 72 were found to have discrepancies in charging process. Out of the 72 blood banks, 19 were from Mumbai.</p>
                    <p>"Detailed inquiry was conducted in all these 72 blood banks including 19 in Mumbai, and merely three were given clean chit," Patil said, adding that in Mumbai, blood banks of leading hospitals such as Jaslok, Breach Candy, Cumballa Hill, the Bombay Hospital, Asian Heart Institute, Prince Aly Khan, Kokilaben Dhirubhai Ambani, SevenHills Hospital, Raheja and Fortis were found to be overcharging patients. Among the independent blood banks, the Borivali Blood Bank, Suburban Hightech (Malad), Manas Serological (Jogeshwari), West Coast (Mahim), SR Mehta Cardiac Instate (Sion) and Ashirwad (Dadar) were found to be over charging patients.</p>
                    <p>While the FDA had already issued show cause notices to the blood banks, the names have also been sent to the State Blood Transfusion Council (SBTC) for action. "After we issued notices, these blood banks have corrected their rates.</p>
                    <p>However, action has to be taken as they were found over charging during the inquiry," Patil said, adding that there is no act or clause under which FDA can take action. "Since SBTC has the powers, they will have to initiate action," he said.</p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital rated 8th Best Multispeciality Hospital in Inda by THE WEEK, Nov'2014 </div>
                <div class="update_box accd_content c_expand_box">
                    <p><strong>The Week - Nielsen Research Survey</strong></p>
                    <p>
                        The survey was conducted among specialists and general practitioners in 15 cities from September to October.<br />
                        Interviews were conducted among 1,347 doctors. Only experts with 5 years of practice were consulted 303 general practitioners and Specialists were consulted.
                    </p>
                    <p><strong>Parameter used for ranking are:</strong></p>

                    <ul class="bullet">
                        <li>Competency of doctors,</li>
                        <li>Patient Care</li>
                        <li>Availability of multispeciality</li>
                        <li>Reputation of infrastructure</li>
                        <li>Innovation and environment</li>
                    </ul>

                    <p><strong>Ranking Methodology</strong></p>
                    <p>These expert nominated top 5 multispeciality hospital in India, top 5 multispeciality hospital in their cities, top 5 hospital in their specialty, Top 5 in Medical education and top 5 in research facility , output and staff training programme.</p>
                    <p>
                        <img src="images/updates/8_best_2014.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital rated as TOP Multispeciality Hospital in Mumbai by THE WEEK, Nov 2014 </div>
                <div class="update_box accd_content c_expand_box">
                    <p><strong>The Week - Nielsen Research Survey</strong></p>
                    <p>
                        The survey was conducted among specialists and general practitioners in 15 cities from September to October.<br />
                        Interviews were conducted among 1,347 doctors. Only experts with 5 years of practice were consulted 303 general practitioners and Specialists were consulted.
                    </p>
                    <p><strong>Parameter used for ranking are:</strong></p>

                    <ul class="bullet">
                        <li>Competency of doctors,</li>
                        <li>Patient Care</li>
                        <li>Availability of multispeciality</li>
                        <li>Reputation of infrastructure</li>
                        <li>Innovation and environment</li>
                    </ul>

                    <p><strong>Ranking Methodology</strong></p>
                    <p>These expert nominated top 5 multispeciality hospital in India, top 5 multispeciality hospital in their cities, top 5 hospital in their specialty, Top 5 in Medical education and top 5 in research facility , output and staff training programme.</p>
                    <p>
                        <img src="images/updates/mum_best_2014.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital, Most Popular Maternity Hospital </div>
                <div class="update_box accd_content c_expand_box">
                    <p>Lilavati Hospital is the winner in the catagory of Most Popular Maternity Hospital (All India) in the 2nd edition of Child Most Popular Awards 2014 from Child India Magzine. </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">New state of the art pysiotherapy </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/pysiotherapy.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">ASR Hip Replacement done at Lilavati Hospital between 2006 to 2012 </div>
                <div class="update_box accd_content c_expand_box">
                    <table>
                        <thead>
                            <tr>
                                <td>Sr.No</td>
                                <td>NATURE OF OPERATION</td>
                                <td>PATIENT NAME </td>
                                <td>SEX</td>
                                <td>AGE</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>THR (LT)</td>
                                <td>Mr. Amirali Mulji Altani</td>
                                <td>M</td>
                                <td>70</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>THR (RT)</td>
                                <td>Mr. Ashok B. L.</td>
                                <td>M</td>
                                <td>54</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>THR (RT)</td>
                                <td>Mr. Prushottam Das</td>
                                <td>M</td>
                                <td>64</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Vimal Ramakant Patil</td>
                                <td>F</td>
                                <td>55</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>THR (REVISION)</td>
                                <td>Mr. Prabhavati Bachubhai Muchhala</td>
                                <td>M</td>
                                <td>75</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>THR (RT)</td>
                                <td>Mr. Jayesh Dalpat Rai Mehta</td>
                                <td>M</td>
                                <td>43</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>THR (RT)</td>
                                <td>Mr. Umer Saiyad </td>
                                <td>M</td>
                                <td>53</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Nimisha Prashant Mehta</td>
                                <td>F</td>
                                <td>43</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Razia Zafir Ameen</td>
                                <td>F</td>
                                <td>62</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>THR (RT)</td>
                                <td>Mrs. Jayashree M Anaokar</td>
                                <td>F</td>
                                <td>57</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Ashabai Prakash Tulse</td>
                                <td>F</td>
                                <td>39</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>THR (RT)</td>
                                <td>Mrs. Hansbai L. Maheshvari</td>
                                <td>F</td>
                                <td>45</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>THR (LT)</td>
                                <td>Mr. Ganpat B. Rathod</td>
                                <td>M</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>THR (RT)</td>
                                <td>Mr. Srichand B. Manghwani</td>
                                <td>M</td>
                                <td>55</td>
                            </tr>
                            <tr>
                                <td>15</td>
                                <td>THR (RT)</td>
                                <td>Mrs. Rani T. Kukreja</td>
                                <td>M</td>
                                <td>65</td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>THR (BILATERAL)</td>
                                <td>Mr. Ambadas J. Khaire</td>
                                <td>F</td>
                                <td>57</td>
                            </tr>
                            <tr>
                                <td>17</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Usha P. Singhal</td>
                                <td>F</td>
                                <td>57</td>
                            </tr>
                            <tr>
                                <td>18</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Madhuri Padh</td>
                                <td>F</td>
                                <td>63</td>
                            </tr>
                            <tr>
                                <td>19</td>
                                <td>THR (LT)</td>
                                <td>Mr. Sameer Chunilal Bheda</td>
                                <td>M</td>
                                <td>36</td>
                            </tr>
                            <tr>
                                <td>20</td>
                                <td>THR (LT)</td>
                                <td>Mrs. Jyoti Shah</td>
                                <td>F</td>
                                <td>64</td>
                            </tr>
                        </tbody>
                    </table>

                    <p>For Follow-up please contact the doctor who has performed the surgery and/or Lilavati Hospital.</p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">India Healthcare Awards 2013 </div>
                <div class="update_box accd_content c_expand_box">

                    <ul class="img_gallery">
                        <li>
                            <img src="images/updates/award2013_1.jpg" data-title="2013"></li>
                        <li>
                            <img src="images/updates/award2013_2.jpg" data-title="2013"></li>
                        <li>
                            <img src="images/updates/icici.jpg" data-title="2012"></li>
                    </ul>
                    <p>"Lilavati Hospital and Research Centre has been honored with prestigious healthcare award second time in a row for the BEST MULTI SPECIALTY HOSPITAL – METRO category at a ceremony held in New Delhi. The event was presided by Montek Singh Ahluwalia, Deputy Chairman – Planning Commission, Govt. of India.</p>
                    <p>Lilavati Hospital and Research Centre has been honored with prestigious healthcare award for the <strong>BEST MULTI SPECIALTY HOSPITAL – METRO</strong> category at a ceremony held in New Delhi. The event was presided by Montek Singh Ahluwalia, Deputy Chairman – Planning Commission, Govt. of India.</p>
                    <p>The India Healthcare Awards, in association with CNBC TV-18 and ICICI Lombard General Insurance, is a unique platform that recognizes India’s best healthcare providers.</p>
                    <p>Dr. Narendra Trivedi, COO at Lilavati Hospital said “We are extremely proud to have received this recognition. Lilavati Hospital invests in state-of-the-art biomedical instruments and equipments for diagnostic and patient care.”</p>
                    <p>India Healthcare Awards has become the benchmark for recognizing the best healthcare facilities in India and the short listing of hospitals for the final round was based on a robust multi-phase methodology by IMRB, India’s leading research agency.</p>
                    <p>“These esteemed awards were only possible with the commitment, dedication and devotion of all our staff members. This recognition reiterates our motto which is ‘More than Health Care, Human Care’. It is an honor to know that we are able to contribute significantly to our country’s rapidly growing Healthcare industry” said Mr Chetan Mehta, Trustee of the Lilavati Hospital & Research Centre.</p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Save and Empower the Girl Child : An Initiative by Doctors of Lilavati Hospital </div>
                <div class="update_box accd_content c_expand_box">

                    <p>Doctors of Lilavati hospital feel that they need to play in protecting Girl Child from deliberate harm to ensuring her well-being, from conception to adulthood. They may as concept formers and advice givers play a major role in correcting the distorted views that parts of society have formed, and are passing on to their future generations.</p>
                    <p>On 11th April 2012, through a mega event at J W Marriot, Mumbai; Lilavati Hospital and Research Centre (LHRC) launched its most ambitious campaign – “Save and Empower the Girl Child.” Lilavati hospital’s doctors have taken this initiative. The event was organized by LHRC and coordinated by Ms. Poonam Dhillon. Leading bollywood fashion designer Mr. Manish Malhotra paid tribute to the cause with his spectacular fashion show. Dignitaries from the various fields came in to support the noble cause and grace the occasion.</p>
                    <p>Leading Gynaecologist & IVF Expert, <strong>Dr. Hrishikesh Pai</strong> was the Chairman of the Organizing Committee. The core organizing committee consisted of leading Gynecologists - <strong>Dr. Kiran Coelho</strong> as Convener, <strong>Dr. Nandita Palshetkar, Dr. Rishma Pai and Dr. Deepak Ugra</strong>.</p>
                    <p>Chief Guest of the evening <strong>Kajol</strong> said, ’her father never for a moment regretted the fact that he did not have a son’. The actress further said ’children are God’s gift and no one has right to harm a little child or take anyone’s life’, she then urged ’I think the country needs to stand up and take notice about saving the girl child. In today’s time people should not think that women are less that men.I don’t think it is like that today. Basically, it is wrong and illiterate attitude’, she then concluded ‘it is very important and essential to join ‘SAVE AND EMPOWER THE GILR CHILD’ initiative.It is a nice cause and I am happy to be here to support it’.</p>
                    <p>Leading Tennis Player <strong>Leander Paes</strong> said,’the best thing we can do to our children is to let live their dreams like my parents did for me’ he further waxed eloquent about the role of a woman in a man’s life be it mother,sister or daughter.He then praised significant roles played by his wife Rhea and his lovely daughter in his life’s success.</p>
                    <p>Designer <strong>Manish Malhotra</strong> said,’A baby girl is one of the most beautiful miracles in one’s life. This is an opportunity to reach out to people at large. So I am not just dressing celebrities and models but respected doctors and politicians too,the show is about real people. Everyone has come forward with a genuine desire to support and create awareness towards the cause.When it comes to women power,I am all for it’.</p>
                    <p>Event show stopper, <strong>Sushmita Sen</strong> said,’I am happy that Lilavati Hospital is doing this.They are doing this initiative for saving the girl child, which is a powerful thing.’God bless Lilavati Hospital’. I am happy that so many people are here to support, and I am blessed to be here,she further said,’there are very strong laws and acts in our country but they have not been implemented in the right way.Either the case dosent start early or if it starts,iy gets delayed. We all are examples of the fact that givena change we prove ourselves. Its a matter of having that faith’.</p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Latest 3 Tesla Digital Broad Band MRI </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/mri_brochure.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Healthier CT Scanner – 1st of its Kind in Western Region </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <img src="images/updates/ct-scanner.jpg">
                    </p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital – NABH Accredited Healthcare Provider </div>
                <div class="update_box accd_content c_expand_box">
                    <p>NABH is an acronym for National Accreditation Board for Hospitals & Healthcare Providers. NABH is a constituent Board of Quality Council of India (QCI), set up with co-operation of the Ministry of Health & Family Welfare, Government of India and the Indian Health Industry. This Board caters to the much desired needs of the consumers and has set standards for progress of the health industry. NABH is a member of the International Society for Quality in Health Care [ISQua] and thus NABH standard is recognized globally.</p>
                    <p>The board is set up to establish and operate accreditation programme for healthcare organizations. NABH have designed an exhaustive healthcare standard for hospitals and healthcare providers. This standard consists of stringent 600 plus objective elements for the hospital to achieve in order to get the NABH accreditation. These standards are divided between patient centered standards and organization centered standards. NABH accreditation helps planners to promote, implement, monitor and evaluate robust practice in order to ensure that it occupies a central place in the development of the health care system. Thus the objective of NABH accreditation is on continuous improvement in the organizational and clinical performance of health services, not just the achievement of a certificate or award or merely assuring compliance with minimum acceptable standards.</p>
                    <p>
                        <strong>How NABH accreditation helps patients:</strong><br>
                        In an NABH accredited hospital, there is a strong focus on the following:
                    </p>
                    <ul class="bullet">
                        <li>Patient rights and benefits</li>
                        <li>Patient safety</li>
                        <li>Control and prevention of infections</li>
                        <li>Practicing good patient-care protocols e.g. special care for vulnerable groups, critically ill patients</li>
                        <li>Better and controlled clinical outcome.</li>
                    </ul>
                    <p>So, if a hospital is NABH accredited, the patient can be rest assured that the hospital follows stringent standards as laid down by the accreditation body for providing best in patient care comparable to any international hospital of repute.</p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital – Gold Award Winner of Reader’s Digest Trusted Brand Award 2012 </div>
                <div class="update_box accd_content c_expand_box">
                    <p>
                        <strong>Reader’s Digest Most Trusted Brand Awards:</strong><br>
                        2012 is the 14th year in which Reader’s Digest Asia presented its annual trusted brand survey based on the consumer feedback.
                    </p>
                    <p>Thousands of questionnaires were sent out to get responses; also direct feedbacks were taken from Asia’s key consumer markets- China, HongKong, India, Malaysia, Phillipines, Singapore, Taiwan and Thailand. 43 categories of Product and services were taken into account for the survey.</p>

                    <p><strong>Six attributes used for each Brand to be rated:</strong></p>
                    <ul class="bullet">
                        <li>Value</li>
                        <li>Quality</li>
                        <li>Trustworthiness & Credibility</li>
                        <li>Understanding of consumer needs</li>
                        <li>Innovation</li>
                        <li>Social responsibility</li>
                    </ul>

                    <p><strong>Questions asked to Survey participants were:</strong></p>
                    <ul class="bullet">
                        <li>Is the brand believable, safe to use & does it consistently deliver on the promises it makes?</li>
                        <li>Are the Brand’s Products or Services well-made or well designed?</li>
                        <li>Does the Brand offer good value for Money?</li>
                        <li>Does the Brand respond to and satisfy consumers’ changing needs?</li>
                        <li>Does the Brand regularly introduce product, features or services?</li>
                        <li>Does the Brand support the Community, employees, environment and practice good corporate ethics?</li>
                    </ul>

                    <p><strong>Ranking Methodology:</strong></p>
                    <p>Final ranking were reached by multiplying the no. of consumers choosing the brand with the scores on these qualitative factors and incorporated population weighting factors to ensure the statistical accuracy of the results.</p>
                    <p>Awards were made in each of the eight markets as well as on an Asia-wide basis.</p>
                    <p><strong>Gold Trusted Brand award goes to category-leading brand/s that scores significantly higher than their rivals.</strong></p>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital - Amongst Top 10 Hospitals of India 2013 </div>
                <div class="update_box accd_content c_expand_box">
                    <p><strong>The Week - Nielsen Research Survey</strong></p>
                    <p>The survey was conducted in 15 Indian Cities including Delhi, Mumbai, Chennai, Kolkata, and Hyderabad.</p>
                    <p>Phase one- 22 Specialists and 20 general practitioners were taken into account for survey from four metros, Mumbai, Delhi NCR, Kolkata, and Chennai.</p>
                    <p>Phase 2- Face to face, telephonic and online interviews were conducted among 1,163 doctors across 15 cities among select specialties. All experts were required to have minimum five years of practice.</p>
                    <p>306 General practitioners and close about 850 specialties gave their opinion.</p>

                    <p><strong>Parameter used for ranking are:</strong></p>
                    <ul class="bullet">
                        <li>Competency of the Doctors</li>
                        <li>Infrastructure</li>
                        <li>Availability of multi-specialty</li>
                        <li>Patient care</li>
                        <li>Innovation in treatment</li>
                        <li>Overall reputation and hospital environment</li>
                    </ul>

                    <p><strong>Ranking Methodology</strong></p>

                    <ul class="bullet">
                        <li><u>For the best Multi Specialty Hospital in India</u><br>
                            The experts nominated top five multi-specialty hospitals in India in rank order and rated them on seven different attributes.</li>
                        <li><u>For the best Multi Specialty Hospital in City</u><br>
                            Experts also nominated top five multi specialty hospitals in their respective cities based on research facilities and outputs and in terms of staff training programme. Views were also taken on developments related to health care industry.</li>
                    </ul>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <%--<div class="accd_box">
                <div class="hd c_expand_hd">Lilavati Hospital - Best Multispecialty Hospital of Mumbai 2013 </div>
                <div class="update_box accd_content c_expand_box">
                    <p><strong>The Week - Nielsen Research Survey</strong></p>
                    <p>The survey was conducted in 15 Indian Cities including Delhi, Mumbai, Chennai, Kolkata, and Hyderabad.</p>
                    <p>Phase one- 22 Specialists and 20 general practitioners were taken into account for survey from four metros, Mumbai, Delhi NCR, Kolkata, and Chennai.</p>
                    <p>Phase 2- Face to face, telephonic and online interviews were conducted among 1,163 doctors across 15 cities among select specialties. All experts were required to have minimum five years of practice.</p>
                    <p>306 General practitioners and close about 850 specialties gave their opinion.</p>

                    <p><strong>Parameter used for ranking are:</strong></p>
                    <ul class="bullet">
                        <li>Competency of the Doctors</li>
                        <li>Infrastructure</li>
                        <li>Availability of multi-specialty</li>
                        <li>Patient care</li>
                        <li>Innovation in treatment</li>
                        <li>Overall reputation and hospital environment</li>
                    </ul>

                    <p><strong>Ranking Methodology</strong></p>

                    <ul class="bullet">
                        <li><u>For the best Multi Specialty Hospital in India</u><br>
                            The experts nominated top five multi-specialty hospitals in India in rank order and rated them on seven different attributes.</li>
                        <li><u>For the best Multi Specialty Hospital in City</u><br>
                            Experts also nominated top five multi specialty hospitals in their respective cities based on research facilities and outputs and in terms of staff training programme. Views were also taken on developments related to health care industry.</li>
                    </ul>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>--%>

            <!-- xxxxxx -->

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</asp:Content>

