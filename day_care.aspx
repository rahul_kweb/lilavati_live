﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="day_care.aspx.cs" Inherits="day_care" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Day Care</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Our day care centre caters to patients who do not have to stay overnight at the hospital but require to undergo minor surgeries and procedures. The department is manned with a team of highly skilled and experienced and efficient nurses</p>


                            <p><strong class="green_text uppercase">Timings</strong></p>

                            <p>Day care services are available from 7am to 8pm on all days.</p>

                            <p><strong class="green_text uppercase">Admission</strong></p>

                            <p>Admission to day care is done only on doctor’s advice.</p>

                            <p><strong class="green_text uppercase">Location</strong></p>
                            <ul class="bullet">
                                <li>Daycare located on ground floor for patient’s convenience is for all medical & surgical procedures. Here the total bed capacity is 6 beds.</li>
                                <li>Daycare located on the 8th Floor also has 6 beds and is exclusively for patients receiving chemotherapy and supportive treatments ofcancer.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Timing</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>All minor, intermediate, major procedures such as Cardiac Catheterization, Cataract with IOL, Lithotripsy can be performed here.</li>
                                <li>A separate day-care is available on the 8th floor for patients receiving chemotherapy and supportive treatments for cancer.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->


                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

