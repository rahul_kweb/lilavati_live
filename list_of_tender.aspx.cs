﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class list_of_tender : System.Web.UI.Page
{

    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindTender();
            ddlDepartment.Items.Insert(0, "");
            ddlCategory.Items.Insert(0, "");

        }
    }

    public void BindTender()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_AppliedForTender 'getTenderDetails'");
        if (dt.Rows.Count > 0)
        {
            rptTender.DataSource = dt;
            rptTender.DataBind();
        }
        else
        {
            rptTender.DataSource = null;
            rptTender.DataBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_AppliedForTender"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@Designation", txtDesignation.Text.Trim());
            cmd.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text.Trim());
            cmd.Parameters.AddWithValue("@ContactNo", txtContactNos.Text.Trim());
            cmd.Parameters.AddWithValue("@EmailAddress", txtEmailAddress.Text.Trim());
            cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
            cmd.Parameters.AddWithValue("@eTenderNo", hdnTenderCode.Value);
            cmd.Parameters.AddWithValue("@eId", hdneId.Value);
            if (utility.Execute(cmd))
            {
                eTenderEmail mailer = new eTenderEmail();
                mailer.UserDetails(txtName.Text.Trim(), txtDesignation.Text.Trim(), txtCompanyName.Text.Trim(), txtContactNos.Text.Trim(), txtEmailAddress.Text.Trim(), txtRemarks.Text.Trim(), hdnTenderCode.Value);

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("select TenderName,Department,Category,TenderDescription from eTender where eId = '" + hdneId.Value + "'");
                string tenderName = string.Empty;
                string tenderDepartment = string.Empty;
                string tenderCategory = string.Empty;
                string tenderDescription = string.Empty;
                if (dt1.Rows.Count > 0)
                {
                    tenderName = dt1.Rows[0]["TenderName"].ToString();
                    tenderDepartment = dt1.Rows[0]["Department"].ToString();
                    tenderCategory = dt1.Rows[0]["Category"].ToString();
                    tenderDescription = dt1.Rows[0]["TenderDescription"].ToString();
                }

                string path = HttpContext.Current.Server.MapPath("~/docs/Vendor_Registration_Form.docx");
                mailer.TenderDetails(tenderDepartment, tenderCategory, tenderDescription, txtName.Text.Trim(), txtEmailAddress.Text.Trim(), path);

                Reset();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "show", "ShowPopUp();", true);
            }
        }
    }

    public void Reset()
    {
        txtName.Text = string.Empty;
        txtDesignation.Text = string.Empty;
        txtCompanyName.Text = string.Empty;
        txtContactNos.Text = string.Empty;
        txtEmailAddress.Text = string.Empty;
        txtRemarks.Text = string.Empty;

        ddlDepartment.SelectedIndex = -1;
        ddlCategory.SelectedIndex = -1;
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Id = ddlDepartment.SelectedValue;

        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_AppliedForTender 'getTenderDetailsByDept','0','0','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptTender.DataSource = dt;
            rptTender.DataBind();
        }
        else
        {
            rptTender.DataSource = null;
            rptTender.DataBind();
        }
        ddlCategory.SelectedIndex = -1;
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Id = ddlCategory.SelectedValue;

        DataTable dt = new DataTable();
        dt = utility.Display("exec Proc_AppliedForTender 'getTenderDetailsByCat','0','0','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptTender.DataSource = dt;
            rptTender.DataBind();
        }
        else
        {
            rptTender.DataSource = null;
            rptTender.DataBind();
        }

        ddlDepartment.SelectedIndex = -1;
    }


    protected void btnViewAll_Click(object sender, EventArgs e)
    {
        BindTender();
        Reset();
    }
}