﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class awards_accreditations : System.Web.UI.Page
{
    CMSBAL cmsbal = new CMSBAL();
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            AwardsRepeater();
        }
    }

    public void AwardsRepeater()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = cmsbal.GetAwards();
            if (dt.Rows.Count > 0)
            {

                rptAwards.DataSource = dt;
                rptAwards.DataBind();
            }

            tblcmsInnerPageMaster.PageName = "Awards Accreditations";
            DataTable dt1 = new DataTable();
            dt1 = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

            StringBuilder sb1 = new StringBuilder(dt1.Rows[0]["Description"].ToString());
            string dsc1 = sb1.Replace("&nbsp;", "")
                          .Replace("../", "").ToString();

            string val1 = dsc1;

            dt1.Rows[0]["Description"] = val1;

            if (dt1.Rows.Count > 0)
            {
                rptContent.DataSource = dt1;
                rptContent.DataBind();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

        
    }
}