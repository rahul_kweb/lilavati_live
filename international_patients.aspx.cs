﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class international_patients : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
            BindBanner();
        }
    }

    public void Content()
    {
        cms.ID = 17;
        cmsbal.GETBYIDContent(cms);
        litContent.Text = cms.Description;
    }

    //public void BindRepeater()
    //{
    //    try
    //    {
    //        tblcmsInnerPageMaster.PageName = "International Patients";
    //        DataTable dt = new DataTable();
    //        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

    //        if (dt.Rows.Count > 0)
    //        {
    //            rptContent.DataSource = dt;
    //            rptContent.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        this.Title = ex.Message;
    //        //throw;
    //    }

    //}

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(52);
    }

}