﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    HomeBanner tblHomeBanner = new HomeBanner();
    CMSBAL cmsbal = new CMSBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindHomeBanner();
            BindHomeTestimonial();
            BindHomeHealthCheckup();
            //BindHomeAwards();
            BindHomeUpdates();
        }
    }

    public void BindHomeBanner()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetHomeBanner();

        if (dt.Rows.Count > 0)
        {
            rptHomeBanner.DataSource = dt;
            rptHomeBanner.DataBind();
        }
        else
        {
            rptHomeBanner.DataSource = null;
            rptHomeBanner.DataBind();
        }
    }

    public void BindHomeTestimonial()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetHomeTestimonial();

        if (dt.Rows.Count > 0)
        {
            rptTestimonial.DataSource = dt;
            rptTestimonial.DataBind();
        }
        else
        {
            rptTestimonial.DataSource = null;
            rptTestimonial.DataBind();
        }
    }

    public void BindHomeHealthCheckup()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetHealthCheckup();

        if (dt.Rows.Count > 0)
        {
            rptHealthCheckUp.DataSource = dt;
            rptHealthCheckUp.DataBind();
            BindSingleDataOfHealthCheckup();
        }
    }

    //comment on 27-1-20

    //public void BindHomeAwards()
    //{
    //    DataTable dt = new DataTable();
    //    dt = cmsbal.GetAwards();

    //    if (dt.Rows.Count > 0)
    //    {
    //        rptAwards.DataSource = dt;
    //        rptAwards.DataBind();
    //    }
    //}

    public void BindHomeUpdates()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GEtTopTEnUpdatesPage();

        if (dt.Rows.Count > 0)
        {
            rptUpdates.DataSource = dt;
            rptUpdates.DataBind();
        }
    }


    public void BindSingleDataOfHealthCheckup()
    {
        StringBuilder sb = new StringBuilder();
        Utility utility = new Utility();
        SqlCommand cmd = new SqlCommand("SELECT * FROM HEALTH_CHECKUP 	WHERE	ISACTIVE = 1 and Health_Id in (22,20) ORDER BY HEALTH_ID DESC");
        DataTable dt = new DataTable();
        dt = utility.Display(cmd);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<li>");
                sb.Append("<a href='Packages-Details/" + dr["HEALTH_ID"].ToString() + "'>");
                sb.Append(dr["PACKAGE_NAME"].ToString());
                sb.Append(" </a>");
                sb.Append("</li>");
            }
            ltrlget.Text = sb.ToString();
        }
    }
}