﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard/DashboardMaster.master"
    AutoEventWireup="true" CodeFile="update_profile.aspx.cs" Inherits="Dashboard_update_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(function () {
            $(".dob").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "c-100:c",
                dateFormat: "dd-mm-yy"
            });

            $(".dob").change(function () {
                var date = $(".dob").val();
                if (date.length != 0 && date != "") {
                    $("#dob").addClass("populated");
                    $("#dob").removeClass("error");
                }
                else {
                    $("#dob").addClass("error");
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="form_div FlowupLabels">
        <div class="form_field fl_wrap" id="Div1">
            <label class="fl_label uppercase">
                User ID</label>
            <asp:TextBox ID="txtUserID" runat="server" class="fl_input surename" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="Surname">
            <label class="fl_label uppercase">
                Surname</label>
            <asp:TextBox ID="txtSureName" runat="server" class="fl_input surename" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="Div2">
            <label class="fl_label uppercase">
                Title</label>
            <asp:TextBox ID="txtTitle" runat="server" class="fl_input" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="FirstName">
            <label class="fl_label uppercase">
                First Name</label>
            <asp:TextBox ID="txtFirstName" runat="server" class="fl_input FN" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="MiddleName">
            <label class="fl_label uppercase">
                Middle Name</label>
            <asp:TextBox ID="txtMiddleName" runat="server" class="fl_input MN" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Email</label>
            <asp:TextBox ID="txtEmail" runat="server" class="fl_input" Enabled="false" ReadOnly="true"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="dob">
            <label class="fl_label uppercase" for="dob">
                Date of Birth</label>
            <asp:TextBox ID="txtDob" runat="server" class="fl_input dob" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="Sex">
            <label class="fl_label uppercase">
                Sex</label>
            <asp:DropDownList ID="rdbSex" runat="server" class="fl_input sex" Enabled="false">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem>Male</asp:ListItem>
                <asp:ListItem>Female</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_field fl_wrap" id="Div3">
            <label class="fl_label uppercase">
                Blood Group</label>
            <asp:TextBox ID="txtBloodGroup" runat="server" class="fl_input" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Marital Status</label>
            <asp:DropDownList ID="ddlMaritalStatus" runat="server" class="fl_input" Enabled="false">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem Text="Single" Value="Single"></asp:ListItem>
                <asp:ListItem Text="Married" Value="Married"></asp:ListItem>
                <asp:ListItem Text="Divorced" Value="Divorced"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Religion</label>
            <%--<select class="fl_input">
                <option></option>
                <option selected>Hindu</option>
                <option>Muslim</option>
                <option>Christian</option>
                <option>Sikh</option>
                <option>Parsi</option>
                <option>Jain</option>
                <option>Buddhist</option>
                <option>Jewish</option>
                <option>No Religion</option>
                <option>Spiritual</option>
                <option>Other</option>
            </select>--%>
            <asp:DropDownList ID="ddlReligion" runat="server" class="fl_input" Enabled="false">
                <asp:ListItem></asp:ListItem>
                <asp:ListItem>Hindu</asp:ListItem>
                <asp:ListItem>Muslim</asp:ListItem>
                <asp:ListItem>Christian</asp:ListItem>
                <asp:ListItem>Sikh</asp:ListItem>
                <asp:ListItem>Parsi</asp:ListItem>
                <asp:ListItem>Jain</asp:ListItem>
                <asp:ListItem>Buddhist</asp:ListItem>
                <asp:ListItem>Jewish</asp:ListItem>
                <asp:ListItem>No Religion</asp:ListItem>
                <asp:ListItem>Spiritual</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Occupation</label>
            <asp:TextBox ID="txtOccupation" runat="server" class="fl_input" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="clear">
        </div>
        <div class="full_width fl_wrap" id="Address">
            <label class="fl_label uppercase">
                Address</label>
            <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" class="fl_input add" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="clear">
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Country</label>
            <asp:DropDownList ID="ddlCountry" runat="server" class="fl_input" Enabled="false">
                <asp:ListItem>Afghanistan</asp:ListItem>
                <asp:ListItem>Albania</asp:ListItem>
                <asp:ListItem>Algeria</asp:ListItem>
                <asp:ListItem>American Samoa</asp:ListItem>
                <asp:ListItem>Andorra</asp:ListItem>
                <asp:ListItem>Angola</asp:ListItem>
                <asp:ListItem>Anguilla</asp:ListItem>
                <asp:ListItem>Antarctica</asp:ListItem>
                <asp:ListItem>Antigua And Barbuda</asp:ListItem>
                <asp:ListItem>Argentina</asp:ListItem>
                <asp:ListItem>Armenia</asp:ListItem>
                <asp:ListItem>Aruba</asp:ListItem>
                <asp:ListItem>Australia</asp:ListItem>
                <asp:ListItem>Austria</asp:ListItem>
                <asp:ListItem>Azerbaijan</asp:ListItem>
                <asp:ListItem>Bahamas</asp:ListItem>
                <asp:ListItem>Bahrain</asp:ListItem>
                <asp:ListItem>Bangladesh</asp:ListItem>
                <asp:ListItem>Barbados</asp:ListItem>
                <asp:ListItem>Belarus</asp:ListItem>
                <asp:ListItem>Belgium</asp:ListItem>
                <asp:ListItem>Belize</asp:ListItem>
                <asp:ListItem>Benin</asp:ListItem>
                <asp:ListItem>Bermuda</asp:ListItem>
                <asp:ListItem>Bhutan</asp:ListItem>
                <asp:ListItem>Bolivia</asp:ListItem>
                <asp:ListItem>Bosnia And Herzegowina</asp:ListItem>
                <asp:ListItem>Botswana</asp:ListItem>
                <asp:ListItem>Bouvet Island</asp:ListItem>
                <asp:ListItem>Brazil</asp:ListItem>
                <asp:ListItem>British Indian Ocean Territory</asp:ListItem>
                <asp:ListItem>Brunei Darussalam</asp:ListItem>
                <asp:ListItem>Bulgaria</asp:ListItem>
                <asp:ListItem>Burkina Faso</asp:ListItem>
                <asp:ListItem>Burundi</asp:ListItem>
                <asp:ListItem>Cambodia</asp:ListItem>
                <asp:ListItem>Cameroon</asp:ListItem>
                <asp:ListItem>Canada</asp:ListItem>
                <asp:ListItem>Cape Verde</asp:ListItem>
                <asp:ListItem>Cayman Islands</asp:ListItem>
                <asp:ListItem>Central African Republic</asp:ListItem>
                <asp:ListItem>Chad</asp:ListItem>
                <asp:ListItem>Chile</asp:ListItem>
                <asp:ListItem>China</asp:ListItem>
                <asp:ListItem>Christmas Island</asp:ListItem>
                <asp:ListItem>Cocos (Keeling) Islands</asp:ListItem>
                <asp:ListItem>Colombia</asp:ListItem>
                <asp:ListItem>Comoros</asp:ListItem>
                <asp:ListItem>Congo</asp:ListItem>
                <asp:ListItem>Cook Islands</asp:ListItem>
                <asp:ListItem>Costa Rica</asp:ListItem>
                <asp:ListItem>Cote D'Ivoire</asp:ListItem>
                <asp:ListItem>Croatia (Local Name: Hrvatska)</asp:ListItem>
                <asp:ListItem>Cuba</asp:ListItem>
                <asp:ListItem>Cyprus</asp:ListItem>
                <asp:ListItem>Czech Republic</asp:ListItem>
                <asp:ListItem>Denmark</asp:ListItem>
                <asp:ListItem>Djibouti</asp:ListItem>
                <asp:ListItem>Dominica</asp:ListItem>
                <asp:ListItem>Dominican Republic</asp:ListItem>
                <asp:ListItem>East Timor</asp:ListItem>
                <asp:ListItem>Ecuador</asp:ListItem>
                <asp:ListItem>Egypt</asp:ListItem>
                <asp:ListItem>El Salvador</asp:ListItem>
                <asp:ListItem>Equatorial Guinea</asp:ListItem>
                <asp:ListItem>Eritrea</asp:ListItem>
                <asp:ListItem>Estonia</asp:ListItem>
                <asp:ListItem>Ethiopia</asp:ListItem>
                <asp:ListItem>Falkland Islands (Malvinas)</asp:ListItem>
                <asp:ListItem>Faroe Islands</asp:ListItem>
                <asp:ListItem>Fiji</asp:ListItem>
                <asp:ListItem>Finland</asp:ListItem>
                <asp:ListItem>France</asp:ListItem>
                <asp:ListItem>French Guiana</asp:ListItem>
                <asp:ListItem>French Polynesia</asp:ListItem>
                <asp:ListItem>French Southern Territories</asp:ListItem>
                <asp:ListItem>Gabon</asp:ListItem>
                <asp:ListItem>Gambia</asp:ListItem>
                <asp:ListItem>Georgia</asp:ListItem>
                <asp:ListItem>Germany</asp:ListItem>
                <asp:ListItem>Ghana</asp:ListItem>
                <asp:ListItem>Gibraltar</asp:ListItem>
                <asp:ListItem>Greece</asp:ListItem>
                <asp:ListItem>Greenland</asp:ListItem>
                <asp:ListItem>Grenada</asp:ListItem>
                <asp:ListItem>Guadeloupe</asp:ListItem>
                <asp:ListItem>Guam</asp:ListItem>
                <asp:ListItem>Guatemala</asp:ListItem>
                <asp:ListItem>Guinea</asp:ListItem>
                <asp:ListItem>Guinea-Bissau</asp:ListItem>
                <asp:ListItem>Guyana</asp:ListItem>
                <asp:ListItem>Haiti</asp:ListItem>
                <asp:ListItem>Heard And Mc Donald Islands</asp:ListItem>
                <asp:ListItem>Holy See (Vatican City State)</asp:ListItem>
                <asp:ListItem>Honduras</asp:ListItem>
                <asp:ListItem>Hong Kong</asp:ListItem>
                <asp:ListItem>Hungary</asp:ListItem>
                <asp:ListItem>Icel And</asp:ListItem>
                <asp:ListItem Selected="True">India</asp:ListItem>
                <asp:ListItem>Indonesia</asp:ListItem>
                <asp:ListItem>Iran (Islamic Republic Of)</asp:ListItem>
                <asp:ListItem>Iraq</asp:ListItem>
                <asp:ListItem>Ireland</asp:ListItem>
                <asp:ListItem>Israel</asp:ListItem>
                <asp:ListItem>Italy</asp:ListItem>
                <asp:ListItem>Jamaica</asp:ListItem>
                <asp:ListItem>Japan</asp:ListItem>
                <asp:ListItem>Jordan</asp:ListItem>
                <asp:ListItem>Kazakhstan</asp:ListItem>
                <asp:ListItem>Kenya</asp:ListItem>
                <asp:ListItem>Kiribati</asp:ListItem>
                <asp:ListItem>Korea, Dem People'S Republic</asp:ListItem>
                <asp:ListItem>Korea, Republic Of</asp:ListItem>
                <asp:ListItem>Kuwait</asp:ListItem>
                <asp:ListItem>Kyrgyzstan</asp:ListItem>
                <asp:ListItem>Lao People'S Dem Republic</asp:ListItem>
                <asp:ListItem>Latvia</asp:ListItem>
                <asp:ListItem>Lebanon</asp:ListItem>
                <asp:ListItem>Lesotho</asp:ListItem>
                <asp:ListItem>Liberia</asp:ListItem>
                <asp:ListItem>Libyan Arab Jamahiriya</asp:ListItem>
                <asp:ListItem>Liechtenstein</asp:ListItem>
                <asp:ListItem>Lithuania</asp:ListItem>
                <asp:ListItem>Luxembourg</asp:ListItem>
                <asp:ListItem>Macau</asp:ListItem>
                <asp:ListItem>Macedonia</asp:ListItem>
                <asp:ListItem>Madagascar</asp:ListItem>
                <asp:ListItem>Malawi</asp:ListItem>
                <asp:ListItem>Malaysia</asp:ListItem>
                <asp:ListItem>Maldives</asp:ListItem>
                <asp:ListItem>Mali</asp:ListItem>
                <asp:ListItem>Malta</asp:ListItem>
                <asp:ListItem>Marshall Islands</asp:ListItem>
                <asp:ListItem>Martinique</asp:ListItem>
                <asp:ListItem>Mauritania</asp:ListItem>
                <asp:ListItem>Mauritius</asp:ListItem>
                <asp:ListItem>Mayotte</asp:ListItem>
                <asp:ListItem>Mexico</asp:ListItem>
                <asp:ListItem>Micronesia, Federated States</asp:ListItem>
                <asp:ListItem>Moldova, Republic Of</asp:ListItem>
                <asp:ListItem>Monaco</asp:ListItem>
                <asp:ListItem>Mongolia</asp:ListItem>
                <asp:ListItem>Montserrat</asp:ListItem>
                <asp:ListItem>Morocco</asp:ListItem>
                <asp:ListItem>Mozambique</asp:ListItem>
                <asp:ListItem>Myanmar</asp:ListItem>
                <asp:ListItem>Namibia</asp:ListItem>
                <asp:ListItem>Nauru</asp:ListItem>
                <asp:ListItem>Nepal</asp:ListItem>
                <asp:ListItem>Netherlands</asp:ListItem>
                <asp:ListItem>Netherlands Ant Illes</asp:ListItem>
                <asp:ListItem>New Caledonia</asp:ListItem>
                <asp:ListItem>New Zealand</asp:ListItem>
                <asp:ListItem>Nicaragua</asp:ListItem>
                <asp:ListItem>Niger</asp:ListItem>
                <asp:ListItem>Nigeria</asp:ListItem>
                <asp:ListItem>Niue</asp:ListItem>
                <asp:ListItem>Norfolk Island</asp:ListItem>
                <asp:ListItem>Northern Mariana Islands</asp:ListItem>
                <asp:ListItem>Norway</asp:ListItem>
                <asp:ListItem>Oman</asp:ListItem>
                <asp:ListItem>Pakistan</asp:ListItem>
                <asp:ListItem>Palau</asp:ListItem>
                <asp:ListItem>Panama</asp:ListItem>
                <asp:ListItem>Papua New Guinea</asp:ListItem>
                <asp:ListItem>Paraguay</asp:ListItem>
                <asp:ListItem>Peru</asp:ListItem>
                <asp:ListItem>Philippines</asp:ListItem>
                <asp:ListItem>Pitcairn</asp:ListItem>
                <asp:ListItem>Poland</asp:ListItem>
                <asp:ListItem>Portugal</asp:ListItem>
                <asp:ListItem>Puerto Rico</asp:ListItem>
                <asp:ListItem>Qatar</asp:ListItem>
                <asp:ListItem>Reunion</asp:ListItem>
                <asp:ListItem>Romania</asp:ListItem>
                <asp:ListItem>Russian Federation</asp:ListItem>
                <asp:ListItem>Rwanda</asp:ListItem>
                <asp:ListItem>Saint K Itts And Nevis</asp:ListItem>
                <asp:ListItem>Saint Lucia</asp:ListItem>
                <asp:ListItem>Saint Vincent, The Grenadines</asp:ListItem>
                <asp:ListItem>Samoa</asp:ListItem>
                <asp:ListItem>San Marino</asp:ListItem>
                <asp:ListItem>Sao Tome And Principe</asp:ListItem>
                <asp:ListItem>Saudi Arabia</asp:ListItem>
                <asp:ListItem>Senegal</asp:ListItem>
                <asp:ListItem>Seychelles</asp:ListItem>
                <asp:ListItem>Sierra Leone</asp:ListItem>
                <asp:ListItem>Singapore</asp:ListItem>
                <asp:ListItem>Slovakia (Slovak Republic)</asp:ListItem>
                <asp:ListItem>Slovenia</asp:ListItem>
                <asp:ListItem>Solomon Islands</asp:ListItem>
                <asp:ListItem>Somalia</asp:ListItem>
                <asp:ListItem>South Africa</asp:ListItem>
                <asp:ListItem>South Georgia , S Sandwich Is.</asp:ListItem>
                <asp:ListItem>Spain</asp:ListItem>
                <asp:ListItem>Sri Lanka</asp:ListItem>
                <asp:ListItem>St. Helena</asp:ListItem>
                <asp:ListItem>St. Pierre And Miquelon</asp:ListItem>
                <asp:ListItem>Sudan</asp:ListItem>
                <asp:ListItem>Suriname</asp:ListItem>
                <asp:ListItem>Svalbard, Jan Mayen Islands</asp:ListItem>
                <asp:ListItem>Sw Aziland</asp:ListItem>
                <asp:ListItem>Sweden</asp:ListItem>
                <asp:ListItem>Switzerland</asp:ListItem>
                <asp:ListItem>Syrian Arab Republic</asp:ListItem>
                <asp:ListItem>Taiwan</asp:ListItem>
                <asp:ListItem>Tajikistan</asp:ListItem>
                <asp:ListItem>Tanzania, United Republic Of</asp:ListItem>
                <asp:ListItem>Thailand</asp:ListItem>
                <asp:ListItem>Togo</asp:ListItem>
                <asp:ListItem>Tokelau</asp:ListItem>
                <asp:ListItem>Tonga</asp:ListItem>
                <asp:ListItem>Trinidad And Tobago</asp:ListItem>
                <asp:ListItem>Tunisia</asp:ListItem>
                <asp:ListItem>Turkey</asp:ListItem>
                <asp:ListItem>Turkmenistan</asp:ListItem>
                <asp:ListItem>Turks And Caicos Islands</asp:ListItem>
                <asp:ListItem>Tuvalu</asp:ListItem>
                <asp:ListItem>Uganda</asp:ListItem>
                <asp:ListItem>Ukraine</asp:ListItem>
                <asp:ListItem>United Arab Emirates</asp:ListItem>
                <asp:ListItem>United Kingdom</asp:ListItem>
                <asp:ListItem>United States</asp:ListItem>
                <asp:ListItem>United States Minor Is.</asp:ListItem>
                <asp:ListItem>Uruguay</asp:ListItem>
                <asp:ListItem>Uzbekistan</asp:ListItem>
                <asp:ListItem>Vanuatu</asp:ListItem>
                <asp:ListItem>Venezuela</asp:ListItem>
                <asp:ListItem>Viet Nam</asp:ListItem>
                <asp:ListItem>Virgin Islands (British)</asp:ListItem>
                <asp:ListItem>Virgin Islands (U.S.)</asp:ListItem>
                <asp:ListItem>Wallis And Futuna Islands</asp:ListItem>
                <asp:ListItem>Western Sahara</asp:ListItem>
                <asp:ListItem>Yemen</asp:ListItem>
                <asp:ListItem>Yugoslavia</asp:ListItem>
                <asp:ListItem>Zaire</asp:ListItem>
                <asp:ListItem>Zambia</asp:ListItem>
                <asp:ListItem>Zimbabwe</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                State</label>
            <asp:TextBox ID="txtState" runat="server" class="fl_input" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                City</label>
            <asp:TextBox ID="txtCity" runat="server" class="fl_input" ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Pincode</label>
            <asp:TextBox ID="txtPincode" runat="server" class="fl_input" onKeyPress='javascript:return onlyNumbers();' ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="Mobile">
            <label class="fl_label uppercase">
                Mobile No.</label>
            <asp:TextBox ID="txtMobileNo" runat="server" class="fl_input mob" MaxLength="10"
                onKeyPress='javascript:return onlyNumbers();' ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap">
            <label class="fl_label uppercase">
                Alternate Contact No.</label>
            <asp:TextBox ID="txtTelephoneResidence" runat="server" class="fl_input" onKeyPress='javascript:return onlyNumbers();' ReadOnly="true" Enabled="false"></asp:TextBox>
        </div>
        <div class="clear">
        </div>
        <div class="clear">
        </div>
        <div class="form_centering_div" style="display: none;">
            <asp:Button ID="btnUpdateProfile" runat="server" Text="Update Profile" OnClick="btnUpdateProfile_Click"
                OnClientClick='javascript:return CheckSave();' class="submit_btn fade_anim uppercase" />
            <asp:Label ID="lblmsg" runat="server"></asp:Label>
        </div>
    </div>
    <script type="text/javascript">
        function CheckSave() {
            var SureName = $('#<%=txtSureName.ClientID%>').val();
            var FirstName = $('#<%=txtFirstName.ClientID%>').val();
            var MiddleName = $('#<%=txtMiddleName.ClientID%>').val();
            var DoB = $('#<%=txtDob.ClientID%>').val();
            var Sex = $('#<%=rdbSex.ClientID%>').val();
            var Address = $('#<%=txtAddress.ClientID%>').val();
            var Mobile = $('#<%=txtMobileNo.ClientID%>').val();
            var blank = false;

            if (SureName == '') {
                $("#Surname").addClass("error");
                blank = true;
            }
            else {
                $("#Surname").removeClass("error");
            }

            if (FirstName == '') {
                $("#FirstName").addClass("error");
                blank = true;
            }
            else {
                $("#FirstName").removeClass("error");
            }

            if (MiddleName == '') {
                $("#MiddleName").addClass("error");
                blank = true;
            }
            else {
                $("#MiddleName").removeClass("error");
            }

            if (DoB == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (Sex == '') {
                $("#Sex").addClass("error");
                blank = true;
            }
            else {
                $("#Sex").removeClass("error");
            }

            if (Address == '') {
                $("#Address").addClass("error");
                blank = true;
            }
            else {
                $("#Address").removeClass("error");
            }

            if (Mobile == '') {
                $("#Mobile").addClass("error");
                blank = true;
            }
            else {
                if (Mobile.length < 10) {
                    $("#Mobile").addClass("error");
                    blank = true;
                }
                else {
                    $("#Mobile").removeClass("error");
                }
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

    </script>
    <script type="text/javascript">
        function SureName() {
            var SureName = $('#<%=txtSureName.ClientID%>').val();
            var blank = false;

            if (SureName == '') {
                $("#Surname").addClass("error");
                blank = true;
            }
            else {
                $("#Surname").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function FirstName() {
            var FirstName = $('#<%=txtFirstName.ClientID%>').val();
            var blank = false;

            if (FirstName == '') {
                $("#FirstName").addClass("error");
                blank = true;
            }
            else {
                $("#FirstName").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function MiddleName() {
            var MiddleName = $('#<%=txtMiddleName.ClientID%>').val();
            var blank = false;

            if (MiddleName == '') {
                $("#MiddleName").addClass("error");
                blank = true;
            }
            else {
                $("#MiddleName").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function DOB() {
            var DoB = $('#<%=txtDob.ClientID%>').val();
            var blank = false;

            if (DoB == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function Sex() {
            var Sex = $('#<%=rdbSex.ClientID%>').val();
            var blank = false;

            if (Sex == '') {
                $("#Sex").addClass("error");
                blank = true;
            }
            else {
                $("#Sex").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }


        function Address() {
            var Address = $('#<%=txtAddress.ClientID%>').val();
            var blank = false;

            if (Address == '') {
                $("#Address").addClass("error");
                blank = true;
            }
            else {
                $("#Address").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function Mobile() {
            var Mobile = $('#<%=txtMobileNo.ClientID%>').val();
            var blank = false;

            if (Mobile == '') {
                $("#Mobile").addClass("error");
                blank = true;
            }
            else {
                if (Mobile.length < 10) {
                    $("#Mobile").addClass("error");
                    blank = true;
                }
                else {
                    $("#Mobile").removeClass("error");
                }
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }

        }




        $(function () {

            /*** Sure Name ***/

            $('.surename').on('click', function () {
                SureName();
            });
            $('.surename').on('keyup', function () {
                SureName();
            });

            /***** First Name **********/
            $('.surename').on('click', function () {
                SureName();
            });
            $('.surename').on('keyup', function () {
                SureName();
            });

            /***** First Name **********/

            $('.FN').on('click', function () {
                FirstName();
            });
            $('.FN').on('keyup', function () {
                FirstName();
            });

            /***** Middle Name **********/

            $('.MN').on('click', function () {
                MiddleName();
            });
            $('.MN').on('keyup', function () {
                MiddleName();
            });

            /***** DOB **********/

            $('.dob').on('click', function () {
                DOB();
            });
            $('.dob').on('keyup', function () {
                DOB();
            });

            /***** Sex **********/

            $('.sex').on('click', function () {
                Sex();
            });
            $('.sex').on('keyup', function () {
                Sex();
            });

            /***** Address **********/

            $('.add').on('click', function () {
                Address();
            });
            $('.add').on('keyup', function () {
                Address();
            });

            /***** Mobile No **********/

            $('.mob').on('click', function () {
                Mobile();
            });
            $('.mob').on('keyup', function () {
                Mobile();
            });

        });

    </script>
    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $('#txtMobileNo').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });

        $(function () {
            $('#txtPincode').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });

        $(function () {
            $('#txtTelephoneResidence').keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });
    </script>
</asp:Content>
