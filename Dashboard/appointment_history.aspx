﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard/DashboardMaster.master" AutoEventWireup="true" CodeFile="appointment_history.aspx.cs" Inherits="Dashboard_appointment_history" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="history_table">
        <asp:Repeater ID="rptAppHistory" runat="server" OnItemCommand="rptAppHistory_ItemCommand">
            <HeaderTemplate>
                <thead>
                    <tr>
                        <%--<td>
                            <asp:LinkButton ID="lnkSrNo" runat="server" CommandName="App_Status" style="color:#fff;">Sr. No.</asp:LinkButton>
                        </td>--%>
                        <%--<td>
                            <asp:LinkButton ID="lnkSpeciality" runat="server" CommandName="Speciality_vcr" style="color:#fff;text-decoration:underline;">Speciality</asp:LinkButton>
                        </td>--%>
                        <td>Sr. No.</td>
                        <td>Speciality</td>
                        <td>Doctor's Name</td>
                        <td>Available Time</td>
                        <td>Date of Appointment</td>
                        <td>Preferred Time</td>
                        <td>Status</td>
                        <%--<td><asp:LinkButton ID="lnkAppStatus" runat="server" CommandName="App_Status" style="color:#fff;text-decoration:underline;">App. Status</asp:LinkButton> </td>--%>
                        <td style="display:none;"></td>
                    </tr>
                </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tbody>
                    <tr>
                        <td data-title="Sr. No."><%#Container.ItemIndex+1 %> </td>
                        <%--<td data-title="Sr. No."><%#Eval("App_Id_bint") %> </td>--%>
                        <td data-title="Speciality"><%#Eval("Speciality_vcr") %></td>
                        <td data-title="Doctor's Name"><%#Eval("Doctor_Name_vcr") %> </td>
                        <td data-title="Available Time"><%#Eval("App_Avai_Time") %> </td>
                        <td data-title="Date"><%#Eval("App_Date") %> </td>
                        <td data-title="Preferred Time"><%#Eval("App_Visit_Time") %> </td>
                        <td data-title="Status" id="status" style='<%#Eval("App_Status").ToString()=="Cancelled"?"color:#A25020": ""%>; <%#Eval("App_Status").ToString()=="Closed"?"color:#F44336": ""%>; <%#Eval("App_Status").ToString()=="Confirmed"?"color:#04869A": ""%>'><%#Eval("App_Status") %> </td>
                        <td align="center" style="display:none;">
                            <div>
                                <a href='book_appointment.aspx?<%#Eval("App_Id_bint","AppId={0}") %>' id="<%#Eval("App_Status")%>" class="fa fa-edit" style='<%#Eval("App_Status").ToString()=="Cancelled"?"color:#ccc": ""%>; <%#Eval("App_Status").ToString()=="Closed"?"color:#ccc": ""%>' title="Edit"></a>
                                <a id="<%#Eval("App_Id_bint") %>" title="<%#Eval("App_Status")%>" onclick="cancelappointment(<%#Eval("App_Id_bint") %>)" class="fa fa-times" style='<%#Eval("App_Status").ToString()=="Cancelled"?"color:#ccc": ""%>; <%#Eval("App_Status").ToString()=="Closed"?"color:#ccc": ""%>' title="Cancel"></a>

                                <%-- <asp:LinkButton ID="btncancel"   runat="server" title="Cancel" class="fa fa-times" CommandArgument='<%#Eval("App_Id_bint") %>' OnClientClick="if (!confirm('Are you sure do you want to cancel appointment?')) return false;"></asp:LinkButton>--%>
                            </div>
                        </td>
                    </tr>                                     
                </tbody>
            </ItemTemplate>
        </asp:Repeater>
    </table>

    <script type="text/javascript">
        var arr = [];

        $("td").each(function () {

            arr.push($(this).text());
        });

        $.each(arr, function (index, value) {

            //alert(arr[index]);
            var data = arr[index];


            if (data == "Cancelled ") {



            }
        });

        $('a.fa-edit').click(function (e) {

            var id = $(this).attr('id');

            if (id == "Cancelled" || id == "Closed") {
                //this.style.color = "#ccc";
                e.preventDefault();
            }

        });



        function cancelappointment(apid) {
            var status = document.getElementById(apid).title;
            if (status == "Cancelled" || status == "Closed") {

                apid.preventDefault();
            }
            var r = confirm("Are you sure do you want to cancel appointment?'");
            if (r == true) {
                $.ajax({
                    type: "POST",
                    url: "appointment_history.aspx/CancelAppointment",
                    data: '{id: "' + apid + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        // alert(response.d);
                    }
                });
            } else {

            }
        }

        function OnSuccess(response) {
            //alert(response.d);
            location.reload();
        }

    </script>

    <style>
        .hidediv a::before {
            color: #ccc;
        }
    </style>

</asp:Content>

