﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard/DashboardMaster.master"
    AutoEventWireup="true" CodeFile="book_appointment.aspx.cs" Inherits="Dashboard_book_appointment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../select2/select2.min.js"></script>
    <link href="../select2/select2.min.css" rel="stylesheet" />
    <%--<link href="../css/select2.min.css" rel="stylesheet" />
    <script src="../js/select2.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-example-basic-single").select2();

            $(".js-example-basic-hide-search").select2({
                minimumResultsForSearch: Infinity
            });



            $('#spl').click(function () {
                var $example = $('#<%=ddlSpeciality.ClientID%>').select2();
                $example.select2("open");
            });

            $('#docname').click(function () {
                var $example = $('#<%=ddldrName.ClientID%>').select2();
                $example.select2("open");
            });

            $('#avltime').click(function () {
                var $example = $('#<%=ddlAvailableTime.ClientID%>').select2();
                $example.select2("open");
            });
            $('#reffertime').click(function () {
                var $example = $('#<%=ddlVisitTime.ClientID%>').select2();
                $example.select2("open");
            });



            $('#<%=ddlVisitTime.ClientID%>').change(function () {
                var data = $(".availtm").val();
                if (data.length != 0 || data != "") {
                    $("#slottimebid").removeClass("error");
                    $("#slottimebid").addClass("populated");
                }
                else {
                    $("#slottimebid").addClass("error");
                }
            });

            $('#<%=ddlAvailableTime.ClientID%>').change(function () {
                var data = $(".availtm").val();
                if (data.length != 0 || data != "") {
                    $("#AvailTm").removeClass("error");
                    $("#AvailTm").addClass("populated");
                }
                else {
                    $("#AvailTm").addClass("error");
                }
            });



        });


        function EnterEvent(e) {
            e.preventDefault();
        }


    </script>
    <%--<link href="../css/AppointmentDropdowncss.css" rel="stylesheet" type="text/css" />
    <script src="../js/dropdowndashboard.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {


            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "book_appointment.aspx/GetAutoCompleteData",
                        data: "{'speciality':'" + $('#<%=txtSpeciality.ClientID %>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                }
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#<%=ddldrName.ClientID %>').on("click", function () {
                var items = $("#ddldrName option").length;

                //                console.log(items);

                var speciality = $('#<%=txtSpeciality.ClientID %>').val();

                if (items == "1" && speciality != '') {
                    console.log("if" + items);

                    __doPostBack('<%# txtSpeciality.ClientID %>', 'OnTextChanged')

                }
                else {
                    console.log("else");
                    return false;
                }
            })

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#txtSpeciality").on('keyup', function () {
                var length = $("#txtSpeciality").val().length;
                var dropdown = $("#ddldrName").val();
                //alert(length);


                /*set value for insert record in dropdown*/
                var newOption = "<option value='" + "" + "'></option>";
                if (length == "0") {
                    console.log("length is a:" + length + "   if condition disabled null");

                    /*empty dropdown list*/
                    $('#ddldrName').empty();

                    /*insert first default value in dropdown list*/
                    $("#ddldrName").append(newOption);



                    /*empty dropdown list*/
                    $('#ddlVisitTime').empty();

                    /*insert first default value in dropdown list*/
                    // $("#ddlVisitTime").append(newOption);

                    //  alert("working");

                }
                else {
                    console.log("length is a:" + length + "   else condition");

                    //$("#ddldrName").attr('disabled', 'disabled');
                }
            });
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="display: none;">
        <asp:Literal ID="ltrlpatientinvoice" runat="server"></asp:Literal>
    </div>
    <div class="form_div FlowupLabels">
        <div class="form_field fl_wrap single_width" id="divMsg" runat="server" visible="false">
            <div class="validate_msg">
                <asp:Label ID="lblmsg" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_field fl_wrap" id="Specility">
            <label class="fl_label uppercase" id="spl">
                Speciality</label>
            <%--<select class="fl_input">
                <option></option>
                <option>Male</option>
                <option>Female</option>
            </select>--%>
            <asp:TextBox ID="txtSpeciality" runat="server" class="autosuggest fl_input" ClientIDMode="Static"
                OnTextChanged="txtSpeciality_TextChanged" Visible="false">
            </asp:TextBox>
            <asp:DropDownList ID="ddlSpeciality" runat="server" class="js-example-basic-single fl_input sp"
                OnSelectedIndexChanged="ddlSpeciality_SelectedIndexChanged" AutoPostBack="true"
                Visible="true">
            </asp:DropDownList>
        </div>
        <div class="form_field fl_wrap" id="docName">
            <label class="fl_label uppercase" id="docname">
                Name of Doctor</label>
            <%--<select class="fl_input">
                <option></option>
                <option>Male</option>
                <option>Female</option>
            </select>--%>
            <asp:DropDownList ID="ddldrName" runat="server" class="js-example-basic-single fl_input doc"
                OnSelectedIndexChanged="ddldrName_SelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static">
            </asp:DropDownList>
        </div>
        <div class="clear">
        </div>
        <div class="form_field fl_wrap" id="AvailTm">
            <label class="fl_label uppercase" id="avltime">
                Available Timing</label>
            <%--<select class="fl_input">
                <option></option>
                <option>Male</option>
                <option>Female</option>
            </select>--%>
            <asp:DropDownList ID="ddlAvailableTime" runat="server" class="js-example-basic-hide-search fl_input availtm"
                OnSelectedIndexChanged="ddlslottime_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="form_field fl_wrap" id="dob">
            <label class="fl_label uppercase" for="doa">
                Date of Appointment</label>
            <%--<input class="fl_input" type="text" id="doa" />--%>
            <asp:TextBox ID="txtDate" runat="server" class="fl_input doa" onkeypress="return EnterEvent(event)"
                autocomplete="off" OnTextChanged="txtDate_TextChanged" AutoPostBack="true"></asp:TextBox>
        </div>
        <div class="form_field fl_wrap" id="slottimebid">
            <label class="fl_label uppercase" id="reffertime">
                Preferred Time</label>
            <%--<input class="fl_input" type="text" />--%>
            <asp:TextBox ID="txtVisitTime" runat="server" class="fl_input" Visible="false"></asp:TextBox>
            <asp:DropDownList ID="ddlVisitTime" runat="server" class="js-example-basic-hide-search fl_input availtm">
            </asp:DropDownList>
        </div>
        <div class="clear" style="height: 10px;">
        </div>
        <div class="form_centering_div">
            <%--<input class="submit_btn fade_anim uppercase" type="submit" value="Fix the Appointment" />--%>
            <asp:Button ID="btnBookAppointment" runat="server" Text="Fix the Appointment" OnClick="btnBookAppointment_Click"
                OnClientClick='javascript:return CheckSave();' class="submit_btn fade_anim uppercase" />
            &nbsp;<asp:Button ID="btnreselect" runat="server" Text="Re-Select" class="submit_btn fade_anim uppercase"
                OnClick="btnreselect_Click" />
        </div>
    </div>
    <%--<script>
        $(function () {
            $(".doa").datepicker({
                minDate: 1,
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'dd-mm-yy',
            });

            $(".doa").change(function () {
                var data = $(".doa").val();
                if (data.length != 0 || data != "") {
                    $("#dob").removeClass("error");
                    $("#dob").addClass("populated");
                }
                else {
                    $("#dob").addClass("error");
                }
            });

        });
    </script>--%>
    <script type="text/javascript">

        var selectedday;
        var ddAvailTime = "";
        $(document).ready(function () {

            $('#<%=ddlAvailableTime.ClientID%>').on("change", function () {

                $('#<%=ddlVisitTime.ClientID%>').empty();

                ddAvailTime = $('#<%=ddlAvailableTime.ClientID%> option:selected').text();

                $('#<%=txtDate.ClientID%>').val("");
                $('#dob').removeClass("populated");

                var OnlyThreeChar = ddAvailTime.substring(0, 3);

                var arr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

                if (arr[1] == OnlyThreeChar) {
                    selectedday = 1;
                }
                else if (arr[2] == OnlyThreeChar) {
                    selectedday = 2;
                }
                else if (arr[3] == OnlyThreeChar) {
                    selectedday = 3;
                }
                else if (arr[4] == OnlyThreeChar) {
                    selectedday = 4;
                }
                else if (arr[5] == OnlyThreeChar) {
                    selectedday = 5;
                }
                else if (arr[6] == OnlyThreeChar) {
                    selectedday = 6;
                }
                else if (OnlyThreeChar == "") {
                    selectedday = 8;
                    $('#AvailTm').removeClass("populated");
                }
                // console.log("values : " + ddAvailTime + " Only Three char : " + OnlyThreeChar + "Array : " + arr[0]);

            });

            /** On Page Load **/

            ddAvailTime = $('#<%=ddlAvailableTime.ClientID%> option:selected').text();
            var OnlyThreeChar = ddAvailTime.substring(0, 3);
            var arr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

            if (arr[1] == OnlyThreeChar) {
                selectedday = 1;
            }
            else if (arr[2] == OnlyThreeChar) {
                selectedday = 2;
            }
            else if (arr[3] == OnlyThreeChar) {
                selectedday = 3;
            }
            else if (arr[4] == OnlyThreeChar) {
                selectedday = 4;
            }
            else if (arr[5] == OnlyThreeChar) {
                selectedday = 5;
            }
            else if (arr[6] == OnlyThreeChar) {
                selectedday = 6;
            }
            //console.log("load " + selectedday + " ava : " + ddAvailTime.length + " last three char : " + OnlyThreeChar + " final : " + selectedday);

            /** End **/

        });

        $(function () {

            /** Days to be disabled as an array */
            // var disableddates = ["8-15-2016", "8-25-2016", "9-5-2016", "9-15-2016", "10-11-2016", "11-1-2016", "1-26-2017"];
            var disableddates = "";

            $(document).ready(function () {
                $.ajax({
                    type: "POST",
                    url: "/Dashboard/book_appointment.aspx/GetTimeForBind",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        // alert(response.d);
                        // console.log(response.d);

                        var xmlDoc = $.parseXML(response.d);
                        var xml = $(xmlDoc);
                        // var alldate = xml.find("Table1");

                        // alert(customers);
                        // console.log(alldate);
                        var check = "";
                        var getdisabledate = [];


                        xml.find("Table1").each(function () {

                            // check += '"' + $(this).find("NOTAVAILBLEDATE").text().replace("/", "-").replace("/", "-") + '"' + ',';
                            check = $(this).find("NOTAVAILBLEDATE").text().replace("/", "-").replace("/", "-");
                            getdisabledate.push(check);

                        });
                        // var finaldate = check.substring(0, check.length - 1);

                        //  finaldate = '[' + finaldate + ']';
                        //finaldate = finaldate;
                        console.log(getdisabledate);
                        if (getdisabledate != '') {
                            //console.log(disableddates + "if");
                            disableddates = getdisabledate;
                        }
                        else {
                            disableddates = ["8-15-2016"];
                            // console.log(disableddates + "else");

                        }



                        //check = check.substring(0, check, length - 1);
                        //console.log(idcontainer);

                    }
                });
            });


            function DisableSpecificDates(date) {

                var day = date.getDay();

                var m = date.getMonth();
                var d = date.getDate();
                var y = date.getFullYear();

                // First convert the date in to the mm-dd-yyyy format 
                // Take note that we will increment the month count by 1 
                var currentdate = (m + 1) + '-' + d + '-' + y;

                //console.log(currentdate + " mo :" + m + " disa : " + disableddates);

                // We will now check if the date belongs to disableddates array 
                for (var i = 0; i < disableddates.length; i++) {
                    // Now check if the current date is in disabled dates array. 
                    if ($.inArray(currentdate, disableddates) != -1 || day != selectedday) {
                        return [false];
                    } else {
                        return [true];
                    }
                }
            }

            function DisableMonday(date) {
                var day = date.getDay();
                // If day == 1 then it is MOnday
                if (day != selectedday) {
                    return [false];
                } else {
                    return [true];
                }
            }


            $(".doa").datepicker({
                minDate: 1,
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: 'dd-mm-yy',
                maxDate: 60,
                beforeShowDay: DisableSpecificDates
                //beforeShowDay: $.datepicker.noWeekends,
                // beforeShowDay: DisableMonday

            });

            $(".doa").change(function () {
                var data = $(".doa").val();
                if (data.length != 0 || data != "") {
                    $("#dob").removeClass("error");
                    $("#dob").addClass("populated");
                    //__doPostBack('<%# ID=ddlAvailableTime.ClientID %>', ' OnSelectedIndexChanged')
                    // alert("xsxsbjsxbs");
                }
                else {
                    $("#dob").addClass("error");
                }
            });

            $('#<%=ddlSpeciality.ClientID%>').change(function () {
                $('#<%=txtDate.ClientID%>').val("");
                $('#<%=txtVisitTime.ClientID%>').val("");
            });

            $('#<%=ddldrName.ClientID%>').change(function () {
                $('#<%=txtDate.ClientID%>').val("");
                $('#<%=txtVisitTime.ClientID%>').val("");
            });

        });

    </script>
    <script type="text/javascript">
        function CheckSave() {
            //            var Speciality = $('#<%=txtSpeciality.ClientID%>').val();
            var Speciality = $('#<%=ddlSpeciality.ClientID%>').val();

            var DoctorName = $('#<%=ddldrName.ClientID%>').val();
            var AppDate = $('#<%=txtDate.ClientID%>').val();
            var AvaiTime = $('#<%=ddlAvailableTime.ClientID%>').val();
            var Slottime = $('#<%=ddlVisitTime.ClientID%>').val();

            // alert(Slottime);
            var blank = false;

            if (Speciality == '') {
                $("#Specility").addClass("error");
                blank = true;
            }
            else {
                $("#Specility").removeClass("error");
            }

            if (DoctorName == '') {
                $("#docName").addClass("error");
                blank = true;
            }
            else {
                $("#docName").removeClass("error");
            }

            if (AppDate == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (AvaiTime == '') {
                $("#AvailTm").addClass("error");
                blank = true;

            }
            else {
                $("#AvailTm").removeClass("error");
            }

            if (Slottime == '') {
                $("#slottimebid").addClass("error");
                blank = true;

            }
            else {
                $("#slottimebid").removeClass("error");
            }


            if (blank) {
                return false;
            }
            else {
                return true;
            }

        }

    </script>
    <script type="text/javascript">
        function SpecialityFun() {
            var Speciality = $('#<%=ddlSpeciality.ClientID%>').val();
            var blank = false;

            if (Speciality == '') {
                $("#Specility").addClass("error");
                blank = true;
            }
            else {
                $("#Specility").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function DocFun() {
            var DoctorName = $('#<%=ddldrName.ClientID%>').val();
            var blank = false;

            if (DoctorName == '') {
                $("#docName").addClass("error");
                blank = true;
            }
            else {
                $("#docName").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function AppDateFun() {
            var AppDate = $('#<%=txtDate.ClientID%>').val();
            var blank = false;

            if (AppDate == '') {
                $("#dob").addClass("error");
                blank = true;
            }
            else {
                $("#dob").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        function AvailableTimeFun() {
            var AvaiTime = $('#<%=ddlAvailableTime.ClientID%>').val();
            var blank = false;

            if (AvaiTime == '') {
                $("#AvailTm").addClass("error");
                blank = true;

            }
            else {
                $("#AvailTm").removeClass("error");
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        /****  Speciality  *****/

        $('.sp').click(function () {
            SpecialityFun();
        });

        $('.sp').change(function () {
            SpecialityFun();
        });

        $('.sp').keypress(function () {
            SpecialityFun();
        });

        /****  Doctor  *****/

        $('.doc').click(function () {
            DocFun();
        });

        $('.doc').change(function () {
            DocFun();
        });

        $('.doc').keypress(function () {
            DocFun();
        });

        /****  Appointment Date  *****/

        $('.doa').click(function () {
            AppDateFun();
        });

        $('.doa').change(function () {
            AppDateFun();
        });

        $('.doa').keypress(function () {
            AppDateFun();
        });


        /****  Available Timing  *****/

        $('.availtm').click(function () {
            AvailableTimeFun();
        });

        $('.availtm').change(function () {
            AvailableTimeFun();
        });

        $('.availtm').keypress(function () {
            AvailableTimeFun();
        });



    </script>
    <style>
        .availa {
            display: none;
        }
    </style>
</asp:Content>
