﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_update_profile : UserLogin
{
    RegistrationTable tbl = new RegistrationTable();
    BAL bal = new BAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetUpdateDetail();
        }
    }

    public void GetUpdateDetail()
    {
        bal.GetUpdateProfileDetailes(tbl);

        txtUserID.Text = tbl.UserName;
        txtSureName.Text = tbl.SureName;
        txtFirstName.Text = tbl.FirstName;
        txtMiddleName.Text = tbl.MiddleName;
        rdbSex.SelectedValue = tbl.Sex;
        ddlMaritalStatus.SelectedValue = tbl.MaritalStatus;
        ddlReligion.SelectedValue = tbl.Religion;
        txtOccupation.Text = tbl.Occupation;
        txtTelephoneResidence.Text = tbl.TelephoneResidence;
        txtAddress.Text = tbl.Address;
        txtMobileNo.Text = tbl.MobileNo;
        ddlCountry.SelectedValue = tbl.Country;
        txtState.Text = tbl.State;
        txtCity.Text = tbl.City;
        txtPincode.Text = tbl.Pincode;
        txtEmail.Text = tbl.EmailID;
        txtDob.Text = tbl.DOB;
        txtTitle.Text = tbl.Title;
        txtBloodGroup.Text = tbl.BloodGroup;

        /*** DOB ***
        if (tbl.DOB != null)
        {
            string[] separators = { ",", ".", "!", "?", ";", ":", " ", "/", "-" };
            string value = tbl.DOB;
            string[] words = value.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            string day = words[0];
            string month = words[1];
            string year = words[2];
                        

            //txtDob.Text = day + "-" + month + "-" + year;

            /*****    live server getting error on DOB *****

            if (month.Length < 1)
            {
                month = 0 + month;
            }
            else if (day.Length < 1)
            {
                day = 0 + day;
            }
            txtDob.Text = month + "-" + day + "-" + year;         
                        
           
            /*** End ***

        }
        else
        {

        }

        /*** End ****/
    }

    protected void btnUpdateProfile_Click(object sender, EventArgs e)
    {

        try
        {
            tbl.SureName = txtSureName.Text;
            tbl.FirstName = txtFirstName.Text;
            tbl.MiddleName = txtMiddleName.Text;
            tbl.Sex = rdbSex.SelectedValue;
            //tbl.DOB = ddlYear.SelectedValue + "-" + ddlMonth.SelectedValue + "-" + ddlDay.SelectedValue;
            tbl.MaritalStatus = ddlMaritalStatus.SelectedValue;
            tbl.Religion = ddlReligion.SelectedValue;
            tbl.Occupation = txtOccupation.Text;
            tbl.TelephoneResidence = txtTelephoneResidence.Text;
            tbl.Address = txtAddress.Text;
            tbl.MobileNo = txtMobileNo.Text;            
            string date = DateTime.ParseExact(txtDob.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
            tbl.DOB = date;
            tbl.Country = ddlCountry.SelectedValue;
            tbl.State = txtState.Text;
            tbl.City = txtCity.Text;
            tbl.Pincode = txtPincode.Text;

            bal.UpdateProfile(tbl);

            if (Session["update"] == "Success")
            {
                lblmsg.ForeColor = System.Drawing.Color.Green;
                lblmsg.Text = "Update Successfully";
            }
            else
            {
                lblmsg.ForeColor = System.Drawing.Color.Red;
                lblmsg.Text = "Could Not Update Successfully";
            }
        }
        catch (Exception)
        {

            //throw;
        }
    }

}