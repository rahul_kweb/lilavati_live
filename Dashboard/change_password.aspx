﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard/DashboardMaster.master"
    AutoEventWireup="true" CodeFile="change_password.aspx.cs" Inherits="Dashboard_Welcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function CheckSave() {
            var oldPwd = $('#<%=txtOldPassword.ClientID%>').val();
            var NewPwd = $('#<%=txtPassword.ClientID%>').val();
            var ConfPwd = $('#<%=txtConfirmPassword.ClientID%>').val();
            var blank = false;

            if (oldPwd == '') {
                alert("Old Password is required.");
                $("#Old").addClass("error");
                blank = true;
            } else {
                $("#Old").removeClass("error");
            }

            if (NewPwd == '') {
                alert("New Password is required.");
                $("#newPwd").addClass("error");
                blank = true;
            } else {
                if (NewPwd.length < 6) {
                    alert("Your new password must be at least 6 characters. Please try again.");
                    $("#newPwd").addClass("error");
                    blank = true;
                } else {
                    $("#newPwd").removeClass("error");
                }
            }

            if (ConfPwd == '') {
                alert("Confirm Password is required.");
                $("#confPwd").addClass("error");
                blank = true;
            } else {
                if (ConfPwd != NewPwd) {
                    alert("Please enter same password as above.");
                    $("#confPwd").addClass("error");
                    blank = true;
                } else {
                    $("#confPwd").removeClass("error");
                }
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="pnlChangePassword" runat="server">
        <div class="form_div FlowupLabels">
            <div class="form_field fl_wrap single_width" id="divUsername" runat="server" visible="false">
                <div class="validate_msg">
                    <asp:Label ID="lblmsg" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_field fl_wrap single_width" id="Old">
                <label class="fl_label uppercase">
                    Old Password</label>
                <asp:TextBox ID="txtOldPassword" runat="server" class="fl_input" TextMode="Password"></asp:TextBox>
            </div>
            <div class="form_field fl_wrap single_width" id="newPwd">
                <label class="fl_label uppercase">
                    New Password</label>
                <asp:TextBox ID="txtPassword" runat="server" class="fl_input" TextMode="Password"></asp:TextBox>
            </div>
            <div class="form_field fl_wrap single_width" id="confPwd">
                <label class="fl_label uppercase">
                    Confirm Password</label>
                <asp:TextBox ID="txtConfirmPassword" runat="server" class="fl_input" TextMode="Password"></asp:TextBox>
            </div>
            <div class="clear">
            </div>
            <div class="form_centering_div" style="margin-bottom: 10px;">
                <asp:Button ID="btnUpdatePassword" runat="server" Text="Change Password" OnClientClick='javascript:return CheckSave()'
                    class="submit_btn fade_anim uppercase" OnClick="btnUpdatePassword_Click" />
            </div>
        </div>
    </asp:Panel>    
</asp:Content>
