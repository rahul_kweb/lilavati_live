﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Services;
using CCA.Util;
using System.Text;
using System.IO;

using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

public partial class Dashboard_book_appointment : UserLogin
{
    BAL bal = new BAL();
    Utility utility = new Utility();
    SpecialityTable spltbl = new SpecialityTable();
    DoctorMasterTable doctbl = new DoctorMasterTable();
    AppointmentTable apptbl = new AppointmentTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        DisableUnwantedAvaTiminig();
        if (!IsPostBack)
        {
            #region Payment process

            //Response.Cookies["checkpaymentprocess"].Value = "";
            if (Request.Cookies["checkpaymentprocess"] != null)
            {

                if (Request.Cookies["checkpaymentprocess"].Value != "")
                {
                    Response.Cookies["checkpaymentprocess"].Expires = DateTime.Now.AddDays(-1);

                    #region Live credential
                    string workingKey = "6C308E27F7EE5C56BD2B579DA151F471";//put in the 32bit alpha numeric key in the quotes provided here
                    #endregion

                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);

                    //NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');


                    #region Get Status (Getting problem when using if else condition overhere )


                    string checkstatus = segments[3];

                    string[] parts = checkstatus.Split('=');

                    string getmsg = "";

                    foreach (string parts1 in parts)
                    {

                        getmsg = parts1.ToString();
                    }


                    #endregion

                    #region Store all array Record in session

                    string orderid = segments[0];
                    string[] orderids = orderid.Split('=');
                    foreach (string orderid1 in orderids)
                    {
                        Session["order_id"] = orderid1;
                    }

                    string trackingid = segments[1];
                    string[] trackingids = trackingid.Split('=');
                    foreach (string trackingid1 in trackingids)
                    {
                        Session["tracking_id"] = trackingid1;
                    }

                    string bankrefno = segments[2];
                    string[] bankrefnos = bankrefno.Split('=');
                    foreach (string bankrefno1 in bankrefnos)
                    {
                        Session["bank_ref_no"] = bankrefno1;
                    }

                    string orderstatus = segments[3];
                    string[] orderstatuss = orderstatus.Split('=');
                    foreach (string orderstatus1 in orderstatuss)
                    {
                        Session["order_status"] = orderstatus1;
                    }

                    string paymentmode = segments[5];
                    string[] paymentmodes = paymentmode.Split('=');
                    foreach (string paymentmode1 in paymentmodes)
                    {
                        Session["payment_mode"] = paymentmode1;
                    }

                    string cardname = segments[6];
                    string[] cardnames = cardname.Split('=');
                    foreach (string cardname1 in cardnames)
                    {
                        Session["card_name"] = cardname1;
                    }

                    string amount = segments[10];
                    string[] amounts = amount.Split('=');
                    foreach (string amount1 in amounts)
                    {
                        Session["amount"] = amount1;
                    }

                    string currency = segments[9];
                    string[] currencys = currency.Split('=');
                    foreach (string currency1 in currencys)
                    {
                        Session["currency"] = currency1;
                    }

                    #endregion

                    GetStatus(getmsg);
                }
            }

            #endregion


            if (Session["appointmentstart"] == null)
            {
                string AppId = Request.QueryString["AppId"];
                BindSpecility();
                btnreselect.Visible = false;
                ddlSpeciality.Items.Insert(0, "");
                ddldrName.Items.Insert(0, "");
                ddlAvailableTime.Items.Insert(0, "");

                ddlVisitTime.Items.Insert(0, "");

                //ddlSpeciality.Items.Insert(0, "--Select Speciality--");
                //ddldrName.Items.Insert(0, "--Select Doctor--");
                //ddlAvailableTime.Items.Insert(0, "--Timing of Doctor--");

                if (AppId != null)
                {
                    GetAppHistoryDetail(AppId);
                }
                else
                {

                }
            }
            else
            {

                btnreselect.Visible = true;
                BindFrontAppointment();
            }

        }
    }

    public void BindSpecility()
    {
        ddlSpeciality.DataSource = bal.Speciality();
        ddlSpeciality.DataValueField = "Speciality_Id_bint";
        ddlSpeciality.DataTextField = "Speciality_vcr";
        ddlSpeciality.DataBind();

        ddlAvailableTime.Items.Clear();
        ddlAvailableTime.Items.Insert(0, "");
    }
    protected void ddlSpeciality_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSpeciality.SelectedIndex > 0)
        {
            spltbl.Speciality_Id_bint = Convert.ToInt32(ddlSpeciality.SelectedValue);
            ddldrName.DataSource = bal.SpecilityDoc(spltbl);
            ddldrName.DataValueField = "Doctor_Id_bint";
            ddldrName.DataTextField = "Doctor_Name_vcr";
            ddldrName.DataBind();

            ddldrName.Items.Insert(0, "");


            ddlAvailableTime.Items.Clear();
            ddlAvailableTime.Items.Insert(0, "");

            ddlVisitTime.Items.Clear();
            ddlVisitTime.Items.Insert(0, "");


            //ddldrName.Items.Insert(0, "--Select Doctor--");



            //spltbl.Speciality_Id_bint = getid(txtSpeciality.Text.Trim());
            //ddldrName.DataSource = bal.SpecilityDoc(spltbl);
            //ddldrName.DataValueField = "Doctor_Id_bint";
            //ddldrName.DataTextField = "Doctor_Name_vcr";
            //ddldrName.DataBind();

            //ddldrName.Items.Insert(0, "");
        }
        else
        {
            ddldrName.Items.Clear();
            ddlAvailableTime.Items.Clear();
            ddlVisitTime.Items.Clear();
        }
    }
    protected void ddldrName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddldrName.SelectedIndex > 0)
        {

            doctbl.Doctor_Id_bint = Convert.ToInt32(ddldrName.SelectedValue);
            ddlAvailableTime.DataSource = bal.AvailableTime(doctbl);
            ddlAvailableTime.DataValueField = "Day";
            ddlAvailableTime.DataTextField = "Avail_Time";
            ddlAvailableTime.DataBind();

            #region get dr consid by doctor id
            DataTable dt = new DataTable();
            dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_DRID',0,'" + doctbl.Doctor_Id_bint + "'");
            if (dt.Rows.Count > 0)
            {
                Session["doctorconsdocid"] = dt.Rows[0]["CONSDOCID"].ToString();

                Response.Cookies["DoctorIdconsid"].Value = dt.Rows[0]["CONSDOCID"].ToString();
                Response.Cookies["DoctorIdconsid"].Expires = DateTime.Now.AddDays(1);
            }
            #endregion

            foreach (System.Web.UI.WebControls.ListItem li in ddlAvailableTime.Items)
            {
                string len = li.Text.Length.ToString();
                int lent = Convert.ToInt32(li.Text.Length.ToString());

                if (li.Text.Contains("--") || li.Text.Contains("N.A") || li.Text == "" || len == "6" || len == "9" || len == "13" || lent <= 15)
                {
                    li.Attributes.Add("disabled", "true");
                    //li.Attributes.Add("class", "availa");
                }
            }

            ddlAvailableTime.Items.Insert(0, "");

            ddlVisitTime.Items.Clear();
            ddlVisitTime.Items.Insert(0, "");
        }
        else
        {
            ddlAvailableTime.Items.Clear();
            ddlVisitTime.Items.Clear();

        }
    }

    protected void btnBookAppointment_Click(object sender, EventArgs e)
    {
        if (btnBookAppointment.Text == "Fix the Appointment")
        {
            SaveAppointment();
        }
        else
        {
            UpdateAppHistory();
        }
    }

    public void SaveAppointment()
    {
        //apptbl.Speciality_Id_bint = int.Parse(ddlSpeciality.SelectedValue.ToString());

        try
        {
            //Session["Speciality_Id_bint"] = ddlSpeciality.SelectedValue.ToString();
            Response.Cookies["Speciality_Id_bint"].Value = ddlSpeciality.SelectedValue.ToString();
            Response.Cookies["Speciality_Id_bint"].Expires = DateTime.Now.AddDays(1);

            // Session["Doctor_Id_bint"] = ddldrName.SelectedValue.ToString();
            Response.Cookies["Doctor_Id_bint"].Value = ddldrName.SelectedValue.ToString();
            Response.Cookies["Doctor_Id_bint"].Expires = DateTime.Now.AddDays(1);

            // Session["App_Time_vcr"] = ddlAvailableTime.SelectedItem.Text.Trim();
            Response.Cookies["App_Time_vcr"].Value = ddlAvailableTime.SelectedItem.Text.Trim();
            Response.Cookies["App_Time_vcr"].Expires = DateTime.Now.AddDays(1);

            //Session["App_Visit_Time"] = ddlVisitTime.SelectedItem.Text;
            Response.Cookies["App_Visit_Time"].Value = ddlVisitTime.SelectedItem.Text;
            Response.Cookies["App_Visit_Time"].Expires = DateTime.Now.AddDays(1);

            //Session["App_date_dtm"] = txtDate.Text;
            Response.Cookies["App_date_dtm"].Value = txtDate.Text;
            Response.Cookies["App_date_dtm"].Expires = DateTime.Now.AddDays(1);

            // Session["patientsId"] = HttpContext.Current.Session["AddressIndexid"].ToString();
            Response.Cookies["patientsId"].Value = HttpContext.Current.Session["AddressIndexid"].ToString();
            Response.Cookies["patientsId"].Expires = DateTime.Now.AddDays(1);

            //Session["Speciality"] = ddlSpeciality.SelectedItem.Text.ToString();
            Response.Cookies["Speciality"].Value = ddlSpeciality.SelectedItem.Text.ToString();
            Response.Cookies["Speciality"].Expires = DateTime.Now.AddDays(1);

            //Session["Doctors"] = ddldrName.SelectedItem.Text.ToString();
            Response.Cookies["Doctors"].Value = ddldrName.SelectedItem.Text.ToString();
            Response.Cookies["Doctors"].Expires = DateTime.Now.AddDays(1);

            Response.Cookies["DoctorsName"].Value = ddldrName.SelectedItem.Text.ToString();
            Response.Cookies["DoctorsName"].Expires = DateTime.Now.AddDays(1);

            Response.Cookies["patientsIdNew"].Value = HttpContext.Current.Session["AddressIndexid"].ToString();
            Response.Cookies["patientsIdNew"].Expires = DateTime.Now.AddDays(1);

            string appointmentdatenew = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            Response.Cookies["appointmentdate"].Value = appointmentdatenew;
            Response.Cookies["appointmentdate"].Expires = DateTime.Now.AddDays(1);

            //Response.Cookies["DoctorIdconsid"].Value = Session["doctorconsdocid"].ToString();
            //Response.Cookies["DoctorIdconsid"].Expires = DateTime.Now.AddDays(1);

            Response.Cookies["checkpaymentprocess"].Value = "Paymentprocess";
            Response.Cookies["checkpaymentprocess"].Expires = DateTime.Now.AddDays(1);


            DataTable dtnewuserdetail = new DataTable();
            dtnewuserdetail = utility.Display("Proc_PCipopRegistrationMaster 'GET_BY_ID','" + Response.Cookies["patientsId"].Value + "'");
            if (dtnewuserdetail.Rows.Count > 0)
            {
                string patientname = dtnewuserdetail.Rows[0]["FIRSTNAME"].ToString() + " " + dtnewuserdetail.Rows[0]["LASTNAME"].ToString();
                Response.Redirect("/dataFrom.htm?billing_name=" + patientname + "&&billing_address=" + dtnewuserdetail.Rows[0]["ADDRESS"].ToString() + "&&billing_city=" + dtnewuserdetail.Rows[0]["CITY"].ToString() + "&&billing_state=" + dtnewuserdetail.Rows[0]["STATE"].ToString() + "&&billing_zip=" + dtnewuserdetail.Rows[0]["PINCODE"].ToString() + "&&billing_country=" + dtnewuserdetail.Rows[0]["COUNTRY"].ToString() + "&&billing_tel=" + dtnewuserdetail.Rows[0]["MOBILENO"].ToString() + "&&billing_email=" + dtnewuserdetail.Rows[0]["EMAIL"].ToString());
            }



            #region Commented Code and using same coding on ccabresponsehandler page

            //  apptbl.Speciality_Id_bint = int.Parse(ddlSpeciality.SelectedValue.ToString());

            // apptbl.Doctor_Id_bint = int.Parse(ddldrName.SelectedValue.ToString());
            // apptbl.App_Avai_Time = ddlAvailableTime.SelectedItem.Text;
            //apptbl.App_Visit_Time = ddlVisitTime.SelectedItem.Text;

            //string date = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
            //apptbl.App_Date = date;


            // apptbl.AddressIndex = int.Parse(HttpContext.Current.Session["AddressIndexid"].ToString());

            // apptbl._Specialityname = ddlSpeciality.SelectedItem.Text.Trim().ToString();
            //  apptbl._Doctor_Name_vcr = ddldrName.SelectedItem.Text.Trim().ToString();
            // apptbl._ConsDrId = ddldrName.SelectedValue.ToString();


            //bal.AddAppointment(apptbl);

            //if (Session["AppMsg"] == "Success")
            //{
            //    divMsg.Visible = true;
            //    lblmsg.ForeColor = System.Drawing.Color.Green;
            //    lblmsg.Text = "Appointment Fixed Successfully";

            //    DataTable dt = new DataTable();
            //    dt = utility.Display("SELECT * FROM GAADADDRESSMASTER WHERE ADDRESSINDEX = '" + int.Parse(HttpContext.Current.Session["AddressIndexid"].ToString()) + "'");
            //    if (dt.Rows.Count > 0)
            //    {
            //        string EMailTo = dt.Rows[0]["Email"].ToString();
            //        BookAnAppointment appointment = new BookAnAppointment();
            //        appointment.EmailAppointment(ddlSpeciality.SelectedItem.Text, ddldrName.SelectedItem.Text, ddlAvailableTime.SelectedItem.Text, txtDate.Text.Trim(), ddlVisitTime.SelectedItem.Text, EMailTo);
            //    }

            //    Session.Remove("appointmentstart");

            //    Reset();
            //}
            //else
            //{
            //    divMsg.Visible = true;

            //    lblmsg.ForeColor = System.Drawing.Color.Red;
            //    lblmsg.Text = "Appointment Could Not Fixed Successfully";
            //}

            #endregion
        }
        catch (Exception error)
        {

            this.Title = error.Message;
            // throw;
        }

    }

    public void GetAppHistoryDetail(string AppID)
    {
        try
        {
            apptbl.App_Id_bint = int.Parse(AppID.ToString());
            bal.GetAppHistory(apptbl);

            ddlSpeciality.SelectedValue = Convert.ToInt32(apptbl.Speciality_Id_bint).ToString();

            spltbl.Speciality_Id_bint = Convert.ToInt32(ddlSpeciality.SelectedValue);
            ddldrName.DataSource = bal.SpecilityDoc(spltbl);
            ddldrName.DataValueField = "Doctor_Id_bint";
            ddldrName.DataTextField = "Doctor_Name_vcr";
            ddldrName.DataBind();

            ddldrName.Items.Insert(0, "");

            ddldrName.SelectedValue = Convert.ToInt32(apptbl.Doctor_Id_bint).ToString();

            doctbl.Doctor_Id_bint = Convert.ToInt32(ddldrName.SelectedValue);
            ddlAvailableTime.DataSource = bal.AvailableTime(doctbl);
            ddlAvailableTime.DataValueField = "Day";
            ddlAvailableTime.DataTextField = "Avail_Time";
            ddlAvailableTime.DataBind();

            DisableUnwantedAvaTiminig();
            SplitTime();
            /** Available Time **/
            string AvailTm = apptbl.App_Avai_Time;
            AvailTm = AvailTm.Substring(0, 3);

            string[] arr = new string[6];
            int TM = 0;
            arr[0] = "Mon";
            arr[1] = "Tue";
            arr[2] = "Wed";
            arr[3] = "Thu";
            arr[4] = "Fri";
            arr[5] = "Sat";

            if (arr[0] == AvailTm)
            {
                TM = 1;
            }
            else if (arr[1] == AvailTm)
            {
                TM = 2;
            }
            else if (arr[2] == AvailTm)
            {
                TM = 3;
            }
            else if (arr[3] == AvailTm)
            {
                TM = 4;
            }
            else if (arr[4] == AvailTm)
            {
                TM = 5;
            }
            else if (arr[5] == AvailTm)
            {
                TM = 6;
            }

            /** End **/

            ddlAvailableTime.SelectedValue = Convert.ToInt32(TM).ToString();
            //ddlAvailableTime.SelectedItem.Text = apptbl.App_Avai_Time;
            ddlVisitTime.SelectedValue = apptbl.App_Visit_Time;

            txtDate.Text = apptbl.App_Date;

            btnBookAppointment.Text = "Update Appointment";
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

    }

    public void UpdateAppHistory()
    {
        string day = string.Empty;
        string month = string.Empty;
        string year = string.Empty;

        apptbl.Speciality_Id_bint = int.Parse(ddlSpeciality.SelectedValue.ToString());
        apptbl.Doctor_Id_bint = int.Parse(ddldrName.SelectedValue.ToString());
        apptbl.App_Avai_Time = ddlAvailableTime.SelectedItem.Text;
        apptbl.App_Visit_Time = ddlVisitTime.SelectedValue;

        if (txtDate.Text != null)
        {
            string[] separators = { ",", ".", "!", "?", ";", ":", " ", "/", "-" };
            string value = txtDate.Text;
            string[] words = value.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            day = words[0];
            month = words[1];
            year = words[2];

            txtDate.Text = year + "-" + month + "-" + day;

        }
        else
        {

        }

        apptbl.App_Date = txtDate.Text;

        string AppID = Request.QueryString["AppId"];
        apptbl.App_Id_bint = int.Parse(AppID.ToString());

        bal.UpdateAppointment(apptbl);

        if (Session["AppMsg"] == "Success")
        {
            lblmsg.ForeColor = System.Drawing.Color.Green;
            lblmsg.Text = "Appointment Update Successfully";

            txtDate.Text = day + "-" + month + "-" + year;

        }
        else
        {
            lblmsg.ForeColor = System.Drawing.Color.Red;
            lblmsg.Text = "Failed";
        }

    }

    public void Reset()
    {
        ddlSpeciality.SelectedIndex = -1;
        ddlSpeciality.Items.Clear();

        ddlSpeciality.DataSource = bal.Speciality();
        ddlSpeciality.DataValueField = "Speciality_Id_bint";
        ddlSpeciality.DataTextField = "Speciality_vcr";
        ddlSpeciality.DataBind();
        ddlSpeciality.Items.Insert(0, "");



        txtSpeciality.Text = string.Empty;
        //ddldrName.SelectedIndex = -1;
        ddldrName.Items.Clear();
        txtDate.Text = string.Empty;
        // ddlAvailableTime.SelectedIndex = -1;
        ddlAvailableTime.Items.Clear();
        txtVisitTime.Text = string.Empty;
        ddlVisitTime.Items.Clear();

        Session.Remove("appointmentstart");
        Session.Remove("specialtyid");
        Session.Remove("doctorid");
        Session.Remove("doctorname");
        Session.Remove("specialtyname");

        btnreselect.Visible = false;
        txtSpeciality.ReadOnly = false;
        ddldrName.Enabled = true;
        ddlSpeciality.Enabled = true;

        ddldrName.Items.Insert(0, "");
        ddlAvailableTime.Items.Insert(0, "");
    }

    [WebMethod]
    public static List<string> GetAutoCompleteData(string speciality)
    {


        List<string> result = new List<string>();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        //using (SqlCommand cmd = new SqlCommand("select distinct Name from Events where IsActive='1' and Name LIKE '%'+@SearchText+'%'", con))
        using (SqlCommand cmd = new SqlCommand("SELECT Speciality_Id_bint,Speciality_vcr FROM  dbo.SpecialityMaster	WHERE Is_Active_bit=1 and (Speciality_vcr like '%'+@SearchText+'%') order by Speciality_vcr", con))
        {
            con.Open();
            cmd.Parameters.AddWithValue("@SearchText", speciality);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                result.Add(dr["Speciality_vcr"].ToString());
                //result.Add(dr["Speciality_Id_bint"].ToString());
            }
            return result;
        }
    }
    public int getid(string speciality)
    {

        Utility utility = new Utility();
        DataTable dt = new DataTable();
        int id = 0;
        try
        {
            SqlCommand cmd = new SqlCommand("select distinct Speciality_Id_bint FROM  dbo.SpecialityMaster where Is_Active_bit=1 and Speciality_vcr='" + speciality + "'");
            dt = utility.Display(cmd);

            if (dt.Rows.Count > 0)
            {
                id = Convert.ToInt32(dt.Rows[0]["Speciality_Id_bint"].ToString());
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

        return id;
    }
    protected void txtSpeciality_TextChanged(object sender, EventArgs e)
    {
        if (txtSpeciality.Text != "" && txtSpeciality.Text.Length > 3)
        {
            spltbl.Speciality_Id_bint = getid(txtSpeciality.Text.Trim());
            ddldrName.DataSource = bal.SpecilityDoc(spltbl);
            ddldrName.DataValueField = "Doctor_Id_bint";
            ddldrName.DataTextField = "Doctor_Name_vcr";
            ddldrName.DataBind();

            ddldrName.Items.Insert(0, "");
            //ddldrName.Items.Insert(0, "--Select Doctor--");
        }
        else
        {
            ddldrName.Items.Clear();
        }
    }

    protected void ddlslottime_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAvailableTime.SelectedValue != "" && ddlAvailableTime.SelectedValue != "----" && ddlAvailableTime.SelectedValue != "--")
        {
            //SplitTime();
        }
        else
        {

        }



    }
    //public string Day()
    //{
    //    string day = DateTime.Now.DayOfWeek.ToString();
    //    //SplitTime(day);
    //    return day;
    //}
    public string SplitTime()
    {
        ddlVisitTime.Items.Clear();
        ddlVisitTime.Items.Insert(0, "");
        string timeDiff = string.Empty;
        try
        {
            /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx----get day and time start----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            #region switching commented code

            //SqlCommand cmd = new SqlCommand("select * from DoctorMaster where Doctor_Id_bint ='" + ddldrName.SelectedValue + "'");
            //DataTable dt = new DataTable();
            //dt = utility.Display(cmd);
            //switch (day)
            //{
            //    case "Monday":
            //        Daywisetime = dt.Rows[0]["Avail_Time_Mon_vcr"].ToString();
            //        break;
            //    case "Tuesday":
            //        Daywisetime = dt.Rows[0]["Avail_Time_Tue_vcr"].ToString();
            //        break;
            //    case "Wednesday":
            //        Daywisetime = dt.Rows[0]["Avail_Time_Wed_vcr"].ToString();
            //        break;
            //    case "Thursday":
            //        Daywisetime = dt.Rows[0]["Avail_Time_Thu_vcr"].ToString();
            //        break;
            //    case "Friday":
            //        Daywisetime = dt.Rows[0]["Avail_Time_Fri_vcr"].ToString();
            //        break;
            //    case "Saturday":
            //        Daywisetime = dt.Rows[0]["Avail_Time_Sat_vcr"].ToString();
            //        break;
            //    case "Sunday":
            //        Daywisetime = "";
            //        break;
            //    default:
            //        //"Sunday";
            //        break;
            //}
            #endregion

            string Daywisetime = string.Empty;
            char[] times = { '-', ' ' };

            #region Store value of Dropdown in String

            //Daywisetime = ddlAvailableTime.SelectedItem.Text.Trim();
            if (apptbl.App_Avai_Time != "" && apptbl.App_Avai_Time != null)
            {
                Daywisetime = apptbl.App_Avai_Time;
            }
            else
            {
                Daywisetime = ddlAvailableTime.SelectedItem.Text.Trim();
            }
            #endregion


            #region Replacement mon,tue,wed,thur,frid,sat

            Daywisetime = Daywisetime.Replace("Mon :", "").Replace("Tue :", "").Replace("Wed :", "").Replace("Thu :", "").Replace("Fri :", "").Replace("Sat :", "").Trim();

            #endregion


            #region get actual from and to time


            string[] timesplit = Daywisetime.Split(times);
            string timestore = string.Empty;
            string timestoreampm = string.Empty;
            //List<string> timestore = new List<string>();

            foreach (string time in timesplit)
            {
                if (time == "0" || time == "1" || time == "2" || time == "3" || time == "4" || time == "5" || time == "6" || time == "7" || time == "8" || time == "9" || time == "10" || time == "11" || time == "12")
                {
                    timestore += time + ",";
                    // timestore.Add(time);
                }
                else
                {
                    timestoreampm += time + ",";
                }
            }
            timestore = timestore.Substring(0, timestore.Length - 1);
            timestoreampm = timestoreampm.Substring(0, timestoreampm.Length - 1);
            #endregion
            /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx----get day and time End----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

            #region diffrence between to and from time

            string to = string.Empty;
            string from = string.Empty;
            string toampm = string.Empty;
            string fromampm = string.Empty;

            char[] tofromsplit = { ',' };

            string[] tofromtimes = timestore.Split(tofromsplit);
            string[] tofromtimesampm = timestoreampm.Split(tofromsplit);

            foreach (string tofromtime in tofromtimes)
            {
                if (from == "")
                {
                    from = tofromtime.ToString();
                }
                else if (to == "")
                {
                    to = tofromtime.ToString();
                }
                else
                {

                }
            }

            foreach (string tofromtimeampm in tofromtimesampm)
            {
                if (fromampm == "")
                {
                    fromampm = tofromtimeampm.ToString();
                }
                else if (toampm == "")
                {
                    toampm = tofromtimeampm.ToString();
                }
                else
                {

                }
            }


            //DateTime dFrom;
            //DateTime dTo;
            string sDateFrom = from.ToString() + ":00:00 " + fromampm.ToString().ToUpper();
            string sDateTo = to.ToString() + ":00:00 " + toampm.ToString().ToUpper(); ;

            //if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
            //{
            //    TimeSpan TS = dFrom - dTo;
            //    int hour = TS.Hours;
            //    int mins = TS.Minutes;
            //    int secs = TS.Seconds;
            //     timeDiff = hour.ToString("00");
            //   // Response.Write(timeDiff); //output 16 mins in format 00:16:00
            //}
            //timeDiff = timeDiff.Replace("-", "");

            #endregion


            #region set value in dropdown list

            SqlCommand cmd = new SqlCommand("select * from DoctorMaster where Doctor_Id_bint ='" + ddldrName.SelectedValue + "'");
            DataTable dt = new DataTable();
            dt = utility.Display(cmd);

            DateTime start = DateTime.Parse(sDateFrom);
            DateTime end = DateTime.Parse(sDateTo);
            string slot = dt.Rows[0]["Slotbooking"].ToString();
            double duration = double.Parse(slot);

            List<string> listslot = new List<string>();

            while (true)
            {
                DateTime dtNext = start.AddMinutes(duration);
                if (start > end || dtNext > end)
                    break;
                if (start < DateTime.Parse("12:00 PM"))
                {
                    listslot.Add(start.ToShortTimeString() + "-" + dtNext.ToShortTimeString());
                    //morning += start.ToShortTimeString() + "-" + dtNext.ToShortTimeString() + "<br>";
                    // morning  start.ToShortTimeString() + "-" + dtNext.ToShortTimeString() + "<br>";

                }
                else
                {
                    listslot.Add(start.ToShortTimeString() + "-" + dtNext.ToShortTimeString());
                    //afternon += start.ToShortTimeString() + "-" + dtNext.ToShortTimeString() + "<br>";
                }
                start = dtNext;
            }

            if (listslot.Count > 0)
            {
                // ddlVisitTime.DataSource = listslot;

                string date = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("dd-MM-yyyy");

                SqlCommand cmd1 = new SqlCommand("select App_Visit_Time from OnlineAppointmentMaster where convert(varchar(10),App_Date,105)='" + date + "' and ConsDocId='" + Session["doctorconsdocid"].ToString() + "' and IsActive=1");

                DataTable dt1 = new DataTable();
                dt1 = utility.DisplayOnlineLHRC(cmd1);
                List<string> slotrowas = new List<string>();
                if (dt1.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt1.Rows)
                    {
                        slotrowas.Add(dr["App_Visit_Time"].ToString());
                    }
                }

                foreach (string lists in listslot)
                {

                    System.Web.UI.WebControls.ListItem li = new System.Web.UI.WebControls.ListItem(lists, lists);
                    if (slotrowas != null)
                    {
                        foreach (string check in slotrowas)
                        {
                            //string getvalue = check.;

                            if (check == lists)
                            {
                                li.Attributes.Add("disabled", "true");
                            }
                            else
                            {
                                //li.Attributes.Add("disabled", "false");
                            }

                        }
                    }
                    else
                    {

                        //  li.Attributes.Add("disabled", "false");
                    }
                    ddlVisitTime.Items.Add(li);
                }
                ddlVisitTime.DataBind();
                //ddlVisitTime.Items.Insert(0, "");
                //ddlVisitTime.Items.Remove("");
            }


            /*xxxxxxxxxxxxxxxxxxxxxxxxxxxxx----get diffrence between to and from time End----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
            #endregion
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }
        return timeDiff;
    }

    public void BindFrontAppointment()
    {
        try
        {


            #region Bind Specialty name

            txtSpeciality.Text = Session["specialtyname"].ToString();
            txtSpeciality.ReadOnly = true;

            string specialtyId = Session["specialtyid"].ToString();
            string specialtyname = Session["specialtyname"].ToString();

            ddlSpeciality.Items.Insert(0, new System.Web.UI.WebControls.ListItem(specialtyname, specialtyId));
            //ddldrName.Items.Insert(0, "--Select Doctor--");
            ddlSpeciality.Enabled = false;

            apptbl._Specialityname = ddlSpeciality.SelectedItem.Text.Trim().ToString();

            #endregion

            #region Bind Doctor name

            if (txtSpeciality.Text != "" && txtSpeciality.Text.Length > 3)
            {
                //spltbl.Speciality_Id_bint = getid(txtSpeciality.Text.Trim());
                //ddldrName.DataSource = bal.SpecilityDoc(spltbl);
                //ddldrName.DataValueField = "Doctor_Id_bint";
                //ddldrName.DataTextField = "Doctor_Name_vcr";
                //ddldrName.DataBind();



                string Id = Session["doctorid"].ToString();
                string name = Session["doctorname"].ToString();

                ddldrName.Items.Insert(0, new System.Web.UI.WebControls.ListItem(name, Id));
                //ddldrName.Items.Insert(0, "--Select Doctor--");
                ddldrName.Enabled = false;

                apptbl._Doctor_Name_vcr = ddldrName.SelectedItem.Text.Trim().ToString();
                apptbl._ConsDrId = ddldrName.SelectedValue.ToString();


            }
            else
            {
                ddldrName.Items.Clear();
            }

            #endregion

            #region Bind Doctor Time

            //if (ddldrName.SelectedIndex > 0)
            //{

            doctbl.Doctor_Id_bint = Convert.ToInt32(ddldrName.SelectedValue);
            ddlAvailableTime.DataSource = bal.AvailableTime(doctbl);
            ddlAvailableTime.DataValueField = "Day";
            ddlAvailableTime.DataTextField = "Avail_Time";
            ddlAvailableTime.DataBind();

            foreach (System.Web.UI.WebControls.ListItem li in ddlAvailableTime.Items)
            {
                string len = li.Text.Length.ToString();

                if (li.Text.Contains("--") || li.Text.Contains("N.A") || li.Text == "" || len == "6")
                {
                    li.Attributes.Add("disabled", "true");
                    //li.Attributes.Add("class", "availa");
                }
            }

            ddlAvailableTime.Items.Insert(0, "");
            //}
            //else
            //{
            //    ddlAvailableTime.Items.Clear();
            //}

            #endregion
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }
    }


    protected void btnreselect_Click(object sender, EventArgs e)
    {

        ButtonReselectDrAtSessionTime();
    }
    public void ButtonReselectDrAtSessionTime()
    {
        Session.Remove("appointmentstart");
        Session.Remove("specialtyid");
        Session.Remove("doctorid");
        Session.Remove("doctorname");
        Session.Remove("specialtyname");
        btnreselect.Visible = false;
        this.Response.Redirect(this.Request.Url.ToString());
    }

    public void DisableUnwantedAvaTiminig()
    {
        if (ddldrName.SelectedIndex > 0)
        {
            DataTable dt = new DataTable();
            doctbl.Doctor_Id_bint = Convert.ToInt32(ddldrName.SelectedValue);
            ddlAvailableTime.DataSource = bal.AvailableTime(doctbl);
            ddlAvailableTime.DataValueField = "Day";
            ddlAvailableTime.DataTextField = "Avail_Time";
            //ddlAvailableTime.DataBind();

            foreach (System.Web.UI.WebControls.ListItem li in ddlAvailableTime.Items)
            {
                string len = li.Text.Length.ToString();

                if (li.Text.Contains("--") || li.Text.Contains("N.A") || li.Text == "" || len == "6")
                {
                    li.Attributes.Add("disabled", "true");
                    //li.Attributes.Add("class", "availa");
                }
            }
        }
    }

    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        //Session["doctorconsdocid"] = "4297529";
        if (Session["doctorconsdocid"] != "")
        {
            DateTime getdate = new DateTime();
            string getdatefromtextbox = txtDate.Text.ToString();
            // getdate = DateTime.ParseExact(getdatefromtextbox, "dd-MM-yyyy", null);
            // getdate = DateTime.Parse("11-07-2016");


            DataTable dt = CheckDoctorAvailability(getdatefromtextbox, Session["doctorconsdocid"].ToString());
            if (dt.Rows.Count > 0)
            {
                string msg = string.Empty;

                msg += "We apologize for the inconvenience </br>";
                msg += dt.Rows[0]["Dr Name"].ToString() + " is not available " + " from " + dt.Rows[0]["FromDate"].ToString() + " to " + dt.Rows[0]["TODATE"].ToString() + " date.";
                lblmsg.Text = msg;
                divMsg.Visible = true;
                btnBookAppointment.Enabled = false;

            }
            else
            {
                SplitTime();
                divMsg.Visible = false;
                btnBookAppointment.Enabled = true;
            }

        }
        else
        {
            SplitTime();
        }

    }


    public DataTable CheckDoctorAvailability(string getdate, string drconsid)
    {
        ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");

        obj.DoctorAvailabilityGet(getdate, drconsid);

        DataSet ds = new DataSet();
        ds = obj.DoctorAvailabilityGet(getdate, drconsid);

        DataTable dt = ds.Tables[0];

        return dt;
        #region commented Code
        //string msg = string.Empty;
        //if (dt.Rows.Count > 0)
        //{

        //    msg += "We apologize for the inconvenience </br>";
        //    msg += dt.Rows[0]["Dr Name"].ToString() + " is not available " + " from " + dt.Rows[0]["FromDate"].ToString() + " to " + dt.Rows[0]["TODATE"].ToString() + " date.";

        //    // Response.Write("<script>alert('" + msg + "')</script>");
        //    // Response.Write(msg);


        //}
        //else
        //{
        //    msg = "Doctor Available";
        //    // Response.Write("<script>alert('" + msg + "')</script>");
        //}
        #endregion
    }



    /*after payment processs event will occur */

    #region Payment  Process
    public string InsertIntoPaymentTable(string patientid, string DrName, string orderid, string trackingid, string bankrefno, string order_status, string paymentmode, string card_name, string amount, string currency)
    {
        string response = "";

        using (SqlCommand cmd = new SqlCommand("Proc_PaymentTable"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "add");
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", patientid);
            cmd.Parameters.AddWithValue("@DOCTOR_NAME_VCR", DrName);
            cmd.Parameters.AddWithValue("@ORDER_ID", orderid.ToString());
            cmd.Parameters.AddWithValue("@TRACKING_ID", trackingid.ToString());
            cmd.Parameters.AddWithValue("@BANK_REF_NO", bankrefno.ToString());
            cmd.Parameters.AddWithValue("@ORDER_STATUS", order_status.ToString());
            cmd.Parameters.AddWithValue("@PAYMENT_MODE", paymentmode.ToString());
            cmd.Parameters.AddWithValue("@CARD_NAME", card_name.ToString());
            cmd.Parameters.AddWithValue("@AMOUNT", amount.ToString());
            cmd.Parameters.AddWithValue("@CURRENCY", currency.ToString());
            if (utility.Execute(cmd))
            {
                response = "success";
            }
            else
            {
                response = "fail";
            }
            return response;
        }
    }


    public void GetStatus(string msg)
    {
        if (msg == "Success")
        {
            MakeAppointment();


            string orderid = Session["order_id"].ToString();
            string trackingid = Session["tracking_id"].ToString();
            string bankrefno = Session["bank_ref_no"].ToString();
            string order_status = Session["order_status"].ToString();
            string paymentmode = Session["payment_mode"].ToString();
            string card_name = Session["card_name"].ToString();
            string amount = Session["amount"].ToString();
            string currency = Session["currency"].ToString();
            string patiestid = Request.Cookies["patientsIdNew"].Value;
            string doctorname = Request.Cookies["DoctorsName"].Value;
            string appointmentdate = Request.Cookies["appointmentdate"].Value;
            string doctorconsid = Request.Cookies["DoctorIdconsid"].Value;

            string InsertResult = InsertIntoPaymentTable(patiestid, doctorname, orderid, trackingid, bankrefno, order_status, paymentmode, card_name, amount, currency);
            // string InsertResult = "success";
            if (InsertResult == "success")
            {

                DataTable getpatientdetail = new DataTable();
                getpatientdetail = utility.Display("Proc_PCipopRegistrationMaster 'GET_BY_ID','" + patiestid + "'");
                if (getpatientdetail.Rows.Count > 0)
                {
                    #region Call Web services and update LHNO into database

                    ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");
                    DataSet ds = new DataSet();
                    //ds = obj.webappointmentprocess("LH00057131", "Mr", "Sunil", "Dinesh", "Tiwari", "Male", "1994/05/04", "Single", "B+", "Hindu", "Indian", "India", "Software Developer","TLHNO6603", "Mahalaxmi", "Mumbai", "Maharashtra", "India", "400011", "9594907366", "sunil.t@kwebmaker.com", "9594907366", 0,"1210318", "2017/05/04", "106203791411", "");
                    ds = obj.webappointmentprocess(getpatientdetail.Rows[0]["LH_NO"].ToString(), getpatientdetail.Rows[0]["TITLE"].ToString(), getpatientdetail.Rows[0]["FIRSTNAME"].ToString(), getpatientdetail.Rows[0]["MIDDLENAME"].ToString(), getpatientdetail.Rows[0]["LASTNAME"].ToString(), getpatientdetail.Rows[0]["SEX"].ToString(), getpatientdetail.Rows[0]["DATEOFBIRTHNEW"].ToString(), getpatientdetail.Rows[0]["MARITALSTATUS"].ToString(), getpatientdetail.Rows[0]["BLOODGROUP"].ToString(), getpatientdetail.Rows[0]["RELIGION"].ToString(), getpatientdetail.Rows[0]["NATIONALITY"].ToString(), getpatientdetail.Rows[0]["RESIDENCE"].ToString(), getpatientdetail.Rows[0]["OCCUPATION"].ToString(), getpatientdetail.Rows[0]["LHNO"].ToString(), getpatientdetail.Rows[0]["ADDRESS"].ToString(), getpatientdetail.Rows[0]["CITY"].ToString(), getpatientdetail.Rows[0]["STATE"].ToString(), getpatientdetail.Rows[0]["COUNTRY"].ToString(), getpatientdetail.Rows[0]["PINCODE"].ToString(), getpatientdetail.Rows[0]["TELNORESIDENCE"].ToString(), getpatientdetail.Rows[0]["EMAIL"].ToString(), getpatientdetail.Rows[0]["MOBILENO"].ToString(), float.Parse(amount), doctorconsid, appointmentdate, orderid, "");
                    DataTable dt123 = ds.Tables[0];
                    if (dt123.Columns.Contains("output"))
                    {
                        using (SqlCommand cmdfailed = new SqlCommand("Proc_FailedAppointment"))
                        {
                            cmdfailed.CommandType = CommandType.StoredProcedure;
                            cmdfailed.Parameters.AddWithValue("@PARA", "ADD");
                            cmdfailed.Parameters.AddWithValue("@ADDRESSINDEX", patiestid);
                            cmdfailed.Parameters.AddWithValue("@DOCTORNAME", doctorname);
                            cmdfailed.Parameters.AddWithValue("@SPECIALITYNAME", Response.Cookies["Speciality"].Value);
                            cmdfailed.Parameters.AddWithValue("@DATEOFAPP", appointmentdate);
                            cmdfailed.Parameters.AddWithValue("@AVAILABLETIME", Response.Cookies["App_Time_vcr"].Value);
                            cmdfailed.Parameters.AddWithValue("@PREFERREDTIME", Response.Cookies["App_Visit_Time"].Value);
                            cmdfailed.Parameters.AddWithValue("@AMOUNT", amount);
                            cmdfailed.Parameters.AddWithValue("@CONSDOCID", doctorconsid);
                            cmdfailed.Parameters.AddWithValue("@ORDERID", orderid);
                            cmdfailed.Parameters.AddWithValue("@FAILEDFROM", "Website");
                            utility.Execute(cmdfailed);


                            divMsg.Visible = true;
                            lblmsg.ForeColor = System.Drawing.Color.Green;
                            lblmsg.Text = "Appointment Fixed Successfully!";                                                        
                        }
                    }
                    else
                    {

                        using (SqlCommand cmdnew = new SqlCommand("Proc_PCipopRegistrationMaster"))
                        {
                            cmdnew.CommandType = CommandType.StoredProcedure;
                            cmdnew.Parameters.AddWithValue("@PARA", "UPDATE_OG_LHNO_AFTER_WEBSERVICES_RESP");
                            cmdnew.Parameters.AddWithValue("@ADDRESSINDEX", patiestid);
                            if (dt123.Rows[0]["RegistrationNumber"] != "")
                            {
                                cmdnew.Parameters.AddWithValue("@LH_NO", dt123.Rows[0]["RegistrationNumber"].ToString());
                            }
                            else
                            {
                                cmdnew.Parameters.AddWithValue("@LH_NO", "-");
                            }
                            utility.Execute(cmdnew);

                            CreateInvoiceForAppointment(dt123.Rows[0]["Reg NO"].ToString(), dt123.Rows[0]["Op No"].ToString(), dt123.Rows[0]["Doc No"].ToString(), dt123.Rows[0]["RegistrationNumber"].ToString(), dt123.Rows[0]["Firstname"].ToString(), dt123.Rows[0]["Middlename"].ToString(), dt123.Rows[0]["Lastname"].ToString(), doctorname);


                        }

                        #region send mail with attachment to patient

                        if (Session["AppMsg"] == "Success")
                        {
                            DataTable dt = new DataTable();
                            dt = utility.Display("SELECT * FROM GAADADDRESSMASTER WHERE ADDRESSINDEX = '" + int.Parse(Request.Cookies["patientsId"].Value) + "'");
                            if (dt.Rows.Count > 0)
                            {
                                string EMailTo = dt.Rows[0]["Email"].ToString();
                                BookAnAppointment appointment = new BookAnAppointment();
                                appointment.EmailAppointment(Request.Cookies["Speciality"].Value, Request.Cookies["Doctors"].Value, Request.Cookies["App_Time_vcr"].Value, Request.Cookies["App_date_dtm"].Value, Request.Cookies["App_Visit_Time"].Value, EMailTo, Session["attachmentname"].ToString());
                            }

                            // Response.Write("Your Payment Done SuccessFully!!!!");
                            divMsg.Visible = true;
                            lblmsg.ForeColor = System.Drawing.Color.Green;
                            lblmsg.Text = "Appointment Fixed Successfully.";

                            Session.Remove("appointmentstart");
                        }
                        else
                        {
                            //divMsg.Visible = true;

                            //lblmsg.ForeColor = System.Drawing.Color.Red;
                            //lblmsg.Text = "Appointment Could Not Fixed Successfully";
                        }
                        #endregion
                    }
                    #endregion                                        
                }                
                ResetAllSessionValue();    
            }
            else
            {
                //  Response.Write("Your Payment Could Not Done SuccessFully!!!!");
                divMsg.Visible = true;
                lblmsg.ForeColor = System.Drawing.Color.Red;
                lblmsg.Text = "Appointment Could Not Fixed Successfully.";
                ResetAllSessionValue();
            }
        }
        else
        {
            //   Response.Write("Your Payment Could Not Done SuccessFully!!!!");
            divMsg.Visible = true;
            lblmsg.ForeColor = System.Drawing.Color.Red;
            lblmsg.Text = "Appointment Could Not Fixed Successfully.";
            ResetAllSessionValue();
        }
        //return msg;
    }

    private void MakeAppointment()
    {

        #region Offline appointment code
        //using (SqlCommand cmd = new SqlCommand("AddUpdateGetAppointmentMaster"))
        //{
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@Para_vcr", "Add");
        //    cmd.Parameters.AddWithValue("@Speciality_Id_bint ", Session["Speciality_Id_bint"].ToString());
        //    cmd.Parameters.AddWithValue("@Doctor_Id_bint", Session["Doctor_Id_bint"].ToString());
        //    cmd.Parameters.AddWithValue("@App_Time_vcr", Session["App_Time_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@App_date_dtm", Session["App_date_dtm"].ToString());
        //    cmd.Parameters.AddWithValue("@Patient_Name_vcr", Session["Patient_Name_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@Address_vcr", Session["Address_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@Country_Id_bint", Session["Country_Id_bint"].ToString());
        //    cmd.Parameters.AddWithValue("@State_vcr", Session["State_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@City_vcr", Session["City_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@Postal_vcr", Session["Postal_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@Mobile_No_vcr", Session["Mobile_No_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@Email_Id_vcr", Session["Email_Id_vcr"].ToString());
        //    cmd.Parameters.AddWithValue("@Visit_vcr", Session["Visit_vcr"].ToString());



        //    if (utility.Execute(cmd))
        //    {

        //        OfflineAppMail offlinemail = new OfflineAppMail();

        //        string Speciality = Session["Speciality"].ToString();
        //        string Doctors = Session["Doctors"].ToString();
        //        string Avail = Session["Avail"].ToString();
        //        string Country = Session["Country"].ToString();
        //        string Visit = Session["Visit"].ToString();
        //        string State = Session["State"].ToString();
        //        string City = Session["City"].ToString();

        //        offlinemail.EmailOfflineApp(Speciality, Doctors, Avail, Session["App_date_dtm"].ToString(), Session["Patient_Name_vcr"].ToString(), Session["Email_Id_vcr"].ToString(), Session["Mobile_No_vcr"].ToString(), Session["Address_vcr"].ToString(), Country, State, City, Session["Postal_vcr"].ToString(), Visit);
        //    }
        //    else
        //    {
        //        //Response.Write("<script>alert('Failed');</script>");
        //    }
        //}
        #endregion

        #region Online Appointmnet
        AppointmentTable apptbl = new AppointmentTable();
        BAL bal = new BAL();
        apptbl.Speciality_Id_bint = int.Parse(Request.Cookies["Speciality_Id_bint"].Value);

        apptbl.Doctor_Id_bint = int.Parse(Request.Cookies["Doctor_Id_bint"].Value);
        apptbl.App_Avai_Time = Request.Cookies["App_Time_vcr"].Value;
        apptbl.App_Visit_Time = Request.Cookies["App_Visit_Time"].Value;

        string date = DateTime.ParseExact(Request.Cookies["App_date_dtm"].Value, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        apptbl.App_Date = date;

        apptbl.AddressIndex = int.Parse(Request.Cookies["patientsId"].Value);
        apptbl._Specialityname = Request.Cookies["Speciality"].Value;
        apptbl._Doctor_Name_vcr = Request.Cookies["Doctors"].Value;
        apptbl._ConsDrId = Request.Cookies["DoctorIdconsid"].Value;



        bal.AddAppointment(apptbl);

        //if (Session["AppMsg"] == "Success")
        //{
        //    DataTable dt = new DataTable();
        //    dt = utility.Display("SELECT * FROM GAADADDRESSMASTER WHERE ADDRESSINDEX = '" + int.Parse(Request.Cookies["patientsId"].Value) + "'");
        //    if (dt.Rows.Count > 0)
        //    {
        //        string EMailTo = dt.Rows[0]["Email"].ToString();
        //        BookAnAppointment appointment = new BookAnAppointment();
        //        // appointment.EmailAppointment(Session["Speciality"].ToString(), Session["Doctors"].ToString(), Session["App_Time_vcr"].ToString(), Session["App_date_dtm"].ToString(), Session["App_Visit_Time"].ToString(), EMailTo);
        //    }

        //    Session.Remove("appointmentstart");
        //}
        //else
        //{
        //    //divMsg.Visible = true;

        //    //lblmsg.ForeColor = System.Drawing.Color.Red;
        //    //lblmsg.Text = "Appointment Could Not Fixed Successfully";
        //}
        #endregion

    }

    public void ResetAllSessionValue()
    {
        //Session.Remove("Doctors");
        //Session.Remove("order_id");
        //Session.Remove("tracking_id");
        //Session.Remove("bank_ref_no");
        //Session.Remove("order_status");
        //Session.Remove("payment_mode");
        //Session.Remove("card_name");
        //Session.Remove("amount");
        //Session.Remove("currency");

        //Session.Remove("Speciality_Id_bint");
        //Session.Remove("Doctor_Id_bint");
        //Session.Remove("App_Time_vcr");
        //Session.Remove("App_date_dtm");
        //Session.Remove("Patient_Name_vcr");
        //Session.Remove("Address_vcr");
        //Session.Remove("Country_Id_bint");
        //Session.Remove("State_vcr");
        //Session.Remove("City_vcr");
        //Session.Remove("Postal_vcr");
        //Session.Remove("Mobile_No_vcr");
        //Session.Remove("Email_Id_vcr");
        //Session.Remove("Visit_vcr");

        //Session.Remove("Speciality");
        //Session.Remove("Avail");
        //Session.Remove("Country");
        //Session.Remove("Visit");
        //Session.Remove("State");
        //Session.Remove("City");

        //Session.Remove("doctorconsdocid");


        Response.Cookies["DoctorsName"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["patientsIdNew"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["appointmentdate"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["DoctorIdconsid"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Speciality_Id_bint"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Doctor_Id_bint"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["App_Time_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["App_date_dtm"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Patient_Name_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Address_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Country_Id_bint"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["State_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["City_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Postal_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Mobile_No_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Email_Id_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Visit_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["checkpaymentprocess"].Expires = DateTime.Now.AddDays(-1);
    }
    #endregion endprocess


    #region Send mail and attachment

    public void CreateInvoiceForAppointment(string regno, string opno, string docno, string lhno, string fname, string middlename, string lastname, string docname)
    {
        //#region get data into litrel
        //string regno1 = regno; string opno1 = opno; string docno1 = docno; string lhno1 = lhno; string fname1 = fname; string middlename1 = middlename; string lastname1 = lastname; string docname1 = docname;
        //ltrlpatientinvoice.Text = AppointmentInvoiceStructure(regno1, opno1, docno1, lhno1, fname1, middlename1, lastname1, docname1);
        //#endregion



        //Response.ClearContent();
        //Response.Buffer = true;
        //string strFileName = fname + "_" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + ".doc";

        //Session["attachmentname"] = strFileName;

        //HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName);
        ////Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", DateTime.Today.ToShortDateString().Replace("/", "").Replace("-", "") + "_" + DateTime.Now.ToShortDateString() + ".doc"));
        ////Response.ContentType = "application/ms-word";
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter htw = new HtmlTextWriter(sw);

        //ltrlpatientinvoice.RenderControl(htw);

        //string strPath = Request.PhysicalApplicationPath + "\\uploads\\visa_invitation\\" + strFileName;
        //System.IO.StreamWriter sWriter = new System.IO.StreamWriter(strPath);
        //sWriter.Write(sw.ToString());

        //sWriter.Close();

        #region Create invoice for PDF by Arvind

        #region get data into litrel
        string regno1 = regno; string opno1 = opno; string docno1 = docno; string lhno1 = lhno; string fname1 = fname; string middlename1 = middlename; string lastname1 = lastname; string docname1 = docname;
        ltrlpatientinvoice.Text = AppointmentInvoiceStructure(regno1, opno1, docno1, lhno1, fname1, middlename1, lastname1, docname1);
        #endregion

        string strFileName = fname + "_" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + ".pdf";

        Session["attachmentname"] = strFileName;

        //string fileName = "pdfDocument" + DateTime.Now.Ticks + ".pdf";
        //string strPath = Request.PhysicalApplicationPath + "\\PDF\\" + fileName;
        
        StringReader sr = new StringReader(ltrlpatientinvoice.Text);
        Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

        var output = new FileStream(Path.Combine(Request.PhysicalApplicationPath + "\\uploads\\visa_invitation\\", strFileName), FileMode.Create);

        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, output);
        pdfDoc.Open();
        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
        pdfDoc.Close();

        #endregion
    }

    public string AppointmentInvoiceStructure(string regno, string opno, string docno, string lhno, string fname, string middlename, string lastname, string docname)
    {
        if (regno == "") { regno = "-"; } if (opno == "") { opno = "-"; } if (docno == "") { docno = "-"; } if (lhno == "") { lhno = "-"; }

        fname = fname + " " + middlename + " " + lastname;


       /// DateTime dateofappointment = Convert.ToDateTime(Request.Cookies["App_date_dtm"].Value);
        string dateofappointment = Request.Cookies["App_date_dtm"].Value + " " + Request.Cookies["App_Visit_Time"].Value;

        StringBuilder dbinvoice = new StringBuilder();


        dbinvoice.Append("<table id='tblMain' runat='server' width='100%' cellpadding='0' cellspacing='0' style='border:1px solid #ccc; border-collapse:collapse; font-family:Arial, Helvetica, sans-serif; color:#333;font-size:5px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='10'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td><img src='http://lilavatihospital.com/images/invoice_logo.png' title='Lilavati Hospital & Research Center' width='300px' /></td>");
        dbinvoice.Append("<td width='50%' align='center' style='font-size:10px;' valign='middle'><strong>OPD Invoice</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center' height='25'><hr width='96%' color='#04869a' /></td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' cellpadding='5' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td width='39.33%'>Reg. No.: <strong>" + regno + "</strong></td>");
        dbinvoice.Append("<td width='30.33%'>OP No.: <strong>" + opno + "</strong></td>");
        dbinvoice.Append("<td width='30.33%'>Document No.: <strong>" + docno + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td colspan='2'>Patient Name: <strong>" + fname + "</strong></td>");
        dbinvoice.Append("<td>Date: <strong>" + dateofappointment + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td colspan='2'>Referring Dr: <strong>No Reference</strong></td>");
        dbinvoice.Append("<td>New LHNO: <strong>" + lhno + "</strong></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='30'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='100%' cellpadding='7' style='font-size:10px;'>");
        dbinvoice.Append("<tr bgcolor='#04869a' style='background-color:#04869a;color:#fff;'>");
        dbinvoice.Append("<td><strong>Department</strong></td>");
        dbinvoice.Append("<td><strong>Charge Description</strong></td>");
        dbinvoice.Append("<td width='100'><strong>Qty.</strong></td>");
        dbinvoice.Append("<td><strong>Rs.</strong></td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td>OPD Services</td>");
        dbinvoice.Append("<td>Consultation - Hospital OPD<br />" + docname + "</td>");
        dbinvoice.Append("<td>1</td>");
        dbinvoice.Append("<td>" + Session["amount"].ToString() + "</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr bgcolor='#eeeeee' style='background-color:#eeeeee;color:#000;'>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td><strong>TOTAL (Rs.)</strong></td>");
        dbinvoice.Append("<td><strong>" + Session["amount"].ToString() + "</strong></td>");
        dbinvoice.Append("</tr>");


        //dbinvoice.Append("<tr>");
        //dbinvoice.Append("<td colspan='4'>Rupees one thousand five hundred only received in cash</td>");
        //dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("<hr color='#04869a' width='96%' />");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");


        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='15'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' cellpadding='5' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td width='69.66%'>Bill Prepared by: <strong>Web Appointment</strong></td>");
        dbinvoice.Append("<td width='30.33%'>Conf By: <strong>Web Appointment</strong></td>");
        dbinvoice.Append("</tr>");
        //dbinvoice.Append("<tr>");
        //dbinvoice.Append("<td>Pan No. <strong>AAATL1398Q</strong></td>");
        //dbinvoice.Append("<td></td>");
        //dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='50'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td align='center'>");
        dbinvoice.Append("<table width='96%' style='font-size:10px;'>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td valign='top' width='45'><strong>NOTE:</strong> </td>");
        dbinvoice.Append("<td>Please preserve this original invoice carefully and produce it while collecting your investgation reports.<br /> Enclosed Requisition Slip(s) should be handed over to the hospital departments where service is availed.</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td></td>");
        dbinvoice.Append("<td>This is an electronic generated invoice.</td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");
        dbinvoice.Append("</td>");
        dbinvoice.Append("</tr>");

        dbinvoice.Append("<tr>");
        dbinvoice.Append("<td height='15'></td>");
        dbinvoice.Append("</tr>");
        dbinvoice.Append("</table>");

        return dbinvoice.ToString();
    }

    #endregion

    [System.Web.Services.WebMethod]
    public static string GetTimeForBind()
    {
        DataTable dt = new DataTable();
        Utility utilil = new Utility();
        dt = utilil.Display("EXEC PROC_DRNOTAVAILABETABLE 'GET_DATE_FOR_FRONTEND'");
        foreach (DataRow date in dt.Rows)
        {
            string val = date[1].ToString();
            val = val.TrimStart('0');

            Char delimiter = '-';
            string[] arr = val.Split(delimiter);

            string mm = arr[0];
            string dy = arr[1].TrimStart('0');
            string yy = arr[2];

            //date[1] = val;
            date[1] = mm + "-" + dy + "-" + yy;
        }
        DataSet ds = new DataSet();
        ds.Tables.Add(dt);
        return ds.GetXml();
    }
}