﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_appointment_history : UserLogin
{
    BAL bal = new BAL();
    AppointmentTable apptbl = new AppointmentTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["Column"] = "App_Status";
            ViewState["Sortorder"] = "DESC";
            BindRepeater();
        }
    }

    public void BindRepeater()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = bal.AppointmentHistory();

            DataView dvData = new DataView(dt);
            dvData.Sort = ViewState["Column"] + " " + ViewState["Sortorder"];

            //rptAppHistory.DataSource = bal.AppointmentHistory();

            rptAppHistory.DataSource = dvData;
            rptAppHistory.DataBind();
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

    }


    [System.Web.Services.WebMethod]
    public static string CancelAppointment(string id)
    {
        BAL bal = new BAL();
        AppointmentTable apptbl = new AppointmentTable();

        apptbl.App_Id_bint = int.Parse(id.ToString());
        bal.CancelledAppointmet(apptbl);

        if (HttpContext.Current.Session["CanMsg"] == "Success")
        {
            //Dashboard_appointment_history his = new Dashboard_appointment_history();
            //his.BindRepeater();
            
        }
        else
        {
            
        }
        return HttpContext.Current.Session["CanMsg"].ToString();

        //return "Hello " + id + Environment.NewLine + "The Current Time is: "
        //    + DateTime.Now.ToString();
    }

    protected void rptAppHistory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == ViewState["Column"].ToString())
            {
                if (ViewState["Sortorder"].ToString() == "ASC")
                    ViewState["Sortorder"] = "DESC";
                else
                    ViewState["Sortorder"] = "ASC";
            }
            else
            {
                ViewState["Column"] = e.CommandName;
                ViewState["Sortorder"] = "ASC";
            }
            BindRepeater();
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            //throw;
        }

    }
}