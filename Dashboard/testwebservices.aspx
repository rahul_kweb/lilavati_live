﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testwebservices.aspx.cs" Inherits="testwebservices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DoctorAvailabilityGet</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="txttodaydate" runat="server" placeholder="Date"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtdrconsid" runat="server" placeholder="Dr cons id"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnsubmit" runat="server" Text="submit" OnClick="btnsubmit_Click" />
        </div>
        <div style="background-color: red;">
            <asp:GridView ID="grd" runat="server"></asp:GridView>
        </div>

        <br />
        <br />
        Testing Datetime
        <asp:TextBox ID="txtDate" runat="server" Text="27-03-2017"></asp:TextBox>
        <asp:Label ID="lblDateTime" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="txtOutput" runat="server"></asp:TextBox>
    </form>
</body>
</html>
