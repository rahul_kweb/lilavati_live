﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testwebservices : System.Web.UI.Page
{
    //string adtconsdoccode = "4297529";
    //DateTime date = Convert.ToDateTime("11/07/2016 12:00:00 AM");

    protected void Page_Load(object sender, EventArgs e)
    {
       // checkwebservices();
        if (!IsPostBack)
        {
            //TestDateTime();
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        checkwebservices();
        // DateTime dt12121 = Convert.ToDateTime(txttodaydate.Text.ToString());
        //  DateTime datetime = DateTime.ParseExact(txttodaydate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        // string date = DateTime.ParseExact(txttodaydate.Text, "dd/MM/yyyy", null).ToString("dd/MM/yyyy");
        // DateTime datetime = Convert.ToDateTime(date.ToString()).ToShortDateString("dd/MM/yyyy");
        //datetime = datetime.Date.ToString() + "/" + datetime.Month.ToString() + "/" + datetime.Year.ToString();

        //ds = 

        // DataTable dt = new DataTable();
        //dt = ds.Tables[0];
    }
    public void checkwebservices()
    {
       

        ServicesMaster.MastersDataSoapClient obj = new ServicesMaster.MastersDataSoapClient("MastersDataSoap");

        obj.DoctorAvailabilityGet(DateTime.Parse(txttodaydate.Text), txtdrconsid.Text);

        DataSet ds = new DataSet();
        ds = obj.DoctorAvailabilityGet(DateTime.Parse(txttodaydate.Text), txtdrconsid.Text);

        DataTable dt = ds.Tables[0];

        string msg = string.Empty;
        if (dt.Rows.Count > 0)
        {
            
            msg +="We apologize for the inconvenience </br>";
            msg += dt.Rows[0]["Dr Name"].ToString() + " is not available " + " from " + dt.Rows[0]["FromDate"].ToString() + " to " + dt.Rows[0]["TODATE"].ToString()+" date.";

           // Response.Write("<script>alert('" + msg + "')</script>");
            Response.Write(msg);

            grd.DataSource = dt;
            grd.DataBind();
        }
        else {
            msg = "Doctor Available";
            Response.Write("<script>alert('" + msg + "')</script>");
        }


    }

    public void TestDateTime()
    {
        //string date = "27/03/2017";
        //DateTime date = DateTime.Parse(txtDate.Text);
        DateTime date = DateTime.ParseExact(this.txtDate.Text.ToString(), "dd-MM-yyyy", null);
        
        //DateTime date = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        //lblDateTime.Text = date.ToShortDateString();
        //txtOutput.Text = date.ToShortDateString();
        //Response.Write("<script>alert(' + date + ')<script>");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + date + "')", true);
    }

}