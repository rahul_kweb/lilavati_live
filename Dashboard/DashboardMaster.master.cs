﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_DashboardMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[AppKey.EmailID] != null && !Session[AppKey.Password].ToString().Equals(""))
        {
            lblwelcome.Text = "Welcome, " + Session[AppKey.UserName].ToString();
        }

        string Url = System.IO.Path.GetFileNameWithoutExtension(Request.Path);
        if (Url == "Welcome")
        {
            idmenu.Visible = false;
            dashboard.Attributes.Add("class", "active");
        }
        else if (Url == "Book-An-Appointment")
        {
            bookApp.Attributes.Add("class", "active");
        }
        else if (Url == "Appointment-History")
        {
            AppHistory.Attributes.Add("class", "active");
        }
        else if (Url == "Profile-Details")
        {
            UpdatePro.Attributes.Add("class", "active");
        }
        else if (Url == "Change-Password")
        {
            changePass.Attributes.Add("class", "active");
        }
        else
        {
            dashboard.Attributes.Add("class", "active");
        }        
    }

    protected void idlogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Cookies["EMAIL"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["PASSWORD"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["FIRSTNAME"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["AddressIndex"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies[AppKey.AddressIndex].Expires = DateTime.Now.AddDays(-1);

        Response.Cookies["DoctorsName"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["patientsIdNew"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["appointmentdate"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["DoctorIdconsid"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Speciality_Id_bint"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Doctor_Id_bint"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["App_Time_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["App_date_dtm"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Patient_Name_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Address_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Country_Id_bint"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["State_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["City_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Postal_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Mobile_No_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Email_Id_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["Visit_vcr"].Expires = DateTime.Now.AddDays(-1);
        Response.Cookies["checkpaymentprocess"].Expires = DateTime.Now.AddDays(-1);
        if (Request.Cookies["checkpaymentprocess"] != null)
        {
            Request.Cookies["checkpaymentprocess"].Value = "";
        }

        Response.Redirect("~/login");
    }
}
