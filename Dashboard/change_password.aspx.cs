﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Dashboard_Welcome : UserLogin
{
    BAL bal = new BAL();
    RegistrationTable tbl = new RegistrationTable();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUpdatePassword_Click(object sender, EventArgs e)
    {
        tbl.Password = txtOldPassword.Text.Trim();
        tbl.NewPassword = txtConfirmPassword.Text.Trim();
        bal.ChangePassword(tbl);

        if (HttpContext.Current.Session["ChangePassword"] == "Success")
        {
            divUsername.Visible = true;
            lblmsg.ForeColor = System.Drawing.Color.Green;
            lblmsg.Text = "Your password has been changed successfully.";
            Session.Abandon();
            AutoRedirectLoginPage();
        }
        else
        {
            divUsername.Visible = true;
            lblmsg.ForeColor = System.Drawing.Color.Red;
            lblmsg.Text = "Sorry you have provided an incorrect old password";
        }
    }

    public void AutoRedirectLoginPage()
    {
        HtmlMeta meta = new HtmlMeta();
        meta.HttpEquiv = "Refresh";
        meta.Content = "5;url=../login";
        this.Page.Controls.Add(meta);        
    }

}