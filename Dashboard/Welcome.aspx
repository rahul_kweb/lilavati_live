﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dashboard/DashboardMaster.master"
    AutoEventWireup="true" CodeFile="Welcome.aspx.cs" Inherits="Dashboard_Welcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="db_box_main fade_anim">
        <div class="db_box">
            <a href="Book-An-Appointment">
                <img src="../images/dashboard/book_appointment.png">
                Book an Appointment </a>
        </div>
        <div class="db_box">
            <a href="Appointment-History">
                <img src="../images/dashboard/appointment_history.png">
                Appointment History </a>
        </div>
        <div class="db_box">
            <a href="Profile-Details">
                <img src="../images/dashboard/update_profile.png">
                View Profile</a>
        </div>
        <div class="db_box">
            <a href="Change-Password">
                <img src="../images/dashboard/change_password.png">
                Change Password</a>
        </div>
    </div>
</asp:Content>
