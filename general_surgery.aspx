﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="general_surgery.aspx.cs" Inherits="general_surgery" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">General Surgery</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/General_surgery.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--<div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Our department takes care of various surgical emergencies like acute appendicitis, acute cholecystitis, intestinal obstruction, trauma, abscesses, carbuncles, gangrene and necrotizing fasciitis, patients with sepsis due to surgical conditions. Our department acts as a tertiary referral unit for these emergency cases. Patients are received in our well-equipped casualty department and are promptly operated upon, if required, after stabilization. Peri-operatively, these patients are treated in our intensive care unit or in wards. </p>
                            <p>Our department initiates and executes various research projects on contemporary topics. Our consultants, students routinely attend national and international conferences to remain abreast with the recent knowledge and to present our work. As a part of training, we have regular academic activities happening in the department.</p>
                            <p>Our department is involved in training of DNB candidates as a part of DNB program of the hospital and we have intake of 2 candidates per year. We have a very successful DNB program running for last 12 years.  All our past students have excelled in their chosen fields.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <p><strong class="uppercase green_text">Elective surgeries</strong></p>
                            <ul class="bullet">
                                <li><strong>Routine elective general surgeries  -</strong><br>
                                    We perform surgeries for various indications like hernia (Inguinal, incisional, umbilical etc. by both open and laparoscopic approach), hydrocele, Hemorrhoids (piles), Fissure-in-ano, enlarged lymph nodes, lipomas and other skin swellings, breast  and axilla lumps, neck swellings including thyroid and parotid gland swellings, etc. Besides these, we offer holistic treatment for diabetic foot, peripheral vascular disease</li>
                                <li><strong>Gastrointestinal surgeries –</strong><br>
                                    Our department performs both open and laparoscopic surgeries for gall-stones, appendicitis, tumors of abdomen, adrenal tumors, Bowel resection and anastomosiqcancers of gastrointestinal tract, and various other GI pathologies. Advanced laparoscopic surgeries like splenectomy, anti-reflux surgeries (fundoplication), surgery for achalasia cardia (Heller’s Myotomy),</li>
                                <li><strong>Thoracic surgeries –</strong><br>
                                    Our department performs highly specialized work of VATS (Video-assisted Thoracoscopic Surgery) and open thoracic surgeries (thoracotomy and thoracoplasty) for various indications like tuberculosis, infective loculated pleural effusions, spontaneous pneumothorax, lung biopsies, lobectomies, pneumonectomy excision of thymus tumor and mediastinal masses.</li>
                            </ul>

                            <p><strong class="uppercase green_text">Surgical referral service for admitted patients</strong></p>
                            <p>We take care of surgical problems arising in patients admitted to hospital or attending OPDs for non-surgical causes. We are committed towards their overall well-being.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

