﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class cme : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    CME tblCme = new CME();
    CMEDetails tblCMEDetails = new CMEDetails();
    CMEDetails tblCmedetails = new CMEDetails();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
            BindBanner();
            TableData();
        }
    }

    public void Content()
    {
        cms.ID = 13;
        cmsbal.GETBYIDContent(cms);
        litContent.Text = cms.Description;
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(60);
    }

    public void TableData()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = cmsbal.CMEEventName();

        DataTable dt2 = new DataTable();

        foreach (DataRow row in dt.Rows)
        {            
            tblCmedetails.Cme_Id = Convert.ToInt32(row[0].ToString());
            //dt2 = cmsbal.CMEEventDetails(tblCmedetails);
            dt2 = cmsbal.GetCMEDetails(tblCmedetails);

            if (dt2.Rows.Count > 0)
            {
                //strBuil.Append("<p><strong class='uppercase green_text'> " + row[1] + " </strong></p>");

                strBuil.Append("<p><strong class='green_text'> " + row[1] + " </strong></p>");
                strBuil.Append("<table>");
                strBuil.Append("<thead>");
                strBuil.Append("<tr>");
                strBuil.Append("<td>Sr.</td>");
                strBuil.Append("<td>Date &amp; Month</td>");
                strBuil.Append("<td>Topic</td>");
                strBuil.Append("<td>Department</td>");
                strBuil.Append("</tr>");
                strBuil.Append("</thead>");
                strBuil.Append("<tbody>");

                int i = 1;
                foreach (DataRow row2 in dt2.Rows)
                {
                    strBuil.Append("<tr>");
                    strBuil.Append("<td> " + i + " </td>");

                    string date = row2[4].ToString();
                    string[] words = date.Split(' ');

                    //string dd = words[0] + "th";
                    string dd = words[0];
                    if (dd == "01")
                    {
                        dd = dd + "st";
                    }
                    else if (dd == "02")
                    {
                        dd = dd + "nd";
                    }
                    else if (dd == "03")
                    {
                        dd = dd + "rd";
                    }
                    else
                    {
                        dd = dd + "th";
                    }

                    string mm = words[1] + ",";
                    string yy = words[2];

                    string finaldate = dd + " " + mm + " " + yy;
                    strBuil.Append("<td> " + finaldate + " </td>");

                    strBuil.Append("<td>" + row2[5] + "</td>");
                    strBuil.Append("<td>" + row2[6] + "</td>");
                    strBuil.Append("</tr>");

                    i++;
                }

                strBuil.Append("</tbody>");
                strBuil.Append("</table>");
            }
        }

        litTable.Text = strBuil.ToString();
    }

}