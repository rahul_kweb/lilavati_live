﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="anesthesiology.aspx.cs" Inherits="anesthesiology" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Anesthesiology</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Anaesthesiology.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--    <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litAnesthesiology" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--                    <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Lilavati Hospital & Research Centre has the leading Anaesthesiology department in the city. Since its inception in 1997 anaesthesia department has seen unprecedented growth in terms of quality of care, techniques and safety which makes it one of the best in the fraternity A dedicated team of anaesthesiologists having a wide experience in the management of all types of complicated cases ensure that comprehensive care is at par with international standards. This ability to provide the highest quality of care is facilitated by having access to the best Anaesthetic machines equipment as well as a complete assortment of pharmacological tools. We use both general and regional anaesthetic techniques either alone or in combination using the latest in nerve block techniques like ultrasound guidance. Our endeavour is to make anaesthesia and surgery a safe and stress free experience.</p>
                            <p>We provide anaesthetic care to over 10000 surgical and diagnostic procedures every year. These patients comprise of all age groups coming for various types of surgeries. We have a team of 13 dedicated full time consultants who are extremely competent and immensely experienced. Consultants are helped by clinical associates for clinical work. All clinical associates are qualified Anaesthetists too.</p>
                            <p>We promise to put all our efforts and knowledge to make journey of our patient stress and pain free throughout the operation, keeping patient safety as our first priority.</p>
                            <p><strong class="green_text uppercase">We care when you are unaware...</strong></p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <p>We give anaesthesia for various surgical specialties like orthopedics, general surgery, obstetrics and gynaecology, IVF, surgical oncology, ENT, ophthalmology,  Neuro and spine gurgery, paediatric surgery, thoracic surgery, vascular surgery, urology and kidney transplants. We also cater Anaesthetic services to cath lab for interventional cardiology and radiology procedures. Endoscopic gastroenterology procedures and various adult and paediatric ward procedures requiring Anaesthesia are also facilitated by our department. We also run out patient department services for pre-operative anaesthetic evaluation.</p>
                            <p>Apart from delivering safe Anaesthesia, our aim is to render patients pain free after surgery. We expertise in management of acute post operative pain by means of patient controlled analgesia (PCA) , epidural analgesia and nerve blocks. Our dedicated pain nurses under the guidance of consultants look after the patients who are on acute pain treatments.  We also conduct painless labour and deliveries using labour epidural analgesia.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <p>To support our team’s excellent clinical skills we also have latest equipments in anaesthesia which give us the ability to provide highest quality of care that matches any international standards. The list includes:</p>
                            <ul class="bullet">
                                <li>Modern anaesthesia </li>
                                <li>Work stations and Vapourisers  </li>
                                <li>Ultra sound machines (for nerve blocks and vascular access),  </li>
                                <li>Fiber-optic bronchoscopes </li>
                                <li>Video-laryngoscopes  </li>
                                <li>Cardiac output monitors </li>
                                <li>Rapid fluid infusors  </li>
                                <li>We strive to be better and keep ourselves updated with latest trends in anaesthesia by conducting regular weekly clinical meetings and updates.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

