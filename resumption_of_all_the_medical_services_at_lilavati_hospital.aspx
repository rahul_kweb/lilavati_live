﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="resumption_of_all_the_medical_services_at_lilavati_hospital.aspx.cs" Inherits="resumption_of_all_the_medical_services_at_lilavati_hospital" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Resumption of All the Medical Services at Lilavati Hospital</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>



                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                             <asp:Literal ID="ltrContent" runat="server"></asp:Literal>



<%--                <p><strong class="green_text uppercase">Introduction</strong></p>
                <p>COVID-19 is changing everything about life and work as we know it. We at Lilavati Hospital are focussed on how to save and treat our patients in the face of this unfolding crisis with quality medical care</p>
                <p>We were the first in the city to have separate screening, Triage and separate corridor for COVID and Non COVID patients.</p>

                <p><strong class="green_text uppercase">Safety Measures</strong></p>
                <p><strong>Screening</strong></p>
                <p>Details of travel and COVID related history, pulse, oxygen saturation level and temperature are noted for all the patients/relatives/visitors entering the hospital. Visitors with fever are sent to our fever clinic and advised not to enter the hospital and patients (for admission) with symptoms (suspected cases) are moved to the Triage area. All others are asked to fill up self-declaration from before entering the hospital premises</p>

                <p><strong>Separate corridors</strong></p>
                <p>A dedicated  pathways for confirmed COVID ,COVID suspects and NON COVID cases have been delineated and the hospital is divided in 3 zones:</p>
                <ul class="bullet">
                    <li>Green Zone is a NON COVID area wherein the patients/relatives can enter the hospital from lobby entrance or emergency department</li>
                    <li>Orange Zone where the suspected cases (those with no swab reports) are accommodated</li>
                    <li>Red Zone for confirmed COVID patients. We have an entirely separate entrance from the basement Triage area and a dedicated lift (which stops only on COVID floor) for these patients</li>


                </ul>

                <p><strong>Staff screening/segregation</strong></p>
                <ul class="bullet">
                    <li>The staffs working in COVID and NON COVID areas are different. The staffs in direct contact with patients / visitors are given PPE kits, masks and OT gowns.</li>
                    <li>Swabs for RT PCR are collected every day and all healthcare workers returning for duty are screened separately for fitness to resume duties</li>
                </ul>

                <p><strong>Admission protocol</strong></p>
                <p>All elective procedures /admission for non-suspected COVID patients planning to get admitted for other treatments are advised to provide HRCT Chest and COVID negative reports before admission</p>
                <p>For all emergencies we do a Antigen Swab test which gives result in 30mins along with HRCT</p>


                <p><strong>Surgical cases safety</strong></p>
                <p>Safe methods for intubation are established</p>


                <p><strong>Following these stringent safety protocols, hospital has resumed all its operations in full force and has opened OT, Cathlab, OPD, Diagnostic services for Non COVID cases also. High end surgeries pertaining to Cardiac, Gastroenterology, Neurology, Orthopaedics and Nephrology are routinely performed for NON COVID cases</strong></p>

                <p><strong>Lilavati Hospital is committed to you and your loved ones, even in the wake of the COVID-19 pandemic while ensuring safety of all the patients and care givers. We adhere to stringent protocols to prevent contamination and to make our regular healthcare services available to all.</strong></p>

                <p><strong>We support & stand by you in pandemic while continuing to provide “More than Healthcare, Human Care”</strong></p>


                <div class="newsbox">
                <ul class="bullet">
                    <li>

                        <a class="fancybox-media" href="https://youtu.be/C21ep9XkxxM" target="_blank" rel="media-gallery">Cancer can prove to be fatal if not treated on Time.Dr.Mohan Menon,Medical Oncologist shares the message of taking timely treatment for Cancer without the fear of COVID at our hospital </a>
                    </li>
                    <li>

                        <a class="fancybox-media" href="https://youtu.be/CcOmpgk7sos" target="_blank" rel="media-gallery">Dr.Abhay Bhave, Consultant - Hematologist instills faith in our hospital safety precautions and urges patients to visit and take timely treatment at our hospital without the fear of COVID </a>
                    </li>
                    <li>
                        <a class="fancybox-media" href="https://youtu.be/e0y6BuYJPGA" target="_blank" rel="media-gallery">Dr.P.S.Ramani,Neurosurgeon at Lilavati Hospital explains how our hospital has taken all the necessary precautions to ensure a COVID free environment to treat NON COVID patients </a>
                    </li>
                    <li>

                        <a class="fancybox-media" href="https://youtu.be/Gx63KCcUHWE" target="_blank" rel="media-gallery">Is Cardiac Surgery Safe during COVID? Here's what our Cardiothoracic Surgeon, Dr.Pavan Kumar has to say </a>
                    </li>
                    <li>

                        <a class="fancybox-media" href="https://youtu.be/18upMZH2I38" target="_blank" rel="media-gallery">Don't let the pandemic affect your precious Pregnancy. Watch Dr.Kiran Coelho ,Consultant - Gynaecologist sharing useful information on Pregnancy Care </a>
                    </li>
                    <li>

                        <a class="fancybox-media" href="https://youtu.be/WsDSAihwExM" target="_blank" rel="media-gallery">Worried for your child's vaccinations during this pandemic? Here's what our expert, Dr.Deepak Ugra ,Consultant - Pediatrician says </a>
                    </li>

                </ul>
                    </div>--%>


    


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />


            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>




        <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox-media')
                     .attr('rel', 'media-gallery')
                     .fancybox({
                         openEffect: 'none',
                         closeEffect: 'none',
                         prevEffect: 'none',
                         nextEffect: 'none',

                         arrows: false,
                         helpers: {
                             thumbs: { width: 75, height: 50 },
                             media: {},
                             buttons: {}
                         }
                     });

        });
    </script>
</asp:Content>

