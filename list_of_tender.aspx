﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="list_of_tender.aspx.cs" Inherits="list_of_tender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" />
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="js/FlowupLabels_plugin.js"></script>
    <link href="select2/select2.min.css" rel="stylesheet" />
    <script src="select2/select2.min.js"></script>
    <script>
        $(function () {
            $(".js-example-basic-hide-search").select2({
                minimumResultsForSearch: Infinity
            });

            $('#dep').click(function () {
                $('#<%=ddlDepartment.ClientID%>').select2('open');
            })

            $('#pos').click(function () {
                $('#<%=ddlCategory.ClientID%>').select2('open');
            })

            var data = $('#<%=ddlDepartment.ClientID%>').val();
            if (data.length > 0 && data != "") {
                $('#dep').addClass("populated");
            } else {
                $('#dep').removeClass("populated");
            }

            var posdata = $('#<%=ddlCategory.ClientID%>').val();
            if (posdata.length > 0 && posdata != "") {
                $('#pos').addClass("populated");
            } else {
                $('#pos').removeClass("populated");
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">List of Tender</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">
            <div class="form_div career_filters FlowupLabels">

                <div class="form_field fl_wrap" id="dep">
                    <label class="fl_label uppercase">
                        Department</label>
                    <asp:DropDownList ID="ddlDepartment" runat="server" class="fl_input" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"
                        AutoPostBack="true" CssClass="js-example-basic-hide-search" Style="width: 100%;">
                        <%--<asp:ListItem Text="" Value=""></asp:ListItem>--%>
                        <asp:ListItem Text="Material (Non-Surgical) Department" Value="Material (Non-Surgical) Department"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form_field fl_wrap" id="pos">
                    <label class="fl_label uppercase">
                        Category</label>
                    <asp:DropDownList ID="ddlCategory" runat="server" class="fl_input" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                        AutoPostBack="true" CssClass="js-example-basic-hide-search" Style="width: 100%;">
                        <%--<asp:ListItem Text="" Value=""></asp:ListItem>--%>
                        <%--<asp:ListItem Text="Printing and stationery" Value="Printing and stationery"></asp:ListItem>
                        <asp:ListItem Text="Engineering AMC/CMC/Hire" Value="Engineering AMC/CMC/Hire"></asp:ListItem>
                        <asp:ListItem Text="Engineering Equipment" Value="Engineering Equipment"></asp:ListItem>--%>
                        <asp:ListItem Value="Bio Medical Equipment">Bio Medical Equipment</asp:ListItem>
                        <asp:ListItem Value="Bio Medical Spare parts">Bio Medical Spare parts</asp:ListItem>
                        <asp:ListItem Value="Bio Medical Consumable">Bio Medical Consumable</asp:ListItem>
                        <asp:ListItem Value="Bio Medical AMC/CMC">Bio Medical AMC/CMC</asp:ListItem>
                        <asp:ListItem Value="Engineering Equipment">Engineering Equipment</asp:ListItem>
                        <asp:ListItem Value="Engineering Spare parts">Engineering Spare parts</asp:ListItem>
                        <asp:ListItem Value="Engineering Consumable">Engineering Consumable</asp:ListItem>
                        <asp:ListItem Value="Engineering AMC/CMC">Engineering AMC/CMC</asp:ListItem>
                        <asp:ListItem Value="Engineering Services">Engineering Services</asp:ListItem>
                        <asp:ListItem Value="IT Equipment">IT Equipment</asp:ListItem>
                        <asp:ListItem Value="IT Software">IT Software</asp:ListItem>
                        <asp:ListItem Value="IT Consumable">IT Consumable</asp:ListItem>
                        <asp:ListItem Value="IT AMC/CMC">IT AMC/CMC</asp:ListItem>
                        <asp:ListItem Value="IT Services">IT Services</asp:ListItem>
                        <asp:ListItem Value="Other Equipment">Other Equipment</asp:ListItem>
                        <asp:ListItem Value="Printing and stationery">Printing and stationery</asp:ListItem>
                        <asp:ListItem Value="Food Raw material">Food Raw material</asp:ListItem>
                        <asp:ListItem Value="Soft Drink & Beverages">Soft Drink & Beverages</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:Button ID="btnViewAll" runat="server" Text="View All" OnClick="btnViewAll_Click"
                    class="submit_btn fade_anim uppercase" Style="float: left; cursor: pointer;" />
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>

            <div class="content_main_box1 text_format">
                <table class="dnb">
                    <thead>
                        <tr>
                            <td>Sr. No.</td>
                            <td>Tendor Code</td>
                            <td>Tender Description</td>
                            <td>Type of Tender</td>
                            <td>Tender Date</td>
                            <td>End Date</td>
                            <td>Last Date & Time of Submission</td>
                            <td>Tender Opening Date & Time</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptTender" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("eId") %>' />
                                    <td data-title="Sr."><%# Eval("Sr") %> </td>
                                    <td data-title="Tendor Code"><%# Eval("eTenderNo") %></td>
                                    <td data-title="Tender Description"><%# Eval("TenderDescription") %></td>
                                    <td data-title="Type of Tender"><%# Eval("TypeOfTender") %></td>
                                    <td data-title="Tender Date"><%# Eval("TenderStartDate") %></td>
                                    <td data-title="End Date"><%# Eval("TenderEndDate") %></td>
                                    <td data-title="Last Date & Time of Submission"><%# Eval("LastTenderReceiveDateTime") %></td>
                                    <td data-title="Tender Opening Date & Time"><%# Eval("TenderOpeningDateTime") %></td>
                                    <td data-title="Apply for Tender"><a href="#tender_form" class="uppercase submit_btn fancybox" style="color: #FFF;">Apply for Tender</a> </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>


                        <%--<tr>
                            <td data-title="Sr.">2.</td>
                            <td data-title="Tendor Code">T - 002</td>
                            <td data-title="Tender Description">Tender Description </td>
                            <td data-title="Type of Tender">One Time</td>
                            <td data-title="Tender Date">10th May 20</td>
                            <td data-title="End Date">15th May 20</td>
                            <td data-title="Last Date & Time of Submission"></td>
                            <td data-title="Tender Opening Date & Time"></td>
                            <td data-title="Apply for Tender"><a href="#tender_form" class="uppercase submit_btn fancybox" style="color: #FFF;">Apply for Tender</a> </td>
                        </tr>--%>
                    </tbody>
                </table>

            </div>



            <div id="tender_form" style="display: none">

                <div class="hc_form_div">

                    <div class="apply_form_tl uppercase1 green_text">
                        Thanks for showing interest in our E Tender... Kindly fill up a small form 
                    </div>
                    <div class="clear"></div>

                    <div class="FlowupLabels hc_form text_format">

                        <div class="form_field fl_wrap" id="Name">
                            <label class="fl_label uppercase">Name</label>
                            <asp:TextBox ID="txtName" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="Designation">
                            <label class="fl_label uppercase">Designation</label>
                            <asp:TextBox ID="txtDesignation" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>

                        <div class="form_field fl_wrap" id="CompanyName">
                            <label class="fl_label uppercase">Company Name</label>
                            <asp:TextBox ID="txtCompanyName" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>


                        <div class="form_field fl_wrap" id="ContactNos">
                            <label id="spl" class="fl_label uppercase">Contact Nos</label>
                            <asp:TextBox ID="txtContactNos" runat="server" class="fl_input" MaxLength="13" onKeyPress='javascript:return onlyNumbers();' autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>

                        <div class="form_field fl_wrap" id="EmailAddress">
                            <label class="fl_label uppercase">Email Address</label>
                            <asp:TextBox ID="txtEmailAddress" runat="server" class="fl_input" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="form_field fl_wrap" id="Remarks">
                            <label class="fl_label uppercase" for="Remarks">Remarks</label>
                            <asp:TextBox ID="txtRemarks" runat="server" class="fl_input" TextMode="MultiLine" autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="clear"></div>
                        <div class="clear"></div>

                        <p class="green_text" style="display: none"><strong>*To ensure that your pledge is fulfilled, discuss this important decision with your Next of Kin , family doctor and someone close to you.</strong></p>

                        <div class="clear"></div>

                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="submit_btn fade_anim uppercase" Style="cursor: pointer;" OnClick="btnSubmit_Click" OnClientClick='javascript:return CheckSave();' />

                    </div>






                    <div class="clear"></div>
                </div>

            </div>

            <div id="showmsg" class="hc_form_div" style="display: none;">

                <div class="apply_form_tl uppercase green_text">Thank you! </div>
                <div class="clear"></div>

                <div class="hc_thanks text_format">
                    <p><strong class="green_text">Your details has been successfully sent. We will contact you very soon!</strong></p>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>
        <asp:HiddenField ID="hdneId" runat="server" />
        <asp:HiddenField ID="hdnTenderCode" runat="server" />
        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="/js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="/js/FlowupLabels_plugin.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/jquery.fancybox.css" />
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js"></script>

    <script>
        $(function () {

            //fancy box
            //$('.fancybox').fancybox({
            //    width: 200
            //});

            $(".fancybox").fancybox({
                'width': 400,
                'height': 300,
                'autoSize': true
            });

            //get Tender Code
            $(".dnb").on('click', '.submit_btn', function () {
                // get the current row
                var currentRow = $(this).closest("tr");
                var tenderCode = currentRow.find("td:eq(1)").text(); // get current row 2nd TD
                var eId = "";
                $(this).closest('tr').find("input").each(function () {
                    eId = this.value;
                });
                $("#<%=hdneId.ClientID%>").val(eId);
                $("#<%=hdnTenderCode.ClientID%>").val(tenderCode);
            });

        });

        //number validation
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        //form validation
        function CheckSave() {
            var Name = $('#<%=txtName.ClientID%>').val();
            var Designation = $('#<%=txtDesignation.ClientID%>').val();
            var CompanyName = $('#<%=txtCompanyName.ClientID%>').val();
            var Mobile = $('#<%=txtContactNos.ClientID%>').val();
            var EmailAddress = $('#<%=txtEmailAddress.ClientID%>').val();
            var Remarks = $('#<%=txtRemarks.ClientID%>').val();

            var blank = false;

            if (Name == '') {
                $("#Name").addClass("error");
                blank = true;
            }
            else {
                $("#Name").removeClass("error");
            }

            if (Designation == '') {
                $("#Designation").addClass("error");
                blank = true;
            }
            else {
                $("#Designation").removeClass("error");
            }

            if (CompanyName == '') {
                $("#CompanyName").addClass("error");
                blank = true;
            }
            else {
                $("#CompanyName").removeClass("error");
            }

            if (Mobile == '') {
                $("#ContactNos").addClass("error");
                blank = true;
            }
            else {
                if (Mobile.length < 10) {
                    $("#ContactNos").addClass("error");
                    blank = true;
                }
                else {
                    $("#ContactNos").removeClass("error");
                }
            }

            if (EmailAddress == '') {
                $("#EmailAddress").addClass("error");
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(EmailAddress)) {
                    $("#EmailAddress").addClass("error");
                    alert("Enter valid email address.");
                    blank = true;
                }
                else {
                    $("#EmailAddress").removeClass("error");
                }
            }


            if (Remarks == '') {
                $("#Remarks").addClass("error");
                blank = true;
            }
            else {
                $("#Remarks").removeClass("error");
            }


            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }

        //success msg
        function ShowPopUp() {
            $(function () {
                $("#showmsg").fancybox().trigger('click');
            });
        }
    </script>



</asp:Content>

