﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Routing" %>
<script RunAt="server">
    void Application_Start(object sender, EventArgs e)
    {
        RegisterRoutes(RouteTable.Routes);
    }

    static void RegisterRoutes(RouteCollection routes)
    {
        //routes.MapPageRoute("SecondPage", "Customers", "Customers.aspx");


        routes.MapPageRoute("Home", "", "~/index.aspx");

        /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx--------Dashboard Route Url--------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
        routes.MapPageRoute("login", "login", "~/login.aspx");
        routes.MapPageRoute("registration", "registration", "~/registration.aspx");
        routes.MapPageRoute("Welcome", "Dashboard/Welcome", "~/Dashboard/Welcome.aspx");
        routes.MapPageRoute("Welcome1", "Welcome", "~/Dashboard/Welcome.aspx");
        routes.MapPageRoute("book_appointment", "Dashboard/Book-An-Appointment", "~/Dashboard/book_appointment.aspx");
        routes.MapPageRoute("appointment_history", "Dashboard/Appointment-History", "~/Dashboard/appointment_history.aspx");
        routes.MapPageRoute("update_profile", "Dashboard/Profile-Details", "~/Dashboard/update_profile.aspx");
        routes.MapPageRoute("change_password", "Dashboard/Change-Password", "~/Dashboard/change_password.aspx");


        /*xxxxxxxxxxxxxxxxxxxxxxxxxxxx-------- Front-End Route Url--------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

        routes.MapPageRoute("Introduction", "Introduction", "~/introduction.aspx");
        routes.MapPageRoute("our_founders", "Founder", "~/our_founders.aspx");
        routes.MapPageRoute("board_of_trustees", "Board-of-Trustees", "~/board_of_trustees.aspx");
        routes.MapPageRoute("management", "Management", "~/management.aspx");
        routes.MapPageRoute("mission_motto", "Mission-Motto", "~/mission_motto.aspx");
        routes.MapPageRoute("awards_accreditations", "Awards-Accreditations", "~/awards_accreditations.aspx");
        routes.MapPageRoute("media_press_releases", "Media-Press-Releases", "~/media_press_releases.aspx");
        routes.MapPageRoute("statutory_compilance", "Statutory-Compilance", "~/statutory_compilance.aspx");


        routes.MapPageRoute("centres_of_excellence", "Centres-of-Excellence", "~/centres_of_excellence.aspx");
        routes.MapPageRoute("critical_care", "Critical-Care", "~/critical_care.aspx");
        routes.MapPageRoute("day_care", "Day-Care", "~/day_care.aspx");
        routes.MapPageRoute("physiotherapy", "Physiotherapy", "~/physiotherapy.aspx");
        routes.MapPageRoute("blood_bank", "Blood-Bank", "~/blood_bank.aspx");
        routes.MapPageRoute("ambulance", "Ambulance", "~/ambulance.aspx");
        routes.MapPageRoute("visa_investigation", "Visa-Investigation", "~/visa_investigation.aspx");
        routes.MapPageRoute("out_patient", "Out-Patient", "~/out_patient.aspx");
        routes.MapPageRoute("emergency_trauma", "Emergency-Trauma", "~/emergency_trauma.aspx");
        routes.MapPageRoute("dialysis_centre", "Dialysis-Centre", "~/dialysis_centre.aspx");
        routes.MapPageRoute("diagnostics", "Diagnostics", "~/diagnostics.aspx");
        routes.MapPageRoute("pharmacy", "Pharmacy", "~/pharmacy.aspx");
        routes.MapPageRoute("health_checkup", "Health-Checkup", "~/health_checkup.aspx");
        routes.MapPageRoute("social_initiative", "Social-Initiative", "~/social_initiative.aspx");


        routes.MapPageRoute("Doctor", "Doctor", "~/find_a_doctor.aspx");
        routes.MapPageRoute("Doctorprofiles", "Doctorprofile", "~/doctor_profile.aspx");

        routes.MapPageRoute("Doctorprofiles_by_id", "Doctorprofile/{ID}", "~/doctor_profile.aspx");
        routes.MapPageRoute("Doctorprofiles_by_id_consid", "Doctorprofile/{ID}/{ConsDocId}", "~/doctor_profile.aspx");

        routes.MapPageRoute("tariff", "Tariff", "~/tariff.aspx");
        routes.MapPageRoute("international_patients", "International-Patients", "~/international_patients.aspx");
        routes.MapPageRoute("inpatients", "Inpatients", "~/inpatients.aspx");
        routes.MapPageRoute("patients_education_brochure", "Patients-Education-Brochure", "~/patients_education_brochure.aspx");
        routes.MapPageRoute("empanelled_corporates", "Empanelled-Corporates", "~/empanelled_companies.aspx");
        routes.MapPageRoute("empanelled_insurance_Co_TPAs", "Empanelled-Insurance-Co-TPAs", "~/empanelled_insurance_companies_tpa.aspx");
        routes.MapPageRoute("feedback", "Feedback", "~/feedback.aspx");


        routes.MapPageRoute("visiting_hours", "Visiting-Hours", "~/visiting_hours.aspx");
        routes.MapPageRoute("convenience_facilities", "Convenience-Facilities", "~/convenience_facilities.aspx");
        routes.MapPageRoute("dos_donts", "Dos-Donts", "~/dos_donts.aspx");
        routes.MapPageRoute("visitors_policy", "Visitors-Policy", "~/visitors_policy.aspx");

        routes.MapPageRoute("muhs_Fellowship", "MUHS_Fellowship", "~/muhs_Fellowship.aspx");
        routes.MapPageRoute("nurses", "Nurses", "~/nurses.aspx");
        routes.MapPageRoute("dnb", "DNB", "~/dnb.aspx");
        routes.MapPageRoute("research", "Research", "~/research.aspx");
        routes.MapPageRoute("lhmt", "LHMT", "~/lhmt.aspx");
        routes.MapPageRoute("cme", "CME", "~/cme.aspx");
        routes.MapPageRoute("careers", "Careers", "~/careers.aspx");
        routes.MapPageRoute("doctors", "Doctors", "~/doctors.aspx");


        routes.MapPageRoute("contact_us", "Contact-Us", "~/contact_us.aspx");


        routes.MapPageRoute("Home1", "Home", "~/index.aspx");

        routes.MapPageRoute("donate_blood", "Donate-Blood", "~/donate_blood.aspx");
        routes.MapPageRoute("photo_gallery", "Photo-Gallery", "~/photo_gallery.aspx");
        routes.MapPageRoute("pledge_your_eyes", "Pledge-Your-Eyes", "~/pledge_your_eyes.aspx");
        routes.MapPageRoute("testimonials", "Testimonials", "~/testimonials.aspx");
        routes.MapPageRoute("privacy_policy", "Privacy_policy", "~/privacy_policy.aspx");
        routes.MapPageRoute("sample_collection_faq", "sample-collection-faq", "~/SampleCollectionFaq.aspx");
        routes.MapPageRoute("hydrotherapy_centre", "Hydrotherapy-Centre", "~/hydrotherapy.aspx");

        routes.MapPageRoute("covid_antibody_test", "covid-antibody-test", "~/covid_antibody_test.aspx");

        routes.MapPageRoute("resumption_of_all_the_medical_services_at_lilavati_hospital", "resumption-of-all-the-medical-services-at-lilavati-hospital", "~/resumption_of_all_the_medical_services_at_lilavati_hospital.aspx");

        routes.MapPageRoute("covid_19_video_rehabilitation_services", "covid-19-video-rehabilitation-services", "~/covid_19_video_rehabilitation_services.aspx");

        routes.MapPageRoute("post_covid_care_healthcare_packages", "post-covid-care-healthcare-packages", "~/post_covid_care_healthcare_packages.aspx");

        routes.MapPageRoute("Feedback_InPatient", "feedback-inpatient", "~/Feedback_InPatient.aspx");
        routes.MapPageRoute("Feedback_OutPatient", "feedback-outpatient", "~/Feedback_OutPatient.aspx");
        routes.MapPageRoute("Feedback_Health_Checkup", "feedback-healthcheckup", "~/Feedback_Health_Checkup.aspx");
        routes.MapPageRoute("Suggestions_Complaints", "Suggestions-Complaints", "~/Suggestions_Complaints.aspx");
        /*** COE Inner Pages ***/

        routes.MapPageRoute("anesthesiology", "Anesthesiology", "~/anesthesiology.aspx");
        routes.MapPageRoute("audiology_speech_therapy", "Audiology-Speech-Therapy", "~/audiology_speech_therapy.aspx");
        routes.MapPageRoute("bariatric_surgery", "Bariatric-Surgery", "~/bariatric_surgery.aspx");
        routes.MapPageRoute("cardiology_CVTS", "Cardiology-CVTS", "~/cardiology_CVTS.aspx");
        routes.MapPageRoute("chest_medicine", "Chest-Medicine", "~/chest_medicine.aspx");
        routes.MapPageRoute("colorectal_surgery", "Colorectal-Surgery", "~/colorectal_surgery.aspx");
        routes.MapPageRoute("dental", "Dental", "~/dental.aspx");
        routes.MapPageRoute("dermo_cosmetology", "Dermo-Cosmetology", "~/dermo_cosmetology.aspx");
        routes.MapPageRoute("ent", "Ent", "~/ent.aspx");
        routes.MapPageRoute("gastroenterology_gastrointestinal_surgery", "Gastroenterology-Gastrointestinal-Surgery", "~/gastroenterology_gastrointestinal_surgery.aspx");
        routes.MapPageRoute("general_surgery", "General-Surgery", "~/general_surgery.aspx");
        routes.MapPageRoute("gynaecology_obstetrics", "Gynaecology-Obstetrics", "~/gynaecology_obstetrics.aspx");
        routes.MapPageRoute("haematology", "Haematology", "~/haematology.aspx");
        routes.MapPageRoute("headache_migraine_clinic", "Headache-Migraine-Clinic", "~/headache_migraine_clinic.aspx");
        routes.MapPageRoute("hip_replacement", "Hip-Replacement", "~/hip_replacement.aspx");
        routes.MapPageRoute("infectious_diseases", "Infectious-Diseases", "~/infectious_diseases.aspx");
        routes.MapPageRoute("internal_medicine", "Internal-Medicine", "~/internal_medicine.aspx");
        routes.MapPageRoute("minimally_invasive_surgery", "Minimally-Invasive-Surgery", "~/minimally_invasive_surgery.aspx");
        routes.MapPageRoute("nephrology", "Nephrology", "~/nephrology.aspx");
        routes.MapPageRoute("neurology_neuro_surgery", "Neurology-Neuro-Surgery", "~/neurology_neuro_surgery.aspx");
        routes.MapPageRoute("oncology", "Oncology", "~/oncology.aspx");
        routes.MapPageRoute("ophthalmology", "Ophthalmology", "~/ophthalmology.aspx");
        routes.MapPageRoute("orthopaedics", "Orthopaedics", "~/orthopaedics.aspx");
        routes.MapPageRoute("Pediatrics_and_paediatric_surgery", "Pediatrics-And-Paediatric-Surgery", "~/Pediatrics_and_paediatric_surgery.aspx");
        routes.MapPageRoute("plastic_surgery", "Plastic-Surgery", "~/plastic_surgery.aspx");
        routes.MapPageRoute("psychiatry_psychology_neuropsychology", "Psychiatry-Psychology-Neuropsychology", "~/psychiatry_psychology_neuropsychology.aspx");
        routes.MapPageRoute("rheumatology", "Rheumatology", "~/rheumatology.aspx");
        routes.MapPageRoute("sleep_medicine", "Sleep-Medicine", "~/sleep_medicine.aspx");
        routes.MapPageRoute("urology", "Urology", "~/urology.aspx");
        routes.MapPageRoute("vascular_endovascular_surgery", "Vascular-Endovascular-Surgery", "~/vascular_endovascular_surgery.aspx");
        routes.MapPageRoute("pain-management", "Pain-Management", "~/pain_management.aspx");

        routes.MapPageRoute("interventional_radiology", "Interventional_Radiology", "~/interventional_radiology.aspx");
        routes.MapPageRoute("nuclear_medicine", "Nuclear_Medicine", "~/nuclear_medicine.aspx");

        routes.MapPageRoute("updates", "Updates", "~/updates.aspx");
        routes.MapPageRoute("packages_details", "Packages-Details/{ID}", "~/packages_details.aspx");
        routes.MapPageRoute("search_results", "Search_Results/{key}", "~/search_results.aspx");
        //routes.MapPageRoute("book_appointment_page", "Book-Appointment", "~/book_appointment_page.aspx");
        routes.MapPageRoute("floor_directory", "Floor-Directory", "~/floor_directory.aspx");
        routes.MapPageRoute("book_appointment_page", "Book-Appointment", "~/book_appointment_page.aspx");
        routes.MapPageRoute("book_appointment_page1", "Book-Appointment/{ID}", "~/book_appointment_page.aspx");

        routes.MapPageRoute("hair_transplant", "Hair-Transplant", "~/hair_transplant.aspx");
        routes.MapPageRoute("diabetology_endocrinology", "Diabetology-Endocrinology", "~/diabetology_endocrinology.aspx");
        routes.MapPageRoute("accommodation", "Accommodation", "~/accommodation.aspx");
        routes.MapPageRoute("write_to_us", "Write-To-Us", "~/write_to_us.aspx");
        routes.MapPageRoute("request_an_estimate", "Request-An-Estimate", "~/request_an_estimate.aspx");
        routes.MapPageRoute("patients_rights_responsibilities", "Patients-Rights-Responsibilities", "~/patients_rights_responsibilities.aspx");
        routes.MapPageRoute("visa_invitation_form", "Visa-Invitation-Form", "~/visa_invitation_form.aspx");

        routes.MapPageRoute("faqs_for_fitness_package", "Faq's-For-Fitness-Package", "~/faq.aspx");
        routes.MapPageRoute("corneal_transplant", "Corneal-Transplant", "~/transplant.aspx");

        routes.MapPageRoute("forgot_password", "ForgotPassword", "~/ForgotPassword.aspx");
        routes.MapPageRoute("register_with_lhrc", "register_with_lhrc", "~/register_with_lhrc.aspx");
        routes.MapPageRoute("Stents_and_TKR_implants", "Stents-and-TKR-implants", "~/Stents_and_TKR_implants.aspx");
        routes.MapPageRoute("interventional_neuroradiology", "Interventional-Neuroradiology", "~/interventional_neuroradiology.aspx");
        routes.MapPageRoute("video_consultation", "Video-Consultation", "~/video_consultation.aspx");
        routes.MapPageRoute("tnc", "tnc", "~/tnc.aspx");
        routes.MapPageRoute("list_of_tender", "List-of-Tender", "~/list_of_tender.aspx");
        routes.MapPageRoute("privacypolicy", "privacy-policy", "~/privacypolicy.aspx");

        /*payment response page*/
        routes.MapPageRoute("AppointmentResponse", "Dashboard/AppointmentResponse", "~/Dashboard/TestResponsePageaspx.aspx");
        routes.MapPageRoute("AppointmentResponseCancel", "AppointmentCancel", "~/ccavResponseHandler.aspx");
        routes.MapPageRoute("Clear", "Clear", "~/ClearCookies.aspx");
        routes.MapPageRoute("Act", "Act", "~/Act.aspx");
        routes.MapPageRoute("lactation", "lactation", "~/lactation.aspx");
        routes.MapPageRoute("liver-transplant", "liver-transplant", "~/liver_clinic.aspx");

        /*xxxxx web view pages xxxxxx*/
        routes.MapPageRoute("web-ambulance", "webView/Web-Ambulance", "~/webView/ambulance.aspx");
        routes.MapPageRoute("web-blood-bank", "webView/Web-Blood-Bank", "~/webView/blood_bank.aspx");
        routes.MapPageRoute("web-critical-care", "webView/Web-Critical-Care", "~/webView/critical_care.aspx");
        routes.MapPageRoute("web-day-care", "webView/Web-Day-Care", "~/webView/day_care.aspx");
        routes.MapPageRoute("web-diagnostics", "webView/Web-Diagnostics", "~/webView/diagnostics.aspx");
        routes.MapPageRoute("web-dialysis-centre", "webView/Web-Dialysis-Centre", "~/webView/dialysis_centre.aspx");
        routes.MapPageRoute("web-emergency-trauma", "webView/Web-Emergency-Trauma", "~/webView/emergency_trauma.aspx");
        routes.MapPageRoute("web-health-checkup", "webView/Web-Health-Checkup", "~/webView/health_checkup.aspx");
        routes.MapPageRoute("web-packages_details", "webView/Web-Packages-Details/{ID}", "~/webView/packages_details.aspx");
        routes.MapPageRoute("web-interventional_radiology", "webView/Web-Interventional_Radiology", "~/webView/interventional_radiology.aspx");
        routes.MapPageRoute("web-out-patient", "webView/Web-Out-Patient", "~/webView/out_patient.aspx");
        routes.MapPageRoute("web-pharmacy", "webView/Web-Pharmacy", "~/webView/pharmacy.aspx");
        routes.MapPageRoute("web-physiotherapy", "webView/Web-Physiotherapy", "~/webView/physiotherapy.aspx");
        routes.MapPageRoute("web-social-initiative", "webView/Web-social-Initiative", "~/webView/social_initiative.aspx");
        routes.MapPageRoute("web-visa-investigation", "webView/Web-Visa-Investigation", "~/webView/visa_investigation.aspx");
        routes.MapPageRoute("web-book-an-appointment", "webView/web-book-an-appointment", "~/webView/book_appointment_page.aspx");

        /*xxxxx web view coe pages xxxxxx*/
        routes.MapPageRoute("web-centres-of-excellence", "webView/Web-Centres-of-Excellence", "~/webView/centres_of_excellence.aspx");
        routes.MapPageRoute("web-anesthesiology", "webView/Web-Anesthesiology", "~/webView/anesthesiology.aspx");
        routes.MapPageRoute("web-audiology-speech-therapy", "webView/Web-Audiology-Speech-Therapy", "~/webView/audiology_speech_therapy.aspx");
        routes.MapPageRoute("web-audiology-bariatric-surgery", "webView/Web-Bariatric-Surgery", "~/webView/bariatric_surgery.aspx");
        routes.MapPageRoute("web-audiology-cardiology-CVTS", "webView/Web-Cardiology-CVTS", "~/webView/cardiology_CVTS.aspx");
        routes.MapPageRoute("web-audiology-chest-medicine", "webView/Web-Chest-Medicine", "~/webView/chest_medicine.aspx");
        routes.MapPageRoute("web-corneal-transplant", "webView/web-Corneal-Transplant", "~/webView/transplant.aspx");
        routes.MapPageRoute("web-colorectal-surgery", "webView/web-Colorectal-Surgery", "~/webView/colorectal_surgery.aspx");
        routes.MapPageRoute("web-dental", "webView/web-Dental", "~/webView/dental.aspx");
        routes.MapPageRoute("web-diabetology-endocrinology", "webView/web-Diabetology-Endocrinology", "~/webView/diabetology_endocrinology.aspx");
        routes.MapPageRoute("web-ent", "webView/web-Ent", "~/webView/ent.aspx");
        routes.MapPageRoute("web-gastroenterology_gastrointestinal_surgery", "webView/web-Gastroenterology-Gastrointestinal-Surgery", "~/webView/gastroenterology_gastrointestinal_surgery.aspx");
        routes.MapPageRoute("web-general-surgery", "webView/web-General-Surgery", "~/webView/general_surgery.aspx");
        routes.MapPageRoute("web-gynaecology-obstetrics", "webView/web-Gynaecology-Obstetrics", "~/webView/gynaecology_obstetrics.aspx");
        routes.MapPageRoute("web-haematology", "webView/web-Haematology", "~/webView/haematology.aspx");
        routes.MapPageRoute("web-hair-transplant", "webView/web-Hair-Transplant", "~/webView/hair_transplant.aspx");
        routes.MapPageRoute("web-headache-migraine-clinic", "webView/web-Headache-Migraine-Clinic", "~/webView/headache_migraine_clinic.aspx");
        routes.MapPageRoute("web-infectious-diseases", "webView/web-Infectious-Diseases", "~/webView/infectious_diseases.aspx");
        routes.MapPageRoute("web-internal-medicine", "webView/web-Internal-Medicine", "~/webView/internal_medicine.aspx");
        routes.MapPageRoute("web-lactation", "webView/web-lactation", "~/webView/lactation.aspx");
        routes.MapPageRoute("web-liver_clinic", "webView/web-liver-transplant", "~/webView/liver_clinic.aspx");
        routes.MapPageRoute("web-minimally_invasive_surgery", "webView/web-Minimally-Invasive-Surgery", "~/webView/minimally_invasive_surgery.aspx");
        routes.MapPageRoute("web-nephrology", "webView/web-Nephrology", "~/webView/nephrology.aspx");
        routes.MapPageRoute("web-neurology-neuro-surgery", "webView/web-Neurology-Neuro-Surgery", "~/webView/neurology_neuro_surgery.aspx");
        routes.MapPageRoute("web-oncology", "webView/web-Oncology", "~/webView/oncology.aspx");
        routes.MapPageRoute("web-ophthalmology", "webView/web-Ophthalmology", "~/webView/ophthalmology.aspx");
        routes.MapPageRoute("web-orthopaedics", "webView/web-Orthopaedics", "~/webView/orthopaedics.aspx");
        routes.MapPageRoute("web-pain-management", "webView/web-Pain-Management", "~/webView/pain_management.aspx");
        routes.MapPageRoute("web-Pediatrics-and-paediatric-surgery", "webView/web-Pediatrics-And-Paediatric-Surgery", "~/webView/Pediatrics_and_paediatric_surgery.aspx");
        routes.MapPageRoute("web-plastic-surgery", "webView/web-Plastic-Surgery", "~/webView/plastic_surgery.aspx");
        routes.MapPageRoute("web-psychiatry_psychology_neuropsychology", "webView/web-Psychiatry-Psychology-Neuropsychology", "~/webView/psychiatry_psychology_neuropsychology.aspx");
        routes.MapPageRoute("web-rheumatology", "webView/web-Rheumatology", "~/webView/rheumatology.aspx");
        routes.MapPageRoute("web-sleep-medicine", "webView/web-Sleep-Medicine", "~/webView/sleep_medicine.aspx");
        routes.MapPageRoute("web-urology", "webView/web-Urology", "~/webView/urology.aspx");
        routes.MapPageRoute("web-vascular-endovascular-surgery", "webView/web-Vascular-Endovascular-Surgery", "~/webView/vascular_endovascular_surgery.aspx");
        routes.MapPageRoute("web-interventional_neuroradiology", "webView/web-Interventional-Neuroradiology", "~/webView/interventional_neuroradiology.aspx");
    }


    void Session_Start(object sender, EventArgs e)
    {
        Session.Timeout = 60;

        // Code that runs when a new session is started
        if (Session[AppKey.UserName] != null)
        {
            //Redirect to Welcome Page if Session is not null  
            Response.Redirect("Dashboard/WelcomeDashboard.aspx");
        }
        else
        {
            //Redirect to Login Page if Session is null & Expires   
            //Response.Redirect("LoginForm.aspx");

        }

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
    //protected void Application_BeginRequest(object sender, EventArgs e)
    //{
    //    if (HttpContext.Current.Request.Url.AbsolutePath.Contains("index.aspx"))
    //    {
    //        HttpContext.Current.Response.Redirect("Home");
    //    }
    //    else
    //    {
    //    }
    //}
</script>
