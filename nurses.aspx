﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="nurses.aspx.cs" Inherits="nurses" %>

<%@ Register TagName="Professionals" TagPrefix="menu" Src="Control/professionals.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Nurses</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--  <p><strong>Nursing combines the art of compassion with the science of healthcare.</strong></p>
                <p>The Department of Nursing of Lilavati Hospital and Research Centre encompasses more than 600 Professional Registered Nurses, thus making it one of the biggest departments in the hospital. The faculty and staff in the Department of Nursing are committed to rendering high quality patient care & service with compassion, excellence in learning, teaching & research.</p>

                <div class="accd_box">
                    <div class="hd c_expand_hd">Aim</div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Nursing Department believes in <strong class="green_text">‘Quality in every activity in order to consistently meet & exceed.... Patient expectations!’</strong></p>
                        <p><strong>The Department of Nursing aims to provide:</strong></p>
                        <ul class="bullet">
                            <li>Provide quality Nursing Services to our patients by preventing illness, promoting and restoring their health.</li>
                            <li>Provide individualized holistic patient care utilizing the nursing process.</li>
                            <li>Establish effective communication, cooperation and coordination with all health professionals and administrative services involved in the planning and delivery of patient care.</li>
                            <li>Foster a positive work atmosphere based on team spirit and trust among nursing staff and members of the health team to ensure job satisfaction.</li>
                            <li>Maintain standards of professional practice among nurses by implementing a continuous in-service training program.</li>
                            <li>Assess and evaluate our quality of nursing care on an on-going basis through quality improvement and nursing audits.</li>
                        </ul>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Scope of Services</div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>The scope of nursing services is to provide  comprehensive care to the patients. It covers all inpatients as well as outpatients receiving treatment  in the hospital.</p>
                        <ul class="bullet">
                            <li>Nursing Administration.</li>
                            <li>Staff Development.</li>
                            <li>Infection Control.</li>
                            <li>Smooth functioning of In Patient areas.</li>
                            <li>Giving necessary support to Clinical Department.</li>
                        </ul>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Specialized Nursing</div>
                    <div class="newsbox accd_content c_expand_box">

                        <ul class="bullet">
                            <li>
                                <p><strong class="uppercase">Infection Control Nurse</strong></p>
                                <p>Infection Control Nurse maintains standards, policy & procedures in infection control measures in the hospital and minimizes hospital acquired infections in patients and staff & clinically supervises all category staff. The ICN is one of the key members in the HICC committee who work on two fronts simultaneously – i.e. Monitoring & Preventing.</p>
                                <p>Infection control nursing is a novel speciality which is gaining popularity. The infection control nurse with other members of the team monitors the infection rates throughout the hospital and is responsible  for making sure the infection control practices mandated are followed.</p>
                                <p>There are 2 infection Control Nurses who monitor infection control measures, educate, coordinate & conduct research activities at Lilavati Hospital and Research Centre.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">Pain Management Nurse</strong></p>
                                <p>Pain Management Nurse is responsible and accountable to ensure that a patient receives appropriate evidence-based nursing assessment and intervention which effectively treats the patient’s pain and meets the recognized standard of care.</p>
                                <p>The use of pharmacological and non-pharmocological intervention to control the patient’s identified pain. Pain Management extends beyond pain relief, encompassing the patient’s quality of life, ability to work productively, to enjoy recreation, to function normally in the family and society.</p>
                                <p>At Lilavati Hospital we have 2 Pain Management Nurses who assess the patients for pain & they take all reasonable means to alleviate the patient’s pain & intervene appropriately under the guidance of the Anaesthetists.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">OR Nursing</strong></p>
                                <p>OR Nursing is a specialty in itself; with the state of the art 12 Operation Theatres at Lilavati Hospital where all kinds of complex surgeries are performed the registered Nurse gets a rich & varied experience & become a specialized OR Nurse. The Registered Nurse after joining the Operation theatre duty gets a complete package of Orientation & circulating to different OTs & CSSD, thus facilitating complete learning.</p>
                                <p>Initiatives to improve patient safety and efficiency in operating theatres by introducing the WHO Safe Surgery Checklist & Quality Assurance programme for OT.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">Critical Care Nursing</strong></p>
                                <p>At Lilavati Hospital there are 5 Intensive care units which covers a variety of surgical speciality intensive care units, medical, neonatal and coronary care unit. Competent critical care nurses, under the guidance of able nursing leaders, render comprehensive nursing care to the critically ill patients. The state- of- the art equipment and best and class practice makes it a unique arena of learning.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">Quality Assurance in Nursing</strong></p>
                                <p>The team of Nursing Supervisors, Nurse Officers along with the staff development team strives to maintain the quality of nursing care in accordance with the international standards. Several quality indicators are identified and tracked monthly. Reports based on this monthly tracking are analyzed and communicated to the management there by helping them in developing effective procedures.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">Oncology Nursing</strong></p>
                                <p>A specialized Heamatology Oncology unit with the advanced technology exists at Lilavati Hospital only the trained Nurses provide Chemotherapy to the patients with different Oncology ailments.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">Stroke Nursing</strong></p>
                                <p>Nurses play a pivotal role in all phases of care of the stroke patient. Stroke nursing is a specialist area of clinical, practice that focuses on meeting the holistic needs of individuals and families arising from the varied consequences of stroke. Nurses are actively involved in all stages of the stroke journey including hyperacute and acute care, rehabilitation, community care and long-term support.</p>
                                <p>The stroke unit at Lilavati Hospital a speciaslised 4 bedded unit where stroke patients are catered to providing opportunity to Nurses to provide comprehensive care to them.</p>
                            </li>

                            <li>
                                <p><strong class="uppercase">CPR Team Nursing</strong></p>
                                <p>Specialized Nurses who are trained in Basic Life Support and Advanced Life Support participate in Medical emergencies and Code Blue Events.</p>
                            </li>
                        </ul>


                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Staff Development and Key Activities of Nursing Department</div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Ongoing training and development are important initiatives for delivering quality care to patients. The staff development department has a team of qualified personnel to train the staff to achieve the goal.</p>
                        <ul class="bullet">
                            <li>Orientation programme/ Induction Training</li>
                            <li>In-service education</li>
                            <li>Procedure protocol training</li>
                            <li>BLS training & ACLS training</li>
                            <li>Formulation of nursing policies & protocol</li>
                            <li>Nursing audits, Question paper,</li>
                            <li>Quiz</li>
                            <li>Workshops & Seminars </li>
                            <li>Nursing Satisfaction Audit</li>
                        </ul>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Quality Initiatives</div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Quality improvement initiatives to improve the delivery of care, increase patient satisfaction and improve the overall patient experience.</p>
                        <ul class="bullet">
                            <li>Audit and Research team</li>
                            <li>Quality Team </li>
                            <li>Educational Committee</li>
                            <li>Operational Team</li>
                            <li>NABH Team</li>
                        </ul>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Events </div>
                    <div class="newsbox accd_content c_expand_box">
                        <ul class="bullet">
                            <li>Patient Safety Day</li>
                            <li>Nurses Day</li>
                            <li>Global Pain Update for Nurses</li>
                            <li>Organ Donation Awareness Programme for Nurses</li>
                        </ul>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Prizes and Laurels</div>
                    <div class="newsbox accd_content c_expand_box">
                        <p>Our talented Nurses have bagged prizes in various activities in the hospital and inter hospital competetitions.</p>
                        <ul class="bullet">
                            <li>Slogan Competeition on ‘Evidence Based Pain management update’ @ Bhakti Vedant Hospital.</li>
                            <li>TNAI annual sports meet.</li>
                        </ul>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- XXXXXXXXXXXXXXXXXX -->

                <div class="accd_box">
                    <div class="hd c_expand_hd">Notes of Appreciation</div>
                    <div class="newsbox accd_content c_expand_box">
                        Coming Soon...
    
    <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>--%>

                <!-- XXXXXXXXXXXXXXXXXX -->


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Professionals ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

