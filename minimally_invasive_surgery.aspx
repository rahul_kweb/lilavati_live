﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="minimally_invasive_surgery.aspx.cs" Inherits="minimally_invasive_surgery" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Minimally Invasive Surgery</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Minimally_Invasive_Surgery.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li style="display:none;"><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr' style="display:none;">
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%--<div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>After the first laparoscopic cholecystectomy that happened in the early 1990, Minimally Invasive Surgeryhas made impressive inroads into almost all the domains of abdominal and pelvic surgery; in fact this speciality has become standard of care for many of the common indications. A wide spectrum of these surgeries, more commonly referred to as “Key-hole Surgery” or “laparoscopic surgery” is performed by various departments at our hospital.We follow high level of safety protocols and sterilization techniques for enhancing patient safety and comfort.</p>
                            <p>Lilavati Hospital has one of the finest groups of consultants well known in the field of advanced Laparoscopic and Thorascopic surgery. All types of major gastrointestinal, bariatric, and pulmonary, urology and gynaecology operations are routinely performed by various consultants’ indifferent departments. Advanced imaging departments systems, energy sources and anaesthesia monitoring system make this surgery extremely safe in our institute. Patients undergoing laparoscopic surgery leave the hospital in short time with smiling faces. Our success is extremely high with negligible conversion rate to open surgery.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Cholecystectomy</li>
                                <li>Appendectomy</li>
                                <li>Lymph node biopsies</li>
                                <li>Nissen’s fundoplication (for gastroesophageal reflux disease)</li>
                                <li>Heller’s Myotomy (for achalasia cardia) splenectomy</li>
                                <li>Colonic resections</li>
                                <li>Hernia repairs</li>
                                <li>Bowels resection and anastomosis</li>
                                <li>Abdominal tumors including adrenal cancer surgeries </li>
                                <li>Additionally VATS (Video Assisted Thoracoscopic surgery), </li>
                                <li>Key-hole thoracic surgeries for conditions like recurrent pneumothorax, recurrent pleural effusion, loculated dense pleural effusions, stapled lung biopsies, hyperhidrosis (wet palms), tumors of thymus gland</li>
                                <li>Our Pediatric surgery department performs minimally invasive surgery for various conditions like appendicitis, hernia, undescended testis, gastric volvulus, splenectomy, cholecystectomy, malrotation, pyeloplasty, VATS etc.</li>
                                <li>Our department of Urology is actively practicing this speciality for radical prostatectomy, radical nephrectomy, radical cystectomy, pyeloplasty, etc.</li>
                                <li>Donor nephrectomy for kidney transplantation is performed laparoscopically.</li>
                                <li>Various surgeries like hysterectomy, myomectomy, ovarian cystectomy, surgery for endometriosis, tubal pregnancies are performed by our department of Obstetrics and Gynecology.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <p>All our operation rooms (ORs) are well equipped with all instrumentations for Minimally Invasive Surgerylike HD Camera and monitors, insufllators, state of the art laparoscopic instrument set, morcellators, various energy devices ( electrocautery, Ligasure®, Hormonic scalpel®, CUSA®) . Nurses specially trained for assisting minimally invasive surgeryman our operation rooms.Facility of frozen section histopathology enables us for intra-operative decision making and making a precise choice of the minimally invasive surgery for our patients.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

