﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="visa_investigation.aspx.cs" Inherits="visa_investigation" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Visa Investigation</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Visa.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>We offer the Visa investigation service for Australia, Brunei, Canada, New Zealand.,UK</p>

                            <p><strong class="green_text uppercase">Availability</strong></p>

                            <p>The department is open on all days except Sunday and public holidays, between 8am to 4pm. </p>

                            <p><strong class="green_text uppercase">Contact</strong></p>

                            <p>
                                Board Line : 022-26568000 Extension: 8248/8244<br>
                                Direct line : 022-226568248/26568244<br>
                                Email id :<a href="mailto:visa@lilavatihospital.com">visa@lilavatihospital.com</a>
                            </p>


                            <p><strong class="green_text uppercase">Note</strong></p>
                            <ul class="bullet">
                                <li>Instructions for medical check-up will be provided at the time of booking an appointment.</li>
                                <li>We also conduct e-medical for Australia, Canada & New Zealand visa.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Australia</h2>

                        <div class="tab_content">

                            <p><strong class="green_text uppercase">Instructions for Australian Medical Examination (501, 502, 707)</strong></p>
                            <ul class="bullet">
                                <li>Reporting timing is 9.00 am OR as per appointment given.</li>
                                <li>No fasting required:</li>
                                <li>Prior Appointment is preferable</li>
                                <li>Carry Original passport and a Xerox copy of the first and the last page.</li>
                                <li>Carry HAP Id /TRN or HRI no, received from Embassy.</li>
                                <li>Children under 11 years should be accompanied by relative/ parents for the medicals.</li>
                                <li>Urine sample has to be given.</li>
                                <li>Females in their menstrual cycles should not come due to abnormalities being seen in the urine reports.</li>
                                <li>Applicant under 5 years (only consultation)(501)   Rs. 1400/- </li>
                                <li>Charges from (1/7/2014)</li>
                                <li>Applicant between 5 to < 11 years (Consultation and urine test) (501)   Rs. 1600/- </li>
                                <li>Charges from (1/7/2014)</li>
                                <li>Applicant 11 to 15 years (consultation, urine and chest X-ray) (501 & 502)   Rs. 2300/- </li>
                                <li>Charges from (1/7/2014)</li>
                                <li>Applicant >15 years (consultation, urine test, chest x-ray and blood test) (501, 502 & 707)   Rs. 3800/-</li>
                                <li>Charges from (1/7/2014)</li>
                            </ul>


                            <p><strong class="green_text uppercase">Instructions for Australian Chest x-ray Only (502)</strong></p>

                            <ul class="bullet">
                                <li>Reporting timing is 9.00 am OR as per appointment given.</li>
                                <li>No fasting required:</li>
                                <li>Appointment to be taken by the applicant</li>
                                <li>Carry Original passport and a Xerox copy of the first and the last page.</li>
                                <li>Carry HAP Id /TRN or HRI no received from Embassy.</li>
                                <li>Charges Rs. 700/- </li>
                                <li>Charges from (1/7/2014)</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Brunei</h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">Instructions for Brunei Visa Medical</strong></p>
                            <ul class="bullet">
                                <li>Registration is compulsory and charges applicable are Rs. 50/-</li>
                                <li>Reporting timing is 9.00 am OR as per appointment given.</li>
                                <li>No fasting required</li>
                                <li>Appointment to be taken by the applicant</li>
                                <li>Carry Original passport and a Xerox copy of the first and the last page.</li>
                                <li>Six passport size photos.</li>
                                <li>Charges Rs. 5130/- per head.</li>
                                <li>Job which requires food handling will require additional investigation of Stool Routine and Stool Culture.</li>
                                <li>Married females’ additional Urine Pregnancy test.</li>
                                <li>Reports will be available next day between 3.00pm to 4.00pm.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Canada</h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">Instructions for Canadian Visa</strong></p>
                            <ul class="bullet">
                                <li>Reporting timing is 9.00 am OR as per appointment given.</li>
                                <li>No fasting required</li>
                                <li>Appointment to be taken by the applicant</li>
                                <li>Need to carry Original passport and a Xerox copy of the first and the last page.</li>
                                <li>If Original passport is not available, need to carry a copy of passport with your original pan card /Aadhar card and a copy.</li>
                                <li>Charges Rs. 4100/- per head.</li>
                                <li>For PR / Visitor / Family carry your IME No if received from Embassy.</li>
                                <li>STUDENT VISA - Letter of Acceptance from university, original & photocopy </li>
                                <li>WORK VISA - Offer or appointment letter, original & photocopy </li>
                                <li>Urine sample has to be given.</li>
                                <li>Females in their menstrual cycles should not come due to abnormalities being seen in the urine reports.</li>
                            </ul>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>New Zealand </h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">Instructions for New Zealand Medical Examination</strong></p>
                            <ul class="bullet">
                                <li>Reporting timing is 9.00 am OR as per appointment given.</li>
                                <li>No fasting required</li>
                                <li>Prior Appointment is preferable</li>
                                <li>Carry Original passport and a Xerox copy of the first and the last page.</li>
                                <li>Download and fill required medical forms pertaining to your medical exam.</li>
                                <li>Children under 18 years should be accompanied by relative/ parents for the medicals.</li>
                                <li>Urine sample has to be given.</li>
                                <li>Females in their menstrual cycles should not come due to abnormalities being seen in the urine reports.</li>
                                <li>Digital Photograph will be taken in the Hospital</li>
                            </ul>

                            <table class="visa_certf">
                                <thead>
                                    <tr>
                                        <td>Certificate</td>
                                        <td>Age</td>
                                        <td>Exam</td>
                                        <td>Charges</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-title="Certificate">General medical certificate</td>
                                        <td data-title="Age">14 years of age or younger</td>
                                        <td data-title="Exam">501 (Medical)</td>
                                        <td data-title="Charges">Rs. 3000</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Certificate">General medical certificate</td>
                                        <td data-title="Age">If the client is 15 years of age or older and younger than 70</td>
                                        <td data-title="Exam">501 (Medical) 716 (Hepatitis C) 717 (HbA1C) 707(HIV) 704 (Creatinine) 712 (Syphilis test) 708 (Hepatitis B) 718 (Full blood count & Xray)</td>
                                        <td data-title="Charges">Rs. 6630</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Certificate">Limited medial certificate</td>
                                        <td data-title="Age">If the client is 14 years of age or younger</td>
                                        <td data-title="Exam">512 (Limited medical)</td>
                                        <td data-title="Charges">Rs. 3000</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Certificate">Limited medial certificate</td>
                                        <td data-title="Age">If the client is 15 years of age or older</td>
                                        <td data-title="Exam">512(Limited medical) 704(Serum creatinine) 718(Full blood count)</td>
                                        <td data-title="Charges">Rs. 3300</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Certificate">Supplementary medical certificate</td>
                                        <td data-title="Age">Client must be 15 years of age or older.</td>
                                        <td data-title="Exam">707 (HIV)</td>
                                        <td data-title="Charges">Rs. 650</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Certificate">X-ray certificate</td>
                                        <td data-title="Age">Client must be 11 years of age</td>
                                        <td data-title="Exam">502 (X-ray)</td>
                                        <td data-title="Charges">Rs. 700</td>
                                    </tr>
                                </tbody>
                            </table>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>united kingdom</h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">Instructions for UK VISA (TB Screening)</strong></p>
                            <ul class="bullet">
                                <li>Reporting timing is 9.00 am OR as per appointment given.</li>
                                <li>No fasting required</li>
                                <li>Prior Appointment is preferable</li>
                                <li>Carry Original passport and a Xerox copy of the first and the last page.</li>
                                <li>Charges Rs. 1500/- per head.</li>
                                <li>Children under 11 years should be accompanied by relative/ parents for the medicals.</li>
                                <li>Carry One recent passport size photo.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

