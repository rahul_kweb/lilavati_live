﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="MainMaster.master" CodeFile="SampleCollectionFaq.aspx.cs" Inherits="SampleCollectionFaq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">FAQs for Home Sample Collection</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="print_share">
                <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                <div class="addthis_native_toolbox"></div>
                <div class="clear"></div>
            </div>

            <ul class="bullet">
                <li><strong>How and when can one contact to book the appointment?</strong>

                    <p>Contact on 8879677196 / 93 between 9am to 9pm (weekdays) and 9am to 3pm (Sundays and Public holidays). Our executive shall check for the available slot and confirm accordingly</p>
                </li>

                <li><strong>Is the sample collection done on Sundays?</strong>

                    <p>Yes. The sample collection is done 365 days (including Sundays and Public Holidays)</p>
                </li>

                <li><strong>What is the coverage area for sample collection?</strong>
                    <ul>
                        <li>Western Line - Churchgate to Bhayander</li>
                        <li>Central Line - CST to Kalyan</li>
                        <li>Harbour Line - CST to Panvel</li>
                    </ul>
                </li>

                <li><strong>How shall the beneficiary come to know about the instructions for the tests?</strong>

                    <p>Our executive shall inform the required instructions to be followed at the time of booking the appointment.</p>
                </li>

                <li><strong>Are there any additional charges for sample collection?</strong>

                    <p>Yes. The sample collection charges per person per visit is Rs.250/-.</p>
                </li>


                <li><strong>What all details are to be provided at the time of making appointment?</strong><br />
                    <text>✔ Registration number (if already registered with the hospital ) if not then</text><br />
                    <ul>
                        <li>Full name</li>
                        <li>Date of birth</li>
                        <li>Mobile number</li>
                        <li>Detailed Residential address</li>
                        <li>Email id</li>
                    </ul>
                    <text>✔ Lists of tests to be conducted / share doctor’s prescription</text><br />
                    <text>✔ Referring doctor’s name</text><br />
                     <text>✔ Mode of payment</text><br />
                     <text>✔ Delivery of reports</text><br />
                     <text>✔ Any special condition of the patient e.g. Bed ridden, on catheter etc.</text><br />
              </li>
           <li><strong>How can the beneficiary make payment?</strong>

                    <p>Payment can be made by cash / online via payment link for the tests, sample collection charges, LH new registration on the day of the sample collection</p>
                </li>
                      <li><strong>Are there any cancellation charges?</strong>

                    <p>No. However cancellation is allowed only 24hrs prior</p>
                </li>

                      <li><strong>Is a doctor’s prescription required to avail the home sample collection service?</strong>

                    <p>It is preferred but not mandatory.</p>
                </li>
                     <li><strong>Is the deputed phlebotomist qualified to do sample collection?</strong>

                    <p>The phlebotomists are DMLT qualified and are adequately experienced in sample collection</p>
                </li>
                      <li><strong>Is the sample collection done for children?</strong>

                    <p>Yes we do sample collection for above 5yrs old children only.</p>
                </li>
                       <li><strong>How soon will the beneficiary get the reports?</strong>

                    <p>The reports shall be emailed on the same day only for the samples collected before 10am.For the samples collected beyond 10am then the results shall be provided the next day. However if the test is run on a specific day or requires more time for interpretation then the same shall be informed to the beneficiary</p>
                </li>
                       <li><strong>How will the reports be delivered to the beneficiary?</strong>

                        <ul>
                        <li>Email</li>
                        <li>Courier (at additional charge of Rs.50/-)</li>
                        <li>Personally collect from LHRC ground floor Central Dispatch Section (within 2 months)</li>                       
                    </ul>
                            </li>
                    <li><strong>When can I come to collect the reports from Central dispatch section?</strong>

                    <p>From Monday to Saturday from 8am to 9pm.Please carry TRF cum receipt copy with yourself</p>
                </li>

            </ul>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>


