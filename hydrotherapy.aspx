﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="hydrotherapy.aspx.cs" Inherits="hydrotherapy" %>

<%@ Register TagName="Service" TagPrefix="menu" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/ddaccordion.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Hydrotherapy Centre (First of its Kind) Belhydro Puro2</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="inner_banner">
                    <img src="/uploads/home_banner/DefaultInnerImage.jpg">
                </div>
                <%--<asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>--%>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                   <%-- <asp:Repeater ID="rptContent" runat="server" OnItemDataBound="rptContent_ItemDataBound">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnDesc" runat="server" Value='<%#Eval("DESCRIPTION") %>' />
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>--%>

                     <div class="tabbertab">
                        <h2>Benefits</h2>

                        <div class="tab_content">

                            <%--<p>The department of Critical Care consists of 63 beds along with a high dependency unit. The team part of this department comprises of Chief Intensivists, Junior Intensivists, Senior & Junior Doctors and also a highly efficient, experienced and well trained nursing & paramedical staff. </p>--%>

                            <%--<p class="hd green_text uppercase"><strong></strong></p>--%>
                            <ul class="bullet">
                                <li>Deep cleansing the skin</li>
                                <li>Younger healthier skin</li>
                                <li>Detoxification ,neutralising free radicals</li>
                                <li>Slightly bubbling sensation</li>
                                <li>Soothes sensitive skin and skin disorders</li>

                            </ul>
                            <p><strong>For Details contact : 9167025428</strong></p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <%--<div class="tabbertab">
                        <h2>SICU and ICCU</h2>

                        <div class="tab_content">

                            <p class="hd green_text uppercase"><strong>Surgical Intensive Care Unit (SICU)</strong></p>



                            <p>
                                Recently renovated 12 beds SICU on first floor serves various cardiac and other surgical patients 
The team is headed by Chief Intensivist and provides availability of Qualified and experienced critical care specialist, dedicated and qualified nurses, efficient nursing aid and attendant staff round the clock having sympathetic approach to patient care. Efficient and readily available paramedic staff i.e. X-ray technicians, Phlebotomist, etc. Efficient team serving various organ transplant patients like Kidney transplant recipient, liver transplant recipient, etc.The design of SICU incorporates various infection control practices minimizing risk of hospital acquired infections.
                            </p>
                            <p>SICU provides backbone to conduct highest of high risk surgeries in city of Mumbai</p>

                            <p class="green_text"><strong>Technology & Infrastructure </strong></p>

                            <ul class="bullet">
                                <li>Portable digital X-ray with on spot visual display improves and expedites decision making about patient care.</li>
                                <li>C MAC Flexible intubating Laryngoscope for difficult endotracheal Intubation making procedure safe.</li>
                                <li>Advanced ABG machine with point of care testing for Electrolytes, Glucose and Lactate.</li>
                                <li>The Central pendent in each cubicle provides better interface between patient and ventilator /Monitor.</li>
                                <li>Separate counselling room for relatives thus improves communication and confidence.</li>
                                <li>Isolated and clean DU (Dirty Utility) area to maintain hygiene.</li>
                                <li>Positive & Negative Pressure Isolation rooms with capability to manage Liver transplant.</li>
                                <li>Bright lightning system has been installed creating better atmosphere for doctors and patients.</li>
                                <li>Most beds have Dialysis facility.</li>
                                <li>Remote driven partitions help to keep walls clear</li>
                                <li>Separate doctors room has been designed for isolation beds</li>
                            </ul>

                            <p class="green_text"><strong>Services</strong></p>

                            <ul class="bullet">
                                <li>Vigilant Monitoring by Qualified doctors & nurses</li>
                                <li>Invasive &non-invasive ventilation </li>
                                <li>Basic & Advanced Hemodynamic monitoring like arterial pressure, central Venous pressure, PAP, cardiac output, etc. is done routinely </li>
                                <li>Routine use of IABP pacemaker etc.</li>
                                <li>Advanced therapies like Extracorporeal Cardiac Support (i.e. cardiac ECMO) </li>
                            </ul>

                            <p class="green_text"><strong>Professionals</strong></p>

                            <p class="hd green_text uppercase"><strong>Intensive Critical Care Unit (ICCU)</strong></p>



                            <p>Recently renovated 19 bedded ICCU located on first floor serves various cardiac and non-cardiac emergencies. The team is headed by Chief Intensivist and provides availability of qualified and experienced critical care specialist,dedicated and qualified nurses, efficient nursing aid and attendant staff round the clock having sympathetic approach to patient care. Efficient and readily available paramedic staff i.e. X-ray technicians, Phlebotomists, etc. The design of ICCU incorporates various infection control practices minimizing risk of hospital acquired infections.</p>
                            <p>Average ICCU Occupancyis more than 90%.</p>
                            <p>Round the clock availability of cardiology & other super speciality consultants on call </p>

                            <p class="green_text"><strong>Technology & Infrastructure </strong></p>

                            <ul class="bullet">
                                <li>2 Isolation Beds.</li>
                                <li>1 Isolation Bed with C - Arm Compatibility.</li>
                                <li>Spacious individual cubicle with TV facility.</li>
                                <li>Advanced Ventilators and Multipara Monitors.</li>
                                <li>Wide Cross ventilation arrangements made for each cubicle.</li>
                                <li>Care taken to ensure proper natural light for each patient.</li>
                                <li>Portable digital X-ray with on spot visual display improves and expedites decision making about patient care.</li>
                                <li>C MAC Flexible intubating Laryngoscope for difficult endotracheal Intubation making procedure safe.</li>
                                <li>Advanced ABG machine with point of care testing for Electrolytes, Glucose and Lactate.</li>
                                <li>The Central pendent in each cubicle provides better interface between patient and ventilator /Monitor.</li>
                                <li>Separate counselling room for relatives thus improves communication and confidence.</li>
                                <li>Isolated and clean DU (Dirty Utility) area to maintain hygiene.</li>
                                <li>Positive & Negative Pressure Isolation rooms.</li>
                                <li>Each Cubicle with Dialysis facility</li>
                            </ul>

                            <p class="green_text"><strong>Services</strong></p>
                            <p>Management of below : </p>

                            <ul class="bullet">
                                <li>Cardiac Emergencies like Acute Myocardial Infarction, Cardiogenic Shock as per standard Interventional guidelines & protocol </li>
                                <li>Invasive & Non Invasive Mechanical Ventilation. </li>
                                <li>Basic as well as advanced hemodynamic monitors like PAC cardiac Output Monitoring etc.</li>
                                <li>Advanced procedures like IABP. PAC Insertion, Pacemaker insertion are done routinely bedside in ICU under C- Arm/X ray Guidance </li>
                                <li>Routine use of advanced modalities like CRRT (Continuous Renal Replacement Therapy) , intermittent hemodialysis and Noval therapies like hemoabsorptions & extracorporeal Cytokine removal </li>
                                <li>Advanced Respiratory Care support like Prone Ventilation and ECMO.</li>
                            </ul>

                            <p class="green_text"><strong>Professionals</strong></p>

                            <div class="clear"></div>
                        </div>
                    </div>--%>



                    <div class="clear"></div>
                </div>


                <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Service ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

