﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="sleep_medicine.aspx.cs" Inherits="sleep_medicine" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Sleep Medicine</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Sleep_Medicine.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                         <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                     <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
                <%--    <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Department of sleep medicine offers solutions to sleep related disturbances and disorders. Sleep is a domain of one’s life which is now gaining awareness world -wide. Our team of doctors have been enforcing the fact that sleep hygiene is extremely important these days and disorders related to sleep have implications on varied aspects of health including respiratory, cardiology, metabolic and neurology etc. This specialized science takes care of plethora of disorders which are known to be associated with sleep now. Patients with daytime symptoms like excessive sleepiness, concentration difficulties, extreme urge to doze off and night time symptoms like snoring, fragmented sleep and choking spells on a background of difficult to control obesity should raise an alarm for detailed evaluation. Obstructive sleep apnea is a disorder which is under diagnosed, wherein the upper airway collapses as the patient lies flat on bed primarily due to undue laxity of pharyngeal muscle. Thus, a continuous positive airway pressure (CPAP) ventilation helps by acting as a splint to keep the airway open and thereby ensures oxygenation. In a month, more than 20 sleep studies are being done in our setup. The department runs under guidance of consultants namely  Dr. Pralhad Prabhudesai, Dr. Jalil D Parkar, Dr. Suresh Rang, Dr. Sanjeev Mehta and Dr. Prashant Chajjed.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <p>Sleep health is a relatively newer concept to our patients around. On outpatient basis, we make them aware of advancement of sleep science and its possible effects on our body. Patients with high index of clinical suspicion are subjected to sleep study and once diagnosed with obstructive sleep apnea are advised for second night study of CPAP titration. We assist them with various ways to reduce excess weight. A basic understanding of favorable effects of using the machine regularly are discussed in details to ensure the compliance.</p>
                            <div class="clear"></div>
                        </div>
                    </div>


                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology And Infrastructure</h2>

                        <div class="tab_content">
                            <p>We have a dedicated sleep laboratory for complete overnight polysomnography (sleep study). A single room with all the gadgets and absolutely comfortable environment is offered to our patients to sleep. Patient is physically monitored by a well trained sleep technician throughout the night. The next day a detailed report is produced which is analyzed by our sleep experts. We have facilities of two night sleep study where on first night diagnostic analysis of sleep disorder is done followed by CPAP  titration on the second night for patients with obstructive sleep apnea. In a small subset an option of split night study is also available but the decision is solely taken by our senior consultants. We also have facility for multiple sleep latency test (MSLT) study for diagnosing difficult cases of Narcolepsy. Our laboratory also supports video monitoring even in daytime which is extremely important for patients with Narcolepsy and in Pediatric sleep disorders like epilepsy, attention deficit hyperactive disorders etc. </p>

                            <div class="clear"></div>
                        </div>
                    </div>


                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                   <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

