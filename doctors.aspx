﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="doctors.aspx.cs" Inherits="doctors" %>

<%@ Register TagPrefix="menu" TagName="professionals" Src="Control/professionals.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">DNB</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>DNB</h2>
                        <div class="tab_content">

                            <p><a href="docs/Congratulations_Radiology_Doctors.doc" target="_blank">DNB Trainees Awarded at EDURAD Grand Teaching Files Case Presentation</a></p>

                            <p>Lilavati Hospital is committed towards developing competent human resources in the medical arena.</p>
                            <p>The hospital is a teaching institute and accredited by National Board of Examination for imparting training on different Broad and Superspeciality.</p>
                            <p>The hospital is accredited to conduct <strong>DNB/ FNB</strong> course for 14 Specialties -</p>


                            <table class="dnb">
                                <thead>
                                    <tr>
                                        <td>Sr.</td>
                                        <td>Name of the Course</td>
                                        <td>Eligibility</td>
                                        <td>Duration</td>
                                        <td>Course Commences</td>
                                        <td>No. of Seats</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-title="Sr.">1.</td>
                                        <td data-title="Name of the Course">General Medicine</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">July</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">2.</td>
                                        <td data-title="Name of the Course">Pediatrics – Primary</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">July</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">3.</td>
                                        <td data-title="Name of the Course">Pediatrics – Secondary</td>
                                        <td data-title="Eligibility">DCH + PDCET</td>
                                        <td data-title="Duration">2 Years</td>
                                        <td data-title="Course Commences">July</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">4</td>
                                        <td data-title="Name of the Course">Pediatric Surgery</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">6 Years</td>
                                        <td data-title="Course Commences">July</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">5</td>
                                        <td data-title="Name of the Course">Radiology – Primary</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">July</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">6</td>
                                        <td data-title="Name of the Course">Radiology – Secondary</td>
                                        <td data-title="Eligibility">DMRD + PDCET</td>
                                        <td data-title="Duration">2 Years</td>
                                        <td data-title="Course Commences">July</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">7</td>
                                        <td data-title="Name of the Course">Chest Medicine - Primary</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">8</td>
                                        <td data-title="Name of the Course">Chest Medicine – Secondary</td>
                                        <td data-title="Eligibility">DTCD + PDCET</td>
                                        <td data-title="Duration">2 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">9</td>
                                        <td data-title="Name of the Course">General Surgery</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">10</td>
                                        <td data-title="Name of the Course">Orthopedics</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">11</td>
                                        <td data-title="Name of the Course">Obs & Gynaecology – Primary</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">12</td>
                                        <td data-title="Name of the Course">Obs & Gynaecology – Secondary</td>
                                        <td data-title="Eligibility">DGO + PDCET</td>
                                        <td data-title="Duration">2 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">13</td>
                                        <td data-title="Name of the Course">Urology</td>
                                        <td data-title="Eligibility">MS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">14</td>
                                        <td data-title="Name of the Course">Neuro Surgery</td>
                                        <td data-title="Eligibility">MBBS + DNB CET</td>
                                        <td data-title="Duration">6 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">15</td>
                                        <td data-title="Name of the Course">Nephrology</td>
                                        <td data-title="Eligibility">MD + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">16</td>
                                        <td data-title="Name of the Course">G.I. Surgery</td>
                                        <td data-title="Eligibility">MS + DNB CET</td>
                                        <td data-title="Duration">3 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">01</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">17</td>
                                        <td data-title="Name of the Course">Critical Care Medicine</td>
                                        <td data-title="Eligibility">MD + FET</td>
                                        <td data-title="Duration">2 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                    <tr>
                                        <td data-title="Sr.">18</td>
                                        <td data-title="Name of the Course">Reproductive Medicine</td>
                                        <td data-title="Eligibility">MS + FET</td>
                                        <td data-title="Duration">2 Years</td>
                                        <td data-title="Course Commences">January</td>
                                        <td data-title="No. of Seats">02</td>
                                    </tr>

                                </tbody>
                            </table>

                            <p>
                                For further details contact –<br>
                                Name: <strong>Ms. Apoorva Prabhu</strong><br>
                                Phone Number : 022- 2656 8336<br>
                                Email Address : <a href="mailto:careers@lilavatihospital.com">careers@lilavatihospital.com</a>
                            </p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>CME</h2>
                        <div class="tab_content">

                            <p>Lilavati Hospital is accredited by Maharashtra Medical Council (MMC) for conducting Continues Medical Education. The events are conducted by expert consultants of their specialties to share the knowledge and to have an interactive session with the participants. The key focus of the programme is knowledge sharing which enables the participants to get the recent and latest update on the modern health care developments through such academic programmes. CME’s at Lilavati Hospital are conducted on Wednesdays and invitations are sent through SMS’ to the IMA registered members by the Association and from the hospital. The allocated credit hours are presented to the attendees along with a certificate.</p>
                            <p>
                                Interested members can send email on <a href="mailto:cme@lilavatihospital.com">cme@lilavatihospital.com</a> and register.</p>

                                    <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:professionals id="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</asp:Content>

