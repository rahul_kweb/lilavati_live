﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="nephrology.aspx.cs" Inherits="nephrology" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Nephrology</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Nephrology.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <%--   <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>The department of Nephrology began operations with artificial kidney Dialysis department (AKD). This was one of the first departments of the hospital. It was followed by clinical nephrology, facilities for chronic peritoneal dialysis and kidney transplantation. Since its beginning, AKD has remained as one of the largest in hospital hemodialysis facility in the city. In fact, Lilavati hospital was the first in the city of Mumbai to start facility for hemodialysis in the ICU setting for acutely ill patients. We have special attention towards dialysis access, looked after by our nephrologists. We also have training program for DNB in nephrology. The department has a team of highly qualified and experienced nephrologists to look after adult patients and qualified Pediatric Nephrologists.</p>
                            <p>We perform about 1500 hemodialysis per month, and 10-15 sessions of continuous renal replacement therapy per month. Vascular access procedures are done at bed side in ICU and AKD for temporary access and in OT or Cath lab for long term vascular access. About 150 procedures related to vascular access and other nephrology procedures are performed every month. There are 50 nephrology patients as indoor admissions and 30-40 patients are seen in OPD daily.</p>
                            <p>Team of nephrology comprises of 6 eminent consultants</p>
                            <p>There are 3 registrars, 2 house physicians available round the clock.</p>
                            <p>They are backed by AKD staff of 33 persons to provide round the clock dialysis facilities.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <p>Round the clock Hemodialysis and Nephrology services are offered.</p>

                            <ul class="bullet">
                                <li><strong>Hemodialysis:</strong> There are around 25 machines for hemodialysis, 16 stations in AKD and remaining for ICU and ICCU</li>
                                <li><strong>Peritoneal dialysis:</strong> Counseling, patient acceptance, options for manual or automated peritoneal dialysis and training for the same are provide with help of a special team.</li>
                                <li><strong>Kidney biopsy:</strong> Performed by nephrologists and reported by histopathologist, in close conjunction with nephrologists so as to achieve best diagnosis through best clinic pathological corelation.</li>
                                <li><strong>Kidney transplantation:</strong> Special team for transplantation includes trained nephrologists, specially trained surgeons and anesthetists. Trained nurses to look after the kidney transplant recipients in post operative period, in ICU setting.</li>
                                <li><strong>Vascular access:</strong> Patients opting for long term hemodialysis are evaluated for their vascular access, and if having problem, evaluated by a special team of vascular access specialist. This includes a nephrologists, vascular surgeon and interventional radiologist.</li>
                                <li><strong>Renal histopathology:</strong> Routine processing includes light microscopy, immunofluorescence and electron microscopy. Special immunohistochemistry staining is also performed. </li>
                                <li><strong>Interventional nephrology:</strong> It is a new and upcoming sub-specialty of nephrology and works towards access for dialysis, AV fistula surveillance, interventions to salvage AV fistula, tackling central venous stenosis, creating vascular accesses in patients with multiple levels of venous stenosis, diagnostic ultrasonography etc.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">
                            <p>22 state of the art hemodialysis machines, 2 CRRT machines, high capacity reverse osmosis water plant with facility to provide bed side dialysis to all ICU patients, and 16 beds in AKD (artificial kidney department) to provide 24 x 7 dialysis facility.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

