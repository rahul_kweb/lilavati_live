﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="visitors_policy.aspx.cs" Inherits="visitors_policy" %>

<%@ Register TagName="Visitors" TagPrefix="menu" Src="Control/visitors.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Visitors Policy</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg" />
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--   <table class="passes">
                    <thead>
                        <tr>
                            <td>Room</td>
                            <td>Surgery Pass</td>
                            <td>Attendant Pass</td>
                            <td>Visitor Pass</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Executive Suite</td>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>Super Deluxe</td>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>Deluxe</td>
                            <td>1</td>
                            <td>1</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>Single</td>
                            <td>1</td>
                            <td>1</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>Twin Sharing</td>
                            <td>1</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>Economy</td>
                            <td>1</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>Common</td>
                            <td>1</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>Paediatric</td>
                            <td>1</td>
                            <td>2</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>PICU/NICU</td>
                            <td></td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>ICU/SICU/ICCU</td>
                            <td></td>
                            <td>1</td>
                            <td>1</td>
                        </tr>

                    </tbody>
                </table>

                <ul class="bullet">
                    <li>An additional pass (1) is provided for ICU, for visit between 8:00am to 8:00 pm. </li>
                    <li>No pass for children below 10 years except between 6.30 to 7.00 pm only for ½ hrs.</li>
                    <li>A surgery pass is issued half hour before the scheduled time of surgery and is valid till the end of surgery.</li>
                    <li>Pass issued for CABG surgery is valid until 8 AM next day.</li>
                    <li>Only one attendant is permitted to stay with the patients round the clock (except in Executive, Super Deluxe and Paediatric Rooms). Attendant pass is valid up to the date of discharge.</li>
                    <li>Attendant of the patient admitted in ICUs will remain in the hospital in the attended waiting area on the some floor. He or She is permitted to spend the night in the prescribed area where bunker beds are provided.</li>
                    <li>Passes can be renewed at the Reception / Admission counter.</li>
                    <li>Patient/Relatives who require the authentication (seal) on hospital documents or Mediclaim papers shall be issued a pass for MRD between 9 am to 4.30 pm on all days except Sunday & Public Holidays.</li>
                </ul>--%>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Visitors ID="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

