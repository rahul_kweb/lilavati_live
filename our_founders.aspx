﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="our_founders.aspx.cs" Inherits="our_founders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">
        <div class="green_banner">
            <div class="page_title">The Founder</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">
                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>
                <%--<p>Our Founder Late Shri Kirtilal Mehta started Lilavati Hospital & Research Centre with the vision to provide unmatched healthcare services to the common Man. This vision was also shared by his wife Late Smt. Lilavati Kirtilal Mehta and hence the hospital was named after her.</p>

                <h1 class="hd green_text uppercase">Lilavati Kirtilal Mehta Medical Trust</h1>

                <p>Lilavati Kirtilal Mehta Medical Trust is a Public Charitable Trust formed in 1978 by Late Shri Kirtilal Mehta. The trust not only manages Lilavati Hospital & Research Centre but also carries out numerous other charitable initiatives all over India.</p>

                <div class="f_box">
                    <div class="f_img">
                        <img src="images/f_kirtilal.jpg">
                        <span class="green_text uppercase">Shri Kirtilal Manilal Mehta</span>
                    </div>
                    <div class="f_profile">
                        Born in the small town of Palanpur on February 07, 1907, Shri Mehta joined his family business in Rangoon, Burma. Having lost his father at a very young age, circumstances led him to be independent and responsible. As a young man with great ambition, Mr. Mehta setup an independent business in Mumbai under his own name. He had a keen eye on business opportunities and was able to spot the potential in exporting diamonds to Europe. He pioneered the entry of the Indian community in Antwerp, Belgium. His exploits in the world of business earned him a lot of recognition and awards which include the prestigious “Office of the Order” honour by His Majesty The King of Belgium in 1991, being the first ever Indian to receive it. Other notable awards include,<br>
                        &bull; Leading Exporter Award by Government of India<br>
                        &bull; Outstanding Exporter Award from the President of Israel<br>
                        &bull; Honour by King of Palanpur
                    </div>
                    <div class="clear"></div>
                </div>


                <div class="f_box">
                    <div class="f_img">
                        <img src="images/f_lilavati.jpg">
                        <span class="green_text uppercase">Shri Lilavati Kirtilal Mehta</span>
                    </div>
                    <div class="f_profile">
                        A woman of compassion, Smt. Mehta was a key driving force in setting up Lilavati Hospital & Research Centre. People looked up to her with great respect and bestowed a lot of love upon her, as she always strived for the betterment of the underprivileged. Smt. Mehta strongly believed in equality and that every child has a right to education and equal opportunity in life. Her sudden demise in 1964 left a huge void in the lives of many people and especially at Lilavati Hospital & Research Centre who shall always remain indebted for the inspiration of our beloved Bai!
                    </div>
                    <div class="clear"></div>
                </div>--%>
                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

