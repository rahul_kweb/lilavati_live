﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mission_motto : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
        }
    }

    public void Content()
    {
        cms.ID = 5;
        cmsbal.GETBYIDContent(cms);
        litContent.Text = cms.Description;        
    }
}