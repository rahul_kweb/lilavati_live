﻿using System;

public class AdminPage : System.Web.UI.Page
{
    public string AdminId;
    public string AdminName;

    public AdminPage()
    {

    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (Context.Session != null)
        {
            if (Session[AppKey.SESSION_ADMIN_USERNAME_KEY] != null && !Session[AppKey.SESSION_ADMIN_USERNAME_KEY].ToString().Equals(""))
            {
                AdminId = Session[AppKey.SESSION_ADMIN_USERID_KEY].ToString();
                AdminName = Session[AppKey.SESSION_ADMIN_USERNAME_KEY].ToString();
            }
            else
            {
                Response.Redirect("~/admin/Login.aspx");
            }
        }
    }

}