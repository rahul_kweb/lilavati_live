﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;


/// <summary>
/// Summary description for Dytelwebservice
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Dytelwebservice : System.Web.Services.WebService {
    private string HtmlResult, HtmlResult2;

    public Dytelwebservice () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string SendMail(string strto, string strfrom, string strsubject,string strmessage,string username, string password) {
        try
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(strfrom);
            message.To.Add(strto);
            message.Subject = strsubject;
            message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
            message.Body = strmessage;
            SmtpClient client = new SmtpClient("mail3.dytelworld.com");
            client.Port = 25;
            client.UseDefaultCredentials = false;
            client.EnableSsl = false;
            client.Credentials = new NetworkCredential(username, password);
            client.Send(message);
            return "mail_send";

        }
        catch (Exception ex)
        {
            return "Error occure"+ex.ToString();
          

        }
        
    }

    [WebMethod]
    public string settlepayuPayment(string merchantTransactionId, string totalAmount, string merchantId, string splitAmount, string aggregatorCharges, string splitDetails)
    {
        try
        {
            
            string aggregatorSubTransactionId = merchantTransactionId+"123";
            string amountToBeSettled = totalAmount;
            splitDetails = "payment for " + splitDetails;
            string response = "";
            string childId = "";

            string URI = "https://www.payumoney.com/payment/payment/addPaymentSplit?";
            string myParameters = "merchantKey=HVEsiy&merchantTransactionId=" + merchantTransactionId + "&totalAmount=" + totalAmount + "&totalDiscount=0&jsonSplits=[{aggregatorSubTransactionId:" + aggregatorSubTransactionId + ",merchantId:" + merchantId + ",splitAmount:" + amountToBeSettled + ",aggregatorCharges:" + aggregatorCharges + ",sellerDiscount:0,aggregatorDiscount:0,amountToBeSettled:" + splitAmount + ",splitDetails:" + splitDetails + "}]";
            
			using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                wc.Headers.Add("Authorization", "8z+2gquimPOAqqC15arCF/1rrbkZun6KeTzEfbenFHo=");
                HtmlResult += wc.UploadString(URI, myParameters);

             
                var jss = new JavaScriptSerializer();
                var dict = jss.Deserialize<Dictionary<dynamic, dynamic>>(HtmlResult);
                //Console.WriteLine(dict["message"]);
                response = "response result =" + dict["message"];
               
                string res = jss.Serialize(dict["result"]);
                

                if (res != "null")
                {
                    
                    var dict2 = jss.Deserialize<Dictionary<dynamic, dynamic>>(res);
                    string res2 = jss.Serialize(dict2["splitIdMap"]);
                    var dict3 = jss.Deserialize<Dictionary<dynamic, dynamic>>(res2);

                    response += "/ parentId =" + dict2["paymentId"];
                    //Response.Write("Split map: " + res2);
                    Array keylist = dict3.Keys.ToArray();
                    foreach (var s in keylist)
                    {
                        if (aggregatorSubTransactionId == s.ToString())
                        {
                            response += "/ childId =" + dict3[s.ToString()];
                            childId = Convert.ToString(dict3[s.ToString()]);
                        }

                    }

                   

                }
                else
                {
                    response += "/ parentId =" + "null";
                    response += "/ childId =" + "null";

                }       



            }

            
            string URI2 = "https://www.payumoney.com/payment/merchant/releasePayment?";
            string myparameter2 = "paymentId=" + childId + "&merchantId=" + merchantId;

            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                wc.Headers.Add("Authorization", "8z+2gquimPOAqqC15arCF/1rrbkZun6KeTzEfbenFHo=");
                HtmlResult2 += wc.UploadString(URI2, myparameter2);
                var jss = new JavaScriptSerializer();
                var dict = jss.Deserialize<Dictionary<dynamic, dynamic>>(HtmlResult2);
            }




           return myParameters + " " + response + "/ release payment status =" + HtmlResult2 + " release parameter =" + myparameter2;
        }
        catch(Exception ex)
        {
            return "error occure in payment"+ex.Message+" source"+ex.Source+" stacktrack "+ex.StackTrace;

        }
        

    }



    [WebMethod]
    public string settlecancelpayuPayment(string paymentId, string refundAmount, string merchantId, string merchantAmount, string aggregatorAmount)
    {
        try
        {
            string response = "";
            string URI = "https://www.payumoney.com/payment/refund/refundPayment?";
            string myParameters = "paymentId="+paymentId+"&refundAmount="+refundAmount+"&refundType=1&merchantId="+merchantId+"&merchantAmount="+merchantAmount+"&aggregatorAmount="+aggregatorAmount;
            using (WebClient wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                wc.Headers.Add("Authorization", "8z+2gquimPOAqqC15arCF/1rrbkZun6KeTzEfbenFHo=");
                HtmlResult += wc.UploadString(URI, myParameters);
                var jss = new JavaScriptSerializer();
                var dict = jss.Deserialize<Dictionary<dynamic, dynamic>>(HtmlResult);
                response = "response result =" + dict["message"];

                string res = Convert.ToString(dict["result"]);
                if (res != "null")
                {
                    response += "/ resultId =" + res;

                }
                else
                {
                    response += "/ " + "null";

                }

            }
            return myParameters + " "+ response;

        }
        catch(Exception ex)
        {
            return "Error occure"+ex.ToString();
        }
       

      
    }
}

