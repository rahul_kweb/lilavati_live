﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for OfflineAppMail
/// </summary>
public class OfflineAppMail
{
    //string strTo = "arvind@kwebmaker.com";
    string strTo = "appointments@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";//we have removed @lilavatihospital.com from the user name
    string strMailPassword = "lilavati12";
    string Subject = "Appointment Request From Lilavati Website";

    //string strTo = "arvind@kwebmaker.com";
    ////string strTo = "appointments@lilavatihospital.com";
    //string strFrom = "noreply@kwebmaker.com";
    //string strMailUserName = "noreply@kwebmaker.com";
    //string strMailPassword = "noreply123";
    //string Subject = "Appointment Request From Lilavati Website";

    string strFrom1 = "marketing@lilavatihospital.com";
    string strMailUserName1 = "marketing";
    string strMailPassword1 = "lilavati12";
    string Subject1 = "Appointment Request Details From Lilavati Website";

	public OfflineAppMail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            //mailClient.Host = "smtpout.secureserver.net";
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;
            
            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailOfflineApp(string Speciality, string DoctorName, string AvailableTimings, string AppointmentDate, string PatientName, string Email, string MobileNumber, string Address, string Country, string State, string City, string Pincode, string Visit)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> Appointment Request From Lilavati Website </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Speciality : </strong></td><td> " + Speciality + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Doctor Name : </strong></td> <td> " + DoctorName + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Available Timings : </strong></td> <td> " + AvailableTimings + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Appointment Date : </strong></td> <td> " + AppointmentDate + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Patient Name : </strong></td> <td> " + PatientName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Email : </strong></td> <td> " + Email + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Mobile Number : </strong></td> <td> " + MobileNumber + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Address : </strong></td> <td> " + Address + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Country : </strong></td> <td> " + Country + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>State : </strong></td> <td> " + State + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + City + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Pincode : </strong></td> <td> " + Pincode + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Visit : </strong></td> <td> " + Visit + " </td></tr>");
                        

            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString());


            /*send auto mail who fill the form*/

            string EmailId = Email;

            SmtpClient mailClient1 = null;
            MailMessage message1 = null;
            StringBuilder mailbody1 = new StringBuilder();
            mailClient1 = new SmtpClient();
            message1 = new MailMessage();
            mailClient1.Host = "webmail.lilavatihospital.com";
            mailClient1.Port = 25;

            System.Net.NetworkCredential SMTPUserInfo1 = new System.Net.NetworkCredential(strMailUserName1, strMailPassword1);
            mailClient1.UseDefaultCredentials = false;
            mailClient1.Credentials = SMTPUserInfo1;
            mailClient1.EnableSsl = false;
            mailClient1.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress1 = new MailAddress(strFrom1, "Lilavati Hospital");
            message1.From = fromAddress1;

            //mailbody1.Append("Thanks for booking an appointment with us.<br />");
            //mailbody1.AppendFormat("We will get back to you very soon.<br />");

            mailbody1.Append("Thank you for your request<br />");
            mailbody1.AppendFormat("Please do not consider this as your confirmed appointment<br />");
            mailbody1.AppendFormat("We shall get back to you very soon<br /> <br />");
            //mailbody1.AppendFormat("Hospital holidays: 15th Sept,11th Oct,1st Nov and all Sundays.<br />");

            mailbody1.AppendFormat("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            mailbody1.AppendFormat("<tr><td colspan='2' align='center'><img src='http://www.lilavatihospital.com/images/emailer_logo.jpg' /><br /><hr color='#04869a' /></td></tr>");
            mailbody1.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> Appointment Details From Lilavati Website </font></strong></td></tr>");
            mailbody1.AppendFormat("<tr><td width=\"200px\"><strong>Speciality : </strong></td><td> " + Speciality + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Doctor Name : </strong></td> <td> " + DoctorName + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Available Timings : </strong></td> <td> " + AvailableTimings + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Appointment Date : </strong></td> <td> " + AppointmentDate + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Patient Name : </strong></td> <td> " + PatientName + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Email : </strong></td> <td> " + Email + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Mobile Number : </strong></td> <td> " + MobileNumber + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Address : </strong></td> <td> " + Address + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Country : </strong></td> <td> " + Country + " </td></tr>");

            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>State : </strong></td> <td> " + State + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + City + " </td></tr>");

            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Pincode : </strong></td> <td> " + Pincode + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Visit : </strong></td> <td> " + Visit + " </td></tr>");


            mailbody1.Append("</table>");           

            message1.To.Add(EmailId);
            message1.Subject = Subject1;

            message1.Body = mailbody1.ToString();
            message1.IsBodyHtml = true;
            mailClient1.Send(message1);
            message1 = null;
            mailClient1 = null;
           


            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}