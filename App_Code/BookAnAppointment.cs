﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for BookAnAppointment
/// </summary>
public class BookAnAppointment
{
    //string strTo = "arvind@kwebmaker.com";
    //string strTo = "operations@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Appointment Details";

    public BookAnAppointment()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body,string strTo,string attachment)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;

            Attachment attachFile = new Attachment(HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/") + attachment);
            message.Attachments.Add(attachFile);


            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailAppointment(string strSpeciality, string strNameOfDoctor, string strAvailableTiming, string strDateOfAppointment, string strPreferredTime, string strTo,string attachment)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> YOUR APPOINTMENT DETAILS </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Speciality : </strong></td><td> " + strSpeciality + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Name Of Doctor: </strong></td> <td> " + strNameOfDoctor + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Available Timing : </strong></td> <td> " + strAvailableTiming + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Date Of Appointment : </strong></td> <td> " + strDateOfAppointment + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Preferred Time : </strong></td> <td> " + strPreferredTime + " </td></tr>");

            strEmailBuilder.Append("</table>");

            string attachmentfilename = attachment;
            SendEmail(strEmailBuilder.ToString(), strTo, attachmentfilename);

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}