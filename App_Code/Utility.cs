﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    public const string EMAIL_HOST = "webmail.lilavatihospital.com";
    public const int EMAIL_PORT = 25;
    public const string EMAIL_USERNAME = "marketing";
    public const string EMAIL_PASSWORD = "lilavati12";
    public const string strFrom = "marketing@lilavatihospital.com";
    public const string EMAIL_SUBJECT = "Account Activation Lilavati";

    /*
    public const string EMAIL_HOST1 = "smtpout.secureserver.net";
    public const int EMAIL_PORT1 = 25;
    public const string EMAIL_USERNAME1 = "arvind@kwebmaker.com";
    public const string EMAIL_PASSWORD1 = "arvind123";
    public const string EMAIL_SUBJECT1 = "Lilavati Registration";
    */

    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
	public Utility()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public bool Execute(SqlCommand cmd)
    {
        try
        {            
            cmd.Connection = con;
            con.Open();
            int n = cmd.ExecuteNonQuery();
            return (n > 0);
        }
        catch (Exception ex)
        {
            return false;
            //throw;
        }
        finally
        {
            con.Close();
        }

    }

    public DataTable Display(string sql)
    {
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        da.Fill(dt);
        return dt;
    }

    public DataTable Display(SqlCommand cmd)
    {
        cmd.Connection = con;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        return dt;
    }

    public bool SendEmail(string strUserName, string strPassword, string ActivationCode, string strTLHNO, string strFirstName, string strUserId)
    {
        bool blnRetVal = false;
        try
        {
            SmtpClient mailClient = null;
            MailMessage message = null;
            StringBuilder mailbody = new StringBuilder();
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = EMAIL_HOST;
            mailClient.Port = EMAIL_PORT;

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(EMAIL_USERNAME, EMAIL_PASSWORD);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress = new MailAddress(strFrom, "Lilavati Hospital");
            message.From = fromAddress;

            mailbody.AppendFormat("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
            mailbody.AppendFormat("<tr><td colspan='2' align='center'><img src='http://lilavatihospital.com/images/emailer_logo.jpg' /><br /><hr color='#04869a' /></td></tr>");
            mailbody.AppendFormat("<tr><td colspan='2'>Dear " + strFirstName + ",<br /> Thanks for registering for your account with us. <br /> Find your login details below : </td> </tr>");
            //mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td width='70'>Email Id :</td><td><strong> " + strUserName + " </strong> </td> </tr>");
            //mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td>TLHNO :</td><td><strong> " + strTLHNO + " </strong> </td></tr>");
            mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td width='90'>User ID :</td><td><strong> " + strUserId + " </strong> </td> </tr>");
            mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td>Password :</td><td><strong> " + strPassword + " </strong> </td></tr>");
            mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center'>Please click the following Button to activate your account.</td></tr>");
            mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center'><a href='" + HttpContext.Current.Request.Url.AbsoluteUri.Replace("registration", "Activation.aspx?ActivationCode=" + ActivationCode) + "' style='width: auto; padding: 15px; display: inline-block; background: #04869a; color: #FFF; text-decoration: none;'>ACTIVATE MY ACCOUNT</a></td></tr>");
            mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
            mailbody.AppendFormat("<tr><td colspan='2' align='center' height='1'></td></tr>");
            mailbody.AppendFormat("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Lilavati Hospital & Research Centre</font></strong></td></tr>");
            mailbody.AppendFormat("<tr><td colspan='2' align='center' height='1'></td></tr>");
            mailbody.AppendFormat("</table>");

            message.To.Add(strUserName);
            message.Subject = EMAIL_SUBJECT;

            message.Body = mailbody.ToString();
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;


            /*send auto mail who fill the form*/
            /*
            string EmailId = strUserName;

            SmtpClient mailClient1 = null;
            MailMessage message1 = null;
            StringBuilder mailbody1 = new StringBuilder();
            mailClient1 = new SmtpClient();
            message1 = new MailMessage();
            mailClient1.Host = EMAIL_HOST1;
            mailClient1.Port = EMAIL_PORT1;

            System.Net.NetworkCredential SMTPUserInfo1 = new System.Net.NetworkCredential(EMAIL_USERNAME1, EMAIL_PASSWORD1);
            mailClient1.UseDefaultCredentials = false;
            mailClient1.Credentials = SMTPUserInfo;
            mailClient1.EnableSsl = false;
            mailClient1.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress1 = new MailAddress(EMAIL_USERNAME1);
            message1.From = fromAddress;

            mailbody1.AppendFormat("<body style='font-family:Verdana, Geneva, sans-serif; font-size:13px; color:#7b7c7e; font-weight:bold; line-height:22px;'>");

            mailbody1.AppendFormat("<div style='width:800px; height:625px; border:1px solid #ccc; margin:0 auto; padding:20px;'>");
            mailbody1.AppendFormat("<span style='width:150px; float:left;'>Thank You for Registration in  <a href='www.lilavatihospital.com/index.aspx'>LilavatiHospital</a>.</span><br />");

            mailbody1.AppendFormat("</p>");

            mailbody1.AppendFormat("</div>");

            mailbody1.AppendFormat("</body>");

            message1.To.Add(EmailId);
            message1.Subject = EMAIL_SUBJECT1;

            message1.Body = mailbody1.ToString();
            message1.IsBodyHtml = true;
            mailClient1.Send(message1);
            message1 = null;
            mailClient1 = null;
            */

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

    public bool SendLHNO(string strFirstName, string strLastName, string strLHNO,string strEmailID,string strPassword)
    {
        bool blnRetVal = false;
        try
        {
            SmtpClient mailClient = null;
            MailMessage message = null;
            StringBuilder mailbody = new StringBuilder();
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = EMAIL_HOST;
            mailClient.Port = EMAIL_PORT;

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(EMAIL_USERNAME, EMAIL_PASSWORD);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress = new MailAddress(EMAIL_USERNAME);
            message.From = fromAddress;

            mailbody.AppendFormat("<body style='font-family:Verdana, Geneva, sans-serif; font-size:13px; color:#7b7c7e; font-weight:bold; line-height:22px;'>");

            mailbody.AppendFormat("<div style='width:800px; height:625px; border:1px solid #ccc; margin:0 auto; padding:20px;'>");
            mailbody.AppendFormat("<span style='width:200px; float:left;'>First Name </span>: " + strFirstName + "<br />");
            mailbody.AppendFormat("<span style='width:200px; float:left;'>Last Name </span>: " + strLastName + "<br />");
            mailbody.AppendFormat("<span style='width:200px; float:left;'>LHNO </span>: " + strLHNO + "<br />");
            mailbody.AppendFormat("<span style='width:200px; float:left;'>Password </span>: " + strPassword + "<br />");

            mailbody.AppendFormat("</p>");

            mailbody.AppendFormat("</div>");

            mailbody.AppendFormat("</body>");

            message.To.Add(strEmailID);
            message.Subject = EMAIL_SUBJECT;

            message.Body = mailbody.ToString();
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;
                     

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

    public string GetUniqueName(string path, string initial, string ext, System.Web.UI.Page page, bool returnExtension)
    {
        //string uniquePart = Guid.NewGuid().ToString().Substring(0, 18);
        string uniquePart = DateTime.Now.ToFileTime().ToString().Substring(0, 18);
        string filename = string.Format("{0}{1}-{2}{3}", path, initial, uniquePart, ext);
        while (System.IO.File.Exists(page.Server.MapPath(filename)))
        {
            //uniquePart = Guid.NewGuid().ToString().Substring(0, 18);
            uniquePart = DateTime.Now.ToFileTime().ToString().Substring(0, 18);
            filename = string.Format("{0}{1}-{2}{3}", path, initial, uniquePart, ext);
        }
        if (returnExtension)
        {
            return string.Format("{0}-{1}{2}", initial, uniquePart, ext);
        }
        else
        {
            return string.Format("{0}-{1}", initial, uniquePart);
        }
    }

    public string GetUniqueName(string path, string initial, string ext, System.Web.UI.Page page)
    {
        return GetUniqueName(path, initial, ext, page, true);
    }

    public bool IsValidImageFileExtension(string extension)
    {
        return (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".bmp" || extension == ".gif");
    }
    public bool IsValidPDFFileExtension(string extension)
    {
        return (extension == ".pdf");
    }
    public bool IsValidResumeFileExtension(string extension)
    {
        return (extension == ".pdf" || extension == ".doc" || extension == ".docx");
    }
    public bool IsValidPPTFileExtension(string extension)
    {
        return (extension == ".ppt" || extension == ".pptx");
    }
    public bool IsValidExcelFileExtension(string extension)
    {
        return (extension == ".xls" || extension == ".xlsx");
    }

    #region Send Emails
    public int SendEMail(string strto, string Subject, string Body, string opt, string FilePath)
    {
        bool blnRetVal = false;
        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();

            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;

            string strMailUserName = "marketing";
            string strMailPassword = "lilavati12";

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            // string strFromMail = strMailUserName; //ConfigurationManager.AppSettings["FROM_ADDR"];

            string strFromMail = "marketing@lilavatihospital.com";
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;

            message.Priority = System.Net.Mail.MailPriority.High;
            //to mail address
            if (opt.Equals("Resume"))
            {

                message.To.Add("careers@lilavatihospital.com");                
                //message.To.Add("sunil.t@kwebmaker.com");
                //message.CC.Add("sunil.t@kwebmaker.com");

                Attachment attach = new Attachment(FilePath);
                message.Attachments.Add(attach);
            }
            else if (opt.Equals("Appointment"))
            {
                message.To.Add(strto);
                ////message.Bcc.Add("saloni@kwebmaker.com");
            }
            else if (opt.Equals(""))
            {
                message.To.Add(strto);
                //message.Bcc.Add("saloni@kwebmaker.com");
            }
            else if (opt.Equals("Healthcheck"))
            {
                //message.To.Add("marketing@lilavatihospital.com");
                // message.Bcc.Add("saloni@kwebmaker.com");
            }
            //message.Bcc.Add("info@bullionindia.in");
            message.Subject = Subject;


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }
    #endregion


    #region Online Lhrc Methords

    SqlConnection conOnlineLHRC = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);

    public bool ExecuteOnlineLHRC(SqlCommand cmd)
    {
        try
        {
            cmd.Connection = conOnlineLHRC;
            conOnlineLHRC.Open();
            int n = cmd.ExecuteNonQuery();
            return (n > 0);
        }
        catch (Exception ex)
        {
            return false;
            //throw;
        }
        finally
        {
            conOnlineLHRC.Close();
        }

    }

    public DataTable DisplayOnlineLHRC(string sql)
    {
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, conOnlineLHRC);
        da.Fill(dt);
        return dt;
    }

    public DataTable DisplayOnlineLHRC(SqlCommand cmd)
    {
        cmd.Connection = conOnlineLHRC;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        return dt;
    }

    #endregion

    public string ExtractHtmlInnerText(string htmlText)
    {
        //Match any Html tag (opening or closing tags) 
        // followed by any successive whitespaces
        //consider the Html text as a single line

        Regex regex = new Regex("(<.*?>\\s*)+", RegexOptions.Singleline);

        // replace all html tags (and consequtive whitespaces) by spaces
        // trim the first and last space

        string resultText = regex.Replace(htmlText, " ").Trim();

        return resultText;
    }

    public bool SendForgetEmailToPatient(string Userid, string mobileno, string firstname, string lastname)
    {
        bool blnRetVal = false;
        try
        {
            DataTable dt = new DataTable();
            Utility utility = new Utility();
            if (Userid != "")
            {
                dt = utility.Display("Execute Proc_PCipopRegistrationMaster 'GET_DETAIL_FORGET_MAIL_TIME_BY_USERNAME',0,'','','','',0,'','','','','','','','','','','','','','',0,'','','','','','','','" + Userid + "'");
            }
            else
            {
                dt = utility.Display("Execute Proc_PCipopRegistrationMaster 'GET_DETAIL_FORGET_MAIL_TIME_BY_MOBILENO_FNAME_LNAME',0,'','','" + mobileno.ToString() + "','',0,'','','','" + firstname.ToString() + "','','" + lastname.ToString() + "','','','','','','','','',0,'','','','','','','','" + Userid.ToString() + "'");
            }
            if (dt.Rows.Count > 0)
            {
                SmtpClient mailClient = null;
                MailMessage message = null;
                StringBuilder mailbody = new StringBuilder();
                mailClient = new SmtpClient();
                message = new MailMessage();
                mailClient.Host = EMAIL_HOST;
                mailClient.Port = EMAIL_PORT;

                System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(EMAIL_USERNAME, EMAIL_PASSWORD);
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = SMTPUserInfo;
                mailClient.EnableSsl = false;
                mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailAddress fromAddress = new MailAddress(strFrom, "Lilavati Hospital");
                message.From = fromAddress;

                mailbody.AppendFormat("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
                mailbody.AppendFormat("<tr><td colspan='2' align='center'><img src='http://lilavatihospital.com/images/emailer_logo.jpg' /><br /><hr color='#04869a' /></td></tr>");
                mailbody.AppendFormat("<tr><td colspan='2'>Dear " + dt.Rows[0]["FIRSTNAME"].ToString() + ",<br /> Find your login details below :</td> </tr>");
                mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td width='70'>User Name:</td><td><strong> " + dt.Rows[0]["USERNAME"].ToString() + " </strong> </td> </tr>");

                mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td>Password :</td><td><strong> " + dt.Rows[0]["PASSWORD"].ToString() + " </strong> </td></tr>");
                //mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center'>Please click the following Button to activate your account.</td></tr>");
                //mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center'><a href='" + HttpContext.Current.Request.Url.AbsoluteUri.Replace("registration", "Activation.aspx?ActivationCode=" + ActivationCode) + "' style='width: auto; padding: 15px; display: inline-block; background: #04869a; color: #FFF; text-decoration: none;'>ACTIVATE MY ACCOUNT</a></td></tr>");
                mailbody.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
                mailbody.AppendFormat("<tr><td colspan='2' align='center' height='1'></td></tr>");
                mailbody.AppendFormat("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Lilavati Hospital & Research Centre</font></strong></td></tr>");
                mailbody.AppendFormat("<tr><td colspan='2' align='center' height='1'></td></tr>");
                mailbody.AppendFormat("</table>");

                message.To.Add(dt.Rows[0]["EMAIL"].ToString());
                message.Subject = "Forgot Password";

                message.Body = mailbody.ToString();
                message.IsBodyHtml = true;
                mailClient.Send(message);
                message = null;
                mailClient = null;

                blnRetVal = true;
            }
            else
            {
                blnRetVal = false;
            }
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }
}

