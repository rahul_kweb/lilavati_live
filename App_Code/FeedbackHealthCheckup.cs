﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for FeedbackHealthCheckup
/// </summary>
public class FeedbackHealthCheckup
{
    //string strTo = "rahul@kwebmaker.com";
    //string strTo = "operations@lilavatihospital.com";

    string strTo = "feedback@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Feedback From Lilavati Website from Health Checkup";

	public FeedbackHealthCheckup()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailFeedbackHealthCheckup(string FirstName, string LastName, string LHNO, string Date, string ContactNo, string EmailID, string HealthCheckup, string HowDidYouCome, string AppointmentHowwouldyourate, string RegistrationHowwasyourdocument, string TestsPathologyBlood, string TestsRadiology_Xray, string TestsCardiologyECG, string TestsSpirometry, string ConsultationGeneralPhysician, string ConsultationGynaec, string ConsultationOphthalmology, string ConsultationENT, string ConsultationDental, string ConsultationSkin, string ConsultationDietition, string HousekeepingOverallhygiene, string Staff, string FoodBeverages, string Respectforyour, string Timetakenforhealthcheck, string Howwouldyourate, string WhatdidYouLike, string WhatDidYouLikeLeast, string AnySuggestionYou, string WouldYouLikeTOMention, string Service, string ServiceReason, string StaffPerformance, string AreaofWork, string AreaReason)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> FEEDBACK HEALTH CHECKUP DETAILS </font></strong></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td width='200px'>First Name </td><td> " + FirstName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Last Name </td> <td> " + LastName + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>LH No. </td> <td> " + LHNO + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Date </td> <td> " + Date + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Contact No. </td> <td> " + ContactNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Email ID </td> <td> " + EmailID + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Health Checkup scheme availed </td> <td> " + HealthCheckup + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>How did you come to know about Lilavati Hospital? </strong></td> <td> " + HowDidYouCome + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>How would you rate your overall experience at Lilavati hospital during Health Check Up</strong> </td> <td> " + Howwouldyourate + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong>Appointment</strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>How would you rate the way your appointment call was handled? </td> <td> " + AppointmentHowwouldyourate + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Registration & Billing </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>How was your document formalities and queries handled during Registration and payment? </td> <td> " + RegistrationHowwasyourdocument + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Test & Investigation </strong><br>How was your experience with technical staff(Doctor, Technician) during tests and investigations(in terms of guidance, Behaviour & Courtesy) </td></tr > ");
            strEmailBuilder.AppendFormat("<tr><td>Pathology (Blood Collection) </td> <td> " + TestsPathologyBlood + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Radiology (X ray, Sonography, CT ,Mammography, BMD) </td> <td> " + TestsRadiology_Xray + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Cardiology (ECG, Stress test, 2D Echo, etc) </td> <td> " + TestsCardiologyECG + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Spirometry </td> <td> " + TestsSpirometry + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Staff </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Behaviour of customer care supervisors and guidance provided by them during health check up </td> <td> " + Staff + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Consultation/s </strong><br>How was your experience with our doctors(were all your queries attended properly) ?</td></tr> ");
            strEmailBuilder.AppendFormat("<tr><td>General Physician </td> <td> " + ConsultationGeneralPhysician + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Gynaec/ Surgeon </td> <td> " + ConsultationGynaec + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Ophthalmology </td> <td> " + ConsultationOphthalmology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>ENT </td> <td> " + ConsultationENT + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Dental </td> <td> " + ConsultationDental + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Skin </td> <td> " + ConsultationSkin + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Dietition </td> <td> " + ConsultationDietition + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Food & Beverages </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Quality of Food & Beverages served </td> <td> " + FoodBeverages + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Housekeeping </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Overall hygiene and cleanliness of the health check up department. </td> <td> " + HousekeepingOverallhygiene + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong>Respect for your privacy</strong></td> <td> " + Respectforyour + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>Time taken for health check up as against your expectation</strong> </td> <td> " + Timetakenforhealthcheck + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>What did you like the most about Lilavati hospital?</strong> </td> <td> " + WhatdidYouLike + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>What did you like the least about Lilavati hospital?</strong> </td> <td> " + WhatDidYouLikeLeast + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>Any suggestion you would like to put forward which will help us to serve you better</strong> </td> <td> " + AnySuggestionYou + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Would you like to mention of our services for it's outstanding quality and/ recommend any staff for their outstanding performance? </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Service </td> <td>"+ Service + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Reason </td> <td>"+ ServiceReason + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Staff </td> <td>"+ StaffPerformance + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Area of work </td> <td>"+ AreaofWork + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Reason </td> <td>"+ AreaReason + " </td> </tr>");



            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString());

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}