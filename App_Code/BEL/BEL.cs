﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BEL
/// </summary>
public class BEL
{
	public BEL()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}


#region set property
public class RegistrationTable
{
    public int AddressIndex { get; set; }
    public string SureName { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string Sex { get; set; }
    public string DOB { get; set; }
    public string MaritalStatus { get; set; }
    public string Religion { get; set; }
    public string Occupation { get; set; }
    public string Address { get; set; }
    public string TelephoneResidence { get; set; }
    public string MobileNo { get; set; }
    public string EmailID { get; set; }
    public string Password { get; set; }
    public string LHNO { get; set; }

    public string Country { get; set; }
    public string State { get; set; }
    public string City { get; set; }
    public string Pincode { get; set; }
    public string NewPassword { get; set; }
    public string ORILHNO { get; set; }
    public string TLHNO { get; set; }
    public string UserName { get; set; }
    public string Title { get; set; }
    public string BloodGroup { get; set; }
    public string OriginalLHNO { get; set; }

    public int _AddressIndex
    {
        get
        {
            return AddressIndex;
        }
        set
        {
            AddressIndex = value;
        }
    }

    public string _SureName
    {
        get
        {
            return SureName;
        }
        set
        {
            SureName = value;
        }
    }

    public string _FirstName
    {
        get
        {
            return FirstName;
        }
        set
        {
            FirstName = value;
        }
    }

    public string _MiddleName
    {
        get
        {
            return MiddleName;
        }
        set
        {
            MiddleName = value;
        }
    }

    public string _Sex
    {
        get
        {
            return Sex;
        }
        set
        {
            Sex = value;
        }
    }

    public string _DOB
    {
        get
        {
            return DOB;
        }
        set
        {
            DOB = value;
        }
    }

    public string _MaritalStatus
    {
        get
        {
            return MaritalStatus;
        }
        set
        {
            MaritalStatus = value;
        }
    }

    public string _Religion
    {
        get
        {
            return Religion;
        }
        set
        {
            Religion = value;
        }
    }

    public string _Occupation
    {
        get
        {
            return Occupation;
        }
        set
        {
            Occupation = value;
        }
    }

    public string _Address
    {
        get
        {
            return Address;
        }
        set
        {
            Address = value;
        }
    }

    public string _TelephoneResidence
    {
        get
        {
            return TelephoneResidence;
        }
        set
        {
            TelephoneResidence = value;
        }
    }

    public string _MobileNo
    {
        get
        {
            return MobileNo;
        }
        set
        {
            MobileNo = value;
        }
    }

    public string _EmailID
    {
        get
        {
            return EmailID;
        }
        set
        {
            EmailID = value;
        }
    }

    public string _Password
    {
        get
        {
            return Password;
        }
        set
        {
            Password = value;
        }
    }

    public string _LHNO
    {
        get
        {
            return LHNO;
        }
        set
        {
            LHNO = value;
        }
    }

    public string _Country
    {
        get
        {
            return Country;
        }
        set
        {
            Country = value;
        }
    }

    public string _State
    {
        get
        {
            return State;
        }
        set
        {
            State = value;
        }
    }

    public string _City
    {
        get
        {
            return City;
        }
        set
        {
            City = value;
        }
    }

    public string _Pincode
    {
        get
        {
            return Pincode;
        }
        set
        {
            Pincode = value;
        }
    }

    public string _NewPassword
    {
        get
        {
            return NewPassword;
        }
        set
        {
            NewPassword = value;
        }
    }

    public string _ORILHNO
    {
        get
        {
            return ORILHNO;
        }
        set
        {
            ORILHNO = value;
        }
    }

    public string _TLHNO
    {
        get
        {
            return TLHNO;
        }
        set
        {
            TLHNO = value;
        }
    }

    public string _UserName
    {
        get
        {
            return UserName;
        }
        set
        {
            UserName = value;
        }
    }

    public string _Title
    {
        get
        {
            return Title;
        }
        set
        {
            Title = value;
        }
    }

    public string _BloodGroup
    {
        get
        {
            return BloodGroup;
        }
        set
        {
            BloodGroup = value;
        }
    }

    public string _OriginalLHNO
    {
        get
        {
            return OriginalLHNO;
        }
        set
        {
            OriginalLHNO = value;
        }
    }

}
#endregion

#region Set Speciality
public class SpecialityTable
{
    public int Speciality_Id_bint { get; set; }
    public string Speciality_vcr { get; set; }

    public int _Speciality_Id_bint
    {
        get
        {
            return Speciality_Id_bint;
        }
        set
        {
            Speciality_Id_bint = value;
        }
    }

    public string _Speciality_vcr
    {
        get
        {
            return Speciality_vcr;
        }
        set
        {
            Speciality_vcr = value;
        }
    }
}
#endregion

#region Set DoctorMaster
public class DoctorMasterTable
{
    public int Doctor_Id_bint { get; set; }
    public string Doctor_Name_vcr { get; set; }

    public int _Doctor_Id_bint
    {
        get
        {
            return Doctor_Id_bint;
        }
        set
        {
            Doctor_Id_bint = value;
        }
    }

    public string _Doctor_Name_vcr
    {
        get
        {
            return Doctor_Name_vcr;
        }
        set
        {
            Doctor_Name_vcr = value;
        }
    }
}
#endregion

#region Set AppointmentMaster
public class AppointmentTable
{
    public int App_Id_bint;
    public int Speciality_Id_bint;
    public int Doctor_Id_bint;
    public int AddressIndex;
    public string App_Avai_Time;
    public string App_Date;
    public string App_Visit_Time;
    public string Doctor_Name_vcr;
    public string ConsDrId;
    public string Doctorname;
    public string Specialityname;


    public int _App_Id_bint
    {
        get
        {
            return App_Id_bint;
        }
        set
        {
            App_Id_bint = value;
        }
    }

    public int _Speciality_Id_bint
    {
        get
        {
            return Speciality_Id_bint;
        }
        set
        {
            Speciality_Id_bint = value;
        }
    }

    public int _Doctor_Id_bint
    {
        get
        {
            return Doctor_Id_bint;
        }
        set
        {
            Doctor_Id_bint = value;
        }
    }

    public int _AddressIndex
    {
        get
        {
            return AddressIndex;
        }
        set
        {
            AddressIndex = value;
        }
    }

    public string _App_Avai_Time
    {
        get
        {
            return App_Avai_Time;
        }
        set
        {
            App_Avai_Time = value;
        }
    }

    public string _App_Date
    {
        get
        {
            return App_Date;
        }
        set
        {
            App_Date = value;
        }
    }

    public string _App_Visit_Time
    {
        get
        {
            return App_Visit_Time;
        }
        set
        {
            App_Visit_Time = value;
        }
    }

    public string _Doctor_Name_vcr
    {
        get
        {
            return Doctor_Name_vcr;
        }
        set
        {
            Doctor_Name_vcr = value;
        }
    }

    public string _Doctorname
    {
        get
        {
            return Doctorname;
        }
        set
        {
            Doctorname = value;
        }
    }

    public string _ConsDrId
    {
        get
        {
            return ConsDrId;
        }
        set
        {
            ConsDrId = value;
        }
    }

    public string _Specialityname
    {
        get
        {
            return Specialityname;
        }
        set
        {
            Specialityname = value;
        }
    }
}
#endregion

#region Set UserActivation
public class UserActivation
{
    public int AddressIndex { get; set; }
    public string ActivationCode { get; set; }

    public int _AddressIndex
    {
        get
        {
            return AddressIndex;
        }
        set
        {
            AddressIndex = value;
        }
    }

    public string _ActivationCode
    {
        get
        {
            return ActivationCode;
        }
        set
        {
            ActivationCode = value;
        }
    }
}
#endregion

#region AppointmentDetails
public class AdminAppointmentDetails
{
    public int AddressIndex { get; set; }
    public string SureName { get; set; }
    public string FirstName { get; set; }
    public string MiddleName { get; set; }
    public string Sex { get; set; }
    public string DOB { get; set; }
    public string MaritalStatus { get; set; }
    public string Religion { get; set; }
    public string Occupation { get; set; }
    public string Address { get; set; }
    public string TelephoneResidence { get; set; }
    public string MobileNo { get; set; }
    public string EmailID { get; set; }
    public string Password { get; set; }
    public string LHNO { get; set; }

    public int App_Id_bint { get; set; }
    //public int Speciality_Id_bint { get; set; }
    //public int Doctor_Id_bint { get; set; }
    public string App_Avai_Time { get; set; }
    public string App_Date { get; set; }
    public string App_Visit_Time { get; set; }
    public string Doctor_Name_vcr { get; set; }

    public string SpecialityName { get; set; }
    public string DoctorName { get; set; }

    public string fromdate;
    public string todate;
    public int Doctor_Id_bint = 0;
    public int Speciality_Id_bint = 0;

    public string Title { get; set; }
    public string BloodGroup { get; set; }
    public string OriginalLHNO { get; set; }

    public int _AddressIndex
    {
        get
        {
            return AddressIndex;
        }
        set
        {
            AddressIndex = value;
        }
    }

    public string _SureName
    {
        get
        {
            return SureName;
        }
        set
        {
            SureName = value;
        }
    }

    public string _FirstName
    {
        get
        {
            return FirstName;
        }
        set
        {
            FirstName = value;
        }
    }

    public string _MiddleName
    {
        get
        {
            return MiddleName;
        }
        set
        {
            MiddleName = value;
        }
    }

    public string _Sex
    {
        get
        {
            return Sex;
        }
        set
        {
            Sex = value;
        }
    }

    public string _DOB
    {
        get
        {
            return DOB;
        }
        set
        {
            DOB = value;
        }
    }

    public string _MaritalStatus
    {
        get
        {
            return MaritalStatus;
        }
        set
        {
            MaritalStatus = value;
        }
    }

    public string _Religion
    {
        get
        {
            return Religion;
        }
        set
        {
            Religion = value;
        }
    }

    public string _Occupation
    {
        get
        {
            return Occupation;
        }
        set
        {
            Occupation = value;
        }
    }

    public string _Address
    {
        get
        {
            return Address;
        }
        set
        {
            Address = value;
        }
    }

    public string _TelephoneResidence
    {
        get
        {
            return TelephoneResidence;
        }
        set
        {
            TelephoneResidence = value;
        }
    }

    public string _MobileNo
    {
        get
        {
            return MobileNo;
        }
        set
        {
            MobileNo = value;
        }
    }

    public string _EmailID
    {
        get
        {
            return EmailID;
        }
        set
        {
            EmailID = value;
        }
    }

    public string _Password
    {
        get
        {
            return Password;
        }
        set
        {
            Password = value;
        }
    }

    public string _LHNO
    {
        get
        {
            return LHNO;
        }
        set
        {
            LHNO = value;
        }
    }

    public int _App_Id_bint
    {
        get
        {
            return App_Id_bint;
        }
        set
        {
            App_Id_bint = value;
        }
    }

    public int _Speciality_Id_bint
    {
        get
        {
            return Speciality_Id_bint;
        }
        set
        {
            Speciality_Id_bint = value;
        }
    }

    public int _Doctor_Id_bint
    {
        get
        {
            return Doctor_Id_bint;
        }
        set
        {
            Doctor_Id_bint = value;
        }
    }

    public string _App_Avai_Time
    {
        get
        {
            return App_Avai_Time;
        }
        set
        {
            App_Avai_Time = value;
        }
    }

    public string _App_Date
    {
        get
        {
            return App_Date;
        }
        set
        {
            App_Date = value;
        }
    }

    public string _App_Visit_Time
    {
        get
        {
            return App_Visit_Time;
        }
        set
        {
            App_Visit_Time = value;
        }
    }

    public string _Doctor_Name_vcr
    {
        get
        {
            return Doctor_Name_vcr;
        }
        set
        {
            Doctor_Name_vcr = value;
        }
    }

    public string _SpecialityName
    {
        get
        {
            return SpecialityName;
        }
        set
        {
            SpecialityName = value;
        }
    }

    //public string _DoctorName
    //{
    //    get
    //    {
    //        return DoctorName;
    //    }
    //    set
    //    {
    //        DoctorName = value;
    //    }
    //}

    public string _DoctorName
    {
        get
        {
            return DoctorName;
        }
        set
        {
            DoctorName = value;
        }
    }


    public string _fromdate
    {
        get
        {
            return fromdate;
        }
        set
        {
            fromdate = value;
        }
    }

    public string _todate
    {
        get
        {
            return todate;
        }
        set
        {
            todate = value;
        }
    }


    public string _Title
    {
        get
        {
            return Title;
        }
        set
        {
            Title = value;
        }
    }

    public string _BloodGroup
    {
        get
        {
            return BloodGroup;
        }
        set
        {
            BloodGroup = value;
        }
    }

    public string _OriginalLHNO
    {
        get
        {
            return OriginalLHNO;
        }
        set
        {
            OriginalLHNO = value;
        }
    }

}
#endregion

#region CMS Content
public class CmsMaster
{
    public int ID { get; set; }
    public string Heading { get; set; }
    public string PageName { get; set; }
    public string Description { get; set; }

    public int _ID
    {
        get
        {
            return ID;
        }
        set
        {
            ID = value;
        }
    }

    public string _Heading
    {
        get
        {
            return Heading;
        }
        set
        {
            Heading = value;
        }
    }

    public string _PageName
    {
        get
        {
            return PageName;
        }
        set
        {
            PageName = value;
        }
    }

    public string _Description
    {
        get
        {
            return Description;
        }
        set
        {
            Description = value;
        }
    }
}
#endregion

/*xxxxxxxxxxxxxxx-my code on 21-05-16-xxxxxxxxxxxxxxxxxxxxxxx*/

/*Speciality start */
#region Speciality
public class Specialty
{
    public string Specialityname;
    public string Group;
    public string Details;
    public string Facilities;
    public string Services;
    public int Id = 0;
    public string buttontext;
    public string Motto;
    public string seachKey;
    public string CheckBoxId;



    public string _Specialityname
    {
        get
        {
            return Specialityname;
        }
        set
        {
            Specialityname = value;
        }
    }
    public string _Group
    {
        get
        {
            return Group;
        }
        set
        {
            Group = value;
        }
    }
    public string _Details
    {
        get
        {
            return Details;
        }
        set
        {
            Details = value;
        }
    }
    public string _Facilities
    {
        get
        {
            return Facilities;
        }
        set
        {
            Facilities = value;
        }
    }
    public string _Services
    {
        get
        {
            return Services;
        }
        set
        {
            Services = value;
        }
    }
    public int _Id
    {
        get
        {
            return Id;
        }
        set
        {
            Id = value;
        }
    }
    public string _buttontext
    {
        get
        {
            return buttontext;
        }
        set
        {
            buttontext = value;
        }
    }
    public string _Motto
    {
        get
        {
            return Motto;
        }
        set
        {
            Motto = value;
        }
    }
    public string _seachKey
    {
        get
        {
            return seachKey;
        }
        set
        {
            seachKey = value;
        }
    }
    public string _CheckBoxId
    {
        get
        {
            return CheckBoxId;
        }
        set
        {
            CheckBoxId = value;
        }
    }
}
#endregion
/*Speciality end */


/*Doctors start */
#region Doctors
public class Doctor
{
    /*create a objects*/
    public int DoctorId = 0;
    public string Doctorname;
    public string Achievements;
    public string Qualification;
    public string Areaofinterest;
    public string Gender;
    public string AvailTimeMon;
    public string AvailTimeTue;
    public string AvailTimeWed;
    public string AvailTimeThurs;
    public string AvailTimeFri;
    public string AvailTimeSat;
        public bool NotAvailMonbit;
    public bool NotAvailTuebit;
    public bool NotAvailWedbit;
    public bool NotAvailThursbit;
    public bool NotAvailFribit;
    public bool NotAvailSatbit;
    public string Room_vcr_Mon;
    public string Room_vcr_Tue;
    public string Room_vcr_Wed;
    public string Room_vcr_Thurs;
    public string Room_vcr_Fri;
    public string Room_vcr_Sat;
    public string Speciality;
    public List<string> MultipleSpeciality = new List<string>();
    public List<string> MultipleSpecialitydelete = new List<string>();
    public string Photo;
    public string Buttontext;
    public string SlotMinuts;
    public string ConsDrId;
    public string ResearchPub;
    public string Search;
    public string ProfileCnt;

    public string AvailTimeMon1;
    public string AvailTimeTue1;
    public string AvailTimeWed1;
    public string AvailTimeThurs1;
    public string AvailTimeFri1;
    public string AvailTimeSat1;
    public string Room_vcr_Mon1;
    public string Room_vcr_Tue1;
    public string Room_vcr_Wed1;
    public string Room_vcr_Thurs1;
    public string Room_vcr_Fri1;
    public string Room_vcr_Sat1;

    ///  Direct Available

    public bool DirectAvailable;
    public string ContactNo;
    public string DirectTimeFrom;
    public string DirectTimeTo;
    public string DirectDaysFrom;
    public string DirectDaysTo;

    /*get/set a objects*/
    public int _DoctorId
    {
        get
        {
            return DoctorId;
        }
        set
        {
            DoctorId = value;
        }
    }
    public string _Doctorname
    {
        get
        {
            return Doctorname;
        }
        set
        {
            Doctorname = value;
        }
    }
    public string _Achievements
    {
        get
        {
            return Achievements;
        }
        set
        {
            Achievements = value;
        }
    }
    public string _Qualification
    {
        get
        {
            return Qualification;
        }
        set
        {
            Qualification = value;
        }
    }
    public string _Areaofinterest
    {
        get
        {
            return Areaofinterest;
        }
        set
        {
            Areaofinterest = value;
        }
    }
    public string _Gender
    {
        get
        {
            return Gender;
        }
        set
        {
            Gender = value;
        }
    }
    public string _AvailTimeMon
    {
        get
        {
            return AvailTimeMon;
        }
        set
        {
            AvailTimeMon = value;
        }
    }
    public string _AvailTimeTue
    {
        get
        {
            return AvailTimeTue;
        }
        set
        {
            AvailTimeTue = value;
        }
    }
    public string _AvailTimeWed
    {
        get
        {
            return AvailTimeWed;
        }
        set
        {
            AvailTimeWed = value;
        }
    }
    public string _AvailTimeThurs
    {
        get
        {
            return AvailTimeThurs;
        }
        set
        {
            AvailTimeThurs = value;
        }
    }
    public string _AvailTimeFri
    {
        get
        {
            return AvailTimeFri;
        }
        set
        {
            AvailTimeFri = value;
        }
    }
    public string _AvailTimeSat
    {
        get
        {
            return AvailTimeSat;
        }
        set
        {
            AvailTimeSat = value;
        }
    }
    public bool _NotAvailMonbit
    {
        get
        {
            return NotAvailMonbit;
        }
        set
        {
            NotAvailMonbit = value;
        }
    }
    public bool _NotAvailTuebit
    {
        get
        {
            return NotAvailTuebit;
        }
        set
        {
            NotAvailTuebit = value;
        }
    }
    public bool _NotAvailWedbit
    {
        get
        {
            return NotAvailWedbit;
        }
        set
        {
            NotAvailWedbit = value;
        }
    }
    public bool _NotAvailThursbit
    {
        get
        {
            return NotAvailThursbit;
        }
        set
        {
            NotAvailThursbit = value;
        }
    }
    public bool _NotAvailFribit
    {
        get
        {
            return NotAvailFribit;
        }
        set
        {
            NotAvailFribit = value;
        }
    }
    public bool _NotAvailSatbit
    {
        get
        {
            return NotAvailSatbit;
        }
        set
        {
            NotAvailSatbit = value;
        }
    }
    public string _Room_vcr_Mon
    {
        get
        {
            return Room_vcr_Mon;
        }
        set
        {
            Room_vcr_Mon = value;
        }
    }
    public string _Room_vcr_Tue
    {
        get
        {
            return Room_vcr_Tue;
        }
        set
        {
            Room_vcr_Tue = value;
        }
    }
    public string _Room_vcr_Wed
    {
        get
        {
            return Room_vcr_Wed;
        }
        set
        {
            Room_vcr_Wed = value;
        }
    }
    public string _Room_vcr_Thurs
    {
        get
        {
            return Room_vcr_Thurs;
        }
        set
        {
            Room_vcr_Thurs = value;
        }
    }
    public string _Room_vcr_Fri
    {
        get
        {
            return Room_vcr_Fri;
        }
        set
        {
            Room_vcr_Fri = value;
        }
    }
    public string _Room_vcr_Sat
    {
        get
        {
            return Room_vcr_Sat;
        }
        set
        {
            Room_vcr_Sat = value;
        }
    }
    public string _Speciality
    {
        get
        {
            return Speciality;
        }
        set
        {
            Speciality = value;
        }
    }
    public string _Photo
    {
        get
        {
            return Photo;
        }
        set
        {
            Photo = value;
        }
    }
    public string _Buttontext
    {
        get
        {
            return Buttontext;
        }
        set
        {
            Buttontext = value;
        }
    }

    public string _SlotMinuts
    {
        get
        {
            return SlotMinuts;
        }
        set
        {
            SlotMinuts = value;
        }
    }
    public string _ConsDrId
    {
        get
        {
            return ConsDrId;
        }
        set
        {
            ConsDrId = value;
        }
    }

    public string _ResearchPub
    {
        get
        {
            return ResearchPub;
        }
        set
        {
            ResearchPub = value;
        }
    }
    public string _Search
    {
        get
        {
            return Search;
        }
        set
        {
            Search = value;
        }
    }

    public string _ProfileCnt
    {
        get
        {
            return ProfileCnt;
        }
        set
        {
            ProfileCnt = value;
        }
    }

  
    
    public string _AvailTimeMon1
    {
        get
        {
            return AvailTimeMon1;
        }
        set
        {
            AvailTimeMon1 = value;
        }
    }
    public string _AvailTimeTue1
    {
        get
        {
            return AvailTimeTue1;
        }
        set
        {
            AvailTimeTue1 = value;
        }
    }
    public string _AvailTimeWed1
    {
        get
        {
            return AvailTimeWed1;
        }
        set
        {
            AvailTimeWed1 = value;
        }
    }
    public string _AvailTimeThurs1
    {
        get
        {
            return AvailTimeThurs1;
        }
        set
        {
            AvailTimeThurs1 = value;
        }
    }
    public string _AvailTimeFri1
    {
        get
        {
            return AvailTimeFri1;
        }
        set
        {
            AvailTimeFri1 = value;
        }
    }
    public string _AvailTimeSat1
    {
        get
        {
            return AvailTimeSat1;
        }
        set
        {
            AvailTimeSat1 = value;
        }
    }
    public string _Room_vcr_Mon1
    {
        get
        {
            return Room_vcr_Mon1;
        }
        set
        {
            Room_vcr_Mon1 = value;
        }
    }
    public string _Room_vcr_Tue1
    {
        get
        {
            return Room_vcr_Tue1;
        }
        set
        {
            Room_vcr_Tue1 = value;
        }
    }
    public string _Room_vcr_Wed1
    {
        get
        {
            return Room_vcr_Wed1;
        }
        set
        {
            Room_vcr_Wed1 = value;
        }
    }
    public string _Room_vcr_Thurs1
    {
        get
        {
            return Room_vcr_Thurs1;
        }
        set
        {
            Room_vcr_Thurs1 = value;
        }
    }
    public string _Room_vcr_Fri1
    {
        get
        {
            return Room_vcr_Fri1;
        }
        set
        {
            Room_vcr_Fri1 = value;
        }
    }
    public string _Room_vcr_Sat1
    {
        get
        {
            return Room_vcr_Sat1;
        }
        set
        {
            Room_vcr_Sat1 = value;
        }
    }

    /// Direct Appointment

    public bool _DirectAvailable
    {
        get
        {
            return DirectAvailable;
        }
        set
        {
            DirectAvailable = value;
        }
    }

    public string _ContactNo
    {
        get
        {
            return ContactNo;
        }
        set
        {
            ContactNo = value;
        }
    }

    public string _DirectTimeFrom
    {
        get
        {
            return DirectTimeFrom;
        }
        set
        {
            DirectTimeFrom = value;
        }
    }

    public string _DirectTimeTo
    {
        get
        {
            return DirectTimeTo;
        }
        set
        {
            DirectTimeTo = value;
        }
    }

    public string _DirectDaysFrom
    {
        get
        {
            return DirectDaysFrom;
        }
        set
        {
            DirectDaysFrom = value;
        }
    }

    public string _DirectDaysTo
    {
        get
        {
            return DirectDaysTo;
        }
        set
        {
            DirectDaysTo = value;
        }
    }

    public bool TeleConsultation { get; set; }
}
#endregion
/*Doctors end */


/* Career and Apply for job start*/
#region Career
public class Career
{
    public int JobId = 0;
    public string PostionName;
    public string DepartmentName;
    public int DepartmentId = 0;
    public string FirstName;
    public string LastName;
    public string DOB;
    public string Gender;
    public string Phone;
    public string Residentof;
    public string Presentlyworkinglocation;
    public string RelevantExp;
    public string TotalExp;
    public string MonthlyexpectedSalary;
    public string Qualification;
    public string HostelAccommodationrequired;
    public string RegistrationwithMMC;
    public string Fileofcv;
    public string FileExt;
    public string Emailid;
    public string Fullpath;



    public int _JobId
    {
        get
        {
            return JobId;
        }
        set
        {
            JobId = value;
        }
    }
    public string _PostionName
    {
        get
        {
            return PostionName;
        }
        set
        {
            PostionName = value;
        }
    }
    public string _DepartmentName
    {
        get
        {
            return DepartmentName;
        }
        set
        {
            DepartmentName = value;
        }
    }
    public int _DepartmentId
    {
        get
        {
            return DepartmentId;
        }
        set
        {
            DepartmentId = value;
        }
    }
    public string _FirstName
    {
        get
        {
            return FirstName;
        }
        set
        {
            FirstName = value; ;
        }
    }
    public string _LastName
    {
        get
        {
            return LastName;
        }
        set
        {
            LastName = value;
        }
    }
    public string _DOB
    {
        get
        {
            return DOB;
        }
        set
        {
            DOB = value;
        }
    }
    public string _Gender
    {
        get
        {
            return Gender;
        }
        set
        {
            Gender = value;
        }
    }
    public string _Phone
    {
        get
        {
            return Phone;
        }
        set
        {
            Phone = value;
        }
    }
    public string _Residentof
    {
        get
        {
            return Residentof;
        }
        set
        {
            Residentof = value;
        }
    }
    public string _Presentlyworkinglocation
    {
        get
        {
            return Presentlyworkinglocation;
        }
        set
        {
            Presentlyworkinglocation = value;
        }
    }
    public string _RelevantExp
    {
        get
        {
            return RelevantExp;
        }
        set
        {
            RelevantExp = value;
        }
    }
    public string _TotalExp
    {
        get
        {
            return TotalExp;
        }
        set
        {
            TotalExp = value;
        }
    }
    public string _MonthlyexpectedSalary
    {
        get
        {
            return MonthlyexpectedSalary;
        }
        set
        {
            MonthlyexpectedSalary = value;
        }
    }
    public string _Qualification
    {
        get
        {
            return Qualification;
        }
        set
        {
            Qualification = value;
        }
    }
    public string _HostelAccommodationrequired
    {
        get
        {
            return HostelAccommodationrequired;
        }
        set
        {
            HostelAccommodationrequired = value;
        }
    }
    public string _RegistrationwithMMC
    {
        get
        {
            return RegistrationwithMMC;
        }
        set
        {
            RegistrationwithMMC = value;
        }
    }
    public string _Fileofcv
    {
        get
        {
            return Fileofcv;
        }
        set
        {
            Fileofcv = value;
        }
    }
    public string _FileExt
    {
        get
        {
            return FileExt;
        }
        set
        {
            FileExt = value;
        }
    }
    public string _Emailid
    {
        get
        {
            return Emailid;
        }
        set
        {
            Emailid = value;
        }
    }
    public string _Fullpath
    {
        get
        {
            return Fullpath;
        }
        set
        {
            Fullpath = value;
        }
    }

}
#endregion
/* Career and Apply for job End*/

/*Innerbanner start*/
#region Innerbanner
public class InnerBanner
{
    public int innerid = 0;
    public string pagename = string.Empty;
    public string image = string.Empty;
    public string addedBy = string.Empty;
    public string ButtonName = string.Empty;


    public int _innerid
    {
        get
        {
            return innerid;
        }
        set
        {
            innerid = value;
        }
    }
    public string _pagename
    {
        get
        {
            return pagename;
        }
        set
        {
            pagename = value;
        }
    }
    public string _image
    {
        get
        {
            return image;
        }
        set
        {
            image = value;
        }
    }
    public string _addedBy
    {
        get
        {
            return addedBy;
        }
        set
        {
            addedBy = value;
        }
    }
    public string _ButtonName
    {
        get
        {
            return ButtonName;
        }
        set
        {
            ButtonName = value;
        }
    }
}
#endregion
/* Innerbanner End*/


/*xxxxxxxxxxxxxxx-my code end-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

#region Awards_Accreditations Table
public class Awards_Accreditations
{
    public int ID { get; set; }
    public string AwardsImage { get; set; }
    public string Tittle { get; set; }
    public string Description { get; set; }

    public int _ID
    {
        get
        {
            return ID;
        }
        set
        {
            ID = value;
        }
    }

    public string _AwardsImage
    {
        get
        {
            return AwardsImage;
        }
        set
        {
            AwardsImage = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }

    public string _Description
    {
        get
        {
            return Description;
        }
        set
        {
            Description = value;
        }
    }
}
#endregion

#region Media Press Year
public class MediaYear
{
    public int YearId { get; set; }
    public string YearName { get; set; }

    public int _YearId
    {
        get
        {
            return YearId;
        }
        set
        {
            YearId = value;
        }
    }

    public string _YearName
    {
        get
        {
            return YearName;
        }
        set
        {
            YearName = value;
        }
    }

}
#endregion

#region Media Press
public class MediaPress
{
    public int MEDIA_ID { get; set; }
    public int YEAR_ID { get; set; }
    public int YEARNAME_ID { get; set; }
    public string DESCRIPTION { get; set; }
    public string Year { get; set; }
    public string Month { get; set; }
    public string YearName { get; set; }
    public string PdfFile;
    public string Publication;
    public string Video { get; set; }

    public int _MEDIA_ID
    {
        get
        {
            return MEDIA_ID;
        }
        set
        {
            MEDIA_ID = value;
        }
    }

    public int _YEAR_ID
    {
        get
        {
            return YEAR_ID;
        }
        set
        {
            YEAR_ID = value;
        }
    }

    public int _YEARNAME_ID
    {
        get
        {
            return YEARNAME_ID;
        }
        set
        {
            YEARNAME_ID = value;
        }
    }

    public string _DESCRIPTION
    {
        get
        {
            return DESCRIPTION;
        }
        set
        {
            DESCRIPTION = value;
        }
    }

    public string _Year
    {
        get
        {
            return Year;
        }
        set
        {
            Year = value;
        }
    }

    public string _Month
    {
        get
        {
            return Month;
        }
        set
        {
            Month = value;
        }
    }

    public string _YearName
    {
        get
        {
            return YearName;
        }
        set
        {
            YearName = value;
        }
    }

    public string _PdfFile
    {
        get
        {
            return PdfFile;
        }
        set
        {
            PdfFile = value;
        }
    }

    public string _Publication
    {
        get
        {
            return Publication;
        }
        set
        {
            Publication = value;
        }
    }

    public string _Video
    {
        get
        {
            return Video;
        }
        set
        {
            Video = value;
        }
    }
}
#endregion

#region Testimonial
public class Testimonial
{
    public int Test_Id { get; set; }
    public string Type { get; set; }
    public string Name { get; set; }
    public string CelebrityImage { get; set; }
    public string Description { get; set; }
    public string TestImage { get; set; }
    public string Search { get; set; }

    public int _Test_Id
    {
        get
        {
            return Test_Id;
        }
        set
        {
            Test_Id = value;
        }
    }

    public string _Type
    {
        get
        {
            return Type;
        }
        set
        {
            Type = value;
        }
    }

    public string _Name
    {
        get
        {
            return Name;
        }
        set
        {
            Name = value;
        }
    }

    public string _CelebrityImage
    {
        get
        {
            return CelebrityImage;
        }
        set
        {
            CelebrityImage = value;
        }
    }

    public string _Description
    {
        get
        {
            return Description;
        }
        set
        {
            Description = value;
        }
    }

    public string _TestImage
    {
        get
        {
            return TestImage;
        }
        set
        {
            TestImage = value;
        }
    }

}
#endregion

#region Job Area
public class Job_Area
{
    public int Job_Id { get; set; }
    public string Job_Areas { get; set; }
    public string Job_Position { get; set; }

    public int _Job_Id
    {
        get
        {
            return Job_Id;
        }
        set
        {
            Job_Id = value;
        }
    }

    public string _Job_Areas
    {
        get
        {
            return Job_Areas;
        }
        set
        {
            Job_Areas = value;
        }
    }

    public string _Job_Position
    {
        get
        {
            return Job_Position;
        }
        set
        {
            Job_Position = value;
        }
    }
}
#endregion

#region Job Posting
public class Job_Posting
{
    public int Post_Id { get; set; }
    public int Job_Id { get; set; }
    public string Position { get; set; }
    public string Specification { get; set; }
    public string Duration { get; set; }
    public string Vacancy;

    public int _Post_Id
    {
        get
        {
            return Post_Id;
        }
        set
        {
            Post_Id = value;
        }
    }

    public int _Job_Id
    {
        get
        {
            return Job_Id;
        }
        set
        {
            Job_Id = value;
        }
    }

    public string _Position
    {
        get
        {
            return Position;
        }
        set
        {
            Position = value;
        }
    }

    public string _Specification
    {
        get
        {
            return Specification;
        }
        set
        {
            Specification = value;
        }
    }

    public string _Duration
    {
        get
        {
            return Duration;
        }
        set
        {
            Duration = value;
        }
    }

    public string _Vacancy
    {
        get
        {
            return Vacancy;
        }
        set
        {
            Vacancy = value;
        }
    }

}
#endregion

#region CMS Page Master
public class CMSPageMaster
{
    public int PageId { get; set; }
    public string PageName { get; set; }

    public int _PageId
    {
        get
        {
            return PageId;
        }
        set
        {
            PageId = value;
        }
    }

    public string _PageName
    {
        get
        {
            return PageName;
        }
        set
        {
            PageName = value;
        }
    }

}

#endregion

#region CMS Inner Page Master
public class CMSInnerPageMaster
{
    public int InnerId { get; set; }
    public int PageId { get; set; }
    public string PageName { get; set; }
    public string Tab { get; set; }
    public string Description;

    public int _InnerId
    {
        get
        {
            return InnerId;
        }
        set
        {
            InnerId = value;
        }
    }

    public int _PageId
    {
        get
        {
            return PageId;
        }
        set
        {
            PageId = value;
        }
    }

    public string _PageName
    {
        get
        {
            return PageName;
        }
        set
        {
            PageName = value;
        }
    }

    public string _Tab
    {
        get
        {
            return Tab;
        }
        set
        {
            Tab = value;
        }
    }

    public string _Description
    {
        get
        {
            return Description;
        }
        set
        {
            Description = value;
        }
    }

}

#endregion

#region Health Checkup

public class HealthCheckup
{
    public int Health_Id;
    public string Package_Name;

    public int _Health_Id
    {
        get
        {
            return Health_Id;
        }
        set
        {
            Health_Id = value;
        }
    }

    public string _Package_Name
    {
        get
        {
            return Package_Name;
        }
        set
        {
            Package_Name = value;
        }
    }
}

#endregion

#region Health Packages Details

public class HealthPackagesDetails
{
    public int Pack_Id;
    public int Health_Id;
    public string Pack_Price;
    public string Tittle;
    public string Tab_Name;
    public string Description;
    public string Note;
    public string Package_Name;

    public int _Pack_Id
    {
        get
        {
            return Pack_Id;
        }
        set
        {
            Pack_Id = value;
        }
    }

    public int _Health_Id
    {
        get
        {
            return Health_Id;
        }
        set
        {
            Health_Id = value;
        }
    }

    public string _Pack_Price
    {
        get
        {
            return Pack_Price;
        }
        set
        {
            Pack_Price = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }

    public string _Tab_Name
    {
        get
        {
            return Tab_Name;
        }
        set
        {
            Tab_Name = value;
        }
    }

    public string _Description
    {
        get
        {
            return Description;
        }
        set
        {
            Description = value;
        }
    }

    public string _Note
    {
        get
        {
            return Note;
        }
        set
        {
            Note = value;
        }
    }

    public string _Package_Name
    {
        get
        {
            return Package_Name;
        }
        set
        {
            Package_Name = value;
        }
    }
}

#endregion

#region Contact Us

public class ContactUs
{
    public int Cnt_Id;
    public string Address;
    public string Emergency_Casualty;
    public string Ambulance;
    public string Hospital_Board_Line;
    public string Hospital_Fax;
    public string Admission_Department;
    public string TPA_Cell;
    public string Billing_Inpatient;
    public string Billing_OPD;
    public string Appointment_OPD;
    public string Blood_Bank;
    public string Health_Check_up_Department;
    public string Report_Dispatch_Counter;
    public string MRI_Department;
    public string X_Ray_Sonography_Department;
    public string CT_Scan_Department;
    public string Email_Address;

    public string Nuclear_Medicine;
    public string Physiotherapy;
    public string Dental;
    public string Dermatology;
    public string Ophthalmology;
    public string ENT_Audiometry;
    public string IVF;
    public string EMG_EEG;
    public string Visa_Section;
    public string Cardiology;
    public string Sample_collection_room;
    public string Endoscopy;
    public string TollFree;
    public int _Cnt_Id
    {
        get
        {
            return Cnt_Id;
        }
        set
        {
            Cnt_Id = value;
        }
    }

    public string _Emergency_Casualty
    {
        get
        {
            return Emergency_Casualty;
        }
        set
        {
            Emergency_Casualty = value;
        }
    }

    public string _Ambulance
    {
        get
        {
            return Ambulance;
        }
        set
        {
            Ambulance = value;
        }
    }

    public string _Hospital_Board_Line
    {
        get
        {
            return Hospital_Board_Line;
        }
        set
        {
            Hospital_Board_Line = value;
        }
    }

    public string _Hospital_Fax
    {
        get
        {
            return Hospital_Fax;
        }
        set
        {
            Hospital_Fax = value;
        }
    }

    public string _Admission_Department
    {
        get
        {
            return Admission_Department;
        }
        set
        {
            Admission_Department = value;
        }
    }

    public string _TPA_Cell
    {
        get
        {
            return TPA_Cell;
        }
        set
        {
            TPA_Cell = value;
        }
    }

    public string _Billing_Inpatient
    {
        get
        {
            return Billing_Inpatient;
        }
        set
        {
            Billing_Inpatient = value;
        }
    }

    public string _Billing_OPD
    {
        get
        {
            return Billing_OPD;
        }
        set
        {
            Billing_OPD = value;
        }
    }

    public string _Appointment_OPD
    {
        get
        {
            return Appointment_OPD;
        }
        set
        {
            Appointment_OPD = value;
        }
    }

    public string _Blood_Bank
    {
        get
        {
            return Blood_Bank;
        }
        set
        {
            Blood_Bank = value;
        }
    }

    public string _Health_Check_up_Department
    {
        get
        {
            return Health_Check_up_Department;
        }
        set
        {
            Health_Check_up_Department = value;
        }
    }

    public string _Report_Dispatch_Counter
    {
        get
        {
            return Report_Dispatch_Counter;
        }
        set
        {
            Report_Dispatch_Counter = value;
        }
    }

    public string _MRI_Department
    {
        get
        {
            return MRI_Department;
        }
        set
        {
            MRI_Department = value;
        }
    }

    public string _X_Ray_Sonography_Department
    {
        get
        {
            return X_Ray_Sonography_Department;
        }
        set
        {
            X_Ray_Sonography_Department = value;
        }
    }

    public string _CT_Scan_Department
    {
        get
        {
            return CT_Scan_Department;
        }
        set
        {
            CT_Scan_Department = value;
        }
    }

    public string _Email_Address
    {
        get
        {
            return Email_Address;
        }
        set
        {
            Email_Address = value;
        }
    }

    public string _Address
    {
        get
        {
            return Address;
        }
        set
        {
            Address = value;
        }
    }

    public string _Nuclear_Medicine
    {
        get
        {
            return Nuclear_Medicine;
        }
        set
        {
            Nuclear_Medicine = value;
        }
    }

    public string _Physiotherapy
    {
        get
        {
            return Physiotherapy;
        }
        set
        {
            Physiotherapy = value;
        }
    }

    public string _Dental
    {
        get
        {
            return Dental;
        }
        set
        {
            Dental = value;
        }
    }

    public string _Dermatology
    {
        get
        {
            return Dermatology;
        }
        set
        {
            Dermatology = value;
        }
    }

    public string _Ophthalmology 
    {
        get
        {
            return Ophthalmology;
        }
        set
        {
            Ophthalmology = value;
        }
    }

    public string _ENT_Audiometry
    {
        get
        {
            return ENT_Audiometry;
        }
        set
        {
            ENT_Audiometry = value;
        }
    }

    public string _IVF
    {
        get
        {
            return IVF;
        }
        set
        {
            IVF = value;
        }
    }

    public string _EMG_EEG
    {
        get
        {
            return EMG_EEG;
        }
        set
        {
            EMG_EEG = value;
        }
    }

    public string _Visa_Section
    {
        get
        {
            return Visa_Section;
        }
        set
        {
            Visa_Section = value;
        }
    }

    public string _Cardiology
    {
        get
        {
            return Cardiology;
        }
        set
        {
            Cardiology = value;
        }
    }

    public string _Sample_collection_room
    {
        get
        {
            return Sample_collection_room;
        }
        set
        {
            Sample_collection_room = value;
        }
    }

    public string _Endoscopy
    {
        get
        {
            return Endoscopy;
        }
        set
        {
            Endoscopy = value;
        }
    }

    public string _TollFree
    {
        get
        {
            return TollFree;
        }
        set
        {
            TollFree = value;
        }
    }
}
#endregion

#region LHMT Admin

public class LHMT
{
    public int Id;
    public string Tittle;
    public string Brouchre;

    public int _Id
    {
        get
        {
            return Id;
        }
        set
        {
            Id = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }

    public string _Brouchre
    {
        get
        {
            return Brouchre;
        }
        set
        {
            Brouchre = value;
        }
    }
}

#endregion

#region Empanelled Companies Admin

public class EmpanelledCompanies
{
    public int Tab_Id;
    public string Tab_Name;

    public int _Tab_Id
    {
        get
        {
            return Tab_Id;
        }
        set
        {
            Tab_Id = value;
        }
    }

    public string _Tab_Name
    {
        get
        {
            return Tab_Name;
        }
        set
        {
            Tab_Name = value;
        }
    }

}
#endregion

#region Empanelled Companies Details Admin

public class EmpanelledDetails
{
    public int Emp_Id;
    public int Tab_Id;
    public string Name;
    public string Logo;
    public string Tab_Name;

    public int _Emp_Id
    {
        get
        {
            return Emp_Id;
        }
        set
        {
            Emp_Id = value;
        }
    }

    public int _Tab_Id
    {
        get
        {
            return Tab_Id;
        }
        set
        {
            Tab_Id = value;
        }
    }

    public string _Name
    {
        get
        {
            return Name;
        }
        set
        {
            Name = value;
        }
    }

    public string _Logo
    {
        get
        {
            return Logo;
        }
        set
        {
            Logo = value;
        }
    }

    public string _Tab_Name
    {
        get
        {
            return Tab_Name;
        }
        set
        {
            Tab_Name = value;
        }
    }

}

#endregion

#region Patients Education Admin

public class PatientsEducation
{
    public int Pat_Id;
    public string Tittle;
    public string Brochure;
    public string ThumbImage;

    public int _Pat_Id
    {
        get
        {
            return Pat_Id;
        }
        set
        {
            Pat_Id = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }

    public string _Brochure
    {
        get
        {
            return Brochure;
        }
        set
        {
            Brochure = value;
        }
    }

    public string _ThumbImage
    {
        get
        {
            return ThumbImage;
        }
        set
        {
            ThumbImage = value;
        }
    }

}
#endregion

#region Patients Education Brochure Admin

public class PatientsEducationBrochure
{
    public int Broc_Id;
    public int Pat_Id;
    public string Tittle;
    public string Brochure;

    public int _Broc_Id
    {
        get
        {
            return Broc_Id;
        }
        set
        {
            Broc_Id = value;
        }
    }

    public int _Pat_Id
    {
        get
        {
            return Pat_Id;
        }
        set
        {
            Pat_Id = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }

    public string _Brochure
    {
        get
        {
            return Brochure;
        }
        set
        {
            Brochure = value;
        }
    }

}

#endregion

#region Feedback InPatient

public class FeedBackInPatient
{
    public int ID;
    public string FirstName;
    public string LastName;
    public string Lh_No;
    public string DateOfAdmission;
    public string PhoneNo;
    public string BedNo;
    public string DateOfDischarge;
    public string EmailId;
    public string HowDidYouComeToKnow;
    public string GuidanceSupport;
    public string CourtesyAndFriendliness;
    public string TimeTakenToComplete;
    public string ClarityProvided;
    public string CareAttention;
    public string DoctorsEfforts;
    public string CourtesyFriendnessNursing;
    public string CareAndAttentionMedication;
    public string ResponseToYou;
    public string RespectForPrivacy;
    public string CountesyOfStaff;
    public string ClarityOfInstructions;
    public string QualityOfFood;
    public string VarietyOfFood;
    public string PromptnessOfService;
    public string InformationGuidance;
    public string CleanlinessOfRoom;
    public string ResponseToYouNeed;
    public string CourtesyAndHelpfulness;
    public string CourtesyOfSecurity;
    public string Professionalism;
    public string BillingCounseling;
    public string PromptnessOfBilling;
    public string CountesyHelpfulness;
    public string PromptnessOfDischarge;
    public string DischargeCounseling;
    public string ConvenienceOfDischarge;
    public string HowWouldYouRate;
    public string WhatDidYouLike;
    public string WhatDidYouLikeLeast;
    public string AnysuggestionYouWouldLike;
    public string WouldYouLikeToRecommend;

    public string Pathology_BloodCollection;
    public string Radiology_X_ray;
    public string Cardiology_ECG_Stress_test;
    public string Nuclear_Dept_BoneScanLiverScan;
    public string TimetakenforTestRadiology_X_ray;
    public string TimetakenforTestCardiology_ECG_Stress_test;
    public string TimetakenforTestNuclear_Dept_BoneScanLiverScan;

    public string TimetakenforTestPathology;

    public int _ID
    {
        get
        {
            return ID;
        }
        set
        {
            ID = value;
        }
    }

    public string _FirstName
    {
        get
        {
            return FirstName;
        }
        set
        {
            FirstName = value;
        }
    }

    public string _LastName
    {
        get
        {
            return LastName;
        }
        set
        {
            LastName = value;
        }
    }

    public string _Lh_No
    {
        get
        {
            return Lh_No;
        }
        set
        {
            Lh_No = value;
        }
    }

    public string _DateOfAdmission
    {
        get
        {
            return DateOfAdmission;
        }
        set
        {
            DateOfAdmission = value;
        }
    }

    public string _PhoneNo
    {
        get
        {
            return PhoneNo;
        }
        set
        {
            PhoneNo = value;
        }
    }

    public string _BedNo
    {
        get
        {
            return BedNo;
        }
        set
        {
            BedNo = value;
        }
    }

    public string _DateOfDischarge
    {
        get
        {
            return DateOfDischarge;
        }
        set
        {
            DateOfDischarge = value;
        }
    }

    public string _EmailId
    {
        get
        {
            return EmailId;
        }
        set
        {
            EmailId = value;
        }
    }

    public string _HowDidYouComeToKnow
    {
        get
        {
            return HowDidYouComeToKnow;
        }
        set
        {
            HowDidYouComeToKnow = value;
        }
    }

    public string _GuidanceSupport
    {
        get
        {
            return GuidanceSupport;
        }
        set
        {
            GuidanceSupport = value;
        }
    }

    public string _CourtesyAndFriendliness
    {
        get
        {
            return CourtesyAndFriendliness;
        }
        set
        {
            CourtesyAndFriendliness = value;
        }
    }

    public string _TimeTakenToComplete
    {
        get
        {
            return TimeTakenToComplete;
        }
        set
        {
            TimeTakenToComplete = value;
        }
    }

    public string _ClarityProvided
    {
        get
        {
            return ClarityProvided;
        }
        set
        {
            ClarityProvided = value;
        }
    }

    public string _CareAttention
    {
        get
        {
            return CareAttention;
        }
        set
        {
            CareAttention = value;
        }
    }

    public string _DoctorsEfforts
    {
        get
        {
            return DoctorsEfforts;
        }
        set
        {
            DoctorsEfforts = value;
        }
    }

    public string _CourtesyFriendnessNursing
    {
        get
        {
            return CourtesyFriendnessNursing;
        }
        set
        {
            CourtesyFriendnessNursing = value;
        }
    }

    public string _CareAndAttentionMedication
    {
        get
        {
            return CareAndAttentionMedication;
        }
        set
        {
            CareAndAttentionMedication = value;
        }
    }

    public string _ResponseToYou
    {
        get
        {
            return ResponseToYou;
        }
        set
        {
            ResponseToYou = value;
        }
    }

    public string _RespectForPrivacy
    {
        get
        {
            return RespectForPrivacy;
        }
        set
        {
            RespectForPrivacy = value;
        }
    }

    public string _CountesyOfStaff
    {
        get
        {
            return CountesyOfStaff;
        }
        set
        {
            CountesyOfStaff = value;
        }
    }

    public string _ClarityOfInstructions
    {
        get
        {
            return ClarityOfInstructions;
        }
        set
        {
            ClarityOfInstructions = value;
        }
    }

    public string _QualityOfFood
    {
        get
        {
            return QualityOfFood;
        }
        set
        {
            QualityOfFood = value;
        }
    }

    public string _VarietyOfFood
    {
        get
        {
            return VarietyOfFood;
        }
        set
        {
            VarietyOfFood = value;
        }
    }

    public string _PromptnessOfService
    {
        get
        {
            return PromptnessOfService;
        }
        set
        {
            PromptnessOfService = value;
        }
    }

    public string _InformationGuidance
    {
        get
        {
            return InformationGuidance;
        }
        set
        {
            InformationGuidance = value;
        }
    }

    public string _CleanlinessOfRoom
    {
        get
        {
            return CleanlinessOfRoom;
        }
        set
        {
            CleanlinessOfRoom = value;
        }
    }

    public string _ResponseToYouNeed
    {
        get
        {
            return ResponseToYouNeed;
        }
        set
        {
            ResponseToYouNeed = value;
        }
    }

    public string _CourtesyAndHelpfulness
    {
        get
        {
            return CourtesyAndHelpfulness;
        }
        set
        {
            CourtesyAndHelpfulness = value;
        }
    }

    public string _CourtesyOfSecurity
    {
        get
        {
            return CourtesyOfSecurity;
        }
        set
        {
            CourtesyOfSecurity = value;
        }
    }

    public string _Professionalism
    {
        get
        {
            return Professionalism;
        }
        set
        {
            Professionalism = value;
        }
    }

    public string _BillingCounseling
    {
        get
        {
            return BillingCounseling;
        }
        set
        {
            BillingCounseling = value;
        }
    }

    public string _PromptnessOfBilling
    {
        get
        {
            return PromptnessOfBilling;
        }
        set
        {
            PromptnessOfBilling = value;
        }
    }

    public string _CountesyHelpfulness
    {
        get
        {
            return CountesyHelpfulness;
        }
        set
        {
            CountesyHelpfulness = value;
        }
    }

    public string _PromptnessOfDischarge
    {
        get
        {
            return PromptnessOfDischarge;
        }
        set
        {
            PromptnessOfDischarge = value;
        }
    }

    public string _DischargeCounseling
    {
        get
        {
            return DischargeCounseling;
        }
        set
        {
            DischargeCounseling = value;
        }
    }

    public string _ConvenienceOfDischarge
    {
        get
        {
            return ConvenienceOfDischarge;
        }
        set
        {
            ConvenienceOfDischarge = value;
        }
    }

    public string _HowWouldYouRate
    {
        get
        {
            return HowWouldYouRate;
        }
        set
        {
            HowWouldYouRate = value;
        }
    }

    public string _WhatDidYouLike
    {
        get
        {
            return WhatDidYouLike;
        }
        set
        {
            WhatDidYouLike = value;
        }
    }

    public string _WhatDidYouLikeLeast
    {
        get
        {
            return WhatDidYouLikeLeast;
        }
        set
        {
            WhatDidYouLikeLeast = value;
        }
    }

    public string _AnysuggestionYouWouldLike
    {
        get
        {
            return AnysuggestionYouWouldLike;
        }
        set
        {
            AnysuggestionYouWouldLike = value;
        }
    }

    public string _WouldYouLikeToRecommend
    {
        get
        {
            return WouldYouLikeToRecommend;
        }
        set
        {
            WouldYouLikeToRecommend = value;
        }
    }

    public string _Pathology_BloodCollection
    {
        get
        {
            return Pathology_BloodCollection;
        }
        set
        {
            Pathology_BloodCollection = value;
        }
    }

    public string _Radiology_X_ray
    {
        get
        {
            return Radiology_X_ray;
        }
        set
        {
            Radiology_X_ray = value;
        }
    }

    public string _Cardiology_ECG_Stress_test
    {
        get
        {
            return Cardiology_ECG_Stress_test;
        }
        set
        {
            Cardiology_ECG_Stress_test = value;
        }
    }

    public string _Nuclear_Dept_BoneScanLiverScan
    {
        get
        {
            return Nuclear_Dept_BoneScanLiverScan;
        }
        set
        {
            Nuclear_Dept_BoneScanLiverScan = value;
        }
    }

    public string _TimetakenforTestRadiology_X_ray
    {
        get
        {
            return TimetakenforTestRadiology_X_ray;
        }
        set
        {
            TimetakenforTestRadiology_X_ray = value;
        }
    }

    public string _TimetakenforTestCardiology_ECG_Stress_test
    {
        get
        {
            return TimetakenforTestCardiology_ECG_Stress_test;
        }
        set
        {
            TimetakenforTestCardiology_ECG_Stress_test = value;
        }
    }

    public string _TimetakenforTestNuclear_Dept_BoneScanLiverScan
    {
        get
        {
            return TimetakenforTestNuclear_Dept_BoneScanLiverScan;
        }
        set
        {
            TimetakenforTestNuclear_Dept_BoneScanLiverScan = value;
        }
    }

    public string _TimetakenforTestPathology
    {
        get
        {
            return TimetakenforTestPathology;
        }
        set
        {
            TimetakenforTestPathology = value;
        }
    }
    
}

#endregion

#region Feedback OutPatient

public class FeedbackOutPatient
{
    public int Feed_Opd_Id;
    public string FirstName;
    public string LastName;
    public string Lh_No;
    public string Dates;
    public string NameOfDoctor;
    public string PhoneNo;
    public string EmailId;
    public string HowDidYouComeToKnow;
    public string TimeTakenByAppointment;
    public string GuidanceAndInformation;
    public string CourtesyAndFriendlinessOPD;
    public string TimeTakenToCompleteRegistration;
    public string TimeTakenToCompleteBilling;
    public string CourtesyAndInformation;
    public string WaitingTimeToSeeDoctor;
    public string ClarityAndInformation;
    public string CareAndAttention;
    public string BehaviorAndAttitude;
    public string RespectForYourPrivacy;
    public string CareOfferedDuringProc;
    public string WaitingTimeForTheTest;
    public string ExperienceWithTechnical;
    public string CourtesyAndFriendlinessStaff;
    public string ComfortOfWaitingArea;
    public string CleanlinessInOPD;
    public string CleanlinessOfToilets;
    public string HowWouldYouRate;
    public string WhatDidYouLIke;
    public string WhatDidYouLikeLeast;
    public string AnysuggestionYouWouldLike;
    public string Date;

    public string CourtesyAndFriendlinessStaffPathology;
    public string CourtesyAndFriendlinessStaffRadiology;
    public string CourtesyAndFriendlinessStaffCardiology;
    public string CourtesyAndFriendlinessStaffNuclear;
    public string CourtesyAndFriendlinessStaffOther;

    public string Courtesyother;

    public string WaitingTimeForTheTestPathology;
    public string WaitingTimeForTheTestRadiology;
    public string WaitingTimeForTheTestCardiology;
    public string WaitingTimeForTheTestNuclear;
    public string WaitingTimeForTheTestOther;

    public string WaitingTimeother;

    public string Service;
    public string ServiceReason;
    public string Staff;
    public string AreaofWork;
    public string AreaReason;


    public int _Feed_Opd_Id
    {
        get
        {
            return Feed_Opd_Id;
        }
        set
        {
            Feed_Opd_Id = value;
        }
    }



    public string _FirstName
    {
        get
        {
            return FirstName;
        }
        set
        {
            FirstName = value;
        }
    }

    public string _LastName
    {
        get
        {
            return LastName;
        }
        set
        {
            LastName = value;
        }
    }

    public string _Lh_No
    {
        get
        {
            return Lh_No;
        }
        set
        {
            Lh_No = value;
        }
    }

    public string _Dates
    {
        get
        {
            return Dates;
        }
        set
        {
            Dates = value;
        }
    }

    public string _NameOfDoctor
    {
        get
        {
            return NameOfDoctor;
        }
        set
        {
            NameOfDoctor = value;
        }
    }

    public string _PhoneNo
    {
        get
        {
            return PhoneNo;
        }
        set
        {
            PhoneNo = value;
        }
    }

    public string _EmailId
    {
        get
        {
            return EmailId;
        }
        set
        {
            EmailId = value;
        }
    }

    public string _HowDidYouComeToKnow
    {
        get
        {
            return HowDidYouComeToKnow;
        }
        set
        {
            HowDidYouComeToKnow = value;
        }
    }

    public string _TimeTakenByAppointment
    {
        get
        {
            return TimeTakenByAppointment;
        }
        set
        {
            TimeTakenByAppointment = value;
        }
    }

    public string _GuidanceAndInformation
    {
        get
        {
            return GuidanceAndInformation;
        }
        set
        {
            GuidanceAndInformation = value;
        }
    }

    public string _CourtesyAndFriendlinessOPD
    {
        get
        {
            return CourtesyAndFriendlinessOPD;
        }
        set
        {
            CourtesyAndFriendlinessOPD = value;
        }
    }

    public string _TimeTakenToCompleteRegistration
    {
        get
        {
            return TimeTakenToCompleteRegistration;
        }
        set
        {
            TimeTakenToCompleteRegistration = value;
        }
    }

    public string _TimeTakenToCompleteBilling
    {
        get
        {
            return TimeTakenToCompleteBilling;
        }
        set
        {
            TimeTakenToCompleteBilling = value;
        }
    }

    public string _CourtesyAndInformation
    {
        get
        {
            return CourtesyAndInformation;
        }
        set
        {
            CourtesyAndInformation = value;
        }
    }

    public string _WaitingTimeToSeeDoctor
    {
        get
        {
            return WaitingTimeToSeeDoctor;
        }
        set
        {
            WaitingTimeToSeeDoctor = value;
        }
    }

    public string _ClarityAndInformation
    {
        get
        {
            return ClarityAndInformation;
        }
        set
        {
            ClarityAndInformation = value;
        }
    }

    public string _CareAndAttention
    {
        get
        {
            return CareAndAttention;
        }
        set
        {
            CareAndAttention = value;
        }
    }

    public string _BehaviorAndAttitude
    {
        get
        {
            return BehaviorAndAttitude;
        }
        set
        {
            BehaviorAndAttitude = value;
        }
    }

    public string _RespectForYourPrivacy
    {
        get
        {
            return RespectForYourPrivacy;
        }
        set
        {
            RespectForYourPrivacy = value;
        }
    }

    public string _CareOfferedDuringProc
    {
        get
        {
            return CareOfferedDuringProc;
        }
        set
        {
            CareOfferedDuringProc = value;
        }
    }

    public string _WaitingTimeForTheTest
    {
        get
        {
            return WaitingTimeForTheTest;
        }
        set
        {
            WaitingTimeForTheTest = value;
        }
    }

    public string _ExperienceWithTechnical
    {
        get
        {
            return ExperienceWithTechnical;
        }
        set
        {
            ExperienceWithTechnical = value;
        }
    }

    public string _CourtesyAndFriendlinessStaff
    {
        get
        {
            return CourtesyAndFriendlinessStaff;
        }
        set
        {
            CourtesyAndFriendlinessStaff = value;
        }
    }

    public string _ComfortOfWaitingArea
    {
        get
        {
            return ComfortOfWaitingArea;
        }
        set
        {
            ComfortOfWaitingArea = value;
        }
    }

    public string _CleanlinessInOPD
    {
        get
        {
            return CleanlinessInOPD;
        }
        set
        {
            CleanlinessInOPD = value;
        }
    }

    public string _CleanlinessOfToilets
    {
        get
        {
            return CleanlinessOfToilets;
        }
        set
        {
            CleanlinessOfToilets = value;
        }
    }

    public string _HowWouldYouRate
    {
        get
        {
            return HowWouldYouRate;
        }
        set
        {
            HowWouldYouRate = value;
        }
    }

    public string _WhatDidYouLIke
    {
        get
        {
            return WhatDidYouLIke;
        }
        set
        {
            WhatDidYouLIke = value;
        }
    }

    public string _WhatDidYouLikeLeast
    {
        get
        {
            return WhatDidYouLikeLeast;
        }
        set
        {
            WhatDidYouLikeLeast = value;
        }
    }

    public string _AnysuggestionYouWouldLike
    {
        get
        {
            return AnysuggestionYouWouldLike;
        }
        set
        {
            AnysuggestionYouWouldLike = value;
        }
    }

    public string _Date
    {
        get
        {
            return Date;
        }
        set
        {
            Date = value;
        }
    }


    public string _CourtesyAndFriendlinessStaffPathology
    {
        get
        {
            return CourtesyAndFriendlinessStaffPathology;
        }
        set
        {
            CourtesyAndFriendlinessStaffPathology = value;
        }
    }

    public string _CourtesyAndFriendlinessStaffRadiology
    {
        get
        {
            return CourtesyAndFriendlinessStaffRadiology;
        }
        set
        {
            CourtesyAndFriendlinessStaffRadiology = value;
        }
    }

    public string _CourtesyAndFriendlinessStaffCardiology
    {
        get
        {
            return CourtesyAndFriendlinessStaffCardiology;
        }
        set
        {
            CourtesyAndFriendlinessStaffCardiology = value;
        }
    }

    public string _CourtesyAndFriendlinessStaffNuclear
    {
        get
        {
            return CourtesyAndFriendlinessStaffNuclear;
        }
        set
        {
            CourtesyAndFriendlinessStaffNuclear = value;
        }
    }

    public string _CourtesyAndFriendlinessStaffOther
    {
        get
        {
            return CourtesyAndFriendlinessStaffOther;
        }
        set
        {
            CourtesyAndFriendlinessStaffOther = value;
        }
    }



    public string _Courtesyother
    {
        get
        {
            return Courtesyother;
        }
        set
        {
            Courtesyother = value;
        }
    }


    public string _WaitingTimeForTheTestPathology
    {
        get
        {
            return WaitingTimeForTheTestPathology;
        }
        set
        {
            WaitingTimeForTheTestPathology = value;
        }
    }

    public string _WaitingTimeForTheTestRadiology
    {
        get
        {
            return WaitingTimeForTheTestRadiology;
        }
        set
        {
            WaitingTimeForTheTestRadiology = value;
        }
    }

    public string _WaitingTimeForTheTestCardiology
    {
        get
        {
            return WaitingTimeForTheTestCardiology;
        }
        set
        {
            WaitingTimeForTheTestCardiology = value;
        }
    }

    public string _WaitingTimeForTheTestNuclear
    {
        get
        {
            return WaitingTimeForTheTestNuclear;
        }
        set
        {
            WaitingTimeForTheTestNuclear = value;
        }
    }

    public string _WaitingTimeForTheTestOther
    {
        get
        {
            return WaitingTimeForTheTestOther;
        }
        set
        {
            WaitingTimeForTheTestOther = value;
        }
    }

    public string _WaitingTimeother
    {
        get
        {
            return WaitingTimeother;
        }
        set
        {
            WaitingTimeother = value;
        }
    }

    public string _Service
    {
        get
        {
            return Service;
        }
        set
        {
            Service = value;
        }
    }

    public string _ServiceReason
    {
        get
        {
            return ServiceReason;
        }
        set
        {
            ServiceReason = value;
        }
    }

    public string _Staff
    {
        get
        {
            return Staff;
        }
        set
        {
            Staff = value;
        }
    }
    public string _AreaofWork
    {
        get
        {
            return AreaofWork;
        }
        set
        {
            AreaofWork = value;
        }
    }

    public string _AreaReason
    {
        get
        {
            return AreaReason;
        }
        set
        {
            AreaReason = value;
        }
    }
}

#endregion

#region Photo Gallery

public class PhotoGallery
{
    public int ID;
    public string Images;
    public string Tittle;

    public int _ID
    {
        get
        {
            return ID;
        }
        set
        {
            ID = value;
        }
    }

    public string _Images
    {
        get
        {
            return Images;
        }
        set
        {
            Images = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }
}

#endregion

#region Home Banner

public class HomeBanner
{
    public int ID;
    public string Images;
    public string Order;
    public string PageUrl;
    public string Publish;

    public int _ID
    {
        get
        {
            return ID;
        }
        set
        {
            ID = value;
        }
    }

    public string _Images
    {
        get
        {
            return Images;
        }
        set
        {
            Images = value;
        }
    }

    public string _Order
    {
        get
        {
            return Order;
        }
        set
        {
            Order = value;
        }
    }

    public string _PageUrl
    {
        get
        {
            return PageUrl;
        }
        set
        {
            PageUrl = value;
        }
    }

    public string _Publish
    {
        get
        {
            return Publish;
        }
        set
        {
            Publish = value;
        }
    }
}

#endregion

#region Updates Page

public class Updates
{
    public int Id;
    public string Tittle;
    public string Images;
    public string Content;
    public string Pdf;
    public string Doc;

    public int _Id
    {
        get
        {
            return Id;
        }
        set
        {
            Id = value;
        }
    }

    public string _Tittle
    {
        get
        {
            return Tittle;
        }
        set
        {
            Tittle = value;
        }
    }

    public string _Images
    {
        get
        {
            return Images;
        }
        set
        {
            Images = value;
        }
    }

    public string _Content
    {
        get
        {
            return Content;
        }
        set
        {
            Content = value;
        }
    }

    public string _Pdf
    {
        get
        {
            return Pdf;
        }
        set
        {
            Pdf = value;
        }
    }

    public string _Doc
    {
        get
        {
            return Doc;
        }
        set
        {
            Doc = value;
        }
    }
}

#endregion

#region LHMT Subscribe

public class LHMTSubscribe
{
    public int Id;
    public string Name;
    public string Email;
    public string Contact;

    public int _Id
    {
        get
        {
            return Id;
        }
        set
        {
            Id = value;
        }
    }

    public string _Name
    {
        get
        {
            return Name;
        }
        set
        {
            Name = value;
        }
    }

    public string _Email
    {
        get
        {
            return Email;
        }
        set
        {
            Email = value;
        }
    }

    public string _Contact
    {
        get
        {
            return Contact;
        }
        set
        {
            Contact = value;
        }
    }
}

#endregion

#region Feedback HealthCheckUp

public class FeedBackHealthCheckUp
{
    public int ID;
    public string FirstName;
    public string LastName;
    public string Lh_No;
    public string Date;
    public string Contact;  
    public string EmailId;
    public string HealthCheckup;

    public string HowDidYouComeToKnow;

    public string AppointmentHowwouldyourate;
    public string RegistrationHowwasyourdocument;
    public string TestsPathologyBlood;
    public string TestsRadiology_Xray;
    public string TestsCardiologyECG;
    public string TestsSpirometry;

    public string ConsultationGeneralPhysician;
    public string ConsultationGynaec;
    public string ConsultationOphthalmology;
    public string ConsultationENT;
    public string ConsultationDental;
    public string ConsultationSkin;
    public string ConsultationDietition;

    public string HousekeepingOverallhygiene;
    public string Staff;
    public string FoodBeverages;
    public string Respectforyour;
    public string Timetakenforhealthcheck;
    public string Howwouldyourate;

    public string WhatdidYouLike;
    public string WhatDidYouLikeLeast;
    public string AnySuggestionYou;
    public string WouldYouLikeTOMention;

    public string Service;
    public string ServiceReason;
    public string StaffPerformance;
    public string AreaofWork;
    public string AreaReason;

    public int _ID
    {
        get
        {
            return ID;
        }
        set
        {
            ID = value;
        }
    }

    public string _FirstName
    {
        get
        {
            return FirstName;
        }
        set
        {
            FirstName = value;
        }
    }

    public string _LastName
    {
        get
        {
            return LastName;
        }
        set
        {
            LastName = value;
        }
    }

    public string _Lh_No
    {
        get
        {
            return Lh_No;
        }
        set
        {
            Lh_No = value;
        }
    }

    public string _Date
    {
        get
        {
            return Date;
        }
        set
        {
            Date = value;
        }
    }

    public string _Contact
    {
        get
        {
            return Contact;
        }
        set
        {
            Contact = value;
        }
    }       

    public string _EmailId
    {
        get
        {
            return EmailId;
        }
        set
        {
            EmailId = value;
        }
    }

    public string _HealthCheckup
    {
        get
        {
            return HealthCheckup;
        }
        set
        {
            HealthCheckup = value;
        }
    }

    public string _HowDidYouComeToKnow
    {
        get
        {
            return HowDidYouComeToKnow;
        }
        set
        {
            HowDidYouComeToKnow = value;
        }
    }

    public string _AppointmentHowwouldyourate
    {
        get
        {
            return AppointmentHowwouldyourate;
        }
        set
        {
            AppointmentHowwouldyourate = value;
        }
    }

    public string _RegistrationHowwasyourdocument
    {
        get
        {
            return RegistrationHowwasyourdocument;
        }
        set
        {
            RegistrationHowwasyourdocument = value;
        }
    }

    public string _TestsPathologyBlood
    {
        get
        {
            return TestsPathologyBlood;
        }
        set
        {
            TestsPathologyBlood = value;
        }
    }

    public string _TestsRadiology_Xray
    {
        get
        {
            return TestsRadiology_Xray;
        }
        set
        {
            TestsRadiology_Xray = value;
        }
    }

    public string _TestsCardiologyECG
    {
        get
        {
            return TestsCardiologyECG;
        }
        set
        {
            TestsCardiologyECG = value;
        }
    }

    public string _TestsSpirometry
    {
        get
        {
            return TestsSpirometry;
        }
        set
        {
            TestsSpirometry = value;
        }
    }


    public string _ConsultationGeneralPhysician
    {
        get
        {
            return ConsultationGeneralPhysician;
        }
        set
        {
            ConsultationGeneralPhysician = value;
        }
    }

    public string _ConsultationGynaec
    {
        get
        {
            return ConsultationGynaec;
        }
        set
        {
            ConsultationGynaec = value;
        }
    }

    public string _ConsultationOphthalmology
    {
        get
        {
            return ConsultationOphthalmology;
        }
        set
        {
            ConsultationOphthalmology = value;
        }
    }

    public string _ConsultationENT
    {
        get
        {
            return ConsultationENT;
        }
        set
        {
            ConsultationENT = value;
        }
    }

    public string _ConsultationDental
    {
        get
        {
            return ConsultationDental;
        }
        set
        {
            ConsultationDental = value;
        }
    }

    public string _ConsultationSkin
    {
        get
        {
            return ConsultationSkin;
        }
        set
        {
            ConsultationSkin = value;
        }
    }

    public string _ConsultationDietition
    {
        get
        {
            return ConsultationDietition;
        }
        set
        {
            ConsultationDietition = value;
        }
    }


    public string _HousekeepingOverallhygiene
    {
        get
        {
            return HousekeepingOverallhygiene;
        }
        set
        {
            HousekeepingOverallhygiene = value;
        }
    }

    public string _Staff
    {
        get
        {
            return Staff;
        }
        set
        {
            Staff = value;
        }
    }

    public string _FoodBeverages
    {
        get
        {
            return FoodBeverages;
        }
        set
        {
            FoodBeverages = value;
        }
    }

    public string _Respectforyour
    {
        get
        {
            return Respectforyour;
        }
        set
        {
            Respectforyour = value;
        }
    }

    public string _Timetakenforhealthcheck
    {
        get
        {
            return Timetakenforhealthcheck;
        }
        set
        {
            Timetakenforhealthcheck = value;
        }
    }

    public string _Howwouldyourate
    {
        get
        {
            return Howwouldyourate;
        }
        set
        {
            Howwouldyourate = value;
        }
    }


    public string _WhatdidYouLike
    {
        get
        {
            return WhatdidYouLike;
        }
        set
        {
            WhatdidYouLike = value;
        }
    }

    public string _WhatDidYouLikeLeast
    {
        get
        {
            return WhatDidYouLikeLeast;
        }
        set
        {
            WhatDidYouLikeLeast = value;
        }
    }

    public string _AnySuggestionYou
    {
        get
        {
            return AnySuggestionYou;
        }
        set
        {
            AnySuggestionYou = value;
        }
    }

    public string _WouldYouLikeTOMention
    {
        get
        {
            return WouldYouLikeTOMention;
        }
        set
        {
            WouldYouLikeTOMention = value;
        }
    }

    public string _Service
    {
        get
        {
            return Service;
        }
        set
        {
            Service = value;
        }
    }

    public string _ServiceReason
    {
        get
        {
            return ServiceReason;
        }
        set
        {
            ServiceReason = value;
        }
    }

    public string _StaffPerformance
    {
        get
        {
            return StaffPerformance;
        }
        set
        {
            StaffPerformance = value;
        }
    }
    public string _AreaofWork
    {
        get
        {
            return AreaofWork;
        }
        set
        {
            AreaofWork = value;
        }
    }

    public string _AreaReason
    {
        get
        {
            return AreaReason;
        }
        set
        {
            AreaReason = value;
        }
    }

}

#endregion

#region Visa Invitation

public class VisaInvitation
{
    public int Visa_Id;
    public string PatientName;
    public string EmailID;
    public string ContactNo;
    public string Nationality;
    public string PassportNo;
    public string PatientPasswordCopy;
    public string HospitalName;
    public string TreatmentDocName;
    public string Treatment;
    public string ProvisionalDiagnosis;
    public string PurposeOfVisit;
    public string LilavatiDoctorName;
    public string PreferredDate;
    public string DurationOfTreatment;
    public string ComingForFollowup;
    public string DateOfLastVisit;
    public string NoOfAttendant;
    public string PleaseSpecifyTheReason;
    public string FirstAttName;
    public string FirstRelationship;
    public string FirstPassportNo;
    public string FirstFileUploadCopy;
    public string SecondAttName;
    public string SecondRelationship;
    public string SecondPassportNo;
    public string SecondFileUploadCopy;
    public string ThirdAttName;
    public string ThirdRelationship;
    public string ThirdPassportNo;
    public string ThirdFileUploadCopy;
    public string FourthAttName;
    public string FourthRelationship;
    public string FourthPassportNo;
    public string FourthFileUploadCopy;

    public int _Visa_Id
    {
        get
        {
            return Visa_Id;
        }
        set
        {
            Visa_Id = value;
        }
    }

    public string _PatientName
    {
        get
        {
            return PatientName;
        }
        set
        {
            PatientName = value;
        }
    }

    public string _EmailID
    {
        get
        {
            return EmailID;
        }
        set
        {
            EmailID = value;
        }
    }

    public string _ContactNo
    {
        get
        {
            return ContactNo;
        }
        set
        {
            ContactNo = value;
        }
    }

    public string _Nationality
    {
        get
        {
            return Nationality;
        }
        set
        {
            Nationality = value;
        }
    }

    public string _PassportNo
    {
        get
        {
            return PassportNo;
        }
        set
        {
            PassportNo = value;
        }
    }

    public string _PatientPasswordCopy
    {
        get
        {
            return PatientPasswordCopy;
        }
        set
        {
            PatientPasswordCopy = value;
        }
    }

    public string _HospitalName
    {
        get
        {
            return HospitalName;
        }
        set
        {
            HospitalName = value;
        }
    }

    public string _TreatmentDocName
    {
        get
        {
            return TreatmentDocName;
        }
        set
        {
            TreatmentDocName = value;
        }
    }

    public string _Treatment
    {
        get
        {
            return Treatment;
        }
        set
        {
            Treatment = value;
        }
    }

    public string _ProvisionalDiagnosis
    {
        get
        {
            return ProvisionalDiagnosis;
        }
        set
        {
            ProvisionalDiagnosis = value;
        }
    }

    public string _PurposeOfVisit
    {
        get
        {
            return PurposeOfVisit;
        }
        set
        {
            PurposeOfVisit = value;
        }
    }

    public string _LilavatiDoctorName
    {
        get
        {
            return LilavatiDoctorName;
        }
        set
        {
            LilavatiDoctorName = value;
        }
    }

    public string _PreferredDate
    {
        get
        {
            return PreferredDate;
        }
        set
        {
            PreferredDate = value;
        }
    }

    public string _DurationOfTreatment
    {
        get
        {
            return DurationOfTreatment;
        }
        set
        {
            DurationOfTreatment = value;
        }
    }

    public string _ComingForFollowup
    {
        get
        {
            return ComingForFollowup;
        }
        set
        {
            ComingForFollowup = value;
        }
    }

    public string _DateOfLastVisit
    {
        get
        {
            return DateOfLastVisit;
        }
        set
        {
            DateOfLastVisit = value;
        }
    }

    public string _NoOfAttendant
    {
        get
        {
            return NoOfAttendant;
        }
        set
        {
            NoOfAttendant = value;
        }
    }

    public string _PleaseSpecifyTheReason
    {
        get
        {
            return PleaseSpecifyTheReason;
        }
        set
        {
            PleaseSpecifyTheReason = value;
        }
    }

    public string _FirstAttName
    {
        get
        {
            return FirstAttName;
        }
        set
        {
            FirstAttName = value;
        }
    }

    public string _FirstRelationship
    {
        get
        {
            return FirstRelationship;
        }
        set
        {
            FirstRelationship = value;
        }
    }

    public string _FirstPassportNo
    {
        get
        {
            return FirstPassportNo;
        }
        set
        {
            FirstPassportNo = value;
        }
    }

    public string _FirstFileUploadCopy
    {
        get
        {
            return FirstFileUploadCopy;
        }
        set
        {
            FirstFileUploadCopy = value;
        }
    }

    public string _SecondAttName
    {
        get
        {
            return SecondAttName;
        }
        set
        {
            SecondAttName = value;
        }
    }

    public string _SecondRelationship
    {
        get
        {
            return SecondRelationship;
        }
        set
        {
            SecondRelationship = value;
        }
    }

    public string _SecondPassportNo
    {
        get
        {
            return SecondPassportNo;
        }
        set
        {
            SecondPassportNo = value;
        }
    }

    public string _SecondFileUploadCopy
    {
        get
        {
            return SecondFileUploadCopy;
        }
        set
        {
            SecondFileUploadCopy = value;
        }
    }

    public string _ThirdAttName
    {
        get
        {
            return ThirdAttName;
        }
        set
        {
            ThirdAttName = value;
        }
    }

    public string _ThirdRelationship
    {
        get
        {
            return ThirdRelationship;
        }
        set
        {
            ThirdRelationship = value;
        }
    }

    public string _ThirdPassportNo
    {
        get
        {
            return ThirdPassportNo;
        }
        set
        {
            ThirdPassportNo = value;
        }
    }

    public string _ThirdFileUploadCopy
    {
        get
        {
            return ThirdFileUploadCopy;
        }
        set
        {
            ThirdFileUploadCopy = value;
        }
    }

    public string _FourthAttName
    {
        get
        {
            return FourthAttName;
        }
        set
        {
            FourthAttName = value;
        }
    }

    public string _FourthRelationship
    {
        get
        {
            return FourthRelationship;
        }
        set
        {
            FourthRelationship = value;
        }
    }

    public string _FourthPassportNo
    {
        get
        {
            return FourthPassportNo;
        }
        set
        {
            FourthPassportNo = value;
        }
    }

    public string _FourthFileUploadCopy
    {
        get
        {
            return FourthFileUploadCopy;
        }
        set
        {
            FourthFileUploadCopy = value;
        }
    }
}

#endregion

#region CME Section

public class CME
{
    public int Cme_Id;
    public string EventName;

    public int _Cme_Id
    {
        get
        {
            return Cme_Id;
        }
        set
        {
            Cme_Id = value;
        }
    }

    public string _EventName
    {
        get
        {
            return EventName;
        }
        set
        {
            EventName = value;
        }
    }

}

#endregion

#region CME Details

public class CMEDetails
{
    public int Id;
    public int Cme_Id;
    public string DateAndMonth;
    public string Topic;
    public string Department;

    public int _Id
    {
        get
        {
            return Id;
        }
        set
        {
            Id = value;
        }
    }

    public int _Cme_Id
    {
        get
        {
            return Cme_Id;
        }
        set
        {
            Cme_Id = value;
        }
    }

    public string _DateAndMonth
    {
        get
        {
            return DateAndMonth;
        }
        set
        {
            DateAndMonth = value;
        }
    }

    public string _Topic
    {
        get
        {
            return Topic;
        }
        set
        {
            Topic = value;
        }
    }

    public string _Department
    {
        get
        {
            return Department;
        }
        set
        {
            Department = value;
        }
    }
}

#endregion

#region In Patient Payment Online Form

public class InPatientPayment
{
    public int Patient_Id;
    public string LHNo;
    public string IPNo;
    public string PatientsName;
    public string Name;
    public string ContactNo;
    public string EmailAddress;
    public string Address;
    public string PANNo;
    public string Amount;

    public int _Patient_Id
    {
        get
        {
            return Patient_Id;
        }
        set
        {
            Patient_Id = value;
        }
    }

    public string _LHNo
    {
        get
        {
            return LHNo;
        }
        set
        {
            LHNo = value;
        }
    }

    public string _IPNo
    {
        get
        {
            return IPNo;
        }
        set
        {
            IPNo = value;
        }
    }

    public string _PatientsName
    {
        get
        {
            return PatientsName;
        }
        set
        {
            PatientsName = value;
        }
    }

    public string _Name
    {
        get
        {
            return Name;
        }
        set
        {
            Name = value;
        }
    }

    public string _ContactNo
    {
        get
        {
            return ContactNo;
        }
        set
        {
            ContactNo = value;
        }
    }

    public string _EmailAddress
    {
        get
        {
            return EmailAddress;
        }
        set
        {
            EmailAddress = value;
        }
    }

    public string _Address
    {
        get
        {
            return Address;
        }
        set
        {
            Address = value;
        }
    }

    public string _PANNo
    {
        get
        {
            return PANNo;
        }
        set
        {
            PANNo = value;
        }
    }

    public string _Amount
    {
        get
        {
            return Amount;
        }
        set
        {
            Amount = value;
        }
    }
}

#endregion