﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for HealthCheckUpAppMail
/// </summary>
public class HealthCheckUpAppMail
{
    //string strTo = "arvind@kwebmaker.com";
    string strTo = "hcuonline@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Health Check-Up Appointment Request From Lilavati Website";

    //string strTo = "arvind@kwebmaker.com";
    ////string strTo = "appointments@lilavatihospital.com";
    //string strFrom = "noreply@kwebmaker.com";
    //string strMailUserName = "noreply@kwebmaker.com";
    //string strMailPassword = "noreply123";
    //string Subject = "Health Check-Up Appointment Request From Lilavati Website";

    string strFrom1 = "marketing@lilavatihospital.com";
    string strMailUserName1 = "marketing";
    string strMailPassword1 = "lilavati12";
    string Subject1 = "Health Check-Up Appointment Details From Lilavati Website";
	public HealthCheckUpAppMail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();            
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailHealthApp(string Name, string PreferredPackage, string DateofBirth, string PreferredDateofAppointment, string Email, string ContactNo)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\">Health Check-Up Appointment Request From Lilavati Website </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + Name + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Preferred Package : </strong></td> <td> " + PreferredPackage + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Date of Birth : </strong></td> <td> " + DateofBirth + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Preferred Date of Appointment : </strong></td> <td> " + PreferredDateofAppointment + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + Email + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Contact No. : </strong></td> <td> " + ContactNo + " </td> </tr>");
           
            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString());


            /*send auto mail who fill the form*/

            string EmailId = Email;

            SmtpClient mailClient1 = null;
            MailMessage message1 = null;
            StringBuilder mailbody1 = new StringBuilder();
            mailClient1 = new SmtpClient();
            message1 = new MailMessage();
            mailClient1.Host = "webmail.lilavatihospital.com";
            mailClient1.Port = 25;

            System.Net.NetworkCredential SMTPUserInfo1 = new System.Net.NetworkCredential(strMailUserName1, strMailPassword1);
            mailClient1.UseDefaultCredentials = false;
            mailClient1.Credentials = SMTPUserInfo1;
            mailClient1.EnableSsl = false;
            mailClient1.DeliveryMethod = SmtpDeliveryMethod.Network;

            MailAddress fromAddress1 = new MailAddress(strFrom1, "Lilavati Hospital");
            message1.From = fromAddress1;

            mailbody1.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            mailbody1.AppendFormat("<tr><td colspan='2' align='center'><img src='http://www.lilavatihospital.com/images/emailer_logo.jpg' /><br /><hr color='#04869a' /></td></tr>");
            mailbody1.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\">Health Check-Up Appointment Details From Lilavati Website </font></strong></td></tr>");
            mailbody1.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + Name + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Preferred Package : </strong></td> <td> " + PreferredPackage + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Date of Birth : </strong></td> <td> " + DateofBirth + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Preferred Date of Appointment : </strong></td> <td> " + PreferredDateofAppointment + " </td> </tr>");
            mailbody1.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + Email + " </td></tr>");
            mailbody1.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Contact No. : </strong></td> <td> " + ContactNo + " </td> </tr>");
           

            mailbody1.Append("</table>");

            message1.To.Add(EmailId);
            message1.Subject = Subject1;

            message1.Body = mailbody1.ToString();
            message1.IsBodyHtml = true;
            mailClient1.Send(message1);
            message1 = null;
            mailClient1 = null;



            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}