﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for RequestAnEstimateMail
/// </summary>
public class RequestAnEstimateMail
{
    string strTo = "solanki@lilavatihospital.com,marketing@lilavatihospital.com";
    //string strTo = "appointments@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Request An Estimate Request From Lilavati Website";


    public RequestAnEstimateMail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();           
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;
            
            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailRequestEstimate(string PatientsName, string Speciality, string PreferredDoctor, string TypeofRoom, string Email, string MobileNumber, string Age, string Gender, string Nationality, string Description, string PastHistory)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> Request An Estimate Details </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Patient's Name : </strong></td><td> " + PatientsName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Speciality : </strong></td> <td> " + Speciality + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Preferred Doctor : </strong></td> <td> " + PreferredDoctor + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Type of Room : </strong></td> <td> " + TypeofRoom + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Email ID : </strong></td> <td> " + Email + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Mobile Number : </strong></td> <td> " + MobileNumber + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Age : </strong></td> <td> " + Age + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Gender : </strong></td> <td> " + Gender + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Nationality : </strong></td> <td> " + Nationality + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Description of Procedure : </strong></td> <td> " + Description + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Past History (if any) : </strong></td> <td> " + PastHistory + " </td></tr>");
                              

            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString());
                       

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}