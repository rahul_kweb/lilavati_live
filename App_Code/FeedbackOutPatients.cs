﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for FeedbackInPatients
/// </summary>
public class FeedbackOutPatients
{
    //string strTo = "rahul@kwebmaker.com";

    //string strTo = "operations@lilavatihospital.com";

    string strTo = "feedback@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Feedback From Lilavati Website from OPD";

	public FeedbackOutPatients()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;


            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            //mailClient.EnableSsl = true;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailFeedbackOutPatient(string FirstName, string LastName, string LHNo, string NameOfDoctor, string ContactNo, string EmailId, string HowDidYouComeToKnow, string TimeTakenByAppointment, string GuidanceAndInformation, string CourtesyAndFriendlinessOPD, string TimeTakenToCompleteRegistration, string TimeTakenToCompleteBilling, string CourtesyAndInformation, string WaitingTimeToSeeDoctor, string ClarityAndInformation, string CareAndAttention, string BehaviorAndAttitude, string RespectForYourPrivacy, string CareOfferedDuringProc, string WaitingTimeForTheTest, string ExperienceWithTechnical, string CourtesyAndFriendlinessStaff, string ComfortOfWaitingArea, string CleanlinessInOPD, string CleanlinessOfToilets, string HowWouldYouRate, string WhatDidYouLIke, string WhatDidYouLikeLeast, string AnysuggestionYouWouldLike,string Date,string CourtesyAndFriendlinessStaffPathology,string CourtesyAndFriendlinessStaffRadiology,string CourtesyAndFriendlinessStaffCardiology,string CourtesyAndFriendlinessStaffNuclear,string CourtesyAndFriendlinessStaffOther,string Courtesyother,string WaitingTimeForTheTestPathology,string WaitingTimeForTheTestRadiology,string WaitingTimeForTheTestCardiology,string WaitingTimeForTheTestNuclear,string WaitingTimeForTheTestOther,string WaitingTimeother,string Service,string ServiceReason,string Staff,string AreaofWork,string AreaReason)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.AppendFormat("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> FEEDBACK OUT-PATIENT DETAILS </font></strong></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td width='200px'>First Name  </td><td> " + FirstName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Last Name  </td> <td> " + LastName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>LH No  </td> <td> " + LHNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Name Of Doctor  </td> <td> " + NameOfDoctor + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Contact No  </td> <td> " + ContactNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Email ID  </td> <td> " + EmailId + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Date  </td> <td> " + Date + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>How did you come to know about Lilavati Hospital?</strong> </td> <td> " + HowDidYouComeToKnow + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>How would you rate your overall experience at Lilavati Hospital </strong></td> <td> " + HowWouldYouRate + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong>Appointment</strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Time taken by appointment staff to give an appointment for Doctor/ service  </td> <td> " + TimeTakenByAppointment + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Guidance and information provided by the staff  </td> <td> " + GuidanceAndInformation + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Courtesy and friendliness of OPD staff  </td> <td> " + CourtesyAndFriendlinessOPD + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Registration & Billing </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Time taken to complete the registration process  </td> <td> " + TimeTakenToCompleteRegistration + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Time taken to complete the billing process  </td> <td> " + TimeTakenToCompleteBilling + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Courtesy and information provided by OPD staff  </td> <td> " + CourtesyAndInformation + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Doctor </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Waiting time to see the doctor after completing registration/ billing formalities.  </td> <td> " + WaitingTimeToSeeDoctor + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Clarity & information shared by the doctor about your health & line of treatment  </td> <td> " + ClarityAndInformation + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Care & attention given by the doctor  </td> <td> " + CareAndAttention + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Nursing Care </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Behavior & attitude of nurse  </td> <td> " + BehaviorAndAttitude + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Respect for your privacy  </td> <td> " + RespectForYourPrivacy + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Care offered during procedure/ Investigation by nurse  </td> <td> " + CareOfferedDuringProc + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Test & Investigation </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Courtesy & Friendliness of technical staff, Clarity of instruction </td> <td>  </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Pathology (Blood/ Sample Collection)  </td> <td>"+ CourtesyAndFriendlinessStaffPathology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Radiology (X-ray, Sonography/ Mammo, CT Scan, MRI etc.)  </td> <td>" + CourtesyAndFriendlinessStaffRadiology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Cardiology (ECG, Stress Test, 2D Echo, etc.)  </td> <td>" + CourtesyAndFriendlinessStaffCardiology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Nuclear Dept. (PET CT, SPECT CT, etc.)  </td> <td>" + CourtesyAndFriendlinessStaffNuclear + "  </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Other : " + Courtesyother + "   </td> <td>" + CourtesyAndFriendlinessStaffOther + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Waiting time for the test after completing registration/ Billing formalities </td> <td>  </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Pathology (Blood/ Sample Collection)  </td> <td>" + WaitingTimeForTheTestPathology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Radiology (X-ray, Sonography/ Mammo, CT Scan, MRI etc.)  </td> <td>" + WaitingTimeForTheTestRadiology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Cardiology (ECG, Stress Test, 2D Echo, etc.)  </td> <td>" + WaitingTimeForTheTestCardiology + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Nuclear Dept. (PET CT, SPECT CT, etc.)  </td> <td>" + WaitingTimeForTheTestNuclear + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>&nbsp;&nbsp;&nbsp; - Other : " + WaitingTimeother + "   </td> <td>" + WaitingTimeForTheTestOther + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Comfort of OPD premises </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Comfort of waiting area  </td> <td> " + ComfortOfWaitingArea + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Cleanliness in the OPD area  </td> <td> " + CleanlinessInOPD + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Cleanliness of toilets  </td> <td> " + CleanlinessOfToilets + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>What did you like the most about Lilavati hospital? </strong> </td> <td> " + WhatDidYouLIke + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>What did you like the least about Lilavati hospital?</strong>  </td> <td> " + WhatDidYouLikeLeast + " </td> </tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td><strong>Any suggestion you would like to put forward which will help us to serve you better</strong>  </td> <td> " + AnysuggestionYouWouldLike + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Would you like to mention of our services for it's outstanding quality and/ recommend any staff for their outstanding performance? </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Service </td> <td>" + Service + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Reason </td> <td>" + ServiceReason + "</td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Staff </td> <td>" + Staff + "</td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Area of work </td> <td>" + AreaofWork + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td>Reason </td> <td>" + AreaReason + " </td> </tr>");



            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString());

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }
}