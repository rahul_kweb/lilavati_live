﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for eTenderEmail
/// </summary>
public class eTenderEmail
{

    //string strTo = "ambedkar.arvind89@gmail.com";
    string strFrom = "etender@lilavatihospital.com";
    string strMailUserName = "etender";
    string strMailPassword = "Lhrc@12";
    string Subject = "e-Tender Form";

    //string strTo = "arvind@kwebmaker.com";
    ////string strTo = "appointments@lilavatihospital.com";
    //string strFrom = "noreply@kwebmaker.com";
    //string strMailUserName = "noreply@kwebmaker.com";
    //string strMailPassword = "noreply123";
    //string Subject = "Health Check-Up Appointment Request From Lilavati Website";

    public eTenderEmail()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int SendEmail(string Body, string strTo)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 587;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool UserDetails(string Name, string Designation, string CompanyName, string ContactNo, string EmailAddress, string Remarks, string TenderNo)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"50%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> User Details </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Tender Code : </strong></td><td> " + TenderNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Name : </strong></td> <td> " + Name + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Designation : </strong></td> <td> " + Designation + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Company Name : </strong></td> <td> " + CompanyName + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong>Contact No. : </strong></td> <td> " + ContactNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Email Address : </strong></td> <td> " + EmailAddress + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong>Remarks : </strong></td> <td> " + Remarks + " </td> </tr>");

            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString(), "etender@lilavatihospital.com");

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

    public bool TenderDetails(string tenderDep, string tenderCat, string tenderDesc, string name, string email, string strpath)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            //strEmailBuilder.AppendFormat("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
            //strEmailBuilder.AppendFormat("<tr><td colspan='2' align='center'><img src='http://lilavatihospital.com/images/emailer_logo.jpg' /><br /><hr color='#04869a' /></td></tr>");
            //strEmailBuilder.AppendFormat("<tr><td colspan='2'>Dear " + name + ",<br /> Thanks for contacting with us. <br /> Find your details below : </td> </tr>");
            //strEmailBuilder.AppendFormat("<tr bgcolor='#f4f4f4'><td width='90'>Tender Code:</td><td><strong> " + tenderNo + " </strong> </td> </tr>");
            //strEmailBuilder.AppendFormat("<tr bgcolor='#f4f4f4'><td>Tender Name:</td><td><strong> " + tenderName + " </strong> </td></tr>");
            //strEmailBuilder.AppendFormat("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
            //strEmailBuilder.AppendFormat("<tr><td colspan='2' align='center' height='1'></td></tr>");
            //strEmailBuilder.AppendFormat("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Lilavati Hospital & Research Centre</font></strong></td></tr>");
            //strEmailBuilder.AppendFormat("<tr><td colspan='2' align='center' height='1'></td></tr>");
            //strEmailBuilder.AppendFormat("</table>");


            strEmailBuilder.AppendFormat("<table border='0' cellspacing='0' cellpadding='10' width='700' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
            strEmailBuilder.AppendFormat("<tr><td align='center' colspan='2'><img src='http://lilavatihospital.com/images/emailer_logo.jpg' /><br /><hr color='#04869a' /></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'>Dear " + name + ",<br />Thank you for the interest shown in the Tender floated by our hospital.<br />The details of the tender are as follows</td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor='#f4f4f4'><td width='130'>Tender Department :</td><td><strong>" + tenderDep + " </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>Tender Category :</td><td><strong>" + tenderCat + " </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor='#f4f4f4'><td>Tender Description :</td><td><strong>" + tenderDesc + " </strong></td</tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><p>In order for our hospital to send you the Tender Document to your registered Email Id, you are requested to furnish the following duly self-attested scanned copies of the documents for our verificatio</p></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><ul><li>Vendor Registration Form (Annexed with this email).</li><li>GST Registration Certificate.</li><li>Reference Letter / Testimonials from your Key Client for the same category, if any.</li><li>Any Other related Certificate, License etc.</li></ul></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><p>On receipt and verification of the above documents we shall email you the Tender Document in pdf format. For any further questions in ref to E Tender, kindly send us your queries on our email id :- <a href='mailto:etender@lilavatihospital.com'> etender@lilavatihospital.com</a></p></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><p><strong>Note :</strong> If you have already registered yourself as a Vendor with our Hospital by submitting above documents, kindly email us the details of such submission on :- <a href='mailto:etender@lilavatihospital.com'> etender@lilavatihospital.com</a>.</p></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Lilavati Hospital &amp; Research Centre<br />Mumbai</font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td align='center' colspan='2' height='1'></td></tr>");
            strEmailBuilder.AppendFormat("</table>");

            //SendEmail(strEmailBuilder.ToString(), email);
            int sent = 0;


            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 587;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(email);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");
            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(strpath);
            message.Attachments.Add(attachment);


            message.Body = strEmailBuilder.ToString();
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }
}