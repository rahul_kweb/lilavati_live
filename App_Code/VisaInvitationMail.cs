﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for FeedbackHealthCheckup
/// </summary>
public class VisaInvitationMail
{
    string strTo = "arvind@kwebmaker.com";
    //string strTo = "operations@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Visa Invitation Request From Lilavati Website";

    public VisaInvitationMail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body, string PatientCopy, string ReportsCopy, string FirstAttendant, string SecondAttendant, string ThirdAttendant,string FourthAttendant)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");

            Attachment PatientCopy1 = new Attachment(PatientCopy);
            message.Attachments.Add(PatientCopy1);

            Attachment ReportsCopy1 = new Attachment(ReportsCopy);
            message.Attachments.Add(ReportsCopy1);

            if (FirstAttendant != "")
            {
                Attachment FirstAttendant1 = new Attachment(FirstAttendant);
                message.Attachments.Add(FirstAttendant1);
            }
            if (SecondAttendant != "")
            {
                Attachment SecondAttendant1 = new Attachment(SecondAttendant);
                message.Attachments.Add(SecondAttendant1);
            }
            if (ThirdAttendant != "")
            {
                Attachment ThirdAttendant1 = new Attachment(ThirdAttendant);
                message.Attachments.Add(ThirdAttendant1);
            }
            if (FourthAttendant != "")
            {
                Attachment FourthAttendant1 = new Attachment(FourthAttendant);
                message.Attachments.Add(FourthAttendant1);
            }

            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailVisaInvitation(string PatientName, string EmailID, string ContactNo, string Nationality, string PassportNo, string HospitalName, string TreatmentDocName, string ProvisionalDiagnosis, string PurposeOfVisit, string LilavatiDoctorName, string PreferredDate, string DurationOfTreatment, string ComingForFollowup, string DateOfLastVisit, string NoOfAttendant, string FirstAttName, string FirstRelationship, string FirstPassportNo, string SecondAttName, string SecondRelationship, string SecondPassportNo, string PleaseSpecifyTheReason, string ThirdAttName, string ThirdRelationship, string ThirdPassportNo, string FourthAttName, string FourthRelationship, string FourthPassportNo, string PatientCopy, string ReportsCopy, string FirstAttendant, string SecondAttendant, string ThirdAttendant, string FourthAttendant)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> Visa Invitation Request Details </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Patient's Name : </strong></td><td> " + PatientName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Email Id : </strong></td> <td> " + EmailID + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Contact No. : </strong></td> <td> " + ContactNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Nationality : </strong></td> <td> " + Nationality + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Passport No. : </strong></td> <td> " + PassportNo + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Your country Hospital Name  : </strong></td> <td> " + HospitalName + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Your country Doctor Name : </strong></td> <td> " + TreatmentDocName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Your country Provisional Diagnosis & Name of Ailment : </strong></td> <td> " + ProvisionalDiagnosis + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Lilavati Hospital Purpose of Visit : </strong></td> <td> " + PurposeOfVisit + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Lilavati Hospital Doctor's Name : </strong></td> <td> " + LilavatiDoctorName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Lilavati Hospital Preferred Date of Visit : </strong></td> <td> " + PreferredDate + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Lilavati Hospital Duration of Treatment (approx.) : </strong></td> <td> " + DurationOfTreatment + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Lilavati Hospital Coming for Followup : </strong></td> <td> " + ComingForFollowup + " </td> </tr>");
            if (ComingForFollowup == "Yes")
            {
                strEmailBuilder.AppendFormat("<tr> <td><strong>Lilavati Hospital Date of Last Visit : </strong></td> <td> " + DateOfLastVisit + " </td></tr>");
            }

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>No. of Attendant : </strong></td> <td> " + NoOfAttendant + " </td> </tr>");

            if (NoOfAttendant == "1")
            {
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Attendant Name : </strong></td> <td> " + FirstAttName + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>1st Relationship with Patient : </strong></td> <td> " + FirstRelationship + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Passport No. : </strong></td> <td> " + FirstPassportNo + " </td></tr>");
            }
            else if (NoOfAttendant == "2")
            {
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Attendant Name : </strong></td> <td> " + FirstAttName + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>1st Relationship with Patient : </strong></td> <td> " + FirstRelationship + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Passport No. : </strong></td> <td> " + FirstPassportNo + " </td></tr>");

                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>2nd Attendant Name : </strong></td> <td> " + SecondAttName + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>2nd Relationship with Patient : </strong></td> <td> " + SecondRelationship + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>2nd Passport No. : </strong></td> <td> " + SecondPassportNo + " </td> </tr>");
            }
            else if (NoOfAttendant == "3")
            {
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Attendant Name : </strong></td> <td> " + FirstAttName + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>1st Relationship with Patient : </strong></td> <td> " + FirstRelationship + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Passport No. : </strong></td> <td> " + FirstPassportNo + " </td></tr>");

                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>2nd Attendant Name : </strong></td> <td> " + SecondAttName + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>2nd Relationship with Patient : </strong></td> <td> " + SecondRelationship + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>2nd Passport No. : </strong></td> <td> " + SecondPassportNo + " </td> </tr>");

                strEmailBuilder.AppendFormat("<tr> <td><strong>Please specify the reason for more than 2 attendant : </strong></td> <td> " + PleaseSpecifyTheReason + " </td></tr>");

                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>3rd Attendant Name : </strong></td> <td> " + ThirdAttName + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>3rd Relationship with Patient : </strong></td> <td> " + ThirdRelationship + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>3rd Passport No. : </strong></td> <td> " + ThirdPassportNo + " </td> </tr>");
            }
            else if (NoOfAttendant == "4")
            {
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Attendant Name : </strong></td> <td> " + FirstAttName + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>1st Relationship with Patient : </strong></td> <td> " + FirstRelationship + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>1st Passport No. : </strong></td> <td> " + FirstPassportNo + " </td></tr>");

                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>2nd Attendant Name : </strong></td> <td> " + SecondAttName + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>2nd Relationship with Patient : </strong></td> <td> " + SecondRelationship + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>2nd Passport No. : </strong></td> <td> " + SecondPassportNo + " </td> </tr>");

                strEmailBuilder.AppendFormat("<tr> <td><strong>Please specify the reason for more than 2 attendant : </strong></td> <td> " + PleaseSpecifyTheReason + " </td></tr>");

                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>3rd Attendant Name : </strong></td> <td> " + ThirdAttName + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>3rd Relationship with Patient : </strong></td> <td> " + ThirdRelationship + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>3rd Passport No. : </strong></td> <td> " + ThirdPassportNo + " </td> </tr>");

                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>4th Attendant Name : </strong></td> <td> " + FourthAttName + " </td> </tr>");
                strEmailBuilder.AppendFormat("<tr> <td><strong>4th Relationship with Patient : </strong></td> <td> " + FourthRelationship + " </td></tr>");
                strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>4th Passport No. : </strong></td> <td> " + FourthPassportNo + " </td> </tr>");
            }

            strEmailBuilder.Append("</table>");

            string PatientCopy1 = System.Web.HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/" + PatientCopy);
            string ReportsCopy1 = System.Web.HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/" + ReportsCopy);

            string FirstAttendant1 = string.Empty;
            string SecondAttendant1 = string.Empty;
            string ThirdAttendant1 = string.Empty;
            string FourthAttendant1 = string.Empty;

            if (FirstAttendant != "")
            {
                FirstAttendant1 = System.Web.HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/" + FirstAttendant);
            }
            if (SecondAttendant != "")
            {
                SecondAttendant1 = System.Web.HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/" + SecondAttendant);
            }
            if (ThirdAttendant != "")
            {
                ThirdAttendant1 = System.Web.HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/" + ThirdAttendant);
            }
            if (FourthAttendant != "")
            {
                FourthAttendant1 = System.Web.HttpContext.Current.Server.MapPath("~/uploads/visa_invitation/" + FourthAttendant);
            }

            SendEmail(strEmailBuilder.ToString(), PatientCopy1, ReportsCopy1, FirstAttendant1, SecondAttendant1, ThirdAttendant1, FourthAttendant1);

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}