﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CMSDAL
/// </summary>
public class CMSDAL
{
    Utility utility = new Utility();
    DataTable dt = new DataTable();
	public CMSDAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable GETBYIDContent(CmsMaster cms)
    {
        dt = utility.Display("Execute Proc_CmsMaster 'GET_BY_ID','" + cms.ID + "'");
        if (dt.Rows.Count > 0)
        {
            cms.Heading = dt.Rows[0]["HEADING"].ToString();
            cms.Description = dt.Rows[0]["DESCRIPTION"].ToString();
        }
        return dt;
    }

    public string UpdateContent(CmsMaster cms)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CmsMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@DESCRIPTION", cms.Description);
            cmd.Parameters.AddWithValue("@ID", cms.ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateCms"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateCms"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateCms"].ToString();
    }

    public string SaveCMS(CmsMaster cms)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CmsMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@HEADING", cms.Heading);
            cmd.Parameters.AddWithValue("@PAGE_NAME", cms.PageName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveCms"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveCms"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveCms"].ToString();
    }

    public DataTable BindGrid()
    {
        dt = utility.Display("Execute Proc_CmsMaster 'GET'");
        return dt;
    }

    #region Awards_Accreditations Admin Panel
    public string SaveAwardsAccreditations(Awards_Accreditations tblAwards)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Awards_Accreditations"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@AWARDS_IMAGE", tblAwards.AwardsImage);
            cmd.Parameters.AddWithValue("@AWARDS_TITTLE", tblAwards.Tittle);
            cmd.Parameters.AddWithValue("@AWARDS_DESCRIPTION", tblAwards.Description);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["AwardsSave"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["AwardsSave"] = "Failed";
            }
        }
        return HttpContext.Current.Session["AwardsSave"].ToString();
    }

    public DataTable GetAwards()
    {
        dt = utility.Display("Execute Proc_Awards_Accreditations 'GET'");
        return dt;        
    }

    public string DeletedAwards(Awards_Accreditations tblAwards)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Awards_Accreditations"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@AWARDS_ID", tblAwards.ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["AwardsDelete"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["AwardsDelete"] = "Failed";
            }
        }
        return HttpContext.Current.Session["AwardsDelete"].ToString();
    }

    public DataTable GetByIdAwards(Awards_Accreditations tblAwards)
    {
        dt = utility.Display("Execute Proc_Awards_Accreditations 'GET_BY_ID','" + tblAwards.ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblAwards.ID = Convert.ToInt32(dt.Rows[0]["AWARDS_ID"].ToString());
            tblAwards.AwardsImage = dt.Rows[0]["AWARDS_IMAGE"].ToString();
            tblAwards.Tittle = dt.Rows[0]["AWARDS_TITTLE"].ToString();
            tblAwards.Description = dt.Rows[0]["AWARDS_DESCRIPTION"].ToString();
        }
        return dt;
    }

    public string UpdateAwards(Awards_Accreditations tblAwards)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Awards_Accreditations"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@AWARDS_ID", tblAwards.ID);
            cmd.Parameters.AddWithValue("@AWARDS_IMAGE", tblAwards.AwardsImage);
            cmd.Parameters.AddWithValue("@AWARDS_TITTLE", tblAwards.Tittle);
            cmd.Parameters.AddWithValue("@AWARDS_DESCRIPTION", tblAwards.Description);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateAwards"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateAwards"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateAwards"].ToString();
    }

    #endregion

    #region Awards_Accreditation Front
    public DataTable GET_Awards_Desc(Awards_Accreditations tblAwards)
    {
        dt = utility.Display("Execute Proc_Awards_Accreditations 'GET_BY_DESC','" + tblAwards.ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblAwards.Description = dt.Rows[0]["AWARDS_DESCRIPTION"].ToString();
        }
        return dt;
    }
    #endregion

    #region Media Press Year Admin
    public string SaveMediaYear(MediaYear tblMediaYear)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Year"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@YEAR_NAME", tblMediaYear.YearName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveYear"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveYear"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveYear"].ToString();
    }

    public DataTable GetYear()
    {
        dt = utility.Display("Execute Proc_Media_Year 'GET'");
        return dt;
    }

    public string DeleteYear(MediaYear tblMediaYear)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Year"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@YEAR_ID", tblMediaYear.YearId);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteYear"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteYear"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteYear"].ToString();
    }

    public DataTable GetByYearID(MediaYear tblMediaYear)
    {
        dt = utility.Display("Execute Proc_Media_Year 'GET_BY_ID','" + tblMediaYear.YearId + "'");
        if (dt.Rows.Count > 0)
        {
            tblMediaYear.YearId = int.Parse(dt.Rows[0]["YEAR_ID"].ToString());
            tblMediaYear.YearName = dt.Rows[0]["YEAR_NAME"].ToString();
        }
        return dt;
    }

    public string UpdateYear(MediaYear tblMediaYear)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Year"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@YEAR_ID", tblMediaYear.YearId);
            cmd.Parameters.AddWithValue("@YEAR_NAME", tblMediaYear.YearName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateYear"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateYear"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateYear"].ToString();
    }

    #endregion

    #region Media Press Admin
    public DataTable BindYear()
    {
        dt = utility.Display("Execute Proc_Media_Year 'GET_BY_YEAR'");
        return dt;
    }

    public DataTable BindMonth()
    {
        dt = utility.Display("Execute Proc_Media_Press 'GET_BY_MONTH'");
        return dt;
    }

    public string SaveMediaPress(MediaPress tblMediaPress)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Press"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@YEAR_ID", tblMediaPress.YEAR_ID);
            cmd.Parameters.AddWithValue("@YEARNAME_ID", tblMediaPress.YEARNAME_ID);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblMediaPress.DESCRIPTION);
            if (tblMediaPress.Publication == "Video Gallery")
            {
                cmd.Parameters.AddWithValue("@PDF_FILE", tblMediaPress.Video);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PDF_FILE", tblMediaPress.PdfFile);                
            }
            
            cmd.Parameters.AddWithValue("@PUBLICATION", tblMediaPress.Publication);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SavePressMedia"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SavePressMedia"] = "Failed";
            }
        }

        return HttpContext.Current.Session["SavePressMedia"].ToString();
    }

    public DataTable GetMediaPress()
    {
        dt = utility.Display("Execute Proc_Media_Press 'GET'");
        return dt;
    }

    public string DeleteMediaPress(MediaPress tblMediaPress)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Press"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@MEDIA_ID", tblMediaPress.MEDIA_ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteMediaPress"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteMediaPress"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteMediaPress"].ToString();
    }

    public DataTable GETByMediaPressID(MediaPress tblMediaPress)
    {
        dt = utility.Display("Execute Proc_Media_Press 'GET_BY_ID','" + tblMediaPress.MEDIA_ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblMediaPress.MEDIA_ID = Convert.ToInt32(dt.Rows[0]["MEDIA_ID"].ToString());
            tblMediaPress.Year = dt.Rows[0]["YEAR_NAME"].ToString();
            tblMediaPress.Month = dt.Rows[0]["YEAR_WISENAME"].ToString();
            tblMediaPress.DESCRIPTION = dt.Rows[0]["DESCRIPTION"].ToString();
            tblMediaPress.PdfFile = dt.Rows[0]["PDF_FILE"].ToString();
            tblMediaPress.Publication = dt.Rows[0]["Publication"].ToString();
            tblMediaPress.Video = dt.Rows[0]["PDF_FILE"].ToString();

            tblMediaPress.YEAR_ID = int.Parse(dt.Rows[0]["YEAR_ID"].ToString());
            tblMediaPress.YEARNAME_ID = int.Parse(dt.Rows[0]["YEARNAME_ID"].ToString());

        }
        return dt;
    }

    public string UpdateMediaPress(MediaPress tblMediaPress)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Media_Press"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@MEDIA_ID", tblMediaPress.MEDIA_ID);
            cmd.Parameters.AddWithValue("@YEAR_ID", tblMediaPress.YEAR_ID);
            cmd.Parameters.AddWithValue("@YEARNAME_ID", tblMediaPress.YEARNAME_ID);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblMediaPress.DESCRIPTION);
            //cmd.Parameters.AddWithValue("@PDF_FILE", tblMediaPress.PdfFile);
            if (tblMediaPress.Publication == "Video Gallery")
            {
                cmd.Parameters.AddWithValue("@PDF_FILE", tblMediaPress.Video);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PDF_FILE", tblMediaPress.PdfFile);
            }

            cmd.Parameters.AddWithValue("@PUBLICATION", tblMediaPress.Publication);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateMediaPress"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateMediaPress"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateMediaPress"].ToString();
    }

    #endregion

    #region Media Press Front
    public DataTable GETYearWiseMonth(MediaPress tblMediaPress)    {

        dt = utility.Display("EXECUTE Proc_Media_Year 'GET_YEARWISE_MONTH',0,'" + tblMediaPress.YearName + "','','" + tblMediaPress.Publication + "'");
        if (dt.Rows.Count > 0)
        {
            tblMediaPress.Month = dt.Rows[0]["Year_WiseName"].ToString();            
        }
        return dt;
    }

    public DataTable GETYearWiseMonthWiseDescrption(MediaPress tblMediaPress)
    {
        dt = utility.Display("EXECUTE Proc_Media_Year 'GET_YEARWISE_MONTHWISE_DESC',0,'" + tblMediaPress.YearName + "','" + tblMediaPress.Month + "','" + tblMediaPress.Publication + "'");
        return dt;
    }
    #endregion

    #region Testimonial Admin

    public string SaveTestimonial(Testimonial tblTestimonial)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Testimonial"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@TYPE", tblTestimonial.Type);
            cmd.Parameters.AddWithValue("@NAME", tblTestimonial.Name);
            cmd.Parameters.AddWithValue("@CELEBRITY_IMAGE", tblTestimonial.CelebrityImage);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblTestimonial.Description);
            cmd.Parameters.AddWithValue("@TESTI_IMAGE", tblTestimonial.TestImage);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveTestimonial"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveTestimonial"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveTestimonial"].ToString();
    }

    public DataTable GetTestimonial()
    {
        dt = utility.Display("Execute Proc_Testimonial 'GET'");
        return dt;
    }

    public string DeleteTestimonial(Testimonial tblTestimonial)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Testimonial"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@TEST_ID", tblTestimonial.Test_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteTest"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteTest"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteTest"].ToString();
    }

    public DataTable GetByTestimonial(Testimonial tblTestimonial)
    {
        dt = utility.Display("Execute Proc_Testimonial 'GET_BY_ID','" + tblTestimonial.Test_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblTestimonial.Test_Id = int.Parse(dt.Rows[0]["TEST_ID"].ToString());
            tblTestimonial.CelebrityImage = dt.Rows[0]["CELEBRITY_IMAGE"].ToString();
            tblTestimonial.Name = dt.Rows[0]["NAME"].ToString();
            tblTestimonial.Description = dt.Rows[0]["DESCRIPTION"].ToString();
            tblTestimonial.Type = dt.Rows[0]["TYPE"].ToString();
            tblTestimonial.TestImage = dt.Rows[0]["TESTI_IMAGE"].ToString();
        }
        return dt;
    }

    public string UpdateTestimonial(Testimonial tblTestimonial)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Testimonial"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@TEST_ID", tblTestimonial.Test_Id);
            cmd.Parameters.AddWithValue("@CELEBRITY_IMAGE", tblTestimonial.CelebrityImage);
            cmd.Parameters.AddWithValue("@TYPE", tblTestimonial.Type);
            cmd.Parameters.AddWithValue("@NAME", tblTestimonial.Name);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblTestimonial.Description);
            cmd.Parameters.AddWithValue("@TESTI_IMAGE", tblTestimonial.TestImage);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateTest"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateTest"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateTest"].ToString();
    }

    public DataTable SearchTestimonial(Testimonial tblTestimonial)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Testimonial"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "GET_BY_SEARCH");
            cmd.Parameters.AddWithValue("@SEARCH", tblTestimonial.Search);
            dt = utility.Display(cmd);                       
        }

        return dt;
    }

    #endregion

    #region Testimonial Front

    public DataTable TestimonialCelebrityCommonPeople(Testimonial tblTestimonial)
    {
        dt = utility.Display("Execute [Proc_Testimonial] 'GET_CELEBRITY_COMMON',0,'" + tblTestimonial.Type + "'");
        return dt;
    }

    #endregion

    #region Job Area Admin
    public string SaveJobArea(Job_Area tblJobArea)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Job_Area"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@JOB_AREA", tblJobArea.Job_Areas);
            cmd.Parameters.AddWithValue("@JOB_POSITION", tblJobArea.Job_Position);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveJobArea"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveJobArea"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveJobArea"].ToString();
    }

    public DataTable GetJobArea()
    {
        dt = utility.Display("Execute Proc_Job_Area 'GET'");
        return dt;
    }

    public string DeleteJobArea(Job_Area tblJobArea)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Job_Area"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@JOB_ID", tblJobArea.Job_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteJobArea"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteJobArea"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteJobArea"].ToString();
    }

    public DataTable GEtByJobArea(Job_Area tblJobArea)
    {
        dt = utility.Display("Execute Proc_Job_Area 'GET_BY_ID','" + tblJobArea.Job_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblJobArea.Job_Id = int.Parse(dt.Rows[0]["JOB_ID"].ToString());
            tblJobArea.Job_Areas = dt.Rows[0]["JOB_AREA"].ToString();
            tblJobArea.Job_Position = dt.Rows[0]["JOB_POSITION"].ToString();            
        }
        return dt;
    }

    public string UpdateJobArea(Job_Area tblJobArea)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Job_Area"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@JOB_ID", tblJobArea.Job_Id);
            cmd.Parameters.AddWithValue("@JOB_AREA", tblJobArea.Job_Areas);
            cmd.Parameters.AddWithValue("@JOB_POSITION", tblJobArea.Job_Position);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateJobArea"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateJobArea"] = "Failed";
            }
        }

        return HttpContext.Current.Session["UpdateJobArea"].ToString();
    }

    #endregion

    #region Job Posting Admin

    public DataTable BindJobArea()
    {
        dt = utility.Display("Execute Proc_Job_Area 'GET_JOBAREA'");
        return dt;
    }

    public string SaveJobPosting(Job_Posting tblJobPosting)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Job_Posting"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@JOB_ID", tblJobPosting.Job_Id);
            cmd.Parameters.AddWithValue("@POSITION", tblJobPosting.Position);
            cmd.Parameters.AddWithValue("@SPECIFICATION", tblJobPosting.Specification);
            cmd.Parameters.AddWithValue("@DURATION", tblJobPosting.Duration);
            cmd.Parameters.AddWithValue("@VACANCY", tblJobPosting.Vacancy);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveJobPosting"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveJobPosting"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveJobPosting"].ToString();
    }

    public DataTable GetJobPosting()
    {
        dt = utility.Display("Execute Proc_Job_Posting 'GET'");
        return dt;
    }

    public string DeleteJobPosting(Job_Posting tblJobPosting)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Job_Posting"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@POST_ID", tblJobPosting.Post_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteJobPosting"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteJobPosting"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteJobPosting"].ToString();
    }

    public DataTable GetByJobPostingId(Job_Posting tblJobPosting)
    {
        dt = utility.Display("Execute Proc_Job_Posting 'GET_BY_ID','" + tblJobPosting.Post_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblJobPosting.Post_Id = int.Parse(dt.Rows[0]["POST_ID"].ToString());
            tblJobPosting.Job_Id = int.Parse(dt.Rows[0]["JOB_ID"].ToString());
            tblJobPosting.Position = dt.Rows[0]["POSITION"].ToString();
            tblJobPosting.Specification = dt.Rows[0]["SPECIFICATION"].ToString();
            tblJobPosting.Duration = dt.Rows[0]["DURATION"].ToString();
            tblJobPosting.Vacancy = dt.Rows[0]["VACANCY"].ToString();
        }
        return dt;
    }

    public string UpdateJobPosting(Job_Posting tblJobPosting)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Job_Posting"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@POST_ID", tblJobPosting.Post_Id);
            cmd.Parameters.AddWithValue("@JOB_ID", tblJobPosting.Job_Id);
            cmd.Parameters.AddWithValue("@POSITION", tblJobPosting.Position);
            cmd.Parameters.AddWithValue("@SPECIFICATION", tblJobPosting.Specification);
            cmd.Parameters.AddWithValue("@DURATION", tblJobPosting.Duration);
            cmd.Parameters.AddWithValue("@VACANCY", tblJobPosting.Vacancy);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateJobPosting"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateJobPosting"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateJobPosting"].ToString();
    }

    #endregion

    #region Career Front

    public DataTable CareerDeparmentByPosition(Job_Posting tblJobPosting)
    {
        dt = utility.Display("Execute Proc_Job_Posting 'DEPARTMENT_BY_POSITION',0,'" + tblJobPosting.Job_Id + "'");       
        return dt;
    }

    public DataTable CareerPositionWiseRecord(Job_Posting tblJobPosting)
    {
        dt = utility.Display("Execute Proc_Job_Posting 'POSITION_WISE_RECORD','" + tblJobPosting.Post_Id + "'");
        return dt;
    }

    #endregion

    #region CMSPageMaster Admin

    public string SaveCMSPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CMSPageMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@PAGE_NAME", tblcmsPageMaster.PageName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveCmsPageMAster"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveCmsPageMAster"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveCmsPageMAster"].ToString();
    }

    public DataTable GetCmsPageMaster()
    {
        dt = utility.Display("Execute Proc_CMSPageMaster 'GET'");
        return dt;
    }

    public string DeleteCMSPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CMSPageMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@PAGE_ID", tblcmsPageMaster.PageId);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteCmsPageMAster"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteCmsPageMAster"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteCmsPageMAster"].ToString();
    }

    public DataTable GetBYCmsPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        dt = utility.Display("Execute Proc_CMSPageMaster 'GET_BY_ID','" + tblcmsPageMaster.PageId + "'");
        if (dt.Rows.Count > 0)
        {
            tblcmsPageMaster.PageId = int.Parse(dt.Rows[0]["PAGE_ID"].ToString());
            tblcmsPageMaster.PageName = dt.Rows[0]["PAGE_NAME"].ToString();
        }
        return dt;
    }

    public string UpdateCMSPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CMSPageMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@PAGE_ID", tblcmsPageMaster.PageId);
            cmd.Parameters.AddWithValue("@PAGE_NAME", tblcmsPageMaster.PageName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateCmsPageMAster"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateCmsPageMAster"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateCmsPageMAster"].ToString();
    }

    #endregion

    #region CMSInnerPageMaster Admin

    public DataTable GetPageNAmeCMSInnerPageMaster()
    {
        dt = utility.Display("Execute Proc_CMSInnerPageMaster 'GET_PAGE_NAME'");
        return dt;
    }

    public string SaveCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CMSInnerPageMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@PAGE_ID", tblcmsInnerPageMaster.PageId);
            cmd.Parameters.AddWithValue("@TAB", tblcmsInnerPageMaster.Tab);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblcmsInnerPageMaster.Description);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveCMSInnerPageMaster"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveCMSInnerPageMaster"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveCMSInnerPageMaster"].ToString();
    }

    public DataTable GetCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        dt = utility.Display("Execute Proc_CMSInnerPageMaster 'GET',0,'" + tblcmsInnerPageMaster.PageId + "'");
        return dt;
    }

    public string DeleteCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CMSInnerPageMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@INNER_ID", tblcmsInnerPageMaster.InnerId);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteCmsInnerPageMAster"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteCmsInnerPageMAster"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteCmsInnerPageMAster"].ToString();
    }

    public DataTable GetBYCmsInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        dt = utility.Display("Execute Proc_CMSInnerPageMaster 'GET_BY_ID','" + tblcmsInnerPageMaster.InnerId + "'");
        if (dt.Rows.Count > 0)
        {
            tblcmsInnerPageMaster.InnerId = int.Parse(dt.Rows[0]["INNER_ID"].ToString());
            tblcmsInnerPageMaster.PageId = int.Parse(dt.Rows[0]["PAGE_ID"].ToString());
            tblcmsInnerPageMaster.PageName = dt.Rows[0]["PAGE_NAME"].ToString();
            tblcmsInnerPageMaster.Tab = dt.Rows[0]["TAB"].ToString();
            tblcmsInnerPageMaster.Description = dt.Rows[0]["DESCRIPTION"].ToString();
        }
        return dt;
    }

    public string UpdateCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CMSInnerPageMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@INNER_ID", tblcmsInnerPageMaster.InnerId);
            cmd.Parameters.AddWithValue("@PAGE_ID", tblcmsInnerPageMaster.PageId);
            cmd.Parameters.AddWithValue("@TAB", tblcmsInnerPageMaster.Tab);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblcmsInnerPageMaster.Description);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateCmsInnerPageMAster"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateCmsInnerPageMAster"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateCmsInnerPageMAster"].ToString();
    }

    #endregion

    #region CMSInnerPageMaster Front

    public DataTable BindRepeaterCMS(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        dt = utility.Display("Execute Proc_CMSInnerPageMaster 'BINDREPEATER',0,0,'','','" + tblcmsInnerPageMaster.PageName + "'");
        return dt;
    }

    #endregion

    #region Health Checkup Admin

    public string SaveHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Health_Checkup"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@PACKAGE_NAME", tblHealthCheckup.Package_Name);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveHealthCheckup"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveHealthCheckup"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveHealthCheckup"].ToString();
    }

    public DataTable GetHealthCheckup()
    {
        dt = utility.Display("Execute Proc_Health_Checkup 'GET'");
        return dt;
    }

    public DataTable AdminGetHealthCheckup()
    {
        dt = utility.Display("Execute Proc_Health_Checkup 'ADMIN_GET'");
        return dt;
    }

    public string DeleteHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Health_Checkup"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@HEALTH_ID", tblHealthCheckup.Health_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteHealthCheckup"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteHealthCheckup"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteHealthCheckup"].ToString();
    }

    public DataTable GetBYHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        dt = utility.Display("Execute Proc_Health_Checkup 'GET_BY_ID','" + tblHealthCheckup.Health_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblHealthCheckup.Health_Id = int.Parse(dt.Rows[0]["HEALTH_ID"].ToString());
            tblHealthCheckup.Package_Name = dt.Rows[0]["PACKAGE_NAME"].ToString();
        }
        return dt;
    }

    public string UpdateHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Health_Checkup"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@HEALTH_ID", tblHealthCheckup.Health_Id);
            cmd.Parameters.AddWithValue("@PACKAGE_NAME", tblHealthCheckup.Package_Name);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateHealthCheckup"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateHealthCheckup"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateHealthCheckup"].ToString();
    }

    #endregion

    #region Health Packages Details Admin

    public string SaveHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Health_Packages_Details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@HEALTH_ID", tblHealthPackagesDetails.Health_Id);
            cmd.Parameters.AddWithValue("@PACK_PRICE", tblHealthPackagesDetails.Pack_Price);
            cmd.Parameters.AddWithValue("@TITTLE", tblHealthPackagesDetails.Tittle);
            cmd.Parameters.AddWithValue("@TAB_NAME", tblHealthPackagesDetails.Tab_Name);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblHealthPackagesDetails.Description);
            cmd.Parameters.AddWithValue("@NOTE", tblHealthPackagesDetails.Note);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveHealthPackagesDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveHealthPackagesDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveHealthPackagesDetails"].ToString();
    }

    public DataTable GetHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        dt = utility.Display("Execute Proc_Health_Packages_Details 'GET',0,'" + tblHealthPackagesDetails.Health_Id + "'");
        return dt;
    }

    public string DeleteHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Health_Packages_Details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@PACK_ID", tblHealthPackagesDetails.Pack_Id);           
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteHealthPackagesDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteHealthPackagesDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteHealthPackagesDetails"].ToString();
    }

    public DataTable GetByHealthPackagesDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        dt = utility.Display("Execute Proc_Health_Packages_Details 'GET_BY_ID','" + tblHealthPackagesDetails.Pack_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblHealthPackagesDetails.Pack_Id = int.Parse(dt.Rows[0]["PACK_ID"].ToString());
            tblHealthPackagesDetails.Health_Id = int.Parse(dt.Rows[0]["HEALTH_ID"].ToString());
            tblHealthPackagesDetails.Pack_Price = dt.Rows[0]["PACK_PRICE"].ToString();
            tblHealthPackagesDetails.Tittle = dt.Rows[0]["TITTLE"].ToString();
            tblHealthPackagesDetails.Tab_Name = dt.Rows[0]["TAB_NAME"].ToString();
            tblHealthPackagesDetails.Description = dt.Rows[0]["DESCRIPTION"].ToString();
            tblHealthPackagesDetails.Note = dt.Rows[0]["NOTE"].ToString();
        }
        return dt;
    }

    public string UpdateHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Health_Packages_Details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@PACK_ID", tblHealthPackagesDetails.Pack_Id);
            cmd.Parameters.AddWithValue("@HEALTH_ID", tblHealthPackagesDetails.Health_Id);
            cmd.Parameters.AddWithValue("@PACK_PRICE", tblHealthPackagesDetails.Pack_Price);
            cmd.Parameters.AddWithValue("@TITTLE", tblHealthPackagesDetails.Tittle);
            cmd.Parameters.AddWithValue("@TAB_NAME", tblHealthPackagesDetails.Tab_Name);
            cmd.Parameters.AddWithValue("@DESCRIPTION", tblHealthPackagesDetails.Description);
            cmd.Parameters.AddWithValue("@NOTE", tblHealthPackagesDetails.Note);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateHealthPackagesDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateHealthPackagesDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateHealthPackagesDetails"].ToString();
    }

    #endregion

    #region Health Packages Details Front

    public DataTable PackagesDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        dt = utility.Display("Execute Proc_Health_Packages_Details 'PACKAGES_DETAILS',0,'" + tblHealthPackagesDetails.Health_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblHealthPackagesDetails.Pack_Price = dt.Rows[0]["PACK_PRICE"].ToString();
            tblHealthPackagesDetails.Tittle = dt.Rows[0]["TITTLE"].ToString();
            tblHealthPackagesDetails.Note = dt.Rows[0]["NOTE"].ToString();
            tblHealthPackagesDetails.Description = dt.Rows[0]["DESCRIPTION"].ToString();
            tblHealthPackagesDetails.Tab_Name = dt.Rows[0]["TAB_NAME"].ToString();
        }
        return dt;
    }

    public DataTable PackagesName(HealthPackagesDetails tblHealthPackagesDetails)
    {
        dt = utility.Display("Execute Proc_Health_Packages_Details 'PACKAGE_NAME',0,'" + tblHealthPackagesDetails.Health_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblHealthPackagesDetails.Package_Name = dt.Rows[0]["PACKAGE_NAME"].ToString();
        }
        return dt;
    }

    public DataTable PackagesMenu()
    {
        dt = utility.Display("Execute Proc_Health_Packages_Details 'PACKAGE_MENU'");        
        return dt;
    }

    public DataTable HealthPackagesNamePrice()
    {
        dt = utility.Display("Execute Proc_Health_Packages_Details 'HEALTH_PACKAGE_NAME_PRICE'");
        return dt;
    }


    #endregion

    #region Contact Us Admin

    public string UpdateContactUs(ContactUs tblContactUs)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Contact_Us"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@CNT_ID", tblContactUs.Cnt_Id);
            cmd.Parameters.AddWithValue("@ADDRESS", tblContactUs.Address);
            cmd.Parameters.AddWithValue("@EMERGENCY_CASUALTY", tblContactUs.Emergency_Casualty);
            cmd.Parameters.AddWithValue("@AMBULANCE", tblContactUs.Ambulance);
            cmd.Parameters.AddWithValue("@HOSPITAL_BOARD_LINE", tblContactUs.Hospital_Board_Line);
            cmd.Parameters.AddWithValue("@HOSPITAL_FAX", tblContactUs.Hospital_Fax);
            cmd.Parameters.AddWithValue("@ADMISSION_DEPARTMENT", tblContactUs.Admission_Department);
            cmd.Parameters.AddWithValue("@TPA_CELL", tblContactUs.TPA_Cell);
            cmd.Parameters.AddWithValue("@BILLING_INPATIENT", tblContactUs.Billing_Inpatient);
            cmd.Parameters.AddWithValue("@BILLING_OPD", tblContactUs.Billing_OPD);
            cmd.Parameters.AddWithValue("@APPOINTMENT_OPD", tblContactUs.Appointment_OPD);
            cmd.Parameters.AddWithValue("@BLOOD_BANK", tblContactUs.Blood_Bank);
            cmd.Parameters.AddWithValue("@HEALTH_CHECK_UP_DEPARTMENT", tblContactUs.Health_Check_up_Department);
            cmd.Parameters.AddWithValue("@REPORT_DISPATCH_COUNTER", tblContactUs.Report_Dispatch_Counter);
            cmd.Parameters.AddWithValue("@MRI_DEPARTMENT", tblContactUs.MRI_Department);
            cmd.Parameters.AddWithValue("@X_RAY_SONOGRAPHY_DEPARTMENT", tblContactUs.X_Ray_Sonography_Department);
            cmd.Parameters.AddWithValue("@CT_SCAN_DEPARTMENT", tblContactUs.CT_Scan_Department);
            cmd.Parameters.AddWithValue("@EMAIL_ADDRESS", tblContactUs.Email_Address);

            cmd.Parameters.AddWithValue("@Nuclear_Medicine", tblContactUs.Nuclear_Medicine);
            cmd.Parameters.AddWithValue("@Physiotherapy", tblContactUs.Physiotherapy);
            cmd.Parameters.AddWithValue("@Dental", tblContactUs.Dental);
            cmd.Parameters.AddWithValue("@Dermatology", tblContactUs.Dermatology);
            cmd.Parameters.AddWithValue("@Ophthalmology", tblContactUs.Ophthalmology);
            cmd.Parameters.AddWithValue("@ENT_Audiometry", tblContactUs.ENT_Audiometry);
            cmd.Parameters.AddWithValue("@IVF", tblContactUs.IVF);
            cmd.Parameters.AddWithValue("@EMG_EEG", tblContactUs.EMG_EEG);
            cmd.Parameters.AddWithValue("@Visa_Section", tblContactUs.Visa_Section);
            cmd.Parameters.AddWithValue("@Cardiology", tblContactUs.Cardiology);
            cmd.Parameters.AddWithValue("@Sample_collection_room", tblContactUs.Sample_collection_room);
            cmd.Parameters.AddWithValue("@Endoscopy", tblContactUs.Endoscopy);
            cmd.Parameters.AddWithValue("@TollFree", tblContactUs.TollFree);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateContactUs"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateContactUs"] = "Failed";
            }
        }

        return HttpContext.Current.Session["UpdateContactUs"].ToString();
    }

    public DataTable GetContactUS(ContactUs tblContactUs)
    {         
        dt = utility.Display("Execute Proc_Contact_Us 'GET'");
        if (dt.Rows.Count > 0)
        {
            tblContactUs.Address = dt.Rows[0]["ADDRESS"].ToString();
            tblContactUs.Emergency_Casualty = dt.Rows[0]["EMERGENCY_CASUALTY"].ToString();
            tblContactUs.Ambulance = dt.Rows[0]["AMBULANCE"].ToString();
            tblContactUs.Hospital_Board_Line = dt.Rows[0]["HOSPITAL_BOARD_LINE"].ToString();
            tblContactUs.Hospital_Fax = dt.Rows[0]["HOSPITAL_FAX"].ToString();
            tblContactUs.Admission_Department = dt.Rows[0]["ADMISSION_DEPARTMENT"].ToString();
            tblContactUs.TPA_Cell = dt.Rows[0]["TPA_CELL"].ToString();
            tblContactUs.Billing_Inpatient = dt.Rows[0]["BILLING_INPATIENT"].ToString();
            tblContactUs.Billing_OPD = dt.Rows[0]["BILLING_OPD"].ToString();
            tblContactUs.Appointment_OPD = dt.Rows[0]["APPOINTMENT_OPD"].ToString();
            tblContactUs.Blood_Bank = dt.Rows[0]["BLOOD_BANK"].ToString();
            tblContactUs.Health_Check_up_Department = dt.Rows[0]["HEALTH_CHECK_UP_DEPARTMENT"].ToString();
            tblContactUs.Report_Dispatch_Counter = dt.Rows[0]["REPORT_DISPATCH_COUNTER"].ToString();
            tblContactUs.MRI_Department = dt.Rows[0]["MRI_DEPARTMENT"].ToString();
            tblContactUs.X_Ray_Sonography_Department = dt.Rows[0]["X_RAY_SONOGRAPHY_DEPARTMENT"].ToString();
            tblContactUs.CT_Scan_Department = dt.Rows[0]["CT_SCAN_DEPARTMENT"].ToString();
            tblContactUs.Email_Address = dt.Rows[0]["EMAIL_ADDRESS"].ToString();

            tblContactUs.Nuclear_Medicine = dt.Rows[0]["Nuclear_Medicine"].ToString();
            tblContactUs.Physiotherapy = dt.Rows[0]["Physiotherapy"].ToString();
            tblContactUs.Dental = dt.Rows[0]["Dental"].ToString();
            tblContactUs.Dermatology = dt.Rows[0]["Dermatology"].ToString();
            tblContactUs.Ophthalmology = dt.Rows[0]["Ophthalmology"].ToString();
            tblContactUs.ENT_Audiometry = dt.Rows[0]["ENT_Audiometry"].ToString();
            tblContactUs.IVF = dt.Rows[0]["IVF"].ToString();
            tblContactUs.EMG_EEG = dt.Rows[0]["EMG_EEG"].ToString();
            tblContactUs.Visa_Section = dt.Rows[0]["Visa_Section"].ToString();
            tblContactUs.Cardiology = dt.Rows[0]["Cardiology"].ToString();
            tblContactUs.Sample_collection_room = dt.Rows[0]["Sample_collection_room"].ToString();
            tblContactUs.Endoscopy = dt.Rows[0]["Endoscopy"].ToString();
            tblContactUs.TollFree = dt.Rows[0]["TollFree"].ToString();
           
        }
        return dt;
    }

    #endregion

    #region LHMT Admin

    public string SaveLHMT(LHMT tblLHMT)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_LHMT"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@TITTLE", tblLHMT.Tittle);
            cmd.Parameters.AddWithValue("@BROUCHRE", tblLHMT.Brouchre);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveLHMT"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveLHMT"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveLHMT"].ToString();
    }

    public DataTable GetLHMT()
    {
        dt = utility.Display("Execute Proc_LHMT 'GET'");
        return dt;
    }

    public string DeleteLHMT(LHMT tblLHMT)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_LHMT"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", tblLHMT.Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteLHMT"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteLHMT"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteLHMT"].ToString();
    }

    public DataTable GetByLHMT(LHMT tblLHMT)
    {
        dt = utility.Display("Execute Proc_LHMT 'GET_BY_ID','" + tblLHMT.Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblLHMT.Id = int.Parse(dt.Rows[0]["ID"].ToString());
            tblLHMT.Tittle = dt.Rows[0]["TITTLE"].ToString();
            tblLHMT.Brouchre = dt.Rows[0]["BROUCHRE"].ToString();
        }
        return dt;
    }

    public string UpdateLHMT(LHMT tblLHMT)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_LHMT"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@ID", tblLHMT.Id);
            cmd.Parameters.AddWithValue("@TITTLE", tblLHMT.Tittle);
            cmd.Parameters.AddWithValue("@BROUCHRE", tblLHMT.Brouchre);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateLHMT"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateLHMT"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateLHMT"].ToString();
    }

    #endregion

    #region Empanelled Companies Admin

    public string SaveEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Empanelled_Tab"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@TAB_NAME", tblEmpanelledCompanies.Tab_Name);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveEmpanelledCompanies"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveEmpanelledCompanies"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveEmpanelledCompanies"].ToString();
    }

    public DataTable GetEmpanelledCompanies()
    {
        dt = utility.Display("Execute Proc_Empanelled_Tab 'GET'");
        return dt;
    }

    public string DeleteEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Empanelled_Tab"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@TAB_ID", tblEmpanelledCompanies.Tab_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteEmpanelledCompanies"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteEmpanelledCompanies"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteEmpanelledCompanies"].ToString();
    }

    public DataTable GetBYEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        dt = utility.Display("Execute Proc_Empanelled_Tab 'GET_BY_ID','" + tblEmpanelledCompanies.Tab_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblEmpanelledCompanies.Tab_Id = int.Parse(dt.Rows[0]["TAB_ID"].ToString());
            tblEmpanelledCompanies.Tab_Name = dt.Rows[0]["TAB_NAME"].ToString();
        }
        return dt;
    }

    public string UpdateEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Empanelled_Tab"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@TAB_ID", tblEmpanelledCompanies.Tab_Id);
            cmd.Parameters.AddWithValue("@TAB_NAME", tblEmpanelledCompanies.Tab_Name);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateEmpanelledCompanies"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateEmpanelledCompanies"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateEmpanelledCompanies"].ToString();
    }

    #endregion

    #region Empanelled Companies Details Admin

    public string SaveEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Empanelled_Details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@TAB_ID", tblEmpanelledDetails.Tab_Id);
            cmd.Parameters.AddWithValue("@NAME", tblEmpanelledDetails.Name);
            cmd.Parameters.AddWithValue("@LOGO", tblEmpanelledDetails.Logo);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveEmpanelledDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveEmpanelledDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveEmpanelledDetails"].ToString();
    }

    public DataTable GetEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        dt = utility.Display("Execute Proc_Empanelled_Details 'GET',0,'" + tblEmpanelledDetails.Tab_Id + "'");
        return dt;
    }

    public string DeleteEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Empanelled_Details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@EMP_ID", tblEmpanelledDetails.Emp_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteEmpanelledDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteEmpanelledDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteEmpanelledDetails"].ToString();
    }

    public DataTable GetBYEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        dt = utility.Display("Execute Proc_Empanelled_Details 'GET_BY_ID','" + tblEmpanelledDetails.Emp_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblEmpanelledDetails.Emp_Id = int.Parse(dt.Rows[0]["EMP_ID"].ToString());
            tblEmpanelledDetails.Name = dt.Rows[0]["NAME"].ToString();
            tblEmpanelledDetails.Logo = dt.Rows[0]["LOGO"].ToString();
        }
        return dt;
    }

    public string UpdateEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Empanelled_Details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@EMP_ID", tblEmpanelledDetails.Emp_Id);
            cmd.Parameters.AddWithValue("@NAME", tblEmpanelledDetails.Name);
            cmd.Parameters.AddWithValue("@LOGO", tblEmpanelledDetails.Logo);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateEmpanelledDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateEmpanelledDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateEmpanelledDetails"].ToString();
    }

    public DataTable GetTabNameEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        dt = utility.Display("Execute Proc_Empanelled_Details 'GET_TAB_NAME',0,'" + tblEmpanelledDetails.Tab_Id + "'");
        tblEmpanelledDetails.Tab_Name = dt.Rows[0]["TAB_NAME"].ToString();        
        return dt;
    }

    #endregion

    #region Empanelled Companies Details Front

    public DataTable EmpanelledDetailsTabNameWiseCompany(EmpanelledDetails tblEmpanelledDetails)
    {
        dt = utility.Display("Execute Proc_Empanelled_Details 'TABNAME_WISE_COMPANY',0,'" + tblEmpanelledDetails.Tab_Id + "'");
        return dt;
    }

    #endregion

    #region Patients Education Admin

    public string SavePatientsEducation(PatientsEducation tblPatientsEducation)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Patients_Education"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@TITTLE", tblPatientsEducation.Tittle);
            cmd.Parameters.AddWithValue("@THUMBIMAGE", tblPatientsEducation.ThumbImage);
            cmd.Parameters.AddWithValue("@BROCHURE", tblPatientsEducation.Brochure);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SavePatientsEducation"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SavePatientsEducation"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SavePatientsEducation"].ToString();
    }

    public DataTable GetPatientsEducation()
    {
        dt = utility.Display("Execute Proc_Patients_Education 'GET'");
        return dt;
    }

    public string DeletePatientsEducation(PatientsEducation tblPatientsEducation)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Patients_Education"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@PAT_ID", tblPatientsEducation.Pat_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeletePatientsEducation"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeletePatientsEducation"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeletePatientsEducation"].ToString();
    }

    public DataTable GetBYPatientsEducation(PatientsEducation tblPatientsEducation)
    {
        dt = utility.Display("Execute Proc_Patients_Education 'GET_BY_ID','" + tblPatientsEducation.Pat_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblPatientsEducation.Pat_Id = int.Parse(dt.Rows[0]["PAT_ID"].ToString());
            tblPatientsEducation.Tittle = dt.Rows[0]["TITTLE"].ToString();
            tblPatientsEducation.ThumbImage = dt.Rows[0]["THUMBIMAGE"].ToString();
            tblPatientsEducation.Brochure = dt.Rows[0]["BROCHURE"].ToString();
        }
        return dt;
    }

    public string UpdatePatientsEducation(PatientsEducation tblPatientsEducation)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Patients_Education"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@PAT_ID", tblPatientsEducation.Pat_Id);
            cmd.Parameters.AddWithValue("@TITTLE", tblPatientsEducation.Tittle);
            cmd.Parameters.AddWithValue("@THUMBIMAGE", tblPatientsEducation.ThumbImage);
            cmd.Parameters.AddWithValue("@BROCHURE", tblPatientsEducation.Brochure);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdatePatientsEducation"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdatePatientsEducation"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdatePatientsEducation"].ToString();
    }

    #endregion

    #region Patients Education Brochure Admin

    public string SavePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Patients_Education_Brochure"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@Pat_Id", tblPatientsEducationBrochure.Pat_Id);
            cmd.Parameters.AddWithValue("@Brochure", tblPatientsEducationBrochure.Brochure);            
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SavePatientsEducationBrochure"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SavePatientsEducationBrochure"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SavePatientsEducationBrochure"].ToString();
    }

    public DataTable GetPatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        dt = utility.Display("Execute Proc_Patients_Education_Brochure 'GET',0,'" + tblPatientsEducationBrochure.Pat_Id + "'");
        return dt;
    }

    public string DeletePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Patients_Education_Brochure"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@Broc_Id", tblPatientsEducationBrochure.Broc_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeletePatientsEducationBrochure"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeletePatientsEducationBrochure"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeletePatientsEducationBrochure"].ToString();
    }

    public DataTable GetBYPatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        dt = utility.Display("Execute Proc_Patients_Education_Brochure 'GET_BY_ID','" + tblPatientsEducationBrochure.Broc_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblPatientsEducationBrochure.Broc_Id = int.Parse(dt.Rows[0]["BROC_ID"].ToString());
            tblPatientsEducationBrochure.Brochure = dt.Rows[0]["BROCHURE"].ToString();           
        }
        return dt;
    }

    public string UpdatePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Patients_Education_Brochure"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@BROC_ID", tblPatientsEducationBrochure.Broc_Id);
            cmd.Parameters.AddWithValue("@BROCHURE", tblPatientsEducationBrochure.Brochure);            
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdatePatientsEducationBrochure"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdatePatientsEducationBrochure"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdatePatientsEducationBrochure"].ToString();
    }

    public DataTable GetTittlePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        dt = utility.Display("Execute Proc_Patients_Education_Brochure 'GET_TITTLE',0,'" + tblPatientsEducationBrochure.Pat_Id + "'");
        tblPatientsEducationBrochure.Tittle = dt.Rows[0]["TITTLE"].ToString();
        return dt;
    }

    public DataTable TopPatientsEducationBrochure()
    {
        dt = utility.Display("Execute Proc_Patients_Education_Brochure 'TOP_BROCHURE_TITTLE'");
        return dt;
    }

    public DataTable BroucherPatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        dt = utility.Display("Execute Proc_Patients_Education_Brochure 'GET_BROCHURE',0,'" + tblPatientsEducationBrochure.Pat_Id + "'");
        return dt;
    }

    #endregion

    #region Feedback InPatient

    public string SaveFeedbackInPatient(FeedBackInPatient tblFeedBackInPatient)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_FeedBackInPatient"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("FIRSTNAME", tblFeedBackInPatient.FirstName);
            cmd.Parameters.AddWithValue("LASTNAME", tblFeedBackInPatient.LastName);
            cmd.Parameters.AddWithValue("LH_NO", tblFeedBackInPatient.Lh_No);
            cmd.Parameters.AddWithValue("DATEOFADMISSION", tblFeedBackInPatient.DateOfAdmission);
            cmd.Parameters.AddWithValue("PHONENO", tblFeedBackInPatient.PhoneNo);
            cmd.Parameters.AddWithValue("BEDNO", tblFeedBackInPatient.BedNo);
            cmd.Parameters.AddWithValue("DATEOFDISCHARGE", tblFeedBackInPatient.DateOfDischarge);
            cmd.Parameters.AddWithValue("EMAILID", tblFeedBackInPatient.EmailId);
            cmd.Parameters.AddWithValue("HOWDIDYOUCOMETOKNOW", tblFeedBackInPatient.HowDidYouComeToKnow);
            cmd.Parameters.AddWithValue("GUIDANCESUPPORT", tblFeedBackInPatient.GuidanceSupport);
            cmd.Parameters.AddWithValue("COURTESYANDFRIENDLINESS", tblFeedBackInPatient.CourtesyAndFriendliness);
            cmd.Parameters.AddWithValue("TIMETAKENTOCOMPLETE", tblFeedBackInPatient.TimeTakenToComplete);
            cmd.Parameters.AddWithValue("CLARITYPROVIDED", tblFeedBackInPatient.ClarityProvided);
            cmd.Parameters.AddWithValue("CareAttention", tblFeedBackInPatient.CareAttention);
            cmd.Parameters.AddWithValue("DoctorsEfforts", tblFeedBackInPatient.DoctorsEfforts);
            cmd.Parameters.AddWithValue("CourtesyFriendnessNursing", tblFeedBackInPatient.CourtesyFriendnessNursing);
            cmd.Parameters.AddWithValue("CareAndAttentionMedication", tblFeedBackInPatient.CareAndAttentionMedication);
            cmd.Parameters.AddWithValue("ResponseToYou", tblFeedBackInPatient.ResponseToYou);
            cmd.Parameters.AddWithValue("RespectForPrivacy", tblFeedBackInPatient.RespectForPrivacy);
            //cmd.Parameters.AddWithValue("CountesyOfStaff", tblFeedBackInPatient.CountesyOfStaff);
            //cmd.Parameters.AddWithValue("ClarityOfInstructions", tblFeedBackInPatient.ClarityOfInstructions);
            cmd.Parameters.AddWithValue("QualityOfFood", tblFeedBackInPatient.QualityOfFood);
            cmd.Parameters.AddWithValue("VarietyOfFood", tblFeedBackInPatient.VarietyOfFood);
            cmd.Parameters.AddWithValue("PromptnessOfService", tblFeedBackInPatient.PromptnessOfService);
            cmd.Parameters.AddWithValue("InformationGuidance", tblFeedBackInPatient.InformationGuidance);
            cmd.Parameters.AddWithValue("CleanlinessOfRoom", tblFeedBackInPatient.CleanlinessOfRoom);
            cmd.Parameters.AddWithValue("ResponseToYouNeed", tblFeedBackInPatient.ResponseToYouNeed);
            cmd.Parameters.AddWithValue("CourtesyAndHelpfulness", tblFeedBackInPatient.CourtesyAndHelpfulness);
            cmd.Parameters.AddWithValue("CourtesyOfSecurity", tblFeedBackInPatient.CourtesyOfSecurity);
            cmd.Parameters.AddWithValue("Professionalism", tblFeedBackInPatient.Professionalism);
            cmd.Parameters.AddWithValue("BillingCounseling", tblFeedBackInPatient.BillingCounseling);
            cmd.Parameters.AddWithValue("PromptnessOfBilling", tblFeedBackInPatient.PromptnessOfBilling);
            cmd.Parameters.AddWithValue("CountesyHelpfulness", tblFeedBackInPatient.CountesyHelpfulness);
            cmd.Parameters.AddWithValue("PromptnessOfDischarge", tblFeedBackInPatient.PromptnessOfDischarge);
            cmd.Parameters.AddWithValue("DischargeCounseling", tblFeedBackInPatient.DischargeCounseling);
            cmd.Parameters.AddWithValue("ConvenienceOfDischarge", tblFeedBackInPatient.ConvenienceOfDischarge);
            cmd.Parameters.AddWithValue("HowWouldYouRate", tblFeedBackInPatient.HowWouldYouRate);
            cmd.Parameters.AddWithValue("WhatDidYouLike", tblFeedBackInPatient.WhatDidYouLike);
            cmd.Parameters.AddWithValue("WhatDidYouLikeLeast", tblFeedBackInPatient.WhatDidYouLikeLeast);
            cmd.Parameters.AddWithValue("AnysuggestionYouWouldLike", tblFeedBackInPatient.AnysuggestionYouWouldLike);
            cmd.Parameters.AddWithValue("WouldYouLikeToRecommend", tblFeedBackInPatient.WouldYouLikeToRecommend);

            cmd.Parameters.AddWithValue("Pathology_BloodCollection", tblFeedBackInPatient.Pathology_BloodCollection);
            cmd.Parameters.AddWithValue("Radiology_X_ray", tblFeedBackInPatient.Radiology_X_ray);
            cmd.Parameters.AddWithValue("Cardiology_ECG_Stress_test", tblFeedBackInPatient.Cardiology_ECG_Stress_test);
            cmd.Parameters.AddWithValue("Nuclear_Dept_BoneScanLiverScan", tblFeedBackInPatient.Nuclear_Dept_BoneScanLiverScan);
            cmd.Parameters.AddWithValue("TimetakenforTestRadiology_X_ray", tblFeedBackInPatient.TimetakenforTestRadiology_X_ray);
            cmd.Parameters.AddWithValue("TimetakenforTestCardiology_ECG_Stress_test", tblFeedBackInPatient.TimetakenforTestCardiology_ECG_Stress_test);
            cmd.Parameters.AddWithValue("TimetakenforTestNuclear_Dept_BoneScanLiverScan", tblFeedBackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan);

            cmd.Parameters.AddWithValue("TimetakenforTestPathology", tblFeedBackInPatient.TimetakenforTestPathology);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveFeedbackInPatient"] = "Success";

                FeedbackInPatient feedbackInPatient = new FeedbackInPatient();
                feedbackInPatient.EmailFeedbackInPatient(tblFeedBackInPatient.FirstName, tblFeedBackInPatient.LastName, tblFeedBackInPatient.Lh_No, tblFeedBackInPatient.BedNo, tblFeedBackInPatient.DateOfAdmission, tblFeedBackInPatient.DateOfDischarge, tblFeedBackInPatient.PhoneNo, tblFeedBackInPatient.EmailId, tblFeedBackInPatient.HowDidYouComeToKnow, tblFeedBackInPatient.GuidanceSupport, tblFeedBackInPatient.CourtesyAndFriendliness, tblFeedBackInPatient.TimeTakenToComplete, tblFeedBackInPatient.ClarityProvided, tblFeedBackInPatient.CareAttention, tblFeedBackInPatient.DoctorsEfforts, tblFeedBackInPatient.CourtesyFriendnessNursing, tblFeedBackInPatient.CareAndAttentionMedication, tblFeedBackInPatient.ResponseToYou, tblFeedBackInPatient.RespectForPrivacy, tblFeedBackInPatient.QualityOfFood, tblFeedBackInPatient.VarietyOfFood, tblFeedBackInPatient.PromptnessOfService, tblFeedBackInPatient.InformationGuidance, tblFeedBackInPatient.CleanlinessOfRoom, tblFeedBackInPatient.ResponseToYouNeed, tblFeedBackInPatient.CourtesyAndHelpfulness, tblFeedBackInPatient.CourtesyOfSecurity, tblFeedBackInPatient.Professionalism, tblFeedBackInPatient.BillingCounseling, tblFeedBackInPatient.PromptnessOfBilling, tblFeedBackInPatient.CountesyHelpfulness, tblFeedBackInPatient.PromptnessOfDischarge, tblFeedBackInPatient.DischargeCounseling, tblFeedBackInPatient.ConvenienceOfDischarge, tblFeedBackInPatient.HowWouldYouRate, tblFeedBackInPatient.WhatDidYouLike, tblFeedBackInPatient.WhatDidYouLikeLeast, tblFeedBackInPatient.AnysuggestionYouWouldLike, tblFeedBackInPatient.WouldYouLikeToRecommend, tblFeedBackInPatient.Pathology_BloodCollection, tblFeedBackInPatient.Radiology_X_ray, tblFeedBackInPatient.Cardiology_ECG_Stress_test, tblFeedBackInPatient.Nuclear_Dept_BoneScanLiverScan, tblFeedBackInPatient.TimetakenforTestRadiology_X_ray, tblFeedBackInPatient.TimetakenforTestCardiology_ECG_Stress_test, tblFeedBackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan, tblFeedBackInPatient.TimetakenforTestPathology);
            }
            else
            {
                HttpContext.Current.Session["SaveFeedbackInPatient"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveFeedbackInPatient"].ToString();
    }

    public DataTable GetFeedBackInPatient()
    {
        dt = utility.Display("Execute Proc_FeedBackInPatient 'GET'");
        return dt;
    }

    public string DeleteFeedBackInPatient(FeedBackInPatient tblFeedBackInPatient)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_FeedBackInPatient"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@FEED_ID", tblFeedBackInPatient.ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteFeedBackInPatient"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteFeedBackInPatient"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteFeedBackInPatient"].ToString();
    }

    public DataTable DetailsFeedBackInPatient(FeedBackInPatient tblFeedBackInPatient)
    {
        dt = utility.Display("Execute Proc_FeedBackInPatient 'GET_BY_ID','" + tblFeedBackInPatient.ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblFeedBackInPatient.FirstName = dt.Rows[0]["FirstName"].ToString();
            tblFeedBackInPatient.LastName = dt.Rows[0]["LastName"].ToString();
            tblFeedBackInPatient.Lh_No = dt.Rows[0]["Lh_No"].ToString();
            tblFeedBackInPatient.DateOfAdmission = dt.Rows[0]["DateOfAdmission"].ToString();
            tblFeedBackInPatient.PhoneNo = dt.Rows[0]["PhoneNo"].ToString();
            tblFeedBackInPatient.BedNo = dt.Rows[0]["BedNo"].ToString();
            tblFeedBackInPatient.DateOfDischarge = dt.Rows[0]["DateOfDischarge"].ToString();
            tblFeedBackInPatient.EmailId = dt.Rows[0]["EmailId"].ToString();
            tblFeedBackInPatient.HowDidYouComeToKnow = dt.Rows[0]["HowDidYouComeToKnow"].ToString();
            tblFeedBackInPatient.GuidanceSupport = dt.Rows[0]["GuidanceSupport"].ToString();
            tblFeedBackInPatient.CourtesyAndFriendliness = dt.Rows[0]["CourtesyAndFriendliness"].ToString();
            tblFeedBackInPatient.TimeTakenToComplete = dt.Rows[0]["TimeTakenToComplete"].ToString();
            tblFeedBackInPatient.ClarityProvided = dt.Rows[0]["ClarityProvided"].ToString();
            tblFeedBackInPatient.CareAttention = dt.Rows[0]["CareAttention"].ToString();
            tblFeedBackInPatient.DoctorsEfforts = dt.Rows[0]["DoctorsEfforts"].ToString();
            tblFeedBackInPatient.CourtesyFriendnessNursing = dt.Rows[0]["CourtesyFriendnessNursing"].ToString();
            tblFeedBackInPatient.CareAndAttentionMedication = dt.Rows[0]["CareAndAttentionMedication"].ToString();
            tblFeedBackInPatient.ResponseToYou = dt.Rows[0]["ResponseToYou"].ToString();
            tblFeedBackInPatient.RespectForPrivacy = dt.Rows[0]["RespectForPrivacy"].ToString();
            tblFeedBackInPatient.CountesyOfStaff = dt.Rows[0]["CountesyOfStaff"].ToString();
            tblFeedBackInPatient.ClarityOfInstructions = dt.Rows[0]["ClarityOfInstructions"].ToString();
            tblFeedBackInPatient.QualityOfFood = dt.Rows[0]["QualityOfFood"].ToString();
            tblFeedBackInPatient.VarietyOfFood = dt.Rows[0]["VarietyOfFood"].ToString();
            tblFeedBackInPatient.PromptnessOfService = dt.Rows[0]["PromptnessOfService"].ToString();
            tblFeedBackInPatient.InformationGuidance = dt.Rows[0]["InformationGuidance"].ToString();
            tblFeedBackInPatient.CleanlinessOfRoom = dt.Rows[0]["CleanlinessOfRoom"].ToString();
            tblFeedBackInPatient.ResponseToYouNeed = dt.Rows[0]["ResponseToYouNeed"].ToString();
            tblFeedBackInPatient.CourtesyAndHelpfulness = dt.Rows[0]["CourtesyAndHelpfulness"].ToString();
            tblFeedBackInPatient.CourtesyOfSecurity = dt.Rows[0]["CourtesyOfSecurity"].ToString();
            tblFeedBackInPatient.Professionalism = dt.Rows[0]["Professionalism"].ToString();
            tblFeedBackInPatient.BillingCounseling = dt.Rows[0]["BillingCounseling"].ToString();
            tblFeedBackInPatient.PromptnessOfBilling = dt.Rows[0]["PromptnessOfBilling"].ToString();
            tblFeedBackInPatient.CountesyHelpfulness = dt.Rows[0]["CountesyHelpfulness"].ToString();
            tblFeedBackInPatient.PromptnessOfDischarge = dt.Rows[0]["PromptnessOfDischarge"].ToString();
            tblFeedBackInPatient.DischargeCounseling = dt.Rows[0]["DischargeCounseling"].ToString();
            tblFeedBackInPatient.ConvenienceOfDischarge = dt.Rows[0]["ConvenienceOfDischarge"].ToString();
            tblFeedBackInPatient.HowWouldYouRate = dt.Rows[0]["HowWouldYouRate"].ToString();
            tblFeedBackInPatient.WhatDidYouLike = dt.Rows[0]["WhatDidYouLike"].ToString();
            tblFeedBackInPatient.WhatDidYouLikeLeast = dt.Rows[0]["WhatDidYouLikeLeast"].ToString();
            tblFeedBackInPatient.AnysuggestionYouWouldLike = dt.Rows[0]["AnysuggestionYouWouldLike"].ToString();
            tblFeedBackInPatient.WouldYouLikeToRecommend = dt.Rows[0]["WouldYouLikeToRecommend"].ToString();

            tblFeedBackInPatient.Pathology_BloodCollection = dt.Rows[0]["Pathology_BloodCollection"].ToString();
            tblFeedBackInPatient.Radiology_X_ray = dt.Rows[0]["Radiology_X_ray"].ToString();
            tblFeedBackInPatient.Cardiology_ECG_Stress_test = dt.Rows[0]["Cardiology_ECG_Stress_test"].ToString();
            tblFeedBackInPatient.Nuclear_Dept_BoneScanLiverScan = dt.Rows[0]["Nuclear_Dept_BoneScanLiverScan"].ToString();
            tblFeedBackInPatient.TimetakenforTestRadiology_X_ray = dt.Rows[0]["TimetakenforTestRadiology_X_ray"].ToString();
            tblFeedBackInPatient.TimetakenforTestCardiology_ECG_Stress_test = dt.Rows[0]["TimetakenforTestCardiology_ECG_Stress_test"].ToString();
            tblFeedBackInPatient.TimetakenforTestNuclear_Dept_BoneScanLiverScan = dt.Rows[0]["TimetakenforTestNuclear_Dept_BoneScanLiverScan"].ToString();

        }
        return dt;
    }

    #endregion

    #region Feedback OutPatient

    public string SaveFeedbackOutPatient(FeedbackOutPatient tblFeedbackOutPatient)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_FeedBack_OutPatient"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@FirstName", tblFeedbackOutPatient.FirstName);
            cmd.Parameters.AddWithValue("@LastName", tblFeedbackOutPatient.LastName);
            cmd.Parameters.AddWithValue("@Lh_No", tblFeedbackOutPatient.Lh_No);
            cmd.Parameters.AddWithValue("@Dates", tblFeedbackOutPatient.Dates);
            cmd.Parameters.AddWithValue("@NameOfDoctor", tblFeedbackOutPatient.NameOfDoctor);
            cmd.Parameters.AddWithValue("@PhoneNo", tblFeedbackOutPatient.PhoneNo);
            cmd.Parameters.AddWithValue("@EmailId", tblFeedbackOutPatient.EmailId);
            cmd.Parameters.AddWithValue("@HowDidYouComeToKnow", tblFeedbackOutPatient.HowDidYouComeToKnow);
            cmd.Parameters.AddWithValue("@TimeTakenByAppointment", tblFeedbackOutPatient.TimeTakenByAppointment);
            cmd.Parameters.AddWithValue("@GuidanceAndInformation", tblFeedbackOutPatient.GuidanceAndInformation);
            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessOPD", tblFeedbackOutPatient.CourtesyAndFriendlinessOPD);
            cmd.Parameters.AddWithValue("@TimeTakenToCompleteRegistration", tblFeedbackOutPatient.TimeTakenToCompleteRegistration);
            cmd.Parameters.AddWithValue("@TimeTakenToCompleteBilling", tblFeedbackOutPatient.TimeTakenToCompleteBilling);
            cmd.Parameters.AddWithValue("@CourtesyAndInformation", tblFeedbackOutPatient.CourtesyAndInformation);
            cmd.Parameters.AddWithValue("@WaitingTimeToSeeDoctor", tblFeedbackOutPatient.WaitingTimeToSeeDoctor);
            cmd.Parameters.AddWithValue("@ClarityAndInformation", tblFeedbackOutPatient.ClarityAndInformation);
            cmd.Parameters.AddWithValue("@CareAndAttention", tblFeedbackOutPatient.CareAndAttention);
            cmd.Parameters.AddWithValue("@BehaviorAndAttitude", tblFeedbackOutPatient.BehaviorAndAttitude);
            cmd.Parameters.AddWithValue("@RespectForYourPrivacy", tblFeedbackOutPatient.RespectForYourPrivacy);
            cmd.Parameters.AddWithValue("@CareOfferedDuringProc", tblFeedbackOutPatient.CareOfferedDuringProc);
            cmd.Parameters.AddWithValue("@WaitingTimeForTheTest", tblFeedbackOutPatient.WaitingTimeForTheTest);
            cmd.Parameters.AddWithValue("@ExperienceWithTechnical", tblFeedbackOutPatient.ExperienceWithTechnical);
            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessStaff", tblFeedbackOutPatient.CourtesyAndFriendlinessStaff);
            cmd.Parameters.AddWithValue("@ComfortOfWaitingArea", tblFeedbackOutPatient.ComfortOfWaitingArea);
            cmd.Parameters.AddWithValue("@CleanlinessInOPD", tblFeedbackOutPatient.CleanlinessInOPD);
            cmd.Parameters.AddWithValue("@CleanlinessOfToilets", tblFeedbackOutPatient.CleanlinessOfToilets);
            cmd.Parameters.AddWithValue("@HowWouldYouRate", tblFeedbackOutPatient.HowWouldYouRate);
            cmd.Parameters.AddWithValue("@WhatDidYouLIke", tblFeedbackOutPatient.WhatDidYouLIke);
            cmd.Parameters.AddWithValue("@WhatDidYouLikeLeast", tblFeedbackOutPatient.WhatDidYouLikeLeast);
            cmd.Parameters.AddWithValue("@AnysuggestionYouWouldLike", tblFeedbackOutPatient.AnysuggestionYouWouldLike);
            cmd.Parameters.AddWithValue("@DATE", tblFeedbackOutPatient.Date);

            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessStaffPathology", tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology);
            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessStaffRadiology", tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology);
            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessStaffCardiology", tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology);
            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessStaffNuclear", tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear);
            cmd.Parameters.AddWithValue("@CourtesyAndFriendlinessStaffOther", tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther);
            cmd.Parameters.AddWithValue("@Courtesyother", tblFeedbackOutPatient.Courtesyother);

            cmd.Parameters.AddWithValue("@WaitingTimeForTheTestPathology", tblFeedbackOutPatient.WaitingTimeForTheTestPathology);
            cmd.Parameters.AddWithValue("@WaitingTimeForTheTestRadiology", tblFeedbackOutPatient.WaitingTimeForTheTestRadiology);
            cmd.Parameters.AddWithValue("@WaitingTimeForTheTestCardiology", tblFeedbackOutPatient.WaitingTimeForTheTestCardiology);
            cmd.Parameters.AddWithValue("@WaitingTimeForTheTestNuclear", tblFeedbackOutPatient.WaitingTimeForTheTestNuclear);
            cmd.Parameters.AddWithValue("@WaitingTimeForTheTestOther", tblFeedbackOutPatient.WaitingTimeForTheTestOther);
            cmd.Parameters.AddWithValue("@WaitingTimeother", tblFeedbackOutPatient.WaitingTimeother);

            cmd.Parameters.AddWithValue("@Service", tblFeedbackOutPatient.Service);
            cmd.Parameters.AddWithValue("@ServiceReason", tblFeedbackOutPatient.ServiceReason);
            cmd.Parameters.AddWithValue("@Staff", tblFeedbackOutPatient.Staff);
            cmd.Parameters.AddWithValue("@AreaofWork", tblFeedbackOutPatient.AreaofWork);
            cmd.Parameters.AddWithValue("@AreaReason", tblFeedbackOutPatient.AreaReason);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveFeedbackOutPatient"] = "Success";

                /** Send Mail **/
                FeedbackOutPatients feedbackOutPatient = new FeedbackOutPatients();
                feedbackOutPatient.EmailFeedbackOutPatient(tblFeedbackOutPatient.FirstName, tblFeedbackOutPatient.LastName, tblFeedbackOutPatient.Lh_No, tblFeedbackOutPatient.NameOfDoctor, tblFeedbackOutPatient.PhoneNo, tblFeedbackOutPatient.EmailId, tblFeedbackOutPatient.HowDidYouComeToKnow, tblFeedbackOutPatient.TimeTakenByAppointment, tblFeedbackOutPatient.GuidanceAndInformation, tblFeedbackOutPatient.CourtesyAndFriendlinessOPD, tblFeedbackOutPatient.TimeTakenToCompleteRegistration, tblFeedbackOutPatient.TimeTakenToCompleteBilling, tblFeedbackOutPatient.CourtesyAndInformation, tblFeedbackOutPatient.WaitingTimeToSeeDoctor, tblFeedbackOutPatient.ClarityAndInformation, tblFeedbackOutPatient.CareAndAttention, tblFeedbackOutPatient.BehaviorAndAttitude, tblFeedbackOutPatient.RespectForYourPrivacy, tblFeedbackOutPatient.CareOfferedDuringProc, tblFeedbackOutPatient.WaitingTimeForTheTest, tblFeedbackOutPatient.ExperienceWithTechnical, tblFeedbackOutPatient.CourtesyAndFriendlinessStaff, tblFeedbackOutPatient.ComfortOfWaitingArea, tblFeedbackOutPatient.CleanlinessInOPD, tblFeedbackOutPatient.CleanlinessOfToilets, tblFeedbackOutPatient.HowWouldYouRate, tblFeedbackOutPatient.WhatDidYouLIke, tblFeedbackOutPatient.WhatDidYouLikeLeast, tblFeedbackOutPatient.AnysuggestionYouWouldLike, tblFeedbackOutPatient.Date, tblFeedbackOutPatient.CourtesyAndFriendlinessStaffPathology, tblFeedbackOutPatient.CourtesyAndFriendlinessStaffRadiology, tblFeedbackOutPatient.CourtesyAndFriendlinessStaffCardiology, tblFeedbackOutPatient.CourtesyAndFriendlinessStaffNuclear, tblFeedbackOutPatient.CourtesyAndFriendlinessStaffOther, tblFeedbackOutPatient.Courtesyother, tblFeedbackOutPatient.WaitingTimeForTheTestPathology, tblFeedbackOutPatient.WaitingTimeForTheTestRadiology, tblFeedbackOutPatient.WaitingTimeForTheTestCardiology, tblFeedbackOutPatient.WaitingTimeForTheTestNuclear, tblFeedbackOutPatient.WaitingTimeForTheTestOther, tblFeedbackOutPatient.WaitingTimeother, tblFeedbackOutPatient.Service, tblFeedbackOutPatient.ServiceReason, tblFeedbackOutPatient.Staff, tblFeedbackOutPatient.AreaofWork, tblFeedbackOutPatient.AreaReason);
            }
            else
            {
                HttpContext.Current.Session["SaveFeedbackOutPatient"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveFeedbackOutPatient"].ToString();
    }

    public DataTable GetFeedbackOutPatient()
    {
        dt = utility.Display("Execute Proc_FeedBack_OutPatient 'GET'");
        return dt;
    }

    public string DeleteFeedBackOutPatient(FeedbackOutPatient tblFeedbackOutPatient)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_FeedBack_OutPatient"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@Feed_Opd_Id", tblFeedbackOutPatient.Feed_Opd_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteFeedBackOutPatient"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteFeedBackOutPatient"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteFeedBackOutPatient"].ToString();
    }

    public DataTable DetailsFeedBackOutPatient(FeedbackOutPatient tblFeedbackOutPatient)
    {
        dt = utility.Display("Execute Proc_FeedBack_OutPatient 'GET_BY_ID','" + tblFeedbackOutPatient.Feed_Opd_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblFeedbackOutPatient.FirstName = dt.Rows[0]["FirstName"].ToString();
            tblFeedbackOutPatient.LastName = dt.Rows[0]["LastName"].ToString();
            tblFeedbackOutPatient.Lh_No = dt.Rows[0]["Lh_No"].ToString();            
            tblFeedbackOutPatient.PhoneNo = dt.Rows[0]["PhoneNo"].ToString();
            tblFeedbackOutPatient.NameOfDoctor = dt.Rows[0]["NameOfDoctor"].ToString();            
            tblFeedbackOutPatient.EmailId = dt.Rows[0]["EmailId"].ToString();
            tblFeedbackOutPatient.HowDidYouComeToKnow = dt.Rows[0]["HowDidYouComeToKnow"].ToString();
            tblFeedbackOutPatient.TimeTakenByAppointment = dt.Rows[0]["TimeTakenByAppointment"].ToString();
            tblFeedbackOutPatient.GuidanceAndInformation = dt.Rows[0]["GuidanceAndInformation"].ToString();
            tblFeedbackOutPatient.CourtesyAndFriendlinessOPD = dt.Rows[0]["CourtesyAndFriendlinessOPD"].ToString();
            tblFeedbackOutPatient.TimeTakenToCompleteRegistration = dt.Rows[0]["TimeTakenToCompleteRegistration"].ToString();
            tblFeedbackOutPatient.TimeTakenToCompleteBilling = dt.Rows[0]["TimeTakenToCompleteBilling"].ToString();
            tblFeedbackOutPatient.CourtesyAndInformation = dt.Rows[0]["CourtesyAndInformation"].ToString();
            tblFeedbackOutPatient.WaitingTimeToSeeDoctor = dt.Rows[0]["WaitingTimeToSeeDoctor"].ToString();
            tblFeedbackOutPatient.ClarityAndInformation = dt.Rows[0]["ClarityAndInformation"].ToString();
            tblFeedbackOutPatient.CareAndAttention = dt.Rows[0]["CareAndAttention"].ToString();
            tblFeedbackOutPatient.BehaviorAndAttitude = dt.Rows[0]["BehaviorAndAttitude"].ToString();
            tblFeedbackOutPatient.RespectForYourPrivacy = dt.Rows[0]["RespectForYourPrivacy"].ToString();
            tblFeedbackOutPatient.CareOfferedDuringProc = dt.Rows[0]["CareOfferedDuringProc"].ToString();
            tblFeedbackOutPatient.WaitingTimeForTheTest = dt.Rows[0]["WaitingTimeForTheTest"].ToString();
            tblFeedbackOutPatient.ExperienceWithTechnical = dt.Rows[0]["ExperienceWithTechnical"].ToString();
            tblFeedbackOutPatient.CourtesyAndFriendlinessStaff = dt.Rows[0]["CourtesyAndFriendlinessStaff"].ToString();
            tblFeedbackOutPatient.ComfortOfWaitingArea = dt.Rows[0]["ComfortOfWaitingArea"].ToString();
            tblFeedbackOutPatient.CleanlinessInOPD = dt.Rows[0]["CleanlinessInOPD"].ToString();
            tblFeedbackOutPatient.CleanlinessOfToilets = dt.Rows[0]["CleanlinessOfToilets"].ToString();
            tblFeedbackOutPatient.HowWouldYouRate = dt.Rows[0]["HowWouldYouRate"].ToString();
            tblFeedbackOutPatient.WhatDidYouLIke = dt.Rows[0]["WhatDidYouLIke"].ToString();
            tblFeedbackOutPatient.WhatDidYouLikeLeast = dt.Rows[0]["WhatDidYouLikeLeast"].ToString();
            tblFeedbackOutPatient.AnysuggestionYouWouldLike = dt.Rows[0]["AnysuggestionYouWouldLike"].ToString();
            tblFeedbackOutPatient.Date = dt.Rows[0]["Date"].ToString();
        }
        return dt;
    }

    #endregion

    #region Photo Gallery

    public string SavePhotoGallery(PhotoGallery tblPhotoGallery)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Photo_Gallery"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@IMAGE", tblPhotoGallery.Images);
            cmd.Parameters.AddWithValue("@TITTLE", tblPhotoGallery.Tittle);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SavePhotoGallery"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SavePhotoGallery"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SavePhotoGallery"].ToString();
    }

    public DataTable GetPhotoGallery()
    {
        dt = utility.Display("Execute Proc_Photo_Gallery 'GET'");
        return dt;
    }

    public string DeletePhotoGallery(PhotoGallery tblPhotoGallery)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Photo_Gallery"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", tblPhotoGallery.ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeletePhotoGallery"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeletePhotoGallery"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeletePhotoGallery"].ToString();
    }

    public DataTable GetBYPhotoGallery(PhotoGallery tblPhotoGallery)
    {
        dt = utility.Display("Execute Proc_Photo_Gallery 'GET_BY_ID','" + tblPhotoGallery.ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblPhotoGallery.ID = int.Parse(dt.Rows[0]["ID"].ToString());
            tblPhotoGallery.Images = dt.Rows[0]["IMAGE"].ToString();
            tblPhotoGallery.Tittle = dt.Rows[0]["TITTLE"].ToString();
        }
        return dt;
    }

    public string UpdatePhotoGallery(PhotoGallery tblPhotoGallery)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Photo_Gallery"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@ID", tblPhotoGallery.ID);
            cmd.Parameters.AddWithValue("@IMAGE", tblPhotoGallery.Images);
            cmd.Parameters.AddWithValue("@TITTLE", tblPhotoGallery.Tittle);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdatePhotoGallery"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdatePhotoGallery"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdatePhotoGallery"].ToString();
    }

    #endregion

    #region Home Banner

    public string SaveHomeBanner(HomeBanner tblHomeBanner)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@IMAGE", tblHomeBanner.Images);
            cmd.Parameters.AddWithValue("@ORDER_NO", tblHomeBanner.Order);
            cmd.Parameters.AddWithValue("@PAGE_URL", tblHomeBanner.PageUrl);
            cmd.Parameters.AddWithValue("@PUBLISH", tblHomeBanner.Publish);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveHomeBanner"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveHomeBanner"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveHomeBanner"].ToString();
    }

    public DataTable GetHomeBanner()
    {
        dt = utility.Display("Execute Proc_Banner 'GET'");
        return dt;
    }

    public DataTable AdminGetHomeBanner()
    {
        dt = utility.Display("Execute Proc_Banner 'ADMIN_GET'");
        return dt;
    }

    public string DeleteHomeBanner(HomeBanner tblHomeBanner)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", tblHomeBanner.ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteHomeBanner"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteHomeBanner"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteHomeBanner"].ToString();
    }

    public DataTable GetBYHomeBanner(HomeBanner tblHomeBanner)
    {
        dt = utility.Display("Execute Proc_Banner 'GET_BY_ID','" + tblHomeBanner.ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblHomeBanner.ID = int.Parse(dt.Rows[0]["ID"].ToString());
            tblHomeBanner.Images = dt.Rows[0]["IMAGE"].ToString();
            tblHomeBanner.Order = dt.Rows[0]["ORDER_NO"].ToString();
            tblHomeBanner.PageUrl = dt.Rows[0]["PAGE_URL"].ToString();
            tblHomeBanner.Publish = dt.Rows[0]["PUBLISH"].ToString();
        }
        return dt;
    }

    public string UpdateHomeBanner(HomeBanner tblHomeBanner)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@ID", tblHomeBanner.ID);
            cmd.Parameters.AddWithValue("@IMAGE", tblHomeBanner.Images);
            cmd.Parameters.AddWithValue("@ORDER_NO", tblHomeBanner.Order);
            cmd.Parameters.AddWithValue("@PAGE_URL", tblHomeBanner.PageUrl);
            cmd.Parameters.AddWithValue("@PUBLISH", tblHomeBanner.Publish);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateHomeBanner"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateHomeBanner"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateHomeBanner"].ToString();
    }

    public string ShowHomeBanner(HomeBanner tblHomeBanner)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "SHOW_BANNER");
            cmd.Parameters.AddWithValue("@ID", tblHomeBanner.ID);
            cmd.Parameters.AddWithValue("@PUBLISH", tblHomeBanner.Publish);           
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["ShowHomeBanner"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["ShowHomeBanner"] = "Failed";
            }
        }
        return HttpContext.Current.Session["ShowHomeBanner"].ToString();
    }

    public string HideHomeBanner(HomeBanner tblHomeBanner)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "HIDE_BANNER");
            cmd.Parameters.AddWithValue("@ID", tblHomeBanner.ID);
            cmd.Parameters.AddWithValue("@PUBLISH", tblHomeBanner.Publish);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["HideHomeBanner"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["HideHomeBanner"] = "Failed";
            }
        }
        return HttpContext.Current.Session["HideHomeBanner"].ToString();
    }

    #endregion

    #region Home Page

    public DataTable GetHomeTestimonial()
    {
        dt = utility.Display("Execute Proc_Testimonial 'GET_CELEBRITY_HOME'");
        return dt;
    }

    #endregion

    #region Updates Page

    public string SaveUpdatesPage(Updates tblUpdates)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Update_Page"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@UPD_TITTLE", tblUpdates.Tittle);
            cmd.Parameters.AddWithValue("@UPD_CONTENT", tblUpdates.Content);
            cmd.Parameters.AddWithValue("@UPD_IMAGES", tblUpdates.Images);
            cmd.Parameters.AddWithValue("@UPD_PDF", tblUpdates.Pdf);
            cmd.Parameters.AddWithValue("@UPD_DOC", tblUpdates.Doc);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveUpdatesPage"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveUpdatesPage"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveUpdatesPage"].ToString();
    }

    public DataTable GetUpdatesPage()
    {
        dt = utility.Display("Execute Proc_Update_Page 'GET' ");
        return dt;
    }

    public string DeleteUpdatesPage(Updates tblUpdates)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Update_Page"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@UPD_ID", tblUpdates.Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteUpdatesPage"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteUpdatesPage"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteUpdatesPage"].ToString();

    }

    public DataTable GetByUpdatesPage(Updates tblUpdates)
    {
        dt = utility.Display("Execute Proc_Update_Page 'GET_BY_ID','" + tblUpdates.Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblUpdates.Id = Convert.ToInt32(dt.Rows[0]["Upd_Id"].ToString());
            tblUpdates.Tittle = dt.Rows[0]["Upd_Tittle"].ToString();
            tblUpdates.Images = dt.Rows[0]["Upd_Images"].ToString();
            tblUpdates.Content = dt.Rows[0]["Upd_Content"].ToString();
            tblUpdates.Pdf = dt.Rows[0]["Upd_Pdf"].ToString();
            tblUpdates.Doc = dt.Rows[0]["Upd_Doc"].ToString();
        }

        return dt;
    }

    public string UpdateUpdatesPage(Updates tblUpdates)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Update_Page"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@UPD_ID", tblUpdates.Id);
            cmd.Parameters.AddWithValue("@UPD_TITTLE", tblUpdates.Tittle);
            cmd.Parameters.AddWithValue("@UPD_CONTENT", tblUpdates.Content);
            cmd.Parameters.AddWithValue("@UPD_IMAGES", tblUpdates.Images);
            cmd.Parameters.AddWithValue("@UPD_PDF", tblUpdates.Pdf);
            cmd.Parameters.AddWithValue("@UPD_DOC", tblUpdates.Doc);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateUpdatesPage"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateUpdatesPage"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateUpdatesPage"].ToString();
    }

    public DataTable GEtTopTEnUpdatesPage()
    {
        dt = utility.Display("Execute Proc_Update_Page 'GET_TOP_TEN'");
        return dt;
    }

    #endregion

    #region LHMT Subscribe

    public string SaveLHMTSubscribe(LHMTSubscribe tblLHMTSubscribe)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_LhmtSubscribe"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@NAME", tblLHMTSubscribe.Name);
            cmd.Parameters.AddWithValue("@EMAIL", tblLHMTSubscribe.Email);
            cmd.Parameters.AddWithValue("@CONTACT", tblLHMTSubscribe.Contact);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveLHMTSubscribe"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveLHMTSubscribe"] = "Failed";
            }
            return HttpContext.Current.Session["SaveLHMTSubscribe"].ToString();
        }
    }

    public DataTable GetLHMTSubscribe()
    {
        dt = utility.Display("Execute Proc_LhmtSubscribe 'GET'");
        return dt;
    }

    public string DeleteLHMTSubscribe(LHMTSubscribe tblLHMTSubscribe)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_LhmtSubscribe"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", tblLHMTSubscribe.Id);            

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteLHMTSubscribe"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteLHMTSubscribe"] = "Failed";
            }
            return HttpContext.Current.Session["DeleteLHMTSubscribe"].ToString();
        }
    }

    #endregion

    #region Feedback HealthCheckUp

    public string SaveFeedBackHealthCheckUp(FeedBackHealthCheckUp tblFeedBackHealthCheckUp)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_FeedBackHealthCheckUp"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@FIRSTNAME", tblFeedBackHealthCheckUp.FirstName);
            cmd.Parameters.AddWithValue("@LastName", tblFeedBackHealthCheckUp.LastName);
            cmd.Parameters.AddWithValue("@LHNO", tblFeedBackHealthCheckUp.Lh_No);
            cmd.Parameters.AddWithValue("@Date", tblFeedBackHealthCheckUp.Date);
            cmd.Parameters.AddWithValue("@ContactNo", tblFeedBackHealthCheckUp.Contact);           
            cmd.Parameters.AddWithValue("@EMAILID", tblFeedBackHealthCheckUp.EmailId);
            cmd.Parameters.AddWithValue("@HealthCheckup", tblFeedBackHealthCheckUp.HealthCheckup);
            cmd.Parameters.AddWithValue("@HowDidYouCome", tblFeedBackHealthCheckUp.HowDidYouComeToKnow);

            cmd.Parameters.AddWithValue("@AppointmentHowwouldyourate", tblFeedBackHealthCheckUp.AppointmentHowwouldyourate);
            cmd.Parameters.AddWithValue("@RegistrationHowwasyourdocument", tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument);
            cmd.Parameters.AddWithValue("@TestsPathologyBlood", tblFeedBackHealthCheckUp.TestsPathologyBlood);
            cmd.Parameters.AddWithValue("@TestsRadiology_Xray", tblFeedBackHealthCheckUp.TestsRadiology_Xray);
            cmd.Parameters.AddWithValue("@TestsCardiologyECG", tblFeedBackHealthCheckUp.TestsCardiologyECG);
            cmd.Parameters.AddWithValue("@TestsSpirometry", tblFeedBackHealthCheckUp.TestsSpirometry);

            cmd.Parameters.AddWithValue("@ConsultationGeneralPhysician", tblFeedBackHealthCheckUp.ConsultationGeneralPhysician);
            cmd.Parameters.AddWithValue("@ConsultationGynaec", tblFeedBackHealthCheckUp.ConsultationGynaec);
            cmd.Parameters.AddWithValue("@ConsultationOphthalmology", tblFeedBackHealthCheckUp.ConsultationOphthalmology);
            cmd.Parameters.AddWithValue("@ConsultationENT", tblFeedBackHealthCheckUp.ConsultationENT);
            cmd.Parameters.AddWithValue("@ConsultationDental", tblFeedBackHealthCheckUp.ConsultationDental);
            cmd.Parameters.AddWithValue("@ConsultationSkin", tblFeedBackHealthCheckUp.ConsultationSkin);
            cmd.Parameters.AddWithValue("@ConsultationDietition", tblFeedBackHealthCheckUp.ConsultationDietition);

            cmd.Parameters.AddWithValue("@HousekeepingOverallhygiene", tblFeedBackHealthCheckUp.HousekeepingOverallhygiene);
            cmd.Parameters.AddWithValue("@Staff", tblFeedBackHealthCheckUp.Staff);
            cmd.Parameters.AddWithValue("@FoodBeverages", tblFeedBackHealthCheckUp.FoodBeverages);
            cmd.Parameters.AddWithValue("@Respectforyour", tblFeedBackHealthCheckUp.Respectforyour);
            cmd.Parameters.AddWithValue("@Timetakenforhealthcheck", tblFeedBackHealthCheckUp.Timetakenforhealthcheck);
            cmd.Parameters.AddWithValue("@Howwouldyourate", tblFeedBackHealthCheckUp.Howwouldyourate);
            cmd.Parameters.AddWithValue("@WhatdidYouLike", tblFeedBackHealthCheckUp.WhatdidYouLike);
            cmd.Parameters.AddWithValue("@WhatDidYouLikeLeast", tblFeedBackHealthCheckUp.WhatDidYouLikeLeast);
            cmd.Parameters.AddWithValue("@AnySuggestionYou", tblFeedBackHealthCheckUp.AnySuggestionYou);
            cmd.Parameters.AddWithValue("@WouldYouLikeTOMention", tblFeedBackHealthCheckUp.WouldYouLikeTOMention);

            cmd.Parameters.AddWithValue("@Service", tblFeedBackHealthCheckUp.Service);
            cmd.Parameters.AddWithValue("@ServiceReason", tblFeedBackHealthCheckUp.ServiceReason);
            cmd.Parameters.AddWithValue("@StaffPerformance", tblFeedBackHealthCheckUp.StaffPerformance);
            cmd.Parameters.AddWithValue("@AreaofWork", tblFeedBackHealthCheckUp.AreaofWork);
            cmd.Parameters.AddWithValue("@AreaReason", tblFeedBackHealthCheckUp.AreaReason);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SavetblFeedBackHealthCheckUp"] = "Success";

                FeedbackHealthCheckup feedbackHealthCheckup = new FeedbackHealthCheckup();
                feedbackHealthCheckup.EmailFeedbackHealthCheckup(tblFeedBackHealthCheckUp.FirstName, tblFeedBackHealthCheckUp.LastName, tblFeedBackHealthCheckUp.Lh_No, tblFeedBackHealthCheckUp.Date, tblFeedBackHealthCheckUp.Contact, tblFeedBackHealthCheckUp.EmailId, tblFeedBackHealthCheckUp.HealthCheckup, tblFeedBackHealthCheckUp.HowDidYouComeToKnow, tblFeedBackHealthCheckUp.AppointmentHowwouldyourate, tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument, tblFeedBackHealthCheckUp.TestsPathologyBlood, tblFeedBackHealthCheckUp.TestsRadiology_Xray, tblFeedBackHealthCheckUp.TestsCardiologyECG, tblFeedBackHealthCheckUp.TestsSpirometry, tblFeedBackHealthCheckUp.ConsultationGeneralPhysician, tblFeedBackHealthCheckUp.ConsultationGynaec, tblFeedBackHealthCheckUp.ConsultationOphthalmology, tblFeedBackHealthCheckUp.ConsultationENT, tblFeedBackHealthCheckUp.ConsultationDental, tblFeedBackHealthCheckUp._ConsultationSkin, tblFeedBackHealthCheckUp.ConsultationDietition, tblFeedBackHealthCheckUp.HousekeepingOverallhygiene, tblFeedBackHealthCheckUp.Staff, tblFeedBackHealthCheckUp.FoodBeverages, tblFeedBackHealthCheckUp.Respectforyour, tblFeedBackHealthCheckUp.Timetakenforhealthcheck, tblFeedBackHealthCheckUp.Howwouldyourate, tblFeedBackHealthCheckUp.WhatdidYouLike, tblFeedBackHealthCheckUp.WhatDidYouLikeLeast, tblFeedBackHealthCheckUp.AnySuggestionYou, tblFeedBackHealthCheckUp.WouldYouLikeTOMention, tblFeedBackHealthCheckUp.Service, tblFeedBackHealthCheckUp.ServiceReason, tblFeedBackHealthCheckUp.StaffPerformance, tblFeedBackHealthCheckUp.AreaofWork, tblFeedBackHealthCheckUp.AreaReason);
            }
            else
            {
                HttpContext.Current.Session["SavetblFeedBackHealthCheckUp"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SavetblFeedBackHealthCheckUp"].ToString();
    }

    public DataTable GetFeedBackHealthCheckUp()
    {
        dt = utility.Display("Execute Proc_FeedBackHealthCheckUp 'GET'");
        return dt;
    }

    public string DeleteFeedBackHealthCheckUp(FeedBackHealthCheckUp tblFeedBackHealthCheckUp)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_FeedBackHealthCheckUp"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", tblFeedBackHealthCheckUp.ID);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteFeedBackHealthCheckUp"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteFeedBackHealthCheckUp"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteFeedBackHealthCheckUp"].ToString();
    }

    public DataTable DetailsFeedBackHealthCheckUp(FeedBackHealthCheckUp tblFeedBackHealthCheckUp)
    {
        dt = utility.Display("Execute Proc_FeedBackHealthCheckUp 'GET_BY_ID','" + tblFeedBackHealthCheckUp.ID + "'");
        if (dt.Rows.Count > 0)
        {
            tblFeedBackHealthCheckUp.FirstName = dt.Rows[0]["FirstName"].ToString();
            tblFeedBackHealthCheckUp.LastName = dt.Rows[0]["LastName"].ToString();
            tblFeedBackHealthCheckUp.Lh_No = dt.Rows[0]["LHNO"].ToString();
            tblFeedBackHealthCheckUp.Date = dt.Rows[0]["Date"].ToString();
            tblFeedBackHealthCheckUp.Contact = dt.Rows[0]["ContactNo"].ToString();
            tblFeedBackHealthCheckUp.EmailId = dt.Rows[0]["EmailID"].ToString();
            tblFeedBackHealthCheckUp.HealthCheckup = dt.Rows[0]["HealthCheckup"].ToString();

            tblFeedBackHealthCheckUp.HowDidYouComeToKnow = dt.Rows[0]["HowDidYouCome"].ToString();
            tblFeedBackHealthCheckUp.AppointmentHowwouldyourate = dt.Rows[0]["AppointmentHowwouldyourate"].ToString();
            tblFeedBackHealthCheckUp.RegistrationHowwasyourdocument = dt.Rows[0]["RegistrationHowwasyourdocument"].ToString();
            tblFeedBackHealthCheckUp.TestsPathologyBlood = dt.Rows[0]["TestsPathologyBlood"].ToString();
            tblFeedBackHealthCheckUp.TestsRadiology_Xray = dt.Rows[0]["TestsRadiology_Xray"].ToString();
            tblFeedBackHealthCheckUp.TestsCardiologyECG = dt.Rows[0]["TestsCardiologyECG"].ToString();
            tblFeedBackHealthCheckUp.TestsSpirometry = dt.Rows[0]["TestsSpirometry"].ToString();

            tblFeedBackHealthCheckUp.ConsultationGeneralPhysician = dt.Rows[0]["ConsultationGeneralPhysician"].ToString();
            tblFeedBackHealthCheckUp.ConsultationGynaec = dt.Rows[0]["ConsultationGynaec"].ToString();
            tblFeedBackHealthCheckUp.ConsultationOphthalmology = dt.Rows[0]["ConsultationOphthalmology"].ToString();
            tblFeedBackHealthCheckUp.ConsultationENT = dt.Rows[0]["ConsultationENT"].ToString();
            tblFeedBackHealthCheckUp.ConsultationDental = dt.Rows[0]["ConsultationDental"].ToString();
            tblFeedBackHealthCheckUp.ConsultationSkin = dt.Rows[0]["ConsultationSkin"].ToString();
            tblFeedBackHealthCheckUp.ConsultationDietition = dt.Rows[0]["ConsultationDietition"].ToString();

            tblFeedBackHealthCheckUp.HousekeepingOverallhygiene = dt.Rows[0]["HousekeepingOverallhygiene"].ToString();
            tblFeedBackHealthCheckUp.Staff = dt.Rows[0]["Staff"].ToString();
            tblFeedBackHealthCheckUp.FoodBeverages = dt.Rows[0]["FoodBeverages"].ToString();
            tblFeedBackHealthCheckUp.Respectforyour = dt.Rows[0]["Respectforyour"].ToString();
            tblFeedBackHealthCheckUp.Timetakenforhealthcheck = dt.Rows[0]["Timetakenforhealthcheck"].ToString();
            tblFeedBackHealthCheckUp.Howwouldyourate = dt.Rows[0]["Howwouldyourate"].ToString();
            tblFeedBackHealthCheckUp.WhatdidYouLike = dt.Rows[0]["WhatdidYouLike"].ToString();
            tblFeedBackHealthCheckUp.WhatDidYouLikeLeast = dt.Rows[0]["WhatDidYouLikeLeast"].ToString();
            tblFeedBackHealthCheckUp.AnySuggestionYou = dt.Rows[0]["AnySuggestionYou"].ToString();
            tblFeedBackHealthCheckUp.WouldYouLikeTOMention = dt.Rows[0]["WouldYouLikeTOMention"].ToString();
             
        }
        return dt;
    }

    #endregion

    #region Visa Invitation

    public string SaveVisaInvitation(VisaInvitation tblVisaInvitation)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Visa_Invitation"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@PatientName", tblVisaInvitation.PatientName);
            cmd.Parameters.AddWithValue("@EmailID", tblVisaInvitation.EmailID);
            cmd.Parameters.AddWithValue("@ContactNo", tblVisaInvitation.ContactNo);
            cmd.Parameters.AddWithValue("@Nationality", tblVisaInvitation.Nationality);
            cmd.Parameters.AddWithValue("@PassportNo", tblVisaInvitation.PassportNo);
            cmd.Parameters.AddWithValue("@PatientPasswordCopy", tblVisaInvitation.PatientPasswordCopy);
            cmd.Parameters.AddWithValue("@HospitalName", tblVisaInvitation.HospitalName);
            cmd.Parameters.AddWithValue("@TreatmentDocName", tblVisaInvitation.TreatmentDocName);
            cmd.Parameters.AddWithValue("@Treatment", tblVisaInvitation.Treatment);
            cmd.Parameters.AddWithValue("@ProvisionalDiagnosis", tblVisaInvitation.ProvisionalDiagnosis);
            cmd.Parameters.AddWithValue("@PurposeOfVisit", tblVisaInvitation.PurposeOfVisit);
            cmd.Parameters.AddWithValue("@LilavatiDoctorName", tblVisaInvitation.LilavatiDoctorName);
            cmd.Parameters.AddWithValue("@PreferredDate", tblVisaInvitation.PreferredDate);
            cmd.Parameters.AddWithValue("@DurationOfTreatment", tblVisaInvitation.DurationOfTreatment);
            cmd.Parameters.AddWithValue("@ComingForFollowup", tblVisaInvitation.ComingForFollowup);
            cmd.Parameters.AddWithValue("@DateOfLastVisit", tblVisaInvitation.DateOfLastVisit);
            cmd.Parameters.AddWithValue("@NoOfAttendant", tblVisaInvitation.NoOfAttendant);
            cmd.Parameters.AddWithValue("@PleaseSpecifyTheReason", tblVisaInvitation.PleaseSpecifyTheReason);
            cmd.Parameters.AddWithValue("@FirstAttName", tblVisaInvitation.FirstAttName);
            cmd.Parameters.AddWithValue("@FirstRelationship", tblVisaInvitation.FirstRelationship);
            cmd.Parameters.AddWithValue("@FirstPassportNo", tblVisaInvitation.FirstPassportNo);
            cmd.Parameters.AddWithValue("@FirstFileUploadCopy", tblVisaInvitation.FirstFileUploadCopy);
            cmd.Parameters.AddWithValue("@SecondAttName", tblVisaInvitation.SecondAttName);
            cmd.Parameters.AddWithValue("@SecondRelationship", tblVisaInvitation.SecondRelationship);
            cmd.Parameters.AddWithValue("@SecondPassportNo", tblVisaInvitation.SecondPassportNo);
            cmd.Parameters.AddWithValue("@SecondFileUploadCopy", tblVisaInvitation.SecondFileUploadCopy);
            cmd.Parameters.AddWithValue("@ThirdAttName", tblVisaInvitation.ThirdAttName);
            cmd.Parameters.AddWithValue("@ThirdRelationship", tblVisaInvitation.ThirdRelationship);
            cmd.Parameters.AddWithValue("@ThirdPassportNo", tblVisaInvitation.ThirdPassportNo);
            cmd.Parameters.AddWithValue("@ThirdFileUploadCopy", tblVisaInvitation.ThirdFileUploadCopy);
            cmd.Parameters.AddWithValue("@FourthAttName", tblVisaInvitation.FourthAttName);
            cmd.Parameters.AddWithValue("@FourthRelationship", tblVisaInvitation.FourthRelationship);
            cmd.Parameters.AddWithValue("@FourthPassportNo", tblVisaInvitation.FourthPassportNo);
            cmd.Parameters.AddWithValue("@FourthFileUploadCopy", tblVisaInvitation.FourthFileUploadCopy);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveVisaInvitation"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveVisaInvitation"] = "Success";
            }
            return HttpContext.Current.Session["SaveVisaInvitation"].ToString();
        }
    }

    public DataTable GetVisaInvitation()
    {
        dt = utility.Display("Execute Proc_Visa_Invitation 'GET'");
        return dt;
    }

    public string DeleteVisaInvitation(VisaInvitation tblVisaInvitation)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Visa_Invitation"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@VISA_ID", tblVisaInvitation.Visa_Id);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteVisaInvitation"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteVisaInvitation"] = "Success";
            }
            return HttpContext.Current.Session["DeleteVisaInvitation"].ToString();
        }
    }

    public DataTable GetByIdVisaInvitation(VisaInvitation tblVisaInvitation)
    {
        dt = utility.Display("Execute Proc_Visa_Invitation 'GET_BY_ID','" + tblVisaInvitation.Visa_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblVisaInvitation.PatientName = dt.Rows[0]["PatientName"].ToString();
            tblVisaInvitation.EmailID = dt.Rows[0]["EmailID"].ToString();
            tblVisaInvitation.ContactNo = dt.Rows[0]["ContactNo"].ToString();
            tblVisaInvitation.Nationality = dt.Rows[0]["Nationality"].ToString();
            tblVisaInvitation.PassportNo = dt.Rows[0]["PassportNo"].ToString();
            tblVisaInvitation.PatientPasswordCopy = dt.Rows[0]["PatientPasswordCopy"].ToString();
            tblVisaInvitation.HospitalName = dt.Rows[0]["HospitalName"].ToString();
            tblVisaInvitation.TreatmentDocName = dt.Rows[0]["TreatmentDocName"].ToString();
            tblVisaInvitation.Treatment = dt.Rows[0]["Treatment"].ToString();
            tblVisaInvitation.ProvisionalDiagnosis = dt.Rows[0]["ProvisionalDiagnosis"].ToString();
            tblVisaInvitation.PurposeOfVisit = dt.Rows[0]["PurposeOfVisit"].ToString();
            tblVisaInvitation.LilavatiDoctorName = dt.Rows[0]["LilavatiDoctorName"].ToString();
            tblVisaInvitation.PreferredDate = dt.Rows[0]["PreferredDate"].ToString();
            tblVisaInvitation.DurationOfTreatment = dt.Rows[0]["DurationOfTreatment"].ToString();
            tblVisaInvitation.ComingForFollowup = dt.Rows[0]["ComingForFollowup"].ToString();
            tblVisaInvitation.DateOfLastVisit = dt.Rows[0]["DateOfLastVisit"].ToString();
            tblVisaInvitation.NoOfAttendant = dt.Rows[0]["NoOfAttendant"].ToString();
            tblVisaInvitation.PleaseSpecifyTheReason = dt.Rows[0]["PleaseSpecifyTheReason"].ToString();
            tblVisaInvitation.FirstAttName = dt.Rows[0]["FirstAttName"].ToString();
            tblVisaInvitation.FirstRelationship = dt.Rows[0]["FirstRelationship"].ToString();
            tblVisaInvitation.FirstPassportNo = dt.Rows[0]["FirstPassportNo"].ToString();
            tblVisaInvitation.FirstFileUploadCopy = dt.Rows[0]["FirstFileUploadCopy"].ToString();
            tblVisaInvitation.SecondAttName = dt.Rows[0]["SecondAttName"].ToString();
            tblVisaInvitation.SecondRelationship = dt.Rows[0]["SecondRelationship"].ToString();
            tblVisaInvitation.SecondPassportNo = dt.Rows[0]["SecondPassportNo"].ToString();
            tblVisaInvitation.SecondFileUploadCopy = dt.Rows[0]["SecondFileUploadCopy"].ToString();
            tblVisaInvitation.ThirdAttName = dt.Rows[0]["ThirdAttName"].ToString();
            tblVisaInvitation.ThirdRelationship = dt.Rows[0]["ThirdRelationship"].ToString();
            tblVisaInvitation.ThirdPassportNo = dt.Rows[0]["ThirdPassportNo"].ToString();
            tblVisaInvitation.ThirdFileUploadCopy = dt.Rows[0]["ThirdFileUploadCopy"].ToString();
            tblVisaInvitation.FourthAttName = dt.Rows[0]["FourthAttName"].ToString();
            tblVisaInvitation.FourthRelationship = dt.Rows[0]["FourthRelationship"].ToString();
            tblVisaInvitation.FourthPassportNo = dt.Rows[0]["FourthPassportNo"].ToString();
            tblVisaInvitation.FourthFileUploadCopy = dt.Rows[0]["FourthFileUploadCopy"].ToString();
        }
        return dt;
    }

    #endregion

    #region CME Section

    public string SaveCME(CME tblCME)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CME"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@EVENTNAME", tblCME.EventName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveCME"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveCME"] = "Failed";
            }
        }

        return HttpContext.Current.Session["SaveCME"].ToString();
    }

    public DataTable GetCME()
    {
        dt = utility.Display("Execute Proc_CME 'GET'");
        return dt;
    }

    public string DeleteCME(CME tblCME)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CME"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@CME_ID", tblCME.Cme_Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteCME"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteCME"] = "Failed";
            }
        }

        return HttpContext.Current.Session["DeleteCME"].ToString();
    }

    public DataTable GetByIdCME(CME tblCME)
    {
        dt = utility.Display("Execute Proc_CME 'GET_BY_ID','" + tblCME.Cme_Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblCME.Cme_Id = Convert.ToInt32(dt.Rows[0]["CME_ID"].ToString());
            tblCME.EventName = dt.Rows[0]["EVENTNAME"].ToString();
        }
        return dt;
    }

    public string UpdateCME(CME tblCME)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CME"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@CME_ID", tblCME.Cme_Id);
            cmd.Parameters.AddWithValue("@EVENTNAME", tblCME.EventName);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateCME"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateCME"] = "Failed";
            }
        }

        return HttpContext.Current.Session["UpdateCME"].ToString();
    }

    #endregion

    #region CMEDetails

    public string SaveCMEDetails(CMEDetails tblCMEDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CME_details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@CME_ID", tblCMEDetails.Cme_Id);
            cmd.Parameters.AddWithValue("@DATEANDMONTH", tblCMEDetails.DateAndMonth);
            cmd.Parameters.AddWithValue("@TOPIC", tblCMEDetails.Topic);
            cmd.Parameters.AddWithValue("@DEPARTMENT", tblCMEDetails.Department);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["SaveCMEDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["SaveCMEDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["SaveCMEDetails"].ToString();
    }

    public DataTable GetCMEDetails(CMEDetails tblCMEDetails)
    {      
        //dt = utility.Display("Execute Proc_CME_details 'GET',0,'" + tblCMEDetails.Cme_Id + "'");
        if (tblCMEDetails.Cme_Id == 1)
        {
            dt = utility.Display("Execute Proc_CME_details 'BEFORE_GETDATE'");
        }
        if (tblCMEDetails.Cme_Id == 2)
        {
            dt = utility.Display("Execute Proc_CME_details 'AFTER_GETDATE'");
        }
        return dt;
    }

    public DataTable HeadingCMEDetails(CMEDetails tblCMEDetails)
    {
        dt = utility.Display("Execute Proc_CME_details 'HEADING',0,'" + tblCMEDetails.Cme_Id + "'");
        return dt;
    }

    public string DeleteCMEDetails(CMEDetails tblCMEDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CME_details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@ID", tblCMEDetails.Id);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["DeleteCMEDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["DeleteCMEDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["DeleteCMEDetails"].ToString();
    }

    public DataTable GetBYIdCMEDetails(CMEDetails tblCMEDetails)
    {
        dt = utility.Display("Execute Proc_CME_details 'GET_BY_ID','" + tblCMEDetails.Id + "'");
        if (dt.Rows.Count > 0)
        {
            tblCMEDetails.Id = Convert.ToInt32(dt.Rows[0]["ID"].ToString());
            tblCMEDetails.DateAndMonth = dt.Rows[0]["DATEANDMONTH"].ToString();
            tblCMEDetails.Topic = dt.Rows[0]["TOPIC"].ToString();
            tblCMEDetails.Department = dt.Rows[0]["DEPARTMENT"].ToString();
        }
        return dt;
    }

    public string UpdateCMEDetails(CMEDetails tblCMEDetails)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CME_details"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@ID", tblCMEDetails.Id);
            cmd.Parameters.AddWithValue("@DATEANDMONTH", tblCMEDetails.DateAndMonth);
            cmd.Parameters.AddWithValue("@TOPIC", tblCMEDetails.Topic);
            cmd.Parameters.AddWithValue("@DEPARTMENT", tblCMEDetails.Department);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["UpdateCMEDetails"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["UpdateCMEDetails"] = "Failed";
            }
        }
        return HttpContext.Current.Session["UpdateCMEDetails"].ToString();
    }

    #endregion

    #region CME Front

    public DataTable CMEEventName()
    {
        dt = utility.Display("Execute Proc_CME_details 'EVENTNAME'");
        return dt;
    }

    public DataTable CMEEventDetails(CMEDetails tblCMEDetails)
    {
        dt = utility.Display("Execute Proc_CME_details 'EVENT_DETAILS',0,'" + tblCMEDetails.Cme_Id + "'");
        return dt;
    }

    #endregion
}