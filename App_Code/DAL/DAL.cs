﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;

/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
    BEL bel = new BEL();
    Utility utility = new Utility();
    DataTable dt = new DataTable();
    UserActivation usertbl = new UserActivation();
    public DAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string SaveData(RegistrationTable tbl)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster", con))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@LASTNAME", tbl.SureName);
            cmd.Parameters.AddWithValue("@FIRSTNAME", tbl.FirstName);
            cmd.Parameters.AddWithValue("@MIDDLENAME", tbl.MiddleName);
            cmd.Parameters.AddWithValue("@SEX", tbl.Sex);
            cmd.Parameters.AddWithValue("@DATEOFBIRTH", tbl.DOB);
            cmd.Parameters.AddWithValue("@MARITALSTATUS", tbl.MaritalStatus);
            cmd.Parameters.AddWithValue("@RELIGION", tbl.Religion);
            cmd.Parameters.AddWithValue("@OCCUPATION", tbl.Occupation);
            cmd.Parameters.AddWithValue("@RESIDENCE", tbl.TelephoneResidence);
            cmd.Parameters.AddWithValue("@ADDRESS", tbl.Address);
            cmd.Parameters.AddWithValue("@MOBILENO", tbl.MobileNo);
            cmd.Parameters.AddWithValue("@EMAIL", tbl.EmailID);
            cmd.Parameters.AddWithValue("@PASSWORD", tbl.Password);
            cmd.Parameters.AddWithValue("@CITY", tbl.City);
            cmd.Parameters.AddWithValue("@STATE", tbl.State);
            cmd.Parameters.AddWithValue("@COUNTRY", tbl.Country);
            cmd.Parameters.AddWithValue("@PINCODE", tbl.Pincode);
            cmd.Parameters.AddWithValue("@LHNO", tbl.TLHNO);
            cmd.Parameters.AddWithValue("@UserName", tbl.UserName);
            cmd.Parameters.AddWithValue("@TITLE", tbl.Title);
            cmd.Parameters.AddWithValue("@BloodGroup", tbl.BloodGroup);
            cmd.Parameters.AddWithValue("@LH_NO", tbl.OriginalLHNO);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            //if (utility.ExecuteOnlineLHRC(cmd))
            if (dt.Rows.Count > 0)
            {
                HttpContext.Current.Session["msg"] = "Success";

                string AddressId = dt.Rows[0]["ADDRESSINDEX"].ToString();
                AddUserActivation(AddressId);

                /*** SMS Verification ***/
                UserSMSVerificaton(tbl.MobileNo, tbl.UserName, tbl.Password, usertbl.ActivationCode);
                /*** SMS Verification End ***/

                /*** Email Send ***/
                utility.SendEmail(tbl.EmailID, tbl.Password, usertbl.ActivationCode, tbl.TLHNO, tbl.FirstName, tbl.UserName);
                /*** End ***/
            }
            else
            {
                HttpContext.Current.Session["msg"] = "failed";
            }
        }
        return HttpContext.Current.Session["msg"].ToString();
    }

    public void UserSMSVerificaton(string strMobileNo, string strUserId, string strPwd, string strCode)
    {
        try
        {
            string TO = strMobileNo;
            string username = "9920874020";
            string password = "jddgd";
            string msg = "UserID : " + strUserId + "\n" + "Pwd : " + strPwd + "\n" + "Pls click http://lilavatihospital.com/Act?Ac=" + strCode + "\n" + "to activate your account.";

            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create("http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=341739&username=" + username + "&password=" + password + "&To=" + TO + "&Text=" + msg);
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();
            //this.Title = "Sms Send SuccessFully";
        }
        catch (Exception ex)
        {
            //this.Title = ex.Message;           
        }
    }

    public string AddUserActivation(string AddressID)
    {
        string ActivationCode = Guid.NewGuid().ToString();
        usertbl.ActivationCode = ActivationCode;

        using (SqlCommand cmd = new SqlCommand("Proc_UserActivation"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@ACTIVATIONCODE", usertbl.ActivationCode);
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", AddressID);
            if (utility.ExecuteOnlineLHRC(cmd))
            {
                HttpContext.Current.Session["Usermsg"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["Usermsg"] = "Failed";
            }
        }
        return HttpContext.Current.Session["Usermsg"].ToString();
    }

    public string UpdateUserActivation(UserActivation usertbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_UserActivation"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE");
            cmd.Parameters.AddWithValue("@ACTIVATIONCODE", usertbl.ActivationCode);
            if (utility.ExecuteOnlineLHRC(cmd))
            {
                HttpContext.Current.Session["Usermsg"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["Usermsg"] = "Failed";
            }
        }
        return HttpContext.Current.Session["Usermsg"].ToString();
    }

    public string Login(RegistrationTable tbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@PARA", "LOGIN_BY_EMAIL_OR_LHNO");
            //cmd.Parameters.AddWithValue("@EMAIL", tbl.EmailID);
            //cmd.Parameters.AddWithValue("@LHNO", tbl.LHNO);           
            //cmd.Parameters.AddWithValue("@LH_NO", tbl.ORILHNO);
            cmd.Parameters.AddWithValue("@PARA", "LOGIN_BY_USERID");
            cmd.Parameters.AddWithValue("@USERNAME", tbl.UserName);
            cmd.Parameters.AddWithValue("@PASSWORD", tbl.Password);
            dt = utility.DisplayOnlineLHRC(cmd);
            if (dt.Rows.Count > 0)
            {
                #region Remove Cookies

                HttpContext.Current.Response.Cookies["DoctorsName"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["patientsIdNew"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["appointmentdate"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["DoctorIdconsid"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Speciality_Id_bint"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Doctor_Id_bint"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["App_Time_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["App_date_dtm"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Patient_Name_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Address_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Country_Id_bint"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["State_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["City_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Postal_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Mobile_No_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Email_Id_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["Visit_vcr"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["checkpaymentprocess"].Expires = DateTime.Now.AddDays(-1);
                if (HttpContext.Current.Request.Cookies["checkpaymentprocess"] != null)
                {
                    HttpContext.Current.Request.Cookies["checkpaymentprocess"].Value = "";
                }

                HttpContext.Current.Response.Cookies["EMAIL"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["PASSWORD"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["FIRSTNAME"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies["AddressIndex"].Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies[AppKey.AddressIndex].Expires = DateTime.Now.AddDays(-1);
                #endregion
                

                HttpContext.Current.Session["SessionEmailID"] = dt.Rows[0]["EMAIL"].ToString();
                HttpContext.Current.Session["SessionLHNO"] = dt.Rows[0]["LHNO"].ToString();

                //HttpContext.Current.Session["SessionEmailID"] = tbl.EmailID;
                //HttpContext.Current.Session["SessionLHNO"] = tbl.LHNO;

                //HttpContext.Current.Session["AppHistoryEmail"] = tbl.EmailID;
                //HttpContext.Current.Session["AppHistoryLHNO"] = tbl.LHNO;

                HttpContext.Current.Session["AppHistoryEmail"] = dt.Rows[0]["EMAIL"].ToString();
                HttpContext.Current.Session["AppHistoryLHNO"] = dt.Rows[0]["LHNO"].ToString();

                HttpContext.Current.Session["msg"] = "Login Success";
                tbl.EmailID = dt.Rows[0]["EMAIL"].ToString();
                tbl.FirstName = dt.Rows[0]["FIRSTNAME"].ToString();
                tbl.SureName = dt.Rows[0]["LASTNAME"].ToString();
                tbl.MiddleName = dt.Rows[0]["MIDDLENAME"].ToString();
                tbl.MobileNo = dt.Rows[0]["MOBILENO"].ToString();

                string UserName = tbl.FirstName + " " + tbl.SureName;


                //HttpContext.Current.Session[AppKey.UserName] = dt.Rows[0]["FIRSTNAME"].ToString() + " " + dt.Rows[0]["LASTNAME"].ToString();

                HttpContext.Current.Session[AppKey.UserName] = dt.Rows[0]["FIRSTNAME"].ToString();
                HttpContext.Current.Session[AppKey.EmailID] = dt.Rows[0]["EMAIL"].ToString();
                HttpContext.Current.Session[AppKey.LHNO] = dt.Rows[0]["LHNO"].ToString();
                HttpContext.Current.Session[AppKey.Password] = dt.Rows[0]["PASSWORD"].ToString();
                HttpContext.Current.Session[AppKey.AddressIndex] = dt.Rows[0]["AddressIndex"].ToString();

                HttpContext.Current.Session["AddressIndexid"] = dt.Rows[0]["AddressIndex"].ToString();

                HttpContext.Current.Response.Cookies["EMAIL"].Value = dt.Rows[0]["EMAIL"].ToString();
                HttpContext.Current.Response.Cookies["EMAIL"].Expires = DateTime.Now.AddDays(10);

                HttpContext.Current.Response.Cookies["PASSWORD"].Value = dt.Rows[0]["PASSWORD"].ToString();
                HttpContext.Current.Response.Cookies["PASSWORD"].Expires = DateTime.Now.AddDays(10);

                HttpContext.Current.Response.Cookies["FIRSTNAME"].Value = dt.Rows[0]["FIRSTNAME"].ToString();
                HttpContext.Current.Response.Cookies["FIRSTNAME"].Expires = DateTime.Now.AddDays(10);

                HttpContext.Current.Response.Cookies["AddressIndex"].Value = dt.Rows[0]["ADDRESSINDEX"].ToString();
                HttpContext.Current.Response.Cookies["AddressIndex"].Expires = DateTime.Now.AddDays(10);

                HttpContext.Current.Response.Cookies[AppKey.AddressIndex].Value = dt.Rows[0]["ADDRESSINDEX"].ToString();
                HttpContext.Current.Response.Cookies[AppKey.AddressIndex].Expires = DateTime.Now.AddDays(10);

                //HttpContext.Current.Response.Write(HttpContext.Current.Session[AppKey.LHNO].ToString());
            }
            else
            {
                HttpContext.Current.Session["msg"] = "Login failed";
            }
        }
        return HttpContext.Current.Session["msg"].ToString();
    }

    public DataTable GetProfileDetail(RegistrationTable tbl)
    {

        //if (HttpContext.Current.Session["SessionEmailID"] != null)
        //{            
        //    dt = utility.DisplayOnlineLHRC("EXECUTE Proc_PCipopRegistrationMaster 'GET_PROFILE_DETAIL_EMAILID',0,'','" + HttpContext.Current.Session[AppKey.EmailID].ToString() + "','','" + HttpContext.Current.Session[AppKey.Password].ToString() + "'");
        //}
        //else if (HttpContext.Current.Session["SessionLHNO"] != null)
        //{
        //    dt = utility.DisplayOnlineLHRC("EXECUTE Proc_PCipopRegistrationMaster 'GET_PROFILE_DETAIL_LHNO',0,'','','','" + HttpContext.Current.Session[AppKey.Password].ToString() + "',0,'','','','','','','','','','','','','','',0,'" + HttpContext.Current.Session[AppKey.LHNO].ToString() + "'");
        //}

        //dt = utility.DisplayOnlineLHRC("EXECUTE Proc_PCipopRegistrationMaster 'GET_PROFILE_DETAIL_BY_TLHNO',0,'','','','',0,'','','','','','','','','','','','','','',0,'" + HttpContext.Current.Session[AppKey.LHNO].ToString() + "'");
        dt = utility.DisplayOnlineLHRC("Execute Proc_PCipopRegistrationMaster 'GET_VIEW_PROFILE_BY_ADDRESSINDEX','" + HttpContext.Current.Session[AppKey.AddressIndex].ToString() + "'");

        if (dt.Rows.Count > 0)
        {
            tbl.UserName = dt.Rows[0]["UserName"].ToString();
            tbl.SureName = dt.Rows[0]["LASTNAME"].ToString();
            tbl.FirstName = dt.Rows[0]["FIRSTNAME"].ToString();
            tbl.MiddleName = dt.Rows[0]["MIDDLENAME"].ToString();
            tbl.Sex = dt.Rows[0]["SEX"].ToString();
            tbl.DOB = dt.Rows[0]["Dateofbirth"].ToString();
            tbl.MaritalStatus = dt.Rows[0]["MARITALSTATUS"].ToString();
            tbl.Religion = dt.Rows[0]["RELIGION"].ToString();
            tbl.Occupation = dt.Rows[0]["OCCUPATION"].ToString();
            tbl.Address = dt.Rows[0]["ADDRESS"].ToString();
            tbl.TelephoneResidence = dt.Rows[0]["RESIDENCE"].ToString();
            tbl.MobileNo = dt.Rows[0]["MOBILENO"].ToString();
            tbl.Country = dt.Rows[0]["COUNTRY"].ToString();
            tbl.State = dt.Rows[0]["STATE"].ToString();
            tbl.City = dt.Rows[0]["CITY"].ToString();
            tbl.Pincode = dt.Rows[0]["PINCODE"].ToString();
            tbl.EmailID = dt.Rows[0]["EMAIL"].ToString();
            tbl.Title = dt.Rows[0]["TITLE"].ToString();
            tbl.BloodGroup = dt.Rows[0]["BLOODGROUP"].ToString();
        }
        return dt;
    }

    public string UpdateProfile(RegistrationTable tbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;

            if (HttpContext.Current.Session["SessionLHNO"] != null)
            {
                cmd.Parameters.AddWithValue("@LHNO", HttpContext.Current.Session["SessionLHNO"].ToString());
                cmd.Parameters.AddWithValue("@PARA", "UPDATE_PROFILE_LHNO");
            }
            else if (HttpContext.Current.Session["SessionEmailID"] != null)
            {
                cmd.Parameters.AddWithValue("@Email", HttpContext.Current.Session[AppKey.EmailID].ToString());
                cmd.Parameters.AddWithValue("@PARA", "UPDATE_PROFILE_EMAIL");
            }
            cmd.Parameters.AddWithValue("@PASSWORD", HttpContext.Current.Session[AppKey.Password].ToString());
            cmd.Parameters.AddWithValue("@LASTNAME", tbl.SureName);
            cmd.Parameters.AddWithValue("@FIRSTNAME", tbl.FirstName);
            cmd.Parameters.AddWithValue("@MIDDLENAME", tbl.MiddleName);
            cmd.Parameters.AddWithValue("@SEX", tbl.Sex);
            cmd.Parameters.AddWithValue("@Dateofbirth", tbl.DOB);
            cmd.Parameters.AddWithValue("@MARITALSTATUS", tbl.MaritalStatus);
            cmd.Parameters.AddWithValue("@RELIGION", tbl.Religion);
            cmd.Parameters.AddWithValue("@OCCUPATION", tbl.Occupation);
            cmd.Parameters.AddWithValue("@RESIDENCE", tbl.TelephoneResidence);
            cmd.Parameters.AddWithValue("@ADDRESS", tbl.Address);
            cmd.Parameters.AddWithValue("@MOBILENO", tbl.MobileNo);
            cmd.Parameters.AddWithValue("@COUNTRY", tbl.Country);
            cmd.Parameters.AddWithValue("@CITY", tbl.City);
            cmd.Parameters.AddWithValue("@STATE", tbl.State);
            cmd.Parameters.AddWithValue("@PINCODE", tbl.Pincode);

            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["update"] = "Success";
                HttpContext.Current.Session[AppKey.UserName] = tbl.FirstName;
            }
            else
            {
                HttpContext.Current.Session["update"] = "failed";
            }
        }
        return HttpContext.Current.Session["update"].ToString();
    }

    public DataTable Speciality()
    {
        dt = utility.Display("Execute AddUpdateGetSpecialtyMaster 'Get'");
        return dt;
    }

    public DataTable SpecilityDoc(SpecialityTable spltbl)
    {
        dt = utility.Display("Execute AddUpdateGetDoctorSpecialityMaster 'Get_For_DataList_By_SplId',0,0,'" + spltbl.Speciality_Id_bint + "'");
        return dt;
    }

    public DataTable AvailableTime(DoctorMasterTable doctbl)
    {
        dt = utility.Display("Execute AddUpdateGetDoctorMaster 'Get_By_Doc_Id_For_Apmnt','" + doctbl.Doctor_Id_bint + "'");
        return dt;
    }

    public string AddAppointment(AppointmentTable apptbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_OnlineAppointmentMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "ADD");
            cmd.Parameters.AddWithValue("@SPECIALITY_ID_BINT", apptbl.Speciality_Id_bint);
            cmd.Parameters.AddWithValue("@DOCTOR_ID_BINT", apptbl.Doctor_Id_bint);
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", apptbl.AddressIndex);
            cmd.Parameters.AddWithValue("@APP_AVAI_TIME", apptbl.App_Avai_Time);
            cmd.Parameters.AddWithValue("@APP_DATE", apptbl.App_Date);
            cmd.Parameters.AddWithValue("@APP_VISIT_TIME", apptbl.App_Visit_Time);

            cmd.Parameters.AddWithValue("@SPECIALITY_VCR", apptbl._Specialityname);
            cmd.Parameters.AddWithValue("@DOCTOR_NAME_VCR", apptbl._Doctor_Name_vcr);
            cmd.Parameters.AddWithValue("@CONSDOCID", apptbl._ConsDrId);


            cmd.Parameters.AddWithValue("@APP_STATUS", "Confirmed");
            if (utility.ExecuteOnlineLHRC(cmd))
            {
                HttpContext.Current.Session["AppMsg"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["AppMsg"] = "failed";
            }
        }
        return HttpContext.Current.Session["AppMsg"].ToString();
    }

    public DataTable AppointmentHistory()
    {

        //if (HttpContext.Current.Session["AppHistoryEmail"] != null)
        //{
        //    dt = utility.DisplayOnlineLHRC("EXECUTE Proc_PCipopRegistrationMaster 'APP_HISTORY_EMAIL',0,'','" + HttpContext.Current.Session["AppHistoryEmail"].ToString() + "','','" + HttpContext.Current.Session[AppKey.Password].ToString() + "'");
        //}
        //else if (HttpContext.Current.Session["AppHistoryLHNO"] != null)
        //{
        //    dt = utility.DisplayOnlineLHRC("EXECUTE Proc_PCipopRegistrationMaster 'APP_HISTORY_LHNO',0,'','','','" + HttpContext.Current.Session[AppKey.Password].ToString() + "',0,'','','','','','','','','','','','','','',0,'" + HttpContext.Current.Session["AppHistoryLHNO"].ToString() + "'");
        //}

        //dt = utility.DisplayOnlineLHRC("EXECUTE Proc_PCipopRegistrationMaster 'APP_HISTORY_BY_TLHNO',0,'','','','',0,'','','','','','','','','','','','','','',0,'" + HttpContext.Current.Session["AppHistoryLHNO"].ToString() + "'");

        dt = utility.DisplayOnlineLHRC("Execute Proc_PCipopRegistrationMaster 'GET_APP_HISTORY_BY_ADDRESSINDEX','" + HttpContext.Current.Session[AppKey.AddressIndex].ToString() + "'");

        return dt;
    }

    public DataTable GetAppHistoryDetail(AppointmentTable apptbl)
    {
        dt = utility.Display("Execute Proc_PCipopRegistrationMaster 'GET_APP_HISTORY','" + apptbl.App_Id_bint + "'");
        if (dt.Rows.Count > 0)
        {
            apptbl.Speciality_Id_bint = int.Parse(dt.Rows[0]["Speciality_Id_bint"].ToString());
            apptbl.Doctor_Id_bint = int.Parse(dt.Rows[0]["Doctor_Id_bint"].ToString());
            apptbl.App_Avai_Time = dt.Rows[0]["App_Avai_Time"].ToString();
            apptbl.App_Visit_Time = dt.Rows[0]["App_Visit_Time"].ToString();
            apptbl.App_Date = dt.Rows[0]["App_Date"].ToString();
        }
        return dt;
    }

    public string UpdateAppointment(AppointmentTable apptbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE_APPOINTMENT");
            cmd.Parameters.AddWithValue("@App_Id_bint", apptbl.App_Id_bint);
            cmd.Parameters.AddWithValue("@SPECIALITY_ID_BINT", apptbl.Speciality_Id_bint);
            cmd.Parameters.AddWithValue("@DOCTOR_ID_BINT", apptbl.Doctor_Id_bint);
            cmd.Parameters.AddWithValue("@APP_AVAI_TIME", apptbl.App_Avai_Time);
            cmd.Parameters.AddWithValue("@APP_DATE", apptbl.App_Date);
            cmd.Parameters.AddWithValue("@APP_VISIT_TIME", apptbl.App_Visit_Time);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["AppMsg"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["AppMsg"] = "failed";
            }
        }
        return HttpContext.Current.Session["AppMsg"].ToString();
    }

    public string CancelledAppointmet(AppointmentTable apptbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "CANCELLED_APP");
            cmd.Parameters.AddWithValue("@APP_ID_BINT", apptbl.App_Id_bint);
            cmd.Parameters.AddWithValue("@APP_STATUS", "Cancelled");
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["CanMsg"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["CanMsg"] = "Failed";
            }
        }
        return HttpContext.Current.Session["CanMsg"].ToString();
    }

    #region Change Password
    public string ChangePassword(RegistrationTable tbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "CHANGE_PASSWORD");
            cmd.Parameters.AddWithValue("@NEWPASSWORD", tbl.NewPassword);
            cmd.Parameters.AddWithValue("@PASSWORD", tbl.Password);
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", HttpContext.Current.Session[AppKey.AddressIndex].ToString());
            if (utility.ExecuteOnlineLHRC(cmd))
            {
                HttpContext.Current.Session["ChangePassword"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["ChangePassword"] = "Failed";
            }
        }
        return HttpContext.Current.Session["ChangePassword"].ToString();
    }
    #endregion

    #region Admin Panel
    public DataTable BindGrid(RegistrationTable tbl)
    {
        dt = utility.DisplayOnlineLHRC("Execute Proc_PCipopRegistrationMaster 'GET'");
        return dt;
    }

    public DataTable ViewRegistrationDetail(RegistrationTable tbl)
    {
        dt = utility.DisplayOnlineLHRC("Execute Proc_PCipopRegistrationMaster 'GET_BY_ID','" + tbl.AddressIndex + "'");
        if (dt.Rows.Count > 0)
        {
            tbl.SureName = dt.Rows[0]["LASTNAME"].ToString();
            tbl.FirstName = dt.Rows[0]["FIRSTNAME"].ToString();
            tbl.MiddleName = dt.Rows[0]["MIDDLENAME"].ToString();
            tbl.Sex = dt.Rows[0]["SEX"].ToString();
            tbl.EmailID = dt.Rows[0]["EMAIL"].ToString();
            tbl.LHNO = dt.Rows[0]["LHNO"].ToString();
            tbl.Password = dt.Rows[0]["PASSWORD"].ToString();
            tbl.DOB = dt.Rows[0]["Dateofbirth"].ToString();
            tbl.MaritalStatus = dt.Rows[0]["MARITALSTATUS"].ToString();
            tbl.Religion = dt.Rows[0]["RELIGION"].ToString();
            tbl.TelephoneResidence = dt.Rows[0]["RESIDENCE"].ToString();
            tbl.Occupation = dt.Rows[0]["OCCUPATION"].ToString();
            tbl.MobileNo = dt.Rows[0]["MOBILENO"].ToString();
            tbl.Address = dt.Rows[0]["ADDRESS"].ToString();
            tbl.Country = dt.Rows[0]["COUNTRY"].ToString();
            tbl.State = dt.Rows[0]["STATE"].ToString();
            tbl.City = dt.Rows[0]["CITY"].ToString();
            tbl.Pincode = dt.Rows[0]["PINCODE"].ToString();
            tbl.UserName = dt.Rows[0]["USERNAME"].ToString();
            tbl.Title = dt.Rows[0]["Title"].ToString();
            tbl.BloodGroup = dt.Rows[0]["BloodGroup"].ToString();
            tbl.OriginalLHNO = dt.Rows[0]["LH_NO"].ToString();
        }
        return dt;
    }

    public string UpdateUserLHNO(RegistrationTable tbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "UPDATE_LHNO");
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", tbl.AddressIndex);
            cmd.Parameters.AddWithValue("@LHNO", tbl.LHNO);
            if (utility.Execute(cmd))
            {
                HttpContext.Current.Session["msgLHNO"] = "Success";
                //SendLHNO(tbl);
            }
            else
            {
                HttpContext.Current.Session["msgLHNO"] = "Failed";
            }
        }
        return HttpContext.Current.Session["msgLHNO"].ToString();
    }

    public string DeleteRegisteredUser(RegistrationTable tbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PCipopRegistrationMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE_REG_USER");
            cmd.Parameters.AddWithValue("@ADDRESSINDEX", tbl.AddressIndex);
            if (utility.ExecuteOnlineLHRC(cmd))
            {
                HttpContext.Current.Session["msgdelete"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["msgdelete"] = "Failed";
            }
        }
        return HttpContext.Current.Session["msgdelete"].ToString();
    }

    public DataTable AdminGetAppointment()
    {
        dt = utility.DisplayOnlineLHRC("Execute Proc_PCipopRegistrationMaster 'ADMIN_GET_APPOINTMENT'");
        return dt;
    }

    public DataTable AdminGetAppointmentDetails(AdminAppointmentDetails apptbl)
    {
        dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'ADMIN_APPOINTMETN_DETAILS','" + apptbl.App_Id_bint + "'");
        if (dt.Rows.Count > 0)
        {
            apptbl.SpecialityName = dt.Rows[0]["Speciality_vcr"].ToString();
            apptbl.DoctorName = dt.Rows[0]["Doctor_Name_vcr"].ToString();
            apptbl.App_Avai_Time = dt.Rows[0]["App_Avai_Time"].ToString();
            apptbl.App_Date = dt.Rows[0]["App_Date"].ToString();
            apptbl.App_Visit_Time = dt.Rows[0]["App_Visit_Time"].ToString();

            apptbl.SureName = dt.Rows[0]["LASTNAME"].ToString();
            apptbl.FirstName = dt.Rows[0]["FIRSTNAME"].ToString();
            apptbl.MiddleName = dt.Rows[0]["MIDDLENAME"].ToString();
            apptbl.Sex = dt.Rows[0]["SEX"].ToString();
            apptbl.EmailID = dt.Rows[0]["EMAIL"].ToString();
            apptbl.LHNO = dt.Rows[0]["LHNO"].ToString();
            apptbl.DOB = dt.Rows[0]["Dateofbirth"].ToString();
            apptbl.MaritalStatus = dt.Rows[0]["MARITALSTATUS"].ToString();
            apptbl.Religion = dt.Rows[0]["RELIGION"].ToString();
            apptbl.TelephoneResidence = dt.Rows[0]["RESIDENCE"].ToString();
            apptbl.Occupation = dt.Rows[0]["OCCUPATION"].ToString();
            apptbl.MobileNo = dt.Rows[0]["MOBILENO"].ToString();
            apptbl.Address = dt.Rows[0]["ADDRESS"].ToString();

            apptbl.Title = dt.Rows[0]["TITLE"].ToString();
            apptbl.BloodGroup = dt.Rows[0]["BLOODGROUP"].ToString();
            apptbl.OriginalLHNO = dt.Rows[0]["LH_NO"].ToString();
        }
        return dt;
    }

    public DataTable SendLHNO(RegistrationTable tbl)
    {
        dt = utility.DisplayOnlineLHRC("Execute Proc_PCipopRegistrationMaster 'SEND_EMAIL_LHNO','" + tbl.AddressIndex + "'");
        if (dt.Rows.Count > 0)
        {
            tbl.AddressIndex = Convert.ToInt32(dt.Rows[0]["ADDRESSINDEX"].ToString());
            tbl.FirstName = dt.Rows[0]["FIRSTNAME"].ToString();
            tbl.SureName = dt.Rows[0]["LASTNAME"].ToString();
            tbl.LHNO = dt.Rows[0]["LHNO"].ToString();
            tbl.EmailID = dt.Rows[0]["EMAIL"].ToString();
            tbl.Password = dt.Rows[0]["PASSWORD"].ToString();

            utility.SendLHNO(tbl.FirstName, tbl.SureName, tbl.LHNO, tbl.EmailID, tbl.Password);
        }
        return dt;
    }

    public string ClosedAppointment(AppointmentTable apptbl)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_OnlineAppointmentMaster"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "CLOSED_APP");
            cmd.Parameters.AddWithValue("@APP_ID_BINT", apptbl.App_Id_bint);
            cmd.Parameters.AddWithValue("@APP_STATUS", "Closed");
            if (utility.ExecuteOnlineLHRC(cmd))
            {
                HttpContext.Current.Session["ClosesMsg"] = "Success";
            }
            else
            {
                HttpContext.Current.Session["ClosesMsg"] = "Failed";
            }
        }
        return HttpContext.Current.Session["ClosesMsg"].ToString();
    }

    #endregion

    /*xxxxxxxxxxxxxxx-my code on 21-05-16-xxxxxxxxxxxxxxxxxxxxxxx*/

    /*Speciality start */
    #region speciality

    #region Add/Update/Delete Speciality in DB
    public int AddSpeciality(Specialty obj)
    {
        int a = 0;
        try
        {
            Utility utility = new Utility();

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
            using (SqlCommand cmd = new SqlCommand("AddUpdateGetSpecialtyMaster", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                if (obj._buttontext == "Add Speciality")
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                    cmd.Parameters.AddWithValue("@Speciality_vcr", obj._Specialityname);
                    cmd.Parameters.AddWithValue("@Details_vcr", obj._Details);
                    cmd.Parameters.AddWithValue("@Facilities_vcr", obj._Facilities);
                    cmd.Parameters.AddWithValue("@Motto_vcr", obj._Motto);
                    cmd.Parameters.AddWithValue("@Group_vcr", obj._Group);
                }
                else if (obj._buttontext == "Update Speciality")
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                    cmd.Parameters.AddWithValue("@Speciality_Id_bint", obj._Id);
                    cmd.Parameters.AddWithValue("@Speciality_vcr", obj._Specialityname);
                    cmd.Parameters.AddWithValue("@Details_vcr", obj._Details);
                    cmd.Parameters.AddWithValue("@Facilities_vcr", obj._Facilities);
                    cmd.Parameters.AddWithValue("@Motto_vcr", obj._Motto);
                    cmd.Parameters.AddWithValue("@Group_vcr", obj._Group);
                }
                else if (obj._buttontext == "Delete Speciality")
                {
                    cmd.Parameters.AddWithValue("@Para_vcr", "Delete");
                    cmd.Parameters.AddWithValue("@Speciality_Id_bint", obj._Id);
                }
                else
                {

                }

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                a = int.Parse(dt.Rows[0].ItemArray[0].ToString());



            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
        return a;

    }
    #endregion

    #region Get  Speciality in GridView
    public DataTable SpecialityList()
    {
        Utility utility = new Utility();
        DataTable dt = new DataTable();
        try
        {

            dt = utility.Display("Exec AddUpdateGetSpecialtyMaster 'Get'");
            if (dt.Rows.Count > 0)
            {

            }
        }
        catch (Exception)
        {

            throw;
        }
        return dt;
    }

    #endregion

    #region Get Speciality at Edit Time
    public DataTable GetSpeciality(Specialty obj)
    {
        DataTable dt = new DataTable();
        try
        {

            dt = utility.Display("Exec AddUpdateGetSpecialtyMaster 'Get_By_Id','" + obj._Id + "'");

            if (dt.Rows.Count > 0)
            {
                obj._Specialityname = dt.Rows[0]["Speciality_vcr"].ToString();
                obj._Services = dt.Rows[0]["Speciality_vcr"].ToString();
                obj._Facilities = dt.Rows[0]["Facilities_vcr"].ToString();
                obj._Details = dt.Rows[0]["Details_vcr"].ToString();
                obj._Group = dt.Rows[0]["Group_vcr"].ToString();

            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
        return dt;
    }
    #endregion

    #region Get  Speciality in GridView @ Searching Time
    public DataTable SpecialitySearchingList(Specialty obj)
    {
        Utility utility = new Utility();
        DataTable dt = new DataTable();
        try
        {

            dt = utility.Display("Exec AddUpdateGetSpecialtyMaster 'Search_AdminPanel',0,'','','','','','" + obj._seachKey + "'");
            if (dt.Rows.Count > 0)
            {

            }
        }
        catch (Exception)
        {

            throw;
        }
        return dt;
    }

    #endregion

    #region Get Specialiaty List in Dr Profile
    public void GetCheckListOfSpeciality(Specialty obj, CheckBoxList chk, string sql)
    {
        chk.DataSource = utility.Display(sql);
        chk.DataTextField = obj._Specialityname;
        chk.DataValueField = obj._CheckBoxId;
        chk.DataBind();
    }
    #endregion
    #endregion
    /*Speciality end */


    /*Doctors start */
    #region Doctors

    #region Add Dr
    public string AddUpdateDeleteDr(Doctor obj)
    {

        int a = 0;
        string Msg = string.Empty;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        try
        {


            if (obj._Buttontext == "Add Doctor")
            {
                using (SqlCommand cmd = new SqlCommand("AddUpdateGetDoctorMaster", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.AddWithValue("@Para_vcr", "Add");


                    cmd.Parameters.AddWithValue("@Doctor_Name_vcr", obj._Doctorname);
                    cmd.Parameters.AddWithValue("@Achievements_vcr", obj._Achievements);

                    cmd.Parameters.AddWithValue("@Avail_Time_Mon_vcr", obj._AvailTimeMon);
                    cmd.Parameters.AddWithValue("@Room_vcr_Mon", obj._Room_vcr_Mon);
                    cmd.Parameters.AddWithValue("@Avail_Time_Mon_vcr1", obj._AvailTimeMon1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Mon1", obj._Room_vcr_Mon1);
                    cmd.Parameters.AddWithValue("@Not_AvailMon_bit", obj._NotAvailMonbit);




                    cmd.Parameters.AddWithValue("@Avail_Time_Tue_vcr", obj._AvailTimeTue);
                    cmd.Parameters.AddWithValue("@Room_vcr_Tue", obj._Room_vcr_Tue);
                    cmd.Parameters.AddWithValue("@Avail_Time_Tue_vcr1", obj._AvailTimeTue1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Tue1", obj._Room_vcr_Tue1);
                    cmd.Parameters.AddWithValue("@Not_AvailTue_bit", obj._NotAvailTuebit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Wed_vcr", obj._AvailTimeWed);
                    cmd.Parameters.AddWithValue("@Room_vcr_Wed", obj._Room_vcr_Wed);
                    cmd.Parameters.AddWithValue("@Avail_Time_Wed_vcr1", obj._AvailTimeWed1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Wed1", obj._Room_vcr_Wed1);
                    cmd.Parameters.AddWithValue("@Not_AvailWed_bit", obj._NotAvailWedbit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Thu_vcr", obj._AvailTimeThurs);
                    cmd.Parameters.AddWithValue("@Room_vcr_Thu", obj._Room_vcr_Thurs);
                    cmd.Parameters.AddWithValue("@Avail_Time_Thu_vcr1", obj._AvailTimeThurs1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Thu1", obj._Room_vcr_Thurs1);
                    cmd.Parameters.AddWithValue("@Not_AvailThu_bit", obj._NotAvailThursbit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Fri_vcr", obj._AvailTimeFri);
                    cmd.Parameters.AddWithValue("@Room_vcr_Fri", obj._Room_vcr_Fri);
                    cmd.Parameters.AddWithValue("@Avail_Time_Fri_vcr1", obj._AvailTimeFri1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Fri1", obj._Room_vcr_Fri1);
                    cmd.Parameters.AddWithValue("@Not_AvailFri_bit", obj._NotAvailFribit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Sat_vcr", obj._AvailTimeSat);
                    cmd.Parameters.AddWithValue("@Room_vcr_Sat", obj._Room_vcr_Sat);
                    cmd.Parameters.AddWithValue("@Avail_Time_Sat_vcr1", obj._AvailTimeSat1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Sat1", obj._Room_vcr_Sat1);
                    cmd.Parameters.AddWithValue("@Not_AvailSat_bit", obj._NotAvailSatbit);

                    /// Direct Appointment

                    cmd.Parameters.AddWithValue("@DirectAvailable", obj._DirectAvailable);
                    cmd.Parameters.AddWithValue("@ContactNo", obj._ContactNo);
                    cmd.Parameters.AddWithValue("@DirectTimeFrom", obj._DirectTimeFrom);
                    //cmd.Parameters.AddWithValue("@DirectTimeTo", obj._DirectTimeTo);
                    //cmd.Parameters.AddWithValue("@DirectDaysFrom", obj._DirectDaysFrom);
                    //cmd.Parameters.AddWithValue("@DirectDaysTo", obj._DirectDaysTo);

                    if (obj._Photo != "")
                    {
                        cmd.Parameters.AddWithValue("@Photo_vcr", obj._Photo);
                    }
                    else
                    {
                        if (obj._Gender == "Male")
                        {
                            cmd.Parameters.AddWithValue("@Photo_vcr", "doctor.jpg");
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Photo_vcr", "female-doctor.jpg");
                        }
                    }
                    if (obj._Gender == "Male")
                    {
                        cmd.Parameters.AddWithValue("@Gender_Vcr", obj._Gender);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Gender_Vcr", obj._Gender);
                    }



                    cmd.Parameters.AddWithValue("@Qualification_vcr", obj._Qualification);
                    cmd.Parameters.AddWithValue("@Areaofinterest_vcr", obj._Areaofinterest);

                    cmd.Parameters.AddWithValue("@Slotbooking", obj._SlotMinuts);
                    cmd.Parameters.AddWithValue("@ConsDocId", obj._ConsDrId);
                    cmd.Parameters.AddWithValue("@RESEARCHPUBLICATION", obj.ResearchPub);
                    cmd.Parameters.AddWithValue("@PROFILECNT", obj.ProfileCnt);

                    //tele consultation
                    cmd.Parameters.AddWithValue("@TeleConsultation", obj.TeleConsultation);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    a = int.Parse(dt.Rows[0].ItemArray[0].ToString());


                }


                if (a > 0)
                {
                    foreach (string li in obj.MultipleSpeciality)
                    {
                        using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetDoctorSpecialityMaster"))
                        {
                            cmd1.CommandType = CommandType.StoredProcedure;
                            cmd1.Parameters.AddWithValue("@Para_vcr", "Add");
                            cmd1.Parameters.AddWithValue("@Doctor_Id_bint", a);
                            cmd1.Parameters.AddWithValue("@Speciality_Id_bint", int.Parse(li));
                            if (utility.Execute(cmd1))
                            {
                                Msg = "success";
                            }
                            else
                            {
                                Msg = "failed";
                            }
                        }
                    }
                }
                else
                {
                    Msg = "failed";
                }
            }
            else if (obj._Buttontext == "Update Doctor")
            {
                using (SqlCommand cmd = new SqlCommand("AddUpdateGetDoctorMaster", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para_vcr", "Update");
                    cmd.Parameters.AddWithValue("@Doctor_Id_bint", obj._DoctorId);
                    cmd.Parameters.AddWithValue("@Doctor_Name_vcr", obj._Doctorname);
                    cmd.Parameters.AddWithValue("@Achievements_vcr", obj._Achievements);

                    cmd.Parameters.AddWithValue("@Avail_Time_Mon_vcr", obj._AvailTimeMon);
                    cmd.Parameters.AddWithValue("@Room_vcr_Mon", obj._Room_vcr_Mon);
                    cmd.Parameters.AddWithValue("@Avail_Time_Mon_vcr1", obj._AvailTimeMon1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Mon1", obj._Room_vcr_Mon1);
                    cmd.Parameters.AddWithValue("@Not_AvailMon_bit", obj._NotAvailMonbit);




                    cmd.Parameters.AddWithValue("@Avail_Time_Tue_vcr", obj._AvailTimeTue);
                    cmd.Parameters.AddWithValue("@Room_vcr_Tue", obj._Room_vcr_Tue);
                    cmd.Parameters.AddWithValue("@Avail_Time_Tue_vcr1", obj._AvailTimeTue1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Tue1", obj._Room_vcr_Tue1);
                    cmd.Parameters.AddWithValue("@Not_AvailTue_bit", obj._NotAvailTuebit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Wed_vcr", obj._AvailTimeWed);
                    cmd.Parameters.AddWithValue("@Room_vcr_Wed", obj._Room_vcr_Wed);
                    cmd.Parameters.AddWithValue("@Avail_Time_Wed_vcr1", obj._AvailTimeWed1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Wed1", obj._Room_vcr_Wed1);
                    cmd.Parameters.AddWithValue("@Not_AvailWed_bit", obj._NotAvailWedbit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Thu_vcr", obj._AvailTimeThurs);
                    cmd.Parameters.AddWithValue("@Room_vcr_Thu", obj._Room_vcr_Thurs);
                    cmd.Parameters.AddWithValue("@Avail_Time_Thu_vcr1", obj._AvailTimeThurs1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Thu1", obj._Room_vcr_Thurs1);
                    cmd.Parameters.AddWithValue("@Not_AvailThu_bit", obj._NotAvailThursbit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Fri_vcr", obj._AvailTimeFri);
                    cmd.Parameters.AddWithValue("@Room_vcr_Fri", obj._Room_vcr_Fri);
                    cmd.Parameters.AddWithValue("@Avail_Time_Fri_vcr1", obj._AvailTimeFri1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Fri1", obj._Room_vcr_Fri1);
                    cmd.Parameters.AddWithValue("@Not_AvailFri_bit", obj._NotAvailFribit);


                    cmd.Parameters.AddWithValue("@Avail_Time_Sat_vcr", obj._AvailTimeSat);
                    cmd.Parameters.AddWithValue("@Room_vcr_Sat", obj._Room_vcr_Sat);
                    cmd.Parameters.AddWithValue("@Avail_Time_Sat_vcr1", obj._AvailTimeSat1);
                    cmd.Parameters.AddWithValue("@Room_vcr_Sat1", obj._Room_vcr_Sat1);
                    cmd.Parameters.AddWithValue("@Not_AvailSat_bit", obj._NotAvailSatbit);


                    /// Direct Appointment

                    cmd.Parameters.AddWithValue("@DirectAvailable", obj._DirectAvailable);
                    cmd.Parameters.AddWithValue("@ContactNo", obj._ContactNo);
                    cmd.Parameters.AddWithValue("@DirectTimeFrom", obj._DirectTimeFrom);
                    //cmd.Parameters.AddWithValue("@DirectTimeTo", obj._DirectTimeTo);
                    //cmd.Parameters.AddWithValue("@DirectDaysFrom", obj._DirectDaysFrom);
                    //cmd.Parameters.AddWithValue("@DirectDaysTo", obj._DirectDaysTo);

                    if (obj._Photo != "")
                    {
                        cmd.Parameters.AddWithValue("@Photo_vcr", obj._Photo);
                    }
                    else
                    {
                        //if (obj._Gender == "Male")
                        //{
                        //    cmd.Parameters.AddWithValue("@Photo_vcr", "doctor.jpg");
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue("@Photo_vcr", "female-doctor.jpg");
                        //}
                    }
                    if (obj._Gender == "Male")
                    {
                        cmd.Parameters.AddWithValue("@Gender_Vcr", obj._Gender);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Gender_Vcr", obj._Gender);
                    }



                    cmd.Parameters.AddWithValue("@Qualification_vcr", obj._Qualification);
                    cmd.Parameters.AddWithValue("@Areaofinterest_vcr", obj._Areaofinterest);

                    cmd.Parameters.AddWithValue("@Slotbooking", obj._SlotMinuts);
                    cmd.Parameters.AddWithValue("@ConsDocId", obj._ConsDrId);
                    cmd.Parameters.AddWithValue("@RESEARCHPUBLICATION", obj.ResearchPub);
                    cmd.Parameters.AddWithValue("@PROFILECNT", obj.ProfileCnt);

                    //tele consultation
                    cmd.Parameters.AddWithValue("@TeleConsultation", obj.TeleConsultation);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //DataTable dt = new DataTable();
                    //da.Fill(dt);
                    //a = int.Parse(dt.Rows[0].ItemArray[0].ToString());


                }

                #region Region To Update Specialities of Doctor
                ///1) Bring Data.
                ///2) Delete If Not Selected
                ///3) Add new If new one is Selected.

                //DataTable dt = new DataTable();
                //dt = utility.Display("EXEC  AddUpdateGetDoctorSpecialityMaster 'Get_By_Doc_Id',0," + obj._DoctorId);
                //ArrayList ar = new ArrayList();
                //foreach (string spid in obj.MultipleSpecialitydelete)
                //{

                //    using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetDoctorSpecialityMaster", con))
                //    {
                //        cmd1.CommandType = CommandType.StoredProcedure;
                //        cmd1.Parameters.AddWithValue("@Para_vcr", "Delete_By_Doctor_And_Spl");
                //        cmd1.Parameters.AddWithValue("@Doctor_Id_bint", obj._DoctorId);
                //        cmd1.Parameters.AddWithValue("@Speciality_Id_bint", spid);
                //        if (con.State == ConnectionState.Closed)
                //        { con.Open(); }
                //        cmd1.ExecuteNonQuery();
                //    }


                //}
                string getid = string.Empty;
                foreach (string li in obj.MultipleSpeciality)
                {
                    using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetDoctorSpecialityMaster"))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@Para_vcr", "update_speciality");
                        cmd1.Parameters.AddWithValue("@Doctor_Id_bint", obj._DoctorId);
                        cmd1.Parameters.AddWithValue("@Speciality_Id_bint", int.Parse(li));
                        if (utility.Execute(cmd1))
                        {
                            Msg = "Dr Update";
                        }
                        else
                        {

                            //Msg = "failed";
                            Msg = "Dr Update";

                        }
                    }
                    getid += li + ",";
                }
                if (getid != "")
                {
                    getid = getid.Substring(0, getid.Length - 1);
                    SqlCommand cmdnew = new SqlCommand("UPDATE DoctorSpecialityMaster SET Is_Active_bit=0,Updated_On_dtm=GETDATE() 	WHERE	Doctor_Id_bint='" + obj._DoctorId + "' 	AND	not Speciality_Id_bint   in(" + getid + ")");
                    utility.Execute(cmdnew);
                }
                #endregion



                //  Msg.ShowSuccess("Record Updated Successfully.");
            }
            else if (obj._Buttontext == "Delete Doctor")
            {
                using (SqlCommand cmd1 = new SqlCommand("AddUpdateGetDoctorMaster"))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@Para_vcr", "Delete");
                    cmd1.Parameters.AddWithValue("@Doctor_Id_bint", obj._DoctorId);

                    if (utility.Execute(cmd1))
                    {
                        Msg = "delete Successfully";
                    }
                    else
                    {
                        Msg = "failed";
                    }
                }
            }
            else
            {

            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
        return Msg;
    }
    #endregion

    #region Get  Dr in GridView
    public DataTable DoctorList()
    {
        Utility utility = new Utility();
        DataTable dt = new DataTable();
        try
        {

            dt = utility.Display("EXEC  AddUpdateGetDoctorSpecialityMaster 'Get_For_Grid'");
            if (dt.Rows.Count > 0)
            {

            }
        }
        catch (Exception)
        {

            throw;
        }
        return dt;
    }
    #endregion

    #region Get Dr at Edit Time
    public DataTable GetDoctorlity(Doctor obj)
    {
        DataTable dt = new DataTable();
        try
        {

            dt = utility.Display("EXEC AddUpdateGetDoctorMaster 'Get_By_Doc_Id','" + obj._DoctorId + "'");

            if (dt.Rows.Count > 0)
            {
                obj._Doctorname = dt.Rows[0]["Doctor_Name_vcr"].ToString();
                //txtDesignation.Text = dt.Rows[0]["Designation_vcr"].ToString();
                obj._Qualification = dt.Rows[0]["Qualification_vcr"].ToString();
                obj._Areaofinterest = dt.Rows[0]["Areaofinterest_vcr"].ToString();
                obj._Achievements = dt.Rows[0]["Achievements_vcr"].ToString();
               
                obj._AvailTimeMon = dt.Rows[0]["Avail_Time_Mon_vcr"].ToString();
                obj._AvailTimeTue = dt.Rows[0]["Avail_Time_Tue_vcr"].ToString();
                obj._AvailTimeWed = dt.Rows[0]["Avail_Time_Wed_vcr"].ToString();
                obj._AvailTimeThurs = dt.Rows[0]["Avail_Time_Thu_vcr"].ToString();
                obj._AvailTimeFri = dt.Rows[0]["Avail_Time_Fri_vcr"].ToString();
                obj._AvailTimeSat = dt.Rows[0]["Avail_Time_Sat_vcr"].ToString();

                obj._Room_vcr_Mon = dt.Rows[0]["Room_vcr_Mon"].ToString();
                obj._Room_vcr_Tue = dt.Rows[0]["Room_vcr_Tue"].ToString();
                obj._Room_vcr_Wed = dt.Rows[0]["Room_vcr_Wed"].ToString();
                obj._Room_vcr_Thurs = dt.Rows[0]["Room_vcr_Thu"].ToString();
                obj._Room_vcr_Fri = dt.Rows[0]["Room_vcr_Fri"].ToString();
                obj._Room_vcr_Sat = dt.Rows[0]["Room_vcr_Sat"].ToString();


                obj._AvailTimeMon1 = dt.Rows[0]["Avail_Time_Mon_vcr1"].ToString();
                obj._AvailTimeTue1 = dt.Rows[0]["Avail_Time_Tue_vcr1"].ToString();
                obj._AvailTimeWed1 = dt.Rows[0]["Avail_Time_Wed_vcr1"].ToString();
                obj._AvailTimeThurs1 = dt.Rows[0]["Avail_Time_Thu_vcr1"].ToString();
                obj._AvailTimeFri1 = dt.Rows[0]["Avail_Time_Fri_vcr1"].ToString();
                obj._AvailTimeSat1 = dt.Rows[0]["Avail_Time_Sat_vcr1"].ToString();

                obj._Room_vcr_Mon1 = dt.Rows[0]["Room_vcr_Mon1"].ToString();
                obj._Room_vcr_Tue1 = dt.Rows[0]["Room_vcr_Tue1"].ToString();
                obj._Room_vcr_Wed1 = dt.Rows[0]["Room_vcr_Wed1"].ToString();
                obj._Room_vcr_Thurs1 = dt.Rows[0]["Room_vcr_Thu1"].ToString();
                obj._Room_vcr_Fri1 = dt.Rows[0]["Room_vcr_Fri1"].ToString();
                obj._Room_vcr_Sat1 = dt.Rows[0]["Room_vcr_Sat1"].ToString();


             

                obj._Photo = dt.Rows[0]["Photo_vcr"].ToString();

                if (obj._Photo.Length < 1)
                {
                    obj._Photo = "~\\images\\doctors_img\\doctor.jpg";
                }
                else
                {
                    obj._Photo = "~/Admin/Doctors/" + obj._Photo;
                }

                obj._NotAvailMonbit = bool.Parse(dt.Rows[0]["Not_AvailMon_bit"].ToString());
                obj._NotAvailTuebit = bool.Parse(dt.Rows[0]["Not_AvailTue_bit"].ToString());
                obj._NotAvailWedbit = bool.Parse(dt.Rows[0]["Not_AvailWed_bit"].ToString());
                obj._NotAvailThursbit = bool.Parse(dt.Rows[0]["Not_AvailThu_bit"].ToString());
                obj._NotAvailFribit = bool.Parse(dt.Rows[0]["Not_AvailFri_bit"].ToString());
                obj._NotAvailSatbit = bool.Parse(dt.Rows[0]["Not_AvailSat_bit"].ToString());

                obj._SlotMinuts = dt.Rows[0]["Slotbooking"].ToString();
                obj._ConsDrId = dt.Rows[0]["ConsDocId"].ToString();
                obj.ResearchPub = dt.Rows[0]["ResearchPublication"].ToString();
                obj.ProfileCnt = dt.Rows[0]["ProfileCnt"].ToString();


                // Direct Appointment

                obj.DirectAvailable = bool.Parse(dt.Rows[0]["DirectAvailable"].ToString());
                obj.ContactNo = dt.Rows[0]["ContactNo"].ToString();
                obj.DirectTimeFrom = dt.Rows[0]["DirectTimeFrom"].ToString();
                //obj.DirectTimeTo = dt.Rows[0]["DirectTimeTo"].ToString();
                //obj.DirectDaysFrom = dt.Rows[0]["DirectDaysFrom"].ToString();
                //obj.DirectDaysTo = dt.Rows[0]["DirectDaysTo"].ToString();

                //Tele Consultation
                obj.TeleConsultation = bool.Parse(dt.Rows[0]["TeleConsultation"].ToString());

            }
        }
        catch (Exception ex)
        {

            throw ex;
        }
        return dt;
    }
    #endregion

    #region Search Doc Name

    public DataTable DoctorNameSearch(Doctor obj)
    {
        Utility utility = new Utility();
        DataTable dt = new DataTable();
        try
        {

            dt = utility.Display("Exec AddUpdateGetDoctorSpecialityMaster 'Admin_Search_Doc',0,0,0,'" + obj.Search + "'");
            if (dt.Rows.Count > 0)
            {

            }
        }
        catch (Exception)
        {

            throw;
        }
        return dt;
    }

    #endregion


    #endregion
    /*Doctors end */

    /* Career and Apply for job start*/
    #region Career
    public int AddAndSendMail(Career obj)
    {
        Utility utility = new Utility();
        int a = 0;
        try
        {
            string ext = "";
            int Id = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);



            using (SqlCommand cmd = new SqlCommand("Proc_Jobapplied", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para_vcr", "Add");
                cmd.Parameters.AddWithValue("@Postion_Name_vcr", obj._PostionName);
                cmd.Parameters.AddWithValue("@Spec_Name_vcr", obj._DepartmentName);
                cmd.Parameters.AddWithValue("@Spec_Id_int", obj._DepartmentId);

                cmd.Parameters.AddWithValue("@First_Name_vcr", obj._FirstName);
                cmd.Parameters.AddWithValue("@Last_Name_vcr", obj._LastName);
                //cmd.Parameters.AddWithValue("@DOB", txtDob.Text.Trim());
                cmd.Parameters.AddWithValue("@DOB", DateTime.ParseExact(obj._DOB, "dd-MM-yyyy", null).ToString("MM/dd/yyyy"));

                cmd.Parameters.AddWithValue("@Gender", obj._Gender);
                cmd.Parameters.AddWithValue("@Phone", obj._Phone);
                cmd.Parameters.AddWithValue("@Resident_of_vcr", obj._Residentof);
                cmd.Parameters.AddWithValue("@Presently_working_location", obj._Presentlyworkinglocation);
                cmd.Parameters.AddWithValue("@Relevant_Exp", obj._RelevantExp);
                cmd.Parameters.AddWithValue("@Total_Exp", obj._TotalExp);
                cmd.Parameters.AddWithValue("@Monthly_expected_Salary", obj._MonthlyexpectedSalary);
                cmd.Parameters.AddWithValue("@Qualification", obj._Qualification);
                cmd.Parameters.AddWithValue("@Hostel_Accommodation_required", obj._HostelAccommodationrequired);
                cmd.Parameters.AddWithValue("@Registration_with_MMC", obj._RegistrationwithMMC);
                cmd.Parameters.AddWithValue("@EMAILID", obj._Emailid);
                cmd.Parameters.AddWithValue("@File_Name_vcr", "UploadedResume");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                Id = int.Parse(dt.Rows[0][0].ToString());
                string fileName = dt.Rows[0][1].ToString();
                ext = fileName + obj._FileExt;
                obj._Fileofcv = ext;
                obj._Fullpath = System.Web.HttpContext.Current.Server.MapPath("Admin\\Resume\\" + obj._Fileofcv);


            }
            using (SqlCommand cmd1 = new SqlCommand("Proc_Jobapplied", con))
            {
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@Para_vcr", "UpdateResumeExtension");
                cmd1.Parameters.AddWithValue("@File_Name_vcr", ext);
                cmd1.Parameters.AddWithValue("@JobApp_Id_bint", Id);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd1.ExecuteNonQuery();

            }



            //SendEMail(string strto, string Subject, string Body, string opt, string FilePath);

            a = 1;
        }
        catch (Exception ex)
        {
            a = 0;
            //lblMsg.Visible = true;
            //lblMsg.Text = "Some error occurred,could not apply for the job.";
        }
        return a;
    }

    public DataTable BindGridViewRecord()
    {
        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("Exec PROC_JOBAPPLIED 'GET'");
            con.Open();
            cmd.Connection = con;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {



            throw;
        }
    }

    public string DeleteGridViewRecord(Career obj)
    {
        string result = string.Empty;
        using (SqlCommand cmd = new SqlCommand("PROC_JOBAPPLIED"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA_VCR", "DELETE");
            cmd.Parameters.AddWithValue("@JOBAPP_ID_BINT", obj._JobId);
            if (utility.Execute(cmd))
            {
                result = "1";
            }
            else
            {
                result = "0";
            }
        }
        return result;
    }

    #endregion
    /* Career and Apply for job End*/



    /*Second Admin Handle Code start*/

    #region Appointment Search History Code

    public DataTable GetAppointmentHistoryByFromdate(AdminAppointmentDetails obj)
    {
        //2016-06-21
        if (obj._fromdate != null && obj._todate == null)
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'GET_BY_FROMDATE',0,0,0,0,'','','','','" + DateTime.ParseExact(obj._fromdate, "dd-MM-yyyy", null).ToString("yyyy-MM-dd") + "'");
        }
        else if (obj._fromdate != null && obj._todate != null)
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'GET_BY_FROMTODATE',0,0,0,0,'','','','','" + DateTime.ParseExact(obj._fromdate, "dd-MM-yyyy", null).ToString("yyyy-MM-dd") + "','" + DateTime.ParseExact(obj._todate, "dd-MM-yyyy", null).ToString("yyyy-MM-dd") + "'");
        }

        return dt;
    }

    #region Export Record In Csv File
    public DataTable ExportAppointmentHistoryByFromdate(AdminAppointmentDetails obj)
    {
        //2016-06-21
        if (obj._fromdate != "" && obj._todate == "")
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'EXPORT_BY_FROMDATE',0,0,0,0,'','','','','" + DateTime.ParseExact(obj._fromdate, "dd-MM-yyyy", null).ToString("yyyy-MM-dd") + "'");
        }
        else if (obj._fromdate != "" && obj._todate != "")
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'EXPORT_BY_FROMTODATE',0,0,0,0,'','','','','" + DateTime.ParseExact(obj._fromdate, "dd-MM-yyyy", null).ToString("yyyy-MM-dd") + "','" + DateTime.ParseExact(obj._todate, "dd-MM-yyyy", null).ToString("yyyy-MM-dd") + "'");
        }

        return dt;
    }

    public DataTable ExportAppointmentHistoryByDoctor(AdminAppointmentDetails obj)
    {
        //2016-06-21
        if (obj._Doctor_Id_bint != 0 && obj._Speciality_Id_bint == 0)
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'EXPORT_BY_DOCTOR',0,0,'" + obj._Doctor_Id_bint + "'");
        }
        if (obj._Doctor_Id_bint == 0 && obj._Speciality_Id_bint != 0)
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'EXPORT_BY_SPECIALTY',0,'" + obj._Speciality_Id_bint + "'");
        }

        return dt;
    }

    #endregion

    public void FillDoctorListOnPageLoad(DropDownList ddllist, string datavaluefields, string datatextfields)
    {
        ddllist.DataSource = utility.Display("Execute AddUpdateGetDoctorSpecialityMaster 'ADMIN_APPOINTMENT_GET_DOCTOR'");
        //ddllist.DataSource = utility.Display("Execute AddUpdateGetDoctorSpecialityMaster 'Get'");
        ddllist.DataValueField = datavaluefields.ToString();
        ddllist.DataTextField = datatextfields.ToString();
        ddllist.DataBind();
        ddllist.Items.Insert(0, new ListItem("Select Doctor", "0"));
        // ddllist.Style.Add("font-weight", "bold");
    }

    public void FillSpecialtyListOnPageLoad(DropDownList ddllist, string datavaluefields, string datatextfields)
    {
        ddllist.DataSource = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_SPECIALTY_DDL'");
        ddllist.DataValueField = datavaluefields.ToString();
        ddllist.DataTextField = datatextfields.ToString();
        ddllist.DataBind();
        ddllist.Items.Insert(0, new ListItem("Select Specialty", "0"));
        //ddllist.Style.Add("font-weight", "bold");
    }

    public DataTable GetAppointmentHistoryByDoctor(AdminAppointmentDetails obj)
    {
        //2016-06-21
        if (obj._Doctor_Id_bint != 0 && obj._Speciality_Id_bint == 0)
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'GET_BY_DOCTOR',0,0,'" + obj._Doctor_Id_bint + "'");
        }
        if (obj._Doctor_Id_bint == 0 && obj._Speciality_Id_bint != 0)
        {
            dt = utility.DisplayOnlineLHRC("Execute Proc_OnlineAppointmentMaster 'GET_BY_SPECIALTY',0,'" + obj._Speciality_Id_bint + "'");
        }

        return dt;
    }


    #endregion

    /*Second Admin Handle Code End*/

    /*Innerbanner start*/
    #region Innerbanner

    public string InsertUpdateInnerBanner(InnerBanner obj)
    {
        string Result = string.Empty;
        string test = HttpContext.Current.Session[AppKey.SESSION_ADMIN_USERNAME_KEY].ToString();

        if (obj._ButtonName == "Submit")
        {
            using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "ADD");
                cmd.Parameters.AddWithValue("@PAGENAME", obj._pagename);
                cmd.Parameters.AddWithValue("@IMAGES", obj._image);
                cmd.Parameters.AddWithValue("@ADDEDBY", HttpContext.Current.Session[AppKey.SESSION_ADMIN_USERNAME_KEY].ToString());
                if (utility.Execute(cmd))
                {
                    Result = "success";
                }
                else
                {
                    Result = "fail";
                }
            }
        }
        else
        {
            using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "UPDATE");
                cmd.Parameters.AddWithValue("@PAGENAME", obj._pagename);
                cmd.Parameters.AddWithValue("@INNERBANNER_ID", obj._innerid);
                cmd.Parameters.AddWithValue("@IMAGES", obj._image);
                cmd.Parameters.AddWithValue("@ADDEDBY", HttpContext.Current.Session[AppKey.SESSION_ADMIN_USERNAME_KEY].ToString());
                if (utility.Execute(cmd))
                {
                    Result = "success";
                }
                else
                {
                    Result = "fail";
                }
            }
        }
        return Result;
    }


    public DataTable BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'GET'");
        return dt;
    }

    public DataTable GetRecordByIdWise(InnerBanner obj)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'GET_BY_ID','" + obj._innerid + "'");
        if (dt.Rows.Count > 0)
        {
            obj._pagename = dt.Rows[0]["PAGENAME"].ToString();
            obj._image = dt.Rows[0]["IMAGES"].ToString();
        }
        return dt;
    }

    public string DeleteRecordById(InnerBanner obj)
    {
        string result = string.Empty;
        using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "DELETE");
            cmd.Parameters.AddWithValue("@INNERBANNER_ID", obj._innerid);
            if (utility.Execute(cmd))
            {
                result = "success";
            }
            else
            {
                result = "fail";
            }
            return result;
        }
    }

    public string GetBannerForBinding(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'GET_BANNER_BY_ID','" + Id + "'");
        StringBuilder sb = new StringBuilder();
        if (dt.Rows.Count > 0)
        {
            sb.Append("<div class='inner_banner'>");
            sb.Append("<img src='/uploads/home_banner/" + dt.Rows[0]["IMAGES"].ToString() + "'>");
            sb.Append("</div>");
        }
        else
        {
            sb.Append("<div class='inner_banner'>");
            sb.Append("<img src='/uploads/home_banner/DefaultInnerImage.jpg'>");
            sb.Append("</div>");
        }
        return sb.ToString();
    }


    #endregion
    /* Innerbanner End*/

    /*xxxxxxxxxxxxxxx-my code end-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/
}