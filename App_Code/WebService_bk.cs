﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService_bk : System.Web.Services.WebService {

    public WebService_bk()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

   



    [WebMethod]
    public DataSet GetDocList()
    {
       Utility utility=new Utility(); 
        //DataTable dt = new DataTable();

       SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
       DataSet ds = new DataSet();
       string query = "Exec AddUpdateGetDoctorMaster 'Get'";
       con.Open();
       SqlDataAdapter da = new SqlDataAdapter(query, con);
       da.Fill(ds);
       con.Close();



        //dt = utility.Display("Exec AddUpdateGetDoctorMaster 'Get'");
       if (ds.Tables[0].Rows.Count > 0)
        {

        }
        else
        {
            ds = null;
        }
        return ds;
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }
    
}
