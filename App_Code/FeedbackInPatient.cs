﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for FeedbackOutPatient
/// </summary>
public class FeedbackInPatient
{
    //string strTo = "rahul@kwebmaker.com";

    //string strTo = "operations@lilavatihospital.com";

    string strTo = "feedback@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Feedback From Lilavati Website from In-Patient";

	public FeedbackInPatient()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body)
    {       

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;

            

            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;
            message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailFeedbackInPatient(string FirstName, string LastName, string LHNo, string BedNo, string DateOfAdmission, string DateOFDischarge, string ContactNo, string EmailID, string HowDidYouComeTo, string GuidanceSupport, string CourtesyAndFriendliness, string TimeTakenToComplete, string ClarityProvided, string CareAttention, string DoctorsEfforts, string CourtesyFriendnessNursing, string CareAndAttentionMedication, string ResponseToYou, string RespectForPrivacy, string QualityOfFood, string VarietyOfFood, string PromptnessOfService, string InformationGuidance, string CleanlinessOfRoom, string ResponseToYouNeed, string CourtesyAndHelpfulness, string CourtesyOfSecurity, string Professionalism, string BillingCounseling, string PromptnessOfBilling, string CountesyHelpfulness, string PromptnessOfDischarge, string DischargeCounseling, string ConvenienceOfDischarge, string HowWouldYouRate, string WhatDidYouLike, string WhatDidYouLikeLeast, string AnysuggestionYouWouldLike, string WouldYouLikeToRecommend, string Pathology_BloodCollection, string Radiology_X_ray, string Cardiology_ECG_Stress_test, string Nuclear_Dept_BoneScanLiverScan, string TimetakenforTestRadiology_X_ray, string TimetakenforTestCardiology_ECG_Stress_test, string TimetakenforTestNuclear_Dept_BoneScanLiverScan,string TimetakenforTestPathology)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> FEEDBACK IN-PATIENT DETAILS </font></strong></td></tr>");

            strEmailBuilder.AppendFormat("<tr><td width='200px'>First Name   </td><td> " + FirstName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> Last Name </td><td> " + LastName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> LH No. </td><td> " + LHNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> Bed No. </td><td> " + BedNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> Date of Admission </td><td> " + DateOfAdmission + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> Date Of Discharge </td><td> " + DateOFDischarge + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> Contact No. </td><td> " + ContactNo + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td> Email ID </td><td> " + EmailID + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong>How did you come to know about Lilavati Hospital? </strong></td><td> " + HowDidYouComeTo + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong>How would you rate your overall experience at Lilavati Hospital </strong></td><td> " + HowWouldYouRate + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong>Admission</strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Guidance & support provided in completing paper work & formalities for admission   </td><td> " + GuidanceSupport + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Courtesy and friendliness of admission staff   </td><td> " + CourtesyAndFriendliness + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Time taken to complete admission process   </td><td> " + TimeTakenToComplete + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong>Doctor</strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Clarity provided about your condition and line of treatment   </td><td> " + ClarityProvided + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Care & attention (in terms of regular visit & time spent)   </td><td> " + CareAttention + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Doctor's efforts to include you in the decision about your treatment   </td><td> " + DoctorsEfforts + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Nursing Care </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Courtesy & friendliness of Nursing Staff.   </td><td> " + CourtesyFriendnessNursing + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Care & attention (in terms of Medication)   </td><td> " + CareAndAttentionMedication + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Response to your needs   </td><td> " + ResponseToYou + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>4. Respect for privacy and dignity   </td><td> " + RespectForPrivacy + " </td></tr>");

            //strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Courtesy & friendliness of Staff : </strong></td> <td> " + CountesyOfStaff + " </td> </tr>");
            //strEmailBuilder.AppendFormat("<tr> <td><strong>Clarity of Instructions : </strong></td> <td> " + ClarityOfInstructions + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Diagnostic </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Pathology (Blood Collection)   </td><td> " + Pathology_BloodCollection + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)   </td><td> " + Radiology_X_ray + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Cardiology (ECG, Stress test, 2D Echo, etc.)   </td><td> " + Cardiology_ECG_Stress_test + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>4. Nuclear Dept. (PET CT, SPECT CT, etc.)   </td><td> " + Nuclear_Dept_BoneScanLiverScan + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Time Taken for Test & Investigation </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Pathology (Blood Collection)   </td><td> "+ TimetakenforTestPathology + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Radiology (X ray, Sonography/ mammo, CT Scan, MRI, etc.)   </td><td> " + TimetakenforTestRadiology_X_ray + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Cardiology (ECG, Stress test, 2D Echo, etc)   </td><td> " + TimetakenforTestCardiology_ECG_Stress_test + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>4. Nuclear Dept. (Bone Scan, Liver Scan, Pet—CT, etc)   </td><td> " + TimetakenforTestNuclear_Dept_BoneScanLiverScan + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Food Services </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Quality of food & Beverage served   </td><td> " + QualityOfFood + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Promptness of service   </td><td> " + PromptnessOfService + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Information & guidance provided by dietitian regarding your diet   </td><td> " + InformationGuidance + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Housekeeping </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Cleanliness of the room, toilet & Linen   </td><td> " + CleanlinessOfRoom + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Response to your needs  </td><td> " + ResponseToYouNeed + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Courtesy & helpfulness of the Housekeeping staff   </td><td> " + CourtesyAndHelpfulness + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Security </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Courtesy of the security guards </td><td> " + CourtesyOfSecurity + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Professionalism of security guards </td><td> " + Professionalism + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Billing </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Billing counseling (in terms of explanation of charges/ bills)   </td><td> " + BillingCounseling + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Promptness of billing process   </td><td> " + PromptnessOfBilling + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Courtesy & helpfulness of the billing staff   </td><td> " + CountesyHelpfulness + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td colspan='2'><strong> Discharge </strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>1. Promptness of discharge process   </td><td> " + PromptnessOfDischarge + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>2. Discharge counseling (In terms of Post discharge care & follow up)   </td><td> " + DischargeCounseling + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr><td>3. Convenience of discharge process   </td><td> " + ConvenienceOfDischarge + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong> What did you like the most about Lilavati hospital?</strong></td><td> " + WhatDidYouLike + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong> What did you like the least about Lilavati hospital?</strong></td><td> " + WhatDidYouLikeLeast + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong> Any suggestion you would like to put forward which will help us to serve you better</strong></td><td> " + AnysuggestionYouWouldLike + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor='#ccc'><td colspan='2'></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td><strong> Would you like to recommend any staff for their outstanding Performance? Please specify name & reason. </strong></td><td> " + WouldYouLikeToRecommend + " </td></tr>");
                      

            
            

            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString());

            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}