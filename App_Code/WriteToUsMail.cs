﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for WriteToUsMail
/// </summary>
public class WriteToUsMail
{
    //string strTo = "arvind@kwebmaker.com";
    string strTo = "marketing@lilavatihospital.com";
    string strFrom = "marketing@lilavatihospital.com";
    string strMailUserName = "marketing";
    string strMailPassword = "lilavati12";
    string Subject = "Write To Us From Lilavati Website";       

    public WriteToUsMail()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int SendEmail(string Body, string enquirytype)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();            
            mailClient.Host = "webmail.lilavatihospital.com";
            mailClient.Port = 25;



            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            string strFromMail = strFrom;
            MailAddress fromAddress = new MailAddress(strFromMail, "Lilavati Hospital");
            message.From = fromAddress;
            message.Priority = System.Net.Mail.MailPriority.High;

            if (enquirytype == "Complaint" || enquirytype == "Patient Feedback")
            {
                //message.To.Add("complaints@lilavatihospital.com");
                message.To.Add("operations@lilavatihospital.com");
            }
            else
            {
                message.To.Add("info@lilavatihospital.com");
            }

            //message.To.Add(strTo);
            message.Subject = Subject;
            //message.CC.Add("sunil.g@kwebmaker.com");


            message.Body = Body;
            message.IsBodyHtml = true;
            //mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailWriteToUsMail(string Name, string Email, string mobile, string Country, string City, string enquiryType, string Message)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> Write To Us Detail From Lilavati Website </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + Name + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Email Id : </strong></td> <td> " + Email + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Mobile No : </strong></td> <td> " + mobile + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Country : </strong></td> <td> " + Country + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + City + " </td></tr>");

            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Enquiry Type : </strong></td> <td> " + enquiryType + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Message : </strong></td> <td> " + Message + " </td></tr>");

            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString(),enquiryType);


            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }

}