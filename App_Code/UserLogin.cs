﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserLogin
/// </summary>
public class UserLogin : System.Web.UI.Page
{
    public string AdminId;
    public string AdminName;

    public UserLogin()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (Context.Session != null)
        {
            if (Session[AppKey.EmailID] != null && !Session[AppKey.Password].ToString().Equals(""))
            {
                AdminId = Session[AppKey.EmailID].ToString();
                AdminName = Session[AppKey.Password].ToString();
            }
            else if (HttpContext.Current.Request.Cookies["EMAIL"] != null && HttpContext.Current.Request.Cookies["PASSWORD"] != null)
            {
                Session[AppKey.EmailID] = HttpContext.Current.Request.Cookies["EMAIL"].Value;
                Session[AppKey.Password] = HttpContext.Current.Request.Cookies["PASSWORD"].Value;
                Session[AppKey.UserName] = HttpContext.Current.Request.Cookies["FIRSTNAME"].Value;

                HttpContext.Current.Session[AppKey.AddressIndex] = HttpContext.Current.Request.Cookies["AddressIndex"].Value;
                HttpContext.Current.Session["AddressIndexid"] = HttpContext.Current.Request.Cookies["AddressIndex"].Value;

                AdminId = Session[AppKey.EmailID].ToString();
                AdminName = Session[AppKey.Password].ToString();
            }            
            else
            {
                Response.Redirect("~/login");
            }
        }
    }
}