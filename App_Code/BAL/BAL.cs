﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for BAL
/// </summary>
public class BAL
{
    DAL dal = new DAL();
    DataTable dt = new DataTable();
	public BAL()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    public string Save(RegistrationTable tbl)
    {
        return dal.SaveData(tbl);
    }

    public string Login(RegistrationTable tbl)
    {
        return dal.Login(tbl);
    }

    public DataTable GetUpdateProfileDetailes(RegistrationTable tbl)
    {
        return dal.GetProfileDetail(tbl);
    }

    public string UpdateProfile(RegistrationTable tbl)
    {
        return dal.UpdateProfile(tbl);
    }

    public DataTable Speciality()
    {
        return dal.Speciality();
    }

    public DataTable SpecilityDoc(SpecialityTable spltbl)
    {
        return dal.SpecilityDoc(spltbl);
    }

    public DataTable AvailableTime(DoctorMasterTable doctbl)
    {
        return dal.AvailableTime(doctbl);
    }

    public string AddAppointment(AppointmentTable apptbl)
    {
        return dal.AddAppointment(apptbl);
    }

    public DataTable AppointmentHistory()
    {
        return dal.AppointmentHistory();
    }
    public DataTable GetAppHistory(AppointmentTable apptbl)
    {
        return dal.GetAppHistoryDetail(apptbl);
    }

    public string UpdateAppointment(AppointmentTable apptbl)
    {
        return dal.UpdateAppointment(apptbl);
    }

    public string UpdateUserActivation(UserActivation usertbl)
    {
        return dal.UpdateUserActivation(usertbl);
    }

    public string CancelledAppointmet(AppointmentTable apptbl)
    {
        return dal.CancelledAppointmet(apptbl);
    }

    public string ChangePassword(RegistrationTable tbl)
    {
        return dal.ChangePassword(tbl);
    }

    #region
    public DataTable BindGrid(RegistrationTable tbl)
    {
        return dal.BindGrid(tbl);
    }

    public DataTable ViewRegistrationDetail(RegistrationTable tbl)
    {
        return dal.ViewRegistrationDetail(tbl);
    }

    public string UpdateUserLHNO(RegistrationTable tbl)
    {
        return dal.UpdateUserLHNO(tbl);
    }

    public string DeleteRegisteredUser(RegistrationTable tbl)
    {
        return dal.DeleteRegisteredUser(tbl);
    }

    public DataTable AdminGetAppointment()
    {
        return dal.AdminGetAppointment();
    }

    public DataTable AdminGetAppointmentDetails(AdminAppointmentDetails apptbl)
    {
        return dal.AdminGetAppointmentDetails(apptbl);
    }

    public DataTable SendLHNO(RegistrationTable tbl)
    {
        return dal.SendLHNO(tbl);
    }

    public string ClosedAppointment(AppointmentTable apptbl)
    {
        return dal.ClosedAppointment(apptbl);
    }
    #endregion

    /*xxxxxxxxxxxxxxx-my code on 21-05-16-xxxxxxxxxxxxxxxxxxxxxxx*/

    /*Speciality start */

    #region Speciality

    #region Pass Value for add Speciality
    public int PassSpeciality(Specialty obj)
    {
        return dal.AddSpeciality(obj);
    }
    #endregion

    #region Get  Speciality in GridView
    public DataTable GetSpecialityList()
    {
        return dal.SpecialityList();
    }
    #endregion

    #region Get Speciality at Edit Time
    public DataTable GetSpeciality(Specialty obj)
    {
        try
        {
            return dal.GetSpeciality(obj);
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    #endregion

    #region Get  Speciality in GridView @ Searching Time
    public DataTable GetSpecialitySearchList(Specialty obj)
    {
        return dal.SpecialitySearchingList(obj);
    }
    #endregion

    #region Get Specialiaty List in Dr Profile
    public void GetCheckListOfSpeciality(Specialty obj, CheckBoxList chk, string sql)
    {
        dal.GetCheckListOfSpeciality(obj, chk, sql);
    }
    #endregion
    #endregion

    /*Speciality end */

    /*Doctors start */

    #region Doctor

    #region Pass Dr Record
    public string PassDrRecord(Doctor obj)
    {
        return dal.AddUpdateDeleteDr(obj);
    }
    #endregion

    #region Get  Dr in GridView
    public DataTable GetDrList()
    {
        return dal.DoctorList();
    }
    #endregion

    #region Get Dr at Edit Time
    public DataTable GetDoctorlity(Doctor obj)
    {
        return dal.GetDoctorlity(obj);
    }
    #endregion

    #region Search Doctor Name

    public DataTable DoctorNameSearch(Doctor obj)
    {
        return dal.DoctorNameSearch(obj);
    }

    #endregion

    #endregion

    /*Doctors end */

    /* Career and Apply for job start*/
    #region Career
    public int AddAndSendMail(Career obj)
    {

        return dal.AddAndSendMail(obj);
    }
    public DataTable BindGridViewRecord()
    {
        try
        {
            return dal.BindGridViewRecord();
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public string DeleteGridViewRecord(Career obj)
    {
        return dal.DeleteGridViewRecord(obj);
    }
    #endregion
    /* Career and Apply for job End*/

    #region Appointment Search History Code
    public DataTable GetAppointmentHistoryByFromdate(AdminAppointmentDetails obj)
    {
        return dal.GetAppointmentHistoryByFromdate(obj);
    }

    #region Export Record In Csv File
    public DataTable ExportAppointmentHistoryByFromdate(AdminAppointmentDetails obj)
    {
        return dal.ExportAppointmentHistoryByFromdate(obj);
    }
    public DataTable ExportAppointmentHistoryByDoctor(AdminAppointmentDetails obj)
    {
        return dal.ExportAppointmentHistoryByDoctor(obj);
    }

    #endregion
    public void FillDoctorListOnPageLoad(DropDownList ddllist, string datavaluefields, string datatextfields)
    {
        dal.FillDoctorListOnPageLoad(ddllist, datavaluefields, datatextfields);
    }
    public void FillSpecialtyListOnPageLoad(DropDownList ddllist, string datavaluefields, string datatextfields)
    {
        dal.FillSpecialtyListOnPageLoad(ddllist, datavaluefields, datatextfields);
    }
    public DataTable GetAppointmentHistoryByDoctor(AdminAppointmentDetails obj)
    {
        //2016-06-21

        return dal.GetAppointmentHistoryByDoctor(obj);

    }

    #endregion

    /*Innerbanner start*/
    #region Innerbanner

    public string InsertUpdateInnerBanner(InnerBanner obj)
    {
        return dal.InsertUpdateInnerBanner(obj);
    }
    public DataTable GetGridRecord()
    {
        DataTable dt = new DataTable();
        dt = dal.BindGrid();
        return dt;
    }
    public DataTable GetRecordByEdittime(InnerBanner obj)
    {
        DataTable dt = new DataTable();
        dt = dal.GetRecordByIdWise(obj);
        return dt;
    }
    public string DeleteRecordById(InnerBanner obj)
    {
        return dal.DeleteRecordById(obj);
    }
    public string GetBannerForBinding(int Id)
    {
        return dal.GetBannerForBinding(Id);
    }

    #endregion
    /* Innerbanner End*/
    /*xxxxxxxxxxxxxxx-my code end-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

}
