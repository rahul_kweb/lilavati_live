﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CMSBAL
/// </summary>
public class CMSBAL
{
    CMSDAL cmsdal = new CMSDAL();
	public CMSBAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable GETBYIDContent(CmsMaster cms)
    {
        return cmsdal.GETBYIDContent(cms);
    }

    public string UpdateContent(CmsMaster cms)
    {
        return cmsdal.UpdateContent(cms);   
    }

    public string SaveCMS(CmsMaster cms)
    {
        return cmsdal.SaveCMS(cms);
    }

    public DataTable BindGrid()
    {
        return cmsdal.BindGrid();
    }

    #region Awards Accreditations Admin
    public string SaveAwardsAccreditations(Awards_Accreditations tblAwards)
    {
        return cmsdal.SaveAwardsAccreditations(tblAwards);
    }

    public DataTable GetAwards()
    {
        return cmsdal.GetAwards();
    }

    public string DeletedAwards(Awards_Accreditations tblAwards)
    {
        return cmsdal.DeletedAwards(tblAwards);
    }

    public DataTable GetByIdAwards(Awards_Accreditations tblAwards)
    {
        return cmsdal.GetByIdAwards(tblAwards);
    }

    public string UpdateAwards(Awards_Accreditations tblAwards)
    {
        return cmsdal.UpdateAwards(tblAwards);
    }

    #endregion

    #region Awards Accredition Front
    public DataTable GET_Awards_Desc(Awards_Accreditations tblAwards)
    {
        return cmsdal.GET_Awards_Desc(tblAwards);
    }
    #endregion

    #region Meadi Press Year Admin
    public string SaveMediaYear(MediaYear tblMediaYear)
    {
        return cmsdal.SaveMediaYear(tblMediaYear);
    }

    public DataTable GetYear()
    {
        return cmsdal.GetYear();
    }

    public string DeleteYear(MediaYear tblMediaYear)
    {
        return cmsdal.DeleteYear(tblMediaYear);
    }

    public DataTable GetByYearID(MediaYear tblMediaYear)
    {
        return cmsdal.GetByYearID(tblMediaYear);
    }

    public string UpdateYear(MediaYear tblMediaYear)
    {
        return cmsdal.UpdateYear(tblMediaYear);
    }

    #endregion

    #region Media Press Admin
    public DataTable BindYear()
    {
        return cmsdal.BindYear();
    }

    public DataTable BindMonth()
    {
        return cmsdal.BindMonth();
    }

    public string SaveMediaPress(MediaPress tblMediaPress)
    {
        return cmsdal.SaveMediaPress(tblMediaPress);
    }

    public DataTable GetMediaPress()
    {
        return cmsdal.GetMediaPress();
    }

    public string DeleteMediaPress(MediaPress tblMediaPress)
    {
        return cmsdal.DeleteMediaPress(tblMediaPress);
    }

    public DataTable GETByMediaPressID(MediaPress tblMediaPress)
    {
        return cmsdal.GETByMediaPressID(tblMediaPress);
    }

    public string UpdateMediaPress(MediaPress tblMediaPress)
    {
        return cmsdal.UpdateMediaPress(tblMediaPress);
    }
    #endregion

    #region Media Press Front   
    public DataTable GETYearWiseMonth(MediaPress tblMediaPress)
    {
        return cmsdal.GETYearWiseMonth(tblMediaPress);
    }

    public DataTable GETYearWiseMonthWiseDescrption(MediaPress tblMediaPress)
    {
        return cmsdal.GETYearWiseMonthWiseDescrption(tblMediaPress);
    }

    #endregion

    #region Testimonial Admin
    public string SaveTestimonial(Testimonial tblTestimonial)
    {
        return cmsdal.SaveTestimonial(tblTestimonial);
    }

    public DataTable GetTestimonial()
    {
        return cmsdal.GetTestimonial();
    }

    public string DeleteTestimonial(Testimonial tblTestimonial)
    {
        return cmsdal.DeleteTestimonial(tblTestimonial);
    }

    public DataTable GetByTestimonial(Testimonial tblTestimonial)
    {
        return cmsdal.GetByTestimonial(tblTestimonial);
    }

    public string UpdateTestimonial(Testimonial tblTestimonial)
    {
        return cmsdal.UpdateTestimonial(tblTestimonial);
    }

    public DataTable SearchTestimonial(Testimonial tblTestimonial)
    {
        return cmsdal.SearchTestimonial(tblTestimonial);
    }

    #endregion

    #region Testimonial Front

    public DataTable TestimonialCelebrityCommonPeople(Testimonial tblTestimonial)
    {
        return cmsdal.TestimonialCelebrityCommonPeople(tblTestimonial);
    }

    #endregion

    #region Job Area Admin
    public string SaveJobArea(Job_Area tblJobArea)
    {
        return cmsdal.SaveJobArea(tblJobArea);
    }

    public DataTable GetJobArea()
    {
        return cmsdal.GetJobArea();
    }

    public string DeleteJobArea(Job_Area tblJobArea)
    {
        return cmsdal.DeleteJobArea(tblJobArea);
    }

    public DataTable GEtByJobArea(Job_Area tblJobArea)
    {
        return cmsdal.GEtByJobArea(tblJobArea);
    }

    public string UpdateJobArea(Job_Area tblJobArea)
    {
        return cmsdal.UpdateJobArea(tblJobArea);
    }

    #endregion

    #region Job Posting Admin

    public DataTable BindJobArea()
    {
        return cmsdal.BindJobArea();
    }

    public string SaveJobPosting(Job_Posting tblJobPosting)
    {
        return cmsdal.SaveJobPosting(tblJobPosting);
    }

    public DataTable GetJobPosting()
    {
        return cmsdal.GetJobPosting();
    }

    public string DeleteJobPosting(Job_Posting tblJobPosting)
    {
        return cmsdal.DeleteJobPosting(tblJobPosting);
    }

    public DataTable GetByJobPostingId(Job_Posting tblJobPosting)
    {
        return cmsdal.GetByJobPostingId(tblJobPosting);
    }

    public string UpdateJobPosting(Job_Posting tblJobPosting)
    {
        return cmsdal.UpdateJobPosting(tblJobPosting);
    }

    #endregion

    #region Career Front

    public DataTable CareerDeparmentByPosition(Job_Posting tblJobPosting)
    {
        return cmsdal.CareerDeparmentByPosition(tblJobPosting);
    }
    public DataTable CareerPositionWiseRecord(Job_Posting tblJobPosting)
    {
        return cmsdal.CareerPositionWiseRecord(tblJobPosting);
    }

    #endregion

    #region CMSPageMaster

    public string SaveCMSPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        return cmsdal.SaveCMSPageMaster(tblcmsPageMaster);
    }

    public DataTable GetCmsPageMaster()
    {
        return cmsdal.GetCmsPageMaster();
    }

    public string DeleteCMSPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        return cmsdal.DeleteCMSPageMaster(tblcmsPageMaster);
    }

    public DataTable GetBYCmsPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        return cmsdal.GetBYCmsPageMaster(tblcmsPageMaster);
    }

    public string UpdateCMSPageMaster(CMSPageMaster tblcmsPageMaster)
    {
        return cmsdal.UpdateCMSPageMaster(tblcmsPageMaster);
    }

    #endregion

    #region CMSInnerPageMaster Admin

    public DataTable GetPageNAmeCMSInnerPageMaster()
    {
        return cmsdal.GetPageNAmeCMSInnerPageMaster();
    }

    public string SaveCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        return cmsdal.SaveCMSInnerPageMaster(tblcmsInnerPageMaster);
    }

    public DataTable GetCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        return cmsdal.GetCMSInnerPageMaster(tblcmsInnerPageMaster);
    }

    public string DeleteCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        return cmsdal.DeleteCMSInnerPageMaster(tblcmsInnerPageMaster);
    }

    public DataTable GetBYCmsInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        return cmsdal.GetBYCmsInnerPageMaster(tblcmsInnerPageMaster);
    }

    public string UpdateCMSInnerPageMaster(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        return cmsdal.UpdateCMSInnerPageMaster(tblcmsInnerPageMaster);
    }

    #endregion

    #region CMSInner Page Master Front

    public DataTable BindRepeaterCMS(CMSInnerPageMaster tblcmsInnerPageMaster)
    {
        return cmsdal.BindRepeaterCMS(tblcmsInnerPageMaster);
    }

    #endregion

    #region Health Checkup Admin

    public string SaveHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        return cmsdal.SaveHealthCheckup(tblHealthCheckup);
    }

    public DataTable GetHealthCheckup()
    {
        return cmsdal.GetHealthCheckup();
    }

    public DataTable AdminGetHealthCheckup()
    {
        return cmsdal.AdminGetHealthCheckup();
    }

    public string DeleteHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        return cmsdal.DeleteHealthCheckup(tblHealthCheckup);
    }

    public DataTable GetBYHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        return cmsdal.GetBYHealthCheckup(tblHealthCheckup);
    }

    public string UpdateHealthCheckup(HealthCheckup tblHealthCheckup)
    {
        return cmsdal.UpdateHealthCheckup(tblHealthCheckup);
    }

    #endregion

    #region Health Packages Details Admin

    public string SaveHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.SaveHealthPackageDetails(tblHealthPackagesDetails);
    }

    public DataTable GetHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.GetHealthPackageDetails(tblHealthPackagesDetails);
    }

    public string DeleteHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.DeleteHealthPackageDetails(tblHealthPackagesDetails);
    }

    public DataTable GetByHealthPackagesDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.GetByHealthPackagesDetails(tblHealthPackagesDetails);
    }

    public string UpdateHealthPackageDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.UpdateHealthPackageDetails(tblHealthPackagesDetails);
    }

    #endregion

    #region Health Packages Details Front

    public DataTable PackagesDetails(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.PackagesDetails(tblHealthPackagesDetails);
    }

    public DataTable PackagesName(HealthPackagesDetails tblHealthPackagesDetails)
    {
        return cmsdal.PackagesName(tblHealthPackagesDetails);
    }

    public DataTable PackagesMenu()
    {
        return cmsdal.PackagesMenu();
    }

    public DataTable HealthPackagesNamePrice()
    {
        return cmsdal.HealthPackagesNamePrice();
    }

    #endregion

    #region Contact Us Admin

    public DataTable GetContactUS(ContactUs tblContactUs)
    {
        return cmsdal.GetContactUS(tblContactUs);
    }

    public string UpdateContactUs(ContactUs tblContactUs)
    {
        return cmsdal.UpdateContactUs(tblContactUs);
    }

    #endregion

    #region LHMT Admin

    public string SaveLHMT(LHMT tblLHMT)
    {
        return cmsdal.SaveLHMT(tblLHMT);
    }

    public DataTable GetLHMT()
    {
        return cmsdal.GetLHMT();
    }

    public string DeleteLHMT(LHMT tblLHMT)
    {
        return cmsdal.DeleteLHMT(tblLHMT);
    }

    public DataTable GetByLHMT(LHMT tblLHMT)
    {
        return cmsdal.GetByLHMT(tblLHMT);
    }

    public string UpdateLHMT(LHMT tblLHMT)
    {
        return cmsdal.UpdateLHMT(tblLHMT);
    }

    #endregion

    #region Empanelled Companies Admin

    public string SaveEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        return cmsdal.SaveEmpanelledCompanies(tblEmpanelledCompanies);
    }

    public DataTable GetEmpanelledCompanies()
    {
        return cmsdal.GetEmpanelledCompanies();
    }

    public string DeleteEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        return cmsdal.DeleteEmpanelledCompanies(tblEmpanelledCompanies);
    }

    public DataTable GetBYEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        return cmsdal.GetBYEmpanelledCompanies(tblEmpanelledCompanies);
    }

    public string UpdateEmpanelledCompanies(EmpanelledCompanies tblEmpanelledCompanies)
    {
        return cmsdal.UpdateEmpanelledCompanies(tblEmpanelledCompanies);
    }

    #endregion

    #region Empanelled Companies Details Admin

    public string SaveEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.SaveEmpanelledDetails(tblEmpanelledDetails);
    }

    public DataTable GetEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.GetEmpanelledDetails(tblEmpanelledDetails);
    }

    public string DeleteEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.DeleteEmpanelledDetails(tblEmpanelledDetails);
    }

    public DataTable GetBYEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.GetBYEmpanelledDetails(tblEmpanelledDetails);
    }

    public string UpdateEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.UpdateEmpanelledDetails(tblEmpanelledDetails);
    }

    public DataTable GetTabNameEmpanelledDetails(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.GetTabNameEmpanelledDetails(tblEmpanelledDetails);
    }

    #endregion

    #region Empanelled Companies Details Front

    public DataTable EmpanelledDetailsTabNameWiseCompany(EmpanelledDetails tblEmpanelledDetails)
    {
        return cmsdal.EmpanelledDetailsTabNameWiseCompany(tblEmpanelledDetails);
    }

    #endregion

    #region Patients Education Admin

    public string SavePatientsEducation(PatientsEducation tblPatientsEducation)
    {
        return cmsdal.SavePatientsEducation(tblPatientsEducation);
    }

    public DataTable GetPatientsEducation()
    {
        return cmsdal.GetPatientsEducation();
    }

    public string DeletePatientsEducation(PatientsEducation tblPatientsEducation)
    {
        return cmsdal.DeletePatientsEducation(tblPatientsEducation);
    }

    public DataTable GetBYPatientsEducation(PatientsEducation tblPatientsEducation)
    {
        return cmsdal.GetBYPatientsEducation(tblPatientsEducation);
    }

    public string UpdatePatientsEducation(PatientsEducation tblPatientsEducation)
    {
        return cmsdal.UpdatePatientsEducation(tblPatientsEducation);
    }

    #endregion

    #region Patients Education Brochure Admin

    public string SavePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.SavePatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    public DataTable GetPatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.GetPatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    public string DeletePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.DeletePatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    public DataTable GetBYPatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.GetBYPatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    public string UpdatePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.UpdatePatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    public DataTable GetTittlePatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.GetTittlePatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    public DataTable TopPatientsEducationBrochure()
    {
        return cmsdal.TopPatientsEducationBrochure();
    }

    public DataTable BroucherPatientsEducationBrochure(PatientsEducationBrochure tblPatientsEducationBrochure)
    {
        return cmsdal.BroucherPatientsEducationBrochure(tblPatientsEducationBrochure);
    }

    #endregion

    #region FeedBack InPatient

    public string SaveFeedbackInPatient(FeedBackInPatient tblFeedBackInPatient)
    {
        return cmsdal.SaveFeedbackInPatient(tblFeedBackInPatient);
    }

    public DataTable GetFeedBackInPatient()
    {
        return cmsdal.GetFeedBackInPatient();
    }

    public string DeleteFeedBackInPatient(FeedBackInPatient tblFeedBackInPatient)
    {
        return cmsdal.DeleteFeedBackInPatient(tblFeedBackInPatient);
    }

    public DataTable DetailsFeedBackInPatient(FeedBackInPatient tblFeedBackInPatient)
    {
        return cmsdal.DetailsFeedBackInPatient(tblFeedBackInPatient);
    }

    #endregion

    #region Feedback OutPatient

    public string SaveFeedbackOutPatient(FeedbackOutPatient tblFeedbackOutPatient)
    {
        return cmsdal.SaveFeedbackOutPatient(tblFeedbackOutPatient);
    }

    public DataTable GetFeedbackOutPatient()
    {
        return cmsdal.GetFeedbackOutPatient();
    }

    public string DeleteFeedBackOutPatient(FeedbackOutPatient tblFeedbackOutPatient)
    {
        return cmsdal.DeleteFeedBackOutPatient(tblFeedbackOutPatient);
    }

    public DataTable DetailsFeedBackOutPatient(FeedbackOutPatient tblFeedbackOutPatient)
    {
        return cmsdal.DetailsFeedBackOutPatient(tblFeedbackOutPatient);
    }

    #endregion

    #region Photo Gallery

    public string SavePhotoGallery(PhotoGallery tblPhotoGallery)
    {
        return cmsdal.SavePhotoGallery(tblPhotoGallery);
    }

    public DataTable GetPhotoGallery()
    {
        return cmsdal.GetPhotoGallery();
    }

    public string DeletePhotoGallery(PhotoGallery tblPhotoGallery)
    {
        return cmsdal.DeletePhotoGallery(tblPhotoGallery);
    }

    public DataTable GetBYPhotoGallery(PhotoGallery tblPhotoGallery)
    {
        return cmsdal.GetBYPhotoGallery(tblPhotoGallery);
    }

    public string UpdatePhotoGallery(PhotoGallery tblPhotoGallery)
    {
        return cmsdal.UpdatePhotoGallery(tblPhotoGallery);
    }

    #endregion

    #region Home Banner

    public string SaveHomeBanner(HomeBanner tblHomeBanner)
    {
        return cmsdal.SaveHomeBanner(tblHomeBanner);
    }

    public DataTable GetHomeBanner()
    {
        return cmsdal.GetHomeBanner();
    }

    public DataTable AdminGetHomeBanner()
    {
        return cmsdal.AdminGetHomeBanner();
    }

    public string DeleteHomeBanner(HomeBanner tblHomeBanner)
    {
        return cmsdal.DeleteHomeBanner(tblHomeBanner);
    }

    public DataTable GetBYHomeBanner(HomeBanner tblHomeBanner)
    {
        return cmsdal.GetBYHomeBanner(tblHomeBanner);
    }

    public string UpdateHomeBanner(HomeBanner tblHomeBanner)
    {
        return cmsdal.UpdateHomeBanner(tblHomeBanner);
    }

    public string ShowHomeBanner(HomeBanner tblHomeBanner)
    {
        return cmsdal.ShowHomeBanner(tblHomeBanner);
    }

    public string HideHomeBanner(HomeBanner tblHomeBanner)
    {
        return cmsdal.HideHomeBanner(tblHomeBanner);
    }

    #endregion

    #region Home Page

    public DataTable GetHomeTestimonial()
    {
        return cmsdal.GetHomeTestimonial();
    }

    #endregion

    #region Update Page

    public string SaveUpdatesPage(Updates tblUpdates)
    {
        return cmsdal.SaveUpdatesPage(tblUpdates);
    }

    public DataTable GetUpdatesPage()
    {
        return cmsdal.GetUpdatesPage();
    }

    public string DeleteUpdatesPage(Updates tblUpdates)
    {
        return cmsdal.DeleteUpdatesPage(tblUpdates);
    }

    public DataTable GetByUpdatesPage(Updates tblUpdates)
    {
        return cmsdal.GetByUpdatesPage(tblUpdates);
    }

    public string UpdateUpdatesPage(Updates tblUpdates)
    {
        return cmsdal.UpdateUpdatesPage(tblUpdates);
    }

    public DataTable GEtTopTEnUpdatesPage()
    {
        return cmsdal.GEtTopTEnUpdatesPage();
    }

    #endregion

    #region LHMT Subscribe

    public string SaveLHMTSubscribe(LHMTSubscribe tblLHMTSubscribe)
    {
        return cmsdal.SaveLHMTSubscribe(tblLHMTSubscribe);
    }

    public DataTable GetLHMTSubscribe()
    {
        return cmsdal.GetLHMTSubscribe();
    }

    public string DeleteLHMTSubscribe(LHMTSubscribe tblLHMTSubscribe)
    {
        return cmsdal.DeleteLHMTSubscribe(tblLHMTSubscribe);
    }

    #endregion

    #region FeedBack HealthCheckUp

    public string SaveFeedBackHealthCheckUp(FeedBackHealthCheckUp tblFeedBackHealthCheckUp)
    {
        return cmsdal.SaveFeedBackHealthCheckUp(tblFeedBackHealthCheckUp);
    }

    public DataTable GetFeedBackHealthCheckUp()
    {
        return cmsdal.GetFeedBackHealthCheckUp();
    }

    public string DeleteFeedBackHealthCheckUp(FeedBackHealthCheckUp tblFeedBackHealthCheckUp)
    {
        return cmsdal.DeleteFeedBackHealthCheckUp(tblFeedBackHealthCheckUp);
    }

    public DataTable DetailsFeedBackHealthCheckUp(FeedBackHealthCheckUp tblFeedBackHealthCheckUp)
    {
        return cmsdal.DetailsFeedBackHealthCheckUp(tblFeedBackHealthCheckUp);
    }

    #endregion

    #region Visa Invitation

    public string SaveVisaInvitation(VisaInvitation tblVisaInvitation)
    {
        return cmsdal.SaveVisaInvitation(tblVisaInvitation);
    }

    public DataTable GetVisaInvitation()
    {
        return cmsdal.GetVisaInvitation();
    }

    public string DeleteVisaInvitation(VisaInvitation tblVisaInvitation)
    {
        return cmsdal.DeleteVisaInvitation(tblVisaInvitation);
    }

    public DataTable GetByIdVisaInvitation(VisaInvitation tblVisaInvitation)
    {
        return cmsdal.GetByIdVisaInvitation(tblVisaInvitation);
    }

    #endregion

    #region CME Section

    public string SaveCME(CME tblCME)
    {
        return cmsdal.SaveCME(tblCME);
    }

    public DataTable GetCME()
    {
        return cmsdal.GetCME();
    }

    public string DeleteCME(CME tblCME)
    {
        return cmsdal.DeleteCME(tblCME);
    }

    public DataTable GetByIdCME(CME tblCME)
    {
        return cmsdal.GetByIdCME(tblCME);
    }

    public string UpdateCME(CME tblCME)
    {
        return cmsdal.UpdateCME(tblCME);
    }

    #endregion

    #region CME Details

    public string SaveCMEDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.SaveCMEDetails(tblCMEDetails);
    }

    public DataTable GetCMEDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.GetCMEDetails(tblCMEDetails);
    }

    public DataTable HeadingCMEDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.HeadingCMEDetails(tblCMEDetails);
    }

    public string DeleteCMEDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.DeleteCMEDetails(tblCMEDetails);
    }

    public DataTable GetBYIdCMEDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.GetBYIdCMEDetails(tblCMEDetails);
    }

    public string UpdateCMEDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.UpdateCMEDetails(tblCMEDetails);
    }

    #endregion

    #region Front

    public DataTable CMEEventName()
    {
        return cmsdal.CMEEventName();
    }

    public DataTable CMEEventDetails(CMEDetails tblCMEDetails)
    {
        return cmsdal.CMEEventDetails(tblCMEDetails);
    }

    #endregion
}