﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for Testing
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Testing : System.Web.Services.WebService {

    public Testing () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }

    [WebMethod]
    public DataSet HealthCheckUp()
    {
        DataSet ds = new DataSet();
        Utility utility = new Utility();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM HEALTH_CHECKUP 	WHERE	ISACTIVE = 1 and Health_Id=20", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        return ds;
    }

    [WebMethod]
    public DataSet GetCountry()
    {
        DataSet ds = new DataSet();
        Utility utility = new Utility();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM COUNTRYMASTER WHERE IS_ACTIVE_BIT = 1", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        return ds;
    }

    [WebMethod]
    public bool InsertLhmtSubscribe(string name,string email,string contact)
    {
        string now = (DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day).ToString();
        //string test = DateTime.Now.ToString("yyyy-dd-MM hh:mm:ss");
        int n = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("insert into LhmtSubscribe (Name,Email,Contact,IsActive,Added_On) Values ('" + name + "','" + email + "','" + contact + "','" + 1 + "','" + now + "')", con);
        n = cmd.ExecuteNonQuery();
        con.Close();
        return (n > 0);
    }

    [WebMethod]
    public DataSet GetLHMT()
    {
        DataSet ds = new DataSet();
        Utility utility = new Utility();
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        //SqlCommand cmd = new SqlCommand("SELECT * FROM LhmtSubscribe WHERE IsActive = 1", con);
        //SqlDataAdapter da = new SqlDataAdapter(cmd);
        //da.Fill(ds);
        //return ds;

        //ds = utility.Display1("Execute Proc_LhmtSubscribe 'GET'");
        return ds;
    }

    [WebMethod]
    public bool DeleteLhmtSubscribe(int id)
    {
        string now = (DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day).ToString();
        int n = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE LHMTSUBSCRIBE SET ISACTIVE='" + 0 + "',DELETED_ON ='" + now + "' where ID = '" + id + "'", con);
        n = cmd.ExecuteNonQuery();
        con.Close();
        return (n > 0);
    }

    [WebMethod]
    public DataSet GetByIDLHMT(int Id)
    {
        DataSet ds = new DataSet();
        Utility utility = new Utility();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM LhmtSubscribe WHERE IsActive = 1 AND ID='" + Id + "'", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);
        return ds;       
    }

    [WebMethod]
    public bool UpdateLhmtSubscribe(int id, string name, string email, string contact)
    {
        string now = (DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.Today.Day).ToString();
        int n = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE LHMTSUBSCRIBE SET Name='" + name + "',Email='" + email + "',Contact='" + contact + "',Updated_On ='" + now + "' where ID = '" + id + "'", con);
        n = cmd.ExecuteNonQuery();
        con.Close();
        return (n > 0);
    }

    [WebMethod]
    public DataSet SendEmail(string Id)
    {
        DataSet ds = new DataSet();
        Utility utility = new Utility();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM LhmtSubscribe WHERE IsActive = 1 AND ID='" + Id + "'", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(ds);

        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        string name = dt.Rows[0]["Name"].ToString();
        string email = dt.Rows[0]["Email"].ToString();
        string contact = dt.Rows[0]["Contact"].ToString();

        TestEmail mail = new TestEmail();
        mail.EmailTestEmail(name, email, contact);

        return ds;
    }

    [WebMethod]
    public string SendEmail2(string name, string email, string contact)
    {
        string rr = "";
        TestEmail mail = new TestEmail();
        mail.EmailTestEmail(name, email, contact);

        return rr;
    }
    
}
