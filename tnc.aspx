﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="tnc.aspx.cs" Inherits="tnc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <script type="text/javascript" src="../js/tabcontent.js"></script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Terms and Conditions</div>
        </div>
        <div class="clear"></div>

        <div class="container text_format fade_anim">

            <div class="print_share">
                <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                <div class="addthis_native_toolbox"></div>
                <div class="clear"></div>
            </div>

            <p>
                <strong>LILAVATI HOSPITAL AND RESEARCH CENTRE (LILAVATI HOSPITAL)</strong> is a reputed hospital providing healthcare services to its patients, including online healthcare services to its registered patients through its web portal and mobile application . Usage of these applications to access these services are subject to the acceptance of these Terms of Use.
If you do not agree to these Terms of Use and/or are not eligible or authorized to agree to these Terms of use, then you are not authorized to use the applications and services supported by LILAVATI HOSPITAL. By using this application, you are accepting these Terms of Use and are legally bound by all terms in this Terms of Use agreement.
            </p>

            <p><strong class="green_text uppercase">User Accounts</strong></p>

            <p>As a registered patient of LILAVATI HOSPITAL you can access your patient user account and applicable services, using the user account details provided by LILAVATI HOSPITAL . Patients who choose to self-register through the patient portal or patient mobile application will need to wait until their registration is confirmed by LILAVATI HOSPITAL, before their account is active. By registering, you agree that:</p>
            <ul class="bullet">
                <li>You are not impersonating any other person and are using your actual identity.</li>
                <li>Your registration is subject to verification and confirmation by LILAVATI HOSPITAL and your patient user account will be activated only after confirmation.</li>
                <li>The personal information provided during the registration process is true, accurate, current and complete. You are required to periodically review and update your registration data to ensure that it is always accurate and correct.</li>
                <li>Your username and password are unique to you, and you agree not to disclose or share your username and password to or with any third party. You are responsible for keeping your password confidential and for notifying us immediately if your password has been hacked or stolen.</li>
                <li>You also agree that you will be solely responsible for any activities conducted on or through the patient portal and/or mobile application with your account regardless of whether or not you are the individual who undertakes such activities. This includes any unauthorized access and/or use of your account or your computer. You hereby release and hold harmless LILAVATI HOSPITAL from any and all claims and causes of action arising out of or resulting from a third party's unauthorized use of your account.</li>
                <li>LILAVATI HOSPITAL reserves the right to deny or revoke access to any user account at its sole discretion without any notice.</li>
                <li>This application is only a tool to allow patients of LILAVATI HOSPITAL to collaborate in their own healthcare, use scheduling and communication services and to access their personal health record maintained by LILAVATI HOSPITAL.</li>
                <li>All engagement between LILAVATI HOSPITAL and you through this application are governed by these Patient Terms.</li>
                <li>As a user of services through the patient portal and/or mobile application of LILAVATI HOSPITAL you agree to receive essential service related notifications in the form of SMS, email or mobile notifications. Notification settings to turn off non-essential notifications are available.</li>
                <li>You shall not use any service or information in this application for any purposes other than managing your personal health information or that of your immediate family members (with due authorization) and/or to engage with healthcare providers of LILAVATI HOSPITAL.</li>
                <li>You are expected to avail of immediate professional medical attention in medical emergencies or critical care situations and not attempt to use the application to access your healthcare provider. The services enabled by the application is only meant to supplement your existing healthcare services and to enable you to collaborate better with your own healthcare providers, as decided by them.</li>
                <li>Being an online application, there might be unforeseeable network and infrastructure issues in which case the application will not be available until the issue is rectified. Under such circumstances please directly contact LILAVATI HOSPITAL in person for any services you may need. LILAVATI HOSPITAL is not liable for any periods of non-availability of this application.</li>
                <li>LILAVATI HOSPITAL will make all possible efforts to provide you the best healthcare services. However, LILAVATI HOSPITAL shall not incur any liability and shall declare any service guarantee void in the event the user is in breach of any of the Terms and Conditions of this Agreement</li>
                <li>Both parties are at liberty to terminate this agreement. Terminating the agreement will cancel only your user accounts in the web and mobile applications of LILAVATI HOSPITAL. It does not terminate nor impact any other consultations or patient-provider relationship that you have with LILAVATI HOSPITAL.</li>
            </ul>


            <p><strong class="green_text uppercase">Data</strong></p>
            <ul class="bullet">
                <li>The application stores data, including PHI data, communications, usage data including but not limited to location, device type, operating system, browser, date-time and context usage, in Information Technology Act & Rules, HIPAA and GDPR compliant cloud infrastructure on Amazon Web services. All data storage and transmissions are encrypted.</li>
                <li>All your personal health data and usage data is owned by LILAVATI HOSPITAL irrespective of the access you may or may not have to it. Your usage and access to your data is defined by LILAVATI HOSPITAL</li>
                <li>The accuracy, integrity and completeness of the personal health information you enter or allow to enter on your behalf is your responsibility, and LILAVATI HOSPITAL assumes no liability for it.</li>
                <li>Data in your personal health records will be accessible to and modifiable by authorised healthcare providers of LILAVATI HOSPITAL , for the purpose of providing you with healthcare services.</li>
                <li>Non- personalised system analysis of non-identifiable patient data may be done to enable better healthcare outcomes for the patients of LILAVATI HOSPITAL . However the identity of individual users will remain completely anonymous in such analysis.</li>
                <li>All user data is covered by the Privacy Policy of LILAVATI HOSPITAL.</li>
            </ul>

            <p><strong class="green_text uppercase">Remote Monitoring Services</strong></p>
            <p>Our Remote Monitoring packages are only for communicating specific health parameters to the healthcare providers in LILAVATI HOSPITAL's patient portal and mobile applications and will not substitute actual clinical visits. You are required to visit the healthcare provider as and when specified by your doctor and whenever you need healthcare.</p>
            <ul class="bullet">
                <li>If the monitoring feedback provided by the healthcare provider is substantially different from your current treatment and prescribed medication, it is your duty to confirm the same before implementing the advice.</li>
                <li>The health information, vital sign readings, dates, timings and notes logged in by you or on your behalf should be accurate.</li>
                <li>As a patient of LILAVATI HOSPITAL, you are required to inform your healthcare provider of all current and previous illnesses, health conditions, surgeries, consultations and medications with any other healthcare providers. While our healthcare team may help to update your PHR based on mutual agreement, it is your responsibility to update all aspects of the PHR completely and correctly.</li>
                <li>For efficient monitoring and proper healthcare services, the patient is required to enter all vital health data through the system as per the schedule and level of detail advised by the doctor.</li>
                <li>For self-monitored readings, it is the patient's responsibility to use devices of proper quality and to ensure that they are re-calibrated when required.</li>
                <li>The healthcare team of the healthcare provider will consist of qualified healthcare professionals. The monitoring and feedback will be handled by any qualified member of the healthcare monitoring team.</li>
                <li>The healthcare provider will monitor your vitals at the intervals specified in your package. Not following the instructions of the healthcare team will be considered as non-compliance of the agreement and the agreement will be terminated.</li>
                <li>The healthcare team will provide instructions based on the vitals entered by you. You are advised to enter the vitals accurately.</li>
                <li>LILAVATI HOSPITAL will not assume any legal responsibility for the patient's medical condition and this online consultation only intends to aid in understanding the condition better based on the information given and is not meant to replace an in-person consultation</li>
                <li>The online system is expected to enhance, not replace clinic visits. It is your responsibility to schedule follow-up appointments as indicated by your doctor.</li>
                <li>The patient is free to discontinue using a Remote Monitoring plan at any time, with the knowledge of your consulting healthcare provider.</li>
                <li>The healthcare provider does not attend to Emergencies through this application. Under these circumstances, the patient has to contact the nearest hospital immediately.</li>
                <li>LILAVATI HOSPITAL may provide one or more Remote Monitoring plans. When signing up for or after being assigned to any such Remote Monitoring package, it is the duty of the patient to understand the instructions and terms of that package and to fulfil them.</li>
                <li>The patient data will be kept confidential by LILAVATI HOSPITAL and will not be disclosed to anyone under normal circumstances except as requested by law or in an emergency situation when requested by another healthcare provider.</li>
            </ul>

            <p><strong class="green_text uppercase">Video Consultations</strong></p>
            <ul class="bullet">
                <li><strong>Video Consultation Appointment Timing:</strong> While the patient may specify the preferred appointment timing for a Video Consultation, LILAVATI HOSPITAL reserves the right to reschedule the appointment for an alternative timing. If this timing is not convenient, the user can request that it be rescheduled. Due to unpredictable medical emergencies, it may be that the LILAVATI HOSPITAL's healthcare provider is sometimes unable to attend to the video consultation exactly at the appointed time. In such cases the session might be rescheduled to a later time in consultation with the patient.</li>
                <li><strong>Video Consultation Duration:</strong> By default the average duration of a Video Consultation will be the duration decided by the LILAVATI HOSPITAL's healthcare Provider LILAVATI HOSPITAL reserves the right to decide when the consultation ends. This could be earlier than the maximum possible duration. If the LILAVATI HOSPITAL's healthcare provider has to be conclude the video consultation mid-way due to a medical emergency, then a new session at an alternative timing will be scheduled.</li>
                <li><strong>Video Session Quality:</strong> The quality of the video session depends on the quality of the Internet connection, web cam and audio facilities of both the patient and LILAVATI HOSPITAL. It is the responsibility of the patient to ensure that the performance and quality of the equipment and connection used, are optimal for doing video calls online. Video Consultations that fail on account of poor equipment and connectivity will not be rescheduled for free and there shall be no refund of any payment made by you.</li>
                <li><strong>Video Session Privacy, Security & Etiquette:</strong> It is required that the patient prepares ahead of time to participate in the Video Consultation, in a quiet and private place, where only the patient is present in the room where the video session is being held. For elderly or disabled patients who require help to participate in the video, a caregiver may be present. There must be no audio/video recording of the video session by the patient. It is necessary that the patient (and any caregiver) act with proper decorum in speech and action during the video consultation. Failure to comply with these requirements can result in termination of the video session with no refund or rescheduling of the consultation.</li>
                <li><strong>Nature of Consultation:</strong> Video Consultations must not be scheduled for patients with critical medical conditions and/or for emergencies. Such consultations are suitable only for basic follow-ups between the patient and healthcare provider and/or preliminary consultation before meeting the healthcare provider in person.</li>
            </ul>

            <p><strong class="green_text uppercase">Text or Question Based Consultations</strong></p>
            <ul class="bullet">
                <li>Every attempt will be made to provide responses to your questions within the stipulated response time. However do not under any circumstances, delay seeking direct medical attention from your healthcare provider or nearest hospital.</li>
                <li>Asking medical questions to a healthcare provider, whom you have not been consulting directly, does not necessarily result in establishing a doctor-patient relationship. Any response from the healthcare provider only constitutes helpful information which must be verified with your own doctor.</li>
                <li>Do not change any medication, treatment regimen or other healthcare measures without confirmation from your own healthcare provider. Any advice or information in responses to your questions/text consultations to healthcare providers of LILAVATI HOSPITAL, do not qualify as a medical prescription, diagnosis or treatment plan.</li>
                <li>In the case of any emergency, always contact your own healthcare provider and go to the nearest hospital or healthcare provider.</li>
                <li>LILAVATI HOSPITAL is not liable for any healthcare issues related to the questions raised. Information provided at the time of asking a question must be accurate and complete. This is only to ensure that the response provided is helpful to the patient and does not involve a liability on part of LILAVATI HOSPITAL.</li>
                <li>Response time for a question is indicative of only the initial response to the question and does not include the time for responding to follow-up notes. Each follow-up should be expected to require the same response time as the initial question.</li>
                <li>Payments for this service are covered by the payment terms for services through the patient portal and mobile application.</li>
            </ul>

            <p><strong class="green_text uppercase">Payments</strong></p>
            <ul class="bullet">
                <li>All consultation fees for services through the mobile and web application interfaces of LILAVATI HOSPITAL have to be paid as per the terms of the service that you use. Offline payment collection for some of these services is supported too. Please contact LILAVATI HOSPITAL about this during your next in-person visit.</li>
                <li>In course of time, LILAVATI HOSPITAL may introduce other services in the system, the charges for which will be communicated to you as and when such services are introduced.</li>
                <li>The LILAVATI HOSPITAL reserves the right to revise the consultation and service charges at any time, at the sole discretion of the LILAVATI HOSPITAL. The charge applicable at any given time, will be the charge displayed at the time of using the service.</li>
                <li>Payments made for any remote monitoring plans will not be refunded should you decide to discontinue.</li>
            </ul>

            <p><strong class="green_text uppercase">Privacy Policy</strong></p>
            <p>Data in this application of LILAVATI HOSPITAL can be created by your healthcare providers at LILAVATI HOSPITAL and by yourself (the patient) after logging into the application with your authorized user account. Information that is collected as you use our web patient portal and mobile application includes the following:</p>
            <ul class="bullet">
                <li><strong>Personal information:</strong> Personal information includes name and email address that you provide at signup. It also includes phone number, address and other personally identifiable information that you may choose to add to your user account profile. No payment method information is stored by this application.</li>
                <li><strong>Personal Health Information:</strong> Personal health information about patients users, that patient users themselves or authorized employees of LILAVATI HOSPITAL enter into the Personal Health Record of a patient, including age, gender, known health conditions, medications, allergies, vaccinations, history of surgeries or procedures, goals, health journal entries and data that you enter into your health trackers either manually or by syncing with a health monitoring device or from a third party source that you alone authorize. It also includes medical reports and files that you choose to upload into your health profile.</li>
                <li><strong>Operations Data:</strong> In order to enhance your usage experience, the application uses cookies, server logs and other similar mechanisms to enable certain functionality, improve services and monitor service usage. These mechanisms are used to save user preferences, preserve session settings, help auto-authentication of frequently used services if required by your settings and to enable other similar functional requirements. You may use browser settings to disable cookies when using our web services, however it may result in some of the features and services of this application not functioning properly.</li>
            </ul>

            <p><strong class="green_text uppercase">Use of Information</strong></p>
            <p>
                The personal and health information added through this application, through use of its services by patients and healthcare providers of LILAVATI HOSPITAL will not be shared, disclosed or displayed to anyone other than authorized employees of LILAVATI HOSPITAL. They will be able to update personal health information of their own patients by updated the patient health profile, uploading medical reports, adding review notes, health goals, monitoring feedback relevant as part of the consultation services used by the patients.
LILAVATI HOSPITAL is fully authorized to terminate patient user accounts at any time and such patients will no longer have further access to their personal health information stored in the application storage in event of that happening. Whether or not to share that information with the patient user before termination of the patient user account is the prerogative of LILAVATI HOSPITAL.
            </p>
            <ul class="bullet">
                <li><strong>Contact Information:</strong> The services platform will send you communications (email and other notifications), as per your notification settings, as part of essential functioning of services that you may avail, such as appointment booking information, alerts related to reviews and consultations by LILAVATI HOSPITAL healthcare providers. At time of registration you will be required to verify your email address. Your contact information may also be used for customer support services, should you choose to avail of them. In addition, we may use your contact information to provide you with information about our services. If you decide at any time that you no longer wish to receive such information or communications from us, please change applicable notification settings in your account. Your contact information is not shared with any third-party.</li>
                <li><strong>Anonymized data:</strong> LILAVATI HOSPITAL may analyze anonymized and aggregated collected through the services, for purposes of evaluating healthcare outcomes, healthcare requirement patterns and to measure the effectiveness of services and content. Such anonymized and aggregated information is not considered as personal information. You are responsible for maintaining the confidentiality of your password and account information. You are responsible for all activities that occur in your account and you agree to notify LILAVATI HOSPITAL immediately of any unauthorized use of your account. LILAVATI HOSPITAL is in no way responsible for any loss that you may incur as a result of any unauthorized use of your user account and password.</li>
            </ul>

            <p><strong class="green_text uppercase">Disclosure of Information</strong></p>
            <p>We may disclose any information to government or law enforcement officials if we believe doing so is required to comply with law enforcement and legal process; to prevent or stop any illegal, unethical, or legally actionable activity; to protect your, our or others rights and safety.</p>
            <p>We may disclose your personal information on your own request accompanied by your permission.</p>
            <p>LILAVATI HOSPITAL does however reserve the right to deactivate or delete accounts of any user found to be in violation of our Terms of Use.</p>

            <p><strong class="green_text uppercase">Transmission and Storage Security</strong></p>
            <p>All data is stored only in the servers of our technology vendor on the highly secure Amazon Web Service platform. Necessary measures in providing secure transmission of personal health information before it is transferred across the Internet from your personal computer or devices are ensured. However, you should be aware of possible risk involved in transmitting information over the Internet as no data transmission can be guaranteed to be 100% secure and of risk that others could find a way to thwart our security systems. As a result, while we strive to protect your personal information, we cannot ensure or warrant the security and privacy of personal information you transmit to us, and you do so at your own risk.</p>

            <p><strong class="green_text uppercase">Restriction on the basis of age</strong></p>
            <p>All users of the patient portal and mobile app of LILAVATI HOSPITAL are expected to be above 18 years of age. Parents, caregivers/guardians and healthcare providers are allowed to provide and store personal information about others, including minors and children. Any user providing, storing or submitting information on behalf of a child/person under guardianship assumes full responsibility over the submission, use, and transmission of such information.</p>

            <p><strong class="green_text uppercase">Changes to this Policy</strong></p>
            <p>
                Please note that this Privacy Policy may change from time to time as we continue to offer evolve our services. In the event of any changes in our Privacy Policy, the updated version will be posted here and take effect from the moment of posting.
The above mentioned terms are applicable as long as you are a registered patient of LILAVATI HOSPITAL using LILAVATI HOSPITAL's web patient portal and mobile application.
            </p>

            <p><strong class="green_text uppercase">DISPUTES</strong></p>
            <p>The Parties (i.e.. yourself/patient and LILAVATI HOSPITAL) shall amicably resolve any and all disputes arising out of or in connection with Terms of Use in respect to the online healthcare services provided by LILAVATI HOSPITAL, failing which the disputes shall be settled by Sole Arbitrator <strong style="text-decoration: underline;">mutually selected and in default by an arbitrator appointed by court of competent jurisdiction at Mumbai and the arbitration will be conducted</strong> in accordance with the provisions of the Arbitration and Conciliation Act, 1996.. The seat and venue for arbitration shall be, exclusively, at <strong style="text-decoration: underline;">Mumbai, Maharashtra</strong> and the language for arbitration shall be English. The arbitration award shall be final and binding and shall be enforceable in any court of law having jurisdiction. <strong style="text-decoration: underline;">For all other purposes the courts at Mumbai will have exclusive jurisdiction.</strong> </p>


            <!-- Privacy & policy -->
            <p><strong class="green_text uppercase">Privacy Policy</strong></p>

            <p>The Users expressly understand, acknowledge and agree to avail the LILAVATI HOSPITAL ecare Tele-consultation services (the “Tele-consultation”) on the following terms and conditions set forth herein below:</p>
            <div class="tab_content">
                <ul class="bullet">
                    <li>User is above the age of 18 years and legally competent to enter into this agreement with us to avail our Tele-consultation services;</li>
                    <li>The use of Tele-consultation is not in violation of any applicable law or third party contracts executed by the User</li>
                    <li>In case any prescription is being provided to User by the Practitioner, the same is being provided basis the online consultation, however it may vary when examined in person, hence, in no event shall the prescription provided by Practitioners be relied upon as a final and conclusive solution.</li>
                    <li>
                        <p>The Users agree to use the advice from Practitioner on the Website pursuant to:</p>
                        <ul class="bullet">
                            <li>an ongoing treatment with their medical practitioner;</li>
                            <li>a condition which does not require emergency treatment, physical examination or medical attention;</li>
                            <li>medical history available as records with them for reference;</li>
                            <li>a record of physical examination and report thereof with them, generated through their local medical practitioner;</li>
                            <li>consultation with their medical practitioner before abandoning or modifying their ongoing treatment.</li>
                        </ul>
                    </li>
                    <li>The User agrees that by using Tele-consultation, the Practitioners on Tele-consultation will not be conducting physical examination of the Users, hence, they may not have or be able to derive important information that is usually obtained through a physical examination. User acknowledges and agrees that the User is aware of this limitation and agrees to assume the complete risk of this limitation.</li>
                    <li>The User understands that Tele-consultation shall not form a substitute for treatment that otherwise needs physical examination/immediate consultation. Further, the User understands that the advice provided by the Practitioner is based on general medical conditions and practices prevalent in India, to the best of his knowledge and ability, and not for conditions which are territory specific for regions other than India, irrespective of where the User is procuring medical services or engaging in communication with the Practitioner.</li>
                    <li>
                        <p>Users shall not persuade Practitioners to prescribe drugs (including higher dose strength) that do not conform to the Tele-consultation prescription policy. The restricted drugs are as follows:</p>
                        <ul class="bullet">
                            <li>Medication for Medical Termination Pregnancy (MTP)</li>
                            <li>Drugs under the following pharmaceutical classifications such as; sedatives, hypnotics, opioids, schedule X drugs, or fourth generation antibiotics.</li>
                        </ul>
                    </li>
                    <li>If restricted drugs are indicated for treatment or management of a disease or condition by a Practitioner, the User shall physically visit the Practitioner of their choice to confirm the requirements/necessity for prescribing such restricted drugs.</li>
                    <li>User understands and agrees to provide complete and accurate information that would aid the Practitioner in arriving at a diagnosis including but not limited to their clinical history, and will not use the Tele-consultation platform for any acts that are considered to be illegal in nature.</li>
                    <li>If Practitioner responds to the User’s query, the system could trigger communications to the User, in the form of notification/text/email/others. The User further understands that LILAVATI HOSPITAL may send such communications like text messages/email/calls before and/or after Practitioner’s consultation (physical or online) to User’s mobile number, based on the Practitioner’s settings (through the Website). However, and notwithstanding anything to the contrary in this Agreement, LILAVATI HOSPITAL does not take responsibility for timeliness of such communications.</li>
                    <li>User hereby consents to the recording of all communications (including audio, video and written communications) with our Practitioners</li>
                    <li>User hereby consents to the recording, saving and storing of all information pertaining to you provided to us or obtained during the course of providing the service, including without limitation, personal contact information, medical history, laboratory reports, communications with our Practitioner, and other patient records. </li>
                    <li>Any conversations and information that the Users have had with the Practitioner will be subject to doctor patient confidentiality and treated as such as per the applicable laws</li>
                    <li>The User hereby agrees to and grants consent to the LILAVATI HOSPITAL’s medical team with the right to audit his/her consultations on the Tele-consultation platform for the purpose of improving treatment quality and other related processes.</li>
                    <li>User shall refrain from raising any personal queries or advice on the Tele-consultation platform which are not related to a specific disease / medicine.</li>
                    <li>Users shall not use abusive language on the Tele-consultation platform. In the event of an abuse from the User is reported by a Practitioner, LILAVATI HOSPITAL reserves the right to block such Users from the Tele-consultation platform and LILAVATI HOSPITAL is not responsible for honouring any refund request towards his/her consultation on the Tele-consultation platform.</li>
                    <li>Users may share images or videos of the affected areas of their body parts with the Practitioner only if it is absolutely necessary for diagnosing his/her condition and if he/she is personally comfortable in sharing such images or videos. LILAVATI HOSPITAL shall not be responsible for any such images or videos shared by the Users with the Practitioners.</li>
                    <li>Users shall ensure that any interaction/communication with the Practitioners, including sharing images or videos of the body parts, shall be only through the Tele-consultation platform. The Users shall not rely on any other external modes of communication for interacting/communicating with the Practitioners.</li>
                    <li>Users shall be prepared to share all relevant documents or reports to the Practitioner promptly upon request.</li>
                    <li>For every paid consultation on the Tele-consultation platform, the Users shall not obtain consultation for more than one User. In the event, the Users attempt to obtain consultation for more than one User through a single paid consultation on the Tele-consultation platform, such consultations will not be addressed by the relevant Practitioner.</li>
                    <li>The User agrees and understands that the transaction with the Practitioner are subject to jurisdiction of Indian laws and that any claim, dispute or difference arising from it shall be subject to the jurisdiction provision as contained in the Terms and Conditions hereunder, at all times. The User further agrees and understands that the Practitioner is a medical practitioner who is licensed to practice medicine in India and the onus is on the User to determine if he/she is eligible to consult with the Practitioners via the Website. It is expressly clarified that at no point in time can it be construed that the Practitioner is practicing medicine in a territory other than India, irrespective of where the User is located and procures medical services or engages in communication with the Practitioner, in any manner whatsoever.</li>
                    <li>LILAVATI HOSPITAL, its affiliates, doctors, representatives or employees shall not be liable for any deficiency in service or loss, damages or adverse impact you may have incurred due to the breach of one or more of the terms and conditions (including the undertakings) by you.</li>
                    <li>LILAVATI HOSPITAL, its affiliates, doctors, representatives or employees shall not be liable for any error in diagnosis, delay in providing the service or deficiency in service due to the occurrence of sub optimal technical conditions (including without limitation, poor network connectivity, power shutdown, service downtime, scheduled maintenance work by the internet service providers.</li>
                    <li>LILAVATI HOSPITAL or its doctors or representatives will not be responsible for any misunderstanding or misinterpretation of any advice given by the doctors during the Service or of any terms and conditions herein.</li>
                    <li>LILAVATI HOSPITAL or its doctors or representatives will not be liable for misdiagnosis / faulty judgment / interpretation error / perception error/Adverse events/ inefficacy of prescribed treatment or advice/validity of the advice or prescription provided by the consulting doctor in your jurisdiction of residence/ unavailability of the recommended or prescribed treatment or medication under any condition or circumstances. Users are advised to use their discretion for following the advice obtained post consultation from our Online Medical Opinion platform.</li>
                    <li>In no event shall LILAVATI HOSPITAL or its doctors and representatives be liable for any direct, indirect, punitive, incidental, special or consequential damages or any damages whatsoever including without limitations, damages for loss of use, data or profits, arising out of or in any way connected with the use of our Services.</li>
                    <li>The User shall indemnify and hold harmless LILAVATI HOSPITAL and its affiliates, subsidiaries, directors, officers, employees and agents from and against any and all claims, proceedings, penalties, damages, loss, liability, actions, costs and expenses (including but not limited to court fees and attorney fees) arising due to or in relation to the use of Website by the User, by breach of the Terms or violation of any law, rules or regulations by the User, or due to such other actions, omissions or commissions of the User that gave rise to the claim.</li>
                    <li>The User shall make payment using the payment gateway to make payments online, solely at User's discretion. </li>
                    <li>The User understands that that any confirmed appointment post payment is only eligible for reschedule within 7 days and as per the Practitioner’s availability</li>
                    <li>Should there be any issues with regard to the payment not reaching the LILAVATI HOSPITAL account, the User may contact the support team: </li>
                    <li>This agreement may be terminated by either party at any time, with or without cause. </li>
                    <li>This agreement shall be governed by the laws of India and shall be subject to the exclusive jurisdiction of the courts at Mumbai
                                    LILAVATI HOSPITAL may at any time, without any prior notification to you, modify these terms of conditions. Please review the latest version of the terms and conditions before proceeding to avail the service</li>
                    <li>If any part of the agreement is determined to be invalid or unenforceable pursuant to applicable law then the said provision will be superseded by a valid, enforceable provision and the remainder of the agreement shall continue in effect</li>
                </ul>
                <div class="clear"></div>
            </div>


            <!-- Cancellation Policy -->
            <p><strong class="green_text uppercase">Cancellation and Refund policy</strong></p>

            <div class="newsbox accd_content c_expand_box">
                <p><strong class="green_text uppercase">Cancellation Policy</strong></p>
                <ul class="bullet">
                    <li>Appointments once confirmed and payments are successfully made cannot be cancelled by the Patient.</li>
                    <li>Appointments which are confirmed but payments are not made within stipulated time will automatically get cancelled and the respective Slot Time will get released without any further intimation to the Patient.</li>
                    <li>Appointments confirmed with successfully payments can only be cancelled / rescheduled by the Doctor on account of his inability to conduct the Tele Consultation due to any emergencies that he / she may have to attend to. However, Doctor will communicate his final decision to the Coordinators of the Hospital to contact the Patient and check whether the patient is agreeable to reschedule the date and time or initiate a complete cancellation and refund.</li>
                </ul>

                <p><strong class="green_text uppercase">Refund Policy</strong></p>
                <ul class="bullet">
                    <li>Payments received successfully through the Payment Gateway by the Hospital on account of confirmed appointment booking can be processed for refund, subject to above cancellation policy.</li>
                    <li>Refunds will be processed by the Payment Gateway and the credit of the amount should normally be given in the respective Patients Account within a period of 7-14 days from the date of initiating the process of Refund.</li>
                    <li>In case of any disputes related to Payments made or Tele Consultation with the Doctor for which the Patient is requesting a Refund of the Amount paid by them, a communication over email will be required to be sent to teledisputes@lilavatihospital.com and the Hospital will discuss the case with respective stakeholders involved and convey appropriately to the Patient as soon as possible. In case, the Patient is not satisfied with the response from the Hospital, they can contact the Coordinator on the Telephone Nos +91 9167025428 / +91 8879341577 and discuss the matter personally. The decision taken by the Hospital will be final and binding.</li>
                </ul>


                <div class="clear">
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="/js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="/js/FlowupLabels_plugin.js"></script>

    <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

