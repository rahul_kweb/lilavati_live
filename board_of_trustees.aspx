﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="board_of_trustees.aspx.cs" Inherits="board_of_trustees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Board of Trustees</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<p>The Lilavati Kirtilal Mehta Medical Trust is being managed and administered by the below board of Trustees, </p>

                <ul class="trustees">
                    <li>Shri. Prabodh K. Mehta </li>
                    <li>Shri. Kishor K. Mehta </li>
                    <li>Shri. Rashmi K. Mehta </li>
                    <li>Smt. Rekha H Sheth </li>
                    <li>Smt. Sushila V. Mehta </li>
                    <li>Smt. Charu K. Mehta </li>
                    <li>Shri. NanikRupani </li>
                    <li>Shri. K. K. Modi </li>
                    <li>Shri. Niket V. Mehta </li>
                    <li>Shri. Chetan P. Mehta </li>
                    <li>Shri. Bhavin R. Mehta </li>
                </ul>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">
                <InnerMenu:InnerMenu ID="menu" runat="server" />
            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

