﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pediatrics_and_paediatric_surgery : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    string docValue = string.Empty;
    string docValue1 = string.Empty;
    string docValue2 = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            BindBrochure();
            BindBanner();
        }
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Pediatrics & Paediatric Surgery";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        string dsc = dt.Rows[1]["Description"].ToString();

        dsc = dsc.Replace("&nbsp;", "");
        string val = dsc;

        dt.Rows[1]["Description"] = val;

        string dsc1 = dt.Rows[0]["Description"].ToString();

        dsc1 = dsc1.Replace("&nbsp;", "");
        string val1 = dsc1;

        dt.Rows[0]["Description"] = val1;

        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();

            rptContent1.DataSource = dt;
            rptContent1.DataBind();

        }
    }
    protected void rptContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdnDesc = (HiddenField)e.Item.FindControl("hdnDesc");
            PediatricDoctorDetails();
            PaediatricSurgeryDoctorDetails();

            StringBuilder val = new StringBuilder(hdnDesc.Value);
            val = val.Replace("PediatricDoctorDetails", docValue);
            val = val.Replace("PaediatricSurgeryDoctorDetails", docValue1);

            Label lblDesc = (Label)e.Item.FindControl("lblDesc");

            lblDesc.Text = val.ToString();
        }
    }

    public void PediatricDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,22");
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }


            //strBuil.Append("<li>");


            ////drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "' class='doc_div'>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");      
        }

        docValue = strBuil.ToString();
    }

    public void PaediatricSurgeryDoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,46");
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }


            //strBuil.Append("<li>");


            ////drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "' class='doc_div'>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");

            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");      
        }

        docValue1 = strBuil.ToString();
    }

    public void BindBrochure()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM PATIENTS_EDUCATION WHERE ISACTIVE = 1 AND Pat_Id IN (4,6,9,10,18)", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);

        if (dt.Rows.Count > 0)
        {
            rptBrochure.DataSource = dt;
            rptBrochure.DataBind();
        }
        else
        {
            rptBrochure.DataSource = null;
            rptBrochure.DataBind();
        }
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(40);
    }
}