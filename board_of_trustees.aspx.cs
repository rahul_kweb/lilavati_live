﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class board_of_trustees : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
        }
    }

    public void Content()
    {
        cms.ID = 3;
        cmsbal.GETBYIDContent(cms);
        litContent.Text = cms.Description;       
    }
}