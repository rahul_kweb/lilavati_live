﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="dental.aspx.cs" Inherits="dental" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Dental</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Dental.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%--     <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Dental Department of Lilavati hospital is a very modern fully equipped department with most advanced treatments being carried out by very selected group of Doctors.</p>
                            <p>Department consists of two well equipped, most modern operatories with latest chair and unit</p>
                            <p>It caters to on daily basis 15 to 20 high level supra-major treatments delivered by various specialists who are attached to the department.</p>
                            <p>It is located in most comfortable location on the ground floor and works as a unit of its own with its own consulting cabin and  personalised waiting area.</p>
                            <p>It has team of 9 Doctors who work in rotation and see patients on daily basis from 8 to 8 every day.</p>
                            <p>The treatments offered are of highly specialised nature and cater to all super specialities of Dentistry.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Endodontics</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Root canal Treatment</li>
                                        <li>Repeat Root Canal Treatment</li>
                                        <li>Peroration sealing</li>
                                        <li>Removal of Broken Instruments</li>
                                        <li>Post and core Buld Up</li>
                                        <li>Treatment of Incompletely formed or Open Apex</li>
                                        <li>Peri apical surgery</li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Oral and Maxillofacial surgery</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Surgical Extraction of Impacyted wisdom teeth</li>
                                        <li>Extraction of complex teeh and Medically compromised teeth.</li>
                                        <li>Treatment of Jaw fractures</li>
                                        <li>Surgery for Cysts and Tumours and other Jaw disdorders</li>
                                        <li>Surgical correction of Teeth alignment </li>
                                        <li>Treatment of Temporomandibular joints</li>
                                        <li>Treatment of Oral Lesions</li>
                                        <li>Biopsy</li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Implantology</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Routine Dental implants for fixed teeth</li>
                                        <li>Full mouth Rehabilitation with implants</li>
                                        <li>Implant supported dentures</li>
                                        <li>Bone augmentation surgery for placement of Implants</li>
                                        <li>Sinus augmentation</li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Paediatric Dentistry</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Full Dental Treatment of Milk teeth and Permanent teeth for children</li>
                                        <li>Preventive Treatment to avoid cavities</li>
                                        <li>Full dental treatment under General anaesthesia</li>
                                        <li>Early identification of future problems like misaligned teeth, Jaw deformities, anomalies of teeth like flourosis</li>
                                        <li>Guidance for taking good care of teeth</li>
                                        <li>Diet counselling to avoid dental problems</li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Orthodontic treatment</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Correction of Mal-aligned teeth</li>
                                        <li>Early identification and correction of teeth and Jaw alignment</li>
                                        <li>Breaking habits like thumb sucking</li>
                                        <li>Variety of Braces for teeth alignment and smile enhancement</li>
                                        <li>Invisaline treatment</li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="accd_box">
                                <div class="hd c_expand_hd">General Dental treatment</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <ul class="bullet">
                                        <li>Crown and Bridge of all types</li>
                                        <li>Tooth coloured fillings</li>
                                        <li>Teeth whitening</li>
                                        <li>Teeth cleaning</li>
                                        <li>Routine extractions</li>
                                        <li>Full and Partial Denture treatments</li>
                                        <li>Full mouth Rehabilitation</li>
                                        <li>Smile design</li>
                                        <li>Veneers</li>
                                    </ul>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <p>The Root canal treatments are carried out with the help of Microscope and delivered as painless treatments. At the same the Endodontists attached to the department also cater to complex Root canals  and solve problem areas like Removal of Broken files from canals, Repeat Rootcanals of failed cases , Perforations etc . All these treatments require lot of modern  Endodontic gadgets which the department is full of and the specialists have no limitations due to lack of specialised instruments.</p>
                            <p>The Implantologist delivers most modern implants and evn complex implant related surgeries with ease due to the facility.</p>
                            <p>The paediatric dentist takes good care of children and they love to visit department again and again. He also has the facility to use most modern Operation theatre in case the children  don’t cooperate.</p>
                            <p>The orthodontist delivers most aesthetic treatment of Teeth alignment with latest braces treatment.</p>
                            <p>The Oral surgeon  carries out most complex Maxillofacial surgeries and also has the facility to utilise OT and admission facilities.</p>
                            <p>Like this all the modern dental treatment is carried out by various specialists and general Dentists.</p>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">

                            <p>The infrastructure comprises of two latest dental chairs from stern Webber, Italy.</p>
                            <p>It has its own Digital x-rays attached to both chairs and modern DC x-ray machines. So an  x-ray can be immediately viewed on screen and discussed with the patient.</p>
                            <p>Beside that the patient can view his teeth with intraoral camera on the screen so they have better understanding of these teeth.</p>
                            <p>Quick enhancement of smile can be done by a Powerful Bleaching Machine.</p>
                            <p>An Panoramic x-ray is also available to see the complete look of the teeth and arrive at a proper diagnosis and treatment plan.</p>
                            <p>The entire range of treatment is delivered with complete guarantee of sterilization and record keeping Making patient feel totally safe.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>
            <div class="box_aside">
                <menu:Services ID="menu" runat="server" />
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

