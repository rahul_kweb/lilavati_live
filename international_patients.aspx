﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="international_patients.aspx.cs" Inherits="international_patients" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">International Patients</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/International_Patients.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<div class="tabber">

                    

                    <%--<asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>--%>

                <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Lilavati Hospital & Research centre is world renowned for its facilities and care. Hence, we cater to several patients from different parts of the world. The services availed by international patients include health check-up, in patients & out patients. On an average, 60 international patients are admitted monthly.</p>
                            <p>We have also tied-up with several international service providers who provide overseas medical coverage to foreign patients. With NABH accreditation, the patients are rest assured to receive facilities and services of international standard.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Contact</h2>

                        <div class="tab_content">
                            <p>A dedicated email ID for international patients: <strong><a href="mailto:lilaworld@lilavatihospital.com">lilaworld@lilavatihospital.com</a></strong></p>

                            <ul>
                                <li>Get prompt response for any query.</li>
                                <li>Send in your medical details and history to get an appointment with the right specialist doctor.</li>
                                <li>Get appointments for health check up.</li>
                            </ul>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Letter for Visa</h2>

                        <div class="tab_content">
                            <p>The letter of invitation for Visa is sent once the medical appointment is confirmed and the travel arrangements finalized. A scan copy of the invitation letter is sent to the patient for submission to the respective embassy for approval.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Class of Accommodation</h2>

                        <div class="tab_content">
                            <p>All international patients are given a choice of accommodation from twin sharing and above.</p>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Billing and Payment</h2>

                        <div class="tab_content">
                            <ul>
                                <li>A surcharge of 20% is levied on the final bill of all foreign nationals & NRI’s.</li>
                                <li>You could make a payment via Cash, Credit Card & RTGS.<br>
                                    (Please call the billing department at 022-26561586 (for inpatient billing) & 022-26561585 (OPD billing) for bank details.)</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>---

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

