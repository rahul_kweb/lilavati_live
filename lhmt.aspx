﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="lhmt.aspx.cs" Inherits="lhmt" %>

<%@ Register TagName="Professionals" TagPrefix="menu" Src="Control/professionals.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
        });
    </script>

    <script>
        function CheckSave() {
            var name = $('#<%=txtName.ClientID%>').val();
            var email = $('#<%=txtEmail.ClientID%>').val();
            var contact = $('#<%=txtContact.ClientID%>').val();
            var blank = false;

            if (name == '') {
                $("#nm").addClass("error");
                blank = true;
            }
            else {
                $("#nm").removeClass("error");
            }

            if (email == '') {
                $("#em").addClass("error");
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(email)) {
                    $("#em").addClass("error");
                    alert("Valid Email ID");
                    blank = true;
                }
                else {
                    $("#em").removeClass("error");
                }
            }

            if (contact == '') {
                $("#cnt").addClass("error");
                blank = true;
            }
            else {
                if (contact.length < 10) {
                    $("#cnt").addClass("error");
                    blank = true;
                }
                else {
                    $("#cnt").removeClass("error");
                }
            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>

    <script>
        $(document).ready(function () {

            function NameFun() {
                var name = $('#<%=txtName.ClientID%>').val();
                var blank = false;

                if (name == '') {
                    $("#nm").addClass("error");
                    blank = true;
                }
                else {
                    $("#nm").removeClass("error");
                }

                if (blank) {
                    return false;
                }
                else {
                    return true;
                }
            }

            function EmailFun() {
                var email = $('#<%=txtEmail.ClientID%>').val();
                var blank = false;

                if (email == '') {
                    $("#em").addClass("error");
                    blank = true;
                }
                else {
                    var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                    if (!EmailText.test(email)) {
                        $("#em").addClass("error");
                        alert("Valid Email ID");
                        blank = true;
                    }
                    else {
                        $("#em").removeClass("error");
                    }
                }

                if (blank) {
                    return false;
                }
                else {
                    return true;
                }
            }

            function ContantFun() {
                var contact = $('#<%=txtContact.ClientID%>').val();
                var blank = false;
                console.log(contact.length);
                if (contact == '') {
                    $("#cnt").addClass("error");
                    blank = true;
                }
                else {
                    if (contact.length < 10) {
                        $("#cnt").addClass("error");
                        blank = true;
                    }
                    else {
                        $("#cnt").removeClass("error");
                    }
                }

                if (blank) {
                    return false;
                }
                else {
                    return true;
                }
            }

            $(".nm").click(function () {
                NameFun();
            });
            $(".nm").keypress(function () {
                NameFun();
            });
            $(".nm").change(function () {
                NameFun();
            });


            $(".em").click(function () {
                EmailFun();
            });
            $(".em").change(function () {
                EmailFun();
            });


            $(".cnt").click(function () {
                ContantFun();
            });
            $(".cnt").keypress(function () {
                ContantFun();
            });
            $(".cnt").change(function () {
                ContantFun();
            });

            $('html').keyup(function (e) { if (e.keyCode == 8) ContantFun(); })

        });
    </script>

    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function ShowPopUp() {
            $(function () {
                $("#showmsg").fancybox().trigger('click');
            });
        }

        $(document).ready(function () {
            $(".btnfun").click(function () {
                var name = $('#<%=txtName.ClientID%>').val("");
                $("#nm").removeClass("populated");
                $("#nm").removeClass("error");

                var email = $('#<%=txtEmail.ClientID%>').val("");
                $("#em").removeClass("populated");
                $("#em").removeClass("error");

                var contact = $('#<%=txtContact.ClientID%>').val("");
                $("#cnt").removeClass("populated");
                $("#cnt").removeClass("error");
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Lilavati Hospital Medical Times</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>


                <p>Lilavati Hospital Medical Times (LHMT),our quarterly medical magazine provides useful information on healthcare through various case reports of patients treated at our hospital .It also offers relevant information on existing developments in our hospital,our consultants achievements,educational activities etc. The fundamentals of LHMT lie in the fact that it throws enormous light on the medical compassion of the clinicians to empower their skills to make a healthier society.</p>

                <div class="clear"></div>

                <ul class="lhmt fade_anim">

                    <asp:Repeater ID="rptLHMT" runat="server">
                        <ItemTemplate>
                            <li><a href='<%#Eval("BROUCHRE","uploads/lhmt/{0}") %>' target="_blank">
                                <img src="images/spacer.gif" />
                                <span><%#Eval("TITTLE") %> </span></a></li>
                        </ItemTemplate>
                    </asp:Repeater>


                    <%--<li><a href="docs/LHMT/MEDICAL TIMES_A3_001.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>February, 2016</span> </a></li>
                        <li><a href="docs/LHMT/Lilavati_Hospital_Medical_Times_Issue_September_2015.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>September, 2015</span> </a></li>
                        <li><a href="docs/LHMT/LHMT_April_issue.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>April, 2015</span> </a></li>
                        <li><a href="docs/LHMT/LilavatiTimesIssue_5.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>November, 2014</span> </a></li>
                        <li><a href="docs/LHMT/Lilavati Times - Issue -4 (25-04-14)-new (29-05-14).pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>June, 2014</span> </a></li>
                        <li><a href="docs/LHMT/LilavatiTimesfinalissue.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>January, 2014</span> </a></li>
                        <li><a href="docs/LHMT/MEDICAL TIMES_V2_Spt.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>September, 2013</span> </a></li>
                        <li><a href="docs/LHMT/MEDICAL TIMES_A3_001.pdf" target="_blank">
                            <img src="images/spacer.gif">
                            <span>June, 2013</span> </a></li>--%>
                </ul>

                <div class="clear"></div>

                <p><a href="#subscribe_form" class="submit_btn fade_anim fancybox btnfun" style="font-size: 14px; color: #FFF;">Subscribe to LHMT</a></p>

                <div id="subscribe_form" style="display: none">

                    <div class="subscribe_form_div">

                        <div class="apply_form_tl uppercase green_text">Subscribe to LHMT</div>
                        <div class="clear"></div>

                        <div id="form" class="FlowupLabels subscribe_fields text_format">

                            <p>Provide the below details to subscribe:</p>

                            <div class="form_field fl_wrap" id="nm">
                                <label class="fl_label uppercase">Name</label>
                                <asp:TextBox ID="txtName" runat="server" class="fl_input nm" autocomplete="off"></asp:TextBox>
                            </div>

                            <div class="form_field fl_wrap" id="em">
                                <label class="fl_label uppercase">Email</label>
                                <asp:TextBox ID="txtEmail" runat="server" class="fl_input em" autocomplete="off"></asp:TextBox>
                            </div>

                            <div class="form_field fl_wrap" id="cnt">
                                <label class="fl_label uppercase">Contact No.</label>
                                <asp:TextBox ID="txtContact" runat="server" class="fl_input cnt" MaxLength="10" onKeyPress='javascript:return onlyNumbers();' autocomplete="off"></asp:TextBox>
                            </div>

                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" class="submit_btn fade_anim uppercase" OnClientClick='javascript:return CheckSave();' Style="cursor: pointer;" />

                        </div>

                        <div class="clear"></div>
                    </div>

                </div>

                <div id="showmsg" style="display: none">
                    <div class="subscribe_form_div">
                        <div class="apply_form_tl uppercase green_text">Subscribe to LHMT</div>
                        <div class="subscribe_thanks text_format">
                            <p><strong class="green_text uppercase">Thank you, for your interest!</strong></p>
                            <p>Our next edition of the LHMT will be send to you on your registered email id.</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Professionals ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="js/jquery.FlowupLabels.js"></script>
    <script type="text/javascript" src="js/FlowupLabels_plugin.js"></script>

</asp:Content>

