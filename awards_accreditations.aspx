﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="awards_accreditations.aspx.cs" Inherits="awards_accreditations" %>

<%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>

    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox();

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Awards & Accreditations</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <p style="font-size: 16px;">“Efforts and hardwork put in by team Lilavati Hospital has resulted in various awards and accolades”</p>

                <div class="inner_banner">
                    <img src="/uploads/home_banner/Horizontal award pic.jpg">
                </div>

                <div class="tabber">

                    <div class="tabbertab">
                        <h2>Awards</h2>

                        <div class="tab_content">

                            <asp:Repeater ID="rptAwards" runat="server">
                                <ItemTemplate>
                                    <div class="accd_box">
                                        <div class="hd c_expand_hd"><%#Eval("AWARDS_TITTLE") %></div>
                                        <div class="newsbox accd_content c_expand_box">
                                            <p>
                                                <img src='<%#Eval("AWARDS_IMAGE","uploads/awards/{0}") %>' class="awards_img">
                                            </p>
                                            <%#Eval("AWARDS_DESCRIPTION") %>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="clear"></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>

                           

                            <%-- <div class="accd_box">
                                <div class="hd c_expand_hd">Our COO, Lt. Gen. Dr. V Ravishankar VSM (Retd.), with the award for India’s Most Trusted Brand- 2015.</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>
                                        <img src="images/awards/1.jpg" class="awards_img">
                                    </p>
                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <!-- xxxxxx -->

                            <div class="accd_box">
                                <div class="hd c_expand_hd">India's Best Multi Speciality Hospital in Metro</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>
                                        <img src="images/awards/2.jpg" class="awards_img">
                                    </p>

                                    <p>Lilavati Hospital & Research Centre has been awarded as the ‘Best Multi Speciality Hospital in Metro’ by ICICI Lombard & CNBC TV 18 in ‘India Healthcare Awards 2012’ which were held in New Delhi.</p>

                                    <ul class="bullet">
                                        <li>Total Entries: 2100</li>
                                        <li>Shortlisted: 160</li>
                                        <li>Award Categories: 20</li>
                                        <li>Winners: 32</li>
                                    </ul>

                                    <p><strong class="uppercase">Selection Criteria</strong></p>
                                    <ul>
                                        <li>No. of patients treated in the hospital</li>
                                        <li>No. of beds in the hospital</li>
                                        <li>No. of doctors, nurses & medical specialities in the hospital</li>
                                        <li>Hospital infrastructure</li>
                                        <li>Customer Service</li>
                                    </ul>

                                    <p><strong class="uppercase">Selection Process Survey</strong></p>
                                    <ul>
                                        <li>300 General Practitioners</li>
                                        <li>333 Specialists (Min 10 yrs Experience)</li>
                                        <li>2400 Patients</li>
                                        <li>300 Administrators</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <!-- xxxxxx -->

                            <div class="accd_box">
                                <div class="hd c_expand_hd">The Week No.1 Super-Speciality Hospital in Mumbai.</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>
                                        <img src="images/awards/4.jpg" class="awards_img">
                                    </p>

                                    <p><strong>The Week-Nielsen Research Survey</strong></p>
                                    <p>This research was conducted in 15 Indian cities which included Delhi, Mumbai, Kolkata, Chennai & Hyderabad.</p>
                                    <p>In the first Phase of the survey, 22 specialists & 20 general practitioners were surveyed from the four metros, Mumbai, Delhi NCR, Chennai & Kolkata.</p>
                                    <p>The phase 2 of the research involved face to face, telephonic & online interviews of 1,163 doctors across 15 cities of select specialities.  The criteria; these doctors would have minimum 5 years of experience in their respective specialities. Also, 306 General Practitioners & 850 specialists gave their opinion.</p>

                                    <p>Below are the parameters used for the ranking:</p>
                                    <ul class="bullet">
                                        <li>Competency of the doctors</li>
                                        <li>Infrastructure</li>
                                        <li>Availability of multi-speciality</li>
                                        <li>Patient care</li>
                                        <li>Innovation in treatment</li>
                                        <li>Overall reputation and hospital environment</li>
                                    </ul>

                                    <p><strong>Ranking methodology</strong></p>
                                    <ul class="bullet">
                                        <li>For the best multi-speciality hospital in India<br>
                                            The experts nominated top five multi-speciality hospitals in India and ranked as well as rated them on seven different attributes.</li>
                                        <li>For the best multi-speciality in the city<br>
                                            Experts also nominated top five multi-specialty hospitals in their respective cities based on research facilities and outputs and in terms of staff training programme. Their views were also sought on development related to health care industry.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <!-- xxxxxx -->

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Lilavati Hospital among the Top 10 Hospitals in India</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>
                                        <img src="images/awards/7.jpg" class="awards_img">
                                    </p>
                                    <p><strong>The Week/ Hansa Research Survey</strong></p>
                                    <p>This survey was conducted in 15 Indian Cities Delhi, Mumbai, Chennai, Bangalore, Kolkata, Hyderabad, Ludhiana, Lucknow, Pune, Ahmadabad, Nagpur, Indore, Coimbatore, Kochi and Thiruvananthapuram.</p>
                                    <p>399 General Practitioners and 438 specialists gave their opinion for the hospitals listed in the survey.</p>
                                    <p>Below mentioned are the parameters used for the ranking,</p>
                                    <ul class="bullet">
                                        <li>Competency of the doctors</li>
                                        <li>Infrastructure</li>
                                        <li>Availability of multi-speciality</li>
                                        <li>Patient care</li>
                                        <li>Innovation in treatment</li>
                                        <li>Overall reputation and hospital environment</li>
                                    </ul>

                                    <p><strong>Ranking methodology</strong></p>

                                    <ul class="bullet">
                                        <li><u>For the best multi-speciality hospital in India</u>
                                            <br>
                                            The experts nominated top five multi-speciality hospitals in India and ranked as well as rated them on seven different attributes.</li>
                                        <li><u>For the best multi-speciality in the city</u>
                                            <br>
                                            Experts also nominated top five multi-specialty hospitals in their respective cities based on research facilities and outputs and in terms of staff training programme. Their views were also sought on development related to health care industry.</li>
                                    </ul>


                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <!-- xxxxxx -->

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Lilavati Hospital among the Top 10 Hospitals in India - 2013</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>
                                        <img src="images/awards/9.jpg" class="awards_img">
                                    </p>
                                    <p><strong>The Week-Nielsen Research Survey</strong></p>
                                    <p>This research was conducted in 15 Indian cities which included Delhi, Mumbai, Kolkata, Chennai and Hyderabad.</p>
                                    <p>In the first Phase of the survey, 22 specialists & 20 general practitioners were surveyed from the four metros, Mumbai, Delhi NCR, Chennai and Kolkata.</p>
                                    <p>The phase 2 of the research involved face to face, telephonic & online interviews of 1,163 doctors across 15 cities of select specialities.  The criteria; these doctors would have minimum 5 years of experience in their respective specialities. Also, 306 General Practitioners & 850 specialists gave their opinion.</p>

                                    <p>Below mentioned are the parameters used for the rankings,</p>
                                    <ul class="bullet">
                                        <li>Competency of the doctors</li>
                                        <li>Infrastructure</li>
                                        <li>Availability of multi-speciality</li>
                                        <li>Patient care</li>
                                        <li>Innovation in treatment</li>
                                        <li>Overall reputation and hospital environment</li>
                                    </ul>

                                    <p><strong>Ranking methodology</strong></p>

                                    <ul class="bullet">
                                        <li><u>For the best multi-speciality hospital in India</u>
                                            <br>
                                            The experts nominated top five multi-speciality hospitals in India and ranked as well as rated them on seven different attributes.</li>
                                        <li><u>For the best multi-speciality in the city</u>
                                            <br>
                                            Experts also nominated top five multi-specialty hospitals in their respective cities based on research facilities and outputs and in terms of staff training programme. Their views were also sought on development related to health care industry.</li>
                                    </ul>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>

                            <!-- xxxxxx -->

                            <div class="accd_box">
                                <div class="hd c_expand_hd">Gold winner of Reader’s Digest Trusted Brand Award 2012</div>
                                <div class="newsbox accd_content c_expand_box">
                                    <p>
                                        <img src="images/awards/6.jpg" class="awards_img">
                                    </p>
                                    <p>Reader’s Digest most trusted Brand Award2012 was the 14th year of these awards, which are conducted based on feedback from consumer surveys.</p>
                                    <p>Questionnaires & direct feedback was sought from Asia’s key consumer markets, China, India, Hong Kong, Malaysia, Philippines, Singapore, Taiwan & Thailand.  43 different categories of products & services were taken into account for this survey,</p>

                                    <p><strong>The 6 attributes for the rating:</strong></p>
                                    <ul class="bullet">
                                        <li>Value</li>
                                        <li>Quality</li>
                                        <li>Trustworthiness & Credibility</li>
                                        <li>Understanding of consumer needs</li>
                                        <li>Innovation</li>
                                        <li>Social responsibility</li>
                                    </ul>

                                    <p><strong>Questions </strong></p>
                                    <ul class="bullet">
                                        <li>Is the brand believable, safe to use & does it consistently deliver on the promises it makes?</li>
                                        <li>Are the Brand’s Products or Services well-made or well designed?</li>
                                        <li>Does the Brand offer good value for Money?</li>
                                        <li>Does the Brand respond to and satisfy consumers’ changing needs?</li>
                                        <li>Does the Brand regularly introduce product, features or services?</li>
                                        <li>Does the Brand support the Community, employees, natural environment and practice good corporate ethics?</li>
                                    </ul>

                                    <p><strong>Ranking Methodology</strong></p>
                                    <ul class="bullet">
                                        <li>The final rankings were arrived at by multiplying the number of consumers choosing the brand with the scores on these qualitative factors and incorporated population weighing factors to ensure the statistical accuracy of the results.</li>
                                        <li>These awards were given in each of these markets as well as for the whole of Asia.</li>
                                    </ul>

                                    <p><strong class="green_text">The Gold Trusted Brand award goes to category-leading brand/s that scores significantly higher than their rivals.</strong></p>

                                    <div class="clear"></div>
                                </div>

                                <div class="clear"></div>
                            </div>--%>

                            <!-- xxxxxx -->

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--<div class="tabbertab">
                        <h2>Accreditations</h2>

                        <div class="tab_content">

                            <p>
                                <img src="images/awards/8.jpg" class="awards_img">
                            </p>
                            <p><strong class="green_text uppercase">NABH ACCREDITATION</strong></p>
                            <p>NABH is an acronym for National Accreditation Board for Hospitals & Healthcare Providers. NABH is equivalent to JCI and other International standards including HAS: Haute Authorite de Sante, Australian Council on Healthcare Standards, the Japan Council for Quality in Health Care, and the National Committee for Quality Assurance in the United States. Its standards have been accredited by ISQUA*. The approval of ISQua authenticates that NABH standards are in consonance with the global benchmarks set by ISQua. Hence making NABH accreditation at par with the world’s most leading hospital accreditations.</p>
                            <p>NABH is a constituent Board of Quality Council of India (QCI), set up with co-operation of the Ministry of Health & Family Welfare, Government of India and the Indian Health Industry. This Board caters to the much desired needs of the consumers and has set standards for progress of the health industry. The board is set up to establish and operate accreditation programme for healthcare organizations. NABH have designed an exhaustive healthcare standard for hospitals and healthcare providers. This standard consists of stringent 600 plus objective elements for the hospital to achieve in order to get the NABH accreditation. These standards are divided between patient centered standards and organization centered standards. NABH accreditation helps planners to promote, implement, monitor and evaluate robust practice in order to ensure that it occupies a central place in the development of the health care system. Thus the objective of NABH accreditation is on continuous improvement in the organizational and clinical performance of health services, not just the achievement of a certificate or award or merely assuring compliance with minimum acceptable standards.</p>

                            <p>
                                <a href="images/awards/nabh_1.jpg" class="fancybox">
                                    <img src="images/awards/nabh_1.jpg" class="certf_img"></a>
                                <a href="images/awards/nabh_2.jpg" class="fancybox">
                                    <img src="images/awards/nabh_2.jpg" class="certf_img"></a>
                            </p>

                            <p><strong class="green_text uppercase">How does NABH accreditation help patients?</strong></p>
                            <ul class="bullet">
                                <li>Patients Rights & Benefits</li>
                                <li>Patients Safety</li>
                                <li>Control & Prevention of infections</li>
                                <li>Practicing good patient care protocols</li>
                                <li>Better & controlled clinical outcome</li>
                            </ul>
                            <p>Effectively, an NABH accreditation means that the hospital follows stringent standards and meets international benchmarks.</p>




                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>


                <div style="display: none;">

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/1.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">Our COO, Lt. Gen. Dr. V Ravishankar VSM (Retd.), with the award for India’s Most Trusted Brand- 2015.</h2>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/2.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">India’s Best Multi-speciality Hospital in Metro</h2>
                        </div>
                        <a href="awards_1.html"></a>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/3.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">India's Best Multi Speciality Hospital Megapolis Award 2013</h2>
                        </div>
                        <a href="awards_2.html"></a>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/4.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">The Week No. 1 Super-speciality Hospital in Mumbai.</h2>
                        </div>
                        <a href="awards_3.html"></a>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/5.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">Runner-Up in the category India’s Most Popular Maternity Hospital.</h2>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/6.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">Gold winner of Reader’s Digest Trusted Brand Award 2012</h2>
                        </div>
                        <a href="awards_4.html"></a>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/7.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">Lilavati Hospital among the Top 10 hospitals in India</h2>
                        </div>
                        <a href="awards_5.html"></a>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/8.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">NABH Accreditation</h2>
                        </div>
                        <a href="awards_6.html"></a>
                        <div class="clear"></div>
                    </div>

                    <div class="award_box">
                        <div class="award_img">
                            <img src="images/awards/9.jpg">
                        </div>
                        <div class="award_text">
                            <h2 class="">Lilavati Hospital among the Top 10 hospitals in India - 2013</h2>
                        </div>
                        <a href="awards_7.html"></a>
                        <div class="clear"></div>
                    </div>

                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
</asp:Content>

