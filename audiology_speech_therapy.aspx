﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="audiology_speech_therapy.aspx.cs" Inherits="audiology_speech_therapy" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Audiology & Speech Therapy</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Audiology.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <%-- <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>This department began operations in 1998 and since then has been committed to providing exceptional services in diagnosing and treating all problems related to speech, voice & hearing. The department boasts of leading consultant audiologist and speech therapists.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Audiometry</li>
                                <li>Sound proof set up</li>
                                <li>Pure Tune</li>
                                <li>Impedance</li>
                                <li>Speech Audiometry</li>
                                <li>Speech Tests</li>
                                <li>OAE</li>
                                <li>Speech Therapy</li>
                                <li>Stag Voice, Delayed language, Misarticualtions, stroke, degenerative motor</li>
                                <li>Hearing Cuds</li>
                                <li>Fitting</li>
                                <li>Cochler Implants</li>
                                <li>Preoperative Assessment and Post-operative rehabilitation</li>
                                <li>Preoperative and Post-operative rehab</li>
                                <li>Counseling</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                        <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

