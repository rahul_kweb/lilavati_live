﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="critical_care.aspx.cs" Inherits="critical_care" %>

<%@ Register TagName="Service" TagPrefix="menu" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Critical Care</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/critical_care.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server" OnItemDataBound="rptContent_ItemDataBound">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <asp:Label ID="lblDesc" runat="server" Text='<%#Eval("DESCRIPTION") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnDesc" runat="server" Value='<%#Eval("DESCRIPTION") %>' />
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%-- <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">

                            <p>The department of Critical Care consists of 63 beds along with a high dependency unit. The team part of this department comprises of Chief Intensivists, Junior Intensivists, Senior & Junior Doctors and also a highly efficient, experienced and well trained nursing & paramedical staff. </p>

                            <p class="hd green_text uppercase"><strong>Facilities</strong></p>
                            <ul class="bullet">
                                <li>Multichannel monitoring including ETCO2, Bispectral index & pulmonary arterial pressure.</li>
                                <li>Cardiac output monitoring.</li>
                                <li>Bedside ABG analysis in each unit.</li>
                                <li>Bedside EEG, EMG-NCV studies, 2D Echo, Sonography & Doppler studies in coordination with other departments.</li>
                                <li>Hemodialysis, SLED, CRRT, IABP, Pulmonary catheter, Temporary pacing-intravenous & transcutaneous.  </li>
                                <li>Sophisticated ventilators for invasive as well as non-invasive ventilation</li>
                                <li>Proximity to OT complex, Cathlab, CT scan & MR imaging.</li>
                                <li>Resuscitation equipment, maintenance of crash carts, defibrillation equipment & advanced airway devices. </li>

                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>SICU and ICCU</h2>

                        <div class="tab_content">

                            <p class="hd green_text uppercase"><strong>Surgical Intensive Care Unit (SICU)</strong></p>



                            <p>
                                Recently renovated 12 beds SICU on first floor serves various cardiac and other surgical patients 
The team is headed by Chief Intensivist and provides availability of Qualified and experienced critical care specialist, dedicated and qualified nurses, efficient nursing aid and attendant staff round the clock having sympathetic approach to patient care. Efficient and readily available paramedic staff i.e. X-ray technicians, Phlebotomist, etc. Efficient team serving various organ transplant patients like Kidney transplant recipient, liver transplant recipient, etc.The design of SICU incorporates various infection control practices minimizing risk of hospital acquired infections.
                            </p>
                            <p>SICU provides backbone to conduct highest of high risk surgeries in city of Mumbai</p>

                            <p class="green_text"><strong>Technology & Infrastructure </strong></p>

                            <ul class="bullet">
                                <li>Portable digital X-ray with on spot visual display improves and expedites decision making about patient care.</li>
                                <li>C MAC Flexible intubating Laryngoscope for difficult endotracheal Intubation making procedure safe.</li>
                                <li>Advanced ABG machine with point of care testing for Electrolytes, Glucose and Lactate.</li>
                                <li>The Central pendent in each cubicle provides better interface between patient and ventilator /Monitor.</li>
                                <li>Separate counselling room for relatives thus improves communication and confidence.</li>
                                <li>Isolated and clean DU (Dirty Utility) area to maintain hygiene.</li>
                                <li>Positive & Negative Pressure Isolation rooms with capability to manage Liver transplant.</li>
                                <li>Bright lightning system has been installed creating better atmosphere for doctors and patients.</li>
                                <li>Most beds have Dialysis facility.</li>
                                <li>Remote driven partitions help to keep walls clear</li>
                                <li>Separate doctors room has been designed for isolation beds</li>
                            </ul>

                            <p class="green_text"><strong>Services</strong></p>

                            <ul class="bullet">
                                <li>Vigilant Monitoring by Qualified doctors & nurses</li>
                                <li>Invasive &non-invasive ventilation </li>
                                <li>Basic & Advanced Hemodynamic monitoring like arterial pressure, central Venous pressure, PAP, cardiac output, etc. is done routinely </li>
                                <li>Routine use of IABP pacemaker etc.</li>
                                <li>Advanced therapies like Extracorporeal Cardiac Support (i.e. cardiac ECMO) </li>
                            </ul>

                            <p class="green_text"><strong>Professionals</strong></p>

                            <p class="hd green_text uppercase"><strong>Intensive Critical Care Unit (ICCU)</strong></p>



                            <p>Recently renovated 19 bedded ICCU located on first floor serves various cardiac and non-cardiac emergencies. The team is headed by Chief Intensivist and provides availability of qualified and experienced critical care specialist,dedicated and qualified nurses, efficient nursing aid and attendant staff round the clock having sympathetic approach to patient care. Efficient and readily available paramedic staff i.e. X-ray technicians, Phlebotomists, etc. The design of ICCU incorporates various infection control practices minimizing risk of hospital acquired infections.</p>
                            <p>Average ICCU Occupancyis more than 90%.</p>
                            <p>Round the clock availability of cardiology & other super speciality consultants on call </p>

                            <p class="green_text"><strong>Technology & Infrastructure </strong></p>

                            <ul class="bullet">
                                <li>2 Isolation Beds.</li>
                                <li>1 Isolation Bed with C - Arm Compatibility.</li>
                                <li>Spacious individual cubicle with TV facility.</li>
                                <li>Advanced Ventilators and Multipara Monitors.</li>
                                <li>Wide Cross ventilation arrangements made for each cubicle.</li>
                                <li>Care taken to ensure proper natural light for each patient.</li>
                                <li>Portable digital X-ray with on spot visual display improves and expedites decision making about patient care.</li>
                                <li>C MAC Flexible intubating Laryngoscope for difficult endotracheal Intubation making procedure safe.</li>
                                <li>Advanced ABG machine with point of care testing for Electrolytes, Glucose and Lactate.</li>
                                <li>The Central pendent in each cubicle provides better interface between patient and ventilator /Monitor.</li>
                                <li>Separate counselling room for relatives thus improves communication and confidence.</li>
                                <li>Isolated and clean DU (Dirty Utility) area to maintain hygiene.</li>
                                <li>Positive & Negative Pressure Isolation rooms.</li>
                                <li>Each Cubicle with Dialysis facility</li>
                            </ul>

                            <p class="green_text"><strong>Services</strong></p>
                            <p>Management of below : </p>

                            <ul class="bullet">
                                <li>Cardiac Emergencies like Acute Myocardial Infarction, Cardiogenic Shock as per standard Interventional guidelines & protocol </li>
                                <li>Invasive & Non Invasive Mechanical Ventilation. </li>
                                <li>Basic as well as advanced hemodynamic monitors like PAC cardiac Output Monitoring etc.</li>
                                <li>Advanced procedures like IABP. PAC Insertion, Pacemaker insertion are done routinely bedside in ICU under C- Arm/X ray Guidance </li>
                                <li>Routine use of advanced modalities like CRRT (Continuous Renal Replacement Therapy) , intermittent hemodialysis and Noval therapies like hemoabsorptions & extracorporeal Cytokine removal </li>
                                <li>Advanced Respiratory Care support like Prone Ventilation and ECMO.</li>
                            </ul>

                            <p class="green_text"><strong>Professionals</strong></p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>PICU</h2>

                        <div class="tab_content">



                            <p>Our Pediatric Intensive Care Unit (PICU) is a multidisciplinary unit that provides care for critically ill infants, children and adolescents upto 18 years of age.</p>
                            <p>The many physicians, nurses and allied medical care professionals who work in our PICU are expert in caring for children with serious and life threatening conditions. They have the knowledge, skill and judgment to quickly assess and treat your child so as to achieve the best possible outcome from critical illness or injury. </p>
                            <p>Equipped with advanced technology, our multidisciplinary team  of qualified & experienced Pediatric & Neonatal Intensivist, Paediatric  Cardiologists,  Paediatric Nephrologist, Paediatrics Neurologist, Paediatric Hemato-oncologist, Paediatric Surgeon, Paediatric Dietician, Paediatric Physiotherapist and Paediatric trained nurses,  improves survival, speeds recovery, minimizes disability and relieves pain and suffering in a caring and respectful manner. Our family-centered care approach ensures that you are kept up-to-date about your child’s condition and receive the information you need to make informed choices about their care.</p>


                            <p class="hd green_text uppercase"><strong>Technology and Infrastructure</strong></p>
                            <p>We have state of the art 5 bedded PICU with latest equipment & infrastructure with 1:1 nurse to patient ratio. In PICU our staffs are trained in providing comprehensive care to critically sick children with tender love and make them feel closer to their home. Our PICU have round the clock in house trained &qualified Paediatrics Intensivist who can take care of your children once they are at emergency department of our hospital. Critically sick children are under continuous observation of our trained and qualified nurses who have experience in managing such cases. We treat array of conditions from medical illness like complicated pneumonia, meningoencephalitis, ARDS to surgical conditions like Polytrauma, traumatic brain injury and other surgical illnesses including postoperative care. Our survival rate is comparable to developed countries. Facilities available are as follows</p>
                            <ul class="bullet">
                                <li>Conventional and advanced invasive ventilation</li>
                                <li>CPAP</li>
                                <li>High flow nasal oxygen therapy</li>
                                <li>Multipara monitors & infusion pumps</li>
                                <li>Invasive haemodynamic monitoring</li>
                                <li>Capnography</li>
                                <li>Defibrillator</li>
                                <li>Cardiac pacemaker</li>
                                <li>Round the clock bedside  X ray, Sonography and 2 D Echo</li>
                                <li>Bedside  EEG Monitoring</li>
                                <li>Peritoneal Dialysis facility</li>
                                <li>In hospital facility for continuous renal replacement therapy (CRRT)  and Plasmapharesis</li>
                                <li>Bedside blood chemistry including ABG</li>
                                <li>Round the clock imaging services including MRI and CT .</li>
                                <li>Robust back up with State of Art blood bank and other laboratory services. </li>
                                <li>Infection control &Surveillance policies</li>
                            </ul>

                            <p class="hd green_text uppercase"><strong>Services</strong></p>

                            <ul class="bullet">
                                <li>Management of various  complex respiratory and cardiac conditions</li>
                                <li>Management of complex congenital conditions.</li>
                                <li>IEM & Other Metabolic emergencies</li>
                                <li>Neurological emergencies</li>
                                <li>Complex endocrine conditions</li>
                                <li>Drug overdose & poisoning</li>
                                <li>Infections with multiorgan failure</li>
                                <li>Haematological malignancies & emergencies</li>
                                <li>Management of AKI and renal failure</li>
                                <li>Management of Polytrauma</li>
                                <li>Postoperative management of neurological & cardiac cases</li>
                                <li>Postoperative management of Pediatric surgical cases</li>
                                <li>Post Renal transplant care</li>
                                <li>Intracranial pressure monitoring</li>
                                <li>Total parenteral nutrition</li>
                                <li>Point of care bedside laboratory testing</li>
                                <li>All referred patient will be transferred back to referring physician for the follow up</li>
                            </ul>

                            <p class="hd green_text uppercase"><strong>Professionals</strong></p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>NICU</h2>

                        <div class="tab_content">


                            <p>The entire 10th floor of our hospital is dedicated to mother & child health to provide comprehensive prenatal & postnatal care with multidisplinary approach. Our 6 bedded NICU is having state of art facility to provide advanced medical & surgical care to new born with complex clinical scenarios. At Lilavati Hospital and Research Center we have interactive session with our neonatologist, Intensivist, obstetrician & Pediatric surgeon in high risk delivery to prepare the arrival of babies. Our NICU is fully equipped with professionals & trained staff to provide care of extremely premature & low birth weight new born. We are trained to manage new born with extreme prematurity, extreme low birth weight, respiratory distress syndrome, persistent primary pulmonary hypertension, meconium aspiration syndrome, birth asphyxia, Congenital anomalies, Neonatal hyperbilirubinemia & complex neonatal surgical conditions. At our hospital we allow mother to actively participate in care of her child to promote mother child bonding. We also encourage mother to provide Kangaroo Mother Care (KMC) which provide comprehensive developmental care of babies.  Our NICU is managed by in house Intensivistalong with neonatologist, subspecialty consultants  & Pediatric surgeons. </p>
                            <p>Our family-centered care approach keeps you up-to-date about your child’s condition and receive the information you need to make informed choices about their care.</p>

                            <p class="hd green_text uppercase"><strong>Technology and Infrastructure:</strong></p>
                            <p>At Lilavati Hospital and Research Center we have state of the art NICU with latest equipment & infrastructure with 1:1 nurse to patient ratio. In NICU our staffs are trained in providing comprehensive care to critically sick new born with nesting & tender love to make them feel closer to their mother’s womb. NICU at Lilavati hospital is managed by round the clock inhouse trained & qualified Intensivist which assures you that your baby is in safe hand since time of admission.  Facilities available are as follows:</p>

                            <ul class="bullet">
                                <li>Servo controlled radiant  warmer to provide optimum temperature control</li>
                                <li>Conventional and advanced invasive neonatal ventilation</li>
                                <li>CPAP</li>
                                <li>High Flow Nasal Oxygen Therapy</li>
                                <li>Bedside neurosonography</li>
                                <li>Multipara monitors & infusion pumps</li>
                                <li>Invasive Haemodynamic Monitoring</li>
                                <li>Round the clock bedside  X ray, Sonography and 2 D Echo</li>
                                <li>Bedside  EEG Monitoring</li>
                                <li>Phototherapy</li>
                                <li>Surfactant administration</li>
                                <li>Peritoneal Dialysis facility</li>
                                <li>Bedside blood chemistry including ABG</li>
                                <li>Round the clock imaging facility including MRI and CT </li>
                                <li>Robust back up with State of Art blood bank and other laboratory services. </li>
                                <li>Infection control and  Surveillance policies</li>
                            </ul>

                            <p class="hd green_text uppercase"><strong>Services:</strong></p>

                            <ul class="bullet">
                                <li>Management of extreme premature & extreme low birth weight babies</li>
                                <li>Management of respiratory distress syndrome, meconium aspiration and PPHN</li>
                                <li>Management of various  complex  respiratory and cardiac conditions</li>
                                <li>Management of complex congenital conditions</li>
                                <li>Inborn error of metabolism  & other metabolic emergencies</li>
                                <li>Neonatal seizure & other neurological emergencies</li>
                                <li>New born with complex endocrine conditions</li>
                                <li>Infections with multiorgan failure </li>
                                <li>Management of acute kidney injury and renal failure</li>
                                <li>Postoperative management of neurological & cardiac cases in new born</li>
                                <li>Postoperative management of  surgical cases</li>
                                <li>Phototherapy</li>
                                <li>Exchange transfusion</li>
                                <li>Total Parenteral Nutrition</li>
                                <li>Kangaroo Mother Care (KMC)</li>
                                <li>Lactation Management</li>
                                <li>New born screening for genetic & metabolic diseases</li>
                                <li>Bedside retinopathy of prematurity  screening & Laser treatment</li>
                                <li>Bedside otoacoustic emission (OAE) test for regular hearing screening of all new born.</li>
                                <li>Point of care bedside laboratory testing</li>
                                <li>All referred new born will be transferred back to referring neonatologist for follow up</li>
                            </ul>

                            <p class="hd green_text uppercase"><strong>Professionals</strong></p>

                            <div class="clear"></div>
                        </div>
                    </div>--%>


                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Service ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

