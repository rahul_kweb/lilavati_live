﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;


public partial class find_a_doctor : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["PreviousPage"] != null)
            {
                Session.Remove("PreviousPage");
                if (Session["GetSearchText"] != null)
                {
                    BindSpecialtyDropdown();
                    SearchBySplAndDoctorOnBackButtonClick(Session["GetSearchText"].ToString());
                    //Session.Remove("PreviousPage");
                    Session.Remove("ddlspeciality");
                    Session.Remove("GetSearchText");
                }
                else if (Session["ddlspeciality"] != null)
                {
                    BindSpecialtyDropdownOnBackButtonClick(Session["ddlspeciality"].ToString());
                    //string passvalue = "1";
                    BindDrBySpecialtyOnBackButtonClick(Session["ddlspeciality"].ToString());
                    // Session.Remove("PreviousPage");
                    Session.Remove("ddlspeciality");
                }

                else
                {
                    BindSpecialtyDropdown();
                }
            }
            else
            {
                BindSpecialtyDropdown();
            }
        }
    }

    public void BindSpecialtyDropdown()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_SPECIALTY_DDL'");
        if (dt.Rows.Count > 0)
        {
            ddlspecialty.DataSource = dt;
            ddlspecialty.DataTextField = "Speciality_vcr";
            ddlspecialty.DataValueField = "Speciality_Id_bint";
            ddlspecialty.DataBind();

            ListItem itemToRemove = ddlspecialty.Items.FindByValue("10065");
            if (itemToRemove != null)
            {
                ddlspecialty.Items.Remove(itemToRemove);
            }

            ddlspecialty.Items.Insert(0, new ListItem("By Speciality"));
        }

    }

    public void BindDoctorDropdown()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,'" + ddlspecialty.SelectedValue + "'");
        if (dt.Rows.Count > 0)
        {
            ddldoctorlist.DataSource = dt;
            ddldoctorlist.DataTextField = "DOCTOR_NAME_VCR";
            ddldoctorlist.DataValueField = "DOCTOR_ID_BINT";
            ddldoctorlist.DataBind();

            //ddldoctorlist.Items.Insert(0, new ListItem("By Doctor"));
        }
    }


    public void BindDrBySpecialty()
    {

        try
        {
            StringBuilder drlist = new StringBuilder();
            DataTable dt = new DataTable();
            if (ddlspecialty.SelectedValue != "By Speciality")
            {
                dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,'" + ddlspecialty.SelectedValue + "'");
            }
            else
            {
                dt = null;
            }


            if (dt != null)
            {


                if (dt.Rows.Count > 0)
                {
                    drlist.Append("<ul class='doc_search_result'>");
                    foreach (DataRow dr in dt.Rows)
                    {
                        string consdrid = string.Empty;
                        if (dr["CONSDOCID"].ToString() != "")
                        {
                            consdrid = dr["CONSDOCID"].ToString();
                        }
                        else
                        {
                            consdrid = "0";
                        }


                        //drlist.Append("<li>");                    
                        //drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div'>");
                        //drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        //drlist.Append("<span class='name green_text'><strong>");
                        //drlist.Append(dr["DOCTOR_NAME_VCR"].ToString());
                        //drlist.Append("</strong></span>");
                        //drlist.Append("<span class='desig'>");
                        //drlist.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
                        //drlist.Append("</span>");
                        //drlist.Append("<span class='view_btn'>");
                        //drlist.Append("View Profile");
                        //drlist.Append("</span>");
                        //drlist.Append("<div class='clear'>");
                        //drlist.Append("</div>");
                        //drlist.Append("</a>");
                        //drlist.Append("</li>");



                        drlist.Append("<li>");
                        drlist.Append("<div class='doc_div'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
                        drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        if (dr["DOCTOR_ID_BINT"].ToString() == "334")
                        {
                            drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString().Replace("Dr.", "Ms.") + " </strong></span>");
                        }
                        else
                        {
                            drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        }
                        //drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        drlist.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
                        drlist.Append("</a>");
                        drlist.Append("<div class='docBtns'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                        if (bool.Parse(dr["DirectAvailable"].ToString()) == false && dr["Speciality_Id_bint"].ToString() != "23" && dr["Speciality_Id_bint"].ToString() != "28" && dr["Speciality_Id_bint"].ToString() != "60")
                        {
                            drlist.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                        }
                        drlist.Append("</div>");
                        //drlist.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                        drlist.Append("<div class='clear'></div>");
                        drlist.Append("</div>");
                        drlist.Append("</li>");


                    }
                    drlist.Append("</ul>");
                    ltrldoctors.Text = drlist.ToString();

                    BindDoctorDropdown();
                    pError.Visible = false;
                }
                else
                {
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = true;
                }
            }
            else
            {
                ltrldoctors.Text = drlist.ToString();
                pError.Visible = true;
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            // throw;
        }


    }

    protected void ddlspecialty_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlspecialty.SelectedIndex > 0)
        {
            BindDrBySpecialty();
            ddldoctorlist.Items.Insert(0, new ListItem("Doctor Name"));
            ddldoctorlist.Enabled = true;
            Session["ddlspeciality"] = ddlspecialty.SelectedValue;
        }
        else
        {
            ddldoctorlist.Enabled = false;
            BindDrBySpecialty();
            ddldoctorlist.Items.Clear();
            ddldoctorlist.Items.Insert(0, new ListItem("Doctor Name"));
        }
    }
    protected void ddldoctorlist_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlspecialty.SelectedIndex > 0)
        {
            BindDrByDoctorId();
        }
        else
        {
            BindDrBySpecialty();
        }
    }
    public void BindDrByDoctorId()
    {
        try
        {
            StringBuilder drlist = new StringBuilder();
            DataTable dt = new DataTable();
            if (ddldoctorlist.SelectedValue != "Doctor Name" && ddlspecialty.SelectedValue != "By Speciality")
            {
                dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_SPL_DR_BY_DRID',0,'" + ddldoctorlist.SelectedValue + "','" + ddlspecialty.SelectedValue + "'");
            }
            else
            {
                dt = null;
            }


            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    drlist.Append("<ul class='doc_search_result'>");
                    foreach (DataRow dr in dt.Rows)
                    {
                        string consdrid = string.Empty;
                        if (dr["CONSDOCID"].ToString() != "")
                        {
                            consdrid = dr["CONSDOCID"].ToString();
                        }
                        else
                        {
                            consdrid = "0";
                        }


                        //drlist.Append("<li>");
                        //drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div'>");
                        //drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        //drlist.Append("<span class='name green_text'><strong>");
                        //drlist.Append("Dr. " + dr["DOCTOR_NAME_VCR"].ToString() + "");
                        //drlist.Append("</strong></span>");
                        //drlist.Append("<span class='desig'>");
                        //drlist.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
                        //drlist.Append("</span>");
                        //drlist.Append("<span class='view_btn'>");
                        //drlist.Append("View Profile");
                        //drlist.Append("</span>");
                        //drlist.Append("<div class='clear'>");
                        //drlist.Append("</div>");
                        //drlist.Append("</a>");
                        //drlist.Append("</li>");

                        drlist.Append("<li>");
                        drlist.Append("<div class='doc_div'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
                        drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        if (dr["DOCTOR_ID_BINT"].ToString() == "334")
                        {
                            drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString().Replace("Dr.", "Ms.") + " </strong></span>");
                        }
                        else
                        {
                            drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        }
                        //drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        drlist.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
                        drlist.Append("</a>");
                        drlist.Append("<div class='docBtns'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                        if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
                        {
                            drlist.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                        }
                        //drlist.Append("<a href='Book-Appointment' class='app_btn'>Book an<br> Appointment</a>");
                        drlist.Append("</div>");
                        drlist.Append("<div class='clear'></div>");
                        drlist.Append("</div>");
                        drlist.Append("</li>");

                    }
                    drlist.Append("</ul>");
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = false;
                }

                else
                {
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = true;
                }
            }
            else
            {

                BindDrBySpecialty();
                ddldoctorlist.Items.Insert(0, new ListItem("Doctor Name"));
                //ltrldoctors.Text = drlist.ToString();
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            // throw;
        }

    }

    public void SearchBySplAndDoctor()
    {
        StringBuilder drlist = new StringBuilder();
        string value = txtSearch.Text.Trim();

        using (SqlCommand cmd = new SqlCommand("ADDUPDATEGETDOCTORSPECIALITYMASTER"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para_vcr", "SEARCH_SPL_DR");
            cmd.Parameters.AddWithValue("@searchKey", value);
            DataTable dt = new DataTable();
            dt = utility.Display(cmd);


            if (dt != null)
            {


                if (dt.Rows.Count > 0)
                {
                    Session["GetSearchText"] = txtSearch.Text.Trim();
                    drlist.Append("<ul class='doc_search_result'>");
                    foreach (DataRow dr in dt.Rows)
                    {
                        string consdrid = string.Empty;
                        if (dr["CONSDOCID"].ToString() != "")
                        {
                            consdrid = dr["CONSDOCID"].ToString();
                        }
                        else
                        {
                            consdrid = "0";
                        }


                        //drlist.Append("<li>");
                        //drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div'>");
                        //drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        //drlist.Append("<span class='name green_text'><strong>");
                        //drlist.Append("Dr. " + dr["DOCTOR_NAME_VCR"].ToString() + "");
                        //drlist.Append("</strong></span>");
                        //drlist.Append("<span class='desig'>");
                        //drlist.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
                        //drlist.Append("</span>");
                        //drlist.Append("<span class='view_btn'>");
                        //drlist.Append("View Profile");
                        //drlist.Append("</span>");
                        //drlist.Append("<div class='clear'>");
                        //drlist.Append("</div>");
                        //drlist.Append("</a>");
                        //drlist.Append("</li>");

                        drlist.Append("<li>");
                        drlist.Append("<div class='doc_div'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
                        drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        if (dr["DOCTOR_ID_BINT"].ToString() == "334")
                        {
                            drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString().Replace("Dr.", "Ms.") + " </strong></span>");
                        }
                        else
                        {
                            drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        }
                        //drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        drlist.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
                        drlist.Append("</a>");
                        drlist.Append("<div class='docBtns'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                        if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
                        {
                            drlist.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                        }
                        //drlist.Append("<a href='Book-Appointment' class='app_btn'>Book an<br> Appointment</a>");
                        drlist.Append("</div>");
                        drlist.Append("<div class='clear'></div>");
                        drlist.Append("</div>");
                        drlist.Append("</li>");

                    }
                    drlist.Append("</ul>");
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = false;
                }
                else
                {
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = true;
                }

            }
            else
            {
                ltrldoctors.Text = drlist.ToString();
                pError.Visible = true;
            }

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
       SearchBySplAndDoctor();           
    }

    #region Back Button click on browser

    public void BindDrBySpecialtyOnBackButtonClick(string selectedValue)
    {

        try
        {
            StringBuilder drlist = new StringBuilder();
            DataTable dt = new DataTable();

            dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,'" + selectedValue + "'");
           
            if (dt != null)
            {


                if (dt.Rows.Count > 0)
                {
                    drlist.Append("<ul class='doc_search_result'>");
                    foreach (DataRow dr in dt.Rows)
                    {
                        string consdrid = string.Empty;
                        if (dr["CONSDOCID"].ToString() != "")
                        {
                            consdrid = dr["CONSDOCID"].ToString();
                        }
                        else
                        {
                            consdrid = "0";
                        }

                        #region
                        //drlist.Append("<li>");                    
                        //drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div'>");
                        //drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        //drlist.Append("<span class='name green_text'><strong>");
                        //drlist.Append(dr["DOCTOR_NAME_VCR"].ToString());
                        //drlist.Append("</strong></span>");
                        //drlist.Append("<span class='desig'>");
                        //drlist.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
                        //drlist.Append("</span>");
                        //drlist.Append("<span class='view_btn'>");
                        //drlist.Append("View Profile");
                        //drlist.Append("</span>");
                        //drlist.Append("<div class='clear'>");
                        //drlist.Append("</div>");
                        //drlist.Append("</a>");
                        //drlist.Append("</li>");

                        #endregion
                        drlist.Append("<li>");
                        drlist.Append("<div class='doc_div'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
                        drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        drlist.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
                        drlist.Append("</a>");
                        drlist.Append("<div class='docBtns'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                        if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
                        {
                            drlist.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                        }
                        //drlist.Append("<a href='Book-Appointment' class='app_btn'>Book an<br> Appointment</a>");
                        drlist.Append("</div>");
                        drlist.Append("<div class='clear'></div>");
                        drlist.Append("</div>");
                        drlist.Append("</li>");


                    }
                    drlist.Append("</ul>");
                    ltrldoctors.Text = drlist.ToString();

                    BindDoctorDropdownOnBackButtonClick(selectedValue);
                    ddldoctorlist.Enabled = true;
                    pError.Visible = false;
                }
                else
                {
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = false;
                }
            }
            else
            {
                ltrldoctors.Text = drlist.ToString();
                pError.Visible = true;
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
            // throw;
        }


    }
    public void BindDoctorDropdownOnBackButtonClick(string selected)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,'" + selected + "'");
        if (dt.Rows.Count > 0)
        {
            ddldoctorlist.DataSource = dt;
            ddldoctorlist.DataTextField = "DOCTOR_NAME_VCR";
            ddldoctorlist.DataValueField = "DOCTOR_ID_BINT";
            ddldoctorlist.DataBind();
            //ddldoctorlist.Items.Insert(0, new ListItem("By Doctor"));
        }
    }
    public void BindSpecialtyDropdownOnBackButtonClick(string speciality)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_SPECIALTY_DDL'");
        if (dt.Rows.Count > 0)
        {
            ddlspecialty.DataSource = dt;
            ddlspecialty.DataTextField = "Speciality_vcr";
            ddlspecialty.DataValueField = "Speciality_Id_bint";

            ddlspecialty.DataBind();

            ddlspecialty.Items.FindByValue(speciality).Selected = true;
            ddlspecialty.Items.Insert(0, new ListItem("By Speciality"));
        }

    }
    public void SearchBySplAndDoctorOnBackButtonClick(string Searchtext)
    {
        StringBuilder drlist = new StringBuilder();
        string value = Searchtext;
        txtSearch.Text = value;
        using (SqlCommand cmd = new SqlCommand("ADDUPDATEGETDOCTORSPECIALITYMASTER"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para_vcr", "SEARCH_SPL_DR");
            cmd.Parameters.AddWithValue("@searchKey", value);
            DataTable dt = new DataTable();
            dt = utility.Display(cmd);


            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    drlist.Append("<ul class='doc_search_result'>");
                    foreach (DataRow dr in dt.Rows)
                    {
                        string consdrid = string.Empty;
                        if (dr["CONSDOCID"].ToString() != "")
                        {
                            consdrid = dr["CONSDOCID"].ToString();
                        }
                        else
                        {
                            consdrid = "0";
                        }
                        drlist.Append("<li>");
                        drlist.Append("<div class='doc_div'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
                        drlist.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
                        drlist.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
                        drlist.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
                        drlist.Append("</a>");
                        drlist.Append("<div class='docBtns'>");
                        drlist.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
                        if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
                        {
                            drlist.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
                        }
                        //drlist.Append("<a href='Book-Appointment' class='app_btn'>Book an<br> Appointment</a>");
                        drlist.Append("</div>");
                        drlist.Append("<div class='clear'></div>");
                        drlist.Append("</div>");
                        drlist.Append("</li>");

                    }
                    drlist.Append("</ul>");
                    ltrldoctors.Text = drlist.ToString();
                    pError.Visible = false;
                }
            }
            else
            {
                pError.Visible = true;
            }

        }
    }

    #endregion
}