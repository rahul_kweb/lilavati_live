﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="post_covid_care_healthcare_packages.aspx.cs" Inherits="post_covid_care_healthcare_packages" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/ddaccordion.js"></script>
    <script type="text/javascript" src="js/ddaccordion_plugin.js"></script>
    <script type="text/javascript" src="js/equate.js"></script>

    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Post COVID Care Healthcare packages</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">
               <div class="inner_banner">
           <img src="/uploads/home_banner/imgBanner-132538984009694577.jpg"/>
            </div>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>
                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <li><a href='#' rel="tab_content_1">Overview</a></li>
                        <li><a href='#' rel="tab_content_2">Healthcare packages</a></li>
                    </ul>

                    <div id="tab_content_1">
                        <p><strong class="uppercase green_text">Post COVID Care – A New Beginning</strong></p>
                        <p>COVID -19 Pandemic has affected many lives across the globe. Interestingly, majority of patients suffer from milder form of illness (80%) and only a smaller proportion (20 %) of patients have moderate or severe illness. This disease has an acute phase like any other viral illness with constitutional symptoms and a chronic phase with persistent symptoms like fatigue, body ache, cough, sore throat and difficulty in breathing. A comprehensive care model is essential to monitor these patients closely to avoid possible complications and have healthy living.</p>
                        <p><strong class="uppercase green_text">hat are the likely complications of COVID-19?</strong></p>
                        <ul class="bullet">
                            <li><strong>Respiratory</strong> – Mild/Moderate/Severe Pneumonia</li>
                            <li><strong>CVS</strong> – ACS, Arrhythmia, Cardiomyopathy</li>
                            <li><strong>Kidney</strong> – Acute Kidney injury electrolyte imbalance</li>
                            <li><strong>Obstetrics</strong> – Preterm IUGR, Miscarriage</li>
                            <li><strong>Coagulopathy</strong> – Blood clotting issue leading to vascular complicatio</li>
                            <li><strong>G.I</strong> – Diarrhoea, abdominal pain, nausea/vomiting</li>
                            <li><strong>CNS</strong> – Dizziness, fatigue, headache, seizures, haemorrhag</li>
                            <li><strong>MODS</strong> - Lung sequelae , Greater than 40% patient residual abnormal lung changes</li>
                        </ul>
                        <p><strong class="uppercase green_text">What does Post COVID care mean?</strong></p>
                        <p>After recovering from acute phase of illness, we see lingering bodily symptoms. Pandemic has raised fear of worsening in recovered patients too. Thus, many feel rejected and disturbed. They need to be counselled and given enough support by people around. Mostly, they comprise of constitutional features and in some may indicate towards possible complications. It’s important to continue being cautious especially for 3 months after discharge. Elderly age group needs more care as vascular complications such as bleeding or clotting both can be life threatening. For those who suffered from Pneumonia, need to be monitored for any further progression in form of fibrosis. Physiological and anatomical follow up is a must in form of pulmonary function tests and HRCT scan of chest., as advised by your physician. Post COVID period needs a supervised care and persistent cautious attitude.  Home quarantine should be done for appropriate duration as per your doctor. </p>
                        <p><strong class="uppercase green_text">What measures are to be taken at home after discharge?</strong></p>
                        <ul class="bullet">
                            <li>Continue COVID appropriate behaviour (use of mask, hand & respiratory hygiene, physical distancing)</li>
                            <li>Readdress your on-going medications for other pre-existing ailments</li>
                            <li>Self-monitoring is essential in form of daily check of oxygen levels and temperature.</li>
                            <li>Look for warning signs like fever, breathlessness, low oxygen levels (Sp02 < 95%), unexplained chest pain, new onset of confusion, focal bodily weakness</li>
                            <li>If you are started on blood thinners, keep a close watch on bleeding from any site like red coloured urine, blood in stools, blood in phlegm etc.</li>
                            <li>If you are started on oral corticosteroids, monitor your sugar levels</li>
                            <li>Ensure nutritious diet, adequate hydration (with plenty of fluids) and sound sleep</li>
                            <li>Be vigilant and vocal about your mental health</li>
                            <li>Recovered individuals should discuss the importance of right reflexes at right time and clear the myths and stigma around the disease</li>
                            <li>Professional work to be resumed in graded manner only after confirming with treating Physician</li>
                            <li>COVID Rehabilitation is extremely beneficial</li>
                            <li>Advisable to follow up with primary Physician after 10 days from discharge</li>
                        </ul>
                        <p><strong class="uppercase green_text">Alarms to contact doctors after discharge !!!</strong></p>
                        <p>Post recovery, your body is still in a vulnerable state and is prone to various types of infections or COVID related complications. Staying cautious would help you by picking these signs early. Some of the signs one should be on the lookout for include:</p>
                        <ul class="bullet">
                            <li>Fever</li>
                            <li>Worsening breathlessness</li>
                            <li>Drop in oxygen levels(less than 95% on ambient air)</li>
                            <li>High pulse rate</li>
                            <li>New onset chest pain</li>
                            <li>Delirium, confusion</li>
                            <li>Focal bodily weakness</li>
                            <li>Depressive thoughts</li>
                            <li>Gastrointestinal issues</li>
                            <li>Bleeding from any site</li>
                        </ul>
                        <p><strong class="uppercase green_text">How does COVID Rehabilitation  help ?</strong></p>
                        <p>COVID Rehabilitation is a comprehensive programme that takes care of your physical as well as mental health. In view of persistent effects of disease on body, immediate addressal is always welcomed. Need for this programme is more in ICU survivors or those who battled moderate or severe form of disease as they all have persisting limitations in respiratory function and gas exchange. Majority of patients with milder forms of disease do develop movement related fatigue. This programme offers physical, cognitive and psychosocial benefits. Additionally, forms a link between treating physician and patients to identify limitations early.</p>
                        <p><strong class="uppercase green_text">How does COVID Rehabilitation  help ?</strong></p>
                        <p>Do’s</p>
                        <ul class="bullet">
                            <li>Continue sanitization and maintain social distancing</li>
                            <li>Continue breathing exercises</li>
                            <li>Take you regular pre COVID medicines</li>
                            <li>Continue sustainable physical exercise preferably Pulmonary Rehabilitation</li>
                            <li>Take adequate Nutrition, hydration and sleep</li>
                            <li>Help others around by improving awareness about the disease</li>
                            <li>Kindly follow up with your consulting physician</li>
                        </ul>
                        <p>Don’ts</p>
                        <ul class="bullet">
                            <li>Do not panic once you have recovered from COVID</li>
                            <li>Don’t overstress yourself or get fatigued</li>
                            <li>Don’t feel helpless/ alone /anxious</li>
                            <li>Don’t self-medicate yourself</li>
                            <li>Don not perform strenuous exercise</li>
                            <li>Don’t shy away speaking about your health issues be it mental or physical</li>
                        </ul>
                        <a href="/uploads/brochure/pdf-132542263478907525.pdf" target="_blank" class="submit_btn fade_anim fancybox" style="color: #FFF; text-align: center">Download Booklet</a>
                        <div class="clear"></div>
                    </div>
                    <div id="tab_content_2">
                        <div class="accd_box">
                            <div class="hd c_expand_hd" headerindex="1h">
                                <span class="accordprefix"></span>
                                Basic Care Package: Cost Rs. 8,000/-<span class="accordsuffix"></span>
                            </div>
                            <div class="newsbox accd_content c_expand_box" contentindex="1c" style="display: none;">
                                
                        <p><strong>For patients managed at home or admitted in wards with NO oxygen requirement</strong></p>
                        <ul>
                            <li>Patients who had asymptomatic disease or mild symptoms like cough , sore throat or cold with no Pneumonia throughout the acute course</li>
                            <li>Never dropped Oxygen levels</li>
                            <li>Did not require Steroids or blood thinners on discharge</li>
                        </ul>
                        <p><strong class="green_text uppercase">After 6 weeks</strong>.</p>
                        <p><strong>Home Sample Collection</strong></p>
                        <ul class="bullet pckg_list">
                            <li>CBC</li>
                            <li>CRP</li>
                            <li>SGOT</li>
                            <li>SGPT</li>
                            <li>Creatinine</li>
                        </ul>
                        <p><strong>Tests conducted at the hospital</strong></p>
                        <ul class="bullet pckg_list">
                            <li>ECG</li>
                            <li>COVID Antibody</li>
                            <li>Chest X-ray PA view</li>
                            <li>Spirometry with bronchodilator reversibility</li>
                        </ul>
                        <p><strong>Consultations at the hospital</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Pulmonologist</li>
                            <li>Nutritionist</li>
                        </ul>
                        <p><strong>Physiotherapy assessment video consultation</strong></p>
                        <ul class="bullet pckg_list">
                            <li>One session</li>
                        </ul>
                        <p><strong>Medicines</strong></p>
                        <ul class="bullet pckg_list">
                            <li>C Becozinc twice a day for 45 days</li>
                            <li>T. Celin 500 mg thrice a day for 45 days</li>
                            <li>Uprise D3 60,000 once a week for 8 weeks</li>
                            <li>Resourse high protein/ Diabetic Resource high Protein</li>
                        </ul>
                        <p><strong>Cost Rs. 8,000/-</strong></p>
                            </div>
                        </div>

                        <div class="accd_box">
                            <div class="hd c_expand_hd" headerindex="1h">
                                <span class="accordprefix"></span>
                               Advanced Care Package: Cost Rs. 30,000/-<span class="accordsuffix"></span>
                            </div>
                            <div class="newsbox accd_content c_expand_box" contentindex="1c" style="display: none;">
                               <p><strong> For patients admitted in wards with oxygen requirements</strong></p>
                        <ul>
                            <li>Patient those who had Pneumonia with our without history of drop in Oxygen levels. They need close monitoring of Oxygen at home also</li>
                            <li>Prescribed blood thinner on discharge</li>
                            <li>Prescribed oral corticosteroids on discharge</li>
                        </ul>
                        <p><strong class="green_text uppercase">After 10 days</strong>.</p>
                        <p><strong>Home Sample Collection</strong></p>
                        <ul class="bullet pckg_list">
                            <li>CBC</li>
                            <li>CRP</li>
                            <li>Blood Sugar Fasting (FBS)</li>
                            <li>Blood Sugar Post Prandial (PPBS)</li>
                        </ul>
                        <p><strong>Video Consultations</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Pulmonologist</li>
                            <li>Nutritionist</li>
                            <li>Psychologist</li>
                        </ul>
                        <p><strong class="green_text uppercase">After 10 days</strong>.</p>
                        <p><strong>Home Sample Collection</strong></p>
                        <ul class="bullet pckg_list">
                            <li>CBC</li>
                            <li>CRP</li>
                        </ul>
                        <p><strong>Liver Profile</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Total protein</li>
                            <li>Albumin (Globulin , A/G Ratio)</li>
                            <li>SGOT</li>
                            <li>SGPT</li>
                            <li>Bilirubin</li>
                            <li>GGTP</li>
                            <li>Alkaline phosphatase</li>
                        </ul>
                        <p><strong>Kidney Profile</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Serum Uric Acid</li>
                            <li>Serum Calcium</li>
                            <li>Serum Electrolyte</li>
                            <li>Serum Creatinine</li>
                            <li>Serum Protein</li>
                            <li>Albumin (Globulin , A/G Ratio)</li>
                            <li>Blood Urea</li>
                            <li>Phosphorus</li>
                        </ul>
                        <p><strong>D Dimer</strong></p>
                        <p><strong>Glyco HbA1c</strong></p>
                        <p><strong>Blood Sugar Fasting (FBS)</strong></p>
                        <p><strong>Blood Sugar Post Prandial (PPBS)</strong></p>
                        <p><strong>At Hospital</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Prothombin Time (PT-INR)</li>
                            <li>ECG</li>
                            <li>COVID Antibody</li>
                            <li>HRCT Chest Plain or with Contrast</li>
                            <li>PFT</li>
                            <li>6 minute Walk test</li>
                        </ul>
                        <p><strong>In person Consultation</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Pulmonologist</li>
                            <li>Nutritionist</li>
                            <li>Psychologist</li>
                            <li>Complete Pulmonary Function Test</li>
                            <li>Any One Consultation (Diabetologist/Psychiatrist/ Cardiologist/ Nephrologist/ Neurologist)</li>
                        </ul>
                        <p><strong>Medicines</strong></p>
                        <ul class="bullet pckg_list">
                            <li>C Becozinc twice a day for 45 days</li>
                            <li>T. Celin 500 mg thrice a day for 45 days</li>
                            <li>Uprise D3 60,000 once a week for 8 weeks</li>
                            <li>Resourse high protein/ Diabetic Resource high Protein</li>
                        </ul>
                        <p><strong>Covid Rehabiltation</strong></p>
                        <ul class="bullet pckg_list">
                            <li>10 session package - Video Consultation</li>
                        </ul>
                        <p><strong>Cost Rs. 30,000/-</strong></p>

                            </div>
                        </div>

                        <div class="accd_box">
                            <div class="hd c_expand_hd" headerindex="1h">
                                <span class="accordprefix"></span>
                                Supreme Care: Cost Rs. 40,000/-<span class="accordsuffix"></span>
                            </div>
                            <div class="newsbox accd_content c_expand_box" contentindex="1c" style="display: none;">
                               <p><strong>For patients admitted in ICU</strong></p>
                        <ul>
                            <li>Patient who had moderate or severe Pneumonia with or without history of drop in Oxygen levels. They need close monitoring of Oxygen and temperature at home.</li>
                            <li>Prescribed blood thinners on discharge</li>
                            <li>Prescribed corticosteroids on discharge</li>
                        </ul>
                        <p><strong class="green_text uppercase">After 10 days</strong>.</p>
                        <p><strong>Home Sample Collection</strong></p>
                        <ul class="bullet pckg_list">
                            <li>CBC</li>
                            <li>CRP</li>
                            <li>Blood Sugar Fasting (FBS)</li>
                            <li>Blood Sugar Post Prandial (PPBS)</li>
                        </ul>
                        <p><strong>Video Consultations</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Pulmonologist</li>
                            <li>Nutritionist</li>
                            <li>Cardiologist</li>
                            <li>Psychologist</li>
                        </ul>
                        <p><strong class="green_text uppercase">After 2-4 weeks</strong>.</p>
                        <p><strong>Home Sample Collection</strong></p>
                        <ul class="bullet pckg_list">
                            <li>CBC</li>
                            <li>CRP</li>
                        </ul>
                        <p><strong>Liver Profile</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Total protein</li>
                            <li>Albumin (Globulin , A/G Ratio)</li>
                            <li>SGOT</li>
                            <li>SGPT</li>
                            <li>Bilirubin</li>
                            <li>GGTP</li>
                            <li>Alkaline phosphatase</li>
                        </ul>
                        <p><strong>Kidney Profile</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Serum Uric Acid</li>
                            <li>Serum Calcium</li>
                            <li>Serum Electrolyte</li>
                            <li>Serum Creatinine</li>
                            <li>Serum Protein</li>
                            <li>Albumin (Globulin , A/G Ratio)</li>
                            <li>Blood Urea</li>
                            <li>Phosphorus</li>
                        </ul>
                        <p><strong>D Dimer</strong></p>
                        <p><strong>Glyco HbA1c</strong></p>
                        <p><strong>Blood Sugar Fasting (FBS)</strong></p>
                        <p><strong>Blood Sugar Post Prandial (PPBS)</strong></p>
                        <p><strong>Ferrtin</strong></p>
                        <p><strong>LDH</strong></p>
                        <p><strong>At Hospital</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Prothombin Time (PT-INR)</li>
                            <li>ECG</li>
                            <li>COVID Antibody</li>
                            <li>HRCT Chest Plain or with Contrast</li>
                            <li>Complete Pulmonary Function Test</li>
                            <li>6 minute Walk test</li>
                            <li>2D Echo</li>
                        </ul>
                        <p><strong>Consultation</strong></p>
                        <ul class="bullet pckg_list">
                            <li>Pulmonologist</li>
                            <li>Nutritionist</li>
                            <li>Cardiologist</li>
                            <li>Psychologist</li>
                            <li>Any One Consultation (Diabetologist/Psychiatrist/ Cardiologist/ Nephrologist/ Neurologist)</li>
                        </ul>
                        <p><strong>Medicines</strong></p>
                        <ul class="bullet pckg_list">
                            <li>C Becozinc twice a day for 45 days</li>
                            <li>T. Celin 500 mg thrice a day for 45 days</li>
                            <li>Uprise D3 60,000 once a week for 8 weeks</li>
                            <li>Resourse high protein/ Diabetic Resource high Protein</li>
                        </ul>
                        <p><strong>Covid Rehabiltation</strong></p>
                        <ul class="bullet pckg_list">
                            <li>18 session package - Video Consultation</li>
                        </ul>
                        <p><strong>Cost Rs. 40,000/-</strong></p>
                            </div>
                        </div>



                        
                        
                        <a href="/uploads/brochure/pdf-132542263478907525.pdf" target="_blank" class="submit_btn fade_anim fancybox" style="color: #FFF; text-align: center">Download Booklet</a>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">
                <menu:Services ID="menu" runat="server" />
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>
