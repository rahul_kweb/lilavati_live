﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="blood_bank.aspx.cs" Inherits="blood_bank" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital - Donate Blood & Save Life's | Blood Bank in Mumbai</title>
    <meta name="description" content="Volunteer for blood donation camps & help people in need. Every year around 6000 donors give blood to the bank which is split into various modules for medical & surgical purpose.">

    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Blood Bank</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Blood_Bank.jpg">
                </div>--%>

                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%--   <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Blood bank caters to all patients including out-patients. We receive blood from 6000 donors every year. A system of credit-debit with other blood banks which are part of Federation of Bombay Blood Banks, works out when there is excess availability at our end. Likewise, we receive blood from other banks whenever there is a shortage. This goes a long way in supporting the lives of several patients who need blood urgently. The Blood Bank caters to all the needs of the inpatients and out patients of the Hospital.</p>


                            <p>We have strict quality control measures for the blood bank and ensure the right blood type is supplied after thorough testing. The blood collected from donors is split in to various components - packed red cells, fresh frozen plasma, cryo depleted plasma, platelet concentrate, single donor platelets and cryoprecipitate of all the blood groups is on shelf. The donor bleeding hours is from 9 a.m. to 5 p.m. every day.</p>

                            <p>There is a constant effort to encourage and motivate patients and relatives of patients to donate blood at our blood bank. Not only this, we conduct blood donation drives and camps from time to time to voluntarily collect blood from donors. In times of natural disaster or any other crisis in the city, we strive to reach out and help any citizen who maybe in urgent need of blood for survival.</p>

                            <p>The Blood Bank performs all the prescribed tests for testing each unit before issue to the patient.</p>

                            <p>The blood bank is inspected and audited by the FDA (Food & Drug Administration) who has laid down standards for management of blood banks.</p>

                            <p>For details contact: <strong>91-22-26568000</strong> Extensions <strong>8214, 8215, 8216</strong>.</p>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->



                    <div class="clear"></div>
                </div>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

