﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class lhmt : System.Web.UI.Page
{
    LHMT tblLHMT = new LHMT();
    CMSBAL cmsbal = new CMSBAL();
    LHMTSubscribe tblLHMTSubscribe = new LHMTSubscribe();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            BindRepeater();
            BindBanner();
        }
    }

    public void BindRepeater()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetLHMT();
        if (dt.Rows.Count > 0)
        {            
            rptLHMT.DataSource = dt;
            rptLHMT.DataBind();            
        }
        else
        {
            rptLHMT.DataSource = null;
            rptLHMT.DataBind();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        tblLHMTSubscribe.Name = txtName.Text.Trim();
        tblLHMTSubscribe.Email = txtEmail.Text.Trim();
        tblLHMTSubscribe.Contact = txtContact.Text.Trim();

        cmsbal.SaveLHMTSubscribe(tblLHMTSubscribe);

        if (HttpContext.Current.Session["SaveLHMTSubscribe"] == "Success")
        {
            LhmtSubscribeMail mail = new LhmtSubscribeMail();
            mail.EmailLHMTSub(txtName.Text.Trim(), txtEmail.Text.Trim(), txtContact.Text.Trim());

            Reset();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "show", "ShowPopUp();", true);
        }
        else
        { 
            
        }
    }

    public void Reset()
    {
        txtName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtContact.Text = string.Empty;
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(63);
    }



}