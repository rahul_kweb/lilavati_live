﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hydrotherapy : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBrochure();
        }
    }

    public void BindBrochure()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM PATIENTS_EDUCATION WHERE ISACTIVE = 1 AND Pat_Id IN (20)", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);

        if (dt.Rows.Count > 0)
        {
            rptBrochure.DataSource = dt;
            rptBrochure.DataBind();
        }
        else
        {
            rptBrochure.DataSource = null;
            rptBrochure.DataBind();
        }
    }
}