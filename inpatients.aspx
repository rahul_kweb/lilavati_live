﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="inpatients.aspx.cs" Inherits="inpatients" %>

<%@ Register TagName="Patients" TagPrefix="menu" Src="Control/patients.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Inpatients</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Inpatient.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%-- <div class="tabbertab">
                        <h2>Admission</h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">Before</strong></p>
                            <ul class="bullet">
                                <li>Patients  are admitted only through doctors or CMO's recommendation. </li>
                                <li>Visit admission counter and provide details such as date, doctor & class preference.</li>
                                <li>For Maternity, pre-registration before 60 days is required.</li>
                                <li>Pay deposit in case of planned surgery or maternity.</li>
                                <li>Foreign nationals & NRI's are required to produce Passport & Visa/PIO, or the admission maybe denied.</li>
                                <li>An intimation call is made on the date of admission. Incase not received, report to admission counter.</li>
                                <li>Reservation deposits are non-transferable.</li>
                                <li>Payment via Cash, DD, Credit & Debit Cards of all major banks accepted.</li>
                            </ul>

                            <p><strong class="green_text uppercase">After</strong></p>
                            <ul class="bullet">
                                <li>Bed charge is billed at the end of each day.</li>
                                <li>Day care admission for a maximum of 8 hours, post which either discharged or admitted.</li>
                                <li>Transfer from one class to another based on availability.</li>
                                <li>Incase of an upgrade, bed charges will be applicable as per higher rate from Day 1.</li>
                                <li>Downgrade subject to management approval.</li>
                                <li>In the event of a shift ICU, the existing room has to be vacated except classes above twin sharing based on availability.</li>
                                <li>Transfer cut-off time is 11 AM.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Billing And Payments</h2>

                        <div class="tab_content">
                            <p><strong class="green_text uppercase">Timings</strong></p>
                            <ul class="bullet">
                                <li>Billing counter is open 24x7</li>
                                <li>Billing queries  shall not be entertained after 30 days of discharge.</li>
                                <li>For redressal on billing queries, call  022-26751594. 9 AM to 5 PM for admitted patients, 2 PM to 5 PM for discharged patients.</li>
                            </ul>

                            <p><strong class="green_text uppercase">Charges</strong></p>
                            <ul class="bullet">
                                <li>Charges of investigations, OT & Doctors fees vary based on room category.</li>
                                <li>Bed charges include only diet & nursing services. Any additional purchase shall be charged seperately.</li>
                                <li>Emergency doctor visit charges  @ 2 times the routine rate are applicable between 11 PM to 6 AM /Sundays/Public Holidays.</li>
                                <li>OT emergency charge @ 1.5 times the routine rate under similar conditions as the emergency charge.</li>
                                <li>Intensivist visit  in the ICU will be charged on a per day basis.</li>
                                <li>Incase of more than one surgery conducted during the same slot, charges for multiple surgeries shall be applicable.</li>
                                <li>Cardiac packages are only for patients admitted in Common, Economy and Twin Sharing class. Upgradation or downgradation of the class within package is not permissible for Angiography, Angioplasty, Bypass.</li>
                                <li>A 20% surcharge is levied on the final bill amount of NRI's & Foreign National patients.</li>
                            </ul>

                            <p><strong class="green_text uppercase">Statement of Accounts</strong></p>
                            <ul class="bullet">
                                <li>Generated  and distributed on every Monday, Wednesday & Friday.</li>
                                <li>SOA will list charges accrued to the patient.</li>
                                <li>SOA is not generated for package surgeries such as Angiography, Angioplasty, Bypass & Cataract.</li>
                            </ul>


                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Discharge</h2>

                        <div class="tab_content">

                            <p><strong class="green_text uppercase">Procedure & Timing:</strong></p>
                            <ul class="bullet">
                                <li>Doctor's authorization required for discharge.</li>
                                <li>Discharges are processed24x7.</li>
                                <li>Discharge order recieved after 9 AM will attract the entire day's charge.</li>
                            </ul>

                            <p><strong class="green_text uppercase">Payment:</strong></p>
                            <ul class="bullet">
                                <li>Based on request and discharge paper, billing department prepares the bill. On final bill completion, information is passed onto patient room.</li>
                                <li>Settlement shall be done at the discharge counter.</li>
                                <li>Visitor passes are to be submitted at the counter. In case the pass is misplaced/damaged, a charge of Rs. 500/- will have to be borne.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>General Instructions</h2>

                        <div class="tab_content">

                            <ul class="bullet">
                                <li>Outside food is not allowed inside hospital for patient/attendant.</li>
                                <li>Smoking, consumption of alcohol and non-vegetarian food is not permitted in the hospital premises.</li>
                                <li>Mobile phones are not permitted in the restricted areas of the hospital.</li>
                                <li>Photography is prohibited inside hospital premises.</li>
                                <li>Outside medicines or consumables are not allowed in the hospital.</li>
                                <li>Patients are advised not to carry cash or wear or keep any jewellery during their hospitalization. The hospital will not be responsible for any kind of loss.</li>
                                <li>The hospital security has the right to check your baggage and ask for passes at any time.</li>
                                <li>The admission counter / ward nurse will issue a feedback form. Please spend some time to let us know how we can serve you better.</li>
                                <li>Reports can be collected from the ground floor central dispatch department which functions from 8am to 9pm on all days except Sunday & Public Holiday.</li>

                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>


                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Patients ID="menu" runat="server" />

            </div>


            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

