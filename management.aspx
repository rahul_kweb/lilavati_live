﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="management.aspx.cs" Inherits="management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%@ Register TagPrefix="InnerMenu" TagName="InnerMenu" Src="Control/about_us_menu.ascx" %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Management</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format mngmt" style="display: none;">



                <div class="clear"></div>
            </div>

            <div class="content_main_box text_format mngmt">

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>


                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                <%--<h1 class="hd"><span class="green_text uppercase">Lt. Gen. (Dr.) V Ravishankar, VSM (Retd.)</span><br>
                    COO, Senior Consultant – Cardiothoracic Surgery </h1>

                <img src="images/v_ravishankar.jpg">

                <p>Lt Gen (Dr) V Ravishankar,VSM (Retd.) is a Cardiothoracic surgeon having served in Armed Forces Medical Services for close to 38 years and has assumed the appointment of Chief Operating officer and Senior Consultant Cardiothoracic Surgery at Lilavati Hospital in November, 2015. A graduate of Maulana Azad Medical College, New Delhi, he did his MS, DNB in General surgery and subsequently M.Ch (Cardiothoracic surgery) from AFMC, under Pune University. He has been trained abroad at St Vincent's Hospital, Portland, Oregon, USA and also at Perth, Australia in adult cardiac surgery.</p>

                <p>In a span of 38 yrs he held many specialist and administrative appointments in Army Medical Corps having worked at Military Hospital (Cardiothoracic Thoracic Centre) Pune and the prestigious Army Hospital (Research & Referral) New Delhi. He has been performing all adult and paediatric cardiac surgery including complex congenital surgeries and was designated professor and Head of Department at both these institutions.</p>

                <p>He is examiner for DNB and M.Ch in his speciality and also a assessor for DNB CT Surgery. His rich experience includes administrative appointments as Director and Commandant of Command Hospital Pune, the largest hospital in armed forces with over 1000 beds and Commandant of Army Medical Corps centre and College at Lucknow where over 5000 paramedics are being trained and where all medical officers attend courses at different phases in their career. For his distinguished services to the Nation he has been awarded Army commanders commendation twice and Vishisht Seva Medal (VSM) by President of India. He has also received the Sakaal Times award for outstanding contribution to the society at Pune.</p>

                <p>
                    <h1 class="hd"><span class="green_text uppercase">Mr. Ajaykumar Pande</span><br>
                        Director Operations & Supply Chain</h1>
                </p>

                <p>
                    <h1 class="hd"><span class="green_text uppercase">Mr. Paresh Parmar</span><br>
                        Director- Finance</h1>
                </p>

                <p>
                    <h1 class="hd"><span class="green_text uppercase">Dr. Conrad Rui Vas</span><br>
                        Medical Director</h1>
                </p>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <InnerMenu:InnerMenu ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

