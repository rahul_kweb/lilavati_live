﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="health_checkup.aspx.cs" Inherits="health_checkup" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Lilavati Hospital - "Stay healthy" schedule your health check up today</title>
    <meta name="description" content="Take the first step forward & detect early symptoms of diseases, schedule your health checkup online quickly with best health care provider in Mumbai. You can get customized packages as per what suits you the best.">

    <script type="text/javascript" src="js/tabcontent.js"></script>
    <script>
        function CallEvent(value) {

            if (value == "1") {
                $('a[rel="tab_36"]').click();
            }
            else if (value == "2") {
                $('a[rel="tab_37"]').click();
            }
            else {
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Health Check Up</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Health_Checkup.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <%-- <div class="tabber">



                    <asp:Literal ID="litContent" runat="server"></asp:Literal>

                    <asp:Literal ID="litHealth" runat="server"></asp:Literal>

                </div>--%>
                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Literal ID="ltrltabname" runat="server"></asp:Literal>
                    </ul>
                    <asp:Literal ID="ltrldescription" runat="server"></asp:Literal>
                    <div class="clear"></div>
                </div>
                <!--  xxxxxxxxxxxx  -->
                <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p><strong class="uppercase">Prevention is better than Cure!</strong></p>
                            <p>Today’s urban lifestyle leads to numerous health issues. It is always advisable to undergo a regular health check-up to avoid unpleasant surprises in life. Our health check-up program is designed to promote good health and facilitate early detection of health problems. Our goal is to encourage people to take steps toward a longer, healthier and happier life.</p>
                            <p>Schedule a Health Check-up today! Call <strong>022-26568354/ 55 (9AM to 5PM)</strong>.</p>
                            <p>Appointments are available for all days except Sunday & Public Holidays.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Health Checkup Packages</h2>

                        <div class="tab_content">

                            <div class="pckg"><a href="#">Special Packages</a></div>

                            <div class="pckg"><a href="solitaire_plus_female_package.html">Solitaire Plus Female Package   - Rs. 37000/-</a></div>

                            <div class="pckg"><a href="solitaire_plus_male_package.html">Solitaire Plus Male Package  - Rs. 36000/-</a></div>

                            <div class="pckg"><a href="solitaire_female_package.html">Solitaire Female Package   - Rs. 31000/-</a></div>

                            <div class="pckg"><a href="solitaire_male_package.html">Solitaire Male Package  - Rs. 30000/-</a></div>

                            <div class="pckg"><a href="platinum_female_package.html">Platinum Female Package  - Rs. 18500/-</a></div>

                            <div class="pckg"><a href="platinum_male_package.html">Platinum Male Package  - Rs. 17500/-</a></div>

                            <div class="pckg"><a href="gold_female_package.html">Gold Female Package  - Rs. 12000/-</a></div>

                            <div class="pckg"><a href="gold_male_package.html">Gold Male Package  - Rs. 11000/-</a></div>

                            <div class="pckg"><a href="gold_plus_package.html">Gold Plus Package  - Rs. 19500/-</a></div>

                            <div class="pckg"><a href="silver_female_package.html">Silver Female Package  - Rs. 8500/-</a></div>

                            <div class="pckg"><a href="silver_male_package.html">Silver Male Package  - Rs. 8000/-</a></div>

                            <div class="pckg"><a href="senior_citizen_female_package.html">Senior Citizen Female Package  - Rs. 8000/-</a></div>

                            <div class="pckg"><a href="senior_citizen_male_package.html">Senior Citizen Male Package  - Rs. 7500/-</a></div>

                            <div class="pckg"><a href="eves_plus_package.html">Eves Plus Package  - Rs. 6000/-</a></div>

                            <div class="pckg"><a href="adams_plus_package.html">Adams Plus Package  - Rs. 6000/-</a></div>

                            <div class="pckg"><a href="eves_package.html">Eves Package  - Rs. 5000/-</a></div>

                            <div class="pckg"><a href="adams_package.html">Adams Package  - Rs. 5000/-</a></div>

                            <div class="pckg"><a href="cardiac_evaluation_package.html">Cardiac Evaluation Package  - Rs. 7500/-</a></div>

                            <div class="pckg"><a href="basic_package.html">Basic Package   - Rs. 3000/-</a></div>

                            <div class="clear"></div>
                        </div>

                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Instructions</h2>

                        <div class="tab_content">

                            <p><strong class="uppercase green_text">Appointment & Reporting Time</strong></p>
                            <ul class="bullet">
                                <li>Health checks are done through prior appointment. Please report at the appointed time given to you at the reception counter of the Health Checkup Department on 2nd Floor.</li>
                                <li>We request you to kindly observe the appointment timings to facilitate the smooth flow of your Health Check. Patient reporting more than an hour late than his/her scheduled appointment time will not be taken.</li>
                                <li>Kindly inform us in case of cancellations or postponements atleast 24hrs in advance.</li>
                            </ul>

                            <p><strong class="uppercase green_text">Fasting & Medication</strong></p>
                            <ul class="bullet">
                                <li>You are advised to observe minimum 12hrs to maximum 13hrs of fasting and abstinence from alcohol. Hence it is recommended that you do not eat or drink anything after 8pm the previous night. You can only take few sips of water in the morning of the Health Check with your antihypertensive or any other essential drugs except anti-diabetic medication, which should be taken before/with breakfast as per your regular schedule.</li>
                                <li>Inhalers & Oral Bronchodilators for Asthma or Bronchitis should preferably be avoided on the day of the Check up for better Lung Function Test results.</li>
                            </ul>

                            <p><strong class="uppercase green_text">What to bring?</strong></p>
                            <ul class="bullet">
                                <li>Please bring your morning stool and urine samples along with you on the date of the Health Check. You can collect the container from the Health Check Department in advance as per your convenience.</li>
                                <li>In case you are under certain medication and/or are suffering from previous disease, you are advised to bring the previous Medical Records.</li>
                            </ul>

                            <p><strong class="uppercase green_text">Radiological Tests</strong></p>
                            <ul class="bullet">
                                <li>Ultrasonography of Pelvis involves Intra Vaginal Sonography, which is a detailed examination of the Pelvic organs. If you do not wish to undergo the same kindly inform the Health Check executives at the Health Check Reception.</li>
                                <li>Mammography is generally not recommended for women below the age of 35yrs, as they have dense breasts.</li>
                                <li>CT Scan is not recommended for 40 years & below for radiation risk. CT Test requires dye to be injected via IV and it requires normal kidney function. It cannot be performed for patient with Valve Replacement & Pacemarker.</li>
                                <li>Sonomammography will be done only if advised by the Radiologist.</li>
                                <li>Sonography Pelvis for males below 40yrs will be done only if advised by the Radiologist.</li>
                            </ul>

                            <p>Certain Radiological examination involves low dose radiation.</p>

                            <p><strong class="uppercase green_text">General</strong></p>
                            <ul class="bullet">
                                <li>We can provide additional tests blood investigations/Procedure/Consultations subject to availability of appointment. These will be billed separately in addition to the package payment.</li>
                                <li>Please wear comfortable attire, footwear and minimum jewellery on the day of the Health Check.</li>
                                <li>For eye check up: Patients should be without contact lens atleast for 24 hrs.</li>
                                <li>The PAP Smear Test for females cannot be done during Menses (Periods).For further details, please contact our Health Check Reception.</li>
                            </ul>

                            <p><strong class="uppercase green_text">Corporate Client</strong></p>
                            <ul class="bullet">
                                <li>Corporate sponsored clients are requested to bring their referral letter / credit note and employee ID.</li>
                            </ul>

                            <p><strong>In case we are unable to carry out any test/s or consultation because of unforeseen reason/s; the same test/s will be carried out on some other working day as per your convenience but within 10 days of the date of Health check OR proportionate amount will be refunded from the package.</strong></p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Description - Key Tests</h2>

                        <div class="tab_content">

                            <table class="tests">
                                <thead>
                                    <tr>
                                        <td>Name of Test</td>
                                        <td>Screening Organ/ Body Part</td>
                                        <td>How the test is performed</td>
                                        <td>Purpose of the test</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-title="Name of Test">2D Echo</td>
                                        <td data-title="Screening Organ/ Body Part">Heart</td>
                                        <td data-title="How the test is performed">2D Echo is ultrasound of the heart.</td>
                                        <td data-title="Purpose of the test">It provides information regarding the valves, walls of the heart and the pumping capacity of the heart (ejection fraction). Color doppler gives accurate information about congenital heart defects, leaking or choked valves.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Acid Phosphatase</td>
                                        <td data-title="Screening Organ/ Body Part">Liver and Prostate</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Acid Phosphatase is an enzy me found throughout the body but primarily in the prostate gland.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Anti Hepatitis C Virus</td>
                                        <td data-title="Screening Organ/ Body Part">Liver</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Anti Hepatitis C Virus helps detecting the presence of antibodies of the virus, indicating exposure to Hepatitis C Virus.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Anti CCP</td>
                                        <td data-title="Screening Organ/ Body Part">Joints</td>
                                        <td data-title="How the test is performed">Through Blood Sample</td>
                                        <td data-title="Purpose of the test">Is very useful test to order during the diagnostic evaluation of a person who may have rheumatoid arthritis.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Biochemical Screening for Osteoporosis</td>
                                        <td data-title="Screening Organ/ Body Part">Bones</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Has four markers which are tested in blood and have a lot of utility in diagnosis and management of Osteoporosis.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Bone Mineral Densitometry (BMD)</td>
                                        <td data-title="Screening Organ/ Body Part">Bone</td>
                                        <td data-title="How the test is performed">Uses low dose radiation (X-ray)</td>
                                        <td data-title="Purpose of the test">BMD detects osteopenia and osteoporosis at an early and reversible stage.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">C - Reactive Protein (High sensitivity)</td>
                                        <td data-title="Screening Organ/ Body Part">Heart</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Predicts a healthy person's risk for heart disease / stroke.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Calcium Scoring</td>
                                        <td data-title="Screening Organ/ Body Part">Coronary Arteries of Heart</td>
                                        <td data-title="How the test is performed">Non invasive CT Scan</td>
                                        <td data-title="Purpose of the test">To understand probability of blockages of Coronary Arteries of Heart.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Cancer Markers
                                                <br />
                                            CA-125</td>
                                        <td data-title="Screening Organ/ Body Part">Antibodies to Cancer cells in the body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">To detect Ovarian cancer in females</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">CT Coronary Angiography</td>
                                        <td data-title="Screening Organ/ Body Part">Coronary Arteries of Heart</td>
                                        <td data-title="How the test is performed">It is a non-invasive, CT angiography.</td>
                                        <td data-title="Purpose of the test">Preliminary test for evaluation of coronary artery diseases.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">CT Stroke Imaging</td>
                                        <td data-title="Screening Organ/ Body Part">Brain</td>
                                        <td data-title="How the test is performed">It is the non-invasive, CT angiography.</td>
                                        <td data-title="Purpose of the test">For the evaluation of circulation of the blood in the arteries of the brain.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Ferritin</td>
                                        <td data-title="Screening Organ/ Body Part">Iron stores in body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">The test is done to evaluate the severity of Iron deficiency and overload.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Folic Acid</td>
                                        <td data-title="Screening Organ/ Body Part">Cell Division</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Folic acid is a part of an evaluation for anemia and neuropathy.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">FSH</td>
                                        <td data-title="Screening Organ/ Body Part">Ovaries</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">The test is used to check damage or disease of testes or ovaries, pituitary gland or hypothalamus.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">(Hb) Haemoglobin Electrophoresis</td>
                                        <td data-title="Screening Organ/ Body Part">Oxyen carrying protein (Haemoglobin) in blood</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Detects abnormality of haemoglobin like Sickle cell anaemia, Thalassemia.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">HBsAg</td>
                                        <td data-title="Screening Organ/ Body Part">Liver</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Helps in detecting any infectiondueto Hepatitis B surface Antigen so that early diagnosis helps in early treatment.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Lipid Profile (Heart)</td>
                                        <td data-title="Screening Organ/ Body Part">Abnormal lipid in blood</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">The purpose of lipid testing is to determine whether abnormally high or low concentration of specific macromolecular fat are present.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Liver Profile</td>
                                        <td data-title="Screening Organ/ Body Part">Liver</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Liver function tests determine whether the liver has been damaged or its function impaired.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Lung Function Pulmonary Function Test (PFT) Spirometry</td>
                                        <td data-title="Screening Organ/ Body Part">Lungs</td>
                                        <td data-title="How the test is performed">Non invasive screening test for checking the status of the Lungs.</td>
                                        <td data-title="Purpose of the test">PFT checks the volume and capacity of the Lungs. It is very important in patients with Asthma, Bronchitis and also Chronic Smoker. Determines if lung has been damaged or its function is impaired.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Lipoproteins A</td>
                                        <td data-title="Screening Organ/ Body Part">Abnormal lipid in body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Lipoproteins are conjugated proteins in which one component is a lipid. They are principal means whereby lipids are transported in the body.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Apolipoprotein A1</td>
                                        <td data-title="Screening Organ/ Body Part">Abnormal lipid in body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">The protein promotes cholesterol efflux from tissues to the liver.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Apolipoprotein B</td>
                                        <td data-title="Screening Organ/ Body Part">Abnormal lipid in body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Apolipoprotein B is responsible for carrying cholesterol to tissues. High levels of Apo-B can lead to plaques that cause vascular disease leading to heart disease.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Magnesium</td>
                                        <td data-title="Screening Organ/ Body Part">Heart, Liver and Calcium metabolism of the body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">The test reveals body's defense mechanism and calcium metabolism.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Mammography</td>
                                        <td data-title="Screening Organ/ Body Part">Breasts</td>
                                        <td data-title="How the test is performed">X-ray screening test for breasts</td>
                                        <td data-title="Purpose of the test">This is a screening test. It enables early detection of breast cancer. It also helps
                in detection of other breast lesions and masses. It is a 'must' yearly examination in women especially 40 years and above.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">PAP Smear</td>
                                        <td data-title="Screening Organ/ Body Part">Cervix and Vagina</td>
                                        <td data-title="How the test is performed">Through scrape cells from the cervix.</td>
                                        <td data-title="Purpose of the test">To check for cervical cancer, vaginal cancer or abnormal changes in cells that could
                lead to cancer in females.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Prostate SP Antigen(Free &amp; Total)</td>
                                        <td data-title="Screening Organ/ Body Part">Prostate Gland</td>
                                        <td data-title="How the test is performed">Through Blood Sample</td>
                                        <td data-title="Purpose of the test">Abnormality of the prostate gland.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Renal Profile (Kidney)</td>
                                        <td data-title="Screening Organ/ Body Part">Kidneys</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Diagnostic test that measures the functionality of the kidney and its filtration mechanism of toxins.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Serum Electrophoresis- Protein</td>
                                        <td data-title="Screening Organ/ Body Part">Specific proteins in the blood</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Sr. Protein electrophoresis is used to identify patients with multiple myeloma and other serum protein disorders.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Serum Homocysteine</td>
                                        <td data-title="Screening Organ/ Body Part">Cardiovascular System</td>
                                        <td data-title="How the test is performed">Through Blood Sample</td>
                                        <td data-title="Purpose of the test">Elevanted level of homocysteine in the bood may be associated with atherosclerosis (hardening and narrowing of the arteries) as well as an increased risk of heart attacks,strokes,boold clotformation, and possibly Alzheimer's disease.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Serum Iron</td>
                                        <td data-title="Screening Organ/ Body Part">Body's ability to transport Iron</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Serum Iron are ordered when there are signs of anaemia especially when Hemoglobin and Hematocrit are low or Iron overload present.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Sonography of Abdomen</td>
                                        <td data-title="Screening Organ/ Body Part">All abdominal organs like Liver, Spleen, Gall Bladder, Kidney and Pancreas</td>
                                        <td data-title="How the test is performed">Ultrasound of the Abdomen</td>
                                        <td data-title="Purpose of the test">It includes examination of all the important abdominal organs like Liver, Spleen, Gall Bladder, Kidneys and Pancreas. Hence it helps in excluding any abnormality and to pick up early disease in any of these abdominal organs.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Sonography Pelvis</td>
                                        <td data-title="Screening Organ/ Body Part">Pelvis</td>
                                        <td data-title="How the test is performed">Ultrasound of the Pelvis</td>
                                        <td data-title="Purpose of the test">In females it helps in examination of Urinary Bladder, Uterus and Ovaries. It helps in ruling out any abnormalities in these organs like fibroids or cysts. In males it is important for screening of any pathology related to the Urinary Bladder and Prostate.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Sono Mammography</td>
                                        <td data-title="Screening Organ/ Body Part">Breasts</td>
                                        <td data-title="How the test is performed">Ultrasound of the breasts</td>
                                        <td data-title="Purpose of the test">Combined with mammography it further helps in grading the breast masses & lesions and for differentiating benign from malignant masses. Ultrasonography of the breast is also helpfulin giving guidance for FNAC's and biopsies from breast.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Stress Test</td>
                                        <td data-title="Screening Organ/ Body Part">Heart</td>
                                        <td data-title="How the test is performed">The test is a continuous ECG and BP monitoring done during exercise on a treadmill.</td>
                                        <td data-title="Purpose of the test">This test is useful for detection of Coronary Artery Disease and to evaluate functional capacity of the heart.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">T3, T4, TSH</td>
                                        <td data-title="Screening Organ/ Body Part">Thyroid</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Helps in early detection of Thyroid problems.</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Vitamin B12</td>
                                        <td data-title="Screening Organ/ Body Part">Whole body</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Deficiency or excess of the vitamin can be corrected effectively</td>
                                    </tr>
                                    <tr>
                                        <td data-title="Name of Test">Vitamin D3</td>
                                        <td data-title="Screening Organ/ Body Part">Bones</td>
                                        <td data-title="How the test is performed">Through blood sample</td>
                                        <td data-title="Purpose of the test">Deficiency or excess of the vitamin can be corrected effectively</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="clear"></div>
                        </div>
                    </div>--%>

                <!-- xxxxxxxxxxxxxxxxxx -->





                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

