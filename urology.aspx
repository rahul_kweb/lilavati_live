﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="urology.aspx.cs" Inherits="urology" %>

<%@ Register TagPrefix="menu" TagName="Services" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/equate.js"></script>
    <script type="text/javascript" src="js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Urology</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/Urology.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabs_main">
                    <ul id="tab_list" class="tab_list">
                        <asp:Repeater ID="rptContent" runat="server">
                            <ItemTemplate>
                                <li><a href="#" rel='tab_<%#Eval("INNER_ID") %>'><%#Eval("TAB") %> </a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <li><a href="#" rel='tab_Dr'>Professionals</a></li>
                    </ul>

                    <!--  xxxxxxxxxxxx  -->

                    <asp:Repeater ID="rptContent1" runat="server">
                        <ItemTemplate>
                            <div id='tab_<%#Eval("INNER_ID") %>'>
                                <%#Eval("DESCRIPTION") %>
                                <div class="clear"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div id='tab_Dr'>
                        <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <%--   <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">
                            <asp:Literal ID="litDocDetails" runat="server"></asp:Literal>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <%--  <div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>We have well equipped and an advanced Urology department with highly qualified and experienced Urologists in our team supported with cutting edge technology including 100 w Holmium Laser, Flexible Ureteroscope, etc.</p>
                            <p>We have experts in all subspecialties of urology like Anrogy, male infertility, Female Urology, Paediatric Urology, Uro-oncology laparoscopic and Endourology.</p>
                            <p>Our department is accredited by National Board of Examination, New Delhi for conducting super specialty post-graduation in Urology. The department is admitting 2 DNB trainees per year through national selection process. Our well organized academic programme and work exposure for post graduate residents have given good results in DNB examination. Work load handled is more than 1300 major and minor surgeries each year with excellent results and patient satisfaction rate.</p>
                            <p>In terms of outdoor patient consultation the hospital has well organized OPD setup with services available whole day. The department has most experienced doctors in the field of urology who are pioneer in this field. The department is currently having 9 consultants and 6 DNB trainees dedicated to serve the patient.</p>
                            <p>Investigations available are all urological blood investigations, Radiological Investigations like CT scan, MRI prostate, PET scan,USG guided Prostate Biopsies, Histopathological services etc.</p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">

                            <p>The dept is offering all kinds of advanced facilities including Endourology, laparoscopic and reconstructive surgeries.</p>

                            <ul class="bullet">
                                <li><strong>Endourology :</strong>
                                    <ul>
                                        <li>Internal Urethotomy,</li>
                                        <li>VIU</li>
                                        <li>Cystolithotripsy,</li>
                                        <li>TUR Prostate</li>
                                        <li>HOLeP (Holmium laser resection of prostate)</li>
                                        <li>TURBT (Bladder Tumor resection)</li>
                                        <li>RIRS</li>
                                        <li>Ureterorenoscopy</li>
                                        <li>P.C.N.L.</li>
                                        <li>Advanced Laparoscopic Surgeries</li>
                                    </ul>
                                </li>
                                <li><strong>Open urology operations :</strong> Various urologic operations of Kidney, Ureter & Bladder like LITHOTRIPY FOR URINARY STONE DISEASE (ESWL)</li>
                                <li><strong>Infertility & Impotence :</strong> Penile prosthesis,Microsurgery for male infertility</li>
                                <li><strong>Renal Transplantation :</strong> Living and Cadaveric Donor transplant</li>
                                <li><strong>Paediatric Urology :</strong> Hypospadias, VU reflux, PU Valves, PCNL</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Technology and Infrastructure</h2>

                        <div class="tab_content">

                            <ul class="bullet">
                                <li>ENDO-UROLOGY
        <ul>
            <li>LASER GUIDED URS</li>
            <li>LITHOCLAST (AMS)</li>
            <li>RIRS (FLEXIBLE URETEROSCOPE)</li>
            <li>HOLEP (100 watt Holmium)</li>
            <li>MiniPerc</li>
        </ul>
                                </li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="tabbertab">
                        <h2>Professionals</h2>

                        <div class="tab_content">



                            <div class="clear"></div>
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>--%>

                  <div class="dwn_brochures">
                    <div class="dwn_tl"><i class="fa fa-file-pdf-o" style="font-size: 18px;"></i>&nbsp; <strong>Download Brochure</strong></div>
                    <ul class="ped fade_anim">
                        <asp:Repeater ID="rptBrochure" runat="server">
                            <ItemTemplate>
                                <li>
                                    <a href='<%#Eval("Brochure","uploads/brochure/{0}") %>' target="_blank">
                                        <img src='<%#Eval("ThumbImage","uploads/brochure/{0}") %>'>
                                        <span><%#Eval("TITTLE") %> </span></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Services ID="menu" runat="server" />

            </div>






            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript">
        var countries = new ddtabcontent("tab_list")
        countries.setpersist(true)
        countries.setselectedClassTarget("link") //"link" or "linkparent"
        countries.init()
    </script>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

