﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="muhs_Fellowship.aspx.cs" Inherits="MUHS" %>

<%@ Register TagName="Professionals" TagPrefix="menu" Src="Control/professionals.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">MUHS Fellowship</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/hospital.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <asp:Literal ID="litContent" runat="server"></asp:Literal>

                  <%--              <p><strong>Below candidates have sucessfully cleared MUHS Fellowship Examinations for Academic Year 2018-19</strong></p>

                <ul class="doc_search_result prof_tab fade_anim">
                    <li>
                        <div class="doc_div inner" style="padding-bottom:10px">
                            <div class="prof_holder">
                                <img src="Admin/Doctors/Anuj.jpg">
                                <span class="name green_text"><strong>Dr. Anuj Jain</strong></span>
                                <span class="desig">MUHS Fellow in Chronic Pain Medicine</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                     <li>
                        <div class="doc_div inner" style="padding-bottom:10px">
                            <div class="prof_holder">
                                <img src="Admin/Doctors/Hemesh.jpg">
                                <span class="name green_text"><strong>Dr. Hemesh Shewale</strong></span>
                                <span class="desig">MUHS Fellow in Chronic Pain Medicine</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                     <li>
                        <div class="doc_div inner" style="padding-bottom:10px">
                            <div class="prof_holder">
                                <img src="Admin/Doctors/neha.jpg">
                                <span class="name green_text"><strong>Dr. Neha Jain</strong></span>
                                <span class="desig">MUHS Fellow in Microsurgery</span>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                </ul>--%>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Professionals ID="menu" runat="server" />

            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>

</asp:Content>

