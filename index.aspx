﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true"
    CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta charset="UTF-8">
    <title>Lilavati Hospital & Research Centre | Top Multi Speciality Hospital in India</title>
    <meta name="description" content="India’s top multi speciality hospital “Lilavati Hospital” have the world best doctors & teams to give you & your children the best healthcare that you truly deserve.">


    <link rel="stylesheet" type="text/css" href="css/slick.css" />
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            /* ----- Home Page Slider ----- */
            $('.home_carousel').slick({
                dots: false,
                infinite: true,
                speed: 1000,
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
                cssEase: 'ease',
                easing: 'ease-in',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: "unslick"
                    }
                ]
            });

            //	var slidefirst = jQuery('.slick-list').find('.sliderbox');
            //	slideHeight = slidefirst.height()
            //	slidefirst.css('width',slideHeight)

        });
    </script>
    <script type="text/javascript" src="js/jquery.cycle2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
    <style type="text/css">
        @media only screen and (min-width:1024px) {
            .foot_fixed {
                position: fixed;
                bottom: 0;
            }
        }

         .absolute_anchor {width:100%;height:100%;position:absolute;}
    </style>

    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();

            $(".fancybox_trig").fancybox({}).delay(5000).trigger("click");
            $(".fancybox-inner").html("<a href='https://www.lilavatihospital.com/Plastic-Surgery' class='absolute_anchor'></a>")
        });

        function FaqPageCall() {
            window.location.assign("Faq's-For-Fitness-Package");
        }

        function WHDFun() {
            window.location.assign("/Packages-Details/20?tab_list=1");
        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="index_container">
        <div class="home_carousel">
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div sliderbox">
                <div class="slider cycle-slideshow" data-cycle-timeout="15000" data-cycle-speed="1000" data-cycle-slides="> a">
                    <%--<img src="images/slides/WHD.jpg" onclick="WHDFun();" style="cursor: pointer" />
                    <img src="images/slides/slides.jpg" onclick="FaqPageCall();" style="cursor: pointer;" />--%>
                    <asp:Repeater ID="rptHomeBanner" runat="server">
                        <ItemTemplate>
                            <a href="<%#Eval("PAGE_URL").ToString()==""?'#':Eval("PAGE_URL") %>" target="_blank">
                                <img src='<%#Eval("IMAGE","uploads/home_banner/{0}") %>'>
                            </a>
                        </ItemTemplate>
                    </asp:Repeater>
                    <span class="cycle-pager"></span>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div small_width">
                <div class="tl_box">
                    QUICK LINKS <a href="#" class="viewall_btn fade_anim"></a>
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <ul class="box_links fade_anim">
							<li><a href="Video-Consultation"><span class="bl_img">
                                <img src="images/ql/teleconsultation.png" /></span> <span class="bl_txt">Video Consultation</span>
                            </a></li>
                              <li>
                                <a href="post-covid-care-healthcare-packages"><span class="bl_img">
                                    <img src="images/ql/Post_COVID_Care.png"/></span> <span class="bl_txt">Post COVID Care Healthcare packages</span> </a>
                            </li>
                             <li>
                                <a href="https://payment.atomtech.in/payment/form/pay.action?mId=3024EE386563138851A07B40D58EAE28" target="_blank"><span class="bl_img">
                                    <img src="images/ql/onlinepayment.png"/></span> <span class="bl_txt">Online Bill Payment (For Inpatients)</span> </a>
                            </li>
                            <li><a href="Book-Appointment"><span class="bl_img">
                                <img src="images/ql/ic_appointment.png" /></span> <span class="bl_txt">Book an Appointment</span>
                            </a></li>

                            <li><a href="Doctor"><span class="bl_img">
                                <img src="images/ql/ic_fd.png"></span> <span class="bl_txt">Find a Doctor</span>
                            </a></li>
                            <li><a href="Health-Checkup"><span class="bl_img">
                                <img src="images/ql/ic_hc.png"></span> <span class="bl_txt">Health Checkup</span>
                            </a></li>
                            <li>
                                <%-- <a href="Stents-and-TKR-implants"><span class="bl_img">
                                <img src="images/ql/ic_stent.png"></span> <span class="bl_txt blink" style="font-weight:bold">Stents and Knee implants available in Lilavati Hospital at Government Approved rates</span>
                            </a>--%>

                                <%--<a href="uploads/Final_Stents280817.pdf" target="_blank"><span class="bl_img">
                                <img src="images/ql/ic_stent.png"></span> <span class="bl_txt">Stents available in Lilavati Hospital at Government Approved rates</span>
                            </a>--%>

                                <a href="docs/Stents detailsnew.pdf" target="_blank"><span class="bl_img">
                                    <img src="images/ql/ic_stent.png"></span> <span class="bl_txt">Stents and Knee implants available in Lilavati Hospital at Government Approved rates</span>
                                </a>


                            </li>
                            <li><a href="Visa-Investigation"><span class="bl_img">
                                <img src="images/ql/ic_visa.png"></span> <span class="bl_txt">Visa Medicals</span>
                            </a></li>
                            <li>
                                <a href="Write-To-Us"><span class="bl_img">
                                    <img src="images/ql/ic_write.png"></span> <span class="bl_txt">Write to us</span> </a>
                            </li>
                            <li>
                                <a href="Request-An-Estimate"><span class="bl_img">
                                    <img src="images/ql/ic_re.png"></span> <span class="bl_txt">Request an estimate</span> </a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div small_width">
                <div class="tl_box">
                    CENTERS OF EXCELLENCE <a href="Centres-of-Excellence" class="viewall_btn fade_anim">View all</a>
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <ul class="box_links fade_anim">
                            <li><a href="liver-transplant"><span class="bl_img">
                                <img src="images/coe/liver_transplant.png"></span> <span class="bl_txt">Liver Transplant</span> </a></li>
                            <li><a href="Cardiology-CVTS"><span class="bl_img">
                                <img src="images/coe/cardiology.png"></span> <span class="bl_txt">Cardiology & Cardiac
                                    surgery</span> </a></li>
                            <li><a href="Neurology-Neuro-Surgery"><span class="bl_img">
                                <img src="images/coe/neurology.png"></span> <span class="bl_txt">Neurology & Neurosurgery</span>
                            </a></li>
                            <li><a href="Gynaecology-Obstetrics"><span class="bl_img">
                                <img src="images/coe/gynecology.png"></span> <span class="bl_txt">Gynaecology & Obstetrics</span>
                            </a></li>
                            <li><a href="Orthopaedics"><span class="bl_img">
                                <img src="images/coe/orthopedics.png"></span> <span class="bl_txt">Orthopedics</span>
                            </a></li>
                            <li><a href="Pediatrics-And-Paediatric-Surgery"><span class="bl_img">
                                <img src="images/coe/pediatrics.png"></span> <span class="bl_txt">Pediatrics</span>
                            </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div testimonials">
                <div class="tl_box">
                    TESTIMONIALS <a href="Testimonials" class="viewall_btn fade_anim">View all</a>
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <ul class="testimonial_list">
                            <asp:Repeater ID="rptTestimonial" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <a href='#<%#Container.ItemIndex+1 %>' class="fancybox">
                                            <div class="testimonial_img">
                                                <img src='<%#Eval("Celebrity_Image","uploads/testimonial/{0}") %>'>
                                                <span class="name"><%#Eval("Name") %></span>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="testimonial_txt"><%#Eval("Des") %></div>
                                        </a>

                                        <div class="testimonial_box" id='<%#Container.ItemIndex+1 %>'>
                                            <div class="img_box">
                                                <img src='<%#Eval("Celebrity_Image","uploads/testimonial/{0}") %>'>
                                            </div>
                                            <div class="testim_name green_text"><%#Eval("Name") %></div>
                                            <div class="testim_content">
                                                <%#Eval("Description") %>
                                            </div>
                                        </div>

                                    </li>

                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div small_width">
                <div class="tl_box">
                    UPDATES <a href="Updates" class="viewall_btn fade_anim">View all</a>
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <ul class="updates fade_anim">
                            <asp:Repeater ID="rptUpdates" runat="server">
                                <ItemTemplate>
                                    <li><a href='Updates?c_expand_hd=<%# Container.ItemIndex+0 %>'><%#Eval("UPD_TITTLE") %> </a></li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div small_width">
                <div class="tl_box">
                    HEALTH CHECKUP PACKAGES <a href="Health-Checkup" class="viewall_btn fade_anim">View all</a>
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <img src="images/health_chkup.jpg" style="margin: 20px 20px 15px 20px; display: block;">
                        <ul class="package_lists fade_anim">
                            <asp:Repeater ID="rptHealthCheckUp" runat="server">
                                <ItemTemplate>
                                    <li><a href='Packages-Details/<%#Eval("HEALTH_ID") %>'>
                                        <%#Eval("PACKAGE_NAME") %>
                                    </a></li>
                                </ItemTemplate>
                            </asp:Repeater>

                            <asp:Literal ID="ltrlget" runat="server"></asp:Literal>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <%-- comment on 27-1-20--%>
            <%--       <div class="scroll_div awards_width">
                <div class="tl_box">
                    AWARDS & ACCOLADES <a href="Awards-Accreditations" class="viewall_btn fade_anim">View all</a>
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <ul class="awards fade_anim">
                            <asp:Repeater ID="rptAwards" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <a href="Awards-Accreditations?c_expand_hd=<%#Container.ItemIndex+0 %>">
                                            <img src='<%#Eval("AWARDS_IMAGE","uploads/awards/{0}") %>'>
                                            <span>
                                                <%#Eval("AWARDS_TITTLE") %>
                                            </span>
                                        </a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>--%>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->

            <div class="scroll_div small_width">
                <div class="tl_box">SOCIAL INITIATIVES  <a href="Social-Initiative?tab_list=0" class="viewall_btn fade_anim">View all</a> </div>

                <div class="index_content_box">
                    <div class="scroll_box">

                        <ul class="social_init fade_anim">
                            <li>
                                <img src="images/sewa_logo.jpg" style="margin: 10px 18px 15px 18px; width: 180px; display: block;">
                                <ul class="bullet">
                                    <li>Free OPD</li>
                                    <li>Mobile clinic</li>
                                    <li>Nana-Nani</li>
                                </ul>
                            </li>
                            <li><a href="Social-Initiative?tab_list=1">Swastha Bachpan</a></li>
                            <li><a href="Social-Initiative?tab_list=2">Save & Empower Girl Child</a></li>
                            <li><a href="Social-Initiative?tab_list=3">Social Welfare Programs</a></li>
                        </ul>
                    </div>
                </div>

            </div>

            <%--<div class="scroll_div small_width">
                <div class="tl_box">SOCIAL INITIATIVES  <a href="Social-Initiative" class="viewall_btn fade_anim">View all</a> </div>

                <div class="index_content_box">
                    <div class="scroll_box">

                        <img src="images/sewa_logo.jpg" style="margin: 20px auto; width: 200px; display: block;">

                        <ul class="social_init fade_anim">

                            <li>SEWA Free OPD</li>
                            <li>SEWA Mobile clinic</li>
                            <li>Swastha Bachpan</li>
                            <li> Save & Empower Girl Child</li>
                            <li>Social Welfare Programs</li>
                        </ul>
                    </div>
                </div>

            </div>--%>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
            <div class="scroll_div small_width">
                <div class="tl_box">
                    SOCIAL MEDIA
                </div>
                <div class="index_content_box">
                    <div class="scroll_box">
                        <img src="images/sm_banner.jpg" style="margin: 15px auto; display: table;">
                        <ul class="social_btns fade_anim">
                            <li><a href="https://www.facebook.com/Lilavati-Hospital-Research-Centre-105138941628217" target="_blank">
                                <img src="images/btn_fb.png"></a></li>
                            <li><a href="https://twitter.com/LilavatiHRC" title="Twitter" target="_blank">
                                <img src="images/btn_tw.png"></a></li>
                            <li>
                                <a href="https://www.youtube.com/channel/UC1neYtQvPaf0PIlNhPZWQLQ" target="_blank"> 
                                <img src="images/btn_yt.png"></a></li>
                            <li><a href="https://www.instagram.com/lilavatihospital.official/?igshid=i2o1esmknup9" target="_blank">
                                <img src="images/btn_in.png"></a></li>
                            <li><a href="https://www.linkedin.com/company/18147846/" target="_blank">
                                <img src="images/btn_li.png"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- xxxxxxxxxxxxxxxxxxxxxxxxxx -->
        </div>
    <%--    <a class="fancybox_trig" href="/images/LHRC_ADVT_2021_09 - Plastic Surgery.jpg"></a>--%>
    </div>
    <script type="text/javascript" src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script>
        (function ($) {
            $(window).load(function () {

                $(".scroll_box").mCustomScrollbar({
                    set_height: "100%",
                    autoDraggerLength: false

                });

            });
        })(jQuery);

    </script>
    <%--<script>
        $(".fancybox_trig").fancybox({}).delay(5000).trigger("click");
        $(".fancybox-inner").html("<a href='https://gjc.org.in/nja_segment.php' class='absolute_anchor'></a>")
    </script>--%>

    
</asp:Content>
