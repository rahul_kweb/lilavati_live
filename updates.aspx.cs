﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class updates : System.Web.UI.Page
{
    Updates tblUpdates = new Updates();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindUpdates();
        }
    }

    public void BindUpdates()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetUpdatesPage();

        if (dt.Rows.Count > 0)
        {
            rptUpdates.DataSource = dt;
            rptUpdates.DataBind();
        }
        else
        {
            rptUpdates.DataSource = null;
            rptUpdates.DataBind();
        }

    }

    protected void rptUpdates_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            var hdnCont = (HiddenField)e.Item.FindControl("hdnConent");

            string Cnt = hdnCont.Value;
            Cnt = Cnt.Replace("../images", "/images");

            var litCnt = (Literal)e.Item.FindControl("litContent");
            litCnt.Text = Cnt;
        }
    }
}