﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="out_patient.aspx.cs" Inherits="out_patient" %>

<%@ Register TagName="Service" TagPrefix="menu" Src="Control/services.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">

        <div class="green_banner">
            <div class="page_title">Out Patient</div>
        </div>
        <div class="clear"></div>

        <div class="container">

            <div class="content_main_box text_format">

                <%--<div class="inner_banner">
                    <img src="images/inner_banner/out_patient.jpg">
                </div>--%>
                <asp:Literal ID="ltrlbanner" runat="server"></asp:Literal>

                <div class="print_share">
                    <a onclick="window.print();return false" title="Print" class="print"><i class="fa fa-print"></i></a>
                    <div class="addthis_native_toolbox"></div>
                    <div class="clear"></div>
                </div>

                <div class="tabber">

                    <asp:Repeater ID="rptContent" runat="server">
                        <ItemTemplate>
                            <div class="tabbertab">
                                <h2><%#Eval("TAB") %>  </h2>

                                <div class="tab_content">
                                    <%#Eval("DESCRIPTION") %>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>


                    <%--<div class="tabbertab">
                        <h2>Overview</h2>

                        <div class="tab_content">
                            <p>Our Outpatient department caters to approximately 1500 patients daily. This remarkable feat is achieved due to the state of the art infrastructure which includes 27 consultation rooms equipped with latest equipments and a dedicated team of specialised doctors and support staff.</p>

                            <p class="green_text"><strong>We provide following services under OPD:</strong></p>
                            <ul class="bullet">
                                <li>Various specialised consultations</li>
                                <li>Diagnostic facilities like Pathology, Radiology, Nuclear Medicine, MRI,CT, PET CT Scan, Endoscopy services</li>
                                <li>Minor procedures and dressing done under OPD services.</li>
                                <li>Vaccination is also administered for children.</li>
                            </ul>

                            <p class="green_text"><strong>Location</strong></p>
                            <ul class="bullet">
                                <li>Ground Floor (23 consultation rooms)</li>
                                <li>2nd Floor (4 consultation rooms)</li>
                            </ul>

                            <p class="green_text"><strong>Types of OPD</strong></p>
                            <ul class="bullet">
                                <li>Hospital OPD (8 am to 4 pm) * Consultation charges are fixed by the hospital</li>
                                <li>Private OPD (4 pm to 9 pm) * Consultation charges are variable. Payment to made to the consultant directly</li>
                            </ul>

                            <p class="green_text"><strong>Facilities available</strong></p>
                            <ul class="bullet">
                                <li>Radiological Investigations</li>
                                <li>Pathology sample collection room</li>
                                <li>Two treatment rooms for minor dressing / suturing etc.</li>
                                <li>Wheelchair is provided on request.</li>
                            </ul>

                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <%--<div class="tabbertab">
                        <h2>Services</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Anaesthesiology / Audiology & Speech Therapy / Cardiovascular & Thoracic Surgery / Cardiology / Chest Medicine / Colorectal Surgery / Chronic Pain Management / Critical Care Medicine / Dentistry /Dental Surgery / Dermatology / Dermo-Cosmetology Clinic / Diabetology / ENT Surgery / Gastro Surgery / Gastroenterology / General Surgery / Gynaecology / Haematology Clinical / Headache & Migraine Clinic / Interventional Radiology / Internal Medicine / Nuclear Medicine / Nephrology / Neurology / Neuropsychology / Neurosurgery / Oncology / Oncosurgery / Surgical Oncology / Ophthalmology / Orthopaedic Surgery / Paediatric Surgery / Paediatrics / Plastic Surgery / Psychiatrist / Psychologist / Rheumatology / Urology / Vascular Surgery /  /Pathology / Physiotherapy / Radiology & Imaging, Pulmonary Function Test (PFT).</li>
                                <li>HIV counselor is available in the OPD from 8am to 4pm for referring patients for pre-test counselling for HIV test.</li>
                                <li>Immunization</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <%--<div class="tabbertab">
                        <h2>For Appointments</h2>

                        <div class="tab_content">

                            <p>Contact <strong>022-26568050/51</strong> for requesting appointment or <a href="#">click here</a></p>
                            <p class="green_text"><strong>Some important points before you take an appointment</strong></p>

                            <ul class="bullet">
                                <li>All new patients need to register by paying a nominal one-time registration fee of Rs. 50/-. An ID card (LH card) will be issued to you, which would be valid for all the services you avail. Registration is mandatory and you could download the form here.</li>
                                <li>If you have registered previously, please keep your LH card number handy before taking an appointment.</li>
                                <li>If you walk-in without an appointment you may have to wait for your turn after all the appointment holders meet the doctor hence, it is advisable to book an appointment.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <%--<div class="tabbertab">
                        <h2>Billing and Payments</h2>

                        <div class="tab_content">
                            <ul class="bullet">
                                <li>Hospital OPD consultation charge is Rs. 1,500/-.</li>
                                <li>Follow-up consultation will be valid for 30 days from the date of consultation. The fees for the same is Rs. 900/-.</li>
                                <li>Hospital billing will be done at the billing counter.</li>
                                <li>Private consulting fees to be paid to doctor directly.</li>
                                <li>OPD charges to be paid at OPD billing counter.</li>
                                <li>Payments/ Deposits are accepted through cash, credit card, pay order & demand draft which shall be in favour of ‘LILAVATI HOSPITAL RESEARCH CENTRE’. We also accept payment via RTGS (you can get details at billing counter).</li>
                                <li>In case due to breakdown of any machine or if the doctor had to suddenly cancel appts due to emergency ,refund will be given or the same receipt can be utilised later for the same but within one month from the date of issue.</li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>--%>

                    <!-- xxxxxxxxxxxxxxxxxx -->

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div>


            <div class="box_aside">

                <menu:Service ID="menu" runat="server" />

            </div>



            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b6b76ec43947a8"></script>
</asp:Content>

