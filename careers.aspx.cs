﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class careers : System.Web.UI.Page
{
    CMSBAL cmsbal = new CMSBAL();
    Job_Posting tblJobPosting = new Job_Posting();
    Career Obj = new Career();
    BAL balobj = new BAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        //anchoridformsg.Attributes.Add("onclick", "javascript:return clickanchorformsg();");
        //anchoridformsg.Click += new System.EventHandler(this.HtmlAnchor_Click);

        

        if (!IsPostBack)
        {
            BindDepartment();
            ddlDepartment.Items.Insert(0, "");
            ddlPosition.Items.Insert(0, "");
            //BindRepeater();
            pnlCareer.Visible = false;
        }
    }

    public void BindDepartment()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.BindJobArea();

        if (dt.Rows.Count > 0)
        {            
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataValueField = "JOB_ID";
            ddlDepartment.DataTextField = "JOB_AREA";
            ddlDepartment.DataBind();
        }
    }

    public void BindRepeater()
    {
        DataTable dt = new DataTable();
        dt = cmsbal.GetJobPosting();
        if (dt.Rows.Count > 0)
        {            
            rptOpening.DataSource = dt;
            rptOpening.DataBind();
        }
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Id = ddlDepartment.SelectedValue;
        //tblJobPosting.Job_Id = int.Parse(Id);
        if (Id != "")
        {
            tblJobPosting.Job_Id = int.Parse(Id);
        }    

        DataTable dt = new DataTable();
        dt = cmsbal.CareerDeparmentByPosition(tblJobPosting);

        if (dt.Rows.Count > 0)
        {
            pnlCareer.Visible = true;

            ddlPosition.DataSource = dt;
            ddlPosition.DataTextField = "POSITION";
            ddlPosition.DataValueField = "POST_ID";
            ddlPosition.DataBind();

            ddlPosition.Items.Insert(0, "");

            rptOpening.DataSource = dt;
            rptOpening.DataBind();
        }
        else
        {
            pnlCareer.Visible = false;
            rptOpening.DataSource = null;
            rptOpening.DataBind();

            ddlPosition.Items.Clear();
        }

    }

    protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
    {
        string Id = ddlPosition.SelectedValue;
        if (Id != "")
        {
            tblJobPosting.Post_Id = int.Parse(Id);
        }

        DataTable dt = new DataTable();
        dt = cmsbal.CareerPositionWiseRecord(tblJobPosting);

        if (dt.Rows.Count > 0)
        {
            rptOpening.DataSource = dt;
            rptOpening.DataBind();
        }
        else
        {
            string Id1 = ddlDepartment.SelectedValue;
            tblJobPosting.Job_Id = int.Parse(Id1);

            DataTable dt1 = new DataTable();
            dt1 = cmsbal.CareerDeparmentByPosition(tblJobPosting);

            if (dt1.Rows.Count > 0)
            {
                pnlCareer.Visible = true;
                rptOpening.DataSource = dt1;
                rptOpening.DataBind();
            }
            else
            {
                pnlCareer.Visible = false;
                rptOpening.DataSource = null;
                rptOpening.DataBind();
            }
        }
    }

    protected void btnViewAll_Click(object sender, EventArgs e)
    {
        BindRepeater();

        ddlDepartment.SelectedIndex = -1;
        //ddlPosition.SelectedIndex = -1;
        ddlPosition.Items.Clear();
    }

    protected void rptOpening_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdID = (HiddenField)e.Item.FindControl("hdnId");
            string val = hdID.Value;
        }
    }

    [WebMethod]
    public static string BindDropDown(string id)
    {
        string Flag = "Invalid";
        Utility util = new Utility();
        DataTable dt = new DataTable();
        // DataSet ds = new DataSet("filterResult");
        string tablename = "Dropdownlist";
        DataSet ds = new DataSet(tablename);
        string postid = id;

        try
        {
            dt = util.Display("select JP.Post_Id,JP.Job_Id,JP.Position,JA.Job_Area from Job_Posting JP inner join  Job_Area as JA ON JP.Job_Id=JA.Job_Id  where JP.Post_Id='" + postid + "'");

            if (dt.Rows.Count > 0)
            {
                ds.Tables.Add(dt);

            }
        }
        catch (Exception ex)
        {

            throw ex;
        }

        //Send message
        return ds.GetXml();
    }




    //private string SaveFile(string fl)
    //{
    //    string extensionI1 = System.IO.Path.GetExtension(filepc.FileName);
    //    if (extensionI1 != "")
    //    {
    //        filepc.SaveAs(Server.MapPath("Admin\\Resume\\" + fl + extensionI1 + ""));
    //        Obj._FileExt = Server.MapPath("Admin\\Resume\\" + fl + extensionI1);
    //    }
    //    return extensionI1;

    //}


    protected void btnsubmit_Click(object sender, EventArgs e)
    {


        string Departmentname = hdndepartmentname.Value;
        string DepartmenId = hdndepartmentid.Value;
        string Positionname = hdnpositionname.Value;
        Obj._DepartmentName = Departmentname;
        if (Obj._DepartmentId.ToString() != "0")
        {
            Obj._DepartmentId = int.Parse(DepartmenId);
        }
        Obj._PostionName = Positionname;
        Obj._FirstName = txtfname.Text.Trim();
        Obj._LastName = txtlname.Text.Trim();
        Obj._DOB = txtDob.Text.Trim();
        Obj._Gender = ddlgender.SelectedValue;
        Obj._Phone = txtcontactno.Text.Trim();
        Obj._Residentof = txtResident.Text.Trim();
        Obj._Presentlyworkinglocation = txtpresentworkinglocation.Text.Trim();
        Obj._RelevantExp = txtrelaventexp.Text.Trim();
        Obj._TotalExp = txttotalexp.Text.Trim();
        Obj._MonthlyexpectedSalary = txtmonthexpsalary.Text.Trim();
        Obj._Qualification = txtqualification.Text.Trim();
        Obj._HostelAccommodationrequired = ddlhostelaccommodation.SelectedValue;
        Obj._RegistrationwithMMC = ddlregistrationwithmmc.SelectedValue;
        Obj._Fileofcv = filepc.FileName;
        Obj._Emailid = txtemail.Text.Trim();
        Obj._Fullpath = fakefilepc.Text.ToString();
        string extensionI1 = System.IO.Path.GetExtension(filepc.FileName);
        Obj._FileExt = extensionI1.ToString();

        int result = balobj.AddAndSendMail(Obj);

        if (result == 1)
        {

            #region  Send Mail
            string Subject = string.Empty;
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            //strEmailBuilder.Append("<table border=\"1\" cellspacing=\"3\" cellpadding=\"3\" align=\"left\" width=\"80%\" style=\"border-color: #aca899\">");
            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family:Arial, Helvetica, sans-serif; font-size:12px; border-collapse:collapse;\">");
            //strEmailBuilder.Append("<tr><td colspan=\"2\" align=\"center\">Job Application Details</td></tr>");
            strEmailBuilder.Append("<tr><td colspan=\"2\" align=\"center\" bgcolor=\"#04869a\"><strong><font color=\"#FFFFFF\"> JOB APPLICATION DETAILS </font></strong></td></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong>Position applied for : </strong></td><td>{0}</td></tr>", Obj._PostionName);
            Subject = "Lilavati Hospital,Job Application For The Post Of " + Obj._PostionName;
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td width=\"200px\"><strong>Deparment applied for : </strong></td><td>{0}</td></tr>", Obj._DepartmentName);
            strEmailBuilder.AppendFormat("<tr><td><strong>First Name : </strong></td><td>{0}</td></tr>", Obj._FirstName);
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Last Name : </strong></td><td>{0}</td></tr>", Obj._LastName);
            strEmailBuilder.AppendFormat("<tr><td><strong>Date of Birth : </strong></td><td>{0}</td></tr>", Obj._DOB);
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Gender : </strong></td><td>{0}</td></tr>", Obj._Gender);
            strEmailBuilder.AppendFormat("<tr><td><strong>Phone : </strong></td><td>{0}</td></tr>", Obj._Phone);
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Resident of : </strong></td><td>{0}</td></tr>", Obj._Residentof);
            strEmailBuilder.AppendFormat("<tr><td><strong>Presently working location : </strong></td><td>{0}</td></tr>", Obj._Presentlyworkinglocation);
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Relevant Experience in years : </strong></td><td>{0}</td></tr>", Obj._RelevantExp);
            strEmailBuilder.AppendFormat("<tr><td><strong>Total Experience : </strong></td><td>{0}</td></tr>", Obj._TotalExp);
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Monthly expected Salary : </strong></td><td>{0}</td></tr>", Obj._MonthlyexpectedSalary);
            strEmailBuilder.AppendFormat("<tr><td><strong>Qualification : </strong></td><td>{0}</td></tr>", Obj._Qualification);
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"><td><strong>Hostel Accommodation required : </strong></td><td>{0}</td></tr>", Obj._HostelAccommodationrequired);
            strEmailBuilder.AppendFormat("<tr><td><strong>Registration with MMC/ MNC : </strong></td><td>{0}</td></tr>", Obj._RegistrationwithMMC);
            strEmailBuilder.Append("</table>");


            filepc.SaveAs(Server.MapPath("Admin\\Resume\\" + Obj._Fileofcv + ""));
            Utility utility = new Utility();
            int Mail = utility.SendEMail("", Subject, strEmailBuilder.ToString(), "Resume", Obj._Fullpath);
            #endregion

            Reset();

            //Response.Write("<Script>alert('Your Application Form Sent Successfully!!!')</script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "PopUp", "ShowPopUp();", true);
        }
        else
        {
            Response.Write("<Script>alert('Your Application Form Could not Sent Successfully!!!')</script>");
        }
        //anchoridformsg.Attributes.Add("href", "http://google.com");
        //anchoridformsg.HRef = "#apply_form1";
    }



    void Reset()
    {
        hdndepartmentname.Value = string.Empty;
        hdndepartmentid.Value = string.Empty;
        hdnpositionname.Value = string.Empty;
        txtfname.Text = string.Empty;
        txtlname.Text = string.Empty;
        txtDob.Text = string.Empty;
        ddlgender.SelectedValue = string.Empty;
        txtcontactno.Text = string.Empty;
        txtResident.Text = string.Empty;
        txtpresentworkinglocation.Text = string.Empty;
        txtrelaventexp.Text = string.Empty;
        txttotalexp.Text = string.Empty;
        txtmonthexpsalary.Text = string.Empty;
        txtqualification.Text = string.Empty;
        ddlhostelaccommodation.SelectedValue = string.Empty;
        ddlregistrationwithmmc.SelectedValue = string.Empty;
        txtemail.Text = string.Empty;
        fakefilepc.Text = string.Empty;
        ddlDepartment1.SelectedValue = string.Empty;
        ddlPosition1.SelectedValue = string.Empty;
      }

   
  
}