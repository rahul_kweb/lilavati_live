﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class chest_medicine : System.Web.UI.Page
{
    CMSInnerPageMaster tblcmsInnerPageMaster = new CMSInnerPageMaster();
    CMSBAL cmsbal = new CMSBAL();
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindRepeater();
            DoctorDetails();
            BindBanner();
            BindBrochure();
        }
    }

    public void BindRepeater()
    {
        tblcmsInnerPageMaster.PageName = "Lactation";
        DataTable dt = new DataTable();
        dt = cmsbal.BindRepeaterCMS(tblcmsInnerPageMaster);

        if (dt.Rows.Count > 0)
        {
            rptContent.DataSource = dt;
            rptContent.DataBind();

            rptContent1.DataSource = dt;
            rptContent1.DataBind();
        }
    }

    public void DoctorDetails()
    {
        StringBuilder strBuil = new StringBuilder();
        //DataTable dt = new DataTable();
        //dt = utility.Display("EXEC AddUpdateGetDoctorSpecialityMaster 'GET_DR_BY_SPECIALTY',0,0,63");
		SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT DOCTOR_ID_BINT,DOCTOR_NAME_VCR,PHOTO_VCR,QUALIFICATION_VCR,CONSDOCID,IS_ACTIVE_BIT,DirectAvailable FROM DOCTORMASTER WHERE DOCTOR_ID_BINT IN (366,367) AND  IS_ACTIVE_BIT = 1", con);
        DataTable dt = new DataTable();
        SqlDataAdapter sa = new SqlDataAdapter(cmd);
        sa.Fill(dt);
        strBuil.Append("<ul class='doc_search_result prof_tab fade_anim'>");

        foreach (DataRow dr in dt.Rows)
        {
            string consdrid = string.Empty;
            if (dr["CONSDOCID"].ToString() != "")
            {
                consdrid = dr["CONSDOCID"].ToString();
            }
            else
            {
                consdrid = "0";
            }

            //strBuil.Append("<li>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='doc_div inner'>");
            //strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            //strBuil.Append("<span class='name green_text'><strong>");
            //strBuil.Append(dr["DOCTOR_NAME_VCR"].ToString());
            //strBuil.Append("</strong></span>");
            //strBuil.Append("<span class='desig'>");
            //strBuil.Append("" + dr["QUALIFICATION_VCR"].ToString() + "");
            //strBuil.Append("</span>");
            //strBuil.Append("<span class='view_btn'>");
            //strBuil.Append("View Profile");
            //strBuil.Append("</span>");
            //strBuil.Append("<div class='clear'>");
            //strBuil.Append("</div>");
            //strBuil.Append("</a>");
            //strBuil.Append("</li>");


            strBuil.Append("<li>");
            strBuil.Append("<div class='doc_div inner'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='prof_holder'>");
            strBuil.Append("<img src=Admin/Doctors/" + dr["PHOTO_VCR"].ToString() + ">");
            if (dr["DOCTOR_ID_BINT"].ToString() == "334")
            {
                strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString().Replace("Dr.","Ms.") + " </strong></span>");
            }
            else
            {
                strBuil.Append("<span class='name green_text'><strong> " + dr["DOCTOR_NAME_VCR"].ToString() + " </strong></span>");
            }
            strBuil.Append("<span class='desig'> " + dr["QUALIFICATION_VCR"].ToString() + "</span>");
            strBuil.Append("</a>");
            strBuil.Append("<div class='docBtns'>");
            strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            if (bool.Parse(dr["DirectAvailable"].ToString()) == false)
            {
                strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            }
            strBuil.Append("</div>");
            //strBuil.Append("<a href='Doctorprofile/" + dr["DOCTOR_ID_BINT"].ToString() + "/" + consdrid + "' class='view_btn'>View<br> Profile</a>");
            //strBuil.Append("<a href='Book-Appointment/" + dr["DOCTOR_ID_BINT"].ToString() + "'  class='app_btn'>Book an<br> Appointment</a>");
            strBuil.Append("<div class='clear'></div>");
            strBuil.Append("</div>");
            strBuil.Append("</li>");      
        }

        litDocDetails.Text = strBuil.ToString();
    }

    public void BindBanner()
    {
        BAL bal = new BAL();
        ltrlbanner.Text = bal.GetBannerForBinding(66);
    }


    public void BindBrochure()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["AppCon"].ConnectionString);
        SqlCommand cmd = new SqlCommand("SELECT * FROM PATIENTS_EDUCATION WHERE ISACTIVE = 1 AND Pat_Id IN (18)", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);

        if (dt.Rows.Count > 0)
        {
            rptBrochure.DataSource = dt;
            rptBrochure.DataBind();
        }
        else
        {
            rptBrochure.DataSource = null;
            rptBrochure.DataBind();
        }
    }
}