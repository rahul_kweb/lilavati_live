﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class management : System.Web.UI.Page
{
    CmsMaster cms = new CmsMaster();
    CMSBAL cmsbal = new CMSBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Content();
        }
    }

    public void Content()
    {
        cms.ID = 4;
        cmsbal.GETBYIDContent(cms);       

        StringBuilder cnt = new StringBuilder(cms.Description);

        cnt.Replace("../images", "images");
        cnt.Replace("&nbsp;", " ");

        litContent.Text = cnt.ToString();
    }
}