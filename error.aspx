﻿<%@ Page Title="" Language="C#" MasterPageFile="MainMaster.master" AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="inner_page_container">
        <div class="green_banner">
            <div class="page_title">Page Not Found</div>
        </div>
        <div class="clear"></div>
        <div class="container">
            <h2>We are very sorry for the inconvenience caused to you...</h2>
        </div>
        <div class="clear"></div>
    </div>
</asp:Content>

